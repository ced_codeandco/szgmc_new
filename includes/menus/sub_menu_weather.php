﻿		   <div id="szgmc_menu" style="display:none" class="dropdown_menu szgmc">
            <div class="top_arrow">&nbsp;</div>
			<ul>
			 <li><a href="<?php echo $site_path; ?>about-szgmc"><span class="p_top p_left">&#1606;&#1576;&#1584;&#1577; &#1593;&#1606; &#1575;&#1604;&#1605;&#1585;&#1603;&#1586;</span></a></li>
			 <li><a href="<?php echo $site_path; ?>minister-hh"><span class="p_top">كلمة سمو الشيخ </span></a></li>
			 <li><a href="<?php echo $site_path; ?>chairmans-message"><span class="p_top">&#1603;&#1604;&#1605;&#1577; &#1585;&#1574;&#1610;&#1587; &#1605;&#1580;&#1604;&#1587; &#1575;&#1604;&#1571;&#1605;&#1606;&#1575;&#1569;</span></a></li>
			 <li><a href="<?php echo $site_path; ?>DG-message"><span class="p_top">كلمة المدير العام</span></a></li>
			 <li><a href="<?php echo $site_path; ?>board-members"><span class="p_top">&#1571;&#1593;&#1590;&#1575;&#1569; &#1605;&#1580;&#1604;&#1587; &#1575;&#1604;&#1571;&#1605;&#1606;&#1575;&#1569;</span></a></li>
			 <li><a href="<?php echo $site_path; ?>organizational-structure"><span class="p_top">&#1575;&#1604;&#1607;&#1610;&#1603;&#1604; &#1575;&#1604;&#1578;&#1606;&#1592;&#1610;&#1605;&#1610;</span></a></li>
			 <li class="no_divider"><a href="<?php echo $site_path; ?>vision-mission-values"><span class="p_top">الرؤية والرسالة</span></a></li>
			</ul>
           </div>

           <div id="founder_menu" style="display:none" class="dropdown_menu founder">
           <div class="top_arrow">&nbsp;</div>
		   <ul>
			 <li ><a href="<?php echo $site_path; ?>founder"><span class="p_top p_left">&#1605;&#1587;&#1610;&#1585;&#1577; &#1602;&#1575;&#1574;&#1583;</span></a></li>
			 <li><a href="<?php echo $site_path; ?>selected-sayings-by-sheikh-zayed-al-nahyan"><span class="p_top">&#1605;&#1606; &#1571;&#1602;&#1608;&#1575;&#1604; &#1575;&#1604;&#1588;&#1610;&#1582; &#1586;&#1575;&#1610;&#1583; &#1576;&#1606; &#1587;&#1604;&#1591;&#1575;&#1606;</span></a></li>
			 <li><a href="<?php echo $site_path; ?>they-said-about-zayed"><span class="p_top">&#1602;&#1575;&#1604;&#1608;&#1575; &#1601;&#1610; &#1575;&#1604;&#1588;&#1610;&#1582; &#1586;&#1575;&#1610;&#1583; &#1576;&#1606; &#1587;&#1604;&#1591;&#1575;&#1606;</span></a></li>
			 <li class="no_divider"><a href="<?php echo $site_path; ?>sheikh-zayed-and-grand-mosque"><span class="p_top">&#1575;&#1604;&#1588;&#1610;&#1582; &#1586;&#1575;&#1610;&#1583; &#1608;&#1575;&#1604;&#1580;&#1575;&#1605;&#1593; &#1575;&#1604;&#1603;&#1576;&#1610;&#1585;</span></a></li>
			
			</ul>
           </div> 
		   
		   <div id="about_menu" style="display:none" class="dropdown_menu about">
           <div class="top_arrow">&nbsp;</div>
		   <ul>
			 <li><a href="<?php echo $site_path; ?>vision-dream-sheikh-zayed-mosque"><span class="p_top p_left">&#1575;&#1604;&#1585;&#1572;&#1610;&#1577; &#1608;&#1575;&#1604;&#1581;&#1604;&#1605;</span></a></li>
             
             <li ><a href="<?php echo $site_path; ?>achievement"><span class="p_top">&#1575;&#1604;&#1573;&#1606;&#1580;&#1575;&#1586;</span></a></li>
			 <!-- <li ><a href="<?php echo $site_path; ?>grand-mosque-message"><span class="p_top">&#1585;&#1587;&#1575;&#1604;&#1577; &#1575;&#1604;&#1580;&#1575;&#1605;&#1593;</span></a></li> -->
             
			
			
			</ul>
           </div> 
		   
		   <div id="archi_menu" style="display:none" class="dropdown_menu archi">
           <div class="top_arrow">&nbsp;</div>
		   <ul>
			 <li ><a href="<?php echo $site_path; ?>general-architecture"><span class="p_top p_left">&#1606;&#1592;&#1585;&#1577; &#1593;&#1575;&#1605;&#1577;</span></a></li>
             <li><a href="<?php echo $site_path; ?>domes"><span class="p_top">&#1575;&#1604;&#1602;&#1576;&#1575;&#1576;</span></a></li>
			 <li><a href="<?php echo $site_path; ?>marbles"><span class="p_top">&#1575;&#1604;&#1585;&#1582;&#1575;&#1605;</span></a></li>
			 <li><a href="<?php echo $site_path; ?>lunar-illumination"><span class="p_top">&#1575;&#1604;&#1573;&#1590;&#1575;&#1569;&#1577; &#1575;&#1604;&#1602;&#1605;&#1585;&#1610;&#1577;</span></a></li>
			 <li><a href="<?php echo $site_path; ?>carpets"><span class="p_top">&#1575;&#1604;&#1587;&#1580;&#1575;&#1583;&#1577;</span></a></li>
			 <li><a href="<?php echo $site_path; ?>chandeliers"><span class="p_top">&#1575;&#1604;&#1579;&#1585;&#1610;&#1617;&#1575;&#1578;</span></a></li>
			 <li class="no_divider"><a href="<?php echo $site_path; ?>pulpit"><span class="p_top">&#1575;&#1604;&#1605;&#1606;&#1576;&#1585;</span></a></li>
			</ul>
           </div>