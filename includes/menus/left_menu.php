<?php //echo $_SERVER['PHP_SELF']; ?><div class="nav">
        	<div class="navigation">
 <div id="smoothmenu1" class="ddsmoothmenu mosquemanner">
	<ul>
		
		 <li><a href="<?php echo $site_path; ?>about-szgmc" class="about_sz">عن المركز</a>
			<ul>
				<li><a href="<?php echo $site_path; ?>minister-hh"><!--كلمة سمو الشيخ منصور بن زايد-->كلمة سمو الشيخ منصور</a></li>
				<li><a href="<?php echo $site_path; ?>chairmans-message">كلمة رئيس مجلس الأمناء</a></li>
				<li><a href="<?php echo $site_path; ?>DG-message">كلمة المدير العام</a></li>
				<!--<li><a href="<?php echo $site_path; ?>board-members">أعضاء مجلس الأمناء</a></li>-->
				<li><a href="<?php echo $site_path; ?>organizational-structure">الهيكل التنظيمي</a></li>
				<li><a href="<?php echo $site_path; ?>vision-mission-values">الرؤية والرسالة والقيم</a></li>
				<li><a href="<?php echo $site_path; ?>sheikh-zayed-and-grand-mosque">الشيخ زايد والجامع الكبير</a></li>
		        <li><a href="<?php echo $site_path; ?>theory-and-implementation">مراحل البناء</a></li>
		      </ul>
               </li>
		<li><a href="<?php echo $site_path;?>visiting-the-mosque">زيارة الجامع</a>
			<ul>
                <li><a href="<?php echo $site_path; ?>visit-mosque-prayers">المصلون</a></li>
				<li><a href="<?php echo $site_path; ?>visit-mosque-visitors">الزوار</a></li>
				<li><a href="<?php echo $site_path; ?>visit-mosque-delegates">الوفود والشركات السياحية</a></li>
				<?php /*?><li><a href="<?php echo $site_path; ?>mosque-manner">آداب دخول الجامع</a></li>
				<li><a href="<?php echo $site_path; ?>mosque-opening-hours">أوقات الزيارة</a></li>
				<li><a href="<?php echo $site_path; ?>visitor-services">أنواع الجولات</a></li>
				
                <li><a href="<?php echo $site_path; ?>e-guide">المرشد الالكتروني</a></li>
                <li><a href="<?php echo $site_path; ?>what-is-tour"> ما هي الجولة الثقافية ؟ </a></li>
                
				<li><a href="<?php echo $site_path; ?>tour-booking-form"> احجز جولة ثقافية </a></li>
				<li><a href="<?php echo $site_path; ?>getting-to-the-mosque" >كيفية الوصول</a></li>
				<li><a href="https://www.google.com/maps/views/streetview/sheikh-zayed-grand-mosque?hr=ar&gl=us" target="_blank">التجول الافتراضي</a></li>
				<li><a href="http://www.tripadvisor.com.eg/Attraction_Review-g294013-d1492221-Reviews-Sheikh_Zayed_Grand_Mosque_Center-Abu_Dhabi_Emirate_of_Abu_Dhabi.html" target="_blank"><!--قـيّمنا--> تقييمك للزيارة</a></li>
				<li><a href="<?php echo $site_path; ?>questions">أسئلة متكررة</a></li>
                <li><a href="<?php echo $site_path; ?>important-information">معلومات تهمك</a></li>
                <li><a href="<?php echo $site_path; ?>esurvey">الإستبيان الإلكتروني</a></li><?php */?>
		       </ul>
		</li>
		<li><a href="<?php echo $site_path; ?>e_services.php">الخدمات الإلكترونية</a>
		
			<ul>
			 <li><a href="<?php echo $site_path; ?>tour-booking-form">الحجوزات السياحية</a></li>
			 <li><a href="<?php echo $site_path; ?>careers">وظائف</a></li>
			 <li><a href="<?php echo $site_path; ?>media-form">تصريح تسجيل بالفيديو</a></li>
			 <li><a href="<?php echo $site_path; ?>lost-found">المفقودات</a></li>
             <!--<li><a href="<?php /*echo $site_path; */?>log_book.php">سجل الأحوال اليومية</a></li>-->
             <li><a href="<?php echo $site_path; ?>suggestion-complaint">الشكاوي والاقتراحات</a></li>
             <?php /*<li><a href="<?php echo $site_path; ?>juniorculturalguide.php">برنامج المرشد الثقافي الصغير</a></li>
             <li><a href="<?php echo $site_path; ?>quran_recitation_course">دورة تجويد القرآن  </a></li>*/?>
			</ul>
		</li>
		<li><a href="javascript:void(0);">الركن الإعلامي</a>
			<ul>
                <li><a href="<?php echo $site_path; ?>news-list">الأخبار</a></li>
                <li><a href="<?php echo $site_path; ?>press-kit">المواد الإعلامية</a></li>
                <li><a href="<?php echo $site_path; ?>eparticipation">المشاركة الإلكترونية</a></li>
            </ul>
		</li>
		<li><a href="javascript:void(0);">المكتبة</a>
			<ul>
				<li><a href="<?php echo $site_path; ?>about-the-library">عن المكتبة</a></li>
				<li><a href="<?php echo $site_path; ?>library-resources">المقتنيات</a></li>
				<li><a href="<?php echo $site_path; ?>library-services">خدمات المكتبة</a></li>
				<li><a href="<?php echo $site_path; ?>search-library">البحث</a></li>
				<li><a href="<?php echo $site_path; ?>publications">اصدارات المركز</a></li>
			</ul>
		
		</li>
		<li><a href="<?php echo $site_path; ?>events-and-activities">أنشطة وفعاليات</a>
            <ul>
                <li><a href="<?php echo $site_path; ?>exhibitions">
                        المعارض
                    </a> </li>
                <li><a href="<?php echo $site_path; ?>activities">
                        الأنشطة
                    </a> </li>
                <li><a href="<?php echo $site_path; ?>social-initiatives">
                        المبادرات المجتمعية
                    </a> </li>
                <li><a href="<?php echo $site_path; ?>spaces-of-light">
                        جائزة فضاءات من نور
                    </a> </li>
            </ul>
        </li>
        <li><a href="<?php echo $site_path;?>religious-programs">البرامج الدينية</a>
            <ul>
                <li><a href="<?php echo $site_path; ?>religious-courses">الدورات الدينية</a></li>
                <?php /*?><li><a href="<?php echo $site_path; ?>religious-lecture">روائع التبيان</a></li><?php */?>
                <li><a href="<?php echo $site_path; ?>zikr-al-hakeem"> الذكر الحكيم</a></li>
                <li><a href="<?php echo $site_path; ?>friday-sermon"> خطب الجمعة</a></li>
                <li><a href="<?php echo $site_path; ?>ramadan-activities">فعاليات رمضان </a></li>
                <?php /*?><li><a href="<?php echo $site_path; ?>prayer-information">معلومات عن الصلوات </a></li><?php */?>	
            </ul>
        </li>
		<li><a href="<?php echo $site_path;?>suggestion-complaint">اقتراحات</a></li>
		<li><a href="<?php echo $site_path;?>architecture">الجماليات والعمارة</a>
           <ul>
			 <li><a href="<?php echo $site_path; ?>general-architecture"> نظرة عامة</a></li>
             <li><a href="<?php echo $site_path; ?>domes">&#1575;&#1604;&#1602;&#1576;&#1575;&#1576;</a></li>
			 <li><a href="<?php echo $site_path; ?>marbles">&#1575;&#1604;&#1585;&#1582;&#1575;&#1605;</a></li>
			 <li><a href="<?php echo $site_path; ?>lunar-illumination">&#1575;&#1604;&#1573;&#1590;&#1575;&#1569;&#1577; &#1575;&#1604;&#1602;&#1605;&#1585;&#1610;&#1577;</a></li>
			 <li><a href="<?php echo $site_path; ?>carpets">&#1575;&#1604;&#1587;&#1580;&#1575;&#1583;&#1577;</a></li>
			 <li><a href="<?php echo $site_path; ?>chandeliers">&#1575;&#1604;&#1579;&#1585;&#1610;&#1617;&#1575;&#1578;</a></li>
			 <li><a href="<?php echo $site_path; ?>pulpit">&#1575;&#1604;&#1605;&#1606;&#1576;&#1585;</a></li>

               <li><a href="<?php echo $site_path; ?>minaret">المنارة    </a></li>
               <li><a href="<?php echo $site_path; ?>reflective-pools"> الأحواض العاكسة</a></li>
               <li><a href="<?php echo $site_path; ?>columns"> الأعمدة   </a></li>
               <li><a href="<?php echo $site_path; ?>mihrab">المحراب   </a></li>
               <li><a href="<?php echo $site_path; ?>sahan">الصحن </a></li>

			</ul>
        
        </li>
		<li><a href="javascript:void(0);">البيانات المفتوحة</a>
			<ul class="open_data">
				<?php /*?><li><a href="<?php echo $site_path; ?>poll-archive" style="width:110px;">نتائج استطلاع الرأي</a></li><?php */?>
				<li><a href="<?php echo $site_path; ?>open-data" style="width:110px;">الاحصاءات</a></li>
			</ul>
		</li>
	</ul>    </div>
 </div>
        </div>