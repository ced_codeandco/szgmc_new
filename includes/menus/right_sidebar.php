<?php 
include '../../includes/functions.php';

$prayer_timings = getPrayers_newapi();

if(!empty($prayer_timings)){ ?>
<div class="prayer_l"><?php echo $prayer_timings['Fajr']; ?></div>&nbsp; <div class="prayer_r space">&#1575;&#1604;&#1601;&#1580;&#1585;</div>
<div class="prayer_l"><?php echo $prayer_timings['Shurooq']; ?></div>&nbsp; <div class="prayer_r">&#1575;&#1604;&#1588;&#1585;&#1608;&#1602;</div>
<div class="prayer_l"><?php echo $prayer_timings['Zuhr']; ?></div>&nbsp; <div class="prayer_r"> &#1575;&#1604;&#1592;&#1607;&#1585; </div>
<div class="prayer_l"><?php echo $prayer_timings['Asr']; ?></div>&nbsp; <div class="prayer_r"> &#1575;&#1604;&#1593;&#1589;&#1585; </div>
<div class="prayer_l"><?php echo $prayer_timings['Maghrib']; ?></div>&nbsp; <div class="prayer_r"> &#1575;&#1604;&#1605;&#1594;&#1585;&#1576; </div>
<div class="prayer_l"><?php echo $prayer_timings['Isha']; ?></div>&nbsp; <div class="prayer_r"> &#1575;&#1604;&#1593;&#1588;&#1575;&#1569; </div>
<?php } ?>