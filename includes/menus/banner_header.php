<?php $main_menu = isset($main_menu) ? $main_menu : '' ?>
    	<div id="logo">
        	<img src="<?php echo $site_path; ?>images/logo.jpg" />
        </div>
        <div id="site_headline">???? ???? ????? ???? ??????</div>
        <div id="date">
        	<div class="img">&nbsp;</div>
		<?php
		date_default_timezone_set("Asia/Dubai");
            $dt=date("D, M j Y h:i a ");
            $todayDate = date("Y-m-d g:i a");// current date
            $currentTime = time($todayDate); //Change date into time
            $new_time = $currentTime+60*180;
            echo date("D, M j Y h:i a",$new_time);
            //echo $dt;
            ?>
        </div>
        <br class="clear" />
        <div class="clear menubar">
        	<div class="home"><a href="#">?????? ????????</a></div>
            <div class="contactus"><a href="#">???? ???</a></div>
            <div class="search">
            	<form action="<?php echo $site_path; ?>search.php" method="get" enctype="multipart/form-data">
                		<span class="search_btn" style="background:#000; width:20px; height:20px; float:left;"><a href="#"></a></span>
                        <input type="submit" value="" >
                        <input class="top-search-box" name="key" value="<?php echo isset($search_title)? $search_title : '?????...'; ?>" id="search_text">
                        <div class="language"><a href="<?php echo $conf->eng_site_url; ?>">English</a></div>	  <input type="submit" alt="Search" class="submitbutton" value="">
                </form>
            </div>
        </div>
        <div class="clear banner">
        	<div class="clear menu">
            
                <div id="mydroplinemenu" class="droplinebar">
                        <ul >
                          <li id="szgmc" > <a <?php echo $main_menu == 'szgmc'? 'class="current"' : ''; ?> href="<?php echo $site_path; ?>about-szgmc" > SZGMC </a> </li>
                          <li id="founder"> <a <?php echo $main_menu == 'founder'? 'class="current"' : ''; ?> href="<?php echo $site_path; ?>founder" > Founding Father </a>  </li>
                          <li id="about"> <a <?php echo $main_menu == 'about'? 'class="current"' : ''; ?> href="<?php echo $site_path; ?>theory-and-implementation"> About the Mosque </a> </li>
                          <li id="archi"> <a <?php echo $main_menu == 'archi'? 'class="current"' : ''; ?> href="<?php echo $site_path; ?>general-architecture"> Architecture </a> </li>
                          <li id="contact" > <a <?php echo $main_menu == 'contact'? 'class="current"' : ''; ?> href="https://webmail.szgmc.ae/"> Staff Only&nbsp;&nbsp;</a> </li>
                        </ul> 
               </div>	
            
           <div class="social_media">
                <div id="facebook" href="<?php echo $conf->fb_url; ?>">&nbsp;</div>
                <div id="twitter" href="<?php echo $conf->twitter_url; ?>">&nbsp;</div>
            </div>
            <div class="top">
                    <?php //if(isset($current_page) && $current_page == 'index') { 
						$w_values = getWeather();
					?>
                    	<div class="weather">
                        <img src="<?php echo $site_path; ?>images/weather/<?php echo $w_values['condition']; ?>.png" />
						<p><?php echo $w_values['temp']; ?>&nbsp;&deg;C</p></div>
                    	<div class="vision"><a href="<?php echo $site_path; ?>vision-mission-values">Vision, Mission, Goals & Values</a></div>
                    <?php //} ?>
					<?php include 'includes/menus/sub_menu_weather.php'; ?>
                </div>
            </div>
            <div class="slider"> 
                <div id="slider" class="nivoSlider">
                    <img src="<?php echo $site_path; ?>images/banners/1.png" /> 
                    <img src="<?php echo $site_path; ?>images/banners/2.png" />
                    <img src="<?php echo $site_path; ?>images/banners/3.png" /> 
                    <img src="<?php echo $site_path; ?>images/banners/4.png" /> 
                    <img src="<?php echo $site_path; ?>images/banners/5.png" /> 
                    <img src="<?php echo $site_path; ?>images/banners/6.png" />  
                </div>
            </div>
            
            
            
            
        </div>