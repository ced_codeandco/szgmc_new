<!-- Bottom Gallery Start -->
<div class="clear"></div>
<div class="partner_box">
	<ul id="mycarousel2" class="jcarousel-skin-tango">
   			<li>
                <a href="http://www.mopa.ae/ar/Pages/default.aspx" target="_blank">
                <img src="<?php echo $site_path;?>images/partner_logo/partner_01.jpg">
                موقع وزارة شؤون الرئاسة
                </a>
            </li>
  			<li>
                <a target="_blank" href="https://www.mygov.ae/mygov/Index.aspx?AspxAutoDetectCookieSupport=1">
                <img src="<?php echo $site_path;?>images/partner_logo/partner_02.jpg">
                حكومتي
                </a>
            </li>
            <li>
                <a href="http://visitabudhabi.ae/ar/default.aspx" target="_blank">
                <img src="<?php echo $site_path;?>images/partner_logo/partner_03.jpg">
                هيئة أبوظبي للسياحة والثقافة
                </a>
            </li>
            <li>
                <a href="http://www.government.ae/ar/web/guest/home" target="_blank">
                <img src="<?php echo $site_path;?>images/partner_logo/partner_04.jpg">

                حكومة الإمارات الإلكترونية
                </a>

            </li>
            <li>
                <a href="https://ar.tripadvisor.com/Attraction_Review-g294013-d1492221-Reviews-Sheikh_Zayed_Grand_Mosque_Center-Abu_Dhabi_Emirate_of_Abu_Dhabi.html" target="_blank">
                <img src="<?php echo $site_path;?>images/partner_logo/partner_05.jpg">
                TripAdvisor
                </a>
            </li>
            
            
       </ul>
</div><div class="clear"></div>
<!-- Bottom Gallery Close -->