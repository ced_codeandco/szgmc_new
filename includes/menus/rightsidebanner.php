
        <div class="content-left-box">
        <div class="content-left-box-txt-menu">
        <div class="content-left-box-txt-menu-left"><a href="<?php echo $site_path;?>visiting-the-mosque">زيارة الجامع</a></div>
        <div class="content-left-box-txt-menu-right"><a href="<?php echo $site_path;?>visiting-the-mosque"><img src="<?php echo $site_path;?>images/button.png" width="33" height="38" /></a></div>
        </div>
        <img src="<?php echo $site_path;?>images/visiting-the-mosque-image.jpg" width="210" height="198" />
        </div>
        <div class="content-left-box">
        <div class="content-left-box-txt-menu">
        <div class="content-left-box-txt-menu-left"><a href="<?php echo $site_path;?>religious-programs">البرامج الدينية</a></div>
        <div class="content-left-box-txt-menu-right"><a href="<?php echo $site_path;?>religious-programs"><img src="<?php echo $site_path;?>images/button.png" width="33" height="38" /></a></div>
        </div>
        <img src="<?php echo $site_path;?>images/religious-programs-image.jpg" width="210" height="198" /></div>
        <div class="content-left-box-round-corner prayer-rnd ">
            <?php $prayerTimes = getPrayerPublicApi();?>
        	<div class="prayer-timing-title-div">مواقيت الصلاة</div>
			<div class="prayer-timing-div"> مدينة أبوظبي </div>
            <div class="prayer-timing-div">الفجر <span><?php echo $prayerTimes['Fajr'];?></span></div>
			<div id="prayertiming" style="display: none+;">
                <div class="prayer-timing-div">الشروق <span><?php echo $prayerTimes['Shurooq'];?></span></div>
                <div class="prayer-timing-div">الظهر <span><?php echo $prayerTimes['Zuhr'];?></span></div>
                <div class="prayer-timing-div">العصر <span><?php echo $prayerTimes['Asr'];?></span></div>
                <div class="prayer-timing-div">المغرب <span><?php echo $prayerTimes['Maghrib'];?></span></div>
                <div class="prayer-timing-div">العشاء <span><?php echo $prayerTimes['Isha'];?></span></div>
			
			</div>
		<!--<span  class="prayer_seemore"><a  id="flip"  href="javascript:void(0);">كل الصلوات‏</a></span>
		<span  class="prayer_seeless"><a  id="flip1"  href="javascript:void(0);">الصلاة القادمة</a></span>-->
      </div>
        
        <div class="content-left-box tour-operators-log">
        <div class="content-left-box-txt-menu">
        <div class="content-left-box-txt-menu-left" style="font-size:11px !important"><a href="<?php echo $site_path;?>userlogin.php">تسجيل دخول الشركات السياحية</a></div>
        <div class="content-left-box-txt-menu-right"><a href="<?php echo $site_path;?>userlogin.php"><img src="<?php echo $site_path;?>images/button.png" width="33" height="38" /></a></div>
        </div>
        <img src="<?php echo $site_path;?>images/tour-operators-login-image.jpg" width="210" height="98" /></div>

        <!--<div class="content-left-box tour-operators-log">
            <div class="content-left-box-txt-menu">
                <div class="content-left-box-txt-menu-left" style="font-size:11px !important"><a href="<?php /*echo $site_path;*/?>juniorculturalguide.php">برنامج المرشد الثقافي الصغير</a></div>
                <div class="content-left-box-txt-menu-right"><a href="<?php /*echo $site_path;*/?>userlogin.php"><img src="<?php /*echo $site_path;*/?>images/button.png" width="33" height="38" /></a></div>
            </div>
            <img src="<?php /*echo $site_path;*/?>images/jcg-right-banner.jpg" width="210" height="98" /></div>-->
	
        <div class="content-left-box">
        <div class="content-left-box-txt-menu">
        <div class="content-left-box-txt-menu-left"><a href="https://itunes.apple.com/us/app/sheikh-zayed-grand-mosque/id715431100?mt=8" target="_blank">حمل تطبيقنا</a></div>
        <div class="content-left-box-txt-menu-right"><a href="https://itunes.apple.com/us/app/sheikh-zayed-grand-mosque/id715431100?mt=8" target="_blank"><img src="<?php echo $site_path;?>images/button.png" width="33" height="38" /></a></div>
        </div>
        <img src="<?php echo $site_path;?>images/zayed-mosque-app-image.jpg" width="210" height="98" /></div>
         <div class="poll_box_left">
         	<span class="poll_title">استطلاع الرأي <img src="<?php echo $site_path;?>images/poll_right_bg.png"></span>
            <p>ما رأيك في استخدام جهاز المرشد الالكتروني؟</p>
           
           <div class="chk_box">
           		<table>
                  <tr>
                  
                  <td><input type="radio" name="radiog_lite" id="radio1" class="css-checkbox" />
                  <label for="radio1" class="css-label">عملي ومفيد</label></td>
                  </tr>
                 <tr>
                 	<td>
                    	<div class="percentdage_box">
                        	<div class="box_01">
                            	<span class="gold_box1"></span>
                            </div>
                            <span class="box_right_02"><strong>(91%)</strong></span>
                        </div>
                    </td>
                 </tr>
                 
                   <tr>
                  
                  <td><input type="radio" name="radiog_lite" id="radio2" class="css-checkbox" />
                  <label for="radio2" class="css-label">غير عملي</label></td>
                  </tr>
                 <tr>
                 	<td>
                    	<div class="percentdage_box">
                        	<div class="box_01">
                            	<span class="gold_box2"></span>
                            </div>
                            <span class="box_right_02"><strong>(1%)</strong></span>
                        </div>
                    </td>
                 </tr>
                 
                 <td><input type="radio" name="radiog_lite" id="radio3" class="css-checkbox" />
                  <label for="radio3" class="css-label">يحتاج الى تطوير</label></td>
                  </tr>
                 <tr>
                 	<td>
                    	<div class="percentdage_box">
                        	<div class="box_01">
                            	<span class="gold_box3"></span>
                            </div>
                            <span class="box_right_02"><strong>(8%)</strong></span>
                        </div>
                    </td>
                 </tr>
                 
                 </table>
           </div>
           
           <div class="left_vote_box">
           	<p>111 عدد الأصوات</p>
            <a href="<?php echo $site_path;?>" class="vote_btn1">تصويت</a>
           </div>
           
           <span class="previous_poll"><a href="<?php echo $site_path;?>poll-archive">استطلاع الرأي السابق</a></span> 
            
         </div>
