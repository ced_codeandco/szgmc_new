<?php //echo '<pre>'.print_r($_SERVER).'</pre>'; ?><script language="javascript" type="text/javascript">
var min=12;
var max=20;
var fontSize = 12;
function increaseFontSize() {
 
    var p = document.getElementsByTagName('p');
    var li= document.getElementsByTagName('li');
    var div= document.getElementsByTagName('div');
    var a= document.getElementsByTagName('a');
    magnifieFont(p);
    magnifieFont(li);
    magnifieFont(div);
    magnifieFont(a);
}
function magnifieFont(p){
    for(i=0;i<p.length;i++) {

        if(p[i].style.fontSize) {
            var s = parseInt(p[i].style.fontSize.replace("px",""));
        } else {

            var s = 12;
        }
        if(s!=max) {

            s += 1;
        }
        p[i].style.fontSize = s+"px"
    }
}
function decreaseFontSize() {
    var p = document.getElementsByTagName('p');
    var li= document.getElementsByTagName('li');
    var div= document.getElementsByTagName('div');
    var a= document.getElementsByTagName('a');
    deMagnifieFont(p);
    deMagnifieFont(li);
    deMagnifieFont(div);
    deMagnifieFont(a);
}
function deMagnifieFont(p)
{
    for(i=0;i<p.length;i++) {

        if(p[i].style.fontSize) {
            var s = parseInt(p[i].style.fontSize.replace("px",""));
        } else {

            var s = 12;
        }
        if(s!=min) {

            s -= 1;
        }
        p[i].style.fontSize = s+"px"

    }
}
function submitform(){
    document.formID.submit();
}
$(document).ready(function(){
	$('.search_link').click(function(){
		$('.search_form_top').toggleClass('active_search');
	});
});
</script>

<?
error_reporting(0);
ob_start();
$eurl = explode('/',$_SERVER['REQUEST_URI']);


if(($eurl[2]=="publications") && end($eurl)!== "publications")
{
$eurl = "publications/".end($eurl);
}

elseif(end($eurl)== "achievement")
{
$eurl = "theory-and-implementation";
}
elseif(end($eurl)== "grand-mosque-message")
{
$eurl = "message-of-the-mosque";
}
else if(($eurl[1]=="news-detail") )
{
$eurl = "news-list";
}
else if(($eurl[1]=="news-list") && ($eurl[2]!="") )
{
$eurl = "news-list/".end($eurl);
}
else if(($eurl[1]=="news-list") )
{
$eurl = "news-list";
}
else if(($eurl[1]=="2") )
{
header("location:/news-list");
}
else if(($eurl[1]=="activities-detail") )
{
$eurl = "center-activities";
}
else if(($eurl[1]=="video-gallery") )
{
$eurl = "video-gallery";
}
else if(($eurl[1]=="poll-archive") && ($eurl[2]!="") )
{
$eurl = "poll-archive/".end($eurl);
}
else if(($eurl[1]=="poll-archive")  )
{
$eurl = "poll-archive/";
}
else 
{
$eurl = end($eurl);
}

?>
<style>
@media screen and (-webkit-min-device-pixel-ratio:0) {    
	
#header div.menubar div.search { margin-top:1px;} 

}

</style>
<?php
					function getMonth_hdr($month) {
						$list = array(
								'Jan' => 'يناير',
								'Feb' => 'فبراير',
								'Mar' => 'مارس',
								'Apr' => 'ابريل',
								'May' => 'مايو',
								'Jun' => 'يونيو',
								'Jul' => 'يوليو',
								'Aug' => 'أغسطس',
								'Sep' => 'سبتمبر',
								'Oct' => 'أكتوبر',
								'Nov' => 'نوفمبر',
								'Dec' => 'ديسمبر'
									);
						
						if(in_array($month, array_keys($list))) {
							//die($list[$slug]);
							return $list[$month];
						}
						else
							return '';
					}
					function get_freq($freq) {
						$freq = explode(" ", $freq);
						//echo $freq[1];
						$fre=$freq[1];
						$list = array(
								'am' => 'ص',
								'pm' => 'م'
									);
						
						if(in_array($fre, array_keys($list))) {
							//die($list[$slug]);
							return $list[$fre];
						}
						else
							return '';
					}
					function getDay($month) {
						$list = array(
								'Sun' => 'الأحد',
								'Mon' => 'الاثنين',
								'Tue' => 'الثلاثاء',
								'Wed' => 'الأربعاء',
								'Thu' => 'الخميس',
								'Fri' => 'الجمعة',
								'Sat' => 'السبت'
									);
						
						if(in_array($month, array_keys($list))) {
							//die($list[$slug]);
							return $list[$month];
						}
						else
							return '';
					}
 date_default_timezone_set("Asia/Dubai");
            $dt=date("D, M j Y h:i a ");
            $todayDate = date("Y-m-d g:i a");// current date
            $currentTime = time($todayDate); //Change date into time
            $new_time = $currentTime;
			
			$updated = explode(" ", $todayDate);
			$utime = $updated[1];
			$date = date('M, d, Y', strtotime($updated[0]));
			$date = explode(",", $date);
			$month = $date[0];
			$month= getMonth_hdr($month);
			$udate=$date[1]."&nbsp;".$month."&nbsp;".$date[2];
			
			$freq=get_freq(date("h:i a",$new_time));
			$day=getDay(date("D",$new_time));
			
          
            ?>
	<!-- Header Start -->
<header>
	<h1><a href="<?php echo $site_path;?>"><img src="<?php echo $site_path;?>images/logo.png"></a></h1>
    	<div class="grid_1">
        <span class="title">
مركز جامع الشيخ زايد الكبير</span>
<?php include 'includes/menus/hijri.php'; ?>
            <ul>
            <li><a href="#"><?php echo $day.' ,'.$udate;?></a></li>
            <li><a href="#"><?php  echo ($hijri[1]).' '.HijriCalendar::monthName($hijri[0]).' '.$hijri[2]; ?></a></li>
            <li><a href="#" class="last"><?php echo date("h:i",$new_time).' '.$freq;?>  </a></li>
           </ul>
       </div>
    <div class="grid_2">
    <img src="<?php echo $site_path;?>images/Top_Right_Logo.jpg">
    </div>
    <span class="pull-left mobile_view" id="menuclick"><div id="nav-icon3"> <span></span> <span></span> <span></span> <span></span> </div></span> 	
</header>
<!-- Header Start Close -->
<!-- Navigation Strart -->
<div class="social_box">
	<div class="sub_social_box"><?php $actual_link = "$_SERVER[REQUEST_URI]"; ?>
    	<ul>
        	<li><a href="index.htm" class="home <?php if($eurl==''){ ?> active <?php } ?>" title="Home"></a></li>
            <li><a href="<?php echo $site_path; ?>contact-us" title="Contact us" class="phone <?php if($eurl=='contact-us'){ ?> active <?php } ?>"></a></li>
            <li><a href="<?php echo $site_path; ?>sitemap" title="Sitemap" class="social_media <?php if($eurl=='sitemap'){ ?> active <?php } ?>"></a></li>
             <li><a href="<?php echo $site_path; ?>help" title="مساعدة" class="qustion"></a></li>
            <li><a  href="javascript:decreaseFontSize();" title="Decrease Font" class="small_a"></a></li>
            <li><a href="javascript:increaseFontSize();" title="Increase Font" class="cap_a"></a></li>
            
        </ul>
        
        <ul class="Right_Side1">
        	<li><a href="<?php echo rtrim($site_path, '/en/').'/en/'.str_replace("/", "", $actual_link); ?>"><img src="<?php echo $site_path;?>images/english.png" title="Switch Language" class="arbi_title"></a></li>
            <li class="reesMr"><a class="rss" href="http://feeds.feedburner.com/Szgmc" title="Rss" target="_blank"></a></li>
            <li><a class="youtube" href="http://www.youtube.com/szgmc" title="Youtube Videos" target="_blank"></a></li>
            <!--li><a href="#" class="facebook"></a></li-->
            <li class="mobile_view"><img src="<?php echo $site_path; ?>img/mobile/search.png" class="search_link" /></li>
			<form method="post" action="/search.php" name="formID" class="search_form_top" id="formID">
              <span class="search_btn" onclick="javascript:submitform();"><a href="#"></a></span>
             <input type="text" class="validate[required]" id="search_text" onblur="if(this.value=='') { this.value='البحث...'}" onfocus="if(this.value=='البحث...') { this.value=''; }" value="البحث..." name="key">
         </form>
        </ul>
        
    </div>
	
</div>
<!-- Navigation Close -->