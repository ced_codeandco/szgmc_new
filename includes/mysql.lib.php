<?php
//Developer vyshakh
//last modified 12/12/2008
function escape($str){
return mysql_real_escape_string($str);
}
function html($str){
//return htmlentities($str);
//$pattern = "ÂÂ¬```";
//preg_replace('//', $str);
//$str=str_replace("'","'",$str)
//$str=str_replace("","",$str)
$str=Helper::UTF8ToHTML($str);
return $str;
//return htmlspecialchars($str, ENT_COMPAT, 'UTF-8');
}


class connect{
   
    var $Host = DB_HOST;			// Hostname of our MySQL server
	var $Database = DB_NAME;		// Logical database name on that server
	var $User = DB_USER;			// Database user
	var $Password = DB_PASS;		// Database user's password
	var $Link_ID = 0;				// Result of mysql_connect()
	var $Query_ID = 0;				// Result of most recent mysql_query()
	var $Record	= array();			// Current mysql_fetch_array()-result
	var $Row;					// Current row number
	var $Errno = 0;					// Error state of query
	var $Error = "";
        
        
        
        
      public function logs($msg)
      {
		if(ERROR_LOG!='true')
		{
		$ip = mysql_real_escape_string($_SERVER['REMOTE_ADDR']);
		$url=$_SERVER["SCRIPT_NAME"];
		preg_match("/[^\/]+$/",$url,$matches);
		$current_file_name= $matches[0];
		$msg=mysql_real_escape_string($msg);				
		$sql="INSERT into error_logs(ip,file_name,error,log_time)values('$ip','$current_file_name','$msg',CURRENT_TIMESTAMP)";
		$this->connect();
		mysql_query($sql,$this->Link_ID);
		}
		

      }
        
    function connect()
	{
		if($this->Link_ID == 0)
		{
			$this->Link_ID = mysql_connect($this->Host, $this->User, $this->Password);
			if (!$this->Link_ID)
			{
				$this->logs("Link_ID == false, connect failed\n");
			}
			$SelectResult = mysql_select_db($this->Database, $this->Link_ID);
			if(!$SelectResult)
			{
				$this->Errno = mysql_errno($this->Link_ID);
				$this->Error = mysql_error($this->Link_ID);
				$this->logs("cannot select database ".$this->Database."\n");
			}
		}
	}
   
     function query($Query_String)
	{
		$this->connect();
		mysql_query("SET NAMES utf8");
		$this->Query_ID = mysql_query($Query_String,$this->Link_ID);
		$this->Row = 0;
		$this->Errno = mysql_errno();
		$this->Error = mysql_error();

		if (!$this->Query_ID){
                    $this->logs("Invalid SQL: ".$Query_String."\n"."SQL Error no : ".$this->Errno."\n"."SQL Error: ".$this->Error."\n");
                }
              if($this->Errno>0){
                $this->logs("Invalid SQL: ".$Query_String."\n"."SQL Error no : ".$this->Errno."\n"."SQL Error: ".$this->Error."\n");
                }
		return $this->Query_ID;
	}
        
	
	
        
        function query_fetch($fetch=0)
        {
            if($fetch==0) {
        		$result=@mysql_fetch_assoc($this->Query_ID);
            } else {
		$result=@mysql_fetch_array($this->Query_ID);
            }
        	if(!is_array($result)) 
        	return false;
        	$this->total_field=mysql_num_fields($this->Query_ID);
        	foreach($result as $key=>$val){
		$result[$key]=trim(htmlspecialchars($val));
        	}
	 return $result; 
        }
        
       
        function num_rows()
	{
		return mysql_num_rows($this->Query_ID);
	}
        

     function loadResult() {
        $array = array();
        while ($row = @mysql_fetch_object($this->Query_ID)) {	
            $array[] = $row;
        }
        @mysql_free_result( $this->Query_ID);
        return $array;
    }

        
    function optimize($tbl_name)
	{
		$this->connect();
		$this->Query_ID = @mysql_query("OPTIMIZE TABLE $tbl_name",$this->Link_ID);
	}

	function clean_results()
	{
		if($this->Query_ID != 0) mysql_free_result($this->Query_ID);
	}

	function close()
	{
		if($this->Link_ID != 0) mysql_close($this->Link_ID);
	}
    
    
      
            
      function get_token($length)
       {
       $random= "";
       srand((double)microtime()*1000000);
       $char_list = "abcdefghijklmnopqrstuvwxyz";
	   //$char_list. = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";       
       $char_list .= "123456789";
       // Add the special characters to $char_list if needed
       for($i = 0; $i < $length; $i++)
       {
         $random .= substr($char_list,(rand()%(strlen($char_list))), 1);
       }
       return $random;
       }

}




       function string_to_filename($word) {
       $tmp = preg_replace('/^\W+|\W+$/', '', $word); // remove all non-alphanumeric chars at begin & end of string
	   $tmp = preg_replace('/[^a-zA-Z0-9-]/', ' ', $tmp);	  
       $tmp = preg_replace('/\s+/', '_', $tmp); // compress internal whitespace and replace with _	   
       return strtolower($tmp);
       }




class Helper
{
    function strSplit($text, $split = 1)
    {
        if (!is_string($text)) return false;
        if (!is_numeric($split) && $split < 1) return false;

        $len = strlen($text);

        $array = array();

        $i = 0;

        while ($i < $len)
        {
            $key = NULL;

            for ($j = 0; $j < $split; $j += 1)
            {
                $key .= $text{$i};

                $i += 1;
            }

            $array[] = $key;
        }

        return $array;
    }

    function UTF8ToHTML($str)
    {
        $search = array();
        $search[] = "/([\\xC0-\\xF7]{1,1}[\\x80-\\xBF]+)/e";
        $search[] = "/&#228;/";
        $search[] = "/&#246;/";
        $search[] = "/&#252;/";
        $search[] = "/&#196;/";
        $search[] = "/&#214;/";
        $search[] = "/&#220;/";
        $search[] = "/&#223;/";

        $replace = array();
        $replace[] = 'Helper::_UTF8ToHTML("\\1")';
        $replace[] = "ä";
        $replace[] = "ö";
        $replace[] = "ü";
        $replace[] = "Ä";
        $replace[] = "Ö";
        $replace[] = "ü";
        $replace[] = "ß";

        $str = preg_replace($search, $replace, $str);

        return $str;
    }

    function _UTF8ToHTML($str)
    {
        $ret = 0;

        foreach((Helper::strSplit(strrev(chr((ord($str{0}) % 252 % 248 % 240 % 224 % 192) + 128).substr($str, 1)))) as $k => $v)
            $ret += (ord($v) % 128) * pow(64, $k);
        return "&#".$ret.";";
    }
	
	
	
}


	
?>