<?php
class Search 
{
	function searchAll($search_query = '', $limit = 3, $offset = 0) {
		$search_list = array();
		
		//searching for news
		$sub_q = " where live='Y' ";
		if($search_query != '')
			$sub_q .= sprintf(" and ( news_title like '%%%s%%' or news_text like '%%%s%%' ) ", $search_query, $search_query);	
		$sql = sprintf("select news_id, news_title, news_text from news %s order by news_date desc limit $offset, $limit ", $sub_q);
		if($all_con = mysql_query($sql)) {
			while($con = mysql_fetch_object($all_con)) {
				//print_r($news);
				$search_list[] = array(
					'id'	=> $con->news_id,
					'title' => $con->news_title,
					'content' => $con->news_text,
					'type' => 'news'
					);
			}
		}
		
		//searching for pages
		$sub_q = " where live='Y' ";
		if($search_query != '')
			$sub_q .= sprintf(" and (meta_title like '%%%s%%' or content like '%%%s%%') ", $search_query, $search_query);	
		$sql = sprintf("select meta_title, url, content from pages %s order by meta_title limit $offset, $limit ", $sub_q);
		if($all_con = mysql_query($sql)) {
			while($con = mysql_fetch_object($all_con)) {
				//print_r($news);
				$search_list[] = array(
					'title' => $con->meta_title,
					'content' => $con->content,
					'url' => $con->url,
					'type' => 'pages'
					);
			}
		}
		
		
		return $search_list;
	}
	
	
	function countSearchRecords($search_query) {
		$sql = "select count(news_id) as total_news from news where live = 'Y'";
		if($all_news = mysql_query($sql)) {
			if($total = mysql_fetch_object($all_news)) {
				return $total->total_news;
			}
		}
		return false;
	}	
}
?>