<?php
function getfMonth($month) {
		$list = array(
				'Jan' => 'يناير',
				'Feb' => 'فبراير',
				'Mar' => 'مارس',
				'Apr' => 'ابريل',
				'May' => 'مايو',
				'Jun' => 'يونيو',
				'Jul' => 'يوليو',
				'Aug' => 'أغسطس',
				'Sep' => 'سبتمبر',
				'Oct' => 'أكتوبر',
				'Nov' => 'نوفمبر',
				'Dec' => 'ديسمبر'
					);
		
		if(in_array($month, array_keys($list))) {
			//die($list[$slug]);
			return $list[$month];
		}
		else
			return '';
	}
if(isset($_SESSION["user"]))
{
$user=$_SESSION["user"];
}

$dir = new SplFileInfo(getcwd());

$stat=  date('d-m-Y H:i:s', $dir->getMTime());
?>
<?php
/*$link = mysql_connect('localhost', 'root', 'P@ssw0rd');
	$db_selected = mysql_select_db('szgmc-new-arb', $link);*/
$_SERVER['DOCUMENT_ROOT'].'/includes/db.php';
$sql_stat = 'select * from site_options where option_name="last_updated"';
$query_stat=mysql_query($sql_stat);
while($res_stat=mysql_fetch_object($query_stat))
{
	 $db_stat = $res_stat->option_value;
}
mysql_close($link);
if($db_stat>$stat)
{
	$stat = $db_stat;
}
else
{
	//$stat = $stat;
	$stat = $db_stat;
}

$updated = explode(" ", $stat);
$utime = $updated[1];
$date = date('M, d, Y', strtotime($updated[0]));
$date = explode(",", $date);
$month = $date[0];
$month= getfMonth($month);
$udate=$date[1]."&nbsp;".$month."&nbsp;".$date[2];
?> 
<!-- footer Start -->
<div class="footer">
	<span class="pull-left mobile_view" id="menuclickfooter"><div id="nav-icon3"> <span></span> <span></span> <span></span> <span></span> </div></span>
	<div class="footer-address">
    <div class="footer-div">
    	<div class="footer-address-left"><img src="<?php echo $site_path;?>images/footer-mosque-image.png" width="174" height="159" /></div>
        <div class="footer-address-middle">
        	<ul>
            <li class="telephone-background">رقم الهاتف :<span class="phone-arb">4191919 2 971+</span></li>
            <li class="writing-background"> شارك برأيك خلال البريد الإلكتروني <a class="bold" href="mailto:info@szgmc.ae">info@szgmc.ae</a>  <br /> <a href="<?php echo $site_path; ?>contact-us" class="underline">أو اتصل بنا</a></li>
            </ul>
            
        </div>
        <div class="footer-address-right">
        <p> آخر تحديث للموقع تم بتاريخ: <?php echo $udate; ?> في <?php echo $utime; ?></p>
        <p>هذا الموقع يمكن تصفحه بالشكل المناسب من خلال شاشة  768x 1024 <br/> تدعم مايكروسوفت إنترنت إكسبلورر + 7.0 ، فايرفوكس + 1.0 ، سفاري + 1.2 ، كروم</p>
        </div>
    </div>
    </div>
    <div class="footer-menu">
    <div class="footer-div menu"><a href="<?php echo $site_path; ?>accessibility"> إمكانية الوصول</a><span>|</span><a href="<?php echo $site_path; ?>privacy-policy">سياسة الخصوصية</a><span>|</span><a href="<?php echo $site_path; ?>info"> الشروط والأحكام</a><span>|</span><a href="<?php echo $site_path; ?>disclaimer">إخلاء المسؤولية</a><span>|</span><a href="<?php echo $site_path; ?>copyrights">حقوق الملكية</a><span>|</span><a href="<?php echo $site_path; ?>sitemap">خريطة الموقع</a><span>|</span><a href="<?php echo $site_path; ?>help"> للمساعدة</a><span>|</span><a href="https://webmail.szgmc.ae/" target="_blank"> للموظفين فقط</a><span>|</span><a href="<?php echo $site_path; ?>contact-us">اتصل بنا</a></div>
    </div>
    <div class="footer-copyright">
    <div class="footer-div copyright copyright_nomobile">حقوق النسخ © 2015 مركز جامع الشيخ زايد الكبير, جميع الحقوق محفوظة</div>
    <div class="footer-div copyright copyright_mobile">حقوق النسخ © 2015 مركز جامع الشيخ زايد الكبير,<br /> جميع الحقوق محفوظة</div>
    </div>
</div>
<!-- Footer End -->


 <!--<script type="text/javascript">
    $(window).load(function() {
        $('#slider').nivoSlider();
    });
	
	
	$("#side_menu > li > div").click(function(){
     
    if(false == $(this).next().is(':visible')) {
        $('#side_menu ul').slideUp(300);
    }
    $(this).next().slideToggle(300);
});
    </script>-->
    <?php
$db->closeDb();
?>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-22417140-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>