<div class="footer_bg">
<div class="footer">

<div class="footer_menu">
<ul>
<li class="no_divider"><a href="#" class="current"><span>Home</span></a></li>

<li><a href="<?php echo $site_path; ?>about-szgmc"><span>SZGMC</span></a></li>
<li><a href="<?php echo $site_path; ?>founder"><span>Founding Father</span></a></li>
<li><a href="<?php echo $site_path; ?>theory-and-implementation"><span>About the Mosque</span></a></li>
<li><a href="<?php echo $site_path; ?>general-architecture"><span>Architecture</span></a></li>
<li ><a href="<?php echo $site_path; ?>contact-us"><span>Contact Us</span></a></li>
<li><a><span class="ptop"><img src="<?php echo $site_path; ?>images/icons.jpg" align="absmiddle"></span></a></li>
<li><a><span><img src="<?php echo $site_path; ?>images/counter.jpg"></span></a></li>

</ul>

</div> 

<div id="copyright">
 
<span>&copy; 2012 by Sheikh Zayed Grand Mosque Centre. All rights reserved.</span></div> 

</div>
</div> 

 <script type="text/javascript">
    $(window).load(function() {
        $('#slider').nivoSlider();
    });
	
	
	$("#side_menu > li > div").click(function(){
     
		if(false == $(this).next().is(':visible')) {
			$('#side_menu ul').slideUp(300);
		}
		$(this).next().slideToggle(300);
	});
    </script>
    <?php
$db->closeDb();
?>