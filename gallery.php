<?php

ini_set('display_errors', 1);
error_reporting(1);

include 'includes/database.php';
include 'includes/functions.php';
include 'includes/config.php';
include 'includes/flickr.php';




$slug = explode('/',$_SERVER['REQUEST_URI']);
$slug = end($slug);
$conf = new Configuration();
$db = new MyDatabase();

$site_path = $conf->site_url;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>البوم الصور - مركز جامع الشيخ زايد الكبير</title>
<!-- <link rel="stylesheet" type="text/css" href="<?php echo $site_path; ?>css/photo_gallery/jquery.ad-gallery.css">-->
	<?php include 'includes/common_header.php'; ?>
  <!--<script type="text/javascript" src="<?php echo $site_path; ?>js/photo_gallery_lib/jquery.ad-gallery.js"></script>-->
 
    
</head>

<body>
<?php include 'includes/menus/banner_header3.php'; ?>		
			<!-- Banner Start -->
<div class="banner">
	<img src="<?php echo $site_path;?>images/visiting_the_mosque_banner.jpg">     
</div>
<!-- Banner Close -->	
<!-- Content Start -->
	<div class="main_box_content">
	 	 <?php include 'includes/menus/left_menu.php'; ?>
        <div class="clear"></div>
        <div class="content">
        	<div class="brad_cram">
            	<ul>
            	   <li><a href="<?php echo $site_path; ?>">الصفحة الرئيسية</a></li>
                    <li><a href="#" class="active">البوم الصور</a></li>
                </ul>
            </div>
      
	  		<div class="content-right" style="margin-right:30px">
			<div class="single_middle" >
		
                <h2> البوم الصور </h2>
				<br class="clear" />            
                
		 
           <div class="gallery" id="gallery" style="margin-right: -14px;">		    
		      <?php
			
			$flickr = new flickr('82986622@N03','7ed59ac53cd1f823d6690008f08848d5');
			$images = $flickr->getImages(100);
			if($images === false) {
				echo 'Flickr Feed Unavailable';
			}
			else {
			$count = 0;
			foreach($images->photos->photo as $photo) {
				$count++;
			}
			
			$totrow = ceil($count / 4);
			?>
            <div class="gallery-div">
				<?php 
				$countInner = 0;
				$countMail = 0;
				foreach($images->photos->photo as $photo) { 
				$countInner++;
				
				//echo 'http://farm' . $photo->attributes()->farm . '.static.flickr.com/' . $photo->attributes()->server . '/' . $photo->attributes()->id . '_' . $photo->attributes()->secret . '_z.jpg';
				?>
					  <a href="<?php echo 'http://farm' . $photo->attributes()->farm . '.static.flickr.com/' . $photo->attributes()->server . '/' . $photo->attributes()->id . '_' . $photo->attributes()->secret . '_z.jpg'; ?>" class="lightbox_popup"><img src="<?php echo 'http://farm' . $photo->attributes()->farm . '.static.flickr.com/' . $photo->attributes()->server . '/' . $photo->attributes()->id . '_' . $photo->attributes()->secret . '_m.jpg'; ?>" title="<?php echo $photo->attributes()->title; ?>"></a>              
                <?php 
					if($countInner == $totrow){
						$countInner = 0;
						$countMail++;
						if($countMail == 3){
							$endDiv = ' gallery-div-end';
						}else{
							$endDiv = '';
						}
						echo '</div><div class="gallery-div'.$endDiv.'">';				
					}
				}?>
            </div>
            <?php } ?>
	       
	       </div>
                
                
                
                
                
     </div>
			</div>
 <br class="clear" />      
        </div>
    
    </div>

<div class="content_bottom">&nbsp;</div>

<?php require 'includes/footer.php'; ?>
</body>
</html>
