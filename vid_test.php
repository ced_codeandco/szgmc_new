<?php

ini_set('display_errors', 1);
error_reporting(1);

/*phpinfo();

die();*/


include 'includes/database.php';
include 'includes/functions.php';
include 'includes/config.php';
include 'includes/youtube.php';
$yt = new YoutubeAPI();

$slug = explode('/',$_SERVER['REQUEST_URI']);
$slug = end($slug);

$userName = 'szgmc';
$youtube = new Zend_Gdata_YouTube();
$youtube->setMajorProtocolVersion(2);
$videoFeed = $youtube->getuserUploads($userName);

if($slug != 'video-gallery') {
	foreach ($videoFeed as $videoEntry) {
		if($slug == $videoEntry->getVideoId()) {
			$video = $videoEntry;
			break;
		}
	}	
}
else {
	$video = $videoFeed[0];
}


$conf = new Configuration();
$db = new MyDatabase();

$site_path = $conf->site_url;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $video->getVideoTitle(); ?> :: Video Gallery :: Sheikh Zayed Grand Mosque</title>
<?php include 'includes/common_header.php'; ?>
    <style type="text/css">
		.video_item {
			float:right; 
			width: 151px; 
			margin: 7px 3px 7px 7px;  
		}
		.video_item img{width:145px;}
		.video_item p {
			color: #BC8545;
		}
		.video_top{background: url(../images/video_frame_top.jpg) no-repeat; width:697px; height:23px;}
		.video_bottom{background: url(/images/video_frame_bottom.jpg) left bottom no-repeat; width:697px; height:23px;}
		#list_videos {
			width: 622px;
			padding: 15px 38px 7px;
			background: url(/images/video_frame_centre.jpg) repeat-y;
		}
		#carousel{
			position:relative;
			margin: 15px 0 0;
			width: 622px;
		}
		#carousel .mask {
			direction: ltr;
			width:622px;
			position:relative;
		    overflow:hidden
		}
		.carousel ul {
		    position:absolute;
			left:0;
		    overflow:hidden;
		    margin:0;
		    padding:0;
		    list-style:none;
		}
		.carousel ul li {
			float: left;
			width: 118px;
			height: 128px;
			color: #fff;
			font-size: 8em;
			text-align: center;
			margin: 0 8px 8px 0;
		}
		.carousel>a{
			position: absolute;
			top: 40%;
			z-index:99999;
			width: 22px;
			height: 21px;
			cursor: pointer;
			text-indent:-9999px
		}
		.carousel>a.disabled{
			-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=40)";
			filter: alpha(opacity=40);
			-moz-opacity:0.4;
			-khtml-opacity: 0.4;
			opacity: 0.4;
		}
		.carousel .prev{
			left: -11px;
			background: transparent url(/css/photo_gallery/ad_scroll_back.png) 0 0;
		}
		.carousel .next{
			right: -11px;
			background: transparent url(/css/photo_gallery/ad_scroll_forward.png) 0 0;
		}
		.carousel li a{
			display: block;
			height: 100%;
			text-decoration: none;
		}
		.carousel li a>span{
			display: block;
			margin-top: 4px;
			color: #BC8545;
			font-size: 11px;
		}
	</style>
<script type="text/javascript" src="/js/jquery.carousel.js"></script>
</head>

<body>
<?php include 'includes/menus/banner_header3.php'; ?>		
			<!-- Banner Start -->
<div class="banner">
	<img src="<?php echo $site_path;?>images/visiting_the_mosque_banner.jpg">     
</div>
<!-- Banner Close -->	
<!-- Content Start -->
	<div class="main_box_content visiting_page_height">
	 	 <?php include 'includes/menus/left_menu.php'; ?>
        <div class="clear"></div>
        <div class="content">
        	<div class="brad_cram">
            	<ul>
            	   <li><a href="<?php echo $site_path; ?>">الصفحة الرئيسية</a></li>
                    <li><a href="#" class="active">مكتبة الفيديو</a></li>
                </ul>
            </div>
      
      		<div class="content-left">
			 <?php //include 'includes/menus/left_menu1.php'; 
				?>
				
                <br class="clear"/>
                <?php 
				include 'includes/menus/ministry_logos.php'; 
				?>
			</div>
	  		<div class="content-right" style="margin-right:30px">
			<div class="single_middle" style="text-align:center">
		<?php 
        include 'includes/menus/marquee.php'; 
        ?>
			<br class="clear" />
			
			<h2> مكتبة الفيديو </h2>
			<div class="video_top">&nbsp;</div>
			<div id="list_videos">
				<div id="single_video" style="margin:0 auto;">
					<div class="gallery_video">
						<iframe width="100%" height="400" src="http://www.youtube.com/embed/<?php echo $video->getVideoId(); ?>" frameborder="0" allowfullscreen></iframe>
						<div id="carousel" class="carousel module">
							<ul>
								<?php
								foreach ($videoFeed as $videoEntry) {
								$videoThumbnails = $videoEntry->getVideoThumbnails();
								?>
								<li><a href="<?php echo $site_path; ?>video-gallery/<?php echo $videoEntry->getVideoId(); ?>" title="<?php echo $videoEntry->getVideoTitle(); ?>"><img src="<?php echo $videoThumbnails[0]['url']; ?>" width="100%" height="75" alt="<?php echo $videoEntry->getVideoTitle(); ?>"><span><?php echo $videoEntry->getVideoTitle(); ?></span></a></li>
								<?php } ?>
							</ul>
							<div class="clear"></div>
						</div>
					</div>
				</div>				
			</div>
			<div class="video_bottom">&nbsp;</div>
			<br class="clear" />
               
		</div>
			</div>
 <br class="clear" />      
        </div>
    
    </div>

<div class="content_bottom">&nbsp;</div>
<script type="text/javascript">
$(document).ready(function(){
	$('#carousel').carousel({
		pagination: false,
		itemsPerPage: 5,
		itemsPerTransition: 5,
		easing: 'swing',
		noOfRows: 3
	});
});
</script>
<?php require 'includes/footer.php'; ?>
</body>
</html>
