<?php
$mydb=new connect;
$p_id='';
$p_obj=get_page_by_page_id(CURRENT_URL);
//var_dump($p_obj);
if(isset($_SESSION['admin_logged']) && $_SESSION['admin_logged']!='')
$admin_common_sql="m.visibility='A' || m.visibility='B' ";
else
$admin_common_sql="m.visibility='P' || m.visibility='B' ";

$sql="select m.*,p.url,p.p_id from header_top_nav m LEFT JOIN pages p ON m.p_id=p.p_id  where ($admin_common_sql) and m.live='Y'  GROUP BY m.hm_id ORDER BY m.pos ASC";
$rs=$mydb->query($sql);
$top_menu_obj=$mydb->loadResult();

$sql="select m.*,p.url,p.p_id from sub_menu_step1 m LEFT JOIN pages p ON m.p_id=p.p_id  where ($admin_common_sql) and m.live='Y'  GROUP BY m.sm1_id ORDER BY m.pos";
$rs=$mydb->query($sql);
$sub_menu_obj_1=$mydb->loadResult();

$sql="select m.*,p.url from sub_menu_step2 m LEFT JOIN pages p ON m.p_id=p.p_id where ($admin_common_sql) and m.live='Y' GROUP BY m.sm2_id ORDER BY m.pos";
$rs=$mydb->query($sql);
$sub_menu_obj_2=$mydb->loadResult();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo htmlspecialchars(TITLE); ?></title>
<meta name="description" content="<?php echo DESC; ?>"/>
<meta name="keywords" content="<?php echo KEYWORD; ?>"/>
<meta name="copyright" content="Grand Mosque"/>
<meta name="author" content="Grand Mosque" />
<meta name="copyright" content="2010 Grand Mosque" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link href="/en/css/styles.css" rel="stylesheet" type="text/css" />
<link href="/en/css/calendar.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/en/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="/en/js/ajaxupload.3.5.js"></script>
<script type="text/javascript" src="/en/js/jquery.validate.js"></script>
<script type="text/javascript" src="/en/js/cal.js"></script>
<script type="text/javascript" src="/en/js/main.js"></script>
<script type="text/javascript" src="/en/js/AC_RunActiveContent.js"></script>
<script type="text/javascript" src="/en/js/swfobject.js"></script>
</head>
<body>

<div class="container">
<div class="header_top">

<div style="float:right">
<div class="date_wheather">
<div class="date_time_now">
<?php
$dt=date("D, M j Y h:i a ");
$todayDate = date("Y-m-d g:i a");// current date
$currentTime = time($todayDate); //Change date into time
$new_time = $currentTime+60*180;
echo date("D, M j Y h:i a",$new_time);
//echo $dt;
?>
</div>
<div class="clear"></div>
<div class="wheather">
<div class="wheather_inner"><?php echo weather();?></div>
</div>
</div>
<div style="padding:4px;float: right; font-weight: bold;"><a href="/index" >عربي</a></div>
<div class="clear"></div>
<div style="float:left; padding-top:15px;" dir="ltr">
<h1>
<a href="/en"><img src="/en/img/grand_mosque.jpg" alt="Sheikh Zayed Grand Mosque Center"/></a>
<!--
مسجد الشيخ زايد الكبير -->
</h1>
</div>
</div>

<div class="grand_mosque_logo">
<a href="/en"><img src="/en/img/logo.jpg" alt="Grand Mosque Logo"/></a>
</div>
</div>
<div class="clear"></div>
</div>


<div class="container">
<div class="top_nav">
<div class="right">
<form action="/en/search.php" method="get">
<label><input type="submit" dir="ltr" class="search_btn" value="Search" /></label><input type="text" name="key"  />
</form>
</div>

<div class="left">
<div class="menu_bar">
<ul id="menu_top">
<li><a  href="/en/index" style="border-left:0px" title="Home">Home</a></li>
<li><a href="/en/contact-us">Contact Us</a></li>
<!--
<li><a  href="#">خارطة الموقع</a></li>
-->

</ul>
</div>
</div>
<div class="clear"></div>
</div>

<div class="clear"></div>
<div id="flash_banner"></div>
<script type="text/javascript">	
var fo = new SWFObject("/en/flash/header.swf", "viewer", "753", "293", "9.0.28", "#D1CAAD");	
fo.addParam("allowFullScreen","true");
fo.write("flash_banner");			
</script>
<div class="clear"></div>

<div class="top_nav">
<div class="left">
<div class="menu_bar">
<ul id="menu_top2">

<?php
if($p_id!='')
$menu_all='';
$sub_menu_flg=0;
$found_sub_menu=false;
$selected_menu_id='';
$crumb_nav1='';
$crumb_nav2='';
foreach($top_menu_obj as $key => $m1_obj){ /* looop through all parent menu*/
if($m1_obj->url=='')
$m1_obj->url='/';
echo '<li><a  title="'.$m1_obj->title.'"  href="/en'.$m1_obj->url.'">'.$m1_obj->hm_name.'</a></li>';

if($m1_obj->url==CURRENT_URL && $m1_obj->url!=''){
$selected_menu_id=$m1_obj->hm_id;
$crumb_nav1='<div class="right arrow"><a href="/en'.$m1_obj->url.'">'.$m1_obj->hm_name.'</a></div>';
}

            foreach($sub_menu_obj_1 as $key => $m2_obj){
                if($selected_menu_id==$m2_obj->hm_id){
                    $found_sub_menu=true;
                }
		if($m2_obj->url==CURRENT_URL && $m2_obj->hm_id==$m1_obj->hm_id){
                    $selected_menu_id=$m1_obj->hm_id;
                    $crumb_nav2='<div class="right arrow"><a href="/en'.$m2_obj->url.'">'.$m2_obj->sm1_name.'</a></div>';
		}
		foreach($sub_menu_obj_2 as $key => $m3_obj){ 
                    if($m3_obj->url==CURRENT_URL && $m3_obj->sm1_id==$m2_obj->sm1_id){
                        $selected_menu_id=$m2_obj->hm_id;
                        if($m2_obj->hm_id === $m1_obj->hm_id)
                            $crumb_nav1='<div class="right arrow"><a href="/en'.$m1_obj->url.'">'.$m1_obj->hm_name.'</a></div>';
                        $crumb_nav2='<div class="right arrow"><a href="/en'.$m2_obj->url.'">'.$m2_obj->sm1_name.'</a></div>';
                        $crumb_nav3='<div class="right arrow"><a href="/en'.$m3_obj->url.'">'.$m3_obj->sm2_name.'</a></div>';
                    }
                }
            }
}
?>
</ul>
</div>
</div>
<div class="clear"></div>
</div>
<div class="clear"></div>
<div id="content_inner">

<?php 
//if($p_obj->p_id=='' && CURRENT_FILE!='search.php' && CURRENT_FILE!='test.php')

if($p_obj->p_id!='' && CURRENT_FILE!='search.php')
{
echo '<div class="right arrow"><a href="/en/index" title="Home">Home</a></div>';
//echo $crumb_nav_parent;
echo $crumb_nav1;
echo $crumb_nav2;
echo $crumb_nav3;
//echo '<div class="clear padding_bottom"></div>';
}
$sp_cls=(CURRENT_URL=='/')?'left_p_margin':'';/* for adujusting top margin in home page*/
?>
<div class="clear"></div>
<div class="left_panel <?php echo $sp_cls;?>">
<?php 
if($p_obj->p_id=='' || CURRENT_URL=='/' || CURRENT_URL=='/minister-hh')
include_once('./src/left_pannel_home.php'); 
else
{
?>
<div id="left_navs">
<ul>
<?php
/*
foreach($sub_menu_obj_1 as $key => $m2_obj){ 

if($selected_menu_id==$m2_obj->hm_id)
	echo '<li><a  title="'.$m2_obj->title.'"  href="'.$m2_obj->url.'">'.$m2_obj->sm1_name.'</a><ul>';
	foreach($sub_menu_obj_2 as $key => $m3_obj){ 
	if($m3_obj->sm1_id==$m2_obj->sm1_id)
		echo '<li><a  title="'.$m3_obj->title.'"  href="'.$m3_obj->url.'">'.$m3_obj->sm2_name.'</a></li>';
	}
	echo '</ul></li>';
}

*/
/*
echo "SELECT sb1.hm_id as id FROM sub_menu_step1 sb1 INNER JOIN sub_menu_step2 sb2 ON sb2.sm1_id = sb1.sm1_id WHERE sb2.sm2_id =$selected_menu_id";

if($found_sub_menu2){
	$sql = "SELECT sb1.hm_id as id FROM sub_menu_step1 sb1 INNER JOIN sub_menu_step2 sb2 ON sb2.sm1_id = sb1.sm1_id WHERE sb2.sm2_id =$selected_menu_id";
	$rs=$mydb->query($sql);
	$page_obj=$mydb->loadResult();
	var_dump($page_obj);
	
	$page_obj=$page_obj[0];

$selected_menu_id=$page_obj->id;
print $selected_menu_id;
}
*/


foreach($sub_menu_obj_1 as $key => $m2_obj){ 
if($selected_menu_id==$m2_obj->hm_id)
{
$sub_menu_step2='';
if($m2_obj->url=='')
$m2_obj->url='/';

if($m2_obj->url == 'http://www.etisalat.ae/eimmail.html'){
    echo '<li><a  title="'.$m2_obj->title.'"  href="/en'.$m2_obj->url.'" target="_blank">'.$m2_obj->sm1_name.'</a>';
}else{
    echo '<li><a  title="'.$m2_obj->title.'"  href="/en'.$m2_obj->url.'">'.$m2_obj->sm1_name.'</a>';
}

foreach($sub_menu_obj_2 as $key => $m3_obj){ 
if($m3_obj->sm1_id==$m2_obj->sm1_id)
$sub_menu_step2.='<li><a  title="'.$m3_obj->title.'"  href="/en'.$m3_obj->url.'">'.$m3_obj->sm2_name.'</a>';
}
if($sub_menu_step2!='')
echo "<ul>$sub_menu_step2</ul>";
echo'</li>';
}
}
?>
</ul>
</div>
<?php
}




?>
</div>