<?php
$mydb=new connect;
$p_id='';

$p_obj=get_page_by_page_id(CURRENT_URL);
//var_dump($p_obj);
if(isset($_SESSION['admin_logged']) && $_SESSION['admin_logged']!='')
$admin_common_sql="m.visibility='A' || m.visibility='B' ";
else
$admin_common_sql="m.visibility='P' || m.visibility='B' ";

$sql="select m.*,p.url,p.p_id from header_top_nav m LEFT JOIN pages p ON m.p_id=p.p_id where ($admin_common_sql)  GROUP BY m.hm_id ORDER BY m.pos desc";
$rs=$mydb->query($sql);
$top_menu_obj=$mydb->loadResult();

$sql="select m.*,p.url,p.p_id from sub_menu_step1 m LEFT JOIN pages p ON m.p_id=p.p_id where ($admin_common_sql)  GROUP BY m.sm1_id ORDER BY m.pos";
$rs=$mydb->query($sql);
$sub_menu_obj_1=$mydb->loadResult();

$sql="select m.*,p.url from sub_menu_step2 m LEFT JOIN pages p ON m.p_id=p.p_id where ($admin_common_sql) and m.live='Y'  GROUP BY m.sm2_id ORDER BY m.pos";
$rs=$mydb->query($sql);
$sub_menu_obj_2=$mydb->loadResult();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo htmlspecialchars(TITLE); ?></title>
<meta name="description" content="<?php echo DESC; ?>"/>
<meta name="keywords" content="<?php echo KEYWORD; ?>"/>
<meta name="copyright" content="Grand Mosque"/>
<meta name="author" content="Grand Mosque" />
<meta name="copyright" content="2010 Grand Mosque" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link href="/css/styles.css" rel="stylesheet" type="text/css" />
<link href="/css/calendar.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="/js/ajaxupload.3.5.js"></script>
<script type="text/javascript" src="/js/jquery.validate.js"></script>
<script type="text/javascript" src="/js/cal.js"></script>
<script type="text/javascript" src="/js/main.js"></script>
<script type="text/javascript" src="/js/AC_RunActiveContent.js"></script>
<script type="text/javascript" src="/js/swfobject.js"></script>
</head>
<body>

<div class="container">
<div class="header_top">

<div style="float:left">
<div class="date_wheather">
<div class="date_time_now">
<?php
$dt=date("D, M j Y h:i a ");
$todayDate = date("Y-m-d g:i a");// current date
$currentTime = time($todayDate); //Change date into time
$new_time = $currentTime+60*180;
echo date("D, M j Y h:i a",$new_time);
//echo $dt;
?>
</div>
<div class="clear"></div>
<div class="wheather">
<div class="wheather_inner"><?php echo weather();?></div>
</div>
</div>

<div class="clear"></div>
<div style="float:left; padding-top:15px;" dir="rtl">
<h1>
<a href="/"><img src="/img/grand_mosque.jpg" alt="مركز جامع الشيخ زايد الكبير"/></a>
<!--
مسجد الشيخ زايد الكبير -->
</h1>
</div>
</div>

<div class="grand_mosque_logo">
<a href="/"><img src="/img/logo.jpg" alt="Grand Mosque Logo"/></a>
</div>
</div>
<div class="clear"></div>
</div>


<div class="container">
<div class="top_nav">
<div class="left">
<form action="/search.php" method="get">
<label><input type="submit" dir="rtl" class="search_btn" value="ابحث" /></label><input type="text" name="key"  />
</form>
</div>

<div class="right">
<div class="menu_bar">
<ul id="menu_top">
<li><a href="#" style="border-left:0px">اتصل بنا</a></li>
<li><a  href="#">خارطة الموقع</a></li>
<li><a  href="/" title="الصفحة الرئيسية">الصفحة الرئيسية</a></li>
</ul>
</div>
</div>
<div class="clear"></div>
</div>

<div class="clear"></div>
<div id="flash_banner"></div>
<script type="text/javascript">	
var fo = new SWFObject("/flash/header.swf", "viewer", "753", "293", "9.0.28", "#D1CAAD");	
fo.addParam("allowFullScreen","true");
fo.write("flash_banner");			
</script>
<div class="clear"></div>

<div class="top_nav">
<div class="right">
<div class="menu_bar">
<ul id="menu_top2">

<?php
if($p_id!='')
$menu_all='';
$sub_menu_flg=0;
$found_sub_menu=false;
$selected_menu_id='';
$crumb_nav1='';
$crumb_nav2='';
foreach($top_menu_obj as $key => $m1_obj){ /* looop through all parent menu*/
if($m1_obj->url=='')
$m1_obj->url='/';
echo '<li><a  title="'.$m1_obj->title.'"  href="'.$m1_obj->url.'">'.$m1_obj->hm_name.'</a></li>';

if($m1_obj->url==CURRENT_URL && $m1_obj->url!='')
{
$selected_menu_id=$m1_obj->hm_id;
$crumb_nav1='<div class="right arrow"><a href="'.$m1_obj->url.'">'.$m1_obj->hm_name.'</a></div>';
}

foreach($sub_menu_obj_1 as $key => $m2_obj){ 
if($selected_menu_id==$m2_obj->hm_id)
{
$found_sub_menu=true;
}

if($m2_obj->url==CURRENT_URL && $m2_obj->hm_id==$m1_obj->hm_id)
{
$selected_menu_id=$m1_obj->hm_id;
$crumb_nav2='<div class="right arrow"><a href="'.$m2_obj->url.'">'.$m2_obj->sm1_name.'</a></div>';
}
}
}
?>
</ul>
</div>
</div>
<div class="clear"></div>
</div>
<div class="clear"></div>
<div id="content_inner">

<?php 
//if($p_obj->p_id=='' && CURRENT_FILE!='search.php' && CURRENT_FILE!='test.php')

if($p_obj->p_id!='' && CURRENT_FILE!='search.php')
{
echo '<div class="right arrow"><a href="/" title="الصفحة الرئيسية">الصفحة الرئيسية</a></div>';
//echo $crumb_nav_parent;
echo $crumb_nav1;
echo $crumb_nav2;
//echo '<div class="clear padding_bottom"></div>';
}



$sp_cls=(CURRENT_URL=='/')?'left_p_margin':'';/* for adujusting top margin in home page*/
?>
<div class="clear"></div>
<div id="left_navs" style="height: 802px;">
<ul>
    <li>
        <a href="/gramd-mosque-tourist-information" title="خطط زيارتك">خطط زيارتك</a>
        <ul>
            <li><a href="" title="رسوم الدخول">رسوم الدخول</a></li>
            <li><a href="" title="لائحة آداب الزوار">لائحة آداب الزوار</a></li>
            <li><a href="" title="الاحتياجات الخاصة">الاحتياجات الخاصة</a></li>
            <li><a href="" title="أوقات الزيارة">أوقات الزيارة</a></li>
        </ul>
    </li>
    <li><a href="/grand-mosque-tourist-information-getting-here" title="طريقة الوصول">طريقة الوصول</a>
        <ul>
            <li><a href="" title="بالسيارة الخاصة">بالسيارة الخاصة</a></li>
            <li><a href="" title="سيارة أجرة">سيارة أجرة</a></li>
            <li><a href="" title="النقل الجماعي">النقل الجماعي</a></li>
            <li><a href="" title="أخرى">أخرى</a></li>
        </ul>
    </li><li><a href="/tours_main.php" title="Tours">Tours</a><ul><li><a href="" title="جولات سياحية مجانية مع الدليل">جولات سياحية مجانية مع الدليل</a></li><li><a href="" title="الجولات الانفرادية">الجولات الانفرادية</a></li><li><a href="/tours.php" title="جولات سياحية جماعية والحجز الخاص">جولات سياحية جماعية والحجز الخاص</a></li></ul></li><li><a href="/filming-media" title="الصحافة / معلومات عن التسجيل الفوتوغرافي">الصحافة / معلومات عن التسجيل الفوتوغرافي</a><ul><li><a href="/media-filiming-information-registration.php" title="Media Form">Media Form</a></li></ul></li><li><a href="/faqs" title="أسئلة متكررة">أسئلة متكررة</a><ul></ul></li><li><a href="/contact-us" title="اتصل بنا">اتصل بنا</a><ul></ul></li><li><a href="/permit-tour-groups" title="تصريح مجموعات سياحية">تصريح مجموعات سياحية</a><ul></ul></li><li><a href="/permission-to-visit" title="تصريح زيارة جماعية">تصريح زيارة جماعية</a><ul></ul></li><li><a href="/entry" title="تصريخ دخول">تصريخ دخول</a><ul></ul></li><li><a href="/gramd-mosque-tourist-information" title="تصريح تصوير للجامع">تصريح تصوير للجامع</a><ul></ul></li></ul>
</div>