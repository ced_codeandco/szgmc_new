<?php
error_reporting(E_ALL);
//ini_set('display_errors','On');
include 'includes/database.php';
include 'includes/dal/news.php';
include 'includes/dal/pages.php';
include 'includes/functions.php';
include 'includes/config.php';

$current_page = 'index';

$db = new MyDatabase();
$conf = new Configuration();
$site_path = $conf->site_url;


$conf->site_description = 'The board members of the Sheikh Zayed Grand Mosque in Abu Dhabi. Find all about the Sheikh Zayed Grand Mosque in Abu Dhabi including, visiting timings, how to get to the mosque, dress code, tours, history, architecture and more.';

$conf->site_keywords = 'grand mosque, sheikh zyed grand mosque, mosque in adu dhabi, what to wear in mosque, how to get to mosque, history of grand mosque, architecture of grand mosque, grand mosque photos, board members';

$dal_news = new ManageNews();
$dal_pages = new ManagePages(); //for message from minister of presedential affairs
$page = $dal_pages->getPage('', 30); // id must be 119 for message from minister, if different then use that


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>مسجد الشيخ زايد الكبير في أبوظبي</title>
<?php include 'includes/common_header.php'; ?>
</head>

<body>

<?php include 'includes/menus/banner_header3.php'; ?>		
			<!-- Banner Start -->
<div class="banner">
	<img src="<?php echo $site_path;?>images/visiting_the_mosque_banner.jpg">     
</div>
<!-- Banner Close -->	
<!-- Content Start -->
	<div class="main_box_content visiting_page_height">
	 <div id="dialognew" style="display:none;">
        	<p style="text-align:right;">لقد تم تقديم طلبكم بنجاح ،</p>
            <p><span dir="ltr">Your request has been submitted successfully</span></p>
            <p style=""><input type="button" value="OK" style="float:right;width:40px" id="confirmnew"></p>
        </div>
		 <?php include 'includes/menus/left_menu.php'; ?>
        <div class="clear"></div>
        <div class="content">
        	<div class="brad_cram">
            	<ul>
            	   <li><a href="<?php echo $site_path; ?>">الصفحة الرئيسية</a></li>
                    <li><a href="#" class="active">لم يتم العثور على الصفحة</a></li>
                </ul>
            </div>
      
      		<div class="content-left">
			 <?php //include 'includes/menus/left_menu1.php'; 
				?>
				
                <br class="clear"/>
                <?php 
				include 'includes/menus/ministry_logos.php';
                ?>
                <?php include 'includes/menus/rightsidebanner.php';?>
			</div>
	  		<div class="content-right" style="margin-right:30px">
			
			<div class="middle"> 
     <div class="general_body_content" style="background:none; border:0; margin-right:20px;">
	   <h2>لم يتم العثور على الصفحة</h2>

       <p style="color:#656565; font-size:15px; font-weight:bold; padding-top:20px;">عذراً ، لا يمكننا العثور على الصفحة المطلوبة.</p>
	   <p style="color:#656565; font-size:15px; font-weight:bold; padding-top:10px;">ماذا يمكنك فعله الآن؟</p>
       <p style="padding-top:10px;color:#646464;">الرجاء محاولة ما يلي:</p>
	   <ul style="color:#646464; margin-right:20px;"  >
           <li style="background:none; border:0; padding:5px;list-style:disc;"> إذا قمت بكتابة عنوان URL، تأكد من دقة الأحرف وحاول مرة أخرى.</li>
           <li style="background:none; border:0;padding:5px;list-style:disc;">	ألق نظرة على  <a href="/sitemap" style="color:#e93b45; text-decoration:none;">خريطة الموقع</a></li>
           <li style="background:none; border:0;padding:5px;list-style:disc;"><a href="/contact-us" style="color:#e93b45; text-decoration:none;">اتصل بنا</a> للحصول على المساعدة</li>
           <li style="background:none; border:0;padding:5px;list-style:disc;">انتقل إلى <a href="/" style="color:#e93b45; text-decoration:none;">الصفحة الرئيسية</a>.</li>
       </ul>    
	 </div> 
	 
	 
   </div>
			
			</div>
 <br class="clear" />      
        </div>
    
    </div>

<div class="content_bottom">&nbsp;</div>
<?php require 'includes/footer.php'; ?>
</body>
</html>
