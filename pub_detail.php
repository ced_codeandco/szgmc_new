<?php
include 'includes/database.php';
include 'includes/dal/publication.php';
include 'includes/functions.php';
include 'includes/config.php';

$id = $_REQUEST['id']; 
$conf = new Configuration();
$db = new MyDatabase();
$dal_news = new ManageNews();
$news = $dal_news->getSingleNews($id);

$slug = 'publications';

$site_path = $conf->site_url;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>اصدارات المركز</title>
<?php include 'includes/common_header.php'; ?>
</head>

<body>
<?php include 'includes/menus/banner_header3.php'; ?>		
			<!-- Banner Start -->
<div class="banner">
	<img src="<?php echo $site_path;?>images/visiting_the_mosque_banner.jpg">     
</div>
<!-- Banner Close -->	
<!-- Content Start -->
	<div class="main_box_content visiting_page_height">
	 	 <?php include 'includes/menus/left_menu.php'; ?>
        <div class="clear"></div>
        <div class="content">
        	<div class="brad_cram">
            	<ul>
            	   <li><a href="<?php echo $site_path; ?>">الصفحة الرئيسية</a></li>
                    <li><a href="#" class="active"><?php echo $news->pub_title; ?></a></li>
                </ul>
            </div>
      
      		<div class="content-left">
			 <?php //include 'includes/menus/left_menu1.php'; 
				?>
				
                <br class="clear"/>
                <?php 
				include 'includes/menus/ministry_logos.php'; 
				?>
                <?php include 'includes/menus/rightsidebanner.php';?>
			</div>
	  		<div class="content-right" style="margin-right:30px">
			<div class="single_middle" >
                <br class="clear" />
                    <?php
                    $news_url= $site_path."publication-detail/".string_to_filename($news->pub_title).'-'.$news->pub_id;
                    ?>
<div class="news_item_single">
                    	<div class="news_content" >
				        	<span class="general_body_title"><?php echo $news->pub_title; ?></span>
                        	<br class="clear" />
                            <img src="<?php echo $site_path; ?>images/news/<?php echo $news->image; ?>" class="content_inside_image" >
                            <div class="general_body_content"><?php echo $news->pub_text; ?></div>
                            <br class="clear" />
                            
                        </div>
                    </div> 
                    <div class="clear bottom_line"> &nbsp </div>
     </div>
			</div>
 <br class="clear" />      
        </div>
    
    </div>

<div class="content_bottom">&nbsp;</div>
<?php require 'includes/footer.php'; ?>
</body>
</html>
