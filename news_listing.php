<?php
include 'includes/database.php';
include 'includes/dal/news.php';
include 'includes/functions.php';
include 'includes/config.php';

$slug = explode('/',$_SERVER['REQUEST_URI']);
$slug = end($slug);
$conf = new Configuration();
$db = new MyDatabase();

$site_path = $conf->site_url;
function pagination($query,$per_page=10,$page=1,$url='?'){  
    global $conDB;
    $query = "SELECT COUNT(*) as `num` FROM {$query}";
    $row = mysqli_fetch_array(mysqli_query($conDB,$query));
    $total = $row['num'];
    $adjacents = "2";
      
    $prevlabel = "&lsaquo; Prev";
    $nextlabel = "Next &rsaquo;";
    $lastlabel = "Last &rsaquo;&rsaquo;";
      
    $page = ($page == 0 ? 1 : $page); 
    $start = ($page - 1) * $per_page;                              
      
    $prev = $page - 1;                         
    $next = $page + 1;
      
    $lastpage = ceil($total/$per_page);
      
    $lpm1 = $lastpage - 1; // //last page minus 1
      
    $pagination = "";
    if($lastpage > 1){  
        $pagination .= "<ul class='pagination'>";
        $pagination .= "<li class='page_info'>Page {$page} of {$lastpage}</li>";
              
            if ($page > 1) $pagination.= "<li><a href='{$url}page={$prev}'>{$prevlabel}</a></li>";
              
        if ($lastpage < 7 + ($adjacents * 2)){  
            for ($counter = 1; $counter <= $lastpage; $counter++){
                if ($counter == $page)
                    $pagination.= "<li><a class='current'>{$counter}</a></li>";
                else
                    $pagination.= "<li><a href='{$url}page={$counter}'>{$counter}</a></li>";                   
            }
          
        } elseif($lastpage > 5 + ($adjacents * 2)){
              
            if($page < 1 + ($adjacents * 2)) {
                  
                for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++){
                    if ($counter == $page)
                        $pagination.= "<li><a class='current'>{$counter}</a></li>";
                    else
                        $pagination.= "<li><a href='{$url}page={$counter}'>{$counter}</a></li>";                   
                }
                $pagination.= "<li class='dot'>...</li>";
                $pagination.= "<li><a href='{$url}page={$lpm1}'>{$lpm1}</a></li>";
                $pagination.= "<li><a href='{$url}page={$lastpage}'>{$lastpage}</a></li>"; 
                      
            } elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2)) {
                  
                $pagination.= "<li><a href='{$url}page=1'>1</a></li>";
                $pagination.= "<li><a href='{$url}page=2'>2</a></li>";
                $pagination.= "<li class='dot'>...</li>";
                for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++) {
                    if ($counter == $page)
                        $pagination.= "<li><a class='current'>{$counter}</a></li>";
                    else
                        $pagination.= "<li><a href='{$url}page={$counter}'>{$counter}</a></li>";                   
                }
                $pagination.= "<li class='dot'>..</li>";
                $pagination.= "<li><a href='{$url}page={$lpm1}'>{$lpm1}</a></li>";
                $pagination.= "<li><a href='{$url}page={$lastpage}'>{$lastpage}</a></li>";     
                  
            } else {
                  
                $pagination.= "<li><a href='{$url}page=1'>1</a></li>";
                $pagination.= "<li><a href='{$url}page=2'>2</a></li>";
                $pagination.= "<li class='dot'>..</li>";
                for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++) {
                    if ($counter == $page)
                        $pagination.= "<li><a class='current'>{$counter}</a></li>";
                    else
                        $pagination.= "<li><a href='{$url}page={$counter}'>{$counter}</a></li>";                   
                }
            }
        }
          
            if ($page < $counter - 1) {
                $pagination.= "<li><a href='{$url}page={$next}'>{$nextlabel}</a></li>";
                $pagination.= "<li><a href='{$url}page=$lastpage'>{$lastlabel}</a></li>";
            }
          
        $pagination.= "</ul>";       

    }
      
    return $pagination;
}
$dal_news = new ManageNews();
$total_news = $dal_news->countNews();
$current_page = isset($_GET['current_page'])?mysql_real_escape_string($_GET['current_page']):1;

$news_per_page = 6;

$starts_with = ($current_page-1) * $news_per_page;

$slug = 'news-list'; 

 function getMonth($month) {
		$list = array(
				'Jan' => 'يناير',
				'Feb' => 'فبراير',
				'Mar' => 'مارس',
				'Apr' => 'ابريل',
				'May' => 'مايو',
				'Jun' => 'يونيو',
				'Jul' => 'يوليو',
				'Aug' => 'أغسطس',
				'Sep' => 'سبتمبر',
				'Oct' => 'أكتوبر',
				'Nov' => 'نوفمبر',
				'Dec' => 'ديسمبر'
					);
		
		if(in_array($month, array_keys($list))) {
			//die($list[$slug]);
			return $list[$month];
		}
		else
			return '';
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sheikh Zayed Grand Mosque in Abu Dhabi</title>
<?php include 'includes/common_header.php'; ?>
<link href="<?php echo $site_path; ?>css/news.css" rel="stylesheet" type="text/css" />
	
</head>

<body>
<?php include 'includes/menus/banner_header3.php'; ?>		
			<!-- Banner Start -->
<div class="banner">
	<img src="<?php echo $site_path;?>images/visiting_the_mosque_banner.jpg">     
</div>
<!-- Banner Close -->	
<!-- Content Start -->
	<div class="main_box_content">
	 	 <?php include 'includes/menus/left_menu.php'; ?>
        <div class="clear"></div>
        <div class="content">
        	<div class="brad_cram">
            	<ul>
            	   <li><a href="<?php echo $site_path; ?>">الصفحة الرئيسية</a></li>
                   <li><a href="javascript:void(0);" class="">الركن الإعلامي</a></li>
                    <li><a href="#" class="active">الأخبار</a></li>
                </ul>
            </div>
      
      		<div class="content-left">
			 <?php //include 'includes/menus/left_menu1.php'; 
				?>
				
                <br class="clear"/>
                <?php 
				include 'includes/menus/ministry_logos.php'; 
				?>
                <?php include 'includes/menus/rightsidebanner.php';?>
			</div>
	  		<div class="content-right" style="margin-right:30px">
			<div class="single_middle" >
		<?php 
        include 'includes/menus/marquee.php'; 
        ?>
                <h2 class="clear"> الأخبار </h2>
<br class="clear" />
                <?php
				
								 
					
					 
					 // displaying paginaiton.
					
				
					$first = false;
					$bk_class = '';
					$news_list = $dal_news->getNews('', $news_per_page, $starts_with);
					//print_r($news_list);
					foreach($news_list as $news) {
					$news_short=strip_tags($news->news_text);
					$news_short=preg_replace('/\s+?(\S+)?$/', '', substr($news_short, 0, 500));
					$news_short = strip_tags($news_short);
					$news_url= $site_path."news-detail/".string_to_filename($news->news_title).'-'.$news->news_id;
					//$bk_class = $first ? 'first' : 'second';
					$first = !$first;
				  ?>
                <div class="news_story <?php echo $bk_class; ?>">
				<?php
				if($news->image!="")
				{
				?>
                	<a href="<?php echo $news_url; ?>"><img src="<?php echo $site_path; ?>news_images/<?php echo $news->image; ?>" height="111" width="111" />
                    </a>
					<?php
					} 
					else
					{
					?>
					<a href="<?php echo $news_url; ?>"><img src="<?php echo $site_path; ?>images/News_arabi.jpg" height="111" width="111" />
                    </a>
					<?php
						}
						$date = date('M, d, Y', strtotime($news->news_date));
						$date = explode(",", $date);
						  $month = $date[0];
						$month= getMonth($month);
						$news_date=$date[1]."&nbsp;".$month."&nbsp;".$date[2];
					?>
					
                    <div class="date"><?php echo $news_date; ?></div>
                    <h3><a href="<?php echo $news_url; ?>"><?php echo $news->news_title; ?></a></h3>
                	<p><?php echo $news_short; ?></p>
                    <div class="read_more"><a href="<?php echo $news_url; ?>">اقرأ المزيد </a>&gt;&gt;</div>
                    <div class="clear"></div>
                </div>
                <br class="clear" />
                <?php } ?>          
                <div id="news_controller">
                	<span class="<?php echo ($current_page>1)?'previous_inactive':'previous_inactive'; ?>">
					<?php if($current_page>1) { ?>
                    	<a href="<?php echo $site_path; ?>news-list/<?php echo $current_page-1; ?>" >السابق</a>
                    <?php } else { echo '&#1575;&#1604;&#1587;&#1575;&#1576;&#1602;'; } ?>
                    </span>
                    
					
					<?php 
					$last_page = $total_news % $news_per_page == 0 ? $total_news / $news_per_page : $total_news / $news_per_page + 1;
					
					if($last_page > 7) {
					if($current_page == 1){						
						$displayArray = array(1,2,3,4,5,6,7);						
					}else if($current_page == 2){
						$displayArray = array(1,2,3,4,5,6,7);
					}else if($current_page == 3){
						$displayArray = array(1,2,3,4,5,6,7);
					}else {
						$displayArray = array($current_page,$current_page-1,$current_page-2,$current_page-3,$current_page+1,$current_page+2,$current_page+3);
					}
					}
					
					for($i=1;$i<=($last_page);$i++) {
					
					?>
                	<span <?php  if($last_page > 7) { if(!in_array($i,$displayArray)){echo 'style="display:none;"';} }?>  <?php echo $i == $current_page ? 'class="current"' : ''; ?>><a href="<?php echo $site_path; ?>news-list/<?php echo $i; ?>" ><?php echo $i; ?></a></span>
                    <?php } ?>
                    <span  class="<?php echo ($current_page<($total_news/$news_per_page)+1)?'next_active':'next_inactive'; ?>">
					<?php if($current_page<($total_news/$news_per_page)) { ?>
                    	<a href="<?php echo $site_path; ?>news-list/<?php echo $current_page+1; ?>"  class="margin-top" >التالي</a>
                    <?php } else { echo 'التالي'; } ?>
                    </span>                </div>
     </div>
			</div>
 <br class="clear" />      
        </div>
    
    </div>

<div class="content_bottom">&nbsp;</div>
<?php require 'includes/footer.php'; ?>
</body>
</html>
