<?php

error_reporting(E_ALL);

//ini_set('display_errors','On');

define("ERROR_LOG",true);

include 'includes/database.php';

include 'includes/dal/news.php';

include 'includes/dal/pages.php';

include 'includes/functions.php';

include 'includes/config.php';

$current_page = 'index';

$db = new MyDatabase();

$conf = new Configuration();

$site_path = $conf->site_url;

$conf->site_description = 'The board members of the Sheikh Zayed Grand Mosque in Abu Dhabi. Find all about the Sheikh Zayed Grand Mosque in Abu Dhabi including, visiting timings, how to get to the mosque, dress code, tours, history, architecture and more.';

$conf->site_keywords = 'grand mosque, sheikh zyed grand mosque, mosque in adu dhabi, what to wear in mosque, how to get to mosque, history of grand mosque, architecture of grand mosque, grand mosque photos, board members';

$dal_news = new ManageNews();

$dal_pages = new ManagePages(); //for message from minister of presedential affairs

$page = $dal_pages->getPage('', 30); // id must be 119 for message from minister, if different then use that

$stat_page= "home";

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<?php include 'includes/common_header.php'; ?>

 <script src="/js/jquery.marquee.js"></script> <style>

.newsMarque{margin:5px 8px 30px 24px!important}

</style>

<script type="text/javascript">

		$(document).ready(function(){

			

			$(".prayer_seeless").css({'display':'none'})

			$("#flip").click(function(){

                            $("#prayertiming").slideDown("slow");

                            $(".prayer_seemore").css({'display':'none'});

                            $(".prayer_seeless").css({'display':'block'});

			});

                        

                        $("#flip1").click(function(){

                            $("#prayertiming").slideUp("slow");

                           $(".prayer_seemore").css({'display':'block'});

                            $(".prayer_seeless").css({'display':'none'});

			});

			

			$('#js-news').ticker({

				speed: 0.3,          

				ajaxFeed: false,      

				feedUrl: false,       

				feedType: 'xml',    

				htmlFeed: true,        

				debugMode: true,      

				controls: true,        

				titleText: false,   

				displayType: 'reveal', 

				direction: 'ltr',       

				pauseOnItems: 2000,    

				fadeInSpeed: 600,     

				fadeOutSpeed: 300

			});

			

		});

	</script>



</head>



<body class="index"  onLoad="$('.page_spinner').animate({opacity:0}, 900).hide(0);">

<div class="page_spinner" style="

                display:block;

                position: fixed;

                background: url(images/footer-mosque-image.png) 50% 50% no-repeat #fff; 

				z-index: 9999999999999999999999999999999!important; 

                width: 100%;

                height: 100%;

                top: 0;

                left: 0; background-color: #fff;">

</div>

<?php include 'includes/menus/banner_header3.php'; ?>

<!-- Banner Start -->

<div class="banner" style="clear:both;">

	<div id="rev_slider_1_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" style="margin:0px auto;padding:0px;margin-top:0px;margin-bottom:0px;max-height:359px;height:359px;">

					<div id="rev_slider_1_1" class="rev_slider fullwidthabanner" style="display:none;max-height:359px;height:359px;">						

                        <ul>

                            <li data-masterspeed="2000"><img src="<?php echo $site_path;?>images/slider_01.jpg"></li>

                            <!--<li><img src="<?php /*echo $site_path;*/?>images/slider_02.jpg"></li>-->

                            <li data-masterspeed="2000"><img src="<?php echo $site_path;?>images/slider_03.jpg"></li>

                            <li data-masterspeed="2000"><img src="<?php echo $site_path;?>images/slider_04.jpg"></li>

                        </ul>

					</div>

				</div>          

</div>

<!-- Banner Close -->

<!-- Content Start -->

	<div class="main_box_content" style="height:1401px;">

		<?php include 'includes/menus/left_menu.php'; ?>

        <div class="clear"></div>

        <div class="content">

        <div class="content-left">

        	<?php include 'includes/menus/rightsidebanner.php';?>

      </div>

      <div class="content-right">

      			<div class="right_banner">

                	<div class="flexslider">

                        <ul class="slides">

                            <li>

                                <div class="caption_right_banner">

                                    <h2><!--<strong>أكبر سجادة في العالم</strong>-->

                                        <!--نظرة عامة-->

                                        الجماليات والعمارة

                                    </h2>

                                    <span class="more_btn"><a href="<?php echo $site_path;?>general-architecture">اقرأ المزيد</a></span>

                                </div>

                                <img src="<?php echo $site_path;?>images/home_slider/general-architecture.jpg"/>

                            </li>



                            <li>

                                <div class="caption_right_banner">

                                    <h2><!--<strong>أكبر سجادة في العالم</strong>-->

                                        القباب

                                    </h2>

                                    <span class="more_btn"><a href="<?php echo $site_path;?>domes">اقرأ المزيد</a></span>

                                </div>

                                <img src="<?php echo $site_path;?>images/home_slider/domes.jpg"/>

                            </li>





                            <li>

                                <div class="caption_right_banner">

                                    <h2><!--<strong>أكبر سجادة في العالم</strong>-->

                                        الرخام

                                    </h2>

                                    <span class="more_btn"><a href="<?php echo $site_path;?>marbles">اقرأ المزيد</a></span>

                                </div>

                                <img src="<?php echo $site_path;?>images/home_slider/marbles.jpg"/>

                            </li>



                            <li>

                                <div class="caption_right_banner">

                                    <h2><!--<strong>أكبر سجادة في العالم</strong>-->

                                        الإضاءة القمرية

                                    </h2>

                                    <span class="more_btn"><a href="<?php echo $site_path;?>lunar-illumination">اقرأ المزيد</a></span>

                                </div>

                                <img src="<?php echo $site_path;?>images/home_slider/lunar-illumination.jpg"/>

                            </li>



                            <li>

                                <div class="caption_right_banner">

                                    <h2>

                                        السجادة:

                                        <strong>أكبر سجادة في العالم</strong> </h2>

                                    <span class="more_btn"><a href="<?php echo $site_path;?>carpets">اقرأ المزيد</a></span>

                                </div>

                                <img src="<?php echo $site_path;?>images/home_slider/carpets.jpg"/>

                            </li>



                            <li>

                                <div class="caption_right_banner">

                                    <h2><!--<strong>أكبر سجادة في العالم</strong>-->

                                        الثريّات

                                    </h2>

                                    <span class="more_btn"><a href="<?php echo $site_path;?>chandeliers">اقرأ المزيد</a></span>

                                </div>

                                <img src="<?php echo $site_path;?>images/home_slider/chandeliers.jpg"/>

                            </li>



                            <li>

                                <div class="caption_right_banner">

                                    <h2><!--<strong>أكبر سجادة في العالم</strong>-->

                                        المنبر

                                    </h2>

                                    <span class="more_btn"><a href="<?php echo $site_path;?>pulpit">اقرأ المزيد</a></span>

                                </div>

                                <img src="<?php echo $site_path;?>images/home_slider/pulpit.jpg"/>

                            </li>



                            <li>

                                <div class="caption_right_banner">

                                    <h2><!--<strong>أكبر سجادة في العالم</strong>-->

                                        المنارة

                                    </h2>

                                    <span class="more_btn"><a href="<?php echo $site_path;?>minaret">اقرأ المزيد</a></span>

                                </div>

                                <img src="<?php echo $site_path;?>images/home_slider/minaret.jpg"/>

                            </li>



                            <li>

                                <div class="caption_right_banner">

                                    <h2><!--<strong>أكبر سجادة في العالم</strong>-->

                                        الأحواض العاكسة

                                    </h2>

                                    <span class="more_btn"><a href="<?php echo $site_path;?>reflective-pools">اقرأ المزيد</a></span>

                                </div>

                                <img src="<?php echo $site_path;?>images/home_slider/reflective-pools.jpg"/>

                            </li>



                            <li>

                                <div class="caption_right_banner">

                                    <h2><!--<strong>أكبر سجادة في العالم</strong>-->

                                        الأعمدة

                                    </h2>

                                    <span class="more_btn"><a href="<?php echo $site_path;?>columns">اقرأ المزيد</a></span>

                                </div>

                                <img src="<?php echo $site_path;?>images/home_slider/columns.jpg"/>

                            </li>





                            <li>

                                <div class="caption_right_banner">

                                    <h2><!--<strong>أكبر سجادة في العالم</strong>-->

                                        المحراب

                                    </h2>

                                    <span class="more_btn"><a href="<?php echo $site_path;?>mihrab">اقرأ المزيد</a></span>

                                </div>

                                <img src="<?php echo $site_path;?>images/home_slider/mihrab.jpg"/>

                            </li>



                            <li>

                                <div class="caption_right_banner">

                                    <h2><!--<strong>أكبر سجادة في العالم</strong>-->

                                        الصحن

                                    </h2>

                                    <span class="more_btn"><a href="<?php echo $site_path;?>sahan">اقرأ المزيد</a></span>

                                </div>

                                <img src="<?php echo $site_path;?>images/home_slider/sahan.jpg"/>

                            </li>



                            <!------------      OLD  ------------------>

                            <!--<li>

                            <div class="caption_right_banner">

                                <h2><strong>أكبر سجادة في العالم</strong> :جماليات والعمارة</h2>

                                <span class="more_btn"><a href="<?php /*echo $site_path;*/?>carpets">اقرأ المزيد</a></span>

                            </div>

                            <img src="<?php /*echo $site_path;*/?>images/right_banner_01.jpg"/>

                            </li>

                            

                            <li>

                            <div class="caption_right_banner">

                                <h2><strong>إضاءة القمر</strong> :جماليات والعمارة</h2>

                                <span class="more_btn"><a href="<?php /*echo $site_path;*/?>lunar-illumination">اقرأ المزيد</a></span>

                            </div>

                            <img src="<?php /*echo $site_path;*/?>images/right_banner_02.jpg"/>

                            </li>

                            

                            <li>

                            <div class="caption_right_banner">

                                <h2><strong>مجلس المدينة</strong> :جماليات والعمارة</h2>

                                <span class="more_btn"><a href="<?php /*echo $site_path;*/?>domes">اقرأ المزيد</a></span>

                            </div>

                            <img src="<?php /*echo $site_path;*/?>images/right_banner_03.jpg"/>

                            </li>-->

                         </ul>

	 				</div>

            </div>

            

            

            

            

            

            <div class="menu_for_mobile mobile_view">

            	<ul>

                	<li><a href="<?php echo $site_path; ?>visit-mosque-visitors"><img src="<?php echo $site_path; ?>img/mobile/menu/icon.png"><label>الزوار</label></a></li>

                    <li><a href="<?php echo $site_path; ?>visiting-the-mosque"><img src="<?php echo $site_path; ?>img/mobile/menu/icon1.png"><label>زيارة الجامع</label></a></li>

                    <li><a href="<?php echo $site_path; ?>about-szgmc"><img src="<?php echo $site_path; ?>img/mobile/menu/icon2.png"><label>عن المركز</label></a></li>

                    <li><a href="<?php echo $site_path; ?>prayer-timings"><img src="<?php echo $site_path; ?>img/mobile/menu/icon3.png"><label>مواقيت الصلاة</label></a></li>

                    <li><a href="<?php echo $site_path; ?>e_services.php"><img src="<?php echo $site_path; ?>img/mobile/menu/icon4.png"><label>الخدمات الإلكترونية</label></a></li>

                    <li><a href="<?php echo $site_path; ?>events-and-activities"><img src="<?php echo $site_path; ?>img/mobile/menu/icon5.png"><label>أنشطة وفعاليات</label></a></li>

                </ul>

            </div>

            

            

            <div class="widge_box" id="ar-home-news-wrap">

            	<ul>

                	<li class="fist_tham"><img src="<?php echo $site_path;?>images/banners/Spaces_of_Light_Award.jpg">

                    <a href="http://photo.szgmc.ae/ar/Default.aspx" target="_blank">جائزة فضاءات من نور</a></li>

                    

                    <li><img src="<?php echo $site_path;?>images/cg-part-time-job-arabic.jpg" width="166px" height="86px">

                    <a href="cg-part-time-job">وظائف بنظام جزئي</a></li>

                    

                    <li><img src="<?php echo $site_path;?>images/mosque.jpg">

                    <a href="<?php echo $site_path;?>mosque-manner"> آداب دخول الجامع</a></li>

                    

                    <li><img src="<?php echo $site_path;?>images/vertual.jpg">

                    <a href="https://www.google.com/maps/streetview/#sheikh-zayed-grand-mosque" target="_blank" >جولة افتراضية</a></li>

                 </ul>

                 <!--<div class="widge_news">

			<ul id="js-news" class="js-hidden">

			<?php

					$news_list = $dal_news->getNews();

					//echo count($news_list);exit;

					foreach($news_list as $news) {

					

					$news_url= $site_path."news-detail/".string_to_filename($news->news_title).'-'.$news->news_id;

					$cls='';

					$news_title=substr($news->news_title,0,120);

			?>

			<li class="news-item"><a href="<?php echo $news_url;?>"><?php echo $news_title;?></a></li>

			<?php }?>

			</ul>

                 </div>-->

		 

                 <?php include 'includes/newshome.php'; ?> 

				                 

                  <span class="vedio_link"><a href="gallery.php"><img src="<?php echo $site_path; ?>images/photo_albem.jpg"></a></span>

                  <span class="vedio_link vedio_link2"><a href="videos_album.php"><img  src="<?php echo $site_path; ?>images/vedio_gallery.jpg"></a></span>

                 

            </div>

      </div>

      

     

      

        </div>

    

    </div>

    

 <div class="clear"></div>  

<!-- Content Close -->

<?php include 'includes/menus/marquees_partner.php';?>

<?php require 'includes/footer.php'; ?>





</body>

</html>

