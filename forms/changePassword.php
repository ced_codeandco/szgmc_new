<?php 
session_start();
?>
<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" id="changePasswordForm" name="changePasswordForm" enctype="multipart/form-data">
	
     <div class="input_row even">
        	<div class="input_label_div">
            	<span>Current Password</span><span class="red">*</span>
            </div>
    		<div class="input_div"> 
            	<input type="password" name="cur_password" id="cur_password" class="input_cls"/>  
            </div>
    		<div class="input_label_div_arb">
      			<div>
        			<div>
                    	<span dir="rtl">كلمة المرور الحالية
      						<span class="red">*</span>
        				</span>
                    </div>
      			</div>
    		</div>
    </div>		 
     <div class="input_row even">
        	<div class="input_label_div">
            	<span>New Password</span><span class="red">*</span>
            </div>
    		<div class="input_div"> 
            	<input type="password" name="a_password" id="a_password" class="input_cls"/>  
            </div>
    		<div class="input_label_div_arb">
      			<div>
        			<div>
                    	<span dir="rtl">كلمة المرور الجديدة
      						<span class="red">*</span>
        				</span>
                    </div>
      			</div>
    		</div>
    </div>
    <div class="input_row even">
        	<div class="input_label_div">
            	<span>Repeat New Password</span><span class="red">*</span>
            </div>
    		<div class="input_div"> 
            	<input type="password" name="a_rptpassword" id="a_rptpassword" class="input_cls"/>  
            </div>
    		<div class="input_label_div_arb">
      			<div>
        			<div>
                    	<span dir="rtl">إعادة كلمة المرور الجديدة</span><span class="red">*</span>
                    </div>
      			</div>
    		</div>
    </div>
   
    <div class="input_row odd border_bottom">
		<div align="center">
        	<div  class="clear" align="center">
        		<input type="submit" value="Submit &#65165;&#1585;&#1587;&#1604;"  name="submitchangePassword" id="submitchangePassword" class="acloginbttn" style="direction:ltr;" />
        	</div>
        </div>

	</div>
    
</form>