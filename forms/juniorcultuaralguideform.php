<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" id="mediaForm" name="mediaForm" enctype="multipart/form-data">

<!--<div class="input_row even">
</div>-->


<div class="input_row even">
<div class="input_label_div"><span>Full Name </span><span class="red">*</span><span>(As per to Passport) </span></div>
<div class="input_div"> <input type="text" name="fname" id="fname" class="input_cls"/>  </div>
<div class="input_label_div_arb">
  <div>
    <div><span dir="rtl">&#1575;&#1604;&#1575;&#1587;&#1605;&nbsp;&#1575;&#1604;&#1603;&#1575;&#1605;&#1604;<br/>
    (&#1608;&#1601;&#1602; &#1580;&#1608;&#1575;&#1586;&nbsp;&#1575;&#1604;&#1587;&#1601;&#1585;)<span class="red">*</span></span></div>
  </div>
</div>
</div>

<div class="input_row_dob odd">
<div class="input_label_div"><span>Date of Birth</span><span class="red">*</span></div>
<div class="input_div"> <input dir="ltr" type="text"  name="dob" id="dob" readonly="readonly"  class="input_cls"/>  </div>
<div class="input_label_div_arb"><span dir="rtl">تاريخ الميلاد</span><span class="red">*</span></div>
</div>

<div class="input_row even">
    <div class="input_label_div"><span>Gender</span><span class="red">*</span></div>
    <div class="input_div iwidth" align="center">
        Male<input type="radio" value="male" name="gender" style="border:0;">&#1584;&#1603;&#1585;&nbsp;&nbsp;&nbsp;&nbsp;Female<input type="radio" value="female" name="gender" style="border:0;">&#1571;&#1606;&#1579;&#1609;</div>
    <div class="input_label_div_arb"><span dir="rtl">&#1575;&#1604;&#1580;&#1606;&#1587;</span><span class="red">*</span></div>
</div>


<div class="input_row odd">
<div class="input_label_div"><span>Name of School</span><span class="red">*</span> </div>
<div class="input_div"> <input type="text" name="school_name" id="school_name" class="input_cls"/>  </div>
<div class="input_label_div_arb"><span dir="rtl">المدرسة </span><span class="red">*</span></div>
</div>

<div class="input_row even">
<div class="input_label_div"><span>Grade</span><span class="red">*</span> </div>
<div class="input_div"> <input type="text" name="grade" id="grade" class="input_cls"/>  </div>
<div class="input_label_div_arb"><span dir="rtl">المرحلة الدراسية</span><span class="red">*</span></div>
</div>


<div class="input_row odd">
<div class="input_label_div"><span>Percentage</span><span class="red">*</span> </div>
<div class="input_div"> <input type="text" name="percentage" id="percentage" class="input_cls"/>  </div>
<div class="input_label_div_arb"><span dir="rtl">المعدل الدراسي </span><span class="red">*</span></div>
</div>


<div class="input_row even">
<div class="input_label_div"><span>Parent's Mobile1 #</span><span class="red">*</span></div>
<div class="input_div"> <input type="text" class="input_cls" id="parent_mobile_1" name="parent_mobile_1">  </div>
<div class="input_label_div_arb"><span dir="rtl">رقم ولي الامر 1</span><span class="red">*</span></div>
</div>


<div class="input_row odd">
<div class="input_label_div"><span>Mobile2 #</span><span class="red">*</span></div>
<div class="input_div"> <input type="text" class="input_cls" id="parent_mobile_2" name="parent_mobile_2">  </div>
<div class="input_label_div_arb"><span dir="rtl">رقم ولي الامر 2</span><span class="red">*</span></div>
</div>


<div class="input_row even">
<div class="input_label_div"><span>Email:</span><span class="red">*</span></div>
<div class="input_div"> <input type="text" class="input_cls" id="email" name="email">  </div>
<div class="input_label_div_arb"><span dir="rtl">&#1575;&#1604;&#1576;&#1585;&#1610;&#1583; &#1575;&#1604;&#1575;&#1604;&#1603;&#1578;&#1585;&#1608;&#1606;&#1610;</span><span class="red">*</span></div>
</div>

<div class="input_row odd">
    <div class="input_label_div"><span>Would you like to use the bus transportation service?</span><span class="red">*</span></div>
    <div class="input_div iwidth" align="center">
        No<input type="radio" value="No لا" name="transportation_required" style="border:0;">   لا
        Yes
        <input type="radio" value="Yes" name="transportation_required" style="border:0;"> نعم</div>
    <div class="input_label_div_arb"><span dir="rtl">هل ترغب باستخدام خدمة المواصلات؟</span><span class="red">*</span></div>
</div>


<div class="input_row even">
<div class="input_label_div"><span>Residential Area</span><span class="red">*</span></div>
<div class="input_div"> <input type="text" name="residential_area" id="residential_area" class="input_cls"/>  </div>
<div class="input_label_div_arb"><span dir="rtl">المنطقة السكنية </span><span class="red">*</span></div>
</div>


<div class="input_row odd">
<div class="input_label_div"><span>Interests</span><span class="red">*</span></div>
<div class="input_div"> <input type="text" name="interests" id="job_title" class="input_cls"/>  </div>
<div class="input_label_div_arb">الهوايات <span class="red">*</span></div>
</div>


<div class="input_row even">
<div id="passport_status_msg"></div>
<div class="input_label_div"><span>Passport Copy</span><span class="red">*</span> </div>
<div class="input_div iwidth" style="width:270px;"> 
   <input type="text" value="" readonly="readonly" id="passport" name="passport" class="binput_cls">
	  <input type="hidden" id="passport_status" name="passport_status">
        <input type="button" style="width:75px; background:#E8EDE9;font-size:11px; padding:1px 0; direction:ltr;" id="passport_btn" name="passport_btn" value="Select / إختار">

</div>
<div class="input_label_div_arb" style="width:180px;"><span>صورة جواز سفر </span><span class="red">*</span></div>
</div>


<div class="input_row odd">
<div id="edu_certificate_status_msg"></div>
<div class="input_label_div" ><span>Latest Education Certificate</span><span class="red">*</span> </div>
<div class="input_div iwidth" style="width:270px;"> 
   <input type="text" value="" readonly="readonly" id="edu_certificate" name="edu_certificate" class="binput_cls">

	  <input type="hidden" id="edu_certificate_status" name="edu_certificate_status">
        <input type="button" style="width:75px; background:#E8EDE9; font-size:11px; padding:1px 0;direction:ltr;" id="edu_certificate_btn" name="edu_certificate_btn" value="Select / إختار">

</div>
<div class="input_label_div_arb" style="width:180px;" > <span>اخر شهادة دراسية </span><span class="red">*</span> </div>
</div>

<div class="input_row even">
<div id="photo_status_msg"></div>
<div class="input_label_div"><span>Passport size photo</span><span class="red">*</span> </div>
<div class="input_div iwidth" style="width:270px;"> 
   <input type="text" value="" readonly="readonly" id="photo" name="photo" class="binput_cls">
	  <input type="hidden" id="photo_status" name="photo_status">
        <input type="button" style="width:75px; background:#E8EDE9;font-size:11px; padding:1px 0;direction:ltr;" id="photo_btn" name="photo_btn" value="Select / إختار">

</div>
<div class="input_label_div_arb" style="width:180px;" >صورة شخصية حديثة<span class="red">*</span></div>
</div>

<div class="input_row odd">
<div id="family_book_status_msg"></div>
<div class="input_label_div"><span>Copy of Family Book</span><span class="red">*</span> </div>
<div class="input_div iwidth" style="width:270px;">
   <input type="text" value="" readonly="readonly" id="family_book" name="family_book" class="binput_cls">
	  <input type="hidden" id="family_book_status" name="family_book_status">
        <input type="button" style="width:75px; background:#E8EDE9;font-size:11px; padding:1px 0;direction:ltr;" id="family_book_btn" name="family_book_btn" value="Select / إختار">

</div>
<div class="input_label_div_arb" style="width:180px;" >صورة خلاصة القيد <span class="red">*</span></div>
</div>


<div class="input_row odd border_bottom">
<div class="clear" style="padding-top:10px;direction:ltr; text-align:center"><img src="captcha.php" id="captcha" style="background:#CCC;outline:none;
transition: all 0.25s ease-in-out;
-webkit-transition: all 0.25s ease-in-out;
-moz-transition: all 0.25s ease-in-out;
border: solid 2px #D7D9E5;
border-radius: 3px;
margin-left: 10px;
margin-bottom: 5px;
"><br>


<!-- CHANGE TEXT LINK -->
<a href="javascript:void(0)" onclick="
    document.getElementById('captcha').src='captcha.php?'+Math.random();
    document.getElementById('captcha-form').focus();" id="change-image" style="direction:ltr;">Not readable? Change text.</a><br><br>



 </div>
 <div class="clear input_label_div" style="float:left;width:250px; text-align:left; margin-top:10px;"><span style="float:left;width:230px;">Type the characters from the picture into <span style="float:left; text-align:left">the text box</span><span class="red">*</span></span> </div>
 <div class="captcha"><input type="text" name="captcha" id="captcha-form" autocomplete="off"></div>
 <div style="float:left;width:180px; text-align:right; margin-top:10px;" class="input_label_div_arb"><span dir="rtl">يرجى كتابة الاحرف الظاهرة في الصورة داخل المربع</span><span class="red">*</span></div>
</div>

<div class="input_row even border_bottom " >
  <div style="float:left;" > 



       <span> I verify the above information is correct  </span>

        


</div>
<input type="checkbox" name="agree" id="agree" value="1"  style="margin-left:120px; border:0;"/>
<div class="input_label_div_arb">أقر بأن جميع المعلومات السابقة صحيحة</div>
<div  class="clear" align="center">
<input type="submit" value="Submit &#65165;&#1585;&#1587;&#1604;" class="acloginbttn" style="direction:ltr;" />
</div>

</div>




</form>