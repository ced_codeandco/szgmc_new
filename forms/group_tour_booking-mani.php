<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" id="tourForm" name="tourForm" enctype="multipart/form-data">
<?php
$_countries = array(
 "GB" => "United Kingdom",
 "US" => "United States",
 "AF" => "Afghanistan",
 "AL" => "Albania",
 "DZ" => "Algeria",
 "AS" => "American Samoa",
 "AD" => "Andorra",
 "AO" => "Angola",
 "AI" => "Anguilla",
 "AQ" => "Antarctica",
 "AG" => "Antigua And Barbuda",
 "AR" => "Argentina",
 "AM" => "Armenia",
 "AW" => "Aruba",
 "AU" => "Australia",
 "AT" => "Austria",
 "AZ" => "Azerbaijan",
 "BS" => "Bahamas",
 "BH" => "Bahrain",
 "BD" => "Bangladesh",
 "BB" => "Barbados",
 "BY" => "Belarus",
 "BE" => "Belgium",
 "BZ" => "Belize",
 "BJ" => "Benin",
 "BM" => "Bermuda",
 "BT" => "Bhutan",
 "BO" => "Bolivia",
 "BA" => "Bosnia And Herzegowina",
 "BW" => "Botswana",
 "BV" => "Bouvet Island",
 "BR" => "Brazil",
 "IO" => "British Indian Ocean Territory",
 "BN" => "Brunei Darussalam",
 "BG" => "Bulgaria",
 "BF" => "Burkina Faso",
 "BI" => "Burundi",
 "KH" => "Cambodia",
 "CM" => "Cameroon",
 "CA" => "Canada",
 "CV" => "Cape Verde",
 "KY" => "Cayman Islands",
 "CF" => "Central African Republic",
 "TD" => "Chad",
 "CL" => "Chile",
 "CN" => "China",
 "CX" => "Christmas Island",
 "CC" => "Cocos (Keeling) Islands",
 "CO" => "Colombia",
 "KM" => "Comoros",
 "CG" => "Congo",
 "CD" => "Congo, The Democratic Republic Of The",
 "CK" => "Cook Islands",
 "CR" => "Costa Rica",
 "CI" => "Cote D'Ivoire",
 "HR" => "Croatia (Local Name: Hrvatska)",
 "CU" => "Cuba",
 "CY" => "Cyprus",
 "CZ" => "Czech Republic",
 "DK" => "Denmark",
 "DJ" => "Djibouti",
 "DM" => "Dominica",
 "DO" => "Dominican Republic",
 "TP" => "East Timor",
 "EC" => "Ecuador",
 "EG" => "Egypt",
 "SV" => "El Salvador",
 "GQ" => "Equatorial Guinea",
 "ER" => "Eritrea",
 "EE" => "Estonia",
 "ET" => "Ethiopia",
 "FK" => "Falkland Islands (Malvinas)",
 "FO" => "Faroe Islands",
 "FJ" => "Fiji",
 "FI" => "Finland",
 "FR" => "France",
 "FX" => "France, Metropolitan",
 "GF" => "French Guiana",
 "PF" => "French Polynesia",
 "TF" => "French Southern Territories",
 "GA" => "Gabon",
 "GM" => "Gambia",
 "GE" => "Georgia",
 "DE" => "Germany",
 "GH" => "Ghana",
 "GI" => "Gibraltar",
 "GR" => "Greece",
 "GL" => "Greenland",
 "GD" => "Grenada",
 "GP" => "Guadeloupe",
 "GU" => "Guam",
 "GT" => "Guatemala",
 "GN" => "Guinea",
 "GW" => "Guinea-Bissau",
 "GY" => "Guyana",
 "HT" => "Haiti",
 "HM" => "Heard And Mc Donald Islands",
 "VA" => "Holy See (Vatican City State)",
 "HN" => "Honduras",
 "HK" => "Hong Kong",
 "HU" => "Hungary",
 "IS" => "Iceland",
 "IN" => "India",
 "ID" => "Indonesia",
 "IR" => "Iran (Islamic Republic Of)",
 "IQ" => "Iraq",
 "IE" => "Ireland",
 "IL" => "Israel",
 "IT" => "Italy",
 "JM" => "Jamaica",
 "JP" => "Japan",
 "JO" => "Jordan",
 "KZ" => "Kazakhstan",
 "KE" => "Kenya",
 "KI" => "Kiribati",
 "KP" => "Korea, Democratic People's Republic Of",
 "KR" => "Korea, Republic Of",
 "KW" => "Kuwait",
 "KG" => "Kyrgyzstan",
 "LA" => "Lao People's Democratic Republic",
 "LV" => "Latvia",
 "LB" => "Lebanon",
 "LS" => "Lesotho",
 "LR" => "Liberia",
 "LY" => "Libyan Arab Jamahiriya",
 "LI" => "Liechtenstein",
 "LT" => "Lithuania",
 "LU" => "Luxembourg",
 "MO" => "Macau",
 "MK" => "Macedonia, Former Yugoslav Republic Of",
 "MG" => "Madagascar",
 "MW" => "Malawi",
 "MY" => "Malaysia",
 "MV" => "Maldives",
 "ML" => "Mali",
 "MT" => "Malta",
 "MH" => "Marshall Islands",
 "MQ" => "Martinique",
 "MR" => "Mauritania",
 "MU" => "Mauritius",
 "YT" => "Mayotte",
 "MX" => "Mexico",
 "FM" => "Micronesia, Federated States Of",
 "MD" => "Moldova, Republic Of",
 "MC" => "Monaco",
 "MN" => "Mongolia",
 "MS" => "Montserrat",
 "MA" => "Morocco",
 "MZ" => "Mozambique",
 "MM" => "Myanmar",
 "NA" => "Namibia",
 "NR" => "Nauru",
 "NP" => "Nepal",
 "NL" => "Netherlands",
 "AN" => "Netherlands Antilles",
 "NC" => "New Caledonia",
 "NZ" => "New Zealand",
 "NI" => "Nicaragua",
 "NE" => "Niger",
 "NG" => "Nigeria",
 "NU" => "Niue",
 "NF" => "Norfolk Island",
 "MP" => "Northern Mariana Islands",
 "NO" => "Norway",
 "OM" => "Oman",
 "PK" => "Pakistan",
 "PW" => "Palau",
 "PA" => "Panama",
 "PG" => "Papua New Guinea",
 "PY" => "Paraguay",
 "PE" => "Peru",
 "PH" => "Philippines",
 "PN" => "Pitcairn",
 "PL" => "Poland",
 "PT" => "Portugal",
 "PR" => "Puerto Rico",
 "QA" => "Qatar",
 "RE" => "Reunion",
 "RO" => "Romania",
 "RU" => "Russian Federation",
 "RW" => "Rwanda",
 "KN" => "Saint Kitts And Nevis",
 "LC" => "Saint Lucia",
 "VC" => "Saint Vincent And The Grenadines",
 "WS" => "Samoa",
 "SM" => "San Marino",
 "ST" => "Sao Tome And Principe",
 "SA" => "Saudi Arabia",
 "SN" => "Senegal",
 "SC" => "Seychelles",
 "SL" => "Sierra Leone",
 "SG" => "Singapore",
 "SK" => "Slovakia (Slovak Republic)",
 "SI" => "Slovenia",
 "SB" => "Solomon Islands",
 "SO" => "Somalia",
 "ZA" => "South Africa",
 "GS" => "South Georgia, South Sandwich Islands",
 "ES" => "Spain",
 "LK" => "Sri Lanka",
 "SH" => "St. Helena",
 "PM" => "St. Pierre And Miquelon",
 "SD" => "Sudan",
 "SR" => "Suriname",
 "SJ" => "Svalbard And Jan Mayen Islands",
 "SZ" => "Swaziland",
 "SE" => "Sweden",
 "CH" => "Switzerland",
 "SY" => "Syrian Arab Republic",
 "TW" => "Taiwan",
 "TJ" => "Tajikistan",
 "TZ" => "Tanzania, United Republic Of",
 "TH" => "Thailand",
 "TG" => "Togo",
 "TK" => "Tokelau",
 "TO" => "Tonga",
 "TT" => "Trinidad And Tobago",
 "TN" => "Tunisia",
 "TR" => "Turkey",
 "TM" => "Turkmenistan",
 "TC" => "Turks And Caicos Islands",
 "TV" => "Tuvalu",
 "UG" => "Uganda",
 "UA" => "Ukraine",
 "AE" => "United Arab Emirates",
 "UM" => "United States Minor Outlying Islands",
 "UY" => "Uruguay",
 "UZ" => "Uzbekistan",
 "VU" => "Vanuatu",
 "VE" => "Venezuela",
 "VN" => "Viet Nam",
 "VG" => "Virgin Islands (British)",
 "VI" => "Virgin Islands (U.S.)",
 "WF" => "Wallis And Futuna Islands",
 "EH" => "Western Sahara",
 "YE" => "Yemen",
 "YU" => "Yugoslavia",
 "ZM" => "Zambia",
 "ZW" => "Zimbabwe"
);
?>
    
	<div class="input_row even">
<div class="input_label_div"><span>Group booking category:</span><span class="red">*</span></div>
<div class="input_div">
<select class="input_cls" name="group_category" id="group_category" onchange="check_additional_fileds(this.options[this.selectedIndex].value);" >
<option value="">Please select - يرجي اختيار</option>
<optgroup label="----------------------------------------" > </optgroup>
<option value="General Publicarb">&#1586;&#1610;&#1575;&#1585;&#1577; &#1593;&#1575;&#1605;&#1607;</option>
<option value="General Public">General Public</option>
<optgroup label="----------------------------------------" > </optgroup>
<option value="Governmentarb">&#1575;&#1604;&#1607;&#1610;&#1574;&#1575;&#1578; &#1575;&#1604;&#1581;&#1603;&#1608;&#1605;&#1610;&#1577;</option>
<option value="Government">Government Entity</option>
<optgroup label="----------------------------------------" > </optgroup>
<option value="Embassyarb">&#1575;&#1604;&#1587;&#1601;&#1575;&#1585;&#1575;&#1578;</option>
<option value="Embassy">Embassy</option>
<optgroup label="----------------------------------------" > </optgroup>
<option value="Tour Operatorarb">&#1575;&#1604;&#1588;&#1585;&#1603;&#1575;&#1578; &#1575;&#1604;&#1587;&#1610;&#1575;&#1581;&#1610;&#1577;</option>
<option value="Tour Operator">Tour Operator</option>
<optgroup label="----------------------------------------" > </optgroup>
<option value="Educationarb">&#1575;&#1604;&#1605;&#1572;&#1587;&#1587;&#1575;&#1578; &#1575;&#1604;&#1578;&#1593;&#1604;&#1610;&#1605;&#1610;&#1577;</option>
<option value="Education">Educational Institution</option>
<optgroup label="----------------------------------------" > </optgroup>
<option value="Hotelarb">الفنادق</option>
<option value="Hotel">Hotel</option>
</select>
 </div>
 <div class="input_label_div_arb">&#1601;&#1574;&#1575;&#1578;&nbsp;&#1575;&#1604;&#1581;&#1580;&#1608;&#1586;&#1575;&#1578; &#1575;&#1604;&#1580;&#1605;&#1575;&#1593;&#1610;&#1577;<span class="red">*</span></div>
</div>







<div class="input_row country odd emb embdelegates" style=" display:none;">
<div class="input_label_div"><span style="text-align:left;">Names and Titles of delegates <br/> or officials</span><span class="red">*</span></div>
<div class="input_div">
<table id="dataTable" align="center">
<tr><td> <input type="text" name="delegates[]" class="input_cls" style="width:240px;" id="delegates"></td></tr>
</table>

	<a href="javascript:addRow('dataTable');" class="add_more_arb" >المزيد</a>
 <a href="javascript:addRow('dataTable');" class="add_more">Add More</a>&nbsp;</div>



 <div class="input_label_div_arb"> <span dir="RTL">الوفود ومناصبهم</span><span class="red">*</span> </div>

</div>

<div class="input_row even emb embphone" style=" display:none;">
<div class="input_label_div"><span>Embassy phone number</span><span class="red">*</span>
</div>
<div class="input_div">
    <input type="text" name="embassy_phone" class="input_cls" id="embassy_phone">
</div>
 <div class="input_label_div_arb"> <span dir="RTL">رقم هاتف السفارة</span><span class="red">*</span> </div>

</div>

<div class="input_row gov odd " style=" display:none;">
<div class="input_label_div"><span style="text-align:left;">Names and Titles of delegates <br/> or officials</span><span class="red">*</span></div>
<div class="input_div">
<table id="dataTable1" align="center">
<tr><td> <input type="text" name="gov_delegates[]" class="input_cls" style="width:240px;" ></td></tr>
</table>

	<a href="javascript:addRow1('dataTable1');" class="add_more_arb" >المزيد</a>
    <a href="javascript:addRow1('dataTable1');" class="add_more">Add More</a>&nbsp;</div>
 <div class="input_label_div_arb"> <span dir="RTL">الوفود ومناصبهم</span><span class="red">*</span> </div>

</div> 

<div class="input_row even gov" style=" display:none;">
<div class="input_label_div"><span>Organization/Company Name</span><span class="red">*</span>
</div>
<div class="input_div">
    <input dir="ltr" type="text"  name="gov_name" id="gov_name"  class="input_cls"/>
</div>
<div class="input_label_div_arb">اسم المؤسسة /الشركة<span class="red">*</span></div>

</div>

<div class="input_row odd gov" style="display:none;">
<div class="input_label_div"><span style="text-align:left">Organization/Company phone <br/>number</span><span class="red">*</span>
</div>
<div class="input_div">
    <input dir="ltr" type="text"  name="gov_phone" id="gov_phone"  class="input_cls"/>
</div>
<div class="input_label_div_arb">رقم هاتف المؤسسة/الشركة<span class="red">*</span></div>


</div>
<div class="input_row odd hotel" style="display:none;">
<div class="input_label_div"><span>Name of the hotel</span><span class="red">*</span>
</div>
<div class="input_div">
    <input dir="ltr" type="text"  name="hotel_name" id="hotel_name"  class="input_cls"/>
</div>
<div class="input_label_div_arb">اسم الفندق<span class="red">*</span></div>

</div> 

<div class="input_row even hotel" style="display:none;">
<div class="input_label_div"><span>Hotel phone number</span><span class="red">*</span>
</div>
<div class="input_div">
    <input dir="ltr" type="text"  name="hotel_phone" id="hotel_phone"  class="input_cls"/>
</div>
<div class="input_label_div_arb">رقم هاتف الفندق<span class="red">*</span></div>

</div>

<div class="input_row odd edu grade" >
<div class="input_label_div"><span>Grade(s)</span><span class="red">*</span> </div>
<div class="input_div">
<select class="input_cls" name="grade" id="grade" >
<option value="">Please select - يرجي اختيار</option>
<optgroup label="----------------------------------------" > </optgroup>
<option value="جامعة"><span dir="rtl">جامعة</span></option>
<option value="University">University</option>

<optgroup label="----------------------------------------" > </optgroup>
<option value="كلية"><span dir="rtl">كلية</span></option>
<option value="College">College</option>


<optgroup label="----------------------------------------" > </optgroup>
<option value="ثانوي"><span dir="rtl">ثانوي</span></option>
<option value="Secondary">Secondary</option>

<optgroup label="----------------------------------------" > </optgroup>
<option value="إعدادي"><span dir="rtl">إعدادي</span></option>
<option value="Preparatory">Preparatory</option>

<optgroup label="----------------------------------------" > </optgroup>
<option value="ابتدائي"><span dir="rtl">ابتدائي</span></option>
<option value="Primary">Primary</option>
<optgroup label="----------------------------------------" > </optgroup>
<option value="روضة">روضة</option>
<option value="Kindergarten">Kindergarten</option>

</select>
 </div>
 <div class="input_label_div_arb">&#1575;&#1604;&#1605;&#1587;&#1578;&#1608;&#1610;&#1575;&#1578;&nbsp;&#1575;&#1604;&#1583;&#1585;&#1575;&#1587;&#1610;&#1577;<span class="red">*</span></div>
</div>


<div class="input_row even edu org_name">
<div class="input_label_div"><span>Organization/Company Name</span><span class="red">*</span>
</div>
<div class="input_div">
    <input dir="ltr" type="text"  name="org_name" id="org_name"  class="input_cls"/>
</div>
<div class="input_label_div_arb">اسم المؤسسة /الشركة<span class="red">*</span></div>

</div>

<div class="input_row odd edu org_phone">
<div class="input_label_div"><span style="text-align:left">Organization/Company phone <br/>number</span><span class="red">*</span>
</div>
<div class="input_div">
    <input dir="ltr" type="text"  name="org_phone" id="org_phone"  class="input_cls"/>
</div>
<div class="input_label_div_arb">رقم هاتف المؤسسة/الشركة<span class="red">*</span></div>


</div>

<div class="input_row even edu gender">
<div class="input_label_div"><span>Gender</span><span class="red">*</span></div>
<div class="input_div">

<select class="input_cls" name="gender" id="gender" >
<option value="">Please select - يرجي اختيار</option>
<optgroup label="----------------------------------------" > </optgroup>
<option value="&#1605;&#1582;&#1578;&#1604;&#1591;" class="arb">&#1605;&#1582;&#1578;&#1604;&#1591;</option>
<option value="Both">Both</option>
<optgroup label="----------------------------------------" > </optgroup>
<option value="&#1573;&#1606;&#1575;&#1579;" class="arb">&#1573;&#1606;&#1575;&#1579;</option>
<option value="Female">Female</option>
<optgroup label="----------------------------------------" > </optgroup>
<option value="&#1584;&#1603;&#1608;&#1585;" class="arb">&#1584;&#1603;&#1608;&#1585;</option>
<option value="Male">Male</option>
</select>
 </div>
 <div class="input_label_div_arb">
 الجنس <span class="red">*</span></div>
</div>




	<div class="input_row country odd">
<div class="input_label_div"><span>Country:</span><span class="red">*</span></div>
<div class="input_div">
<select class="input_cls" name="country" id="country">
<option value="">Please select - يرجي اختيار</option>
<?php foreach ($_countries as $key => $value)
{
?>
<option value="<?php echo $value;?>"><?php echo $value;?></option>
<?php
}
?>

</select>
 </div>
<div class="input_label_div_arb">&#1575;&#1604;&#1576;&#1604;&#1583; <span class="red">*</span></div>
</div>

<div class="input_row even emirate">
<div class="input_label_div"><span>Emirate:</span></div>
<div class="input_div"> <input type="text" name="emirate" id="emirate" class="input_cls"/>  </div>
<div class="input_label_div_arb">
  <div>
    <div>الإمارة</div>
  </div>
</div>
</div> 


<div class="input_row odd tour_operator_div " style=" display:none;">
<div class="input_label_div"><span>Company name of the tour opertor:</span> <span class="red">*</span>
</div>

<div class="input_div">
    <input dir="ltr" type="text"  name="company_name" id="company_name"  class="input_cls"/>
</div>
<div class="input_label_div_arb">&#1575;&#1587;&#1605; &#1575;&#1604;&#1588;&#1585;&#1603;&#1577; &#1575;&#1604;&#1587;&#1610;&#1575;&#1581;&#1610;&#1577;<span class="red">*</span></div>
</div>

<div class="input_row even tour_operator_div " style=" display:none;">
<div class="input_label_div"><span>Phone no. of tour opertor:</span> <span class="red">*</span>
</div>

<div class="input_div">
    <input dir="ltr" type="text"  name="operator_phone" id="operator_phone"  class="input_cls"/>
</div>
<div class="input_label_div_arb">&#1585;&#1602;&#1605; &#1607;&#1575;&#1578;&#1601; &#1575;&#1604;&#1588;&#1585;&#1603;&#1577; &#1575;&#1604;&#1587;&#1610;&#1575;&#1581;&#1610;&#1577;<span class="red">*</span></div>
</div>

<div class="input_row odd tour_operator_div " style=" display:none;">
<div class="input_label_div"><span>Purpose of the visit:</span> <span class="red">*</span>
</div>

<div class="input_div">
    <select class="input_cls" name="purpose" id="purpose" >
<option value="">Please select - يرجي اختيار</option>
<optgroup label="----------------------------------------" > </optgroup>
<option value="تعليمي">تعليمي</option> 
<option value="Education">Education</option>
<optgroup label="----------------------------------------" > </optgroup>
<option value="سياحي">سياحي</option>
<option value="Leisure/Holiday maker">Leisure/Holiday maker</option>
<optgroup label="----------------------------------------" > </optgroup>
<option value="وفود مؤتمرات">وفود مؤتمرات</option>
<option value="Convention/Conference Delegate">Convention/Conference Delegate</option>
<optgroup label="----------------------------------------" > </optgroup>
<option value="&#1576;&#1608;&#1575;&#1582;&#1585; &#1587;&#1610;&#1575;&#1581;&#1610;&#1577;">&#1576;&#1608;&#1575;&#1582;&#1585; &#1587;&#1610;&#1575;&#1581;&#1610;&#1577;</option>
<option value="Curise Tourism">Curise Tourism</option>
</select>
</div>
<div class="input_label_div_arb">الهدف من الزيارة<span class="red">*</span></div>
</div>

<div class="input_row even tour_operator_div " style=" display:none;">
<div class="input_label_div"><span>Type of visit:</span> <span class="red">*</span>
</div>

<div class="input_div">
    <select class="input_cls" name="type" id="type" >
<option value="">Please select - يرجي اختيار</option>
<optgroup label="----------------------------------------" > </optgroup>
 <option value="جولة">جولة</option> 
<option value="Tour">Tour</option>
<optgroup label="----------------------------------------" > </optgroup>
 <option value="وقفة تصوير ">وقفة تصوير </option> 
<option value="Photo Stop">Photo Stop</option>
</select>
</div>
<div class="input_label_div_arb">الهدف من الزيارة<span class="red">*</span></div>
</div>

<div class="input_row odd language">
<div class="input_label_div"><span>Choose the language of your tour:</span><span class="red">*</span></div>
<div class="input_div">
<select class="input_cls" name="language" id="language">
<option value="">Please select - يرجي اختيار</option>
 <option value="عربي">عربي</option>
<option value="Arabic">Arabic</option>
<optgroup label="------------------------" > </optgroup>
<option value="إنجليزي">إنجليزي</option>
<option value="English">English</option>


</select>
 </div>
<div class="input_label_div_arb">&#1575;&#1582;&#1578;&#1610;&#1575;&#1585; &#1575;&#1604;&#1604;&#1594;&#1577; &#1575;&#1604;&#1605;&#1591;&#1604;&#1608;&#1576;&#1577; &#1604;&#1604;&#1580;&#1608;&#1604;&#1577;<span class="red">*</span></div>
</div>	
<div class="input_row even cname">
<div class="input_label_div"><span>Contact name (of person booking):</span><span class="red">*</span> </div>
<div class="input_div"> <input type="text" name="name" id="name" class="input_cls"/>  </div>
<div class="input_label_div_arb">
  <div>
    <div>الاسم (الشخص المختص بالحجز)
	<span class="red">*</span>
	</div>
  </div>
</div>
</div>

<div class="input_row email odd">
<div class="input_label_div"><span>Email (of person booking):</span><span class="red">*</span>
</div>
<div class="input_div">
    <input dir="ltr" type="text"  name="email" id="email"  class="input_cls"/>
</div>
<div class="input_label_div_arb">البريد الإلكتروني<span class="red">*</span></div>
</div>


<div class="input_row odd tour_operator_div " style=" display:none;">
<div class="input_label_div"><span>Name of Tourist Guide:</span> <span class="red">*</span>
</div>

<div class="input_div">
    <input dir="ltr" type="text"  name="guide_name" id="guide_name"  class="input_cls"/>
</div>
<div class="input_label_div_arb">إسم المرشد السياحي المرافق<span class="red">*</span></div>

</div> 

<div class="input_row even tour_operator_div " style=" display:none;">
<div class="input_label_div"><span>Mobile no. of Tourist Guide:</span> <span class="red">*</span>
</div>

<div class="input_div">
    <input dir="ltr" type="text"  name="guide_mobile" id="guide_mobile"  class="input_cls"/>
</div>
<div class="input_label_div_arb">رقم الهاتف النقال للمرشد المرافق<span class="red">*</span></div>
</div>


<div class="input_row even contact_person_group">
<div class="input_label_div"><span>Contact name of person <br/> </span> <span>accompanying the group:</span> <span class="red">*</span>
</div>

<div class="input_div">
    <input dir="ltr" type="text"  name="contact_person_group" id="contact_person_group"  class="input_cls"/>
</div>
<div class="input_label_div_arb">&#1575;&#1587;&#1605; &#1575;&#1604;&#1588;&#1582;&#1589; &#1575;&#1604;&#1584;&#1610; &#1610;&#1585;&#1575;&#1601;&#1602; &#1575;&#1604;&#1605;&#1580;&#1605;&#1608;&#1593;&#1577;<span class="red">*</span></div>
</div>


<div class="input_row odd m_number">
<div class="input_label_div"><span>Contact mobile number of person</span><span> accompanying the group: </span><span class="red">*</span></div>
<div class="input_div"> <input type="text" name="m_number" id="m_number" class="input_cls"/>  </div>
<div class="input_label_div_arb">رقم هاتف الشخص الذي سيرافق المجموعه <span class="red">*</span></div>
</div>





<div class="input_row even date_visit">
<div class="input_label_div"><span>Proposed date of visit:</span><span class="red">*</span></div>
<div class="input_div"> <input type="text" name="date_visit" id="date_visit" class="input_cls"   />  </div>

<div class="input_label_div_arb">&#1575;&#1604;&#1578;&#1575;&#1585;&#1610;&#1582; &#1575;&#1604;&#1605;&#1602;&#1578;&#1585;&#1581; &#1604;&#1604;&#1586;&#1610;&#1575;&#1585;&#1577;<span class="red">*</span></div>
</div>


    <div class="input_row odd prop_time">
        <div class="input_label_div"><span>Proposed time of visit:</span><span class="red">*</span>

        </div><div class="input_div iwidth"  >
		<div class="other">
        	<select name="prop_time" id="prop_time" class="input_cls_2" style="direction:ltr"  >
            	<option value="">Please select a date first</option>
               
<option value="09:00:00">9:00</option>
<option value="09:30:00">9:30</option>
<option value="10:00:00">10:00</option>
<option value="10:30:00">10:30</option>
<option value="11:00:00">11:00</option>
<option value="13:30:00">13:30</option>
<option value="14:00:00">14:00</option>
<option value="14:30:00">14:30</option>
<option value="16:30:00">16:30</option>
<option value="17:00:00">17:00</option>
<option value="17:15:00">17:15</option>
<option value="18:45:00">18:45</option>
<option value="19:00:00">19:00</option>
<option value="19:45:00">19:45</option>
<option value="20:30:00">20:30</option>
            </select>
		</div>
		<div class="fri">
		<select name="prop_time1" id="prop_time1" class="input_cls_2" style="direction:ltr" >
            
        </select> 
		
		</div>
		
		</div>
		<div class="input_label_div_arb">وقت الزيارة <span class="red">*</span></div>
    </div>


<div class="input_row even grp_size">
<div class="input_label_div"><span>Group Size:</span><span class="red">*</span>
</div>
<div class="input_div">
    <input dir="ltr" type="text"  name="grp_size" id="grp_size"  class="input_cls"/>
</div>
<div class="input_label_div_arb">&#1593;&#1583;&#1583;&nbsp;&#1571;&#1601;&#1585;&#1575;&#1583;&nbsp;&#1575;&#1604;&#1605;&#1580;&#1605;&#1608;&#1593;&#1577;<span class="red">*</span></div>
</div>

<div class="input_row even tour_operator_div " style=" display:none;">
<div class="input_label_div"><span>Number of bus:</span> <span class="red">*</span>
</div>

<div class="input_div">
    <input dir="ltr" type="text"  name="bus" id="bus"  class="input_cls"/>
</div>
<div class="input_label_div_arb">&#1593;&#1583;&#1583; &#1575;&#1604;&#1581;&#1575;&#1601;&#1604;&#1575;&#1578;<span class="red">*</span></div>
</div>

<div class="input_row odd library">
<div class="input_label_div ltr" style="width:230px; text-align:left;">Library visit to be included</div>
<div class="input_div iwidth" align="center" style="width:180px"> Yes <input type="radio" name="library" value="yes"  style="border:0;"> &#1606;&#1593;&#1605; 
&nbsp;&nbsp;&nbsp;&nbsp; No <input type="radio" name="library" value="no"  style="border:0;"> &#1604;&#1575; </div>
<div class="input_label_div_arb">&#1586;&#1610;&#1575;&#1585;&#1577; &#1575;&#1604;&#1605;&#1603;&#1578;&#1576;&#1577;</div>

</div>
<div class="input_row even needs">
<div class="input_label_div"><span>Specify any special interests or </span><span>needs for the group:
<img src="/img/questions.png" width="15" style="cursor:pointer; vertical-align:middle;" class="vtip" alt="?" title="<b>Please assist us in making your groups visit a memorable and enjoyable experience by providing us with some information. For example: </b>
<span>&bull; How many persons in your group, are they male/female/mixed?</span>
<span>&bull; Is this the group&rsquo;s first visit?</span>
<span>&bull; Where is the group from, what languages to they speak?</span>
<span>&bull; Do they have a particular interest (i.e. artists, engineers etc).</span>
<span>&bull; Are there special needs visitors?</span>"/>
</span>

</div>
<div class="input_div other_info">
    <textarea class="input_cls" name="other_info" style="width:240px;" id="other_info" cols="25" rows="8" ></textarea></div>
	<div class="input_label_div_arb">تحديد  أية اهتمامات أو احتياجات خاصة<span><img src="/img/questions.png" width="15" style="cursor:pointer; vertical-align:middle;" class="vtip1" alt="?" title="<b> &#1575;&#1604;&#1585;&#1580;&#1575;&#1569; &#1605;&#1587;&#1575;&#1593;&#1583;&#1578;&#1606;&#1575; &#1593;&#1604;&#1609; &#1580;&#1593;&#1604; &#1586;&#1610;&#1575;&#1585;&#1578;&#1603;&#1605; &#1575;&#1604;&#1580;&#1605;&#1575;&#1593;&#1610;&#1577; &#1578;&#1580;&#1585;&#1576;&#1577; &#1605;&#1605;&#1578;&#1593;&#1577; &#1608;&#1604;&#1575; &#1578;&#1606;&#1587;&#1609;&#1548; &#1593;&#1576;&#1585; &#1578;&#1586;&#1608;&#1610;&#1583;&#1606;&#1575; &#1576;&#1576;&#1593;&#1590; &#1575;&#1604;&#1605;&#1593;&#1604;&#1608;&#1605;&#1575;&#1578;. &#1593;&#1604;&#1609; &#1587;&#1576;&#1610;&#1604; &#1575;&#1604;&#1605;&#1579;&#1575;&#1604;:</b>
<span>&bull; &#1603;&#1605; &#1610;&#1576;&#1604;&#1594; &#1593;&#1583;&#1583; &#1571;&#1601;&#1585;&#1575;&#1583; &#1575;&#1604;&#1605;&#1580;&#1605;&#1608;&#1593;&#1577;&#1548; &#1607;&#1604; &#1607;&#1605; &#1584;&#1603;&#1608;&#1585;/&#1573;&#1606;&#1575;&#1579;/ &#1584;&#1603;&#1608;&#1585; &#1608;&#1573;&#1606;&#1575;&#1579; &#1605;&#1593;&#1575;&#1567;</span>
<span>&bull; &#1607;&#1604; &#1607;&#1584;&#1607; &#1607;&#1610; &#1575;&#1604;&#1586;&#1610;&#1575;&#1585;&#1577; &#1575;&#1604;&#1571;&#1608;&#1604;&#1609; &#1604;&#1604;&#1605;&#1580;&#1605;&#1608;&#1593;&#1577;&#1567;</span>
<span>&bull; &#1573;&#1604;&#1609; &#1571;&#1610; &#1576;&#1604;&#1583; &#1578;&#1606;&#1578;&#1605;&#1610; &#1607;&#1584;&#1607; &#1575;&#1604;&#1605;&#1580;&#1605;&#1608;&#1593;&#1577;&#1548; &#1605;&#1575; &#1607;&#1610; &#1575;&#1604;&#1604;&#1594;&#1577; &#1575;&#1604;&#1578;&#1610; &#1610;&#1578;&#1581;&#1583;&#1579;&#1608;&#1606; &#1576;&#1607;&#1575;&#1567;</span>
<span>&bull; &#1607;&#1604; &#1604;&#1583;&#1610;&#1607;&#1605; &#1575;&#1607;&#1578;&#1605;&#1575;&#1605;&#1575;&#1578; &#1582;&#1575;&#1589;&#1577; (&#1605;&#1579;&#1604;&#1575;: &#1601;&#1606;&#1575;&#1606;&#1608;&#1606;&#1548; &#1605;&#1607;&#1606;&#1583;&#1587;&#1608;&#1606;&#1548; &#1608;&#1605;&#1575; &#1573;&#1604;&#1609; &#1584;&#1604;&#1603;)</span>

<span>&bull; &#1607;&#1604; &#1610;&#1608;&#1580;&#1583; &#1576;&#1610;&#1606;&#1607;&#1605; &#1605;&#1606; &#1584;&#1608;&#1610; &#1575;&#1604;&#1575;&#1581;&#1578;&#1610;&#1575;&#1580;&#1575;&#1578; &#1575;&#1604;&#1582;&#1575;&#1589;&#1577;&#1567;</span>"/>
</span>
</div>
</div>


<div class="input_row odd border_bottom" >
 
<div align="center">
<div style="float:left;text-align:left"> 



       <span style="text-align:left">We kindly request that you follow the <br>  Mosque Manners during your visit Thank you. </span>

        


</div>
<input type="checkbox" name="agree" id="agree" value="1"  style=" border:0;"/>
<div class="input_label_div_arb">يرجى التقيد بآداب المسجد عند الزيارة</div>
<div  class="clear" align="center">
<input type="submit" value="Submit &#65165;&#1585;&#1587;&#1604;" class="acloginbttn" name="submitTour" id="submitTour" style="direction:ltr;" /></div>
</div>

</div>



</form>