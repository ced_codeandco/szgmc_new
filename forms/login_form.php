﻿<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" id="loginForm" name="loginForm" enctype="multipart/form-data">

    <div class="input_row even">
        	<div class="input_label_div">
            	<span>Username</span><span class="red">*</span>
            </div>
    		<div class="input_div"> 
            	<input type="text" name="username" id="username" class="input_cls"/>  
            </div>
    		<div class="input_label_div_arb">
      			<div>
        			<div>
                    	<span dir="rtl">&#1575;&#1587;&#1605; &#1575;&#1604;&#1605;&#1587;&#1578;&#1582;&#1583;&#1605;
      						<span class="red">*</span>
        				</span>
                    </div>
      			</div>
    		</div>
    </div>
     <div class="input_row odd">
        	<div class="input_label_div">
            	<span>Password</span><span class="red">*</span>
            </div>
    		<div class="input_div"> 
            	<input type="password" name="password" id="password" class="input_cls"/>  
            </div>
    		<div class="input_label_div_arb">
      			<div>
        			<div>
                    	<span dir="rtl">&#1603;&#1604;&#1605;&#1577; &#1575;&#1604;&#1605;&#1585;&#1608;&#1585;
      					<span class="red">*</span>	
        				</span>
                    </div>
      			</div>
    		</div>
    </div>
    
   
    <div class="input_row odd border_bottom " >
      <div  class="clear" align="center">
        <input type="submit" value="Login دخول"  name="submitLoginForm" id="submitLoginForm" class="acloginbttn" style="direction:ltr;" />
      </div> 
    </div>
</form>
