<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 7/13/2015
 * Time: 2:08 PM
 */

function addNotificationEmail($conn)
{
    if (!empty($_POST['add-email-notification']) && !empty($_POST['notification_type']) && $_POST['notification_type'] == 'JCG_NOTIFICATION') {
        if (empty($_POST['email'])) {
            $_SESSION['error'] = 'Please enter an email id';
        }


        $sql = "SELECT * FROM email_notification WHERE  `email` LIKE '$_POST[email]' AND `notification_type` = '$_POST[notification_type]' AND `status`='1' ";
        $query_result = mysql_query($sql, $conn);
        if (mysql_num_rows($query_result) > 0) {
            $_SESSION['error'] = 'email id already in use';
        } else {
            $sql = "INSERT INTO email_notification (`name`, `email`, `notification_type`, `email_type`, `added_by`, `updated_by`, `created_date_time`, `updated_date_time`) VALUES( '$_POST[name]', '$_POST[email]', '$_POST[notification_type]', '$_POST[email_type]', $_SESSION[uid], $_SESSION[uid], NOW(), NOW())";
            $query_result = mysql_query($sql, $conn);
            $_SESSION['success'] = 'Notification email added successfully';
            header('Location:configure-jcg-email.php');
            exit;
        }
    }
}
function deleteNotificationEmail($conn)
{
    if (!empty($_GET['action']) && !empty($_GET['id']) && $_GET['action'] == 'del') {
        $sql = "UPDATE email_notification SET status ='0' WHERE  `id` = '$_GET[id]' ";
        mysql_query($sql, $conn);
        $_SESSION['success'] = 'Notification email deleted successfully';
        header('Location:configure-jcg-email.php');
        exit;
    }
}

function getAllNotificationEmail($conn, $typeHandle)
{
    $sql = "SELECT  * FROM email_notification  WHERE  `status`='1' AND `notification_type` = '$typeHandle' order by created_date_time DESC  ";
    $query_result = mysql_query($sql, $conn);
    $result = array();
    while ($emailRow = mysql_fetch_object($query_result)) {
        $result[] = $emailRow;
    }

    return $result;
}


function getNotificationEmailsArray($conn, $typeHandle)
{
    $sql = "SELECT  * FROM email_notification  WHERE  `status`='1' AND `notification_type` = '$typeHandle' order by created_date_time DESC  ";
    $query_result = mysql_query($sql, $conn);
    $result = array();
    while ($emailRow = mysql_fetch_object($query_result)) {
        $result[$emailRow->email_type] .= ( !empty($result[$emailRow->email_type]) ? ',' : '').$emailRow->email;
    }

    return $result;
}
