<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" id="mediaForm" name="mediaForm" enctype="multipart/form-data">

    <div class="input_row even">
        <div class="input_label_div"><span> Position Applied For </span><span class="red">*</span></div>
        <div class="input_div">
            <?php
            include_once('loadVacancies.php');
            ?>
        </div>

        <div class="input_label_div_arb"><span dir="rtl">&#1575;&#1604;&#1608;&#1592;&#1610;&#1601;&#1577;  &#1575;&#1604;&#1605;&#1591;&#1604;&#1608;&#1576;&#1577;</span><span class="red">*</span></div>
    </div>
    <div class="input_row even qualificationsRes">
        <div class="input_label_div"><span>Qualifications </span> </div>
        <div class="input_div"> <span class="res">  </div>
        <div class="input_label_div_arb"><span dir="rtl">المؤهلات</span></div>
    </div>

    <div class="input_row odd">
        <div class="input_label_div"><span>Full Name </span><span class="red">*</span><span>(As per to Passport) </span></div>
        <div class="input_div"> <input type="text" name="a_fname" id="a_fname" class="input_cls"/>  </div>
        <div class="input_label_div_arb">
            <div>
                <div><span dir="rtl">&#1575;&#1604;&#1575;&#1587;&#1605;&nbsp;&#1575;&#1604;&#1603;&#1575;&#1605;&#1604;<br/>
    (&#1608;&#1601;&#1602; &#1580;&#1608;&#1575;&#1586;&nbsp;&#1575;&#1604;&#1587;&#1601;&#1585;)<span class="red">*</span></span></div>
            </div>
        </div>
    </div>


    <div class="input_row even">
        <div class="input_label_div"><span> Religion </span><span class="red">*</span></div>
        <div class="input_div"> <select id="religion" name="religion" class="input_cls">
                <option value="">Please select - يرجي اختيار</option>
                <optgroup label="--------------------------------------------"> </optgroup>
                <option value="&#1605;&#1587;&#1604;&#1605;">&#1605;&#1587;&#1604;&#1605;</option>
                <option value="Muslim">Muslim</option>
                <optgroup label="--------------------------------------------"> </optgroup>
                <option value="&#1571;&#1582;&#1585;&#1609;">&#1571;&#1582;&#1585;&#1609;</option>
                <option value="Other">Other</option>

            </select>  </div>
        <div class="input_label_div_arb"><span dir="rtl">&#1575;&#1604;&#1583;&#1610;&#1575;&#1606;&#1577;</span><span class="red">*</span></div>
    </div>
    <div class="input_row_dob odd">
        <div class="input_label_div"><span>Date of Birth</span><span class="red">*</span></div>
        <div class="input_div"> <input dir="ltr" type="text"  name="pub_date" id="pub_date" readonly="readonly"  class="input_cls"/>  </div>
        <div class="input_label_div_arb"><span dir="rtl">&#1578;&#1575;&#1585;&#1610;&#1582; &#1575;&#1604;&#1605;&#1610;&#1604;&#1575;&#1583;</span><span class="red">*</span></div>
    </div>
    <div class="input_row even">
        <div class="input_label_div"><span>Nationality </span><span class="red">*</span> </div>
        <div class="input_div"> <input type="text" name="a_nationality" id="a_nationality" class="input_cls"/>  </div>
        <div class="input_label_div_arb"><span dir="rtl">&#1575;&#1604;&#1580;&#1606;&#1587;&#1610;&#1577;</span><span class="red">*</span></div>
    </div>


    <div class="input_row odd">
        <div class="input_label_div"><span>Mobile</span><span class="red">*</span></div>
        <div class="input_div"> <input type="text" class="input_cls" id="mobile" name="mobile">  </div>
        <div class="input_label_div_arb"><span dir="rtl">&#1585;&#1602;&#1605; &#1575;&#1604;&#1605;&#1608;&#1576;&#1575;&#1610;&#1604;</span><span class="red">*</span></div>
    </div>





    <div class="input_row even">
        <div class="input_label_div"><span>Email Address </span><span class="red">*</span></div>
        <div class="input_div"> <input type="text" class="input_cls" id="c_email" name="c_email">  </div>
        <div class="input_label_div_arb"><span dir="rtl">&#1575;&#1604;&#1576;&#1585;&#1610;&#1583; &#1575;&#1604;&#1575;&#1604;&#1603;&#1578;&#1585;&#1608;&#1606;&#1610;</span><span class="red">*</span></div>
    </div>

    <div class="input_row odd">
        <div class="input_label_div"><span>Current Employer</span><span class="red">*</span></div>
        <div class="input_div"> <input type="text" name="employer" id="employer" class="input_cls"/>  </div>
        <div class="input_label_div_arb"><span dir="rtl">&#1575;&#1604;&#1608;&#1592;&#1610;&#1601;&#1577;  &#1575;&#1604;&#1581;&#1575;&#1604;&#1610;&#1577;</span><span class="red">*</span></div>
    </div>







    <div class="input_row even">
        <div class="input_label_div"><span>Current Job Title</span><span class="red">*</span></div>
        <div class="input_div"> <input type="text" name="job_title" id="job_title" class="input_cls"/>  </div>
        <div class="input_label_div_arb">المسمى الوظيفي الحالي<span class="red">*</span></div>
    </div>

    <div class="input_row odd">
        <div class="input_label_div"><span>Gender</span><span class="red">*</span></div>
        <div class="input_div iwidth" align="center">
            Male<input type="radio" value="male &#1584;&#1603;&#1585;" name="gender" style="border:0;">&#1584;&#1603;&#1585;&nbsp;&nbsp;&nbsp;&nbsp;Female<input type="radio" value="female &#1571;&#1606;&#1579;&#1609;" name="gender" style="border:0;">&#1571;&#1606;&#1579;&#1609;</div>
        <div class="input_label_div_arb"><span dir="rtl">&#1575;&#1604;&#1580;&#1606;&#1587;</span><span class="red">*</span></div>
    </div>

    <div class="input_row even">
        <div class="input_label_div"><span> Marital Status </span><span class="red">*</span></div>
        <div class="input_div"> <select id="martial" name="martial" class="input_cls">
                <option value="">Please select - يرجي اختيار</option>
                <optgroup label="--------------------------------------------"> </optgroup>
                <option value="&#1571;&#1593;&#1586;&#1576;">&#1571;&#1593;&#1586;&#1576;</option>
                <option value="Single">Single</option>
                <optgroup label="--------------------------------------------"> </optgroup>
                <option value="&#1605;&#1578;&#1586;&#1608;&#1580;">&#1605;&#1578;&#1586;&#1608;&#1580;</option>
                <option value="Married">Married</option>
                <optgroup label="--------------------------------------------"> </optgroup>
                <option value="مطلق">مطلق</option>
                <option value="Divorced">Divorced</option>
                <optgroup label="--------------------------------------------"> </optgroup>
                <option value="ارمل">ارمل</option>
                <option value="Divorced">Widow</option>
            </select>  </div>
        <div class="input_label_div_arb"><span dir="rtl">&#1575;&#1604;&#1581;&#1575;&#1604;&#1577;&nbsp;&#1575;&#1604;&#1575;&#1580;&#1578;&#1605;&#1575;&#1593;&#1610;&#1577;</span><span class="red">*</span></div>
    </div>

    <div class="special_dev white_bg" >
        <div class="input_row_special yellow p_left">
            <b>Educational Qualifications  </b>
            <div class="input_label_div_arb"><strong><span dir="rtl">&#1575;&#1604;&#1605;&#1572;&#1607;&#1604;&#1575;&#1578;  &#1575;&#1604;&#1583;&#1585;&#1575;&#1587;&#1610;&#1577;</span></strong> </div>
        </div>
        <table id="dataTable" align="center">
            <tr style="color:#666666; text-align:center font-size: 12px !important;" ><td style="font-size: 12px !important;"><span dir="rtl">&#1578;&#1575;&#1585;&#1610;&#1582;  &#1575;&#1604;&#1578;&#1582;&#1585;&#1580;</span></td>
                <td style="font-size: 12px !important;">الجامعة / الكلية + البلد </td>
                <td style="font-size: 12px !important;"><span dir="rtl">&#1575;&#1604;&#1578;&#1582;&#1589;&#1600;&#1589;</span></td>
                <td style="font-size: 12px !important;">&#1575;&#1604;&#1588;&#1607;&#1575;&#1583;&#1577; &#1575;&#1604;&#1593;&#1604;&#1605;&#1610;&#1577;</td> </tr>
            <tr style="color:#666666;text-align:center; font-size: 12px;"><td>Date of Graduation </td> <td>Country & Institution </td><td> Major </td> <td>Certification Attained </td> </tr>
            <tr><td><input type="text"  name="graduation[]"   ></td>
                <td> <input type="text" name="c_institution[]"  ></td>
                <td><input type="text"  name="major[]" > </td>
                <td><input type="text"  name="attained[]" ></td>
            </tr>
        </table>

        <div class="input_row">
            <a href="javascript:addRow('dataTable');" class="add_more_arb" style="float: right; margin-right: 28px;">المزيد</a>
            <a href="javascript:addRow('dataTable');" class="add_more">Add More</a>&nbsp;</div>


    </div>

    <div class="special_dev " >
        <div class="input_row_special yellow p_left">
            <b>Certified Training Courses  </b>
            <div class="input_label_div_arb"><strong><span dir="rtl">&#1575;&#1604;&#1588;&#1607;&#1575;&#1583;&#1575;&#1578;  &#1575;&#1604;&#1578;&#1582;&#1589;&#1589;&#1610;&#1577;</span></strong></div>
        </div>
        <table id="dataTable1" widtd="100%" align="center" style="margin-left: 100px !important;    margin-right: 100px !important;">
            <tr style="color:#666666; text-align:center; font-size: 12px !important;" align="center"><td style="font-size: 12px !important;" >المؤسسة</td>
                <td style="font-size: 12px !important;"><span dir="rtl">&#1575;&#1604;&#1578;&#1582;&#1589;&#1600;&#1589;</span></td>
                <td style="font-size: 12px !important;"><span dir="rtl">&#1575;&#1604;&#1588;&#1607;&#1575;&#1583;&#1577; &#1575;&#1604;&#1578;&#1582;&#1589;&#1589;&#1610;&#1577;</span></td>
            </tr>
            <tr style="color:#666666; text-align:center; font-size: 12px;" align="center"><td>Institution </td> <td>Major </td><td> Certificated Course </td>  </tr>
            <tr align="center"><td><input type="text"  name="institution[]"   ></td>
                <td> <input type="text" name="c_major[]"  ></td>
                <td><input type="text"  name="course[]" > </td>
            </tr>
        </table>

        <div class="input_row">
            <a href="javascript:addRow1('dataTable1');" class="add_more_arb" style="float: right; margin-right: 28px;">المزيد</a>
            <a href="javascript:addRow1('dataTable1');" class="add_more" style="margin-left: 30px;">Add More</a>&nbsp;</div>


    </div>

    <div class="special_dev white_bg" >
        <div class="input_row_special yellow p_left">
            <b>Languages  </b>
            <div class="input_label_div_arb"><strong><span dir="rtl">&#1575;&#1604;&#1604;&#1594;&#1575;&#1578;</span></strong></div>
        </div>
        <table id="dataTable2" widtd="100%" align="center" style="margin-left:12px;"  class="ltr">
            <tr style="color:#666666;text-align:center; font-size: 12px !important;"><td><span dir="rtl">&#1575;&#1604;&#1604;&#1594;&#1600;&#1600;&#1577;</span></td>
                <td><span dir="rtl">&#1603;&#1578;&#1575;&#1576;&#1577;</span></td>
                <td><span dir="rtl">&#1602;&#1585;&#1575;&#1569;&#1577;</span></td>
                <td><span dir="rtl">&#1605;&#1581;&#1575;&#1583;&#1579;&#1577;</span></td>
            </tr>
            <tr style="color:#666666;text-align:center; font-size: 12px;"><td>Language </td> <td>Writing </td><td> Reading </td> <td>Speaking </td> </tr>
            <tr><td class="row1"  style="text-align:left color:#666666 !important;text-align:center; font-size: 12px;">Arabic </td>
                <td> <select class="input_cls" name="warabic" id="warabic">
                        <option value="">Please select - يرجي اختيار</option>
                        <optgroup label="------------------------"> </optgroup>
                        <option value="&#1605;&#1605;&#1578;&#1575;&#1586;">&#1605;&#1605;&#1578;&#1575;&#1586;</option>
                        <option value="Excellent">Excellent</option>
                        <optgroup label="------------------------"> </optgroup>
                        <option value="&#1580;&#1610;&#1583;">&#1580;&#1610;&#1583;</option>
                        <option value="Good">Good</option>
                        <optgroup label="------------------------"> </optgroup>
                        <option value="&#1605;&#1602;&#1576;&#1608;&#1604;">&#1605;&#1602;&#1576;&#1608;&#1604;</option>
                        <option value="Acceptable">Acceptable</option>
                    </select></td>
                <td>  <select class="input_cls" name="rarabic" id="rarabic">
                        <option value="">Please select - يرجي اختيار</option>
                        <optgroup label="------------------------"> </optgroup>
                        <option value="&#1605;&#1605;&#1578;&#1575;&#1586;">&#1605;&#1605;&#1578;&#1575;&#1586;</option>
                        <option value="Excellent">Excellent</option>
                        <optgroup label="------------------------"> </optgroup>
                        <option value="&#1580;&#1610;&#1583;">&#1580;&#1610;&#1583;</option>
                        <option value="Good">Good</option>
                        <optgroup label="------------------------"> </optgroup>
                        <option value="&#1605;&#1602;&#1576;&#1608;&#1604;">&#1605;&#1602;&#1576;&#1608;&#1604;</option>
                        <option value="Acceptable">Acceptable</option>
                    </select></td>
                <td>  <select class="input_cls" name="sarabic" id="sarabic">
                        <option value="">Please select - يرجي اختيار</option>
                        <optgroup label="------------------------"> </optgroup>
                        <option value="&#1605;&#1605;&#1578;&#1575;&#1586;">&#1605;&#1605;&#1578;&#1575;&#1586;</option>
                        <option value="Excellent">Excellent</option>
                        <optgroup label="------------------------"> </optgroup>
                        <option value="&#1580;&#1610;&#1583;">&#1580;&#1610;&#1583;</option>
                        <option value="Good">Good</option>
                        <optgroup label="------------------------"> </optgroup>
                        <option value="&#1605;&#1602;&#1576;&#1608;&#1604;">&#1605;&#1602;&#1576;&#1608;&#1604;</option>
                        <option value="Acceptable">Acceptable</option>
                    </select></td>
                <td style="font-size: 12px !important;"><span dir="rtl">&#1593;&#1585;&#1576;&#1610;&#1577;</span> </td>
            </tr>

            <tr><td class="row1" style="text-align:left; color:#666666 ;text-align:center; font-size: 12px;">English </td>
                <td> <select class="input_cls" name="wenglish" id="wenglish">
                        <option value="">Please select - يرجي اختيار</option>
                        <optgroup label="------------------------"> </optgroup>
                        <option value="&#1605;&#1605;&#1578;&#1575;&#1586;">&#1605;&#1605;&#1578;&#1575;&#1586;</option>
                        <option value="Excellent">Excellent</option>
                        <optgroup label="--------------------------------------------"> </optgroup>
                        <option value="&#1580;&#1610;&#1583;">&#1580;&#1610;&#1583;</option>
                        <option value="Good">Good</option>
                        <optgroup label="--------------------------------------------"> </optgroup>
                        <option value="&#1605;&#1602;&#1576;&#1608;&#1604;">&#1605;&#1602;&#1576;&#1608;&#1604;</option>
                        <option value="Acceptable">Acceptable</option>
                    </select></td>
                <td>  <select class="input_cls" name="renglish" id="renglish">
                        <option value="">Please select - يرجي اختيار</option>
                        <optgroup label="------------------------"> </optgroup>
                        <option value="&#1605;&#1605;&#1578;&#1575;&#1586;">&#1605;&#1605;&#1578;&#1575;&#1586;</option>
                        <option value="Excellent">Excellent</option>
                        <optgroup label="--------------------------------------------"> </optgroup>
                        <option value="&#1580;&#1610;&#1583;">&#1580;&#1610;&#1583;</option>
                        <option value="Good">Good</option>
                        <optgroup label="--------------------------------------------"> </optgroup>
                        <option value="&#1605;&#1602;&#1576;&#1608;&#1604;">&#1605;&#1602;&#1576;&#1608;&#1604;</option>
                        <option value="Acceptable">Acceptable</option>
                    </select></td>
                <td>  <select class="input_cls" name="senglish" id="senglish">
                        <option value="">Please select - يرجي اختيار</option>
                        <optgroup label="------------------------"> </optgroup>
                        <option value="&#1605;&#1605;&#1578;&#1575;&#1586;">&#1605;&#1605;&#1578;&#1575;&#1586;</option>
                        <option value="Excellent">Excellent</option>
                        <optgroup label="--------------------------------------------"> </optgroup>
                        <option value="&#1580;&#1610;&#1583;">&#1580;&#1610;&#1583;</option>
                        <option value="Good">Good</option>
                        <optgroup label="--------------------------------------------"> </optgroup>
                        <option value="&#1605;&#1602;&#1576;&#1608;&#1604;">&#1605;&#1602;&#1576;&#1608;&#1604;</option>
                        <option value="Acceptable">Acceptable</option>
                    </select></td>
                <td style="font-size: 12px !important;"><span dir="rtl">&#1573;&#1606;&#1580;&#1604;&#1610;&#1586;&#1610;&#1577;</span> </td>
            </tr>
        </table>




    </div>

    <div class="special_dev" >
        <div class="input_row_special yellow p_left">
            <b>Employment Record </b>
            <div class="input_label_div_arb"><strong><span dir="rtl">&#1575;&#1604;&#1582;&#1576;&#1585;&#1577;  &#1575;&#1604;&#1593;&#1605;&#1604;&#1610;&#1577;&nbsp; </span></strong></div>
        </div>
        <table id="dataTable3" widtd="98%" align="center" class="ltr" >
            <tr style="color:#666666; text-align:center;font-size: 12px !important; "> <td style="padding-left:7px;"><span dir="rtl">&#1575;&#1604;&#1608;&#1592;&#1610;&#1601;&#1577;</span></td>
                <td style="padding-left:7px;"><span dir="rtl">&#1605;&#1600;&#1600;&#1606;</span></td>
                <td style="padding-left:7px;"><span dir="rtl">&#1573;&#1604;&#1600;&#1609;</span></td>

                <td style="padding-left:7px;"><span dir="rtl">&#1575;&#1587;&#1605;  &#1575;&#1604;&#1580;&#1607;&#1575;&#1578; &#1575;&#1604;&#1578;&#1610; &#1578;&#1593;&#1605;&#1604; / &#1593;&#1605;&#1604;&#1578; &#1576;&#1607;</span></td>
            </tr>
            <tr style="color:#666666; text-align:center; font-size: 12px;"> <td style="padding-left:7px;">Position </td> <td style="padding-left:7px;">From </td> <td style="padding-left:7px;"> To </td><td style="padding-left:7px;">Name of  Organization</td> </tr>
            <tr>
                <td> <input type="text" name="position[]"  ></td>

                <td><input type="text"  name="from[]" ></td>
                <td><input type="text"  name="to[]"  > </td>
                <td> <input type="text" name="organization[]"  ></td>
            </tr>
            <tr>
                <td style="text-align:left; padding-left:7px; font-size: 12px; widtd: 160px;"><br/> Reason for leaving tde job

                </td> <td></td><td></td><td style="text-align: right; font-size: 12px; padding-right: 11px;"> <br> &#1587;&#1576;&#1576; &#1578;&#1585;&#1603;  &#1575;&#1604;&#1593;&#1605;&#1604;</td> </tr>
            <tr>
                <td colspan="4" align="left">
                    <textarea rows="3" cols="55" id="reason"  name="reason[]" class="input_cls" style="widtd:650px;"></textarea>

                </td>


            </tr>
        </table>

        <div class="input_row">
            <a href="javascript:addRow2('dataTable3');" class="add_more_arb" style="float: right; margin-right: 28px;">المزيد</a>
            <a href="javascript:addRow2('dataTable3');" class="add_more">Add More</a>&nbsp;</div>
    </div>

    <div class="special_dev white_bg ">
        <div class="input_row_special ltr" style="margin-left:5px;width:230px;">Do you have relatives in the Center?</div>
        <div class="input_div iwidth" align="center"> Yes <input type="radio" name="relatives" value="yes" onclick="check_additional_fileds(this.value);" style="border:0;"> &#1606;&#1593;&#1605;
            &nbsp;&nbsp;&nbsp;&nbsp; No <input type="radio" name="relatives" value="no" onclick="check_additional_fileds(this.value);" style="border:0;"> &#1604;&#1575; </div>
        <div class="input_label_div_arb">هل لديك أقرباء في المركز<span dir="rtl">&#1567;</span></div>
        <div class="clear input_row_special " id="rela">
            <div class="input_label_div">Relative Name  </div>
            <div class="input_div">
                <input type="text" class="input_cls" id="rname" name="rname">
                <label style="display: none; color:#CC0000; direction:ltr; float:left; font-size:12px;" id="c_fname_error">*</label>
            </div>
            <div class="input_label_div_arb">اسم القريب</div>
        </div>
    </div>

    <!--<div class="input_row even">
        <div id="cv_upload_status_msg"></div>
        <div class="input_label_div"><span>CV</span><span class="red">*</span> </div>
        <div class="input_div iwidth" style="width:270px;">
            <input type="text" value="" readonly="readonly" id="a_cv_fname" name="a_cv_fname" class="binput_cls">
            <input type="hidden" id="a_cv" name="a_cv">
            <input type="button" style="width:75px; background:#E8EDE9;font-size:12px; padding:1px 0; direction:ltr;" id="a_cv_btn" name="a_cv_btn" value="Select / إختار">

        </div>
        <div class="input_label_div_arb" style="width:180px;"><span>&#1575;&#1604;&#1587;&#1610;&#1585;&#1577;&nbsp;&#1575;&#1604;&#1584;&#1575;&#1578;&#1610;&#1577;</span><span class="red">*</span></div>
    </div>-->
    <div class="input_row odd">
        <div id="a_cv_status_msg"></div>
        <div class="input_label_div"><span>CV</span><span class="red">*</span> </div>
        <div class="input_div iwidth" style="width:270px;">
            <input type="text" value="" readonly="readonly" id="a_cv" name="a_cv" class="binput_cls">
            <input type="hidden" value="" readonly="readonly" id="a_cv_fname" name="a_cv_fname" class="binput_cls">
            <input type="hidden" id="a_cv_status" name="a_cv_status">
            <input type="button" style="width:75px; background:#E8EDE9;font-size:11px; padding:1px 0; direction:ltr;" id="a_cv_btn" name="a_cv_btn" value="Select / إختار">

        </div>
        <div class="input_label_div_arb" style="width:180px;"><span>&#1575;&#1604;&#1587;&#1610;&#1585;&#1577;&nbsp;&#1575;&#1604;&#1584;&#1575;&#1578;&#1610;&#1577;</span><span class="red">*</span></div>
    </div>
    <div class="input_row even">
        <div id="a_pass_status_msg"></div>
        <div class="input_label_div"><span>Passport</span><span class="red">*</span> </div>
        <div class="input_div iwidth" style="width:270px;">
            <input type="text" value="" readonly="readonly" id="a_pass" name="a_pass" class="binput_cls">
            <input type="hidden" value="" readonly="readonly" id="a_pass_fname" name="a_pass_fname" class="binput_cls">
            <input type="hidden" id="a_pass_status" name="a_pass_status">
            <input type="button" style="width:75px; background:#E8EDE9;font-size:11px; padding:1px 0; direction:ltr;" id="a_pass_btn" name="a_pass_btn" value="Select / إختار">

        </div>
        <div class="input_label_div_arb" style="width:180px;"><span>&#1580;&#1608;&#1575;&#1586;&nbsp;&#1587;&#1601;&#1585;</span><span class="red">*</span></div>
    </div>
    <div class="input_row odd">
        <div id="a_certi_status_msg"></div>
        <div class="input_label_div"><span>Certificate</span><span class="red">*</span> </div>
        <div class="input_div iwidth" style="width:270px;">
            <input type="text" value="" readonly="readonly" id="a_certi" name="a_certi" class="binput_cls">
            <input type="hidden" value="" readonly="readonly" id="a_certi_fname" name="a_certi_fname" class="binput_cls">
            <input type="hidden" id="a_certi_status" name="a_certi_status">
            <input type="button" style="width:75px; background:#E8EDE9;font-size:11px; padding:1px 0; direction:ltr;" id="a_certi_btn" name="a_certi_btn" value="Select / إختار">
        </div>
        <div class="input_label_div_arb" style="width:180px;"><span>الشهادة</span><span class="red">*</span></div>
    </div>

    <div class="input_row even border_bottom">
        <div class="clear" style="padding-top:10px;direction:ltr; text-align:center"><img src="captcha.php" id="captcha" style="background:#CCC;outline:none;
transition: all 0.25s ease-in-out;
-webkit-transition: all 0.25s ease-in-out;
-moz-transition: all 0.25s ease-in-out;
border: solid 2px #D7D9E5;
border-radius: 3px;
margin-left: 10px;
margin-bottom: 5px;
"><br>


            <!-- CHANGE TEXT LINK -->
            <a href="javascript:void(0)" onclick="
    document.getElementById('captcha').src='captcha.php?'+Math.random();
    document.getElementById('captcha-form').focus();" id="change-image" style="direction:ltr;">Not readable? Change text.</a><br><br>



        </div>
        <div class="clear input_label_div" style="float:left;width:250px; text-align:left; margin-top:10px;"><span style="float:left;width:230px;">Type the characters from the picture into <span style="float:left; text-align:left">the text box</span><span class="red">*</span></span> </div>
        <div class="captcha"><input type="text" name="captcha" id="captcha-form" autocomplete="off"></div>
        <div style="float:left;width:180px; text-align:right; margin-top:10px;" class="input_label_div_arb"><span dir="rtl">يرجى كتابة الاحرف الظاهرة في الصورة داخل المربع</span><span class="red">*</span></div>
    </div>

    <div class="input_row odd border_bottom checkbox_div" >
        <div style="float:left;" class="input_label_div" >



            <span> I verify the above information is correct  </span>




        </div>
        <input type="checkbox" name="agree" id="agree" value="1"  style="margin-left:120px; border:0;"/>
        <div class="input_label_div_arb">أقر بأن جميع المعلومات السابقة صحيحة</div>
        <div  class="clear" align="center">
            <input type="submit" value="Submit &#65165;&#1585;&#1587;&#1604;" class="acloginbttn" style="direction:ltr;" />
        </div>

    </div>




</form>