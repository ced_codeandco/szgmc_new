<style type="text/css">
.ui-widget-header {
background: url("/images/ui-bg_gloss-wave_35_f6a828_500x100.png") repeat-x scroll 50% 50% #F6A828;
}
#ui-datepicker-div{ direction:ltr;}
</style>
<?php

/*Added and modifies by Irfan Ahmed on 7/11/2013*/

$userId=(isset($_SESSION['user_id']))?$_SESSION['user_id']:'';
if(isset($_SESSION['user_phoneno'])){
	
	$user_phoneno= $_SESSION['user_phoneno'];
	$user_fname= $_SESSION['user_fname'];
	$user_email=$_SESSION['user_email'];
	$user_countryCode=$_SESSION['user_countryCode'];
	$user_mobile=$_SESSION['user_mobile'];
}
else{
	if($userId!=''){
		$user_sql = "select * from site_users where id=$userId and active=1";
		$user_query = mysql_query($user_sql);
		while ($user_res = mysql_fetch_object($user_query)){
			$user_phoneno= $user_res->op_phoneno;
			$user_fname= $user_res->op_fname;
			$user_email= $user_res->op_email;
			$user_countryCode=$user_res->op_countryCode;
			$user_mobile=$user_res->op_mobile;
		}
	}
}
/*Ending */
?>
<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" id="tourForm" name="tourForm" enctype="multipart/form-data" >
<input type="hidden" name="agree" value="1">
<input type="hidden" name="user_id" id="user_id" value="<?php echo (isset($_SESSION['user_id']))?$_SESSION['user_id']:'';?>">
<div class="input_row even">
<div class="input_label_div"><span>Group booking category:</span><span class="red">*</span></div>
<div class="input_div">
<?php 
$choice=(isset($_SESSION['user_id']))?1:0;
//echo $choice;
?>
<select class="input_cls" name="group_category" id="group_category" onchange="check_additional_fileds(this.options[this.selectedIndex].value);" >
<option value="">Please select - يرجي اختيار</option>

<optgroup label="----------------------------------------" > </optgroup>
<option value="Governmentarb">&#1575;&#1604;&#1607;&#1610;&#1574;&#1575;&#1578; &#1575;&#1604;&#1581;&#1603;&#1608;&#1605;&#1610;&#1577;</option>
<option value="Government">Government Entity</option>
<optgroup label="----------------------------------------" > </optgroup>
<option value="Embassyarb">&#1575;&#1604;&#1587;&#1601;&#1575;&#1585;&#1575;&#1578;</option>
<option value="Embassy">Embassy</option>
<optgroup label="----------------------------------------" > </optgroup>
<option value="Tour Operatorarb">&#1575;&#1604;&#1588;&#1585;&#1603;&#1575;&#1578; &#1575;&#1604;&#1587;&#1610;&#1575;&#1581;&#1610;&#1577;</option>
<option value="Tour Operator" <?php if($choice==1){?>selected<?php } ?>>Tour Operator</option>
<optgroup label="----------------------------------------" > </optgroup>
<option value="Educationarb">&#1575;&#1604;&#1605;&#1572;&#1587;&#1587;&#1575;&#1578; &#1575;&#1604;&#1578;&#1593;&#1604;&#1610;&#1605;&#1610;&#1577;</option>
<option value="Education">Educational Institution</option>
<optgroup label="----------------------------------------" > </optgroup>
<option value="Hotelarb">الفنادق</option>
<option value="Hotel">Hotel</option>
<optgroup label="----------------------------------------" > </optgroup>
<option value="Corporatearb">القطاع الخاص</option>
<option value="Corporate">Corporate Sector</option>
</select>
 </div>
 <div class="input_label_div_arb">&#1601;&#1574;&#1575;&#1578;&nbsp;&#1575;&#1604;&#1581;&#1580;&#1608;&#1586;&#1575;&#1578; &#1575;&#1604;&#1580;&#1605;&#1575;&#1593;&#1610;&#1577;<span class="red">*</span></div>
</div>







<div class="input_row country odd emb embdelegates" style=" display:none;">
<div class="input_label_div"><span style="text-align:left;">Names and Titles of delegates <br/> or officials</span><span class="red">*</span></div>
<div class="input_div">
<table id="dataTable" align="center">
<tr><td> <input type="text" name="delegates[]" class="more_delegates input_cls" style="width:240px;font-weight:normal;direction:ltr;  font-size:12px;" id="delegates" value="Ambassador/M. Ahmad سفير/محمد أحمد"></td></tr>
</table>

	<a href="javascript:addRow('dataTable');" class="add_more_arb" >المزيد</a>
 <a href="javascript:addRow('dataTable');" class="add_more">Add More</a>&nbsp;</div>



 <div class="input_label_div_arb"> <span dir="RTL">الوفود ومناصبهم</span><span class="red">*</span> </div>

</div>

<div class="input_row even emb embphone" style=" display:none;">
<div class="input_label_div"><span>Embassy phone number</span><span class="red">*</span>
</div>
<div class="input_div">
    <input type="text" name="embassy_phone" class="input_cls" id="embassy_phone" value="<?php echo (isset($_SESSION['phoneno']))?$_SESSION['phoneno']:'';?>">
</div>
 <div class="input_label_div_arb"> <span dir="RTL">رقم هاتف السفارة</span><span class="red">*</span> </div>

</div>

<div class="input_row gov odd " style=" display:none;">
<div class="input_label_div"><span style="text-align:left;">Names and Titles of delegates <br/> or officials</span><span class="red">*</span></div>
<div class="input_div">
<table id="dataTable1" align="center">
<tr><td> <input type="text" name="gov_delegates[]" class="more_delegates1 input_cls" style="width:240px;normal;direction:ltr;  font-size:12px;" value="Director - مدير"></td></tr>
</table>

	<a href="javascript:addRow1('dataTable1');" class="add_more_arb" >المزيد</a>
    <a href="javascript:addRow1('dataTable1');" class="add_more">Add More</a>&nbsp;</div>
 <div class="input_label_div_arb"> <span dir="RTL">الوفود ومناصبهم</span><span class="red">*</span> </div>

</div> 

<div class="input_row even gov" style=" display:none;">
<div class="input_label_div"><span>Organization / Company Name</span><span class="red">*</span>
</div>
<div class="input_div">
    <input dir="ltr" type="text"  name="gov_name" id="gov_name"  class="input_cls" value="<?php echo (isset($_SESSION['company_name']))?$_SESSION['company_name']:'';?>"/>
</div>
<div class="input_label_div_arb">اسم المؤسسة /الشركة<span class="red">*</span></div>

</div>

<div class="input_row odd gov" style="display:none;">
<div class="input_label_div"><span style="text-align:left">Organization / Company phone <br/>number</span><span class="red">*</span>
</div>
<div class="input_div">
    <input dir="ltr" type="text"  name="gov_phone" id="gov_phone"  class="input_cls" value="<?php echo (isset($_SESSION['phoneno']))?$_SESSION['phoneno']:'';?>"/>
</div>
<div class="input_label_div_arb">رقم هاتف المؤسسة/الشركة<span class="red">*</span></div>


</div>
<div class="input_row odd hotel" style="display:none;">
<div class="input_label_div"><span>Name of the hotel</span><span class="red">*</span>
</div>
<div class="input_div">
    <input dir="ltr" type="text"  name="hotel_name" id="hotel_name"  class="input_cls" value="<?php echo (isset($_SESSION['company_name']))?$_SESSION['company_name']:'';?>"/>
</div>
<div class="input_label_div_arb">اسم الفندق<span class="red">*</span></div>

</div> 

<div class="input_row even hotel" style="display:none;">
<div class="input_label_div"><span>Hotel phone number</span><span class="red">*</span>
</div>
<div class="input_div">
    <input dir="ltr" type="text"  name="hotel_phone" id="hotel_phone"  class="input_cls" value="<?php echo (isset($_SESSION['phoneno']))?$_SESSION['phoneno']:'';?>"/>
</div>
<div class="input_label_div_arb">رقم هاتف الفندق<span class="red">*</span></div>

</div>

<div class="input_row odd edu grade" >
<div class="input_label_div"><span>Grade(s)</span><span class="red">*</span> </div>
<div class="input_div">
<select class="input_cls" name="grade" id="grade" >
<option value="">Please select - يرجي اختيار</option>
<optgroup label="----------------------------------------" > </optgroup>
<option value="جامعة"><span dir="rtl">جامعة</span></option>
<option value="University">University</option>

<optgroup label="----------------------------------------" > </optgroup>
<option value="كلية"><span dir="rtl">كلية</span></option>
<option value="College">College</option>


<optgroup label="----------------------------------------" > </optgroup>
<option value="ثانوي"><span dir="rtl">ثانوي</span></option>
<option value="Secondary">Secondary</option>

<optgroup label="----------------------------------------" > </optgroup>
<option value="إعدادي"><span dir="rtl">إعدادي</span></option>
<option value="Preparatory">Preparatory</option>

<optgroup label="----------------------------------------" > </optgroup>
<option value="ابتدائي"><span dir="rtl">ابتدائي</span></option>
<option value="Primary">Primary</option>
<optgroup label="----------------------------------------" > </optgroup>
<option value="روضة">روضة</option>
<option value="Kindergarten">Kindergarten</option>

</select>
 </div>
 <div class="input_label_div_arb">المستوى الدراسي<span class="red">*</span></div>
</div>


<div class="input_row even edu org_name">
<div class="input_label_div"><span>Organization / Company Name</span><span class="red">*</span>
</div>
<div class="input_div">
    <input dir="ltr" type="text"  name="org_name" id="org_name"  class="input_cls" value="<?php echo (isset($_SESSION['company_name']))?$_SESSION['company_name']:'';?>"/>
</div>
<div class="input_label_div_arb">اسم المؤسسة /الشركة<span class="red">*</span></div>

</div>

<div class="input_row odd edu org_phone">
<div class="input_label_div"><span style="text-align:left">Organization / Company phone <br/>number</span><span class="red">*</span>
</div>
<div class="input_div">
    <input dir="ltr" type="text"  name="org_phone" id="org_phone"  class="input_cls" value="<?php echo (isset($_SESSION['phoneno']))?$_SESSION['phoneno']:'';?>"/>
</div>
<div class="input_label_div_arb">رقم هاتف المؤسسة/الشركة<span class="red">*</span></div>



</div>

<div class="input_row even edu gender">
<div class="input_label_div"><span>Gender</span><span class="red">*</span></div>
<div class="input_div">

<select class="input_cls" name="gender" id="gender" >
<option value="">Please select - يرجي اختيار</option>
<optgroup label="----------------------------------------" > </optgroup>
<option value="&#1605;&#1582;&#1578;&#1604;&#1591;" class="arb">&#1605;&#1582;&#1578;&#1604;&#1591;</option>
<option value="Both">Both</option>
<optgroup label="----------------------------------------" > </optgroup>
<option value="&#1573;&#1606;&#1575;&#1579;" class="arb">&#1573;&#1606;&#1575;&#1579;</option>
<option value="Female">Female</option>
<optgroup label="----------------------------------------" > </optgroup>
<option value="&#1584;&#1603;&#1608;&#1585;" class="arb">&#1584;&#1603;&#1608;&#1585;</option>
<option value="Male">Male</option>
</select>
 </div>
 <div class="input_label_div_arb">
 الجنس <span class="red">*</span></div>
</div>




<div class="input_row country odd">
<div class="input_label_div"><span>Country:</span><span class="red">*</span></div>
<div class="input_div">
<?php
$country_sql = "select * from country_t order by eng_name";
$country_query = mysql_query($country_sql);
?>
<select class="input_cls" name="country" id="country" onchange="check_additional_fileds1(this.options[this.selectedIndex].value);">
<option value="">Please select - يرجي اختيار</option>
<optgroup label="----------------------------------------" > </optgroup>
<option value="United Arab Emiratesarb" >الإمارات العربية المتحدة</option>
<option value="United Arab Emirates" >United Arab Emirates</option>
<?php while ($_countries_res = mysql_fetch_object($country_query))
{
?>
<optgroup label="----------------------------------------" > </optgroup>
<option value="<?php echo $_countries_res->arb_name;?>"><?php echo $_countries_res->arb_name;?></option>

<option value="<?php echo $_countries_res->eng_name;?>"><?php echo $_countries_res->eng_name;?></option>
<?php
}
?>
<optgroup label="----------------------------------------" > </optgroup>
<option value="أخرى" >أخرى</option>
<option value="Other" >Other</option>
</select>
 </div>
<div class="input_label_div_arb">&#1575;&#1604;&#1576;&#1604;&#1583; <span class="red">*</span></div>
</div>

<div class="input_row even emirate" style="display:none">
<div class="input_label_div"><span>Emirate:</span><span class="red">*</span></div>
<div class="input_div"> <select class="input_cls" name="emirate" id="emirate" >
<option value="">Please select - يرجي اختيار</option>
<optgroup label="----------------------------------------" > </optgroup>
<option value="&#1571;&#1576;&#1608; &#1592;&#1576;&#1610;" class="arb"><span dir="RTL">&#1571;&#1576;&#1608; &#1592;&#1576;&#1610;</span></option>
<option value="Abu Dhabi">Abu Dhabi</option>
<optgroup label="----------------------------------------" > </optgroup>
<option value="&#1583;&#1576;&#1610;" class="arb"><span dir="RTL">&#1583;&#1576;&#1610;</span></option>
<option value="Dubai">Dubai</option>
<optgroup label="----------------------------------------" > </optgroup>
<option value="&#1575;&#1604;&#1588;&#1575;&#1585;&#1602;&#1577" class="arb"><span dir="LTR">&#1575;&#1604;&#1588;&#1575;&#1585;&#1602;&#1577;</span></option>
<option value="Sharjah">Sharjah</option>
<optgroup label="----------------------------------------" > </optgroup>
<option value="&#1585;&#1571;&#1587; &#1575;&#1604;&#1582;&#1610;&#1605;&#1577;" class="arb"><span dir="RTL">&#1585;&#1571;&#1587; &#1575;&#1604;&#1582;&#1610;&#1605;&#1577;</span>
</option>
<option value="Ras Al Khaimah">Ras Al Khaimah</option>
<optgroup label="----------------------------------------" > </optgroup>
<option value="&#1575;&#1604;&#1601;&#1580;&#1610;&#1585;&#1577;" class="arb"><span dir="RTL">&#1575;&#1604;&#1601;&#1580;&#1610;&#1585;&#1577;</span></option>

<option value="Fujairah">Fujairah</option>
<optgroup label="----------------------------------------" > </optgroup>
<option value="&#1571;&#1605; &#1575;&#1604;&#1602;&#1610;&#1608;&#1610;&#1606;" class="arb"><span dir="RTL">&#1571;&#1605; &#1575;&#1604;&#1602;&#1610;&#1608;&#1610;&#1606;</span></option>
<option value="Umm Al Quwain">Umm Al Quwain</option>
<optgroup label="----------------------------------------" > </optgroup>
<option value="&#1593;&#1580;&#1605;&#1575;&#1606;" class="arb"><span dir="RTL">&#1593;&#1580;&#1605;&#1575;&#1606;</span></option>
<option value="Ajman">Ajman</option>
</select>
  </div>
<div class="input_label_div_arb">
  <div>
    <div>الإمارة<span class="red">*</span></div>
  </div>
</div>
</div> 


<div class="input_row odd tour_operator_div " style=" display:none;">
<div class="input_label_div"><span>Company name of the tour operator:</span> <span class="red">*</span>
</div>

<div class="input_div">
		<?php if(isset($_SESSION['user_id']) && $_SESSION['user_id']!=''){ ?>
			<input dir="ltr" type="text"  name="company_name" id="company_name"  class="input_cls" value="<?php echo (isset($_SESSION['company_name']))?$_SESSION['company_name']:'';?>" readonly="readonly"/>
		<?php }else{ ?>
    <input dir="ltr" type="text"  name="company_name" id="company_name"  class="input_cls" value=""/>
    	<?php } ?>
</div>
<div class="input_label_div_arb">&#1575;&#1587;&#1605; &#1575;&#1604;&#1588;&#1585;&#1603;&#1577; &#1575;&#1604;&#1587;&#1610;&#1575;&#1581;&#1610;&#1577;<span class="red">*</span></div>
</div>

<div class="input_row even tour_operator_div " style=" display:none;">
<div class="input_label_div"><span>Phone No. of tour operator:</span> <span class="red">*</span>
</div>

<div class="input_div">
    <input dir="ltr" type="text"  name="operator_phone" id="operator_phone"  class="input_cls" value="<?php echo (isset($user_phoneno))?$user_phoneno:'';?>"/>
</div>
<div class="input_label_div_arb">&#1585;&#1602;&#1605; &#1607;&#1575;&#1578;&#1601; &#1575;&#1604;&#1588;&#1585;&#1603;&#1577; &#1575;&#1604;&#1587;&#1610;&#1575;&#1581;&#1610;&#1577;<span class="red">*</span></div>
</div>

<div class="input_row odd tour_operator_div " style=" display:none;">
<div class="input_label_div"><span>Purpose of the visit:</span> <span class="red">*</span>
</div>

<div class="input_div">
    <select class="input_cls" name="purpose" id="purpose" >
<option value="">Please select - يرجي اختيار</option>
<optgroup label="----------------------------------------" > </optgroup>
<option value="تعليمي">تعليمي</option> 
<option value="Education">Education</option>
<optgroup label="----------------------------------------" > </optgroup>
<option value="سياحي">سياحي</option>
<option value="Leisure/Holiday maker">Leisure/Holiday</option>
<optgroup label="----------------------------------------" > </optgroup>
<option value="وفود مؤتمرات">وفود مؤتمرات</option>
<option value="Convention/Conference Delegate">Convention/Conference Delegate</option>
<optgroup label="----------------------------------------" > </optgroup>
<option value="&#1576;&#1608;&#1575;&#1582;&#1585; &#1587;&#1610;&#1575;&#1581;&#1610;&#1577;">&#1576;&#1608;&#1575;&#1582;&#1585; &#1587;&#1610;&#1575;&#1581;&#1610;&#1577;</option>
<option value="Cruise Ship">Cruise Ship</option>
</select>
</div>
<div class="input_label_div_arb">الهدف من الزيارة<span class="red">*</span></div>
</div>

<div class="input_row even tour_operator_div " style=" display:none;">
<div class="input_label_div"><span>Type of visit:</span> <span class="red">*</span>
</div>

<div class="input_div">
    <select class="input_cls" name="type" id="type" >
<option value="">Please select - يرجي اختيار</option>
<optgroup label="----------------------------------------" > </optgroup>
 <option value="جولة">جولة</option> 
<option value="Tour">Tour</option>
<optgroup label="----------------------------------------" > </optgroup>
 <option value="وقفة تصوير ">وقفة تصوير </option> 
<option value="Photo Stop">Photo Stop</option>
</select>
</div>
<div class="input_label_div_arb">نوع الزيارة<span class="red">*</span></div>
</div>

<div class="input_row odd language">
<div class="input_label_div"><span>Choose the language of your tour:</span><span class="red">*</span></div>
<div class="input_div">
<select class="input_cls" name="language" id="language">
<option value="">Please select - يرجي اختيار</option>
 <option value="عربي">عربي</option>
<option value="Arabic">Arabic</option>
<optgroup label="------------------------" > </optgroup>
<option value="إنجليزي">إنجليزي</option>
<option value="English">English</option>


</select>
 </div>
<div class="input_label_div_arb">&#1575;&#1582;&#1578;&#1610;&#1575;&#1585; &#1575;&#1604;&#1604;&#1594;&#1577; &#1575;&#1604;&#1605;&#1591;&#1604;&#1608;&#1576;&#1577; &#1604;&#1604;&#1580;&#1608;&#1604;&#1577;<span class="red">*</span></div>
</div>	
<div class="input_row even cname">
<div class="input_label_div"><span>Contact name (of person booking):</span><span class="red">*</span> </div>
<div class="input_div"> <input type="text" name="name" id="name" class="input_cls" value="<?php echo (isset($user_fname))?$user_fname:'';?>"/>  </div>
<div class="input_label_div_arb">
  <div>
    <div>الاسم (الشخص المختص بالحجز)
	<span class="red">*</span>
	</div>
  </div>
</div>
</div>

<div class="input_row email odd">
<div class="input_label_div"><span>Email (of person booking):</span><span class="red">*</span>
</div>
<div class="input_div">
    <input dir="ltr" type="text"  name="email" id="email"  class="input_cls" value="<?php echo (isset($user_email))?$user_email:'';?>"/>
</div>
<div class="input_label_div_arb">البريد الإلكتروني<br/> (الشخص المختص بالحجز)<span class="red">*</span></div>
</div>


<div class="input_row odd tour_operator_div " style=" display:none;">
<div class="input_label_div"><span>Name of Tourist Guide:</span> <span class="red">*</span>
</div>

<div class="input_div">
    <input dir="ltr" type="text"  name="guide_name" id="guide_name"  class="input_cls"/>
</div>
<div class="input_label_div_arb">إسم المرشد السياحي المرافق<span class="red">*</span></div>

</div> 

<div class="input_row even tour_operator_div " style=" display:none;">
<div class="input_label_div"><span>Mobile No. of Tourist Guide:</span> <span class="red">*</span>
</div>


<div class="input_div">
    <div class="cont">
		<div class="contCode txtCode">
        <?php 
		$code= (isset($user_countryCode))?substr($user_countryCode,3):'';
	
		?>
			<span>+</span><input type="text"  style="width: 35px;border: 1px solid #CEBA69; direction:ltr" name="gcountryCode" id="countryCode" value="971" readonly="readonly">
            <select name="gareacode"><option value="50" <?php if($code==50){?>selected<?php } ?>>50</option><option value="55" <?php if($code==55){?>selected<?php } ?>>55</option><option value="52" <?php if($code==52){?>selected<?php } ?>>52</option><option value="56" <?php if($code==56){?>selected<?php } ?>>56</option> </select>
			<!--<span class="txtEng">(country code)</span>
			<span class="txtArabic">(كود البلد)</span>-->
		</div>
		<div class="contCode txtNum">
			<!--<span>-</span>--><input type="text" style="width: 146px;border: 1px solid #CEBA69;" name="guide_mobile" id="guide_mobile" value="<?php echo (isset($user_mobile))?$user_mobile:'';?>">
			<!--<span class="txtEng">(mobile no, eg +971 503166350)</span>-->
			<!--<span class="txtArabic">(رقم الهاتف النقال)</span>-->
		</div>
	</div>
    </div>
<div class="input_label_div_arb">رقم الهاتف النقال للمرشد المرافق<span class="red">*</span></div>
</div>


<div class="input_row even contact_person_group">
<div class="input_label_div"><span>Contact name of person <br/> </span> <span>accompanying the group:</span> <span class="red">*</span>
</div>

<div class="input_div">
    <input dir="ltr" type="text"  name="contact_person_group" id="contact_person_group"  class="input_cls"/>
</div>
<div class="input_label_div_arb">&#1575;&#1587;&#1605; &#1575;&#1604;&#1588;&#1582;&#1589; &#1575;&#1604;&#1584;&#1610; &#1610;&#1585;&#1575;&#1601;&#1602; &#1575;&#1604;&#1605;&#1580;&#1605;&#1608;&#1593;&#1577;<span class="red">*</span></div>
</div>

<div class="input_row m_number odd">
<div class="input_label_div"><span>Contact mobile number of person</span><span> accompanying the group: </span><span class="red">*</span></div>

    <div class="input_div"><div class="cont">
		<div class="contCode txtCode">
        <?php 
		$code= (isset($_SESSION['countryCode']))?substr($_SESSION['countryCode'],3):'';
	
		?>
			<span>+</span><input type="text"  style="width: 35px;border: 1px solid #CEBA69; direction:ltr" name="countryCode" id="countryCode" value="971" readonly="readonly">
            <select name="areacode"><option value="50" <?php if($code==50){?>selected<?php } ?>>50</option><option value="55" <?php if($code==55){?>selected<?php } ?>>55</option><option value="52" <?php if($code==52){?>selected<?php } ?>>52</option><option value="56" <?php if($code==56){?>selected<?php } ?>>56</option> </select>
			<!--<span class="txtEng">(country code)</span>
			<span class="txtArabic">(كود البلد)</span>-->
		</div>
		<div class="contCode txtNum">
			<!--<span>-</span>--><input type="text" style="width: 146px;border: 1px solid #CEBA69;" name="m_number" id="m_number"value="<?php echo (isset($_SESSION['mobile']))?$_SESSION['mobile']:'';?>">
			<!--<span class="txtEng">(mobile no, eg +971 503166350)</span>-->
			<!--<span class="txtArabic">(رقم الهاتف النقال)</span>-->
		</div>
	</div></div>
<div class="input_label_div_arb">رقم هاتف الشخص الذي سيرافق المجموعة <span class="red">*</span></div>
</div>



<div class="input_row even date_visit">
<div class="input_label_div"><span>Proposed date of visit:</span><span class="red">*</span></div>
<div class="notranslate input_div"> <input type="text" name="date_visit" id="date_visit" class="input_cls notranslate"  style="direction:ltr" readonly="readonly"   />  </div>

<div class="input_label_div_arb">&#1575;&#1604;&#1578;&#1575;&#1585;&#1610;&#1582; &#1575;&#1604;&#1605;&#1602;&#1578;&#1585;&#1581; &#1604;&#1604;&#1586;&#1610;&#1575;&#1585;&#1577;<span class="red">*</span></div>
</div>


    <div class="input_row odd prop_time">
        <div class="input_label_div"><span>Proposed time of visit:</span><span class="red">*</span>

        </div>
        <div class="input_div iwidth"  >
            <div class="other">
                <select name="prop_time" id="prop_time" class="input_cls_2" style="direction:ltr; float:left"  >
                    <option value="">Please select a date first</option>
                </select>
                <div class="time_slot_availability" style="display:inline; float:left; padding-top: 3px; padding-left: 10px;"></div>
            </div>
        </div>
        <div class="input_label_div_arb">وقت الزيارة <span class="red">*</span></div>
	</div>


<div class="input_row even grp_size">
<div class="input_label_div"><span>Group Size:</span><span class="red">*</span>
</div>
<div class="input_div">
    <input dir="ltr" type="text"  name="grp_size" id="grp_size"  class="input_cls" style="width:50px;"/>
    <div class="group_slot_availability" style="display:inline; float:left; padding-top: 3px; padding-left: 10px;"></div>
</div>
<div class="input_label_div_arb">&#1593;&#1583;&#1583;&nbsp;&#1571;&#1601;&#1585;&#1575;&#1583;&nbsp;&#1575;&#1604;&#1605;&#1580;&#1605;&#1608;&#1593;&#1577;<span class="red">*</span></div>
</div>

<div class="input_row even grp_size1"  style="display:none">
<div class="input_label_div"><span>Group Size:</span><span class="red">*</span>
</div>
<div class="input_div">
    <input dir="ltr" type="text"  name="grp_size1" id="grp_size1"  class="input_cls" style="width:50px;"/>
     <div class="group_slot_availability1" style="display:inline; float:left; padding-top: 3px; padding-left: 10px;"></div>
</div>
<div class="input_label_div_arb">&#1593;&#1583;&#1583;&nbsp;&#1571;&#1601;&#1585;&#1575;&#1583;&nbsp;&#1575;&#1604;&#1605;&#1580;&#1605;&#1608;&#1593;&#1577;<span class="red">*</span></div>
</div> 

<div class="input_row even grp_size2"  style="display:none">
<div class="input_label_div"><span>Group Size:</span><span class="red">*</span>
</div>
<div class="input_div">
    <input dir="ltr" type="text"  name="grp_size2" id="grp_size2"  class="input_cls" style="width:50px;"/>
     <div class="group_slot_availability1" style="display:inline; float:left; padding-top: 3px; padding-left: 10px;"></div>
</div>
<div class="input_label_div_arb">&#1593;&#1583;&#1583;&nbsp;&#1571;&#1601;&#1585;&#1575;&#1583;&nbsp;&#1575;&#1604;&#1605;&#1580;&#1605;&#1608;&#1593;&#1577;<span class="red">*</span></div>
</div>

<div class="input_row even grp_size3"  style="display:none">
<div class="input_label_div"><span>Group Size:</span><span class="red">*</span>
</div>
<div class="input_div">
    <input dir="ltr" type="text"  name="grp_size3" id="grp_size3"  class="input_cls" style="width:50px;"/>
     <div class="group_slot_availability1" style="display:inline; float:left; padding-top: 3px; padding-left: 10px;"></div>
</div>
<div class="input_label_div_arb">&#1593;&#1583;&#1583;&nbsp;&#1571;&#1601;&#1585;&#1575;&#1583;&nbsp;&#1575;&#1604;&#1605;&#1580;&#1605;&#1608;&#1593;&#1577;<span class="red">*</span></div>
</div>
<!--<div class="input_row even tour_operator_div " style=" display:none;">
<div class="input_label_div"><span>Number of buses:</span> <span class="red">*</span>
</div>

<div class="input_div">
    
    <select class="input_cls" name="bus" id="bus">
    <option value="">Please select - يرجي اختيار</option>
    <?php for($i=1; $i<=10; $i++)
	       {
			   ?>
               <option value="<?php echo $i; ?>"> <?php echo $i; ?> </option>
               <?php
		   }
	 ?>
    </select>   
</div>
<div class="input_label_div_arb">&#1593;&#1583;&#1583; &#1575;&#1604;&#1581;&#1575;&#1601;&#1604;&#1575;&#1578;<span class="red">*</span></div>
</div>-->

<div class="input_row odd library">
<div class="input_label_div ltr" style="width:230px; text-align:left;">Library visit to be included</div>
<div class="input_div iwidth" align="center" style="width:180px"> Yes <input type="radio" name="library" value="yes"  style="border:0;"> &#1606;&#1593;&#1605; 
&nbsp;&nbsp;&nbsp;&nbsp; No <input type="radio" name="library" value="no"  style="border:0;"> &#1604;&#1575; </div>
<div class="input_label_div_arb">&#1586;&#1610;&#1575;&#1585;&#1577; &#1575;&#1604;&#1605;&#1603;&#1578;&#1576;&#1577;</div>

</div>
<div class="input_row even needs">
<div class="input_label_div"><span>Specify any special interests or </span><span>needs for the group:</span><br/>
<span>e.g. (wheel chair, club car, etc.)</span> <br/>
<img src="/images/wheelchair.png" style="border:0; float:left; margin-top:10px;clear:both;">
</div>
<div class="input_div other_info">
    <textarea class="input_cls" name="other_info" style="width:240px;" id="other_info" cols="25" rows="8" ></textarea></div>
	<div class="input_label_div_arb" style="width:200px;">تحديد  أية اهتمامات أو احتياجات خاصة<span> <br/><span>مثال: (كرسي متحرك، سيارة كهربائية، الخ) </span>
    <br/><img src="/images/wheelchair.png" style="border:0; float:right; margin-top:5px;">
</span>
</div>
</div>


<div class="input_row odd border_bottom" >
 
<div align="center">
<div style="float:left;text-align:left; direction:ltr"> 



       <span style="text-align:left">We kindly request that you follow the </span> <br>  <a class="lightbox_popup" href="http://szgmc-en.cnh.ae/en/img/Mosque-Manners.jpg" target="_blank">Mosque Manners</a> during your visit. Thank you. </span>

        


</div>

<div class="input_label_div_arb">يرجى التقيد <a class="lightbox_popup" href="http://szgmc-en.cnh.ae/en/img/Mosque-Manners.jpg" target="_blank">بآداب المسجد</a> عند الزيارة</div>

<div class="clear" style="padding-top:10px;direction:ltr;"  ><img src="captcha.php" id="captcha" style="background:#CCC;outline:none;
transition: all 0.25s ease-in-out;
-webkit-transition: all 0.25s ease-in-out;
-moz-transition: all 0.25s ease-in-out;
border: solid 2px #D7D9E5;
border-radius: 3px;
margin-left: 10px;
margin-bottom: 5px;
" /><br/>


<!-- CHANGE TEXT LINK -->
<a href="javascript:void(0)" onclick="
    document.getElementById('captcha').src='captcha.php?'+Math.random();
    document.getElementById('captcha-form').focus();"
    id="change-image" style="direction:ltr;">Not readable? Change text.</a><br/><br/>



 </div> 
 <div class="clear input_label_div" style="float:left;width:230px; text-align:left; margin-top:10px;"><span style="float:left;width:230px;">Type the characters from the picture into <span style="float:left; text-align:left">the text box</span><span class="red">*</span></span> </div>
 <div class="captcha"><input type="text" name="captcha" id="captcha-form" autocomplete="off" /></div>
 <div style="float:left;width:200px; text-align:right; margin-top:10px;" class="input_label_div_arb"><span dir="rtl">يرجى كتابة الاحرف الظاهرة في الصورة داخل المربع</span><span class="red">*</span></div>
<div  class="clear" align="center">
<input type="submit" value="Submit &#65165;&#1585;&#1587;&#1604;" class="acloginbttn" name="submitTour" id="submitTour" style="direction:ltr;margin-left: 2px;" /></div>
</div>

</div>



</form>