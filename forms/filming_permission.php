<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" id="mediaForm" name="mediaForm" enctype="multipart/form-data">





<div class="input_row even">

<div class="input_label_div"><span>Full Name "Head of Film Crew"  </span><span class="red">*</span><br/><span><i>(as appears on passport)</i></span></div>

<div class="input_div"> <input type="text" name="a_fname" id="a_fname" class="input_cls"/>  </div>



<div class="input_label_div_arb"><span dir="rtl">&#1575;&#1604;&#1575;&#1587;&#1605; &#1575;&#1604;&#1603;&#1575;&#1605;&#1604; &#1604;&#1585;&#1574;&#1610;&#1587; &#1575;&#1604;&#1591;&#1575;&#1602;&#1605;</span><span class="red">*</span><br/>

  <span><i><span dir="rtl"> </span><em><span dir="rtl"><span dir="rtl"> </span>(&#1603;&#1605;&#1575; &#1610;&#1592;&#1607;&#1585; &#1601;&#1610; &#1580;&#1608;&#1575;&#1586; &#1575;&#1604;&#1587;&#1601;&#1585;</span></em>)</i></span></div>



</div>



<div class="input_row odd">

<div class="input_label_div"><span>Nationality</span><span class="red">*</span> </div>

<div class="input_div"> <input type="text" name="a_nationality" id="a_nationality" class="input_cls"/>  </div>

<div class="input_label_div_arb"><span dir="rtl">&#1575;&#1604;&#1580;&#1606;&#1587;&#1610;&#1577;</span><span class="red">*</span></div>

</div>







<div class="input_row even">

<div class="input_label_div"><span>Number of the crew</span><span class="red">*</span></div>

<div class="input_div"> <input type="text" name="num_persons_expected" id="num_persons_expected" class="input_cls"/>  </div>

<div class="input_label_div_arb"><span dir="rtl"><span dir="rtl">عدد أفراد فريق العمل</span></span><span class="red">*</span></div>

</div>







<div class="input_row odd">

<div class="input_label_div"><span>Company Name</span><span class="red">*</span></div>

<div class="input_div"> <input type="text" name="a_company" id="a_company" class="input_cls"/>  </div>

<div class="input_label_div_arb"><span dir="rtl">&#1575;&#1587;&#1605;&nbsp;&#1575;&#1604;&#1588;&#1585;&#1603;&#1577;</span><span class="red">*</span></div>

</div>



<div class="input_row even">

<div class="input_label_div"><span>Company Name commissioning the film<i>(if different from above)</i></span></div>

<div class="input_div"> <input type="text" name="a_comp_comm" id="a_comp_comm" class="input_cls"/>  </div>

<div class="input_label_div_arb"><span dir="rtl">&#1575;&#1587;&#1605;&nbsp;&#1575;&#1604;&#1588;&#1585;&#1603;&#1577;&nbsp;&#1575;&#1604;&#1605;&#1606;&#1592;&#1605;&#1577; &#1604;&#1604;&#1578;&#1587;&#1580;&#1610;&#1604;</span></div>

</div>



<div class="input_row odd">

<div class="input_label_div"><span>Contact Number</span><span class="red">*</span> </div>

<div class="input_div"> <input type="text" name="contact_num" id="contact_num" class="input_cls"/>  </div>

<div class="input_label_div_arb"><span dir="rtl">&#1585;&#1602;&#1605;&nbsp;&#1607;&#1575;&#1578;&#1601; &#1575;&#1604;&#1605;&#1587;&#1572;&#1608;&#1604;&nbsp;&#1571;&#1608;&nbsp;&#1575;&#1604;&#1605;&#1606;&#1587;&#1602;</span><span class="red">*</span></div>

</div>



<div class="input_row even">

<div class="input_label_div"><span>Email Address</span><span class="red">*</span></div>

<div class="input_div"> <input type="text" name="email" id="email" class="input_cls"/>  </div>

<div class="input_label_div_arb"><span dir="rtl">&#1575;&#1604;&#1576;&#1585;&#1610;&#1583;&nbsp;&#1575;&#1604;&#1573;&#1604;&#1603;&#1578;&#1585;&#1608;&#1606;&#1610;</span><span class="red">*</span></div>

</div>









<div class="input_row odd">

<div class="input_label_div"><span>Purpose of the Film</span><span class="red">*</span> <br/>

<span style="text-align:left;"><i>Please mention "Name of the film, language, content, duration and expected date of display".</i></span>

</div>

<div class="input_div">  <textarea class="input_cls" style="width:250px;" name="purpose" id="purpose" cols="25" rows="8" ></textarea></div>

<div class="input_label_div_arb"><span dir="rtl">&#1575;&#1604;&#1607;&#1583;&#1601; &#1605;&#1606; &#1575;&#1604;&#1578;&#1589;&#1608;&#1610;&#1585;</span><span class="red">*</span> <br/>

  <span dir="rtl"> </span><em><span dir="rtl"><span dir="rtl"> </span>(يرجى ذكر اسم الفيلم ولغته ومحتواه ومدته والتاريخ المتوقع لعرض الفيلم)</span></em></div>

</div>









<div class="input_row even" >



<div class="input_label_div"><span>Equipment </span><span class="red">*</span> <br/> <span> (provide details of any equipment being used during the shoot)





</span></div>

<div class="input_div">  <textarea class="input_cls" style="width:250px;" name="equipments" id="equipments" cols="25" rows="8" ></textarea></div>

<div class="input_label_div_arb"><span dir="rtl">&#1575;&#1604;&#1605;&#1593;&#1583;&#1575;&#1578;</span><span class="red">*</span> <br/><em><span dir="rtl">(يرجي ذكر تفاصيل المعدات  المستخدمة في التصوير)</span></em></div>

</div>





<div class="input_row odd">

<div class="input_label_div"><span>Choose your Organization's category</span><span class="red">*</span></div>

<div class="input_div">

<select class="input_cls" name="group_category" id="group_category" onchange="check_additional_fileds(this.options[this.selectedIndex].value);" >

<option value="">Please select - يرجي اختيار</option>

<optgroup label="----------------------------------------" > </optgroup>

<option value="مؤسسة إعلامية حكومية لدولة الإمارات العربية المتحدة">مؤسسة إعلامية حكومية لدولة الإمارات العربية المتحدة</option>

<option value="UAE Governament Media Organization">UAE Governament Media Organization</option>

<optgroup label="----------------------------------------" > </optgroup>

<option value="مؤسسة إعلامية دولية">مؤسسة إعلامية دولية</option>

<option value="International Media Organization">International Media Organization</option>



</select>

</div>

 <div class="input_label_div_arb">يرجى اختيار فئة مقدم الطلب<span class="red">*</span></div>

</div>





<!--<div class="input_row odd adnmc" style=" display:none;">

<div class="input_label_div"><span>Abu Dhabi National Media Council Permission </span> <br/><span style="float:left; text-align:left;">(Please attach a copy of the ADNMC <br/><span style="float:left; text-align:left; ">Permission)</span> <span class="red">*</span></span></span>  </div>

<div class="input_div iwidth" style="width:260px;"> 

   <input type="text" class="binput_cls" name="a_doc_fname" id="a_doc_fname" readonly="readonly"/>

	  <input type="hidden" name="a_doc" id="a_doc"/>

        <input type="button" value="Select / إختار" name="a_doc_btn" id="a_doc_btn" style="width:75px; background:#E8EDE9;font-size:12px; padding:1px 0;direction:ltr;"/>



</div>

<div class="input_label_div_arb"><span dir="rtl">&#1575;&#1604;&#1578;&#1589;&#1585;&#1610;&#1581;</span><br/>

  <p dir="rtl"><span dir="rtl"> </span><em><span dir="rtl"> </span>(&#1610;&#1585;&#1580;&#1609; &#1573;&#1585;&#1601;&#1575;&#1602; &#1606;&#1587;&#1582;&#1577; &#1605;&#1606; &#1575;&#1604;&#1578;&#1589;&#1585;&#1610;&#1581;<br/> &#1575;&#1604;&#1589;&#1575;&#1583;&#1585; &#1605;&#1606; &#1605;&#1580;&#1604;&#1587; &#1571;&#1576;&#1608;&#1592;&#1576;&#1610; &#1604;&#1604;&#1573;&#1593;&#1604;&#1575;&#1605;)</em><span class="red">*</span></div>

</div>-->



    <div class="input_row odd adnmc" style=" display:none;">

        <div id="a_pass_status_msg"></div>

        <div class="input_label_div"><span>Abu Dhabi National Media Council Permission </span> <br/><span style="float:left; text-align:left;">(Please attach a copy of the ADNMC <br/><span style="float:left; text-align:left; ">Permission)</span> <span class="red">*</span></span></span>  </div>

        <div class="input_div iwidth" style="width:270px;">

            <input type="text" value="" readonly="readonly" id="a_pass" name="a_pass" class="binput_cls">

            <input type="hidden" value="" readonly="readonly" id="a_pass_fname" name="a_pass_fname" class="binput_cls">

            <input type="hidden" id="a_pass_status" name="a_pass_status">

            <input type="button" style="width:75px; background:#E8EDE9;font-size:11px; padding:1px 0; direction:ltr;" id="a_pass_btn" name="a_pass_btn" value="Select / إختار">



        </div>

        <div class="input_label_div_arb" style="width:180px;"><span dir="rtl">&#1575;&#1604;&#1578;&#1589;&#1585;&#1610;&#1581;</span><br/>

            <p dir="rtl"><span dir="rtl"> </span><em><span dir="rtl"> </span>(&#1610;&#1585;&#1580;&#1609; &#1573;&#1585;&#1601;&#1575;&#1602; &#1606;&#1587;&#1582;&#1577; &#1605;&#1606; &#1575;&#1604;&#1578;&#1589;&#1585;&#1610;&#1581;<br/> &#1575;&#1604;&#1589;&#1575;&#1583;&#1585; &#1605;&#1606; &#1605;&#1580;&#1604;&#1587; &#1571;&#1576;&#1608;&#1592;&#1576;&#1610; &#1604;&#1604;&#1573;&#1593;&#1604;&#1575;&#1605;)</em><span class="red">*</span><span class="red">*</span></div>

    </div>









<div class="input_row even">

<div class="input_label_div"><span>Date and Time of Filming </span><span class="red">*</span>  </div>

<div class="input_div1" style="float:left; direction:ltr;text-align:left"> 

<div style="float:left; width:165px;"><input type="text" name="prop_date" id="prop_date" class="input_cls_2" value="" style="width:140px; direction:ltr" readonly="readonly" />  </div>

<div style="float:left; width:125px;">

<select name="prop_time" id="prop_time" class="input_cls_2"  style="width:90px;">

<option value="">Time - وقت</option>

<option value="09:00:00">9:00</option>

 

             <option value="09:30:00">9:30</option>

            <option value="10:00:00">10:00</option>

            <option value="10:30:00">10:30</option>

            <option value="11:00:00">11:00</option>

            <option value="11:30:00">11:30</option>

            <option value="12:00:00">12:00</option>

            <option value="12:30:00">12:30</option>

            <option value="13:00:00">13:00</option>

            <option value="13:30:00">13:30</option>

           <option value="14:00:00">14:00</option>

           <option value="14:30:00">14:30</option>

           <option value="15:00:00">15:00</option>

           <option value="15:30:00">15:30</option>

           <option value="16:00:00">16:00</option>

           <!--<option value="16:30:00">16:30</option>

             <option value="17:00:00">17:00</option>

           <option value="17:30:00">17:30</option>

           <option value="18:00:00">18:00</option>

           <option value="18:30:00">18:30</option>

           <option value="19:00:00">19:00</option>

           <option value="19:30:00">19:30</option>

           <option value="20:00:00">20:00</option>-->

</select> 

 </div>

</div> 



<div class="input_label_div_arb" style='width:170px'><span dir="rtl">&#1578;&#1575;&#1585;&#1610;&#1582; &#1608;&#1608;&#1602;&#1578; &#1575;&#1604;&#1578;&#1589;&#1608;&#1610;&#1585;</span><span class="red">*</span> </div>

</div>





<div class="input_row odd ">

<div class="input_label_div"><span>Short Description about the channel or the organiztion applying for <span style="float:left; text-align:left">permission</span><span class="red">*</span></span>

</div>

<div class="input_div">  <textarea class="input_cls" style="width:250px;" name="short_info" id="short_info" cols="25" rows="8" ></textarea></div>



<div class="input_label_div_arb" style="width:185px;"><span dir="rtl">نبذة مختصرة عن القناة أو الشركة مقدمة الطلب</span><span class="red">*</span></div>

</div>





<div class="input_row even ">

<div class="input_label_div"><span>Other information (if available)</span><br/><span>(In the case of need to interview officials or a culture guide please mention)</span>

</div>

<div class="input_div">  <textarea class="input_cls" style="width:250px;" name="other_info" id="other_info" cols="25" rows="8" ></textarea></div>



<div class="input_label_div_arb" style="width:185px;"><span dir="rtl">&#1605;&#1593;&#1604;&#1608;&#1605;&#1575;&#1578; &#1573;&#1590;&#1575;&#1601;&#1610;&#1577; (&#1573;&#1606; &#1608;&#1580;&#1583;&#1578;) </span><br/>

  <span dir="rtl"> </span><span dir="rtl"> </span><em><span dir="rtl"><span dir="rtl"> </span><span dir="rtl"> </span>(&#1610;&#1585;&#1580;&#1609; &#1584;&#1603;&#1585; &#1575;&#1604;&#1578;&#1601;&#1575;&#1589;&#1610;&#1604; &#1601;&#1610; &#1581;&#1575;&#1604; &#1590;&#1585;&#1608;&#1585;&#1577; <br/> &#1575;&#1604;&#1581;&#1575;&#1580;&#1577; &#1573;&#1604;&#1609; &#1605;&#1602;&#1575;&#1576;&#1604;&#1577; &#1571;&#1581;&#1583; &#1575;&#1604;&#1605;&#1587;&#1572;&#1608;&#1604;&#1610;&#1606; <br/>  &#1571;&#1608; &#1575;&#1604;&#1605;&#1585;&#1588;&#1583;&#1610;&#1606; &#1575;&#1604;&#1579;&#1602;&#1575;&#1601;&#1610;&#1610;&#1606;)</span></em></div>

</div>





<!--<div class="input_row odd ">

<div class="input_label_div"><span>Details of Filming Crew </span><br/><span>(attach a list of crew members and their roles in the filming in word, excel or pdf format)</span>

</div>

<div class="iwidth" >  <div >

        <input type="text" class="binput_cls" name="c_up_fname" id="c_up_fname" readonly="readonly" disabled/>

        <input type="hidden" name="c_up_fname_full" id="c_up_fname_full"/>

        <input type="button" value="Select / إختار" name="c_passport_btn" id="c_passport_btn" style="width:75px; background:#E8EDE9;font-size:12px; padding:1px 0;direction:ltr;"/>



    </div></div> 

	

	<div class="input_label_div_arb"><span dir="rtl">&#1576;&#1610;&#1575;&#1606;&#1575;&#1578; &#1571;&#1601;&#1585;&#1575;&#1583; &#1575;&#1604;&#1591;&#1575;&#1602;&#1605;</span><br/>

  <p dir="rtl" style="font-size:12px !important"><span dir="rtl"> </span><em><span dir="rtl"> </span>(يرجى إرفاق كشف بأسماء أفراد الطاقم وأدوارهم في التصوير على ملف وورد أو اكسل أو ادوبي اكروبات)</em>

	</div>

</div>-->



    <div class="input_row odd">

        <div id="f_crew_status_msg"></div>

        <div class="input_label_div"><span>Details of Filming Crew </span><br/><span>(attach a list of crew members and their roles in the filming in word, excel or pdf format)</span></div>

        <div class="input_div iwidth" style="width:270px;">

            <input type="text" value="" readonly="readonly" id="f_crew" name="f_crew" class="binput_cls">

            <input type="hidden" value="" readonly="readonly" id="f_crew_fname" name="f_crew_fname" class="binput_cls">

            <input type="hidden" id="f_crew_status" name="f_crew_status">

            <input type="button" style="width:75px; background:#E8EDE9;font-size:11px; padding:1px 0; direction:ltr;" id="f_crew_btn" name="f_crew_btn" value="Select / إختار">

        </div>

        <div class="input_label_div_arb" style="width:180px;">

            <span dir="rtl">&#1576;&#1610;&#1575;&#1606;&#1575;&#1578; &#1571;&#1601;&#1585;&#1575;&#1583; &#1575;&#1604;&#1591;&#1575;&#1602;&#1605;</span><br />

            <p dir="rtl" style="font-size:12px !important"><span dir="rtl"> </span><em><span dir="rtl"> </span>(يرجى إرفاق كشف بأسماء أفراد الطاقم وأدوارهم في التصوير على ملف وورد أو اكسل أو ادوبي اكروبات)</em>

        </div>

    </div>



<div class="input_row even">

<div class="clear" style="padding-top:10px;direction:ltr; text-align:center"><img src="captcha.php" id="captcha" style="background:#CCC;outline:none;

transition: all 0.25s ease-in-out;

-webkit-transition: all 0.25s ease-in-out;

-moz-transition: all 0.25s ease-in-out;

border: solid 2px #D7D9E5;

border-radius: 3px;

margin-left: 10px;

margin-bottom: 5px;

"><br>





<!-- CHANGE TEXT LINK -->

<a href="javascript:void(0)" onclick="

    document.getElementById('captcha').src='captcha.php?'+Math.random();

    document.getElementById('captcha-form').focus();" id="change-image" style="direction:ltr;">Not readable? Change text.</a><br><br>







 </div>

 <div class="clear input_label_div" style="float:left;width:250px; text-align:left; margin-top:10px;"><span style="float:left;width:230px;">Type the characters from the picture into <span style="float:left; text-align:left">the text box</span><span class="red">*</span></span> </div>

 <div class="captcha"><input type="text" name="captcha" id="captcha-form" autocomplete="off"></div>

 <div style="float:left;width:180px; text-align:right; margin-top:10px;" class="input_label_div_arb"><span dir="rtl">يرجى كتابة الاحرف الظاهرة في الصورة داخل المربع</span><span class="red">*</span></div>

</div>



<div class="input_row odd border_bottom" >

  <div class="terms"> 

  <div class="input_label_div_term clear input_label_div">

*Please note that this request does not constitute a confirmed booking. We will inform you when your filming permission application has been accepted or should we need any further information.

 <div class="clear"></div>

<br/>

       <span> I agree to the <a href="Terms_and_Conditions.pdf" target="_blank" class="yellow">terms and conditions</a> </span></div>



        



<!--div  style="text-align:center;"><input type="checkbox" name="agree" id="agree" value="1" style="margin-left:-200px;" /></div-->

</div>



<div class="input_label_div_arb">

<span dir="rtl"> * يرجى العلم بأن تعبئة هذا الطلب لا يعتبر حجزا مؤكدا ، حيث أنه سيتم ابلاغكم لاحقاً في حال الموافقة على طلبكم أو الرفض.</span>

   <div class="clear"></div>

   <br/><br/><br/>

   <p dir="rtl" style="font-size:12px !important">أوافق على <a href="Terms_And_Conditions_Arb.pdf" target="_blank" class="yellow">الشروط&nbsp;  والأحكام</a> </p>

   <!-- <br/> <br/>

    <p dir="rtl">&#1571;&#1602;&#1585;  &#1576;&#1571;&#1606; &#1580;&#1605;&#1610;&#1593; &#1575;&#1604;&#1605;&#1593;&#1604;&#1608;&#1605;&#1575;&#1578; &#1575;&#1604;&#1587;&#1575;&#1576;&#1602;&#1577; &#1589;&#1581;&#1610;&#1581;&#1577;</p>-->

  </div>

<!--div  style="text-align:center;margin-top:140px;"><input type="checkbox" name="agree" id="agree" value="1" style="margin-left:-100px;" /></div-->

<div  class="clear" align="center">

<input class="chk" style="width:12px;height:12px;" type="checkbox" name="agree" id="agree" value="1" />

</div>


<div  class="clear" align="center">

<input type="submit" value="Submit &#65165;&#1585;&#1587;&#1604;" class="acloginbttn" style="direction:ltr;" />

</div>


<div class="input_label_div"><span><a href="http://get.adobe.com/reader " target="_blank"><img align="absmiddle" alt="" border="0" src="/site/images/pdf.png" style="margin-right:9px;" />Download Adobe Reader</a> </span>

</div>

<div class="input_label_div_arb" style="width:185px;">

<span dir="rtl"><a href="http://get.adobe.com/reader " target="_blank"><img align="absmiddle" alt="" border="0" src="/site/images/pdf.png" style="margin-right:7px;" /> تحميل برنامج ادوبي ريدر</a></span>

  </div>

</div>











</form>