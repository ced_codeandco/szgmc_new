<?php 
session_start();
//print_r($_SESSION);
if((isset($_SESSION['user_id'])) && $_SESSION['user_id']!=''){
?>
<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" id="changeUsernameForm" name="changeUsernameForm" enctype="multipart/form-data">
	
     <div class="input_row even">
        	<div class="input_label_div">
            	<span>Current Username(Email)</span><span class="red">*</span>
            </div>
    		<div class="input_div"> 
            	<input type="text" name="cur_username" id="cur_username" class="input_cls"/>  
            </div>
    		<div class="input_label_div_arb">
      			<div>
        			<div>
                    	<span dir="rtl">كلمة المرور الحالية
      						<span class="red">*</span>
        				</span>
                    </div>
      			</div>
    		</div>
    </div>		 
     <div class="input_row even">
        	<div class="input_label_div">
            	<span>New Username(Email)</span><span class="red">*</span>
            </div>
    		<div class="input_div"> 
            	<input type="text" name="new_username" id="new_username" class="input_cls"/>  
            </div>
    		<div class="input_label_div_arb">
      			<div>
        			<div>
                    	<span dir="rtl">كلمة المرور الجديدة
      						<span class="red">*</span>
        				</span>
                    </div>
      			</div>
    		</div>
    </div>
   
   
    <div class="input_row odd border_bottom">
		<div align="center">
        	<div  class="clear" align="center">
        		<input type="submit" value="Submit &#65165;&#1585;&#1587;&#1604;"  name="submitchangeUsername" id="submitchangeUsername" class="acloginbttn" style="direction:ltr;" />
        	</div>
        </div>

	</div>
    
</form>
<?php }?>