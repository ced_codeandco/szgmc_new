<form action="" method="post" id="mediaForm" name="mediaForm" enctype="multipart/form-data">


<div class="input_row even">
<div class="input_label_div"><span>Name </span><span class="red">*</span></div>
<div class="input_div"> <input type="text" name="a_name" id="a_name" class="input_cls"/>  </div>

<div class="input_label_div_arb"><span dir="rtl">&#1575;&#1604;&#1575;&#1587;&#1605;</span><span class="red">*</span></div>
</div>

<div class="input_row odd">
<div class="input_label_div"><span>Age:</span><span class="red">*</span></div>
<div class="input_div"> 
        <select name="a_age" class="input_cls">
        <option value="">Please select - يرجى اختر</option>
        <?php
        for($i=18;$i<=65;$i++)
        {
            ?>
            <option value="<?php echo $i;?>"><?php echo $i;?></option>
            <?php
        }
        ?>
        </select>
 </div>
<div class="input_label_div_arb"><span dir="rtl">&#1575;&#1604;&#1593;&#1605;&#1585;</span><span class="red">*</span></div>
</div>

<div class="input_row even">
<div class="input_label_div"><span>Gender</span><span class="red">*</span> </div>
<div class="input_div"> 
        <select name="a_gender" class="input_cls">
        <option value="">Please select - يرجى اختر</option>
        <optgroup label="----------------------------------------" > </optgroup>
        <option value="ذكر">ذكر</option>
        <option value="Male">Male</option>
        <optgroup label="----------------------------------------" > </optgroup>
        <option value="انثى">انثى</option>
        <option value="Female">Female</option>

        </select>
  </div>
<div class="input_label_div_arb"><span dir="rtl">&#1575;&#1604;&#1580;&#1606;&#1587;</span><span class="red">*</span></div>
</div>

<div class="input_row odd">
<div class="input_label_div"><span>Nationality</span><span class="red">*</span></div>
<div class="input_div"> <input dir="ltr" type="text"  name="a_nationality" id="a_nationality"  class="input_cls"/>  </div>
<div class="input_label_div_arb"><span dir="rtl">&#1575;&#1604;&#1580;&#1606;&#1587;&#1610;&#1577;</span><span class="red">*</span></div>
</div>



<div class="input_row even">
<div class="input_label_div"><span>City</span><span class="red">*</span></div>
<div class="input_div"> <input type="text" name="a_city" id="a_city" class="input_cls"/>  </div>
<div class="input_label_div_arb"><span dir="rtl">&#1575;&#1604;&#1605;&#1583;&#1610;&#1606;&#1577;</span><span class="red">*</span></div>
</div>

<div class="input_row even">
<div class="input_label_div"><span>Contact No.</span><span class="red">*</span></div>
<div class="input_div"> <input type="text" name="a_telephone" id="a_telephone" class="input_cls"/>  </div>
<div class="input_label_div_arb"><span dir="rtl">&#1585;&#1602;&#1605; &#1575;&#1604;&#1607;&#1575;&#1578;&#1601;</span><span class="red">*</span></div>
</div>

<div class="input_row even">
<div class="input_label_div"><span>Email</span><span class="red">*</span></div>
<div class="input_div"> <input type="text" name="a_email" id="a_email" class="input_cls"/>  </div>
<div class="input_label_div_arb"><span dir="rtl">&#1575;&#1604;&#1576;&#1585;&#1610;&#1583; &#1575;&#1604;&#1575;&#1603;&#1578;&#1585;&#1608;&#1606;&#1610;</span><span class="red">*</span></div>
</div>

<div class="input_row odd">
<div class="input_label_div"><span>Education Level</span><span class="red">*</span>  
</div>
<div class="input_div">  <textarea class="input_cls" style="width:240px;" name="desc" id="desc" cols="25" rows="8" ></textarea></div>

<div class="input_label_div_arb"><span dir="rtl">&#1575;&#1604;&#1605;&#1587;&#1578;&#1608;&#1609; &#1575;&#1604;&#1578;&#1593;&#1604;&#1610;&#1605;&#1610;</span><span class="red">*</span></div>
</div>
    <div class="input_row even">
        <div class="input_label_div"><span>Memorization Level</span><span class="red">*</span></div>
        <div class="input_div"> <input type="text" name="memorization_level" id="memorization_level" class="input_cls"/>  </div>
        <div class="input_label_div_arb"><span dir="rtl">مقدار الحفظ</span><span class="red">*</span></div>
    </div>

<div class="input_row odd">
<div class="input_label_div"><span>Have you ever participated in the previous courses?</span><span class="red">*</span></div>
<div class="input_div iwidth" align="center">
Yes<input type="radio" value="yes &#1606;&#1593;&#1605;" name="course_partticipation" style="border:0;">&#1606;&#1593;&#1605;&nbsp;&nbsp;&nbsp;&nbsp;No<input type="radio" value="no &#1604;&#1575;" name="course_partticipation" style="border:0;">&#1604;&#1575;</div>
<div class="input_label_div_arb"><span dir="rtl">&#1607;&#1604; &#1587;&#1576;&#1602; &#1604;&#1603;&#1605; &#1575;&#1604;&#1575;&#1588;&#1578;&#1585;&#1575;&#1603; &#1601;&#1610; &#1575;&#1604;&#1583;&#1608;&#1585;&#1575;&#1578; &#1575;&#1604;&#1587;&#1575;&#1576;&#1602;&#1577;&#1567;</span><span class="red">*</span></div>
</div>

 

<div class="input_row odd border_bottom" >
  <div align="center">


<div class="clear" style="padding-top:10px;direction:ltr;"  ><img src="captcha.php" id="captcha" style="background:#CCC;outline:none;
transition: all 0.25s ease-in-out;
-webkit-transition: all 0.25s ease-in-out;
-moz-transition: all 0.25s ease-in-out;
border: solid 2px #D7D9E5;
border-radius: 3px;
margin-left: 10px;
margin-bottom: 5px;
" /><br/>


<!-- CHANGE TEXT LINK -->
<a href="javascript:void(0)" onclick="
    document.getElementById('captcha').src='captcha.php?'+Math.random();
    document.getElementById('a_captcha').focus();"
    id="change-image" style="direction:ltr;">Not readable? Change text.</a><br/><br/>



 </div> 
 <div class="clear input_label_div" style="float:left;width:230px; text-align:left; margin-top:10px;"><span style="float:left;width:230px;">Type the characters from the picture into <span style="float:left; text-align:left">the text box</span><span class="red">*</span></span> </div>
 <div class="captcha"><input type="text" name="a_captcha" id="acaptcha" autocomplete="off" /></div>
 <div style="float:left;width:200px; text-align:right; margin-top:10px;" class="input_label_div_arb"><span dir="rtl">يرجى كتابة الاحرف الظاهرة في الصورة داخل المربع</span><span class="red">*</span></div>
</div> 
<div  align="center">
<input type="submit" value="Submit &#65165;&#1585;&#1587;&#1604;" class="acloginbttn" style="direction:ltr;" />
</div>

</div>





</form>