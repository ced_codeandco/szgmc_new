<style type="text/css">
.ui-widget-header {
background: url("/images/ui-bg_gloss-wave_35_f6a828_500x100.png") repeat-x scroll 50% 50% #F6A828;
}
#ui-datepicker-div{ direction:ltr;}
</style><?php 
session_start();
$slug = explode('/',$_SERVER['REQUEST_URI']);


if((isset($_SESSION['user_id'])) && $_SESSION['user_id']!=''){
    $sql_str="SELECT * FROM site_users WHERE id=".$_SESSION['user_id']." and active=1";
    $result=mysql_query($sql_str);
    $count=mysql_num_rows($result);
	if($count==1){
		if($obj = mysql_fetch_object($result)){
			$company_name=$obj->company_name;
			$trade_license=$obj->trade_license;
			$authority=$obj->authority;
			$name=$obj->name;
			$pub_date=$obj->date_of_expiry;
			$phoneno=$obj->phoneno;
			$countryCode=$obj->countryCode;
			$mobile=$obj->mobile;
			$gm_email=$obj->gm_email;
			$op_fname=$obj->op_fname;
			$op_phoneno=$obj->op_phoneno;
			$op_countryCode=$obj->op_countryCode;
			$op_mobile=$obj->op_mobile;
			$op_email=$obj->op_email;
			$email=$obj->email;
			$trd_license_file=mysql_real_escape_string($obj->trade_license_img);
		}
	}
}
	

?>
<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" id="userRegistrationForm" name="userRegistrationForm" enctype="multipart/form-data">

    <div class="input_row even">
    		<div class="input_row_special yellow">
           	 	<b>Organization Details</b>
            	<div class="input_label_div_arb"><strong><span dir="rtl">بيانات الشركة</span></strong> </div>
            </div>
        	<div class="input_label_div">
            	<span>Name of the Organization</span><span class="red">*</span>
            </div>
    		<div class="input_div"> 
            	<input type="hidden" value="<?php echo (isset($_REQUEST['action']))?$_REQUEST['action']:'';?>" name="mode" id="mode"/>
                <?php
            	if($_REQUEST['action']!="edit"){
			?>
            	<input type="text" name="a_companyname" id="a_companyname" class="input_cls" value="<?php echo $company_name;?>"/>   
                <?php 
				}else{ 
				?>
                <input type="text" name="a_companyname" id="a_companyname" class="input_cls" value="<?php echo $company_name;?>" readonly/> 
                <?php
				}
              ?>
            </div>
    		<div class="input_label_div_arb">
      			<div>
        			<div>
                    	<span dir="rtl">اسم الشركة<span class="red">*</span>
      						
        				</span>
                    </div>
      			</div>
    		</div>
    </div>
     
      <div class="input_row odd" >
        	<div class="input_label_div">
            	<span>Trade License No. </span><span class="red">*</span>
            </div>
    		<div class="input_div"> 
             <?php
            	if($_REQUEST['action']!="edit"){
			?>
            	<input type="text" name="a_tradelicense" id="a_tradelicense" class="input_cls" value="<?php echo $trade_license;?>"/>   
              <?php 
			  	}else{ 
			  ?>
			  		<input type="text" name="a_tradelicense" id="a_tradelicense" class="input_cls" value="<?php echo $trade_license;?>" readonly/>  
			  <?php
				}
              ?>
            	
            </div>
    		<div class="input_label_div_arb">
      			<div>
        			<div>
                    	<span dir="rtl">رقم الرخصة التجارية<span class="red">*</span>
      						
        				</span>
                    </div>
      			</div>
    		</div>
    </div>


    <div class="input_row even" >
                <div class="input_label_div"><span>Emirate:</span><span class="red">*</span></div>
                <div class="input_div">
                <?php $auth=$authority; ?>
                	 <?php
            	if($_REQUEST['action']=="edit"){
			?>
		              <input type="text" name="authority" id="authority" value="<?php echo $auth;?>" readonly/>
                <?php 
				}else{ 
				?>
                <select class="input_cls" name="authority" id="authority" >
                <option value="">Please select - يرجى اختر</option>
              <optgroup label="----------------------------------------" > </optgroup>
<option value="أبو ظبي" class="arb" <?php if($auth=="أبو ظبي"){?>selected<?php } ?>><span dir="RTL">أبو ظبي</span></option>
<option value="Abu Dhabi" <?php if($auth=="Abu Dhabi"){?>selected<?php } ?>>Abu Dhabi</option>
<optgroup label="----------------------------------------" > </optgroup>
<option value="دبي" class="arb" <?php if($auth=="دبي"){?>selected<?php } ?>><span dir="RTL">دبي</span></option>
<option value="Dubai" <?php if($auth=="Dubai"){?>selected<?php } ?>>Dubai</option>
<optgroup label="----------------------------------------" > </optgroup>
<option value="الشارقة" class="arb" <?php if($auth=="الشارقة"){?>selected<?php } ?>><span dir="LTR">الشارقة</span></option>
<option value="Sharjah" <?php if($auth=="Sharjah"){?>selected<?php } ?>>Sharjah</option>
<optgroup label="----------------------------------------" > </optgroup>
<option value="رأس الخيمة" class="arb" <?php if($auth=="رأس الخيمة"){?>selected<?php } ?>><span dir="RTL">رأس الخيمة</span>
</option>
<option value="Ras Al Khaimah" <?php if($auth=="Ras Al Khaimah"){?>selected<?php } ?>>Ras Al Khaimah</option>
<optgroup label="----------------------------------------" > </optgroup>
<option value="الفجيرة" class="arb" <?php if($auth=="الفجيرة"){?>selected<?php } ?>><span dir="RTL">الفجيرة</span></option>

<option value="Fujairah" <?php if($auth=="Fujairah"){?>selected<?php } ?>>Fujairah</option>
<optgroup label="----------------------------------------" > </optgroup>
<option value="أم القيوين" class="arb" <?php if($auth=="أم القيوين"){?>selected<?php } ?>><span dir="RTL">أم القيوين</span></option>
<option value="Umm Al Quwain" <?php if($auth=="Umm Al Quwain"){?>selected<?php } ?>>Umm Al Quwain</option>
<optgroup label="----------------------------------------" > </optgroup>
<option value="عجمان" class="arb" <?php if($auth=="عجمان"){?>selected<?php } ?>><span dir="RTL">عجمان</span></option>
<option value="Ajman" <?php if($auth=="Ajman"){?>selected<?php } ?>>Ajman</option>
</select>
			   <?php
				}
              ?>
                 </div>
                 <div class="input_label_div_arb">جهة الإصدار<span class="red">*</span></div>
                </div>
    
    <div class="input_row_dob odd">
<div class="input_label_div"><span>Date of Expiry</span><span class="red">*</span></div>
<div class="input_div"> 

<input type="text"  name="pub_date" id="pub_date" dir="ltr" value="<?php echo $pub_date; ?>" class="input_cls notranslate"> 

 </div>
<div class="input_label_div_arb"><span dir="rtl">تاريخ الانتهاء</span><span class="red">*</span></div>
</div>
    
    

    <div class="input_row even">
    		<div class="input_row_special yellow">
           	 	<b>Contact Information</b>
            	<div class="input_label_div_arb"><strong><span dir="rtl">بيانات الاتصال</span></strong> </div>
            </div>
        	<div class="input_label_div">
            	<span>General Manager’s Name</span><span class="red">*</span>
            </div>
    		<div class="input_div"> 
            <input type="text" name="a_fname" id="a_fname" class="input_cls" value="<?php echo $name;?>"/>  
			  
            </div>
    		<div class="input_label_div_arb">
      			<div>
        			<div>
                    	<span dir="rtl">اسم مدير الشركة
      						<span class="red">*</span>
        				</span>
                    </div>
      			</div>
    		</div>
    </div>
	<!--///////////// -->
	
	<?php 
		
		if(stristr($_SERVER['REQUEST_URI'], 'edit') && !isValidTelFormat($phoneno)){

			$betterMsg2 = 'update now';
		}
	?>
	
	<!--S -->

	<div class="input_row odd" id="tel1">
        	<div class="input_label_div">
            	<span>Office Tel. No.</span><span class="red">*</span>
            </div>
    		<div class="input_div"> 
            	 
    <div class="cont" style="height:0;">
     <?php

			$phoneno = (isset($phoneno))?explode('-', $phoneno): '';
		?>
		<div class="contCode txtCode">
			<span>+</span><input type="text"  style="width: 35px;border: 1px solid #CEBA69; direction:ltr" name="countryCode" id="countryCode" value="971" readonly>
            <select name="a_op_city_code2">
				<option value="2" <?php if($phoneno[1]==2){?>selected<?php } ?>>2</option><option value="3" <?php if($phoneno[1]==3){?>selected<?php } ?>>3</option><option value="4" <?php if($phoneno[1]==4){?>selected<?php } ?>>4</option><option value="6" <?php if($phoneno[1]==6){?>selected<?php } ?>>6</option>
				<option value="7" <?php if($phoneno[1]==7){?>selected<?php } ?>>7</option><option value="9" <?php if($phoneno[1]==9){?>selected<?php } ?>>9</option>
			</select>
			
		</div>
		<div class="contCode txtNum">
			<input type="text" style="width: 146px;border: 1px solid #CEBA69;" name="a_op_phoneno_new2" id="a_op_phoneno_new2" class="input_cls" value="<?php echo (isset($obj->phoneno))? $phoneno[2]:'';?>" >
			<span style="color: red;"><?php echo $betterMsg2; ?></span>
		</div>
	</div>
            </div>
    		<div class="input_label_div_arb">
      			<div>
        			<div>
                    	<span dir="rtl">رقم المكتب
      					<span class="red">*</span>	
        				</span>
                    </div>
      			</div>
    		</div>
    </div>

	
    <div class="input_row even">
        	<div class="input_label_div">
            	<span>GM’s Email Address</span><span class="red">*</span>
               
            </div>
    		<div class="input_div"> 
                       	<input type="text" name="a_email_gm" id="a_email_gm" class="input_cls" value="<?php echo $gm_email;?>"/>  
              
            </div>
    		<div class="input_label_div_arb">
      			<div>
        			<div>
                    	<span dir="rtl">البريد الإلكتروني لمدير الشركة
      						<span class="red">*</span>
                          
        				</span>
                    </div>
      			</div>
    		</div>
    </div>
     <div class="input_row odd">
    		
        	<div class="input_label_div">
            	<span>Operation Manager’s Name</span><span class="red">*</span>
            </div>
    		<div class="input_div"> 
            <input type="text" name="a_op_fname" id="a_op_fname" class="input_cls" value="<?php echo $op_fname;?>"/>  
			  
            </div>
    		<div class="input_label_div_arb">
      			<div>
        			<div>
                    	<span dir="rtl">اسم مدير العمليات
      						<span class="red">*</span>
        				</span>
                    </div>
      			</div>
    		</div>
    </div>
    	<?php 
		
		if(stristr($_SERVER['REQUEST_URI'], 'edit') && !isValidTelFormat($op_phoneno)){
		
			$betterMsg = 'update now';
		}
	?>
	
	<!--S -->
	<div class="input_row even" id="tel2">
        	<div class="input_label_div">
            	<span>Office Tel. No.</span><span class="red">*</span>
            </div>
    		<div class="input_div"> 
            	 
    <div class="cont" style="height:0;">
    <?php

		$op_phoneno = (isset($op_phoneno))?explode('-', $op_phoneno): '';
	?>
		<div class="contCode txtCode">
			<span>+</span><input type="text"  style="width: 35px;border: 1px solid #CEBA69; direction:ltr" name="countryCode" id="countryCode" value="971" readonly>
            <select name="a_op_city_code">
				<option value="2" <?php if($op_phoneno[1]==2){?>selected<?php } ?>>2</option><option value="3" <?php if($op_phoneno[1]==3){?>selected<?php } ?>>3</option><option value="4" <?php if($op_phoneno[1]==4){?>selected<?php } ?>>4</option><option value="6" <?php if($op_phoneno[1]==6){?>selected<?php } ?>>6</option>
				<option value="7" <?php if($op_phoneno[1]==7){?>selected<?php } ?>>7</option><option value="9" <?php if($op_phoneno[1]==9){?>selected<?php } ?>>9</option>
			</select>
			
		</div>
		<div class="contCode txtNum">
			<input type="text" style="width: 146px;border: 1px solid #CEBA69;" name="a_op_phoneno_new" id="a_op_phoneno_new" class="input_cls" value="<?php echo (isset($obj->op_phoneno))? $op_phoneno[2]:'';?>" >
			<span style="color: red;"><?php echo $betterMsg; ?></span>
		</div>
	</div>
            </div>
    		<div class="input_label_div_arb">
      			<div>
        			<div>
                    	<span dir="rtl">رقم المكتب
      					<span class="red">*</span>	
        				</span>
                    </div>
      			</div>
    		</div>
    </div>
	<!--E -->
     <div class="input_row odd">
        	<div class="input_label_div">
            	<span>Mobile No.</span><span class="red">*</span>
            </div>
    		<div class="input_div"> 
            	 
    
    <div class="cont" style="height:0;">
     <?php 
		$code= (isset($op_countryCode))?substr($op_countryCode,3):'';
	
		?>
		<div class="contCode txtCode">
			<span>+</span><input type="text"  style="width: 35px;border: 1px solid #CEBA69; direction:ltr" name="countryCode" id="countryCode" value="971" readonly>
            <select name="a_op_areacode"><option value="50" <?php if($code==50){?>selected<?php } ?>>50</option><option value="55" <?php if($code==55){?>selected<?php } ?>>55</option><option value="52" <?php if($code==52){?>selected<?php } ?>>52</option><option value="56" <?php if($code==56){?>selected<?php } ?>>56</option></select>
			
		</div>
		<div class="contCode txtNum">
			<input type="text" style="width: 146px;border: 1px solid #CEBA69;" class="a_mobile" name="a_op_mobile" id="a_op_mobile" value="<?php echo (isset($obj->op_mobile))?$obj->op_mobile:'';?>" >
			
		</div>
	</div>
            </div>
    		<div class="input_label_div_arb">
      			<div>
        			<div>
                    	<span dir="rtl">رقم الموبايل
      					<span class="red">*</span>	
        				</span>
                    </div>
      			</div>
    		</div>
    </div>
    <div class="input_row even">
        	<div class="input_label_div">
            	<span>Email Address</span><span class="red">*</span>
                <br/><span style="font-size:10px;">(This must be your daily contact email)</span>
            </div>
    		<div class="input_div"> 
             <?php
            	if($_REQUEST['action']!="edit"){
			?>
                       	<input type="text" name="a_email_op" id="a_email_op" class="input_cls" value="<?php echo $op_email;?>"/>  
             <?php
				}else{ 
			 ?>
             			<input type="text" name="a_email_op" id="a_email_op" class="input_cls" value="<?php echo $op_email;?>" onchange="check_username(this.value);"/> 
                        <input type="hidden"  id="a_email_op1" value="<?php echo $op_email;?>" /> 
             <?php
				}
			 ?>
              
            </div>
    		<div class="input_label_div_arb">
      			<div>
        			<div>
                    	<span dir="rtl">البريد الإلكتروني
      						<span class="red">*</span>
                              <br/><span style="font-size:10px;">(سيتم التواصل معكم عبر هذا البريد الإلكتروني)</span>
        				</span>
                    </div>
      			</div>
    		</div>
    </div>
    <div class="input_row odd " style="border-top:none;">
        <div class="input_label_div"><span>Trade License</span><span class="red">*</span><br/><span>(Attach License copy)</span>
        </div>
      
        <div class="iwidth" > 
         	<div style="position:relative" >
                <input type="text" class="binput_cls" name="c_up_fname" id="c_up_fname" readonly <?php if($trd_license_file!=''){ ?>value="<?php echo $trd_license_file;?>" <?php } ?> />
                <input type="hidden" name="c_up_fname_full" id="c_up_fname_full"/>
                <input type="button" value="Select / إختر" name="c_passport_btn" id="c_passport_btn" style="width:75px; background:#E8EDE9;font-size:11px; padding:1px 0;direction:ltr;"/>
                
            </div>
              <?php
				if($_REQUEST['action']=="edit"){
					$site_path = "http://szgmc.ae/";
					//$site_path = "http://szgmc-en.cnh.ae/en/";
					$path = $site_path."crew_documents/".$trd_license_file;
					$file_path="<p style='padding-top:10px;padding-bottom:0px'><a href='".$path."' target='_blank'>Click here</a></p>";
					echo $file_path;
				}
			  ?>
          </div> 
            
            <div class="input_label_div_arb"><span dir="rtl">الرخصة التجارية <span class="red">*</span><br/><span dir="rtl">(ارفاق نسخة من الرخصة)</span></span>
            </div>
        </div>

    <div class="input_row even " >
        <div class="input_label_div"><span>Authorized Person Details</span><span class="red">*</span><br/><span>(signatory)</span>
        </div>

        <div class="iwidth" >
            <div style="position:relative" >
                <input type="text" class="binput_cls" name="signatory_fname" id="signatory_fname" readonly <?php if($trd_license_file!=''){ ?>value="<?php echo $trd_license_file;?>" <?php } ?> />
                <input type="hidden" name="signatory_fname_full" id="signatory_fname_full"/>
                <input type="button" value="Select / إختر" name="btn_signatory" id="btn_signatory" style="width:75px; background:#E8EDE9;font-size:11px; padding:1px 0;direction:ltr;"/>

            </div>
            <?php
            if($_REQUEST['action']=="edit"){
                $site_path = "http://szgmc.ae/";
                //$site_path = "http://szgmc-en.cnh.ae/en/";
                $path = $site_path."crew_documents/".$trd_license_file;
                $file_path="<p style='padding-top:10px;padding-bottom:0px'><a href='".$path."' target='_blank'>Click here</a></p>";
                echo $file_path;
            }
            ?>
        </div>

        <div class="input_label_div_arb"><span dir="rtl">
        الشخص المعتمد تفاصيل
        <span class="red">*</span><br/><span dir="rtl">(
        الموقعة
        )</span></span>
        </div>
    </div>

    <div class="input_row odd " style="border-top:none;">
        <div class="input_label_div"><span>PRO Card</span><span class="red">*</span><br/><span>(Attach PRO Card copy)</span>
        </div>

        <div class="iwidth" >
            <div style="position:relative" >
                <input type="text" class="binput_cls" name="pro_card_fname" id="pro_card_fname" readonly <?php if($trd_license_file!=''){ ?>value="<?php echo $trd_license_file;?>" <?php } ?> />
                <input type="hidden" name="pro_card_fname_full" id="pro_card_fname_full"/>
                <input type="button" value="Select / إختر" name="btn_pro_card" id="btn_pro_card" style="width:75px; background:#E8EDE9;font-size:11px; padding:1px 0;direction:ltr;"/>

            </div>
            <?php
            if($_REQUEST['action']=="edit"){
                $site_path = "http://szgmc.ae/";
                //$site_path = "http://szgmc-en.cnh.ae/en/";
                $path = $site_path."crew_documents/".$trd_license_file;
                $file_path="<p style='padding-top:10px;padding-bottom:0px'><a href='".$path."' target='_blank'>Click here</a></p>";
                echo $file_path;
            }
            ?>
        </div>


        <div class="input_label_div_arb"><span dir="rtl">
                بطاقة PRO
        <span class="red">*</span><br/><span dir="rtl">(
                    إرفاق نسخة من بطاقة PRO
                )</span></span>
        </div>
    </div>
    <div class="input_row even border_bottom">
		<div align="center">
			<div class="clear" style="padding-top:10px;direction:ltr;"  >
            	<img src="captcha.php" id="captcha" style="background:#CCC;outline:none;transition: all 0.25s ease-in-out;-webkit-transition: all 0.25s ease-in-out;-moz-transition: all 0.25s ease-in-out;border: solid 2px #D7D9E5;border-radius: 3px;
margin-left: 10px;margin-bottom: 5px;" /><br/>
                <!-- CHANGE TEXT LINK -->
                <a href="javascript:void(0)" onclick="  document.getElementById('captcha').src='captcha.php?'+Math.random();
    document.getElementById('captcha-form').focus();"
    id="change-image" style="direction:ltr;"><?php if($slug[1]=="en") { ?> Not readable? Change text. <?php } else { ?> لا يمكنني القراءة ، يرجى التغيير <?php }?></a><br/><br/>
		 </div> 
 		    <div class="clear input_label_div" style="float:left;width:230px; text-align:left; margin-top:10px;">
         		<span style="float:left;width:230px;">Type the characters from the picture into 		<span style="float:left; text-align:left">the text box</span><span class="red">*</span></span> 
         	</div>
 		 	<div class="captcha">
            	<input type="text" name="captcha" id="captcha-form" autocomplete="off" />
            </div>
 		    <div style="float:left;width:200px; text-align:right; margin-top:10px;" class="input_label_div_arb">
             	<span dir="rtl">يرجى كتابة الاحرف الظاهرة في الصورة داخل المربع</span>
             	<span class="red">*</span>
         	</div>
        	<div  class="clear" align="center">
        		<input type="submit" value="Submit &#65165;&#1585;&#1587;&#1604;"  name="submitRegistration" id="submitRegistration" class="acloginbttn" style="direction:ltr;" />
        	</div>
        </div>

	</div>
    
</form>
<script type="text/javascript">
$(document).ready(function(){

	<?php
		
		$target = '';
		if(stristr($_SERVER['REQUEST_URI'], 'tel_format')){
			if(!isValidTelFormat($phoneno)){
				
				$target = '#tel1';
			}
			if(!isValidTelFormat($op_phoneno)){
				
				$target = '#tel2';
			}
	?>
			$(function() {
				$('.old_tel1, .old_tel2').hide();
				taregtLoc = $('<?php echo $target; ?>').offset().top;
				//taregtLoc = taregtLoc - 53;
				$('html , body').animate({ scrollTop: taregtLoc }, 1000);
			});
			
	<?php } ?>
});
</script>