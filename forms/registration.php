<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" id="mediaForm" name="mediaForm" enctype="multipart/form-data">


<div class="input_row even">
<div class="input_label_div"><span>Name </span><span class="red">*</span></div>
<div class="input_div"> <input type="text" name="a_name" id="a_name" class="input_cls"/>  </div>

<div class="input_label_div_arb"><span dir="rtl">&#1575;&#1604;&#1575;&#1587;&#1605;</span><span class="red">*</span></div>
</div>

<div class="input_row odd">
<div class="input_label_div"><span>Age:</span><span class="red">*</span></div>
<div class="input_div"> 
        <select name="a_age" class="input_cls">
        <option value="">Please select - يرجي اختيار</option>
        <?php
        for($i=18;$i<=65;$i++)
        {
            ?>
            <option value="<?php echo $i;?>"><?php echo $i;?></option>
            <?php
        }
        ?>
        </select>
 </div>
<div class="input_label_div_arb"><span dir="rtl">&#1575;&#1604;&#1593;&#1605;&#1585;</span><span class="red">*</span></div>
</div>

<div class="input_row even">
<div class="input_label_div"><span>Gender</span><span class="red">*</span> </div>
<div class="input_div"> 
        <select name="a_gender" class="input_cls">
        <option value="">Please select - يرجي اختيار</option>
        <optgroup label="----------------------------------------" > </optgroup>
        <option value="ذكر">ذكر</option>
        <option value="Male">Male</option>
        <optgroup label="----------------------------------------" > </optgroup>
        <option value="انثى">انثى</option>
        <option value="Female">Female</option>

        </select>
  </div>
<div class="input_label_div_arb"><span dir="rtl">&#1575;&#1604;&#1580;&#1606;&#1587;</span><span class="red">*</span></div>
</div>

<div class="input_row odd">
<div class="input_label_div"><span>Nationality</span><span class="red">*</span></div>
<div class="input_div"> <input dir="ltr" type="text"  name="a_nationality" id="a_nationality"  class="input_cls"/>  </div>
<div class="input_label_div_arb"><span dir="rtl">&#1575;&#1604;&#1580;&#1606;&#1587;&#1610;&#1577;</span><span class="red">*</span></div>
</div>



<div class="input_row even">
<div class="input_label_div"><span>City</span><span class="red">*</span></div>
<div class="input_div"> <input type="text" name="a_city" id="a_city" class="input_cls"/>  </div>
<div class="input_label_div_arb"><span dir="rtl">&#1575;&#1604;&#1605;&#1583;&#1610;&#1606;&#1577;</span><span class="red">*</span></div>
</div>

<div class="input_row even">
<div class="input_label_div"><span>Contact No.</span><span class="red">*</span></div>
<div class="input_div"> <input type="text" name="a_telephone" id="a_telephone" class="input_cls"/>  </div>
<div class="input_label_div_arb"><span dir="rtl">&#1585;&#1602;&#1605; &#1575;&#1604;&#1607;&#1575;&#1578;&#1601;</span><span class="red">*</span></div>
</div>

<div class="input_row even">
<div class="input_label_div"><span>Email</span><span class="red">*</span></div>
<div class="input_div"> <input type="text" name="a_email" id="a_email" class="input_cls"/>  </div>
<div class="input_label_div_arb"><span dir="rtl">&#1575;&#1604;&#1576;&#1585;&#1610;&#1583; &#1575;&#1604;&#1575;&#1603;&#1578;&#1585;&#1608;&#1606;&#1610;</span><span class="red">*</span></div>
</div>

<div class="input_row odd">
<div class="input_label_div"><span>Education Level</span><span class="red">*</span>  
</div>
<div class="input_div">  <textarea class="input_cls" style="width:240px;" name="desc" id="desc" cols="25" rows="8" ></textarea></div>

<div class="input_label_div_arb"><span dir="rtl">&#1575;&#1604;&#1605;&#1587;&#1578;&#1608;&#1609; &#1575;&#1604;&#1578;&#1593;&#1604;&#1610;&#1605;&#1610;</span><span class="red">*</span></div>
</div>


 

<div class="input_row even border_bottom" >
  
<div  align="center">
<input type="submit" value="Submit &#65165;&#1585;&#1587;&#1604;" class="acloginbttn" style="direction:ltr;" />
</div>

</div>





</form>