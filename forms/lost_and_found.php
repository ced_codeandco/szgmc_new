<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" id="mediaForm" name="mediaForm" enctype="multipart/form-data">


<div class="input_row even">
<div class="input_label_div"><span>Name </span><span class="red">*</span></div>
<div class="input_div"> <input type="text" name="a_fname" id="a_fname" class="input_cls"/>  </div>

<div class="input_label_div_arb"><span dir="rtl">&#1575;&#1604;&#1575;&#1587;&#1605;</span><span class="red">*</span></div>
</div>

<div class="input_row odd">
<div class="input_label_div"><span>Passport No.:</span><span class="red">*</span></div>
<div class="input_div"> <input type="text" name="a_passport" id="a_passport" class="input_cls"/>  </div>
<div class="input_label_div_arb"><span dir="rtl">&#1585;&#1602;&#1605;&nbsp;&#1580;&#1608;&#1575;&#1586;&nbsp;&#1575;&#1604;&#1587;&#1601;&#1585;</span><span class="red">*</span></div>
</div>

<div class="input_row even">
<div class="input_label_div"><span>Nationality</span><span class="red">*</span> </div>
<div class="input_div"> <input type="text" name="a_nationality" id="a_nationality" class="input_cls"/>  </div>
<div class="input_label_div_arb"><span dir="rtl">&#1575;&#1604;&#1580;&#1606;&#1587;&#1610;&#1577;</span><span class="red">*</span></div>
</div>

<div class="input_row odd">
<div class="input_label_div"><span>Mobile No.</span><span class="red">*</span></div>
<div class="input_div"> <input dir="ltr" type="text"  name="c_telephone" id="c_telephone"  class="input_cls"/>  </div>
<div class="input_label_div_arb"><span dir="rtl">&#1585;&#1602;&#1605;&nbsp;&#1575;&#1604;&#1605;&#1608;&#1576;&#1575;&#1610;&#1604;</span><span class="red">*</span></div>
</div>



<div class="input_row even">
<div class="input_label_div"><span>Email</span><span class="red">*</span></div>
<div class="input_div"> <input type="text" name="c_email" id="c_email" class="input_cls"/>  </div>
<div class="input_label_div_arb"><span dir="rtl">&#1575;&#1604;&#1576;&#1585;&#1610;&#1583;&nbsp;&#1575;&#1604;&#1575;&#1604;&#1603;&#1578;&#1585;&#1608;&#1606;&#1610;</span><span class="red">*</span></div>
</div>

<div class="input_row odd">
<div class="input_label_div"><span>P.O.Box </span><span class="red">*</span></div>
<div class="input_div"> <input type="text" name="pobox" id="pobox" class="input_cls"/>  </div>
<div class="input_label_div_arb"><span dir="rtl">&#1589;&#1606;&#1583;&#1608;&#1602;&nbsp;&#1575;&#1604;&#1576;&#1585;&#1610;&#1583;</span><span class="red">*</span></div>
</div>

<div class="input_row even">
<div class="input_label_div"><span>Description</span><span class="red">*</span>  <br/><span>(include Color, Style, Size, Shape, etc.)</span> 
</div>
<div class="input_div">  <textarea class="input_cls" style="width:240px;" name="desc" id="desc" cols="25" rows="8" ></textarea></div>

<div class="input_label_div_arb"><span dir="rtl">الوصف</span><span class="red">*</span> <br>
  (بما في ذلك اللون، والشكل, والحجم، الخ)</div>
</div>


 <div class="input_row odd ">
<div class="input_label_div"><span>Where & When Lost?</span><span class="red">*</span></div>
<div class="input_div">  <textarea class="input_cls" style="width:240px;" name="where" id="where" cols="25" rows="8" ></textarea></div>

<div class="input_label_div_arb"><span dir="rtl">أين و متى فقدت&#1567;</span><span class="red">*</span></div>
</div>

<div class="input_row even">
<div class="clear" style="padding-top:10px;direction:ltr; text-align:center"><img src="captcha.php" id="captcha" style="background:#CCC;outline:none;
transition: all 0.25s ease-in-out;
-webkit-transition: all 0.25s ease-in-out;
-moz-transition: all 0.25s ease-in-out;
border: solid 2px #D7D9E5;
border-radius: 3px;
margin-left: 10px;
margin-bottom: 5px;
"><br>


<!-- CHANGE TEXT LINK -->
<a href="javascript:void(0)" onclick="
    document.getElementById('captcha').src='captcha.php?'+Math.random();
    document.getElementById('captcha-form').focus();" id="change-image" style="direction:ltr;">Not readable? Change text.</a><br><br>



 </div>
 <div class="clear input_label_div" style="float:left;width:250px; text-align:left; margin-top:10px;"><span style="float:left;width:230px;">Type the characters from the picture into <span style="float:left; text-align:left">the text box</span><span class="red">*</span></span> </div>
 <div class="captcha"><input type="text" name="captcha" id="captcha-form" autocomplete="off"></div>
 <div style="float:left;width:180px; text-align:right; margin-top:10px;" class="input_label_div_arb"><span dir="rtl">يرجى كتابة الاحرف الظاهرة في الصورة داخل المربع</span><span class="red">*</span></div>
</div>

<div class="input_row add border_bottom" >
  
<div  align="center">
<input type="submit" value="Submit &#65165;&#1585;&#1587;&#1604;" class="acloginbttn" style="direction:ltr;" />
</div>

</div>





</form>