<?php // $Id: GearsMailer.php 44 2010-03-04 17:01:22Z pglh $
class GearsMailerException extends Exception { }

/**
 * A generalised class for sending HTML emails and attachments. Check it out - it's boss!
 *
 * This is GearsMailer, formerly known as MegaMail, renamed to work with autoload and PHP5ed.
 */
class GearsMailer
{
	public $from = 'clicks N hits';
	public $fromAddress = 'vyshak.p@timegroup.ae';

	private $_to = ''; # Use constructor
	private $_subject = ''; # Use constructor
	private $_text = ''; # use setText()
	private $_html = ''; # use setHTML()
	private $_attachments = array(); # use addAttachment()

	# PHP5 constructor
	public function __construct($to, $subject)
	{
		$this->_to = $to;
		$this->_subject = $subject;
	}

	public function setText($text)
	{
		$this->_text = $text;
	}

	public function setTextFromTemplate($tpl_name, $context, $common = false)
	{
		$this->_text = eval_tpl($tpl_name, $context, 'txt', $common);
	}

	public function getText()
	{
		return $this->_text;
	}

	public function getHTML()
	{
		return $this->_html;
	}

	public function setHTML($html)
	{
		$this->_html = $html;
	}

	public function setHTMLFromTemplate($tpl_name, $context, $common = false)
	{
		$this->_text = eval_tpl($tpl_name, $context, 'html', $common);
	}

	public function changeRecipient($newTo)
	{
		$this->_to = $newTo;
	}

	public function addAttachment($filename, $mimeType, $content)
	{
		$this->_attachments[] = array('filename' => $filename, 'mimeType' => $mimeType, 'content' => $content);
		return sizeof($this->_attachments);
	}

	public function addAttachmentFromFile($filename, $mimeType, $path)
	{
		$content = file_get_contents($path);
		$this->_attachments[] = array('filename' => $filename, 'mimeType' => $mimeType, 'content' => $content);
		return sizeof($this->_attachments);
	}

	public function removeAttachment($id)
	{
		unset($this->_attachments[$id-1]);
	}

	public function send()
	{
		$date = date( 'r' );
		$phpversion = phpversion();
		$boundary = md5( microtime() );
		
		if( $this->_html )
		{
			# Boundry for marking the split & Multitype Headers
			$headers = "From: {$this->from} <{$this->fromAddress}>\nDate: $date\nReply-To: {$this->from} <{$this->fromAddress}>\nReturn-Path: {$this->from} <{$this->fromAddress}>\nX-Mailer: PHP v$phpversion\nMIME-Version: 1.0\nContent-Type: multipart/mixed; boundary=\"$boundary\"\n\n";

			# Open the first part of the mail
			$htmlalt_mime_boundary = $boundary."_htmlalt"; # we must define a different MIME boundary for this section
			$message = "--$boundary\nContent-Type: multipart/alternative; boundary=\"$htmlalt_mime_boundary\"\n";

			# Text Version
			$message .= "--$htmlalt_mime_boundary\nContent-Type: text/plain; charset=iso-8859-1\nContent-Transfer-Encoding: 8bit\n\n{$this->_text}\n\n";

			# HTML Version
			$message .= "--$htmlalt_mime_boundary\nContent-Type: text/html; charset=iso-8859-1\nContent-Transfer-Encoding: 8bit\n\n{$this->_html}\n\n";

			# close the html/plain text alternate portion
			$message .= "--$htmlalt_mime_boundary--\n\n";
		}
		else
		{
			$headers = "From: {$this->from} <{$this->fromAddress}>\nDate: $date\nReply-To: {$this->from} <{$this->fromAddress}>\nReturn-Path: {$this->from} <{$this->fromAddress}>\nX-Mailer: PHP v$phpversion\nMIME-Version: 1.0\nContent-Type: multipart/related; boundary=\"$boundary\"";
			$message = "--$boundary\nContent-Type: text/plain; charset=\"iso-9959-1\"\nContent-Transfer-Encoding: 7bit\n\n";
			$message .= $this->_text;
		}
		
		foreach( $this->_attachments as $attachment )
		{
			$message .= "\n\n--$boundary\nContent-Type: octet-stream; name=\"{$attachment['filename']}\"\nContent-Disposition: attachment; filename=\"{$attachment['filename']}\"\nContent-Transfer-Encoding: base64\n\n";
			$message .= chunk_split( base64_encode( $attachment['content'] ) );
		}

		$message .= "\n\n--$boundary--\n";

		if( ! mail($this->_to, $this->_subject, $message, $headers ) )
		{
			throw new GearsMailerException('fail');
		}
	}
}
?>