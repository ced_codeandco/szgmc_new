<?php
include './includes/database.php';
include './includes/functions.php';
include './includes/config.php';
include_once('./includes/generic_functions.php');
$conf = new Configuration();
$db = new MyDatabase();
$site_path = $conf->site_url;

$slug = explode('/',$_SERVER['REQUEST_URI']);
$slug = end($slug);

$current_page = 'ftplogin';
$current_page = isset($_GET['current_page'])?mysql_real_escape_string($_GET['current_page']):1;


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>نتائج استطلاع الرأي</title>
<link href="<?php echo $site_path; ?>css/calendar.css" rel="stylesheet" type="text/css" />
<?php include 'includes/common_header.php'; ?>
<link href="<?php echo $site_path; ?>css/news.css" rel="stylesheet" type="text/css" />
<style>
	.pollElemWrap{
	min-height: 356px;
	background: #F7F8FA;
	border: 1px solid #E5E5E5;
	width: 697px;
	padding: 13px 19px 20px 0px;
	margin-top: 13px;
}
</style>
<script type="text/javascript">
        var is_ie = navigator.userAgent.toLowerCase().indexOf('ie') > -1;
        if(is_ie){
                $(window).load(function() {
   					$('.pollElemWrap').removeClass('pollElemWrap');
					$('#me').addClass('pollElemWrap');
				});
			}
</script>
</head>
<body>

<?php include 'includes/menus/banner_header3.php'; ?>		
			<!-- Banner Start -->
<div class="banner">
	<img src="<?php echo $site_path;?>images/visiting_the_mosque_banner.jpg">     
</div>
<!-- Banner Close -->	
<!-- Content Start -->
	<div class="main_box_content">
	 <div id="dialognew" style="display:none;">
        	<p style="text-align:right;">لقد تم تقديم طلبكم بنجاح ،</p>
            <p><span dir="ltr">Your request has been submitted successfully</span></p>
            <p style=""><input type="button" value="OK" style="float:right;width:40px" id="confirmnew"></p>
        </div>
		 <?php include 'includes/menus/left_menu.php'; ?>
        <div class="clear"></div>
        <div class="content">
        	<div class="brad_cram">
            	<ul>
            	   <li><a href="<?php echo $site_path; ?>">الصفحة الرئيسية</a></li>
                   <li><a href="javascript:void(0);" class="">البيانات المفتوحة</a></li>
                    <li><a href="#" class="active">نتائج استطلاع الرأي</a></li>
                </ul>
            </div>
      
      		<div class="content-left">
			 
                <br class="clear"/>
                <?php 
				include 'includes/menus/ministry_logos.php'; 
				?>
                <?php include 'includes/menus/rightsidebanner.php';?>
			</div>
	  		<div class="content-right" style="margin-right:30px">
			<div class="single_middle">
		<?php 
        include 'includes/menus/marquee.php'; 
        ?>
			<p class="heading"><span dir="rtl">نتائج استطلاع الرأي</span></p>
				 
			<div class="clear page_item_single">
				<div class="page_content" >
					<div class="general_body_content">
						<div id="pollWrapper">
								<?php 
								include("./includes/poll_archive.php");

						while($row_id = mysql_fetch_array($result_id))
						{
							
							if($counter % 3 == 0){
								echo '<div class="pollElemWrap">';
							}
								$poll_id = $row_id['pollID'];
								echo "<div class='poll_archive'>";
								getpoll_archive($poll_id);
								echo "</div>";
								
							if($counter % 3 == 2 ){
								echo '<div class="clear"></div>';
								echo '</div>';
							}
							$counter++;
						}
						/*if( $counter<=2||($counter<6&&$counter>3) ){
							echo '</div>';
						}*/
						if($counter % 3 != 2){
								echo '<div class="clear"></div>';
								echo '</div>';
						}
					  ?>
						</div>		
														  
						<div class="clear">&nbsp;</div>
						<div id="news_controller" class="clear">
							<span class="<?php echo ($current_page>1)?'previous_active':'previous_inactive'; ?>">
							<?php if($current_page>1) { ?>
								<a href="<?php echo $site_path; ?>poll-archive/<?php echo $current_page-1; ?>" >السابق</a>
							<?php } else { echo 'السابق'; } ?>
							</span>
							
							
							<?php 
							$last_page = $total_polls % $polls_per_page == 0 ? $total_polls / $polls_per_page : $total_polls / $polls_per_page + 1;
							for($i=1;$i<=($last_page);$i++) {
							?>
							<span <?php echo $i == $current_page ? 'class="current"' : ''; ?>><a href="<?php echo $site_path; ?>poll-archive/<?php echo $i; ?>" ><?php echo $i; ?></a></span>
							<?php } ?>
							<span class="<?php echo ($current_page<($total_polls/$polls_per_page)+1)?'next_active':'next_inactive'; ?>">

							<?php if($current_page<($total_polls/$polls_per_page)) { ?>
								<a href="<?php echo $site_path; ?>poll-archive/<?php echo $current_page+1; ?>" >التالي</a>
							<?php } else { echo 'التالي'; } ?>
							</span>
						</div>
					</div>
				</div>
				<br class="clear" />
			</div>
		</div>
			</div>
 <br class="clear" />      
        </div>
    
   <div class="clear"></div> </div>

<div class="content_bottom">&nbsp;</div>
<?php include 'includes/footer.php'; ?>
</body>
</html>
