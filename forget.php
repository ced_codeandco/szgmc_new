<?php
include './en/includes/database.php';
include './includes/functions.php';
include './includes/config.php';
include_once('./includes/generic_functions.php');
$conf = new Configuration();
$db = new MyDatabase();
$site_path = $conf->site_url;

$slug = explode('/',$_SERVER['REQUEST_URI']);
$slug = end($slug);
session_start();
$msg="";
$current_page = 'ftplogin';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<?php include 'includes/common_header.php'; ?>
    <title>نسيت كلمة المرور</title>

	<link href="<?php echo $site_path; ?>css/calendar.css" rel="stylesheet" type="text/css" />

 

	<script type="text/javascript" src="<?php echo $site_path; ?>js/ajaxupload.3.5.js"></script>
	<script type="text/javascript" src="<?php echo $site_path; ?>js/jquery.validate.js"></script>
	<script type="text/javascript" src="<?php echo $site_path; ?>js/main.js"></script>
	<script type="text/javascript" src="<?php echo $site_path; ?>js/cal.js"></script>
    
<link href="<?php echo $site_path; ?>css/jquery-ui-1.8.24.custom.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo $site_path; ?>js/jquery-ui-1.8.24.custom.min.js"></script>

<style>
td{text-align:center;}
textarea{width:660px;}
div.ltr{
direction: ltr;
}
table.ltr {
direction: ltr;
}
</style>


</head>
<body >
<?php include 'includes/menus/banner_header3.php'; ?>		
			<!-- Banner Start -->
<div class="banner">
	<img src="<?php echo $site_path;?>images/visiting_the_mosque_banner.jpg">     
</div>
<!-- Banner Close -->	
<!-- Content Start -->
	<div class="main_box_content visiting_page_height">
	 	 <?php include 'includes/menus/left_menu.php'; ?>
        <div class="clear"></div>
        <div class="content">
        	<div class="brad_cram">
            	<ul>
            	   <li><a href="<?php echo $site_path; ?>">الصفحة الرئيسية</a></li>
                    <li><a href="#" class="active">نسيت كلمة المرور</a></li>
                </ul>
            </div>
      
      		<div class="content-left">
			 <?php //include 'includes/menus/left_menu1.php'; 
				?>
				
                <br class="clear"/>
                <?php 
				include 'includes/menus/ministry_logos.php'; 
				?>
                <?php include 'includes/menus/rightsidebanner.php';?>
			</div>
	  		<div class="content-right" style="margin-right:30px">
			<div class="single_middle">
                    <p class="heading"><span dir="rtl">نسيت كلمة المرور</span></p>
                    <div class="page_item_single">
                    	<div class="page_content" >
                            <div class="general_body_content">
							 <div class="reg_form">
                               <div class="text" style="float:right; text-align:right; margin-right:8px;">
	       
    <p style="width:500px"><span dir="rtl">
         لقد تم إرسال كلمة المرور الخاصة بك إلى عنوان بريدك الإلكتروني المسجل ، يرجى التأكد من ذلك ...
     </span></p>
      <p style="text-align:center"><a href="./userlogin.php">دخول</a>
      </p>
    </div>


    <div class="clear"></div>
</div>
							</div>
                            <br class="clear" />
                            
                        </div>
                    </div> 
                   
            </div>
			</div>
 <br class="clear" />      
        </div>
    
    </div>

	<div class="content_bottom">&nbsp;</div>
	<?php include 'includes/footer.php'; ?> 
	
</body>
</html>