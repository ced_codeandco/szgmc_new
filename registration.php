<?php
include './includes/database.php';
include './includes/functions.php';
include './includes/config.php';
include_once('./includes/generic_functions.php');
include_once('./forms/notification_email.php');
session_start();
$conf = new Configuration();
$db = new MyDatabase();
$site_path = $conf->site_url;

$slug = explode('/',$_SERVER['REQUEST_URI']);
$slug = end($slug);

if (!empty($_REQUEST['a_captcha'])) {


    if (empty($_SESSION['captcha']) || trim(strtolower($_REQUEST['a_captcha'])) != $_SESSION['captcha']) {
        $msg="wrong_captcha";
    }
    else
    {

        $enq_mail_from='info@szgmc.ae';
        $enq_mail_to='ab.almarzooqi@szgmc.ae';
        $enq_mail_cc= 'it@szgmc.ae';

        define("ENQ_FROM",$enq_mail_from);
        define("ENQ_TO",$enq_mail_to);
        define("ENQ_CC",$enq_mail_cc);
        $msg="";
        $current_page = 'ftplogin';


        if(isset($_POST['a_name']) && $_POST['a_name']!='' ){

            foreach($_POST as $name => $value) {
                $$name=mysql_real_escape_string($value);
            }
            $fn = "./regid.txt";
            $hits = file($fn);
            $hits[0] ++;


            $fp = fopen($fn , "w");
            fputs($fp , "$hits[0]");
            fclose($fp);
            $ref_id=str_pad($hits[0],3,"0",STR_PAD_LEFT);

            $msg='success';
            $file_flag=0;
            $sql_str="INSERT INTO `quran_recitation_form` ( `name`, `age`, `gender`, `nationality`, `city_name`, `contact_no`, `email`, `education_level`, `memorization_level`, `already_participated`,`created_date_time`, `created_ip`) VALUES(
              '$a_name','$a_age','$a_gender','$a_nationality','$a_city','$a_telephone','$a_email','$desc','$memorization_level','$course_partticipation','".date('Y-m-d H:i:s')."', '".getClientIp()."'
            )";
            $conn = $db->getConnection();
            mysql_query("set names utf8", $conn);
            mysql_query("set collation_connection = @@collation_database", $conn);
            $in_id=0;
            //echo "<br /><br />".$sql_str."<br /><br />";
            mysql_query($sql_str, $conn) or die("Query error");//die();
                $in_id=mysql_insert_id();

            if (!empty($in_id)) {
                $ref_id = "QR" . "-" . str_pad($in_id, 4, '0', STR_PAD_LEFT);
                $mailContent = getregistationContent($_POST, $ref_id);
                $sql = "update quran_recitation_form set details='" . addslashes($mailContent) . "' where id='$in_id' ";
                mysql_query($sql, $conn);

                if ($msg == 'success') {

                    $fromName = 'Quran Recitation';
                    $fromEmail = ENQ_FROM;
                    $toName = 'SZGMC';
                    $toEmail = ENQ_TO;
                    $subject = "Quran Recitation";

                    $toEmail = 'ab.almarzooqi@szgmc.ae';
                    $cc = 'it@szgmc.ae';
                    $bcc = '';
                    sendEmail($toName, $fromName, $toEmail, $fromEmail, $subject, $mailContent, '', $cc, $bcc);

                    /*$email_sql="select email,name from service_email where frm_id='63'";
                    $email_querry= mysql_query($email_sql);
                    while($email_res=mysql_fetch_object($email_querry))
                    {
                        $toName = $email_res->name;
                        $toEmail = $email_res->email;
                        sendEmail($toName, $fromName, $toEmail, $fromEmail, $subject, $content);
                    }*/

                }
                header("Location:".$site_path.'quran_recitation_course?success=1');
                exit;
            }

        }
    }
}
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>استمارة التسجيل في دورة تجويد القرآن</title>
<link href="<?php echo $site_path; ?>css/calendar.css" rel="stylesheet" type="text/css" />
<?php include 'includes/common_header.php'; ?>
<script type="text/javascript" src="<?php echo $site_path; ?>js/jquery.validate.js"></script>
<link href="<?php echo $site_path; ?>css/jquery-ui-1.8.24.custom.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo $site_path; ?>js/jquery-ui-1.8.24.custom.min.js"></script>

    <script type="text/javascript">

        $(document).ready(function(){
            $("#mediaForm").validate({
                rules: {
                    a_name: "required",
                    a_age: "required",
                    a_gender: "required",
                    a_nationality: "required",
                    a_city: "required",
                    a_captcha: "required",
                    a_email: {
                        required: true,
                        email: true
                    },
                    a_telephone: "required",
                    desc: "required",
                    course_partticipation: "required"

                }
            });
        });
    </script>
<style>
.heading{float:none;padding-right:0;text-align:right}
#table caption{margin-bottom:10px;text-align:right}
#table table{margin:20px 0 10px;width:100%}
</style>
</head>

<body>
<?php include 'includes/menus/banner_header3.php'; ?>		
			<!-- Banner Start -->
<div class="banner">
	<img src="<?php echo $site_path;?>images/visiting_the_mosque_banner.jpg">     
</div>
<!-- Banner Close -->	
<!-- Content Start -->
	<div class="main_box_content visiting_page_height">
	 	 <?php include 'includes/menus/left_menu.php'; ?>
        <div class="clear"></div>
        <div class="content">
        	<div class="brad_cram">
            	<ul>
            	   <li><a href="<?php echo $site_path; ?>">الصفحة الرئيسية</a></li>
                    <li><a href="#" class="active">استمارة التسجيل في دورة تجويد القرآن</a></li>
                </ul>
            </div>
      
      		<div class="content-left">
			 <?php //include 'includes/menus/left_menu1.php'; 
				?>
				
                <br class="clear"/>
                <?php 
				include 'includes/menus/ministry_logos.php'; 
				?>
                <?php include 'includes/menus/rightsidebanner.php';?>
			</div>
	  		<div class="content-right" style="margin-right:30px">
			<div class="single_middle">
	                <p class="heading"><span dir="rtl">تسجيل الشركات السياحية</span></p>

                <div id="table">

                    <p style="direction: rtl;text-align: justify; ">انطلاق دورة تحفيظ وتجويد القرآن الكريم لليافعين (ذكور وإناث)، حيث يتم في الدورة تحفيظ القرآن وتعليم أحكام التجويد، وسوف يتم تحديد مستوى الطالب وذلك من قبل لجنة إدارية مختصه من هيئة الأوقاف وذلك لضمان تقسيم المستويات بشكل صحيح.</p> 
                    <table>
                        <tbody><tr>
                            <th colspan="2">تاريخ انعقاد الدورة</th>
                        </tr>
                        <tr>
                            <th>من</th>
                            <th>إلى</th>
                        </tr>
                        <tr>
                            <td>21 فبراير 2016</td>
                            <td>19 مايو 2016</td>
                        </tr>
                        </tbody></table>
                    <table>

                        <tbody><tr>
                            <th>الفئة</th>
                            <th>الأيام</th>
                            <th>التوقيت</th>
                        </tr>
                        <tr>
                            <td>الذكور</td>
                            <td>الأحد و الثلاثاء</td>
                            <td>6:00 م إلى 07:30 م</td>
                        </tr>
                        <tr>
                            <td>الإناث</td>
                            <td>الاثنين والأربعاء</td>
                            <td>4:30 م إلى 06:00 م</td>
                        </tr>
                        </tbody></table>
                </div>
                    <div class="page_item_single">
                    	<div class="page_content" >
                            <div class="general_body_content">
							 <div class="reg_form">
                              <p><span dir="rtl">يرجى تعبئة النموذج أدناه للاشتراك في دورة تحفيظ القرآن و سيتم التواصل مع المسجل خلال 3 أيام عمل</span></p>
    							<?php include './forms/registration_2.php'; ?>

    <div class="clear"></div>
</div>
							</div>
                            <br class="clear" />
                            
                        </div>
                    </div> 
                   
            </div>
			</div>
 <br class="clear" />      
        </div>
    
    </div>

<div class="content_bottom">&nbsp;</div>
<?php require 'includes/footer.php'; ?>
<?php
	
	   if(!empty($_GET['success']) && $_GET['success']== '1'){
	  
		 ?>
		<script>
			$(function() {
				$( "#dialog" ).dialog();
			});
		</script>
        <script type="text/javascript">
		$(function(){
			$('#confirm').click(function() {
				//$('#dialog').hide();
				window.location = "/quran_recitation_course";
				$( "#dialog" ).dialog( "close" );
				
				return false;
				
			});
		});
		
        </script>
        <div id="dialog">
        	<p style="text-align:right;">لقد تم تقديم طلبك بنجاح ، وسوف نقوم بالتواصل معك قريبا </p>
            <p style="direction:ltr;">Your request has been submitted successfully. We will contact you shortly.</p>
            <p style=""><input type="button" value="OK" style="float:right;width:40px" id="confirm"></p>
        </div>
        
        <?php
		 
		}
		else if($msg == 'failed')
		{
		?>
		  <script> alert("Cannot process your request"); </script>
		 <?php
		 print('<script type="text/javascript">window.location = "./quran_recitation_course";</script>');
exit(); 
		}
	  else if($msg == 'wrong_captcha')
		{
		?>
		<script> alert("You entered wrong captcha"); </script>
		<?php
				 print('<script type="text/javascript">window.location = "./quran_recitation_course";</script>');
exit(); 
		}
	?>
	
	

</body>
</html>
