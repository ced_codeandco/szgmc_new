<?php
ob_start();
include 'includes/database.php';
include 'includes/dal/pages.php';
include 'includes/functions.php';
include 'includes/config.php';

$slug = explode('/',$_SERVER['REQUEST_URI']);
$slug = end($slug);

//echo $slug;
//print_r($slug);
//die();
 
$conf = new Configuration();

$db = new MyDatabase();
$dal_pages = new ManagePages();
$page = $dal_pages->getPage($slug);
//print_r($page);
if(($_SERVER['REQUEST_URI']!= $page->url))
					{
						header("location:/404.php");
					}
$site_path = $conf->site_url;

$main_menu = $conf->getCurrentMainPage($slug);
if($main_menu=="szgmc")
{
$main_menu2="about-szgmc";
$main_menu1="&#1593;&#1606; &#1575;&#1604;&#1605;&#1585;&#1603;&#1586;";
}
else if($main_menu=="founder")
{
$main_menu2="founder";
$main_menu1="&#1575;&#1604;&#1602;&#1575;&#1574;&#1583; &#1575;&#1604;&#1605;&#1572;&#1587;&#1587;";
}
else if($main_menu=="about")
{
$main_menu2="vision-dream-sheikh-zayed-mosque";
$main_menu1="&#1593;&#1606; &#1580;&#1575;&#1605;&#1593; &#1575;&#1604;&#1588;&#1610;&#1582; &#1586;&#1575;&#1610;&#1583;";
}
else if($main_menu=="archi")
{
$main_menu2="general-architecture";
$main_menu1="الجماليات والعمارة";
}
else if($main_menu=="planvisit")
				{
					$main_menu1 = "زيارة الجامع";
					$main_menu2= "visiting-the-mosque";
				}
				else if($main_menu=="events")
				{
					$main_menu1 = "برامج الدينية";
					$main_menu2= "events-and-activities";
				}
				else if($main_menu=="mediacenter")
				{
					$main_menu1 = "الركن الإعلامي";
					$main_menu2= "javascript:void(0);";
				}
				else if($main_menu=="library")
				{
					$main_menu1 = "المكتبة";
					$main_menu2= "javascript:void(0);";
				}
				else
				{
					$main_menu1 = $main_menu;
					$main_menu2 = $main_menu;
				}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $page->title; ?> - مركز جامع الشيخ زايد الكبير</title>
<?php include 'includes/common_header.php'; ?>
<script type="text/javascript">
$( document ).ready(function() {
$('dd').hide();
$('dt').click(function() {
var toggle = $(this).nextUntil('dt');
toggle.slideToggle();
$('dd').not(toggle).slideUp();
});
});
</script>

<style>
.page_42 table td,
.page_42 table td p,
.page_42 table td span,
.page_42 table td p span{
	line-height: normal !important;
	font-size: 16px !important;
	font-family: Arial, Helvetica, sans-serif !important;
	color: #464646 !important;
}
.page_42 table td p{
	margin: 0 0 10px !important;
}

.page_42 table table#tab {
    margin-left: 9px !important;
    margin-right: 0 !important;
}

div.single_middle .page_42 h2{
	width: 712px !important;
}

.page_42 form input {
	padding: 10px;
	width: 490px;
}

.page_42 form textarea{
	width: 508px !important;
}

.page_42 img#captcha{
	width: 98% !important;
	margin: 0 !important;
}

.page_42 .acloginbttn {
    padding: 0 !important;
    width: 121px !important;
}

.page_42 #captcha-form {
	width: 150px !important;
	float: right !important;
}

.page_42 .frm_middle {
	padding-bottom: 30px !important;
}

.page_42 #form_middle {
	padding-bottom: 0 !important;
	height: 395px !important;
}

.page_42 .ajax img {
	border: 2px solid #eee;
}

.page_42 .td_bg{
	width: 115px !important;
}

.page_161 dt {
	cursor: pointer !important;
}

.page_161 dt:hover strong { color: #bba368 !important; }

.general_body_content.page_161 span {
 	background: #bba368 none repeat scroll 0 0;
    color: #fff;
    display: inline-block;
    padding: 10px 15px;
    width: auto;
	border-radius: 2px;
	-moz-border-radius: 2px;
	-webkit-border-radius: 2px;
}
</style>
</head>

<body>
<?php include 'includes/menus/banner_header3.php'; ?>		
			<!-- Banner Start -->
<div class="banner">
	<img src="images/visiting_the_mosque_banner.jpg">     
</div>
<!-- Banner Close -->	
<!-- Content Start -->
	<div class="main_box_content">
	 	 <?php include 'includes/menus/left_menu.php'; ?>
        <div class="clear"></div>
        <div class="content">        
        	<div class="brad_cram">
            	<ul>
            	   <li><a href="<?php echo $site_path; ?>">الصفحة الرئيسية</a></li>
                    <li><?php if($main_menu1 == '') { ?>
                    <a href="#" class="active"><?php echo $page->title; ?></a>
                    <?php } else { 
					if($slug!="founder")
					{
					?>
                   <a href="<?php echo $main_menu2; ?>"><?php echo ucfirst($main_menu1);  ?></a>
                    <?php
					} 
					?>
                     
                    <a href="#" class="active"><?php echo $page->title; ?></a>
                    <?php } ?></li>
                </ul>
            </div>
      
      		<div class="content-left">
			 <?php //include 'includes/menus/left_menu1.php'; 
				?>
				
                <br class="clear"/>
                <?php 
				include 'includes/menus/ministry_logos.php'; 
				?>
                 <?php include 'includes/menus/rightsidebanner.php';?>
			</div>
	  		<div class="content-right" style="margin-right:30px">
           
			<div class="single_middle">
		<?php 
        include 'includes/menus/marquee.php'; 
        ?>
					
                    <?php
                    $news_url= $site_path."news-detail/".string_to_filename($page->title).'-'.$page->p_id;
                    ?>
                    <div class="clear page_item_single">
                    	<div class="page_content uniform-h2-wrap" >
                            <div class="general_body_content page_<?php echo $page->p_id;?>  <?php if($page->p_id==42){ echo 'contact_us_page'; }?>"><?php
							if($page->php_content=='Y' && $page->p_id!='')
echo eval("?>".$page->content."<?"); 
else
							echo $page->content; ?></div>
                            <br class="clear" />
                            
                        </div>
                    </div> 
                    <div class="clear bottom_line">&nbsp;</div>
            </div>
			</div>
 <br class="clear" />      
        </div>
    
    <div class="clear"></div></div>

<div class="content_bottom">&nbsp;</div>
<?php require 'includes/footer.php'; ?>

<script src="<?php echo $site_path;?>js/colorbox/jquery.colorbox.js"></script>
<link rel="stylesheet" href="<?php echo $site_path;?>css/colorbox/colorbox.css" type="text/css"/>
<script type="text/javascript">
    $(document).ready(function(){
        if (current_tab == 'getting-to-the-mosque' ) {
            $(".ajax").colorbox();
        }
    })
</script>
</body>
</html>
