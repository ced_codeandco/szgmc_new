// JavaScript Document
var tpj=jQuery;
				
									tpj.noConflict();
								
				var revapi1;
				
				tpj(document).ready(function() {
				
				if (tpj.fn.cssOriginal != undefined)
					tpj.fn.css = tpj.fn.cssOriginal;
				
				if(tpj("#rev_slider_1_1").revolution == undefined)
					revslider_showDoubleJqueryError("#rev_slider_1_1");
				else
				   revapi1 = tpj("#rev_slider_1_1").show().revolution(
					{
						delay:9000,
						startwidth:960,
						startheight:488,
						hideThumbs:200,
						
						thumbWidth:100,
						thumbHeight:50,
						thumbAmount:4,
						
						navigationType:"none",
						navigationArrows:"nexttobullets",
						navigationStyle:"round",
						
						touchenabled:"on",
						onHoverStop:"on",
						
						navOffsetHorizontal:0,
						navOffsetVertical:20,
						
						shadow:0,
						fullWidth:"on",

						stopLoop:"off",
						stopAfterLoops:-1,
						stopAtSlide:-1,

						shuffle:"off"
					});
				
				});	//ready