/**
 * Created by USER on 2/7/2016.
 */

function initializeFileUploadGlobal(input_id, validate_file_type) {
    var btnUpload = $('#'+input_id+'_btn');
    var statusInput = $('#'+input_id+'_status');
    var statusWrap = $('#'+input_id+'_status_msg');
    var fileNameInput = $('#'+input_id);
    var fileNameHiddenInput = $('#'+input_id+'_fname');
    console.log('#'+input_id+'_status_msg' + '   ' + statusWrap.length);
    console.log(statusWrap);
    new AjaxUpload(btnUpload, {
        appendParent: btnUpload.parent(),
        action: '/en/upload-file.php',
        name: 'uploadfile',
        onSubmit: function(file, ext){
            if (typeof validate_file_type == 'undefined' ||  validate_file_type != false) {
                if (!(ext && /^(jpg|png|jpeg|gif|pdf|doc|docx)$/.test(ext))) {
                    // extension is not allowed
                    statusWrap.text('Only PDF,JPG, PNG, GIF, DOC or DOCX files are allowed.');
                    statusWrap.show();
                    setTimeout(function () {
                        statusWrap.text('');
                    }, 2000);

                    return false;
                }
            }
            statusWrap.text('Uploading...');
        },
        onComplete: function(file, response){

            //On completion clear the status
            statusWrap.text('Processing..');
            response=response.split("#",2);

            if(response[0]==="success"){
                fnme=response[1];

                s_pos=fnme.indexOf('_',0)+1;
                len=fnme.length;

                $("#statusInput").val(fnme);
                $(fileNameInput).val(response[1]);
                $(fileNameHiddenInput).val(response[1]);
                statusWrap.text('');

            }else if(response==="file_big"){
                statusWrap.text('File is too big...');
            } else{
                statusWrap.text('Error Found.. Please try later');
            }
            setTimeout(function(){
                statusWrap.text('');
            },2000);
        }

    });
}
