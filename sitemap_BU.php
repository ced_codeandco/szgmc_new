<?php
include 'includes/database.php';
include 'includes/functions.php';
include 'includes/config.php';

$slug = explode('/',$_SERVER['REQUEST_URI']);
$slug = end($slug);
$conf = new Configuration();
$db = new MyDatabase();

$site_path = $conf->site_url;

$main_menu = $conf->getCurrentMainPage($slug);

$conf->site_description = 'What others like King Fahad, Queen Elizbeth, Jimmy Carter and other prominent leaders say about Sheikh Zayed. Find all about the Sheikh Zayed Grand Mosque in Abu Dhabi including, visiting timings, how to get to the mosque, dress code, tours, history, architecture and more.';

$conf->site_keywords = 'grand mosque, sheikh zyed grand mosque, mosque in adu dhabi, what to wear in mosque, how to get to mosque, history of grand mosque, architecture of grand mosque, grand mosque photos';

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>خريطة الموقع</title>
    <?php include 'includes/common_header.php'; ?>
    <link href="<?php echo $site_path; ?>css/sitemap.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<?php echo $site_path; ?>js/jquery-ui-1.8.21.custom.min.js"></script>
    <script type="text/javascript">
		$(document).ready(function() {
			//$("#accordionGiftLelo").msAccordion({vertical:true});
			$( "#accordionGiftLelo" ).accordion({ autoHeight: false });
		});
	</script>
<style>
div.single_middle div.saidby {
border-right: #ccc solid 1px;
border-left: #ccc solid 1px;
border-bottom: #ccc solid 1px;

background: #fff; 

}
div.single_middle div.saidby ul li{color:#000; padding:8px; list-style:inside; direction:rtl;}
div.single_middle div.saidby ul li a{color:#000;  text-decoration:none; direction:rtl; }
</style>	
<script type="text/javascript">
function redirect_link(a)
{
window.location = a;
}
//-->
</script>
</head>

<body>
<?php include 'includes/menus/banner_header3.php'; ?>		
			<!-- Banner Start -->
<div class="banner">
	<img src="<?php echo $site_path;?>images/visiting_the_mosque_banner.jpg">     
</div>
<!-- Banner Close -->	
<!-- Content Start -->
	<div class="main_box_content">
	 	 <?php include 'includes/menus/left_menu.php'; ?>
        <div class="clear"></div>
        <div class="content">
        	<div class="brad_cram">
            	<ul>
            	   <li><a href="<?php echo $site_path; ?>">الصفحة الرئيسية</a></li>
                    <li><a href="#" class="active">خريطة الموقع</a></li>
                </ul>
            </div>
      
      		<div class="content-left">
			 <?php //include 'includes/menus/left_menu1.php'; 
				?>
				
                <br class="clear"/>
                <?php 
				include 'includes/menus/ministry_logos.php'; 
				?>
                <?php include 'includes/menus/rightsidebanner.php';?>
			</div>
	  		<div class="content-right" style="margin-right:30px">
			<div class="single_middle">
		<?php 
        include 'includes/menus/marquee.php'; 
        ?>
                	<h2 style="color:#BC8545">خريطة الموقع</h2>
                    <br class="clear" />
                    
                	<div id="accordionGiftLelo">
                      <h3><a href="<?php echo $site_path; ?>about-the-center">&#1593;&#1606; &#1575;&#1604;&#1605;&#1585;&#1603;&#1586;</a></h3>
                      
                      <div class="saidby">
                    <ul>
			 <li><a href="<?php echo $site_path; ?>about-the-center">&#1606;&#1576;&#1584;&#1577; &#1593;&#1606; &#1575;&#1604;&#1605;&#1585;&#1603;&#1586;</a></li>
			 <li><a href="<?php echo $site_path; ?>minister-hh">كلمة سمو الشيخ منصور بن زايد</a></li>
			 <li><a href="<?php echo $site_path; ?>chairmans-message">كلمة رئيس مجلس الأمناء</a></li>
			 <li><a href="<?php echo $site_path; ?>DG-message">كلمة المدير</a></li>
			 <li><a href="<?php echo $site_path; ?>board-members">أعضاء مجلس الأمناء</a></li>
			 <li ><a href="<?php echo $site_path; ?>organizational-structure">&#1575;&#1604;&#1607;&#1610;&#1603;&#1604; &#1575;&#1604;&#1578;&#1606;&#1592;&#1610;&#1605;&#1610;</a></li>
			</ul>
                      </div>
                      <h3><a href="<?php echo $site_path; ?>founding-father">&#1575;&#1604;&#1602;&#1575;&#1574;&#1583; &#1575;&#1604;&#1605;&#1572;&#1587;&#1587;</a></h3>
                      <div class="saidby">
                		 <ul>
			 <li ><a href="<?php echo $site_path; ?>founding-father">&#1605;&#1587;&#1610;&#1585;&#1577; &#1602;&#1575;&#1574;&#1583;</a></li>
			 <li><a href="<?php echo $site_path; ?>selected-sayings-by-sheikh-zayed-al-nahyan">&#1605;&#1606; &#1571;&#1602;&#1608;&#1575;&#1604; &#1575;&#1604;&#1588;&#1610;&#1582; &#1586;&#1575;&#1610;&#1583; &#1576;&#1606; &#1587;&#1604;&#1591;&#1575;&#1606;</a></li>
			 <li><a href="<?php echo $site_path; ?>they-said-about-zayed">&#1602;&#1575;&#1604;&#1608;&#1575; &#1601;&#1610; &#1575;&#1604;&#1588;&#1610;&#1582; &#1586;&#1575;&#1610;&#1583; &#1576;&#1606; &#1587;&#1604;&#1591;&#1575;&#1606;</a></li>
			 <li ><a href="<?php echo $site_path; ?>sheikh-zayed-grand-mosque">&#1575;&#1604;&#1588;&#1610;&#1582; &#1586;&#1575;&#1610;&#1583; &#1608;&#1575;&#1604;&#1580;&#1575;&#1605;&#1593; &#1575;&#1604;&#1603;&#1576;&#1610;&#1585;</a></li>
			
			</ul>
                   	  </div>
                        <h3><a href="<?php echo $site_path; ?>grand-mosque-message">&#1593;&#1606; &#1580;&#1575;&#1605;&#1593; &#1575;&#1604;&#1588;&#1610;&#1582; &#1586;&#1575;&#1610;&#1583;</a></h3>
                      <div class="saidby">
                	 <ul>
                     <li><a href="<?php echo $site_path; ?>vision-dream-sheikh-zayed-mosque"> الرؤية والحلم </a></li>
			 
             <li ><a href="<?php echo $site_path; ?>achievement">&#1575;&#1604;&#1573;&#1606;&#1580;&#1575;&#1586;</a></li>
             <li ><a href="<?php echo $site_path; ?>grand-mosque-message">&#1585;&#1587;&#1575;&#1604;&#1577; &#1575;&#1604;&#1580;&#1575;&#1605;&#1593;</a></li>
			 
           
			</ul>
                    
                    </div>
                    <h3><a href="<?php echo $site_path; ?>architecture">&#1575;&#1604;&#1580;&#1605;&#1575;&#1604;&#1610;&#1575;&#1578; &#1608;&#1575;&#1604;&#1593;&#1605;&#1575;&#1585;&#1577;</a></h3>
                    <div class="saidby">
                	 <ul>
			 <li ><a href="<?php echo $site_path; ?>architecture">&#1606;&#1592;&#1585;&#1577; &#1593;&#1575;&#1605;&#1577;</a></li>
             <li><a href="<?php echo $site_path; ?>domes">القباب</a></li>
			 <li><a href="<?php echo $site_path; ?>marble">&#1575;&#1604;&#1585;&#1582;&#1575;&#1605;</a></li>
			 <li><a href="<?php echo $site_path; ?>lunar-illumination">&#1575;&#1604;&#1573;&#1590;&#1575;&#1569;&#1577; &#1575;&#1604;&#1602;&#1605;&#1585;&#1610;&#1577;</a></li>
			 <li><a href="<?php echo $site_path; ?>carpet">&#1575;&#1604;&#1587;&#1580;&#1575;&#1583;&#1577;</a></li>
			 <li><a href="<?php echo $site_path; ?>chandeliers">&#1575;&#1604;&#1579;&#1585;&#1610;&#1617;&#1575;&#1578;</a></li>
			 <li ><a href="<?php echo $site_path; ?>pulpit">&#1575;&#1604;&#1605;&#1606;&#1576;&#1585;</a></li>
			</ul>
                    
                    </div>
                    
                    <h3><a href="https://webmail.szgmc.ae/" target="_blank" onclick="redirect_link('https://webmail.szgmc.ae/')">&#1604;&#1604;&#1605;&#1608;&#1592;&#1601;&#1610;&#1606; &#1601;&#1602;&#1591;</a></h3>
                       <div class="saidby" style="margin:0; padding:0;"> </div>
                    
                    <?php 
		$menus_list = $conf->getMenuList(0); // 0 for main defaul menu
		
		/*echo '<pre>';
		print_r($menus_list);
		echo '</pre>';*/
		
		
		foreach($menus_list as $my_menu) {
		?>
		
		<?php
			if(empty($my_menu['sub_menus'])) {
		?>
        	<h3><a href="<?php echo $site_path; ?><?php echo $my_menu['url']; ?>" onclick="redirect_link('<?php echo $site_path; ?><?php echo $my_menu['url']; ?>')"><?php echo $my_menu['name']; ?></a></h3>
			<div class="saidby" style="margin:0; padding:0;"> </div>
        <?php
			} else {
			?>
            <h3><a href="<?php echo $site_path; ?><?php echo $my_menu['url']; ?>" onclick="return false;"><?php echo $my_menu['name']; ?></a></h3>
			    <div class="saidby">
                <ul >
                <?php foreach($my_menu['sub_menus'] as $sub_menu) { ?>
                    <li><a href="<?php echo $site_path; ?><?php echo $sub_menu['url']; ?>"><?php echo $sub_menu['name']; ?></a> </li>
                <?php } ?>
                </ul>
				</div>	
			
        <?php
			}
		}
		?>
                	
                    
                    </div>
                    
                    <div class="clear bottom_line"> &nbsp; </div>
     </div>
			</div>
 <br class="clear" />      
        </div>
    
    <div class="clear"></div></div>

<div class="content_bottom">&nbsp;</div>
<?php require 'includes/footer.php'; ?>
</body>
</html>
