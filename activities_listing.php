<?php
include 'includes/database.php';
include 'includes/dal/activities.php';
include 'includes/functions.php';
include 'includes/config.php';

$slug = explode('/',$_SERVER['REQUEST_URI']);
$slug = end($slug);
$conf = new Configuration();
$db = new MyDatabase();

$site_path = $conf->site_url;

$dal_news = new ManageNews();
$total_news = $dal_news->countNews();
$current_page = isset($_GET['current_page'])?mysql_real_escape_string($_GET['current_page']):1;

$news_per_page = 4;

$starts_with = ($current_page-1) * $news_per_page;

$slug = 'center-activities';
function getMonth($month) {
		$list = array(
				'Jan' => 'يناير',
				'Feb' => 'فبراير',
				'Mar' => 'مارس',
				'Apr' => 'ابريل',
				'May' => 'مايو',
				'Jun' => 'يونيو',
				'Jul' => 'يوليو',
				'Aug' => 'أغسطس',
				'Sep' => 'سبتمبر',
				'Oct' => 'أكتوبر',
				'Nov' => 'نوفمبر',
				'Dec' => 'ديسمبر'
					);
		
		if(in_array($month, array_keys($list))) {
			//die($list[$slug]);
			return $list[$month];
		}
		else
			return '';
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>فعاليات المركز</title>
<?php include 'includes/common_header.php'; ?>
<link href="<?php echo $site_path; ?>css/news.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript">
    $(document).ready(function() {
    });
    </script>
</head>

<body>
<?php include 'includes/menus/banner_header3.php'; ?>		
			<!-- Banner Start -->
<div class="banner">
	<img src="<?php echo $site_path;?>images/visiting_the_mosque_banner.jpg">     
</div>
<!-- Banner Close -->	
<!-- Content Start -->
	<div class="main_box_content visiting_page_height">
	 <div id="dialognew" style="display:none;">
        	<p style="text-align:right;">لقد تم تقديم طلبكم بنجاح ،</p>
            <p><span dir="ltr">Your request has been submitted successfully</span></p>
            <p style=""><input type="button" value="OK" style="float:right;width:40px" id="confirmnew"></p>
        </div>
		 <?php include 'includes/menus/left_menu.php'; ?>
        <div class="clear"></div>
        <div class="content">
        	<div class="brad_cram">
            	<ul>
            	   <li><a href="<?php echo $site_path; ?>">الصفحة الرئيسية</a></li>
                    <li><a href="#" class="active">الأنشطة والفعاليات</a></li>
                </ul>
            </div>
      
      		<div class="content-left">
			 <?php //include 'includes/menus/left_menu1.php'; 
				?>
				
                <br class="clear"/>
                <?php 
				include 'includes/menus/ministry_logos.php'; 
				
				?>
                <?php include 'includes/menus/rightsidebanner.php';?>
			</div>
	  		<div class="content-right" style="margin-right:30px">
				<div class="single_middle" >
		<?php include 'includes/menus/marquee.php';
							 ?>
                <br class="clear" />
                <h2 class="clear"> الأنشطة والفعاليات  </h2>
<br class="clear" />
                <?php
					$first = false;
					$bk_class = '';
					$news_list = $dal_news->getNews('', $news_per_page, $starts_with);
					//print_r($news_list);
					foreach($news_list as $news) {
					$news_short=strip_tags($news->news_text);
					$news_short=substr( $news_short,0,500);
					$news_short = strip_tags($news_short);
					$news_url= $site_path."activities-detail/".string_to_filename($news->news_title).'-'.$news->news_id;
					$bk_class = $first ? 'first' : 'second';
					$first = !$first;
				  ?>
                <div class="news_story <?php echo $bk_class; ?>">
                	<a href="<?php echo $news_url; ?>"><img src="<?php echo $site_path; ?>news_images/<?php echo $news->image; ?>" height="111" width="111" />
                    </a>
                    <?php
					$date = date('M, d, Y', strtotime($news->news_date));
						$date = explode(",", $date);
						  $month = $date[0];
						$month= getMonth($month);
						$news_date=$date[1]."&nbsp;".$month."&nbsp;".$date[2];
					?>
                    <div class="date"><?php echo $news_date; ?></div>
                    <h3><a href="<?php echo $news_url; ?>"><?php echo $news->news_title; ?></a></h3>
                	<p><?php echo $news_short; ?></p>
                    <div class="read_more"><a href="<?php echo $news_url; ?>">اقرأ المزيد </a>&gt;&gt;</div>
                </div>
                <br class="clear" />
                <?php } ?>          
                <div id="news_controller">
                	<span class="<?php echo ($current_page>1)?'previous_active':'previous_inactive'; ?>">
					<?php if($current_page>1) { ?>
                    	<a href="<?php echo $site_path; ?>center-activities/<?php echo $current_page-1; ?>" >السابق</a>
                    <?php } else { echo '&#1575;&#1604;&#1587;&#1575;&#1576;&#1602;'; } ?>
                    </span>
                    
					
					<?php 
					for($i=1;$i<=($total_news/$news_per_page)+1;$i++) {
					?>
                	<span <?php echo $i == $current_page ? 'class="current"' : ''; ?>><a href="<?php echo $site_path; ?>center-activities/<?php echo $i; ?>" ><?php echo $i; ?></a></span>
                    <?php } ?>
                    <span class="<?php echo ($current_page<($total_news/$news_per_page)+1)?'next_active':'next_inactive'; ?>">
					<?php if($current_page<($total_news/$news_per_page)) { ?>
                    	<a href="<?php echo $site_path; ?>center-activities/<?php echo $current_page+1; ?>" >التالي</a>
                    <?php } else { echo 'التالي'; } ?>
                    </span>                </div>
     </div>
			</div>
 <br class="clear" />      
        </div>
    
    </div>



<div class="content_bottom">&nbsp;</div>
<?php require 'includes/footer.php'; ?>
</body>
</html>
