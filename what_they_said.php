<?php
include 'includes/database.php';
include 'includes/functions.php';
include 'includes/config.php';

$slug = explode('/',$_SERVER['REQUEST_URI']);
$slug = end($slug);
$conf = new Configuration();
$db = new MyDatabase();

$site_path = $conf->site_url;

$main_menu = $conf->getCurrentMainPage($slug);

$conf->site_description = 'What others like King Fahad, Queen Elizbeth, Jimmy Carter and other prominent leaders say about Sheikh Zayed. Find all about the Sheikh Zayed Grand Mosque in Abu Dhabi including, visiting timings, how to get to the mosque, dress code, tours, history, architecture and more.';

$conf->site_keywords = 'grand mosque, sheikh zyed grand mosque, mosque in adu dhabi, what to wear in mosque, how to get to mosque, history of grand mosque, architecture of grand mosque, grand mosque photos';

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
 <title>What others said about Sheikh Zayed</title>
<?php include 'includes/common_header.php'; ?>
<link href="<?php echo $site_path; ?>css/jquery-ui-1.8.21.custom.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<?php echo $site_path; ?>js/jquery-ui-1.8.21.custom.min.js"></script>
    <script type="text/javascript">
		$(document).ready(function() {
			//$("#accordionGiftLelo").msAccordion({vertical:true});
			$( "#accordionGiftLelo" ).accordion({ autoHeight: false });
		});
	</script>
</head>

<body>
<?php include 'includes/menus/banner_header3.php'; ?>		
			<!-- Banner Start -->
<div class="banner">
	<img src="<?php echo $site_path;?>images/visiting_the_mosque_banner.jpg">     
</div>
<!-- Banner Close -->	
<!-- Content Start -->
	<div class="main_box_content visiting_page_height">
	 	 <?php include 'includes/menus/left_menu.php'; ?>
        <div class="clear"></div>
        <div class="content">
        	<div class="brad_cram">
            	<ul>
            	   <li><a href="<?php echo $site_path; ?>">الصفحة الرئيسية</a></li>
                    <li><a href="#" class="active">قالوا في الشيخ زايد بن سلطان</a></li>
                </ul>
            </div>
      
      		<div class="content-left">
			 <?php //include 'includes/menus/left_menu1.php'; 
				?>
				
                <br class="clear"/>
                <?php 
				include 'includes/menus/ministry_logos.php'; 
				?>
                <?php include 'includes/menus/rightsidebanner.php';?>
			</div>
	  		<div class="content-right" style="margin-right:30px">
			<div class="single_middle">
                    <h2 style="color:#BC8545"><strong>قالوا في الشيخ زايد بن سلطان</strong></h2>
                    <br class="clear" />
                    
                	<div id="accordionGiftLelo">
                      <h3><a href="#"><strong>خادم الحرمين الشريفين الملك فهد بن عبدالعزيز</strong></a></h3>
                      
                      <div class="saidby">
                      <p id="theysaid_tab1"><img src="<?php echo $site_path;?>images/what_they_said/1.jpg" align="right" /></p>
                      <p> &quot;نحن نشعر بأهمية الدور الذي يؤديه صاحب السمو الشيخ زايد بن سلطان   لخدمة الأمة العربية، وإبراز دور الإمارات في المجال العالمي.. حيث أصبحت   لها الكلمة المسموعة لمساندة ودعم الأشقاء&quot;.</p>
                      </div>
                      <h3><a href="#"> <strong>الرئيس الأمريكي جيمي كارتر</strong></a></h3>
                      <div class="saidby">
                	<p id="theysaid_tab3"><img src="<?php echo $site_path;?>images/what_they_said/3.jpg" align="right" /> &quot;لا يملك المرء إلا أن يبدي إعجابه بزعامة صاحب السمو الشيخ زايد وحنكته السياسية، وهذه المنجزات العمرانية والحضارية الهائلة التي تحققت بدولة الإمارات بفضل قيادة سموه في زمن قياسي&quot;. </p>
                    
                    </div>
                       <h3><a href="#"> <strong>جلالة الملكة إليزابيث الثانية</strong></a></h3>
                      <div class="saidby">
                		<p id="theysaid_tab2"><img src="<?php echo $site_path;?>images/what_they_said/2.jpg" align="right" /> &quot;إننا لمعجبون بالقيادة الحكيمة والخبيرة التي تمارسونها كرئيس، والنجاح الاقتصادي الذي حققته بلادكم ماثل للعيان.. ماثل في المدن الجميلة والمنظمة، وماثل في شبكة الطرق الممتازة، وفي المطارات والموانىء، وفي الاتصالات الحديثة التي تربط أبناء بلدكم بالعالم الخارجي، وفي البيئة الخضراء التي يتمتع بها مواطنوكم في الأماكن التي لم يكن فيها يوماً إلا صحراء، ولم توجهوا ثروة أو تفكير وطاقة بلادكم للأشياء المادية فقط، ولكنكم وضعتم الأساس لما هو اليوم نظام تعليمي حديث وشامل استعداداً لمتطلبات القرن الحادي والعشرين&quot;. </p>
                   	  </div>
                      
                    <h3><a href="#"> <strong>الرئيس الفرنسي جاك شيراك</strong></a></h3>
                    <div class="saidby">
                	<p id="theysaid_tab4"><img src="<?php echo $site_path;?>images/what_they_said/4.jpg" align="right" /> &quot;إن سموكم تتمتعون بصفات القائد، وهي الحكمة والنفوذ والشجاعة والعدل والكرم&quot;. </p>
                    
                    </div>
                    
                    <h3><a href="#"> <strong>الرئيس النمساوي كورت فالدهايم</strong></a></h3>
                     <div class="saidby">
                	<p id="theysaid_tab7"><img src="<?php echo $site_path;?>images/what_they_said/5.jpg" align="right" /> &quot;إن عمر الأمم لا يقاس بعدد السنين والحقب، بقدر ما يقاس بالأعمال والإنجازات، وإذا أخذنا بهذا المقياس لاستطعنا القول إن دولة الإمارات العربية المتحدة خلال تاريخها القصير استطاعت بعد الاستقلال أن تحتل مكاناً لائقاً بها في الأسرة العربية، وهي لم تحتل هذا المركز بين يوم وليلة، لكن ذلك المركز المرموق استطاعت دولة الإمارات أن تحتله بعد مواقف عديدة ماضية حتى قبل إعلان الاستقلال، وكان يقود هذه المواقف لرؤيته البعيدة صاحب السمو الشيخ زايد بن سلطان آل نهيان&quot;. </p>
                    
                    </div> 
                    
                    <h3><a href="#"> <strong>جلالة الملكة مارغريت الثانية ملكة الدنمارك</strong></a></h3>
                    <div class="saidby">
                	<p id="theysaid_tab8"><img src="<?php echo $site_path;?>images/what_they_said/11.jpg" align="right" /> &quot;تحياتي وتقديري لصاحب السمو الشيخ زايد بن سلطان آل نهيان الذي وصل بدولة الإمارات إلى هذه المكانة الدولية الرفيعة التي تتمتع بها، إنني أنظر بالإعجاب والتقدير لنهج السياسة الخارجية لدولة الإمارات التي قرأت عنها الكثير ولاسيما عن التقدم السريع والشامل في كافة المجالات، وأتمنى أن تتاح لي الفرصة لزيارة هذه الدولة الفتية كي اطلع بنفسي على معالمها الحضارية&quot;. </p>
                    
                    </div>
                                       
                	<h3><a href="#"> <strong>الرئيس الفرنسي فرانسوا ميتران</strong></a></h3>
                    <div class="saidby">
                	<p id="theysaid_tab5"><img src="<?php echo $site_path;?>images/what_they_said/6.jpg" align="right" /> &quot;إننا نعرف يا صاحب السمو أنكم تريدون أن يسود الحق فوق القوة، وأن تعلو الشرعية على الأمر الواقع&quot;. </p>
                    
                    </div>
                    
                    
                    <h3><a href="#"> <strong>الرئيس المكسيكي جوزي لوبيز بورتيلو</strong></a></h3>
                    <div class="saidby">
                	<p id="theysaid_tab9"><img src="<?php echo $site_path;?>images/what_they_said/7.jpg" align="right" /> &quot;نحيي الجهود التي بذلها صاحب السمو الشيخ زايد بن سلطان آل نهيان تجاه شعب الإمارات التي حققت بقيادته إنجازات عظيمة في فترة قصيرة من الزمن&quot;. </p>
                    
                    </div>
                    
                    
                    
                    
                    
                	<h3><a href="#"> <strong>الرئيس السنغالي عبده ضيوف</strong></a></h3>
                    <div class="saidby">
                	<p id="theysaid_tab10"><img src="<?php echo $site_path;?>images/what_they_said/8.jpg" align="right" /> &quot;أعتبر صاحب السمو الشيخ زايد رجل دولة من الطراز الأول.. فهو يتميز بصفة خاصة بالحكمة والإلهام في السياسة التي ينتهجها، وأنا أشعر بصفة خاصة بالسعادة حين تسنح لي الفرصة بالاجتماع مع سموه وأستفيد كثيراً من تجارب سموه السياسية الواسعة&quot;. </p>
                    
                    </div>
                    <h3><a href="#"><strong>الرئيس المصري محمد أنور السادات</strong></a></h3>
                    <div class="saidby">
                	<p id="theysaid_tab11"><img src="<?php echo $site_path;?>images/what_they_said/10.jpg" align="right" /> إن والدكم يعمل في صمت، ويشارك في البناء العربي بصمت وهو يعمل معنا، مع أشقائه العرب في كل مكان، يده بيدنا وقلبه معنا، ضد ما يهدد أمتنا. </p>
                    
                    </div> 
                    
                    <h3><a href="#"><strong>الأمين العم لجامعة الدول العربية  د. عصمت عبدالمجيد</strong></a></h3>
                    <div class="saidby">
                	<p id="theysaid_tab11"><img src="<?php echo $site_path;?>images/what_they_said/9.jpg" align="right" /> &quot;زايد قائد عربي أصيل عمل دائماً من أجل التضامن العربي ورأب الصدع وتحقيق الوفاء العربي&quot;. </p>
                    
                    </div>               	
                    
          </div>
                    
                    <div class="clear bottom_line"> &nbsp; </div>
     </div>
			</div>
 <br class="clear" />      
        </div>
    
    </div>

<div class="content_bottom">&nbsp;</div>
<?php require 'includes/footer.php'; ?>
</body>
</html>
