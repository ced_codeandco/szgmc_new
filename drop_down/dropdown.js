var width_screen = $( window ).width();
		if(width_screen>=900){
			ddsmoothmenu.init({
  mainmenuid: "smoothmenu1", //menu DIV id
  orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
  classname: 'ddsmoothmenu', //class added to menu's outer DIV
  //customtheme: ["#1c5a80", "#18374a"],
  contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
})
		}
		
$(document).ready(function() {	
if(width_screen<=900){	
if ( $( ".ddsmoothmenu > ul > li > ul" ).length ) {
 
		$( ".ddsmoothmenu > ul > li > ul" ).parent().addClass('submenu');
		$( ".ddsmoothmenu > ul > li > ul" ).parent().append('<span class="drop_ul">+</span>')
	 
}
$( ".ddsmoothmenu > ul > li" ).each(function( index ) {	
	var hrefval = $(this).find('a').first().attr('href');	
	if ($(this).find('ul').length != 0){
		var hreftext = $(this).find('a').first().text();	
		//console.log(hreftext);
		$(this).find('ul').prepend( "<li><a href='"+hrefval+"'>"+hreftext+"</a></li>" );
		$(this).find('a').first().attr('href','javascript:void(0)');
	}
});
$('.drop_ul').click(function(){
		if($(this).parent().find('span.drop_ul').hasClass('activeli')){
			$('.drop_ul').removeClass('activeli');
			$(this).text('+');
			$(this).parent().find('ul').slideUp(50);
		}else{
			$('.drop_ul').removeClass('activeli');
			$(this).addClass('activeli');
			$('.drop_ul').text('+');
			$(this).text('-');
			$('.ddsmoothmenu ul li ul').hide(0);
			$(this).parent().find('ul').slideDown(50);	
		}
		$('#mainNav').slideToggle(500);
	});
$('.ddsmoothmenu > ul > li > a').click(function(){
		if($(this).parent().find('span.drop_ul').hasClass('activeli')){
			$('.drop_ul').removeClass('activeli');
			$(this).parent().find('span.drop_ul').text('+');
			$(this).parent().find('ul').slideUp(50);
		}else{
			$('.drop_ul').removeClass('activeli');
			$(this).parent().find('span.drop_ul').addClass('activeli');
			$('.drop_ul').text('+');
			$(this).parent().find('span.drop_ul').text('-');
			$('.ddsmoothmenu ul li ul').hide(0);
			$(this).parent().find('ul').slideDown(50);	
		}
		$('#mainNav').slideToggle(500);
	});	
	
$('#dataTable1').attr('style','');
$('.add_more_arb').attr('style','');
$('.add_more').attr('style','');	
}

if(width_screen<=600){
	$('.input_label_div_arb').each(function(){
		var content = $(this).html();
		content = content.replace('<br/>', ' ');
		content = content.replace('<br />', ' ');
		content = content.replace('<br>', ' ');
		$(this).html(content);
	})
	
	$('.input_row, .input_row_dob').each(function(){
		var arabic_row = $(this).find('.input_label_div_arb');
		
		if($(this).find('.input_label_div_term').length==0){ $(this).find('.input_label_div_arb').remove(); }
		if($(this).find('.input_div').length>0){ arabic_row.insertBefore($(this).find('.input_div')); }
		if($(this).find('.captcha').length>0){ arabic_row.insertBefore($(this).find('.captcha')); }
		if($(this).find('.iwidth').length>0){ arabic_row.insertBefore($(this).find('.iwidth')); }
		if($(this).find('.input_div1').length>0){ arabic_row.insertBefore($(this).find('.input_div1')); }
		if($(this).hasClass('checkbox_div')){ arabic_row.insertBefore($(this).find('.input_label_div')); }
		//if($(this).find('.input_label_div_term').length>0){ arabic_row.insertAfter($(this).find('.input_label_div_term')); }
		//console.log($(this).find('#agree').length);
	})
}


$(window).load(function() {
		$('.flexslider').flexslider();
		$('#menuclick #nav-icon3').click(function(){
		if($('div.nav').hasClass('menushow')){
			$('div.nav').removeClass('menushow');
			$('header').removeClass('containershow');
			$('.social_box').removeClass('containershow');				
			$('.banner').removeClass('containershow');	
			$('.main_box_content').removeClass('containershow');	
			$('.partner_box').removeClass('containershow');	
			$('.footer').removeClass('containershow');	
			//$('.partner_box').removeClass('containershow');			
			$('#menuclick #nav-icon3').removeClass('open');
		}else{
			$('div.nav').addClass('menushow');
			$('header').addClass('containershow');
			$('.social_box').addClass('containershow');				
			$('.banner').addClass('containershow');	
			$('.main_box_content').addClass('containershow');	
			$('.partner_box').addClass('containershow');	
			$('.footer').addClass('containershow');	
			//$('.partner_box').addClass('containershow');					
			$('#menuclick #nav-icon3').addClass('open');		
		}
		//$('#mainNav').slideToggle(500);
		
	});
	$('#menuclickfooter #nav-icon3').click(function(){
			$('.footer-menu').slideToggle(200);
		});	
		
	});
				
});