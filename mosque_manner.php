<?php
include 'includes/database.php';
include 'includes/functions.php';
include 'includes/config.php';

$slug = explode('/',$_SERVER['REQUEST_URI']);
$slug = @mysql_real_escape_string(end($slug));
$conf = new Configuration();
$db = new MyDatabase();

$site_path = $conf->site_url;
$slug = explode('/',$_SERVER['REQUEST_URI']);
$slug = end($slug);
$main_menu = $conf->getCurrentMainPage($slug);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sheikh Zayed Grand Mosque in Abu Dhabi</title>
<?php include 'includes/common_header.php'; ?>
</head>

<body>
<?php include 'includes/menus/banner_header3.php'; ?>		
			<!-- Banner Start -->
<div class="banner">
	<img src="<?php echo $site_path;?>images/visiting_the_mosque_banner.jpg">     
</div>
<!-- Banner Close -->	
<!-- Content Start -->
	<div class="main_box_content">
	 	 <?php include 'includes/menus/left_menu.php'; ?>
        <div class="clear"></div>
        <div class="content">
        	<div class="brad_cram">
            	<ul>
            	   <li><a href="<?php echo $site_path; ?>">الصفحة الرئيسية</a></li>
                   <li><a href="<?php echo $site_path; ?>visiting-the-mosque">زيارة الجامع</a></li>
                    <li><a href="#" class="active"><!--الطريقة مسجد--><!--آداب المساجد-->آداب دخول الجامع</a></li>
                </ul>
            </div>
      
      		<div class="content-left">

                <?php 
				include 'includes/menus/ministry_logos.php'; 
				?>
                 <?php include 'includes/menus/rightsidebanner.php';?>
			</div>
	  		<div class="content-right" style="margin-right:30px">
			<div class="single_middle">
		        <h2><!--الهيكل التنظيمي--><!--آداب المساجد-->آداب دخول الجامع</h2>
                                <p>نرحب بكم في جامع الشيخ زايد الكبير متمنين لكم قضاء أفضل الأوقات في رحاب الصرح الكبير ، يرجى التكرم بالتحلي بآداب زيارة الجامع مع الوضع في الاعتبار قدسية المكان بوصفه مكاناً مخصصاً للصلاة مع التنويه بضرورة الالتزام بقواعد وآداب التصوير التالية:</p>
				<ul style="color: #464646; padding-right: 15px;">
					<li>عدم إظهار التصرفات الحميمية خلال التصوير.</li>
					<li>مراعاة وضع غطاء الرأس بالنسبة للنساء في جميع الأوقات.</li>
					<li>مراعاة الاحتفاظ بضوابط اللباس المحتشم في جميع الأوقات.</li>
					<li>عدم التصوير في أوضاع من شأنها أن تستفز الآخرين.</li>
					<li>الابتعاد عن إبراز أي إيحاءات تعبر عن معنى معين.</li>
					<li>تجنب تصوير الزوار والاكتفاء بتصوير الجماليات المعمارية واللقطات الشخصية.</li>
				</ul>
<!--p>تحظى المساجد بمكانتها الدينية وقدسيتها لدى جميع المسلمين، ومن هذا المنطلق فإن دخول هذه المساجد يتطلب من الجميع الالتزام بآداب وقواعد محددة، بالإضافة إلى إظهار القدر الكبير من الاحترام من خلال الالتزام بالتعليمات والإرشادات الموضحة أدناه: </p>
<ul style="color: #464646; padding-right: 15px;">
<li>يرجى عدم ادخال الأطعمة.</li>
<li>يرجى عدم التدخين.</li>
<li>يرجى المحافظة على الهدوء.</li>
<li>يرجى عدم ارتداء الملابس الشفافة.</li>
<li>يرجى عدم ارتداء الملابس والتنانير القصيرة.</li>
<li>يرجى عدم ارتداء قميص بدون اكمام.</li>
<li>يرجى عدم ارتداء ملابس برسومات غير لائقة.</li>
<li>يرجى عدم ارتداء الملابس الضيقة او ملابس السباحة.</li>
</ul-->
				<div class="orgnization">
                
                <img class="desktop_view" src="<?php echo $site_path;?>img/Mosque-Manners.jpg" style="width: 690px;"/>
                
                <div class="mobile_view manners_div"> 
                <span>Allowed<label>مسموح</label></span>
                <img src="<?php echo $site_path; ?>img/mobile/Mosque-Manners_moba.png" style="width: 690px;" />
                <span>Not Allowed<label>غير مسموح</label></span>
                <img src="<?php echo $site_path; ?>img/mobile/Mosque-Manners_mobna.png" style="width: 690px;" />
                </div>
                
                 </div>
     			</div>			
			</div>
 <br class="clear" />      
        </div>
    
    </div>

<div class="content_bottom">&nbsp;</div>
<?php require 'includes/footer.php'; ?>
</body>
</html>
