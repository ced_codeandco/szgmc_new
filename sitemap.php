<?php
include 'includes/database.php';
include 'includes/functions.php';
include 'includes/config.php';

$slug = explode('/',$_SERVER['REQUEST_URI']);
$slug = end($slug);
$conf = new Configuration();
$db = new MyDatabase();

$site_path = $conf->site_url;

$main_menu = $conf->getCurrentMainPage($slug);

$conf->site_description = 'What others like King Fahad, Queen Elizbeth, Jimmy Carter and other prominent leaders say about Sheikh Zayed. Find all about the Sheikh Zayed Grand Mosque in Abu Dhabi including, visiting timings, how to get to the mosque, dress code, tours, history, architecture and more.';

$conf->site_keywords = 'grand mosque, sheikh zyed grand mosque, mosque in adu dhabi, what to wear in mosque, how to get to mosque, history of grand mosque, architecture of grand mosque, grand mosque photos';

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>خريطة الموقع</title>
    <?php include 'includes/common_header.php'; ?>
    <link href="<?php echo $site_path; ?>css/sitemap.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<?php echo $site_path; ?>js/jquery-ui-1.8.21.custom.min.js"></script>
    <script type="text/javascript">
		$(document).ready(function() {
			//$("#accordionGiftLelo").msAccordion({vertical:true});
			$( "#accordionGiftLelo" ).accordion({ autoHeight: false });
		});
	</script>
<style>
div.single_middle div.saidby {
border-right: #ccc solid 1px;
border-left: #ccc solid 1px;
border-bottom: #ccc solid 1px;

background: #fff; 

}
div.single_middle div.saidby ul li{color:#000; padding:8px; list-style:inside; direction:rtl;}
div.single_middle div.saidby ul li a{color:#000;  text-decoration:none; direction:rtl; }
</style>	
<script type="text/javascript">
function redirect_link(a)
{
    window.location = a;
}
//-->
</script>
</head>

<body>
<?php include 'includes/menus/banner_header3.php'; ?>		
			<!-- Banner Start -->
<div class="banner">
	<img src="<?php echo $site_path;?>images/visiting_the_mosque_banner.jpg">     
</div>
<!-- Banner Close -->	
<!-- Content Start -->
	<div class="main_box_content">
	 	 <?php include 'includes/menus/left_menu.php'; ?>
        <div class="clear"></div>
        <div class="content">
        	<div class="brad_cram">
            	<ul>
            	   <li><a href="<?php echo $site_path; ?>">الصفحة الرئيسية</a></li>
                    <li><a href="#" class="active">خريطة الموقع</a></li>
                </ul>
            </div>
      
      		<div class="content-left">
			 <?php //include 'includes/menus/left_menu1.php'; 
				?>
				
                <br class="clear"/>
                <?php 
				include 'includes/menus/ministry_logos.php'; 
				?>
                <?php include 'includes/menus/rightsidebanner.php';?>
			</div>
	  		<div class="content-right" style="margin-right:30px">
			<div class="single_middle">
		<?php 
        include 'includes/menus/marquee.php'; 
        ?>
        <h2 style="color:#BC8545">خريطة الموقع</h2>
        <br class="clear" />

        <div id="accordionGiftLelo">

            <h3><a href="<?php echo $site_path; ?>about-the-center">&#1593;&#1606; &#1575;&#1604;&#1605;&#1585;&#1603;&#1586;</a></h3>
            <div class="saidby">
                <ul>
				<li><a href="<?php echo $site_path; ?>minister-hh">كلمة سمو الشيخ منصور بن زايد</a></li>
				<li><a href="<?php echo $site_path; ?>chairmans-message">كلمة رئيس مجلس الأمناء</a></li>
				<li><a href="<?php echo $site_path; ?>DG-message">كلمة المدير العام</a></li>
				<!--<li><a href="<?php echo $site_path; ?>board-members">أعضاء مجلس الأمناء</a></li>-->
				<li><a href="<?php echo $site_path; ?>organizational-structure">الهيكل التنظيمي</a></li>
				<li><a href="<?php echo $site_path; ?>vision-mission-values">الرؤية والرسالة والقيم</a></li>
				<li><a href="<?php echo $site_path; ?>sheikh-zayed-and-grand-mosque">الشيخ زايد والجامع الكبير</a></li>
		        <li><a href="<?php echo $site_path; ?>achievement">مراحل البناء</a></li>
		      </ul>
            </div>

                            <h3><a href="<?php echo $site_path; ?>visiting-the-mosque">زيارة الجامع – المصلون</a></h3>
                            <div class="saidby">
                            <ul>
                                <li><a href="<?php echo $site_path; ?>visitor-services">البرامج الدينية</a></li>
                                 <li><a href="<?php echo $site_path; ?>prayer-timings">آداب دخول الجامع</a></li>
                                <li><a href="<?php echo $site_path; ?>friday-sermon">مواقيت الصلاة</a></li>
                                <li><a href="<?php echo $site_path; ?>getting-to-the-mosque">الخدمات</a></li>
                                <li><a href="<?php echo $site_path; ?>religious-programs">كيفية الوصول</a></li>
                            </ul>
                            </div>

                            <h3><a href="<?php echo $site_path; ?>visiting-the-mosque">زيارة الجامع –   الزوار</a></h3>
                            <div class="saidby">
                            <ul>
                                <li><a href="<?php echo $site_path; ?>mosque-manner">آداب دخول الجامع</a></li>
                                <li><a href="<?php echo $site_path; ?>mosque-opening-hours">اوقات الزيارة</a></li>
                                <li><a href="<?php echo $site_path; ?>cultural-tours">الجولات الثقافية</a></li>
                                <li><a href="<?php echo $site_path; ?>visitor-services">المرشد الالكتروني</a></li>
                                <li><a href="<?php echo $site_path; ?>electronic-services">كيفية الوصول</a></li>
                                <li><a href="<?php echo $site_path; ?>getting-to-the-mosque">التجول الافتراضي</a></li>
                                <li><a href="<?php echo $site_path; ?>questions">أسئلة متكررة</a></li>
                                <li><a href="<?php echo $site_path; ?>services">الخدمات</a></li>
                            </ul>
                            </div>

                            <h3><a href="<?php echo $site_path; ?>visiting-the-mosque">زيارة الجامع –  الوفود والشركات السياحية</a></h3>
                            <div class="saidby">
                            <ul>
                                <li><a href="<?php echo $site_path; ?>mosque-manner">آداب دخول الجامع</a></li>
                                <li><a href="<?php echo $site_path; ?>mosque-opening-hours">اوقات الزيارة</a></li>
                                <li><a href="<?php echo $site_path; ?>getting-to-the-mosque">كيفية الوصول</a></li>
                                <li><a href="https://www.google.com/maps/views/streetview/sheikh-zayed-grand-mosque?gl=us" target="_blank">التجول الافتراضي</a></li>
                                <li><a href="<?php echo $site_path; ?>questions">أسئلة متكررة</a></li>
                                <li><a href="<?php echo $site_path; ?>services">الخدمات</a></li>
                                <li><a href="<?php echo $site_path; ?>tour-booking-form">احجز جولة ثقافية</a></li>
                                <li><a href="<?php echo $site_path; ?>register.php">تسجيل الشركات السياحية</a></li>
                            </ul>
                            </div>

            <!--h3><a href="<?php echo $site_path;?>visiting-the-mosque">زيارة الجامع</a></h3>
            <div class="saidby">
                <ul>
				<li><a href="<?php echo $site_path; ?>mosque-manner">آداب دخول الجامع</a></li>
				<li><a href="<?php echo $site_path; ?>mosque-opening-hours">أوقات الزيارة</a></li>
				<li><a href="<?php echo $site_path; ?>visitor-services">أنواع الجولات</a></li>
				
                <li><a href="<?php echo $site_path; ?>e-guide">المرشد الالكتروني</a></li>
                <li><a href="<?php echo $site_path; ?>what-is-tour"> ما هي الجولة الثقافية ؟ </a></li>
                
				<li><a href="<?php echo $site_path; ?>tour-booking-form"> احجز جولة ثقافية </a></li>
				<li><a href="<?php echo $site_path; ?>getting-to-the-mosque" >كيفية الوصول</a></li>
				<li><a href="https://www.google.ae/maps/streetview/" target="_blank">التجول الافتراضي</a></li>
				
				<li><a href="<?php echo $site_path; ?>questions">أسئلة متكررة</a></li>
                <li><a href="<?php echo $site_path; ?>important-information">معلومات تهمك</a></li>

		       </ul>
            </div-->

            <h3><a href="<?php echo $site_path; ?>e_services.php">الخدمات الإلكترونية</a></h3>
            <div class="saidby">
               <ul>
			 <li><a href="<?php echo $site_path; ?>tour-booking-form">الحجوزات السياحية</a></li>
			 <li><a href="<?php echo $site_path; ?>careers">وظائف</a></li>
			 <li><a href="<?php echo $site_path; ?>media-form">تصريح تسجيل بالفيديو</a></li>
			 <li><a href="<?php echo $site_path; ?>lost-found">المفقودات</a></li>
             <!--<li><a href="<?php /*echo $site_path; */?>log_book.php">سجل الأحوال اليومية</a></li>-->
             <li><a href="<?php echo $site_path; ?>suggestion-complaint">الشكاوي والاقتراحات</a></li>
             <!--li><a href="<?php echo $site_path; ?>juniorculturalguide.php">برنامج المرشد الثقافي الصغير</a></li-->
			</ul>
            </div>

            <h3><a href="javascript:void(0);">الركن الإعلامي</a></h3>
            <div class="saidby">
                <ul>
                <li><a href="<?php echo $site_path; ?>news-list">الأخبار</a></li>
                <li><a href="<?php echo $site_path; ?>press-kit">المواد الإعلامية</a></li>
                <li><a href="<?php echo $site_path; ?>eparticipation">المشاركة الإلكترونية</a></li>
            </ul>
            </div>

            <h3><a href="javascript:void(0);">المكتبة</a></h3>
            <div class="saidby">
                <ul>
				<li><a href="<?php echo $site_path; ?>about-the-library">عن المكتبة</a></li>
				<li><a href="<?php echo $site_path; ?>library-resources">المقتنيات</a></li>
				<li><a href="<?php echo $site_path; ?>library-services">خدمات المكتبة</a></li>
				<li><a href="<?php echo $site_path; ?>search-library">البحث</a></li>
				<li><a href="<?php echo $site_path; ?>publications">اصدارات المركز</a></li>
			</ul>
            </div>


            <h3><a href="<?php echo $site_path; ?>events-and-activities">أنشطة وفعاليات</a></h3>
            <div class="saidby">
                <ul>
                <li><a href="<?php echo $site_path; ?>exhibitions">
                        المعارض
                    </a> </li>
                <li><a href="<?php echo $site_path; ?>activities">
                        الأنشطة
                    </a> </li>
                <li><a href="<?php echo $site_path; ?>social-initiatives">
                        المبادرات المجتمعية
                    </a> </li>
                <li><a href="<?php echo $site_path; ?>spaces-of-light">
                        جائزة فضاءات من نور
                    </a> </li>
            </ul>
            </div>
            <h3><a href="<?php echo $site_path;?>religious-programs">البرامج الدينية</a></h3>
            <div class="saidby"><ul>
                <li><a href="<?php echo $site_path; ?>religious-courses">الدورات الدينية</a></li>
                <li><a href="<?php echo $site_path; ?>religious-lecture">الذكر الحكيم</a></li>
                <!--li><a href="<?php echo $site_path; ?>zikr-al-hakeem"> الذكر الحكيم</a></li-->
                <li><a href="<?php echo $site_path; ?>friday-sermon">خطب الجمعة</a></li>
                <li><a href="<?php echo $site_path; ?>ramadan-activities">فعاليات رمضان</a></li>
                <!--li><a href="<?php echo $site_path; ?>prayer-information">معلومات عن الصلوات </a></li-->
            </ul></div>
            <h3><a href="<?php echo $site_path;?>suggestion-complaint" onclick="var win = window.open('http://szgmc2016.codeandcode.co/suggestion-complaint', '_self');">اقتراحات</a></h3>
            <div class="saidby emptyChildAccordion"></div>


            <h3><a href="<?php echo $site_path;?>architecture">الجماليات والعمارة</a></h3>
            <div class="saidby">
                <ul>
			 <li><a href="<?php echo $site_path; ?>general-architecture"> نظرة عامة</a></li>
             <li><a href="<?php echo $site_path; ?>domes">&#1575;&#1604;&#1602;&#1576;&#1575;&#1576;</a></li>
			 <li><a href="<?php echo $site_path; ?>marbles">&#1575;&#1604;&#1585;&#1582;&#1575;&#1605;</a></li>
			 <li><a href="<?php echo $site_path; ?>lunar-illumination">&#1575;&#1604;&#1573;&#1590;&#1575;&#1569;&#1577; &#1575;&#1604;&#1602;&#1605;&#1585;&#1610;&#1577;</a></li>
			 <li><a href="<?php echo $site_path; ?>carpets">&#1575;&#1604;&#1587;&#1580;&#1575;&#1583;&#1577;</a></li>
			 <li><a href="<?php echo $site_path; ?>chandeliers">&#1575;&#1604;&#1579;&#1585;&#1610;&#1617;&#1575;&#1578;</a></li>
			 <li><a href="<?php echo $site_path; ?>pulpit">&#1575;&#1604;&#1605;&#1606;&#1576;&#1585;</a></li>

               <li><a href="<?php echo $site_path; ?>minaret">المنارة    </a></li>
               <li><a href="<?php echo $site_path; ?>reflective-pools"> الأحواض العاكسة</a></li>
               <li><a href="<?php echo $site_path; ?>columns"> الأعمدة   </a></li>
               <li><a href="<?php echo $site_path; ?>mihrab">المحراب   </a></li>
               <li><a href="<?php echo $site_path; ?>sahan">الصحن </a></li>

			</ul>
            </div>

            <h3><a href="javascript:void(0);">البيانات المفتوحة</a></h3>
            <div class="saidby">
                <ul>
                    <li><a href="<?php echo $site_path; ?>poll-archive" style="width:110px;">نتائج استطلاع الرأي</a></li>
                    <li><a href="<?php echo $site_path; ?>open-data" style="width:110px;">الاحصاءات</a></li>
                </ul>
            </div>

                    
            <h3><a href="https://webmail.szgmc.ae/" target="_blank" onclick="var win = window.open('https://webmail.szgmc.ae/', '_blank');win.focus();">&#1604;&#1604;&#1605;&#1608;&#1592;&#1601;&#1610;&#1606; &#1601;&#1602;&#1591;</a></h3>
            <div class="saidby emptyChildAccordion"></div>

                            <h3><a href="<?php echo $site_path; ?>contact-us" onclick="var win = window.open('http://szgmc2016.codeandcode.co/contact-us', '_self');">اتصل بنا</a></li></h3>
                            <div class="saidby"></div>

                            <h3><a href="<?php echo $site_path; ?>help" onclick="var win = window.open('http://szgmc2016.codeandcode.co/help', '_self');">للمساعدة</a></li></h3>
                            <div class="saidby"></div>

                            <h3><a href="<?php echo $site_path; ?>gallery.php" onclick="var win = window.open('http://szgmc2016.codeandcode.co/gallery.php', '_self');">البوم الصور</a></li></h3>
                            <div class="saidby"></div>

                            <h3><a href="<?php echo $site_path; ?>videos_album.php" onclick="var win = window.open('http://szgmc2016.codeandcode.co/videos_album.php', '_self');">مكتبة الفيديو</a></li></h3>
                            <div class="saidby"></div>


        </div>
                <!--<div class="clear bottom_line"> &nbsp; </div>-->


        </div>
 <br class="clear" />      
        </div>
    
    <div class="clear"></div></div>

<div class="content_bottom">&nbsp;</div>
<?php require 'includes/footer.php'; ?>
</body>
</html>
