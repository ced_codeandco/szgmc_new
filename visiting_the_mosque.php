<?php
session_start();
include './includes/database.php';
include './includes/functions.php';
include './includes/config.php';
include_once('./includes/generic_functions.php');
$conf = new Configuration();
$db = new MyDatabase();
$site_path = $conf->site_url;

$slug = explode('/',$_SERVER['REQUEST_URI']);
$slug = end($slug);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<?php include 'includes/common_header.php'; ?>
    <title>زيارة المسجد</title>
</head>
<body >

<?php include 'includes/menus/banner_header3.php'; ?>
			<!-- Banner Start -->
<div class="banner">
	<img src="<?php echo $site_path;?>images/visiting_the_mosque_banner.jpg">
</div>
<!-- Banner Close -->	
<!-- Content Start -->
	<div class="main_box_content visiting_page_height">
	 	 <?php include 'includes/menus/left_menu.php'; ?>
        <div class="clear"></div>
        <div class="content">
        	<div class="brad_cram">
            	<ul>
            	   <li><a href="<?php echo $site_path; ?>">الصفحة الرئيسية</a></li>
                    <li><a href="<?php echo $site_path; ?>" class="active">زيارة الجامع </a></li>
                </ul>
            </div>
      
      		<div class="inside_conter">
        		<div class="Visiting_box">
                	<ul>
                    	<li class="fist_tham">
                            <a href="<?php echo $site_path; ?>visit-mosque-prayers">
                                <img src="<?php echo $site_path;?>images/visiting_the_mosque/prayers.jpg">
                                <span><img src="<?php echo $site_path; ?>images/visiting_the_mosque/prayers.png">
<!--ألمصلون-->المصلون
                                </span>
                            </a>
                        </li>
                        
                         <li>
                             <a href="<?php echo $site_path; ?>visit-mosque-visitors">
                                <img src="<?php echo $site_path;?>images/visiting_the_mosque/visitors.jpg">
                                <span><img src="<?php echo $site_path; ?>images/Visiting-The -osque/tham03.png">
الزوار
                                </span>
                             </a>
                        </li>
                        
                        <li>
                            <a href="<?php echo $site_path; ?>visit-mosque-delegates">
                            <img src="<?php echo $site_path;?>images/visiting_the_mosque/delegates_and_operators.jpg">
                            <span><img src="<?php echo $site_path; ?>images/Visiting-The -osque/tham03.png">
                                الوفود والشركات السياحية
                            </span>
                            </a>
                        </li>
                        
                    </ul>
                </div>
        </div>
	  		
	  
            <br class="clear" />      
        </div>
    
    </div>
<!-- Content Close -->
	<?php include 'includes/menus/marquees_partner.php';?>
	<div class="content_bottom">&nbsp;</div>
	<?php include 'includes/footer.php'; ?> 

</body>
</html>