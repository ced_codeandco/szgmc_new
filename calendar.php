<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Language" content="EN-GB">
<style>

.hotspot { cursor:pointer; width:300px; padding:10px; text-align:left;}
#tt {position:absolute; display:block; padding:1px;}

#tttop {display:block;overflow:hidden; }
#ttbot {display:block;overflow:hidden;}
#ttcont {display:block; padding:2px 12px 3px 7px; background:#ffffff;}

#tt ul{list-style-type:none; margin:0px; padding:5px; list-style:none;}
#tt ul li{ padding-bottom:8px; }
#tt ul li span{ float:left; text-align:left;font-weight:bold; white-space:nowrap; padding-right:5px; width:80px;}
#tt ul li span.tip_header{ float:left; font-weight:bold; padding:5px 0px 15px 0px; width:100%; }

</style>

    <script src="js/ajax.js" type="text/javascript"></script>
<script>
var tooltip=function(){
	var id = 'tt';
	var top = 3;
	var left = 3;
	var maxw = 220;
	var speed = 10;
	var timer = 20;
	var endalpha = 95;
	var alpha = 0;
	var tt,t,c,b,h;
	var ie = document.all ? true : false;
	return{
		show:function(v,clr,w){
			if(tt == null){
				tt = document.createElement('div');
				tt.setAttribute('id',id);
				t = document.createElement('div');
				t.setAttribute('id',id + 'top');
				c = document.createElement('div');
				c.setAttribute('id',id + 'cont');
				b = document.createElement('div');
				b.setAttribute('id',id + 'bot');
				tt.appendChild(t);
				tt.appendChild(c);
				tt.appendChild(b);
				document.body.appendChild(tt);
				tt.style.opacity = 0;
				tt.style.filter = 'alpha(opacity=0)';
				document.onmousemove = this.pos;
			}
			tt.style.display = 'block';
			tt.style.color = clr;
			tt.style.background=clr;
			
			c.innerHTML = v;
			tt.style.width = w ? w + 'px' : 'auto';
			if(!w && ie){
				t.style.display = 'none';
				b.style.display = 'none';
				tt.style.width = tt.offsetWidth;
				t.style.display = 'block';
				b.style.display = 'block';
			}
			if(tt.offsetWidth > maxw){tt.style.width = maxw + 'px'}
			h = parseInt(tt.offsetHeight) + top;
			clearInterval(tt.timer);
			tt.timer = setInterval(function(){tooltip.fade(1)},timer);
		},
		pos:function(e){
			var u = ie ? event.clientY + document.documentElement.scrollTop : e.pageY;
			var l = ie ? event.clientX + document.documentElement.scrollLeft : e.pageX;
			tt.style.top = (u - h) + 'px';
			tt.style.left = (l + left) + 'px';
		},
		fade:function(d){
			var a = alpha;
			if((a != endalpha && d == 1) || (a != 0 && d == -1)){
				var i = speed;
				if(endalpha - a < speed && d == 1){
					i = endalpha - a;
				}else if(alpha < speed && d == -1){
					i = a;
				}
				alpha = a + (i * d);
				tt.style.opacity = alpha * .01;
				tt.style.filter = 'alpha(opacity=' + alpha + ')';
			}else{
				clearInterval(tt.timer);
				if(d == -1){tt.style.display = 'none'}
			}
		},
		hide:function(){
			clearInterval(tt.timer);
			tt.timer = setInterval(function(){tooltip.fade(-1)},timer);
		}
	};
}();

function navigate() {
var menuIndex = document.formMenu.selectMenu.selectedIndex;
location = document.formMenu.selectMenu.options[menuIndex].value;
}
</script>
<link href="calendar/calendar.css" rel="stylesheet" type="text/css" />
<?php

 if (isset($_POST["month"]))
{
$month=$_POST["month"];
}
else
{
$month=strtolower(date("M"));
}
?>
<div style="float:right; margin-left:2px; margin-bottom:10px; border:1px solid #ccc; padding:20px; direction:rtl;">
<form id="sort" name="month" method="post" action="calendar.php"> 
      
      <select name="month" onchange="refreshContent();" style=" border: 1px solid #CEBA69;direction: rtl; margin-right: 8px; float:right;">
	           <option value="jan" <? if ($month=="jan") { ?> selected="selected" <? } ?>>يناير - محرم - صفر 1432 هـ</option>
              <option value="feb" <? if ($month=="feb") { ?> selected="selected" <? } ?>> فبراير - صفر - ربيع الأول 1432 هـ</option>

              <option value="mar" <? if ($month=="mar") { ?> selected="selected" <? } ?> > مارس - ربيع الأول - ربيع الثاني 1432 هـ</option>
              <option value="apr" <? if ($month=="apr") { ?> selected="selected" <? } ?> > إبريل - ربيع الثاني - جمادى الأول 1432 هـ </option>
              <option value="may" <? if ($month=="may") { ?> selected="selected" <? } ?> > مايو - جمادى الأول - جمادى الآخر 1432 هـ </option>
              <option value="jun" <? if ($month=="jun") { ?> selected="selected" <? } ?> > حزيران - جمادى الآخر - رجب 1432 هـ </option>
              <option value="jul" <? if ($month=="jul") { ?> selected="selected" <? } ?> > يوليو - رجب - شعبان 1432 هـ</option>
              <option value="aug" <? if ($month=="aug") { ?> selected="selected" <? } ?> > أغسطس- رمضان - شوال 1432 هـ </option>
              <option value="sep" <? if ($month=="sep") { ?> selected="selected" <? } ?> > سبتمبر - شوال - ذو القعدة 1432 هـ </option>
              <option value="oct" <? if ($month=="oct") { ?> selected="selected" <? } ?> > أكتوبر - ذو القعدة - ذو الحجة 1432 هـ </option>
              <option value="nov" <? if ($month=="nov") { ?> selected="selected" <? } ?> > نوفمبر - ذو الحجة 1432 هـ - محرم 1433 هـ </option>
              <option value="dec" <? if ($month=="dec") { ?> selected="selected" <? } ?> > ديسمبر - محرم - صفر 1433 هـ </option>
        </select>
       
        </form>
        <div id="content">
              
<?

	include_once('calendar/'.$month.'.php');
?>   
</div>  </div>       
