<?php
include 'includes/database.php';
include 'includes/functions.php';
include 'includes/config.php';

$slug = explode('/',$_SERVER['REQUEST_URI']);
$slug = end($slug);
$conf = new Configuration();
$db = new MyDatabase();

$site_path = $conf->site_url;



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sheikh Zayed Grand Mosque in Abu Dhabi</title>
<?php include 'includes/common_header.php'; ?>
</head>

<body>
<?php include 'includes/menus/banner_header3.php'; ?>		
			<!-- Banner Start -->
<div class="banner">
	<img src="<?php echo $site_path;?>images/visiting_the_mosque_banner.jpg">     
</div>
<!-- Banner Close -->	
<!-- Content Start -->
	<div class="main_box_content visiting_page_height">
	 	 <?php include 'includes/menus/left_menu.php'; ?>
        <div class="clear"></div>
        <div class="content">
        	<div class="brad_cram">
            	<ul>
            	   <li><a href="<?php echo $site_path; ?>">الصفحة الرئيسية</a></li>
                    <li><a href="#" class="active">&#1571;&#1580;&#1606;&#1583;&#1577; &#1575;&#1604;&#1601;&#1593;&#1575;&#1604;&#1610;&#1575;&#1578;</a></li>
                </ul>
            </div>
      
      		<div class="content-left">
			 <?php //include 'includes/menus/left_menu1.php'; 
				?>
				
                <br class="clear"/>
                <?php 
				include 'includes/menus/ministry_logos.php'; 
				?>
                <?php include 'includes/menus/rightsidebanner.php';?>
			</div>
	  		<div class="content-right" style="margin-right:30px">
			<div class="single_middle" style="text-align:center; direction:rtl" >
                <br class="clear" />
                  <h2 style="background:url(images/event_calendarbg.png) no-repeat ; width:712px; height:36px; background-position:left; margin-bottom:0; padding-top:25px; float:right; direction:rtl; text-align:right;"><strong>&#1571;&#1580;&#1606;&#1583;&#1577; &#1575;&#1604;&#1601;&#1593;&#1575;&#1604;&#1610;&#1575;&#1578;</strong></h2>
                
                <?php include 'calendar.php'; ?>
            </div>
			</div>
 <br class="clear" />      
        </div>
    
    </div>



<div class="content_bottom">&nbsp;</div>
<?php require 'includes/footer.php'; ?>
</body>
</html>
