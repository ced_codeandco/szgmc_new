<?php
ob_start();
include 'includes/database.php';
include 'includes/dal/news.php';
include 'includes/dal/pages.php';
include 'includes/functions.php';
include 'includes/config.php';
$current_page = 'index';
$db = new MyDatabase();
$conf = new Configuration();
$site_path = $conf->site_url;
$msql = "select * from maintenance";
$mquery = mysql_query($msql);
while($mres= mysql_fetch_object($mquery))
{
	$mstatus = $mres->m_status;
}
if($mstatus==0)
{
	header("location:/en/");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>الموقع تحت الصيانة</title>
<link href="<?php echo $site_path; ?>css/style.css" rel="stylesheet" type="text/css" />
</head>

<body>
<?php include 'includes/menus/banner_header3.php'; ?>		
			<!-- Banner Start -->
<div class="banner">
	<img src="<?php echo $site_path;?>images/visiting_the_mosque_banner.jpg">     
</div>
<!-- Banner Close -->	
<!-- Content Start -->
	<div class="main_box_content visiting_page_height">
	 	 <?php include 'includes/menus/left_menu.php'; ?>
        <div class="clear"></div>
        <div class="content">
        	<div class="brad_cram">
            	<ul>
            	   <li><a href="<?php echo $site_path; ?>">الصفحة الرئيسية</a></li>
                    <li><a href="#" class="active">الموقع تحت الصيانة</a></li>
                </ul>
            </div>
	  		<div class="content-right" style="margin-right:30px">
			<div style="width:1000px; margin:0 auto;">
<img src="/images/maintenance_page.jpg" alt="Site Under Maintenance"> </div>
			</div>
 <br class="clear" />      
        </div>
    
    </div>

</body>
</html>