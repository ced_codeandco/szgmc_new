<?php
include_once('./inc/conf.php');
include_once('./inc/mysql.lib.php');
include_once('./inc/generic_functions.php');
$mydb=new connect;

$msg='';
if(isset($_POST['submitTour']) && isset($_POST['agree']) && $_POST['agree']=='1'){
    foreach($_POST as $name => $value) {
        $$name=mysql_real_escape_string($value);
    }
    saveToursData($_POST);
    $msg='before';
    $file_flag=0;
    if($msg == 'before'){
        
		/*
		$fromEmail = $_POST['email'];
        $fromName = ucfirst(strtolower($_POST['name']));
		*/
		$fromName='Tours';
        $fromEmail=ENQ_FROM;
		$toName='SZGMC';
		$toEmail=ENQ_TO;
        $subject = "SZGMC Tour Request";
		
        $content = getContentTours($_POST);		
        if(sendEmail($toName, $fromName, $toEmail, $fromEmail, $subject, $content))
            $msg='success';
        else
            $msg='failed';
    }

}
    define('TITLE',$seo_title);
    define('KEYWORD',$seo_keywords);
    define('DESC',$seo_desc);
    include_once('./templates/header.php');
    if($msg==='success'){
    echo '<div class="right_panel" style="text-align:center; padding-top:30px; color:#B9150C;font-weight:bold">';
      echo 'Thank you!<br/>
Your request has been successfully submitted.<br/>
Please expect to receive an email from us within 48 hours.
';
    echo '</div>';
    }else{

        if($msg === 'failed'){
            echo '<div style="text-align:center; padding-top:30px;">';
            echo 'Cannot process your request';
            echo '</div>';  
        }
//        include './src/booking_tours.php';
        ?>
        <script type="text/javascript">
var myDate = new Date();
start_date=(myDate.getMonth() + 1)+ "/"+myDate.getDate()+ "/" + myDate.getFullYear();
end_date=(myDate.getMonth() + 1)+ "/" + myDate.getDate() + "/" + (myDate.getFullYear()+10);

$(document).ready(function () {
        $('#date_visit').simpleDatepicker({ chosendate: start_date, startdate: start_date, enddate: end_date });
        //$('#pub_date').simpleDatepicker({ chosendate: '1/1/1980', startdate: '1/1/1980', enddate: '1/1/2005' });
        $('#date_visit').focus(function() {
        $("#date_visit").click();
        });

            $("#tourForm").validate({
			rules: {
				name: "required",
				contact_person_group:"required",
				group_category:"required",
				o_g_name:"required",
				grp_size:"required",
				org_number:"required",
				other_info:"required",
				email: {
					required: true,
					email: true
				},
				m_number: "required",
				date_visit: "required",
				prop_time: "required"
			}
		});



});
function check_additional_fileds(val)
{

$(document).ready(function () {
if(val=='Education')
$('.edu').show();
else
$('.edu').hide();
if(val=='Media')
$('.med').show();
else
$('.med').hide();
if(val=='Government')
$('.gov').show();
else
$('.gov').hide();
if(val=='Other')
$('.ot').show();
else
$('.ot').hide();
if(val=='Embassy')
$('.emb').show();
else
$('.emb').hide();
if(val=='Travel Industry')
$('.travel_industry_div').show();
else
$('.travel_industry_div').hide();


});
}
  </script>


<div class="right_panel">

<div class="reg_form">
    <div>
	       
        <p>
            Thank you for your interest in the Sheikh Zayed Grand Mosque Tour. We offer private media tours for journalists, photographers and film-makers. To serve you better we need to understand your specific requirements. Please fill-up the booking form below. We will respond to your request once we have received your email. Please note that we are unable to conduct tours without a confirmed booking request. Remember to include any special requests such as interviews, etc.
</p>  

    </div>

<form action="/en/tours-booking-form" method="post" id="tourForm" name="tourForm" enctype="multipart/form-data">

    <input type="hidden" name="agree" value="1" />
<div class="input_row even">
<div class="input_label_div"><span>Contact name (of person booking):</span><span class="red">*</span> </div>
<div class="input_div"> <input type="text" name="name" id="name" class="input_cls"/>  </div>
</div>

<div class="input_row odd">
<div class="input_label_div"><span>Email (of person booking):</span><span class="red">*</span>
</div>
<div class="input_div">
    <input dir="ltr" type="text"  name="email" id="email"  class="input_cls"/>
</div>
</div>


<div class="input_row even">
<div class="input_label_div"><span>Organization/Group Name:</span><span class="red">*</span></div>
<div class="input_div"> <input type="text" name="o_g_name" id="o_g_name" class="input_cls"/>  </div>
</div>


<div class="input_row odd">
<div class="input_label_div"><span>Organization phone number:</span><span class="red">*</span>
</div>
<div class="input_div">
    <input dir="ltr" type="text"  name="org_number" id="org_number"  class="input_cls"/>
</div>
</div>


<div class="input_row even">
<div class="input_label_div"><span>Contact name of person accompanying</span><span> the group:</span> <span class="red">*</span>
</div>
<div class="input_div">
    <input dir="ltr" type="text"  name="contact_person_group" id="contact_person_group"  class="input_cls"/>
</div>
</div>


<div class="input_row odd">
<div class="input_label_div"><span>Contact mobile number of person</span><span> accompanying the group: </span><span class="red">*</span></div>
<div class="input_div"> <input type="text" name="m_number" id="m_number" class="input_cls"/>  </div>
</div>


<div class="input_row even">
<div class="input_label_div"><span>Group booking category:</span><span class="red">*</span></div>
<div class="input_div">
<select class="input_cls" name="group_category" id="group_category"  >
<option value="Journalist � International">Journalist - International</option>
<option value="Journalist � UAE">Journalist - UAE</option>
<option value="Photographer">Photographer</option>
</select>
 </div>
</div>












<div class="input_row odd med" >
<div class="input_label_div">Publication Name</div>
<div class="input_div">
 <input dir="ltr" type="text"  name="publication" id="publication"  class="input_cls"/>
 </div>
</div>
<div class="input_row even med" >
<div class="input_label_div">Country of Origin</div>
<div class="input_div">
 <input dir="ltr" type="text"  name="Country1" id="Country1"  class="input_cls"/>
 </div>
</div>

<div class="input_row even edu">
<div class="input_label_div">&nbsp;</div>
<div class="input_div">
<select class="input_cls" name="education_level" id="education_level" >
<option value="University">University</option>
<option value="College">College</option>
<option value="School">School</option>
<option value="Secondary">Secondary</option>
<option value="Preparatory">Preparatory</option>
<option value="Primary">Primary</option>
</select>
 </div>
</div>

<div class="input_row odd edu">
<div class="input_label_div">Gender</div>
<div class="input_div">
<select class="input_cls" name="gender" id="gender" >
<option value="Both">Both</option>
<option value="Female">Female</option>
<option value="Male">Male</option>
</select>
 </div>
</div>





<div class="input_row odd">
<div class="input_label_div"><span>Proposed date of visit:</span><span class="red">*</span></div>
<div class="input_div"> <input type="text" name="date_visit" id="date_visit" class="input_cls"/>  </div>


</div>


    <div class="input_row even">
        <div class="input_label_div"><span>Proposed time of visit:</span><span class="red">*</span>

        </div>
        <select name="prop_time" id="prop_time" class="input_cls_2" style="direction:ltr" >
            <option value=""></option>
            <option value="09:00:00">9:00</option>
             <option value="09:30:00">9:30</option>
            <option value="10:00:00">10:00</option>
            <option value="10:30:00">10:30</option>
            <option value="11:00:00">11:00</option>
            <option value="11:30:00">11:30</option>
            <option value="12:00:00">12:00</option>
            <option value="12:30:00">12:30</option>
            <option value="13:00:00">13:00</option>
            <option value="13:30:00">13:30</option>
           <option value="14:00:00">14:00</option>
           <option value="14:30:00">14:30</option>
           <option value="15:00:00">15:00</option>
           <option value="15:30:00">15:30</option>
           <option value="16:00:00">16:00</option>
           <option value="16:30:00">16:30</option>
             <option value="17:00:00">17:00</option>
           <option value="17:30:00">17:30</option>
           <option value="18:00:00">18:00</option>
           <option value="18:30:00">18:30</option>
           <option value="19:00:00">19:00</option>
           <option value="19:30:00">19:30</option>
           <option value="20:00:00">20:00</option>
          
         
        </select>
    </div>


<div class="input_row odd">
<div class="input_label_div"><span>Group Size:</span><span class="red">*</span>
</div>
<div class="input_div">
    <input dir="ltr" type="text"  name="grp_size" id="grp_size"  class="input_cls"/>
</div>
</div>


<div class="input_row even">
<div class="input_label_div"><span>Specify any special interests or needs</span><span>for the group:
<img src="/img/questions.png" width="15" style="cursor:pointer; vertical-align:middle;" class="vtip" alt="?" title="<b>Please assist us in making your groups visit a memorable and enjoyable experience by providing us with some information. For example: </b>
<span>&bull; How many persons in your group, are they male/female/mixed?</span>
<span>&bull; Is this the group&rsquo;s first visit?</span>
<span>&bull; Where is the group from, what languages to they speak?</span>
<span>&bull; Do they have a particular interest (i.e. artists, engineers etc).</span>
<span>&bull; Are there special needs visitors?</span>"/>
</span>
<span class="red">*</span>

</div>
<div class="input_div">
    <textarea class="input_cls" name="other_info" style="width:250px;" id="other_info" cols="25" rows="8" ></textarea></div>
</div>




<div class="input_row odd">
<div style="padding-left:230px;">
<input type="submit" name="submitTour" id="submitTour" value="Submit" />
</div>
</div>

</form>

    <div class="clear"></div>
</div>

<div class="clear"></div>
</div>
        <?php
    }
    ?>
    <?php
    include_once("./templates/footer.php");

?>