<?php
include './en/includes/database.php';
include './includes/functions.php';
include './includes/config.php';
include_once('./includes/generic_functions.php');
$conf = new Configuration();
$db = new MyDatabase();
$site_path = $conf->site_url;

$slug = explode('/',$_SERVER['REQUEST_URI']);
$slug = end($slug);
session_start();
$msg="";
$current_page = 'ftplogin';

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<?php include 'includes/common_header.php'; ?>
    <title>حسابك</title>

	<link href="<?php echo $site_path; ?>css/calendar.css" rel="stylesheet" type="text/css" />

 

	<script type="text/javascript" src="<?php echo $site_path; ?>js/ajaxupload.3.5.js"></script>
	<script type="text/javascript" src="<?php echo $site_path; ?>js/jquery.validate.js"></script>
	<script type="text/javascript" src="<?php echo $site_path; ?>js/main.js"></script>
	<script type="text/javascript" src="<?php echo $site_path; ?>js/cal.js"></script>
    
<link href="<?php echo $site_path; ?>css/jquery-ui-1.8.24.custom.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo $site_path; ?>js/jquery-ui-1.8.24.custom.min.js"></script>
<style>
td{text-align:center;}
textarea{width:660px;}
div.ltr{
direction: ltr;
}
table.ltr {
direction: ltr;
}
</style>


</head>
<body >
<?php include 'includes/menus/banner_header3.php'; ?>		
			<!-- Banner Start -->
<div class="banner">
	<img src="<?php echo $site_path;?>images/visiting_the_mosque_banner.jpg">     
</div>
<!-- Banner Close -->	
<!-- Content Start -->
	<div class="main_box_content visiting_page_height">
	 	 <?php include 'includes/menus/left_menu.php'; ?>
        <div class="clear"></div>
        <div class="content">
        	<div class="brad_cram">
            	<ul>
            	   <li><a href="<?php echo $site_path; ?>">الصفحة الرئيسية</a></li>
                    <li><a href="#" class="active">حسابك</a></li>
                </ul>
            </div>
      
      		<div class="content-left">
			 <?php //include 'includes/menus/left_menu1.php'; 
				?>
				
                <br class="clear"/>
                <?php 
				include 'includes/menus/ministry_logos.php'; 
				?>
                <?php include 'includes/menus/rightsidebanner.php';?>
			</div>
	  		<div class="content-right" style="margin-right:30px">
			<div class="single_middle">
	                <p class="heading"><span dir="rtl">حسابك</span></p>
                    <div class="page_item_single">
                    	<div class="page_content" >
                            <div class="general_body_content">
							 <div class="reg_form">
                             <!--<div class="clear"><h2 style="font-size:13px;color:#000;float:left;">Your Tour Bookings</h2><h2 style="font-size:13px;color:#000;float:right">الحجوزات السياحية الخاصة بك</h2></div>
                             --><?php
if((isset($_SESSION['user_id'])) && $_SESSION['user_id']!=''){
 	include './forms/update_user_profile2.php'; ?>
    <br/>
    
    <div style="float:left;">
        <ul id="links_en" style="padding: 0">
            <li style="float:left;width: 55px;padding-top: 5px;"><a href="/en/register.php?action=edit">Edit Info</a></li>
            <li style="float:left;width: 103px;padding-top: 5px;"><a href="/en/changePassword.php">Change Password</a></li>
            <li style="float:left;width: 99px;padding: 5px 0px 0px 0px;"><span><a href="/en/user_profile.php" style="padding-right: 8px;">Your Account</a></span></li>
			<li style="float:left;width: 55px;padding: 5px 0px 0px 0px;"><span><a href="/en/userlogout.php"> Logout </a></span></li>
         </ul>   
    </div>
    <div style="float:right;">
          <ul id="links_ar" style="padding: 0">
            <li style="padding-top: 5px;"><a href="./register.php?action=edit">تحرير بيانات</a></li>
            <li style="padding-top: 5px;"><a href="./changePassword.php">تغيير كلمة المرور</a></li>
			<li style="background-position: 0px 13px;"><span><a href="/user_profile.php">حسابك</a></li>
			<li style="background: none;"><span><a href="/userlogout.php"> خروج</a></span></li>
           </ul> 
    </div>
    <div class="clear"></div>
	<?php
    $date_parts = explode('/', $obj->date_of_expiry);
    $expDate = strtotime($date_parts[2].'-'.$date_parts[1].'-'.$date_parts[0]);
    if (strtotime(date('Y-m-d')) > $expDate) {
        echo '<div style="color: red;border: 1px solid red;min-height: 40px;padding: 4px 9px 0px 7px;">Your trade licence has been expired and you will not be able to book the tour. Kindly contact the management for further clarifications.</div>';
    } else {
        //if (!empty($obj->approve)) {
        if (isset($obj->approve) & $obj->approve == 'D') {

            echo '<div style="color: red;border: 1px solid red;height: 24px;padding: 4px 16px 0px 0px;">لقد تم منع التسجيل الخاصة بك، وأنك لن تكون قادرة على حجز جولة.يرجى الاتصال بإدارة لمزيد من التوضيحات.</div>';
        } else if ($obj->approve == 'E') {
            echo '<div style="color: red;border: 1px solid red;min-height: 40px;padding: 4px 9px 0px 7px;">Your trade licence has been expired and you will not be able to book the tour. Kindly contact the management for further clarifications.</div>';
        } else if (empty($obj->approve)) {
            echo '<div style="color: red;border: 1px solid red;min-height: 40px;padding: 4px 9px 0px 7px;">Your account is not approved and you will not be able to book the tour. Kindly contact the management for further clarifications.</div>';
        } else {
            ?>

            <div class="book" style="margin-top: 15px;">
                <div><a href="./tours-booking-form">احجز جولة سياحية&nbsp;&nbsp;Book Your Tour </a></div>
            </div>
            <div class="book" style="margin-top: 15px;">

                <div><a href="./bookings.php?type=edit">تعديل على الحجز&nbsp;&nbsp;Edit Tour Booking</a></div>
            </div>
            <!--<div class="book">
                     <div><a href="./tours-booking-form">تعديل على الحجز&nbsp;&nbsp;Edit Tour Booking </a></div>
                </div>  -->
        <?php
        }//ending session var check block
    }
    //}
}
?>
    <div class="clear"></div>
</div>
							</div>
                            <br class="clear" />
                            
                        </div>
                    </div> 
                   
            </div>
			</div>
 <br class="clear" />      
        </div>
    
    </div>
    
	<div class="content_bottom">&nbsp;</div>
	<?php include 'includes/footer.php'; ?> 

</body>
</html>