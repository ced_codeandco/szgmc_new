<?php
include './en/includes/database.php';
include './includes/functions.php';
include './includes/config.php';
include_once('./includes/generic_functions.php');
$conf = new Configuration();
$db = new MyDatabase();
$site_path = $conf->site_url;

$slug = explode('/',$_SERVER['REQUEST_URI']);
$slug = end($slug);
$msg="";
session_start();
if((!isset($_SESSION['user_id'])) && $_SESSION['user_id']==''){
		if(isset($_POST['a_email']) && $_POST['a_email']!='' && isset($_POST['submitResendActivation']) ){
			foreach($_POST as $name => $value) {
				$$name=mysql_real_escape_string($value);
			}
			$sql="select * from site_users where op_email='$a_email'";
			$result=mysql_query($sql);
    		$count=mysql_num_rows($result);			
			if($count==1){
				if($obj = mysql_fetch_object($result)){	
					$active=$obj->active;
					if($active==1){
						//if already active then send login credentials
						$username=$obj->email;
						$pswd = $obj->password;
						$msg="already activated";
						$fromName='Tours';
						$fromEmail="tour@szgmc.ae";
						$toName=$obj->op_email;
						$toEmail=$obj->op_email;
						$subject = "Login Details";
						$usercontent_mail="<html>
	<head><meta http-equiv='Content-Type' content='text/html; charset=utf-8' /></head>
		<body>
			<table width='600' cellspacing='2' cellpadding='0' style='direction:ltr;font-family: Arial,Verdana,Helvetica,sans-serif;'>
            	<tr>
					 <td style='background-color: #D2A118;color: #FFFFFF;font-weight: bold;height: 30px;padding-left:30px;padding-right:30px'>
					 	<table width='600' cellspacing='0' cellpadding='0'>
					    	<tr>
					        	<td valign='middle' style='color: #FFFFFF' ><b style='font-size:16px;'>Login </b></td>
                                <td valign='middle' align='right' style='color:#FFFFFF'> <b style='direction:rtl; font-size:16px;'></b></td> 
                            </tr>
					   </table>
					 </td> 
				</tr>
				<tr>
					<td valign='top' align='center' style='border:1px solid #D2A118;padding-left:30px;padding-right:30px'>
					    <table width='100%' cellspacing='0' cellpadding='0'>
							<tr><td colspan='2' style='padding:12px'></td></tr>
							<tr>
								<td width='50%' style='color:#C7A317'><b style='font-size:14px'>Dear User,</b></td>
								<td width='50%' style='color:#C7A317;direction:rtl;'><b style='font-size:15px'>عزيزي المستخدم،</b></td>
							</tr>
							<tr><td colspan='2' style='padding:12px'></td></tr>
							<tr>
								<td style='color:#000;'><span style='font-size:14px'>Your login details are below:</span></td>
								<td style='color:#000;direction:rtl;'><span style='font-size:15px'>البيانات الخاصة بالدخول كالتالي:</span></td>
							</tr>
							<tr><td colspan='2' style='padding:12px'></td></tr>
							<tr>
								<td style='color:#C7A317;'><span style='font-size:14px'>Username:".$username."</span></td>
								<td style='color:#C7A317;direction:rtl;'><span style='font-size:15px'>اسم المستخدم:".$username."</span></td>
							</tr>
							<tr><td colspan='2' style='padding:6px'></td></tr>
							<tr>
								<td style='color:#C7A317;'><span style='font-size:14px'>Password:".$pswd."</span></td>
								<td style='color:#C7A317;direction:rtl;'><span style='font-size:15px'>كلمة المرور:".$pswd."</span></td>
							</tr>
							<tr><td colspan='2' style='padding:14px'></td></tr>
							<tr>
								<td style='color:#C7A317; font-family: Arial;padding-bottom: 19px;'><span style='font-size:14px'>Kind Regards,<br>SZGMC</span></td>
								<td style='color:#C7A317;direction:rtl;font-family: Arial;padding-bottom: 19px;'><span style='font-size:15px'>مع تحيات<br>مركز جامع الشيخ زايد الكبير</span></td>
							</tr>
							<tr><td colspan='2'>&nbsp;</td></tr>
						</table>
					</td> 
                </tr> 
		  </table>      
	</body>
</html>";
						if($msg="already activated"){
							sendEmail($toName, $fromName, $toEmail, $fromEmail, $subject, $usercontent_mail);
						}
					}else{
						$msg="resend activation mail";
						$toName=$obj->op_email;
						$toEmail=$obj->op_email;
						$hash=$obj->key;
						//send activation mail
						$content="<html>
	<head><meta http-equiv='Content-Type' content='text/html;charset=UTF-8'></head>
		<body>
			<table width='600' cellspacing='2' cellpadding='0' style='direction:ltr;font-family: Arial,Verdana,Helvetica,sans-serif;'>
            	<tr>
					 <td style='background-color: #D2A118;color: #FFFFFF;font-weight: bold;height: 30px;padding-left:30px;padding-right:30px'>
					 	<table width='600' cellspacing='0' cellpadding='0'>
					    	<tr>
					        	<td valign='middle' style='color: #FFFFFF' ><b style='font-size:16px;'>Registration </b></td>
                                <td valign='middle' align='right' style='color:#FFFFFF'> <b style='direction:rtl; font-size:16px;'></b></td> 
                            </tr>
					   </table>
					 </td> 
				</tr>
				<tr>
					<td valign='top' align='center' style='border:1px solid #D2A118;padding-left:30px;padding-right:30px'>
						<table cellspacing='0' cellpadding='0'>
							<tr><td colspan='2' style='padding:12px'></td></tr>
							<tr>
								<td width='50%' style='color:#C7A317'><b style='font-size:14px'>Dear User,</b></td>
								<td width='50%' style='color:#C7A317;direction:rtl;'><b style='font-size:15px'>عزيزي المستخدم،</b></td>
							</tr>
							<tr><td colspan='2' style='padding:14px'></td></tr>
							<tr>
								<td style='color:#000;'><span style='font-size:14px'>You're receiving this email  because you filled out a registration form on our website</span></td>
								<td align='right' style='color:#000' dir='rtl'><span style='font-size:15px;'> لقد قمت بتعبئة الإستمارة الخاصة بالتسجيل على موقعنا الإلكتروني</span> </td>
							</tr>
							<tr><td colspan='2' style='padding:12px'></td></tr>
							<tr>
								<td style='color:#000;'><span style='font-size:14px'>To complete your registration, please click on the link below:</span></td>
								<td style='color:#000;direction:rtl;'><span style='font-size:15px'>لإكمال عملية التسجيل يرجى الضغط على الرابط التالي:</span></td>
							</tr>
							<tr><td colspan='2' style='padding:12px'></td></tr>
							<tr>
								<td style='color:#C7A317;' colspan='2' align='center'><a href='http://szgmc.ae/en/userlogin.php?email=".$toEmail."&hash=".$hash."' style='font-size:14px;text-decoration:none;color:#C7A317;'>http://szgmc.ae/en/userlogin.php?email=".$toEmail."&hash=".$hash."</a></span></td>
								
							</tr>
							<tr><td colspan='2' style='padding:14px'></td></tr>
							<tr>
								<td style='color:#C7A317;font-family: Arial;padding-bottom: 19px;'><span style='font-size:14px'>Kind Regards,<br>SZGMC</span></td>
								<td style='color:#C7A317;direction:rtl;font-family: Arial;padding-bottom: 19px;'><span style='font-size:15px'>مع تحيات<br>مركز جامع الشيخ زايد الكبير</span></td>
							</tr>
							<tr><td colspan='2'>&nbsp;</td></tr>
						</table>
					</td>
                </tr> 
		  </table>      
	</body>
</html>";			
$subject = 'Verification'; 
$fromName = 'SZGMC';
$fromEmail = 'tour@szgmc.ae'; 
if($msg=="resend activation mail"){
	sendEmail($toName, $fromName, $toEmail, $fromEmail, $subject, $content);
}
}
					
				}
			}else{
				$msg='not registered';
			}
		}//isset($_POST['a_email']
}//isset($_SESSION['user_id'])
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<?php include 'includes/common_header.php'; ?>
    <title>إعادة إرسال رابط التفعيل</title>

	<link href="<?php echo $site_path; ?>css/calendar.css" rel="stylesheet" type="text/css" />

 

	<script type="text/javascript" src="<?php echo $site_path; ?>js/ajaxupload.3.5.js"></script>
	<script type="text/javascript" src="<?php echo $site_path; ?>js/jquery.validate.js"></script>
	<script type="text/javascript" src="<?php echo $site_path; ?>js/main.js"></script>
	<script type="text/javascript" src="<?php echo $site_path; ?>js/cal.js"></script>
    
<link href="<?php echo $site_path; ?>css/jquery-ui-1.8.24.custom.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo $site_path; ?>js/jquery-ui-1.8.24.custom.min.js"></script>
<script type="text/javascript">

 $(document).ready(function(){
    $("#resendActivationForm").validate({
			rules: {
				a_email: {
					required: true,
					email: true
				}

	  								
			}
		});
  });
  </script>
<style>
td{text-align:center;}
textarea{width:660px;}
div.ltr{
direction: ltr;
}
table.ltr {
direction: ltr;
}
</style>


</head>
<body >
<?php include 'includes/menus/banner_header3.php'; ?>		
			<!-- Banner Start -->
<div class="banner">
	<img src="<?php echo $site_path;?>images/visiting_the_mosque_banner.jpg">     
</div>
<!-- Banner Close -->	
<!-- Content Start -->
	<div class="main_box_content visiting_page_height">
	 	 <?php include 'includes/menus/left_menu.php'; ?>
        <div class="clear"></div>
        <div class="content">
        	<div class="brad_cram">
            	<ul>
            	   <li><a href="<?php echo $site_path; ?>">الصفحة الرئيسية</a></li>
                    <li><a href="#" class="active">إعادة إرسال رابط التفعيل</a></li>
                </ul>
            </div>
      
      		<div class="content-left">
			 <?php //include 'includes/menus/left_menu1.php'; 
				?>
				
                <br class="clear"/>
                <?php 
				include 'includes/menus/ministry_logos.php'; 
				?>
                <?php include 'includes/menus/rightsidebanner.php';?>
			</div>
	  		<div class="content-right" style="margin-right:30px">
			<div class="single_middle">
		            <p class="heading"><span dir="rtl">إعادة إرسال رابط التفعيل</span></p>
                    <div class="page_item_single">
                    	<div class="page_content" >
                            <div class="general_body_content">
							 <div class="reg_form">
                               <div class="text" style="float:right; text-align:right; margin-right:8px;">
	       
    </div>



<?php include './forms/resendActivation.php'; ?>

    <div class="clear"></div>
</div>
							</div>
                            <br class="clear" />
                            
                        </div>
                    </div> 
                   
            </div>		
			</div>
 <br class="clear" />      
        </div>
    
    </div>
    
	<div class="content_bottom">&nbsp;</div>
	<?php include 'includes/footer.php'; ?> 
 <?php
	 if($msg == 'resend activation mail')
		{
		?>
		  <script>
			$(function() {
				$( "#dialog" ).dialog();
			});
		</script>
        <script type="text/javascript">
		$(function(){
			$('#confirm').click(function() {
				$( "#dialog" ).dialog( "close" );
				return false;
			});
		});
		
        </script>
        <div id="dialog">
        	<p style="text-align:right;"><span dir="rtl">تم إرسال بريد إلكتروني على عنوان البريد الخاص بكم، يرجى تأكيد التسجيل حتى تكمل العملية بنجاح.</span></p>
            <p>A verification mail has been sent to your operations email address. Please verify to complete the registration.</p>
            <p style=""><input type="button" value="OK" style="float:right;width:40px" id="confirm"></p>
        </div>
		 <?php
		  
		}else if($msg == 'already activated'){
		?>
		  <script>
			$(function() {
				$( "#dialog" ).dialog();
			});
		</script>
        <script type="text/javascript">
		$(function(){
			$('#confirm').click(function() {
				$( "#dialog" ).dialog( "close" );
				return false;
			});
		});
		
        </script>
        <div id="dialog">
        	<p style="text-align:right;"><span dir="rtl"></span></p>
            <p>Your account is already active.Your login details are mailed to your email address</p>
            <p style=""><input type="button" value="OK" style="float:right;width:40px" id="confirm"></p>
        </div>
		 <?php
		  
		}else if($msg == 'not registered'){
		?>
		  <script>
			$(function() {
				$( "#dialog" ).dialog();
			});
		</script>
        <script type="text/javascript">
		$(function(){
			$('#confirm').click(function() {
				$( "#dialog" ).dialog( "close" );
				return false;
			});
		});
		
        </script>
        <div id="dialog">
        	<p style="text-align:right;"><span dir="rtl">ليس لديك حساب</span></p>
            <p>You dont have an account</p>
            <p style=""><input type="button" value="OK" style="float:right;width:40px" id="confirm"></p>
        </div>
		 <?php
		  
		}
	?>
    </body>
</html>