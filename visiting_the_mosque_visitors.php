<?php
session_start();
include './includes/database.php';
include './includes/functions.php';
include './includes/config.php';
include_once('./includes/generic_functions.php');
$conf = new Configuration();
$db = new MyDatabase();
$site_path = $conf->site_url;

$slug = explode('/',$_SERVER['REQUEST_URI']);
$slug = end($slug);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<?php include 'includes/common_header.php'; ?>
    <title>زيارة المسجد</title>
</head>
<body >

<?php include 'includes/menus/banner_header3.php'; ?>
			<!-- Banner Start -->
<div class="banner">
	<img src="<?php echo $site_path;?>images/visiting_the_mosque_banner.jpg">
</div>
<!-- Banner Close -->	
<!-- Content Start -->
	<div class="main_box_content visiting_page_height">
	 	 <?php include 'includes/menus/left_menu.php'; ?>
        <div class="clear"></div>
        <div class="content">
        	<div class="brad_cram">
            	<ul>
            	   <li><a href="<?php echo $site_path; ?>">الصفحة الرئيسية</a></li>
                    <li><a href="<?php echo $site_path; ?>" class="active">
                            الزوار
                        </a></li>
                </ul>
            </div>
      
      		<div class="inside_conter">
        		<div class="Visiting_box">
                    <ul>
                        <li class="fist_tham">
                            <a href="<?php echo $site_path; ?>mosque-manner">
                                <img src="<?php echo $site_path; ?>images/Visiting-The -osque/Visiting_The_Mosque_01.jpg">
                                <span><img src="<?php echo $site_path; ?>images/Visiting-The -osque/tham01.png">
                                آداب دخول الجامع
                                </span>
                            </a>
                        </li>

                        <li>
                            <a href="<?php echo $site_path; ?>mosque-opening-hours">
                                <img src="<?php echo $site_path; ?>en/images/Visiting-The -osque/Visiting_The_Mosque_02.jpg">
                                <span><img src="<?php echo $site_path; ?>en/images/Visiting-The -osque/tham02.png">اوقات الزيارة</span>
                            </a>
                        </li>

                        <li>
                            <a href="<?php echo $site_path; ?>cultural-tours">
                                <img src="<?php echo $site_path; ?>images/Visiting-The -osque/Visiting_The_Mosque_05.jpg">
                                <span><img src="<?php echo $site_path; ?>images/Visiting-The -osque/tham05.png">
                                الجولات الثقافية
                                </span>
                            </a>
                        </li>

                   
                        <li class="fist_tham">
                            <a href="<?php echo $site_path; ?>e-guide">
                                <img src="<?php echo $site_path; ?>images/Visiting-The -osque/Visiting_The_Mosque_04.jpg">
                                <span><img src="<?php echo $site_path; ?>images/Visiting-The -osque/tham04.png"> المرشد الالكتروني</span>
                            </a>
                        </li>

                        <li>
                            <a href="<?php echo $site_path; ?>getting-to-the-mosque">
                                <img src="<?php echo $site_path; ?>images/Visiting-The -osque/Visiting_The_Mosque_07.jpg">
                                <span><img src="<?php echo $site_path; ?>images/Visiting-The -osque/tham07.png"> كيفية الوصول</span>
                            </a>
                        </li>

                        <li>
                            <a target="_blank" href="https://www.google.ae/maps/streetview/#sheikh-zayed-grand-mosque">
                                <img src="<?php echo $site_path; ?>images/Visiting-The -osque/Visiting_The_Mosque_08.jpg">
                                <span><img src="<?php echo $site_path; ?>images/visiting_the_mosque/virtual_tour.png"> التجول الافتراضي </span>
                            </a>
                        </li>

                   
                        <li class="fist_tham">
                            <!--a href="<?php echo $site_path; ?>questions"-->
                            <a href="<?php echo $site_path; ?>faqs">
                                <img src="<?php echo $site_path; ?>images/Visiting-The -osque/Visiting_The_Mosque_10.jpg">
                                <span><img src="<?php echo $site_path; ?>images/Visiting-The -osque/tham10.png"> أسئلة متكررة </span>
                            </a>
                        </li>

                        <li>
                            <a href="<?php echo $site_path; ?>services">
                                <img src="<?php echo $site_path; ?>images/Visiting-The -osque/Visiting_The_Mosque_11.jpg">
                                <span><img src="<?php echo $site_path; ?>images/Visiting-The -osque/info-thumb.jpg">
                                الخدمات
                                </span>
                            </a>
                        </li>

                    </ul>
                </div>
        </div>
	  		
	  
            <br class="clear" />      
        </div>
    
    </div>
<!-- Content Close -->
	<?php include 'includes/menus/marquees_partner.php';?>
	<div class="content_bottom">&nbsp;</div>
	<?php include 'includes/footer.php'; ?> 

</body>
</html>