<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 12/15/2015
 * Time: 2:22 PM
 */
include_once('PHPMailerAutoload.php');

date_default_timezone_set('Asia/Dubai');
function sendSafeEmail($toName, $toEmail, $subject, $content, $attachments = '', $bccList = '', $iCal = ''){

    $fromName='SZGMC';
    $fromEmail='tour@szgmc.ae';

    //For test purpose
    //$toEmail = 'anas@codeandco.ae';
    $mailer = new PHPMailer();
    $mailer->IsHTML(true);

    $mailer->CharSet = 'UTF-8';
    $mailer->SetLanguage('ar');
    $mailer->From = $fromEmail;  // This HAVE TO be your gmail adress
    $mailer->FromName = $fromName; // This is the from name in the email, you can put anything you like here
    $mailer->Body = $content;
    $mailer->AltBody = "HTML Content not supported";
    $mailer->Subject = $subject;
    $mailer->AddAddress($toEmail, $toName);
    if (!empty($attachments)) {
        $attachmentsArray = explode(',', $attachments);
        foreach ($attachmentsArray as $val) {
            if (!empty($val) && file_exists($val) && !is_dir($val)) {
                $mailer->addAttachment($val);
            }
        }
    }
    if (!empty($bccList)) {
        $bccArray = explode(',', $bccList);
        foreach ($bccArray as $val) {
            if (!empty($val) && file_exists($val) && !is_dir($val)) {
                $mailer->addBCC($val);
            }
        }
    }
    //For test purpose
    //$mailer->addBCC('anas.muhammed@gmail.com');
    //$mailer->addBCC('anas@codeandco.ae');
    if (!empty($iCal)) {
        $mailer->Ical = $iCal;
    }

    if(!$mailer->Send())
    {
        return TRUE;
        // echo "Message was not sent<br/ >";
        // echo "Mailer Error: " . $mailer->ErrorInfo;
    }else{
        //echo "Mail sent<br/ >";
        return TRUE;
    }
}

