<?php
//error_reporting(E_ALL);
//ini_set("display_errors", 1);
//============================================================+
// File name   : example_061.php
// Begin       : 2010-05-24
// Last Update : 2014-01-25
//
// Description : Example 061 for TCPDF class
//               XHTML + CSS
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: XHTML + CSS
 * @author Nicola Asuni
 * @since 2010-05-25
 */
//define('K_PATH_IMAGES', realpath(__DIR__.'/../images/').'/');
// Include the main TCPDF library (search for installation path).
require_once('tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
//$pdf->SetCreator(PDF_CREATOR);
//$pdf->SetAuthor('Nicola Asuni');
//$pdf->SetTitle('TCPDF Example 061');
//$pdf->SetSubject('TCPDF Tutorial');
//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
//$pdf->SetHeaderData('', PDF_HEADER_LOGO_WIDTH, "Sheikh Sayed Grand Mosque Center", 'Tour Operator Registration Form', array(186,159,20));
$pdf->SetHeaderData('', PDF_HEADER_LOGO_WIDTH, "", '', array(186,159,20));
// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
//$pdf->SetFont('helvetica', '', 10);
//fONT SUPPORTS ARABIC AND ENGLISH
$pdf->SetFont('aefurat', '', 10);
// add a page
$pdf->AddPage();

/* NOTE:
 * *********************************************************
 * You can load external XHTML using :
 *
 * $html = file_get_contents('/path/to/your/file.html');
 *
 * External CSS files will be automatically loaded.
 * Sometimes you need to fix the path of the external CSS.
 * *********************************************************
 */
$cwdir = getcwd();
// define some HTML content with style
$org = "FFFFFFFFFF";
$html = <<<EOF
<!-- EXAMPLE OF CSS STYLE -->
<style>
	h1 {
		color: navy;
		font-family: times;
		font-size: 12pt;
		/*text-decoration: underline;*/
	}
	p.first {
		color: #003300;
		font-family: helvetica;
		font-size: 12pt;
	}
	p.first span {
		color: #006600;
		font-style: italic;
	}
	p#second {
		color: rgb(00,63,127);
		font-family: times;
		font-size: 12pt;
		text-align: justify;
	}
	p#second > span {
		background-color: #FFFFAA;
	}
	table.first {
		/*color: #003300;
		font-family: helvetica;
		font-size: 8pt;
		border-left: 1px solid #BA9F14;
		border-right: 1px solid #BA9F14;
		border-top: 1px solid #BA9F14;
		border-bottom: 1px solid #BA9F14;
		background-color: #BA9F14;*/
	}
	.row-bg td {
		background-color: #F7F8FA;
	}
	td.second {
		/*border: 2px dashed green;*/
	}
	div.test {
		color: #CC0000;
		background-color: #FFFF66;
		font-family: helvetica;
		font-size: 10pt;
		border-style: solid solid solid solid;
		border-width: 1px 1px 1px 1px;
		border-color: green #FF00FF blue red;
		text-align: center;
	}
	.lowercase {
		text-transform: lowercase;
	}
	.uppercase {
		text-transform: uppercase;
	}
	.capitalize {
		text-transform: capitalize;
	}
	.title-row td{color: #000000; background-color: #C4BD97;}
	.content-row td{border-bottom: 1px solid #BA9F14;}
	.full_width_head{text-align:center; font-size:20pt;}
</style>

<!--<h1 class="title" style="color:#BA9F14">Sheikh Syed Grand Mosque Center<br /><i style="color:#000000; font-weight:normal;">Tour Operator Registration Form</i></h1>-->


<table class="first" cellpadding="4" cellspacing="6">
    <tr>
	   <td colspan="2"></td>
	   <td width="150">
	   <table>
	   	<tr>
			<td>
			   <img height="100" src="$cwdir/images/logo.png" />
	   		</td>
			<td>
			   <img height="100" src="$cwdir/images/Top_Right_Logo.jpg" />
	   		</td>
		</tr>
		</table>
	   </td>
	</tr>
	<tr>
		<td colspan="3" class="full_width_head">
		طلب تسجيل شركة سياحية
		</td>
	</tr>
	<tr>
		<td colspan="3" class="full_width_head">
		Tourism Companies Registration Form
		</td>
	</tr>
	<tr>
		<td colspan="3" dir="rtl" align="right">
	يرجى التكرم بالموافقة على تسجيل الشركة أدناه، ضمن سجل الشركات السياحية المسجلة في النظام الالكتروني الخاص لديكم وذلك حسب البيانات التالية :
		<br /><br /></td>
	</tr>
	<tr>
		<td colspan="3">
		Kindly approve the registration of the below mentioned company, in the Tourism Companies register within your entities electronic systems , according to the following data:
		<br /><br /></td>
	</tr>
    <tr class="title-row">
        <td width="150" align="center">
			Company Detalis
        </td>
        <td width="300" align="center"></td>
        <td width="150" align="center">
             بيانات الشركة
        </td>
    </tr>
    <tr class="content-row">
        <td width="150" align="center"><br /><br />
            Company Name
        </td>
        <td width="300" align="center"><br /><br />
            $_POST[a_companyname]
        </td>
        <td width="150" align="center"><br /><br />
        اسم الشركة
        </td>
    </tr>
    <tr class="content-row">
        <td width="150" align="center"><br /><br />
            Trade License No
        </td>
        <td width="300" align="center"><br /><br />
            $_POST[a_tradelicense]
        </td>
        <td width="150" align="center"><br /><br />
        رقم الرخصة التجارية
        </td>
    </tr>
    <tr class="content-row">
        <td width="150" align="center"><br /><br />
            Emirate / issuing Authority:
        </td>
        <td width="300" align="center"><br /><br />
            $_POST[authority]
        </td>
        <td width="150" align="center"><br /><br />
        الإمارة / جهة الإصدار
        </td>
    </tr>
    <tr class="content-row">
        <td width="150" align="center"><br /><br />
            Expiry Date
        </td>
        <td width="300" align="center"><br /><br />
            $_POST[pub_date]
        </td>
        <td width="150" align="center"><br /><br />
        تاريخ الإنتهاء
        </td>
    </tr>
	<tr>
		<td colspan="3">
<br />
		</td>
	</tr>
    <tr class="title-row">
        <td width="150" align="center">
            <span class="title">Contact Information</span>
        </td>
        <td width="300" align="center"></td>
        <td width="150" align="center">
            <span class="title">بيانات الاتصال</span>
        </td>
    </tr>
    <tr class="content-row">
        <td width="150" align="center"><br /><br />
            General Manager's Name
        </td>
        <td width="300" align="center"><br /><br />
            $_POST[a_fname]
        </td>
        <td width="150" align="center"><br /><br />
        بيانات الاتصال
        </td>
    </tr>
    <tr class="content-row">
        <td width="150" align="center"><br /><br />
            Office Tel. No.
        </td>
        <td width="300" align="center"><br /><br />
            $_POST[countryCode]
            $_POST[a_op_city_code2]
            $_POST[a_op_phoneno_new2]
        </td>
        <td width="150" align="center"><br /><br />
        رقم المكتب
        </td>
    </tr>
    <tr class="content-row">
        <td width="150" align="center"><br /><br />
            GM's Email Address
        </td>
        <td width="300" align="center"><br /><br />
            $_POST[a_email_gm]
        </td>
        <td width="150" align="center"><br /><br />
        البريد الإلكتروني لمدير الشركة
        </td>
    </tr>
    <tr class="content-row">
        <td width="150" align="center"><br /><br />
            Operation Manager’s Name
        </td>
        <td width="300" align="center"><br /><br />
            $_POST[a_op_fname]
        </td>
        <td width="150" align="center"><br /><br />
        اسم مدير العمليات
        </td>
    </tr>
    <tr class="content-row">
        <td width="150" align="center"><br /><br />
            Office Tel. No.
        </td>
        <td width="300" align="center"><br /><br />
            $_POST[countryCode]
            $_POST[a_op_city_code]
            $_POST[a_op_phoneno_new]
        </td>
        <td width="150" align="center"><br /><br />
        رقم المكتب
        </td>
    </tr>
</table>
EOF;
	
	
	$html2 = <<<EOF
<!-- EXAMPLE OF CSS STYLE -->
<style>
	h1 {
		color: navy;
		font-family: times;
		font-size: 12pt;
		/*text-decoration: underline;*/
	}
	p.first {
		color: #003300;
		font-family: helvetica;
		font-size: 12pt;
	}
	p.first span {
		color: #006600;
		font-style: italic;
	}
	p#second {
		color: rgb(00,63,127);
		font-family: times;
		font-size: 12pt;
		text-align: justify;
	}
	p#second > span {
		background-color: #FFFFAA;
	}
	table.first {
		/*color: #003300;
		font-family: helvetica;
		font-size: 8pt;
		border-left: 1px solid #BA9F14;
		border-right: 1px solid #BA9F14;
		border-top: 1px solid #BA9F14;
		border-bottom: 1px solid #BA9F14;
		background-color: #BA9F14;*/
	}
	.row-bg td {
		background-color: #F7F8FA;
	}
	td.second {
		/*border: 2px dashed green;*/
	}
	div.test {
		color: #CC0000;
		background-color: #FFFF66;
		font-family: helvetica;
		font-size: 10pt;
		border-style: solid solid solid solid;
		border-width: 1px 1px 1px 1px;
		border-color: green #FF00FF blue red;
		text-align: center;
	}
	.lowercase {
		text-transform: lowercase;
	}
	.uppercase {
		text-transform: uppercase;
	}
	.capitalize {
		text-transform: capitalize;
	}
	.title-row td{color: #000000; background-color: #C4BD97;}
	.content-row td{border-bottom: 1px solid #BA9F14;}
	.full_width_head{text-align:center; font-size:20pt;}
</style>
<table class="first" cellpadding="4" cellspacing="6">
    <tr class="content-row">
        <td width="150" align="center"><br /><br />
            Mobile No.
        </td>
        <td width="300" align="center"><br /><br />
            $_POST[countryCode]
            $_POST[a_op_areacode]
            $_POST[a_op_mobile]
        </td>
        <td width="150" align="center"><br /><br />
        رقم الموبايل
        </td>
    </tr>
    <tr class="content-row">
        <td width="150" align="center"><br /><br />
            Email Address
        </td>
        <td width="300" align="center"><br /><br />
            $_POST[a_email_op]
        </td>
        <td width="150" align="center"><br /><br />
        البريد الإلكتروني
        </td>
    </tr>
	<tr>
		<td colspan="3"><br /><br />
	أقر أنا الموقع أدناه، بأن المعلومات الموضحة أعلاه صحيحة ومطابقة للمستندات الثبوتية المرفقة، وأتحمل كافة المسئوليات المترتبة على عدم صحة أي معلومة، كما أتعهد بالإبلاغ عن أي تغيير في موعد غايته ثلاثون يوماً من 
		</td>
	</tr>
	<tr>
		<td colspan="3"><br /><br />
	I, the undersigned, confirm hereby that the above information is true and in conformity with the attached supporting documents and that I bear full responsibility arising from the non-validity of any provided information. I also pledge to report any change that may take place thereof no later than thirty days from the date effecting such change.		
		</td>
	</tr>
	
	<tr>
		<td colspan="3">
		
		<table width="1050">
	   	<tr>
			<td width="100">
			   Best regards...
	   		</td>
			<td dir="rtl" align="right">
	وتفضلوا بقبول فائق الشكر و التقدير...		
		</td>
		</tr>
		</table>
		
		</td>
	</tr>
	<tr>
		<td colspan="3">
		<table width="400">
	   	<tr>
			<td width="100"><br /><br />
			   Company Owner:
	   		</td>
			<td><br /><br />
			____________________
			</td>
			<td><br /><br />
			   مالك الشركة
	   		</td>
		</tr>
		<tr>
			<td><br /><br />
			   Signature:
	   		</td>
			<td><br /><br />
			____________________
			</td>
			<td><br /><br />
				التوقيع
	   		</td>
		</tr>
		<tr>
			<td><br /><br />
			   Date:
	   		</td>
			<td><br /><br />
			____________________
			</td>
			<td><br /><br />
			   التاريخ
	   		</td>
		</tr>
		</table>
		</td>
	</tr>

</table>
EOF;
/***********************Add attachments*************************/
// Image example with resizing
//$pdf->Image("$cwdir/images/logo.png", 15, 140, 75, 113, 'PNG', 'http://www.tcpdf.org', '', true, 150, '', false, false, 1, false, false, false);
$pdf->writeHTML($html, true, false, true, false, '');
$pdf->AddPage();
$pdf->writeHTML($html2, true, false, true, false, '');
//$pdf->AddPage();

//ini_set('display_errors', 0);



// output the HTML content
//$pdf->writeHTML($mailContent, true, false, true, false, '');

//$pdf->writeHTML($html, true, false, true, false, '');

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

// add a page
/*$pdf->AddPage();

$html = '
<h1>HTML TIPS & TRICKS</h1>

<h3>REMOVE CELL PADDING</h3>
<pre>$pdf->SetCellPadding(0);</pre>
This is used to remove any additional vertical space inside a single cell of text.

<h3>REMOVE TAG TOP AND BOTTOM MARGINS</h3>
<pre>$tagvs = array(\'p\' => array(0 => array(\'h\' => 0, \'n\' => 0), 1 => array(\'h\' => 0, \'n\' => 0)));
$pdf->setHtmlVSpace($tagvs);</pre>
Since the CSS margin command is not yet implemented on TCPDF, you need to set the spacing of block tags using the following method.

<h3>SET LINE HEIGHT</h3>
<pre>$pdf->setCellHeightRatio(1.25);</pre>
You can use the following method to fine tune the line height (the number is a percentage relative to font height).

<h3>CHANGE THE PIXEL CONVERSION RATIO</h3>
<pre>$pdf->setImageScale(0.47);</pre>
This is used to adjust the conversion ratio between pixels and document units. Increase the value to get smaller objects.<br />
Since you are using pixel unit, this method is important to set theright zoom factor.<br /><br />
Suppose that you want to print a web page larger 1024 pixels to fill all the available page width.<br />
An A4 page is larger 210mm equivalent to 8.268 inches, if you subtract 13mm (0.512") of margins for each side, the remaining space is 184mm (7.244 inches).<br />
The default resolution for a PDF document is 300 DPI (dots per inch), so you have 7.244 * 300 = 2173.2 dots (this is the maximum number of points you can print at 300 DPI for the given width).<br />
The conversion ratio is approximatively 1024 / 2173.2 = 0.47 px/dots<br />
If the web page is larger 1280 pixels, on the same A4 page the conversion ratio to use is 1280 / 2173.2 = 0.59 pixels/dots';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');*/

// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
//$pdf->Output('example_061.pdf', 'I');
$pdf->Output(__DIR__.'/regnForm.pdf', 'F');
return __DIR__.'/regnForm.pdf';
//============================================================+
// END OF FILE
//============================================================+
