<div class="right arrow bold">Sheikh Zayed and the Grand Mosque in Abu Dhabi</div>
<div class="clear"></div>
<div class="content_topbox">
      <div class="arabic_box" style="width:330px;">
        <p dir="rtl">
		<a href="/en/sheikh-zayed-and-grand-mosque">
                 
	Sheikh Zayed bin Sultan Al Nahyan (1918-2004) was well known for his contributions as a leader of strong will, insight and vision.&nbsp; After achieving the foundation of the United Arab Emirates, Zayed was able to instigate and realize great contributions to his country which dramatically altered the life and destiny of his citizens on an economical, intellectual and social level.&nbsp; He was able to build a modern and sophisticated country while preserving the particular cultural identity of his nation...... More 
		</a>
		</p>
		  <div class="clear"></div>
      </div>
	  
      <div style="float: left; padding:10px 10px 0px 10px;"><img src="/en/img/o_calligraphy1.jpg">
	  

	  </div>
    </div>
<div class="right arrow bold">Minister's Words of Highness</div>
<div class="clear"></div>
<div class="content_topbox">
      <div class="arabic_box">
        <p dir="rtl">
		<a href="/en/minister-hh">
                 
	The Sheikh Zayed Grand Mosque represents great sentimental and moral value for the people of the UAE, and their leadership. It also constitutes one of the most prominent and beautiful architectural monuments in the world. Perhaps what is reflected in the Mosque, in terms of the unique beauty of its architecture, is an embodiment of such sentimental value. The Mosque also represents a fertile spiritual space, enriched throughout the ages by the shining inheritance of Islam. It seeks to truly reflect the open spirit of renaissance which has long inspired humanity with its great purity..... More 
		</a>
		</p>
		  <div class="clear"></div>
      </div>
	  
      <div style="float: left; padding:10px 10px 0px 10px;"><img src="/en/img/ministry-officials_img.jpg">
	  

	  </div>
    </div>
<div class="clear"></div>
<div class="right arrow bold">News of the Center</div>
<div class="clear"></div>
<div class="content_topbox">

<div class="news_div">

<?php
$sql="SELECT news_id,news_text,news_title,news_title_eng,image,DATE_FORMAT(news_date,'%M %d, %Y') as news_date_formated from news where live='Y' order by news_date DESC LIMIT 1,3";
$result=$mydb->query($sql);
while($obj=mysql_fetch_object($result))	
{

    $news_title=$obj->news_title;
    $news_title_eng=$obj->news_title_eng;

    $news_short=strip_tags($obj->news_text);
    //$seo_desc=preg_replace('/body {(.*)}/','',$seo_desc);
    $news_short=substr($news_short,0,250);
    $news_short=substr($news_short,0,strripos($news_short,' '));
    $news_short=Helper::UTF8ToHTML($news_short);
    $news_url="/en/news-detail/".string_to_filename($news_title_eng).'-'.$obj->news_id;


    echo '<p>';
    if($obj->image!='')
        echo '<img src="/en/news_images/'.$obj->image.'" align="right" alt="'.$news_title.'"/>';
    echo '<a href="'.$news_url.'" class="news_title">'.$news_title.'</a><br/>';
//    echo '<a href=/src/news_details_view.php?news_id='.$obj->news_id.' class="news_title">'.$news_title.'</a><br/>';
    
    if(strlen($news_short)>250)
        echo $news_short.'<a href="'.$news_url.'" class="small left"> ...</a>';
    else
        echo $news_short;
    echo '</p>';
    echo '<div class="clear"></div>';
}
?>




<a href="/en/news-list" style="float:right"><img style="float:right;" src="/en/img/button-news.jpg"/></a>



</div>
<?php
$sql="SELECT news_id,news_text,news_title,news_title_eng,image,DATE_FORMAT(news_date,'%M %d, %Y') as news_date_formated from news where live='Y' order by news_date DESC LIMIT 1";
$result=$mydb->query($sql);
while($obj=mysql_fetch_object($result))
{
    $news_title=$obj->news_title;
    $news_title_eng=$obj->news_title_eng;

    $news_short=strip_tags($obj->news_text);
    //$seo_desc=preg_replace('/body {(.*)}/','',$seo_desc);
    $news_short=substr($news_short,0,250);
    $news_short=substr($news_short,0,strripos($news_short,' '));
    $news_short=Helper::UTF8ToHTML($news_short);
    $news_url="/en/news-detail/".string_to_filename($news_title_eng).'-'.$obj->news_id;

    ?>

<div class="latest_news_div right">
<p>
    <?php
        if(empty($obj->image) || $obj->image == null ){
    ?>
    <img src="/en/news_images/mosque.jpg" style="max-height: 250px; max-width: 264px;" />
    <?php
        }else{
    ?>
    <img src="/en/news_images/<?php echo $obj->image; ?>" style="max-height: 250px; max-width: 264px;" />
    <?php
        }
    ?>
    <br>
<a href="<?php echo $news_url; ?>"><b><?php echo $news_title; ?></b></a>
<br/>
<?php 
    if(strlen($news_short)>250)
        echo $news_short.'<a href="'.$news_url.'" class="">...</a>';
    else
        echo $news_short.'<a href="'.$news_url.'" class="">...</a>';
?>
</p>

<?php
}
?>
</div>



<div class="clear"></div>
</div>
<div class="clear"></div>