
<script type="text/javascript">
var myDate = new Date();
start_date=(myDate.getMonth() + 1)+ "/"+myDate.getDate()+ "/" + myDate.getFullYear();	
end_date=(myDate.getMonth() + 1)+ "/" + myDate.getDate() + "/" + (myDate.getFullYear()+10);	
//
//$(document).ready(function () {
//    $('#pub_date').simpleDatepicker({ chosendate: start_date, startdate: start_date, enddate: end_date });
//    //$('#pub_date').simpleDatepicker({ chosendate: '1/1/1980', startdate: '1/1/1980', enddate: '1/1/2005' });
//    $('#pub_date').focus(function() {
//    $("#pub_date").click();
//    });
//
//
//    $('#prop_date').simpleDatepicker({ chosendate: start_date, startdate: start_date, enddate: end_date });
//    //$('#pub_date').simpleDatepicker({ chosendate: '1/1/1980', startdate: '1/1/1980', enddate: '1/1/2005' });
//    $('#prop_date').focus(function() {
//    $("#prop_date").click();
//    });
//});


$(document).ready(function () {	
		

       
$('#dob').simpleDatepicker({chosendate: start_date, startdate: '1/1/1950', enddate: '1/1/2045' });
       
        //$('#pub_date').simpleDatepicker({ chosendate: '1/1/1980', startdate: '1/1/1980', enddate: '1/1/2005' });
        $('#dob').focus(function() {
        $("#dob").click();
        });


      

});



</script>

<script type="text/javascript">

  $(document).ready(function(){
    $("#mediaForm").validate({
			rules: {
				fname: "required",
				c_email: {
					required: true,
					email: true
				},
				fname: "required",
				nationality: "required",
				p_num: "required",
				dob: "required",
				appear: "required",
				mobile: "required",
				agree: "required",
				
                                c_email:{
								required: true,
                                    email: true
                                }

	  								
			}
		});
  });
  </script>
<script src="/en/js/multifile_compressed.js"></script>
<style>
#upload{
	margin:30px 200px; padding:15px;
	font-weight:bold; font-size:1.3em;
	font-family:Arial, Helvetica, sans-serif;
	text-align:center;
	background:#f2f2f2;
	color:#3366cc;
	border:1px solid #ccc;
	width:150px;
	cursor:pointer !important;
	-moz-border-radius:5px; -webkit-border-radius:5px;
}
.darkbg{
	background:#ddd !important;
}
#status{
	font-family:Arial; padding:5px;
}
ul#files{ list-style:none; padding:0; margin:0; }
ul#files li{ padding:10px; margin-bottom:2px; width:200px; float:left; margin-right:10px;}
ul#files li img{ max-width:180px; max-height:150px; }
.success{ background:#99f099; border:1px solid #339933; }
.error{ background:#f0c6c3; border:1px solid #cc6622; } </style>


<div class="right_panel">

<div class="reg_form">
    <div style="padding: 2px;">
 <p>
The Sheikh Zayed Grand Mosque is located in Abu Dhabi, the capital of the United Arab Emirates and was initiated by the country’s first President, HH Sheikh Zayed bin Sultan Al Nahyan (May God rest his soul). It is the largest mosque in the UAE and has a dedicated Center under Ministry of Presidential Affairs mandated to manage all aspects of its operations.</p>
<p>
The Sheikh Zayed Grand Mosque is a place of worship and Friday gathering; and a center of learning and discovery through its education and visitor programs.  As one of the most visited and important Islamic cultural attractions the UAE, the Sheikh Zayed Grand Mosque plays an important role in the ‘cultural and educational tourism offering’ of the United Arab Emirates.</p>
<p>
The SZGMC allows registered private sector tour operators and tourist guides to conduct 30 minute site visits to the Sheikh Zayed Grand Mosque as part of the ‘Abu Dhabi City Tour’ tourism product.</p>
<p>
For a comprehensive cultural tour with a dedicated SZGMC Culture Guide, tour operators are always encouraged to plan ahead and book their group onto one of the SZGMC tours.</p>

<p><a href="/en/SZGMC Visitor Services Tour Operator and Tourist Guide Procedures 15.8.2011 English.pdf" target="_blank">Procedures for Tour Operators and Tourist Guides Valid 2011-2012 </p>
<p><a href="/en/Sheikh Zayed Grand Mosque - Tour Operator and Tourist Guide Code of Conduct (website).pdf" target="_blank">Tour Operator Code of Conduct</p>
<p><a href="/en/img/Private Sector Tourist Guide Workshop_E-Flyer_Updated 9.5.2011.jpg" target="_blank">Tour Operator Workshop</a> </p>
    </div>
<p><b>Book online for your workshop</b></p>
<form action="/en/touroperator" method="post" id="mediaForm" name="mediaForm" enctype="multipart/form-data">


<div class="input_row even">
<div class="input_label_div"><span>First Name </span><span class="red">*</span></div>
<div class="input_div"> <input type="text" name="fname" id="fname" class="input_cls"/>  </div>
</div>

<div class="input_row odd">
<div class="input_label_div"><span>Last Name</span> </div>
<div class="input_div"> <input type="text" name="lname" id="lname" class="input_cls"/>  </div>
</div>
<div class="input_row even">
<div class="input_label_div"><span>Passport Number</span> <span class="red">*</span></div>
<div class="input_div"> <input type="text" name="p_num" id="p_num" class="input_cls"/>  </div>
</div>

<div class="input_row odd">
<div class="input_label_div"><span>Nationality (as appears in passport)</span><span class="red">*</span></div>
<div class="input_div"> <input type="text" name="nationality" id="nationality" class="input_cls"/>  </div>
</div>

<div class="input_row even">
<div class="input_label_div"><span>Date of Birth </span><span class="red">*</span></div>
<div class="input_div"> <input type="text" name="dob" id="dob" class="input_cls"/>  </div>
</div>

<div class="input_row odd">
<div class="input_label_div">Company/Sponsor</div>
<div class="input_div"> <input type="text" name="company" id="company" class="input_cls"/>  </div>
</div>


<div class="input_row even">
<div class="input_label_div">Upload documants here </div>
<div class="input_div"> 
  
   <input id="my_file_element" type="file" name="file_1" tyle="border : 0px outset #88A0C8; font-family: Arial;  font-size: 11px;  color: #003068;  text-decoration: none;  background-color: #E9EDF0;"/>
   <div id="files_list"></div>
<script>
	<!-- Create an instance of the multiSelector class, pass it the output target and the max number of files -->
	var multi_selector = new MultiSelector( document.getElementById( 'files_list' ), 5 );
	<!-- Pass in the file element -->
	multi_selector.addElement( document.getElementById( 'my_file_element' ) );
</script>
</div>
</div>
<div class="input_row odd">
<div class="input_label_div"><table><tr><td ><span>Name to appear on your certificate</span></td><td><span class="red">*</span></td></tr></table></div>
<div class="input_div"> <input type="text" name="appear" id="appear" class="input_cls"/>  </div>
</div>

<div class="input_row even">
<div class="input_label_div">Male/Female</div>
<div class="input_div"> 
<table><tr><td>
<input type="radio" name="sex" id="sex" class="input_cls" value="male" checked="checked " style="width:20px;"/> </td><td> Male  </td><td><input type="radio" name="sex" id="sex" class="input_cls" value="female" style="width:20px;"/> </td><td>Female</td></tr></table>  </div>
</div>

<div class="input_row odd">
<div class="input_label_div">Tourist Guide Licenses in the UAE </div> <div class="input_div"> &nbsp; </div>
<div class="input_label_div">ADTA License Number </div> <div class="input_div"> <input type="text" name="license" id="lecense" class="input_cls"/>  </div><br/><br/>
<div class="input_label_div">ADTA License Number </div><div class="input_div"><input type="text" class="input_cls" id="lecense2" name="license2"></div><br/><br/>
<div class="input_label_div">Tourist Guide Licenses in the UAE </div><div class="input_div"><input type="text" class="input_cls" id="lecense3" name="license3"></div><br/><br/>
<div class="input_label_div">Tourist Guide Licenses in the UAE </div><div class="input_div"><input type="text" class="input_cls" id="lecense4" name="license4"></div>


 
</div>
<div class="input_row even">
<div class="input_label_div">Workshop (Provide preferred date) </div>
<div class="input_div"> <input type="text" name="work" id="work" class="input_cls"/>  </div>
</div>
<div class="input_row odd">
<div class="input_label_div">P.O. Box (Emirate)</div>
<div class="input_div"> <input type="text" name="pob" id="pob" class="input_cls"/>  </div>
</div>
<div class="input_row even">
<div class="input_label_div"><span>Mobile</span><span class="red">*</span></div>
<div class="input_div"> <input type="text" name="mobile" id="mobile" class="input_cls"/>  </div>
</div>
<div class="input_row odd">
<div class="input_label_div">Residence Telephone</div>
<div class="input_div"> <input type="text" name="phone" id="phone" class="input_cls"/>  </div>
</div>

<div class="input_row even">
<div class="input_label_div"><span>Email Address </span><span class="red">*</span></div>
<div class="input_div"> <input type="text" name="c_email" id="c_email" class="input_cls"/>  </div>
</div>



<div class="input_row even" style="text-align:left">
   

<div class="clear"></div>
        I verify the above information is correct

        

<input type="checkbox" name="agree" id="agree" value="1" style="float:left"/>
</div>


<div class="input_row odd">
<div style="padding-left:230px;">
<input type="submit" value="Submit" />
</div>
</div>


</form>
<div class="clear"></div>
</div>

<div class="clear"></div>
</div>