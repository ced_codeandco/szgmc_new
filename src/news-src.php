<div  id="centre_panel_inner_auto">
<?php
$mydb=new connect;
$tbl_name='news';

if(isset($_GET['pageNo'])){
    $page = $_GET['pageNo'];
}else{
    $page = 1;
}

$limit = 3;
$adjacents = 3;
$query = "SELECT COUNT(*) as num FROM $tbl_name where live='Y' order by news_date desc";

	$total_pages = mysql_fetch_array(mysql_query($query));
	$total_pages = $total_pages['num'];

    $targetpage="/en/news.php";

    $page = $_GET['page'];
	if($page)
		$start = ($page - 1) * $limit; 			//first item to display on this page
	else
		$start = 0;

        $sql="SELECT news_id,news_text,news_title,news_title_eng,image,DATE_FORMAT(news_date,'%M %d, %Y') as news_date_formated from $tbl_name where live='Y' order by news_date desc LIMIT $start, $limit";
	$result = mysql_query($sql);
        if ($page == 0)
            $page = 1;
        $prev = $page - 1;
        $next = $page + 1;
        $lastpage = ceil($total_pages/$limit);
        $lpm1 = $lastpage - 1;	


        $pagination = "";
	if($lastpage > 1)
	{	
		$pagination .= "<div class=\"pagination\">";
		//previous button
		if ($page > 1) 
			$pagination.= "<a href=\"$targetpage?page=$prev\">&lt;&lt; previous</a>";
		else
			$pagination.= "<span class=\"disabled\">&lt;&lt; previous</span>";	
		
		//pages	
		if ($lastpage < 7 + ($adjacents * 2))	//not enough pages to bother breaking it up
		{	
			for ($counter = 1; $counter <= $lastpage; $counter++)
			{
				if ($counter == $page)
					$pagination.= "<span class=\"current\">$counter</span>";
				else
					$pagination.= "<a href=\"$targetpage?page=$counter\">$counter</a>";					
			}
		}
		elseif($lastpage > 5 + ($adjacents * 2))	//enough pages to hide some
		{
			//close to beginning; only hide later pages
			if($page < 1 + ($adjacents * 2))		
			{
				for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
				{
					if ($counter == $page)
						$pagination.= "<span class=\"current\">$counter</span>";
					else
						$pagination.= "<a href=\"$targetpage?page=$counter\">$counter</a>";					
				}
				$pagination.= "...";
				$pagination.= "<a href=\"$targetpage?page=$lpm1\">$lpm1</a>";
				$pagination.= "<a href=\"$targetpage?page=$lastpage\">$lastpage</a>";		
			}
			//in middle; hide some front and some back
			elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
			{
				$pagination.= "<a href=\"$targetpage?page=1\">1</a>";
				$pagination.= "<a href=\"$targetpage?page=2\">2</a>";
				$pagination.= "...";
				for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
				{
					if ($counter == $page)
						$pagination.= "<span class=\"current\">$counter</span>";
					else
						$pagination.= "<a href=\"$targetpage?page=$counter\">$counter</a>";					
				}
				$pagination.= "...";
				$pagination.= "<a href=\"$targetpage?page=$lpm1\">$lpm1</a>";
				$pagination.= "<a href=\"$targetpage?page=$lastpage\">$lastpage</a>";		
			}
			//close to end; only hide early pages
			else
			{
				$pagination.= "<a href=\"$targetpage?page=1\">1</a>";
				$pagination.= "<a href=\"$targetpage?page=2\">2</a>";
				$pagination.= "...";
				for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
				{
					if ($counter == $page)
						$pagination.= "<span class=\"current\">$counter</span>";
					else
						$pagination.= "<a href=\"$targetpage?page=$counter\">$counter</a>";					
				}
			}
		}
		
		//next button
		if ($page < $counter - 1) 
			$pagination.= "<a href=\"$targetpage?page=$next\">next &gt;&gt;</a>";
		else
			$pagination.= "<span class=\"disabled\">next &gt;&gt;</span>";
		$pagination.= "</div>\n";		
	}
?>
    <?php

		$news_id_array=array();
		$news_id_link=array();
                if($news_id_selected!='')
                    echo $news_selected;
		while($obj=mysql_fetch_object($result)){		
                    $news_txt='';
                   
                        $news=str_replace("\n","<br/>",$obj->news_text);
                        $news_image=$obj->image;
                        $news_title=$obj->news_title;
                        $news_title_eng=$obj->news_title_eng;
                        $news_date=$obj->news_date_formated;
                        ?>
                        <div class="clear"></div>
                        <div class="slide_wrapper">
                            <div class="slider" id="slider<?php echo $obj->news_id; ?>">
                                <div class="latest_news_date"><?php echo $news_date; ?>
                                </div>

                                <?php
                                    if($news_image!='' && $news_image!=null)
                                        echo '<img src="/en/news_images/'.$news_image.'"  class="latest_news_img" alt="'.$news_title.'"/>';

                                    $news_url="/en/news-detail/".$obj->news_id;
                                    ?>
                                <p><a href="<?php echo $news_url; ?>" class="a_cls1"><?php echo $news_title; ?></a></p>
                                <?php
//                                    $newsTitleHeader='<a href="'.$news_url.'" class="a_cls1">'.$news_title.'</a>';
                                    if(strlen($news) > 200){
                                        $newsText = substr($news,0,200);
                                    }
                                ?>
                                
                                <p><?php echo strip_tags($newsText); ?></p>
                            </div>
                            <div class="slider_menu" id="slider<?php echo $obj->news_id; ?>_menu" ></div>
                        </div>
                        <?php
		}
		echo '<div class="clear"></div>';
		echo $pagination;
		
		
		?>

    <div class="clear"></div>

</div>  

  <!--
  <div style=" float:right; padding-right:20px;"> <a href="/latest-news-rss.php" target="_blank" > <img src="/images/feed.jpg" alt="Rss Feed" title="clicks N hits latest news" /> </a> </div>
  -->
<div class="clear"></div>

