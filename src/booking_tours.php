<script type="text/javascript">
var myDate = new Date();
start_date=(myDate.getMonth() + 1)+ "/"+myDate.getDate()+ "/" + myDate.getFullYear();
end_date=(myDate.getMonth() + 1)+ "/" + myDate.getDate() + "/" + (myDate.getFullYear()+10);

$(document).ready(function () {
        $('#date_visit').simpleDatepicker({ chosendate: start_date, startdate: start_date, enddate: end_date });
        //$('#pub_date').simpleDatepicker({ chosendate: '1/1/1980', startdate: '1/1/1980', enddate: '1/1/2005' });
        $('#date_visit').focus(function() {
        $("#date_visit").click();
        });

            $("#tourForm").validate({
			rules: {
				name: "required",
				email: {
					required: true,
					email: true
				},
				m_number: "required",
				date_visit: "required",
				prop_time: "required"
			}
		});



});
  </script>


<div class="right_panel">

<div class="reg_form">
    <div style="padding: 2px;">
        <h3>استمارة الحجز على الانترنت للمجموعة السياحية</h3>
        <p>
            شكراَ لرغبتكم في زيارة جامع الشيخ زايد الكبير. نحن نتطلع لتفهم احتياجات مجموعتكم لأن نخدمكم بشكل أفضل. يرجى تعبئة استمارة الحجز أدناه. يجب أن يتم كافة الحجوزات قبل سبعة أيام من تاريخ موعد الزيارة المرغوبة. يرجى اتباع التعليمات عن الحجز الموضحة أدناه. سنقوم بإجابتكم بعدما نستلم منكم جميع المعلومات المطلوبة من قبلنا.
يرجى التكرم بالعلم بأننا سوف لا نستطيع القيام بتنسيق الجولات السياحية دون تأكيد طلب الحجز.

        </p>
    </div>

<form method="post" id="tourForm" name="tourForm" enctype="multipart/form-data">

    <input type="hidden" name="agree" value="1" />
<div class="input_row even">
<div class="input_label_div">الاسم </div>
<div class="input_div"> <input type="text" name="name" id="name" class="input_cls"/>  </div>
</div>

<div class="input_row odd">
<div class="input_label_div">رقم الهاتف المتحرك </div>
<div class="input_div"> <input type="text" name="m_number" id="m_number" class="input_cls"/>  </div>
</div>

<div class="input_row even">
<div class="input_label_div">اسم المؤسسة / المجموعة </div>
<div class="input_div"> <input type="text" name="o_g_name" id="o_g_name" class="input_cls"/>  </div>
</div>


<div class="input_row odd">
<div class="input_label_div">تاريخ الزيارة المرغوب </div>
<div class="input_div"> <input type="text" name="date_visit" id="date_visit" class="input_cls"/>  </div>
</div>


    <div class="input_row odd">
        <div class="input_label_div">وقت الزيارة المرغوب
            <br/>
            <span>يرجى ملاحظة ما يلي:</span>
        </div>
        <select name="prop_time" id="prop_time" class="input_cls_2" style="direction:rtl" >
            <option value=""></option>
            <option value="08:00:00">8 AM</option>
            <option value="09:00:00">9 AM</option>
            <option value="10:00:00">10 AM</option>
            <option value="11:00:00">11 AM</option>
            <option value="12:00:00">12 AM</option>
            <option value="13:00:00">1 PM</option>
            <option value="14:00:00">2 PM</option>
            <option value="15:00:00">3 PM</option>
            <option value="16:00:00">4 PM</option>
        </select>
    </div>

<div class="input_row even">
<div class="input_label_div">رقم هاتف المؤسسة
</div>
<div class="input_div">
    <input dir="ltr" type="text"  name="org_number" id="org_number"  class="input_cls"/>
</div>
</div>

<div class="input_row odd">
<div class="input_label_div">حجم المجموعة
</div>
<div class="input_div">
    <input dir="ltr" type="text"  name="grp_size" id="grp_size"  class="input_cls"/>
</div>
</div>

<div class="input_row even">
<div class="input_label_div">البريد الإلكتروني
</div>
<div class="input_div">
    <input dir="ltr" type="text"  name="email" id="email"  class="input_cls"/>
</div>
</div>


<div class="input_row odd">
<div class="input_label_div">معلومات أخرى <br/>
</div>
<div class="input_div">
    <textarea class="input_cls" name="other_info" id="other_info" cols="25" rows="8" ></textarea></div>
</div>




<div class="input_row even">
<div style="padding-right:230px;">
<input type="submit" name="submitTour" id="submitTour" value="أرسل" />
</div>
</div>

</form>

    <div class="clear"></div>
</div>

<div class="clear"></div>
</div>