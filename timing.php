<?php
error_reporting(E_ALL);
//ini_set('display_errors','On');
include 'includes/database.php';
include 'includes/dal/news.php';
include 'includes/dal/pages.php';
include 'includes/functions.php';
include 'includes/config.php';

$current_page = 'index';

$db = new MyDatabase();
$conf = new Configuration();
$site_path = $conf->site_url;


$conf->site_description = 'The board members of the Sheikh Zayed Grand Mosque in Abu Dhabi. Find all about the Sheikh Zayed Grand Mosque in Abu Dhabi including, visiting timings, how to get to the mosque, dress code, tours, history, architecture and more.';

$conf->site_keywords = 'grand mosque, sheikh zyed grand mosque, mosque in adu dhabi, what to wear in mosque, how to get to mosque, history of grand mosque, architecture of grand mosque, grand mosque photos, board members';

$dal_news = new ManageNews();
$dal_pages = new ManagePages(); //for message from minister of presedential affairs
$page = $dal_pages->getPage('', 30); // id must be 119 for message from minister, if different then use that


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sheikh Zayed Grand Mosque in Abu Dhabi</title>
<?php include 'includes/common_header.php'; ?>
</head>

<body>
<?php include 'includes/menus/banner_header3.php'; ?>		
			<!-- Banner Start -->
<div class="banner">
	<img src="<?php echo $site_path;?>images/visiting_the_mosque_banner.jpg">     
</div>
<!-- Banner Close -->	
<!-- Content Start -->
	<div class="main_box_content visiting_page_height">
	 	 <?php include 'includes/menus/left_menu.php'; ?>
        <div class="clear"></div>
        <div class="content">
        	<div class="brad_cram">
            	<ul>
            	   <li><a href="<?php echo $site_path; ?>">الصفحة الرئيسية</a></li>
                    <li><a href="#" class="active">&#1603;&#1604;&#1605;&#1577; &#1587;&#1605;&#1608; &#1575;&#1604;&#1588;&#1610;&#1582; &#1605;&#1606;&#1589;&#1608;&#1585; &#1576;&#1606; &#1586;&#1575;&#1610;&#1583;</a></li>
                </ul>
            </div>
      
      		<div class="content-left">
			 <?php //include 'includes/menus/left_menu1.php'; 
				?>
				
                <br class="clear"/>
                <?php 
				include 'includes/menus/ministry_logos.php'; 
				?>
                <?php include 'includes/menus/rightsidebanner.php';?>
			</div>
	  		<div class="content-right" style="margin-right:30px">
			<div class="middle"> 
     <div class="content_topbox">
	   <h2>&#1603;&#1604;&#1605;&#1577; &#1587;&#1605;&#1608; &#1575;&#1604;&#1588;&#1610;&#1582; &#1605;&#1606;&#1589;&#1608;&#1585; &#1576;&#1606; &#1586;&#1575;&#1610;&#1583;</h2>
	   <div class="content_img"> <img src="<?php echo $site_path;?>images/ministry-officials.jpg"> </div>
	   <?php
	   $short_desc=strip_tags($page->content);
	   $short_desc=preg_replace('/\s+?(\S+)?$/', '', substr($short_desc, 70, 920));
	   $short_desc = strip_tags($short_desc);
	  
	   ?>
	   <div class="content_text" style="text-align:justify"> <?php echo $short_desc .'...'; ?>
	   <div class="read_more"><a href="minister-hh">اقرأ المزيد</a> >> </div>
	   </div>
	 </div> 
	 
	 <div class="clear news_home">
	  <div id="news_home_heading">الأخبار</div>
	  
      <?php
	  	$news_list = $dal_news->getNews();
		foreach($news_list as $news) {
		$news_short=strip_tags($news->news_text);
		$news_short=preg_replace('/\s+?(\S+)?$/', '', substr($news_short, 0, 300));
		$news_short=substr($news_short,0,strripos($news_short,' '));
		//$news_short=UTF8ToHTML($news_short);
		$news_url= $site_path."news-detail/".string_to_filename($news->news_title).'-'.$news->news_id;
	  ?>
      <div class="news_item">
     	<div class="news_content" <?php echo $news->image == '' ? 'style="width: 100%"' : ''; ?>>
        	<span class="news_item_title"><a href="<?php echo $news_url; ?>"><?php echo $news->news_title; ?></a></span>
            <p class="clear" style="text-align:justify;"><?php echo $news_short; ?></p>
            <span class="read_more"><a href="<?php echo $news_url; ?>">اقرأ المزيد >></a></span>        </div>
        <?php if($news->image != '') { ?>
        <div class="image" >
        	<a href="<?php echo $news_url; ?>">
	        	<img src="<?php echo $site_path; ?>news_images/<?php echo $news->image; ?>" height="111" width="111">
            </a>
        </div>
        <?php } ?>
      </div>
      <?php
	  	}
	  ?>
	  <div class="clear"><a href="news-list" class="news_button">أرشيف الأخبار </a> </div>
	 </div>
     
   </div>
			</div>
 <br class="clear" />      
        </div>
    
    </div>
<div id="wrapper" class="wrapper">
	<?php include 'includes/menus/banner_header3.php'; ?>
   <br class="clear" />
   
</div>
<div class="content_bottom">&nbsp;</div>
<?php require 'includes/footer.php'; ?>
</body>
</html>
