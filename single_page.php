<?php
ob_start();
include 'includes/database.php';

include 'includes/dal/pages.php';

include 'includes/functions.php';

include 'includes/config.php';



$slug = explode('/',$_SERVER['REQUEST_URI']);

$slug = end($slug);

//echo $slug;

//print_r($slug);

//die();



$conf = new Configuration();



$db = new MyDatabase();

$dal_pages = new ManagePages();

$page = $dal_pages->getPage($slug);



$site_path = $conf->site_url;



$main_menu = $conf->getCurrentMainPage($slug);

if($page)

	$stat_page= "true";

else

 	$stat_page=  "false";

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include 'includes/common_header.php'; ?>
<title><?php echo $page->title; ?>- Sheikh Zayed Grand Mosque Center</title>
</head>

<body>
<div class="wrapper">
  <div class="header">
    <?php include 'includes/menus/banner_header.php'; ?>
  </div>
  <div class="content">
    <div class="left">
      <?php include 'includes/menus/left_menu.php'; 

				?>
      <br class="clear"/>
      <?php 

				include 'includes/menus/ministry_logos.php'; 

				if(($page->p_id=="175") || ($page->p_id=="174"))

					{

						include 'includes/news_letter.php'; 

					}

				?>
    </div>
    <div class="single_middle">
      <?php 

        include 'includes/menus/marquee.php'; 

        ?>
      <span class="bread_crumb home"><a href="<?php echo $site_path; ?>">Home</a></span>&nbsp;&gt;
      <?php if($main_menu == '') { ?>
      <span class="bread_crumb bread_crumb_sub"><?php echo $page->title; ?></span>
      <?php } else { ?>
      <span class="bread_crumb home"><a href="<?php echo $site_path.$main_menu; ?>"><?php echo ucfirst($main_menu); ?></a></span>&nbsp;&gt; <span class="bread_crumb bread_crumb_sub"><?php echo $page->title; ?></span>
      <?php } ?>
      <?php

                    $news_url= $site_path."news-detail/".string_to_filename($page->title).'-'.$page->p_id;

                    ?>
                    <div class="clear"></div>
      <div class="page_item_single">
        <div class="page_content" >
          <div class="general_body_content"><?php echo $page->content; ?></div>
          <br class="clear" />
        </div>
      </div>
      <div class="clear bottom_line">&nbsp;</div>
    </div>
    <br class="clear" />
  </div>
</div>
<div class="content_bottom height_20px"></div>
<?php include 'includes/footer.php'; ?>
</body>
</html>