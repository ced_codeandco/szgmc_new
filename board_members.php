<?php
include 'includes/database.php';
include 'includes/functions.php';
include 'includes/config.php';

$slug = explode('/',$_SERVER['REQUEST_URI']);
$slug = end($slug);
$conf = new Configuration();
$db = new MyDatabase();

$site_path = $conf->site_url;

$main_menu = $conf->getCurrentMainPage($slug);

$conf->site_description = 'Find all about the Sheikh Zayed Grand Mosque in Abu Dhabi including, visiting timings, how to get to the mosque, dress code, tours, history, architecture and more.';

$conf->site_keywords = 'grand mosque, sheikh zyed grand mosque, mosque in adu dhabi, what to wear in mosque, how to get to mosque, history of grand mosque, architecture of grand mosque, grand mosque photos';

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sheikh Zayed Grand Mosque in Abu Dhabi</title>
<?php include 'includes/common_header.php'; ?>
</head>

<body>
<?php include 'includes/menus/banner_header3.php'; ?>		
			<!-- Banner Start -->
<div class="banner">
	<img src="<?php echo $site_path;?>images/visiting_the_mosque_banner.jpg">     
</div>
<!-- Banner Close -->	
<!-- Content Start -->
	<div class="main_box_content">
	 	 <?php include 'includes/menus/left_menu.php'; ?>
        <div class="clear"></div>
        <div class="content">
        	<div class="brad_cram">
            	<ul>
            	   <li><a href="<?php echo $site_path; ?>">الصفحة الرئيسية</a></li>
                    <li><a href="#" class="active"><?php if($main_menu == '') { ?>
                    <?php echo $page->title; ?>
                    <?php } else { ?>
                   أعضاء مجلس الأمناء
					 <?php } ?></a></li>
                </ul>
            </div>
      
      		<div class="content-left">
			 <?php //include 'includes/menus/left_menu1.php'; 
				?>
				
                <br class="clear"/>
                <?php 
				include 'includes/menus/ministry_logos.php'; 
				?>
                <?php include 'includes/menus/rightsidebanner.php';?>
			</div>
	  		<div class="content-right" style="margin-right:30px">
			<div class="single_middle">
		        <h2><strong>أعضاء مجلس الأمناء</strong></h2>
                <div class="board_member_item">
                	<img src="<?php echo $site_path;?>images/Ahmed-juma-Al-Zaabi.jpg" /><span class="name">معالي /  أحمد جمعة الزعابي</span><span class="designation">(رئيس مجلس الأمناء)</span>                </div>
<div class="board_member_item">
                	<img src="<?php echo $site_path;?>images/Sultan-Dahi-Al-Hamiri.jpg" /><span class="name">سعادة/ سلطان ضاحي الحميري</span><span class="designation">(نائب رئيس مجلس الأمناء)</span>                </div>
                <div class="board_member_item">
                	<img src="<?php echo $site_path;?>images/Mohamad-Matar.jpg" /><span class="name">سعادة/ د. محمد مطر الكعبي</span><span class="designation">(عضواً)</span>                </div>
                <div class="board_member_item">
                	<img src="<?php echo $site_path;?>images/Bilal-Al-Baddur.jpg" /><span class="name">سعادة / بلال ربيع البدور</span><span class="designation">(عضواً)</span>                </div>
                <div class="board_member_item">
                	<img src="<?php echo $site_path;?>images/Ali-Ghanem-Murshid-AL-Ramithi.jpg" /><span class="name">السيد /علي غانم مرشد الرميثي</span><span class="designation">(عضواً)</span>                </div>
                <div class="board_member_item">
                	<img src="<?php echo $site_path;?>images/Mohamad-Hassan-Mohamad-al-muini.jpg" /><span class="name">السيد/ محمد حسن  المعيني</span><span class="designation">(عضواً)</span>                </div>
                
     </div>
			</div>
 <br class="clear" />      
        </div>
    
    </div>



<div class="content_bottom">&nbsp;</div>
<?php require 'includes/footer.php'; ?>
</body>
</html>
