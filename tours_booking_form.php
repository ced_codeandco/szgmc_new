<?php
session_start();
include './en/includes/database.php';
include './includes/functions.php';
include './includes/config.php';
include_once('./includes/generic_functions.php');
$conf = new Configuration();


$db = new MyDatabase();
$site_path = $conf->site_url;
$slug = explode('/',$_SERVER['REQUEST_URI']);
$slug = end($slug);
if(isset($_POST['submitTour']) && isset($_POST['agree']) && $_POST['agree']=='1'){
	if(!isset($_POST['prop_time']) | $_POST['prop_time'] == ''){
		
		?>
			<script type="text/javascript">
			alert('Error in updating.Time was not selected');
			</script>
		<?php
	}
	else if(!isset($_POST['grp_size3']) | $_POST['grp_size3'] == ''){
		
		?>
			<script type="text/javascript">
			alert('Error in updating.Group size was empty');
			</script>
		<?php
	}
	else{
		$id = $_POST['edit'];
		$time = $_POST['prop_time'];
		$size = $_POST['grp_size3'];
		$date_visit= trim($_POST['date_visit']);
		$date_visit = explode("/",$date_visit);
		$date_visit1 = $date_visit[2]."-".$date_visit[1]."-".$date_visit[0];
		$sql_update="update tours set `purposed_date`='$date_visit1',`time`='$time',`size`='$size',`status`='N',`approved`='',`mdate`=now() where id='$id' ";
		mysql_query($sql_update);
		header("location:bookings.php?type=edit");
	}
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="Language" content="arabic" />

<meta http-equiv="content-language" content="ar"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
    <?php include 'includes/common_header.php'; ?>
    <title>الحجوزات السياحية</title>
    <link href="<?php echo $site_path; ?>css/jquery-ui-1.8.24.custom.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="<?php echo $site_path; ?>js/jquery.validate.js"></script>
	<script type="text/javascript" src="<?php echo $site_path; ?>js/main.js"></script>
	<script type="text/javascript" src="<?php echo $site_path; ?>js/jquery-ui-1.8.24.custom.min.js"></script>
    
<script language="javascript">
       var main_menu = '<?php echo (isset($main_menu) && $main_menu != '')? $main_menu : ''; ?>';
		var current_tab = '<?php echo (isset($slug) && $slug != '')? $slug : ''; ?>';
		function changeClasses() {
			var even = true;
			$('#tourForm div.input_row:visible').each(function(index) {
				$(this).removeClass('odd');
				$(this).removeClass('even');
				if(even) {
					$(this).addClass('even');
				} else {
					$(this).addClass('odd');
				}
				even = !even;
			});
			var n =	$('#tourForm div.input_row:visible').length;
			if(n%2==0){
				var index = $('#tourForm div.input_row:visible').index();
				$("#tourForm div.input_row:visible:eq("+ index +")").css("border-bottom", "1px solid #ccc");
			}
	   }
</script> 
<script type="text/javascript">

		$(document).ready(function () {
			var unavailableDates = [<?php echo $disable_dates_str; ?>];

			function unavailable(date) {
			  dmy = date.getDate() + "-" + (date.getMonth()+1) + "-" + date.getFullYear();
			  if ($.inArray(dmy, unavailableDates) < 0) {
				return [true,"","Book Now"];
			  } else {
				return [false,"","Cant book on this date"];
			  }
			}

			$('#submitTour').click(function(){
				if($('#prop_time').val() == ''){
					
					alert('Please select a time');
					return false;
				}
				if($('#grp_size3').val() < 1){
					
					alert('Can\'t book for less than 1 people');
					return false;
				}
				if($('#grp_size3').val() > 50){
					
					alert('Can\'t book for more than 50 people');
					return false;
				}
	
			});
		
		
			$( "#date_visit" ).datepicker({
				//minDate: new Date(<?php echo date('Y').', '.date('m').' -1, '.date('d'); ?>),
				<?php
				$hour = date('H');
				if($hour >= 15) {
				?>
				minDate: new Date(<?php echo date('Y'); ?>, <?php echo date('m'); ?> - 1 , <?php echo date('d'); ?> + 2),
				<?php } else {?>
				minDate: new Date(<?php echo date('Y'); ?>, <?php echo date('m'); ?> - 1 , <?php echo date('d'); ?> + 1),
				<?php } ?>
				dateFormat: "dd/mm/yy",
				beforeShowDay: unavailable,
				onSelect: function(dateText, inst) {
					//$('.fri').hide();
					//$('.other').hide();
					var date = $(this).datepicker('getDate');
				    var dayOfWeek = date.getUTCDay();
					
					$('div.time_slot_availability').html('<img src="/images/ajax-loader.gif" />');
					
						var condition = false;
					
						$.getJSON('/forms/visit_available_slots.php?date='+$('#date_visit').val()+'&burst='+Math.floor(Math.random() * 1000), function(data) {

							
							$('#prop_time').html('');
							
							if(data.week_day!=5)
							{
							if($('#type').val() && ($('#type').val() == 'Photo Stop' || $('#type').val() == 'وقفة تصوير ')) {
								data.available_times.unshift('08:30:00');
								data.available_times.unshift('08:00:00');
								
							}
							}
							
							data.available_times.unshift('');
						
							$.each(data.available_times, function(key, i) {
								$('#prop_time').html($('#prop_time').html() + '<option value="'+ i + '">' + i + '</option>');
							});
													
							$('div.time_slot_availability').html('');
							
											
							
							});
					
					if(dayOfWeek == 4) {
					}
					else {
					}
				}
			});
			
		$('#type').change(function() {
			if($('#date_visit').val()!="")
			{
				$.getJSON('/forms/visit_available_slots.php?date='+$('#date_visit').val()+'&burst='+Math.floor(Math.random() * 1000), function(data) {

							
							$('#prop_time').html('');
							
							if(data.week_day!=5)
							{
								
							if($('#type').val() && ($('#type').val() == 'Photo Stop' || $('#type').val() == 'وقفة تصوير ')) {
								data.available_times.unshift('08:30:00');
								data.available_times.unshift('08:00:00');
								
							}
							}
							
							data.available_times.unshift('');
						
							$.each(data.available_times, function(key, i) {
								$('#prop_time').html($('#prop_time').html() + '<option value="'+ i + '">' + i + '</option>');
							});
													
							$('div.time_slot_availability').html('');
							
											
							
							});
			}
				});
			$('#grp_size').blur(function() {
				$('div.group_slot_availability').html('<img src="/images/ajax-loader.gif" />');
				if($('#grp_size').val()>=8)
				{
				$.getJSON('/forms/visit_available_slots.php?validity=true&group_size='+$('#grp_size').val()+'&date='+$('#date_visit').val()+'&time='+$('#prop_time').val()+'&burst='+Math.floor(Math.random() * 1000), function(data) {
					
						//$('div.group_slot_availability').html('<img src="/images/tick-icon.png" width="16" height="16" />');	
					$('div.group_slot_availability').html('');
					if(data.status == 0) {
						//$('div.group_slot_availability').html('<img src="/images/Cross-icon.png" width="16" height="16" />');
						$( "#slot_dialog" ).html('<p>Only '+data.available_slots+' people can visit at this time. Please select a different time or different group size');						
						$( "#slot_dialog" ).dialog();
						
					}
						
				});
				}
				else
				{
					$('div.group_slot_availability').html('');
					$( "#slot_dialog" ).html('<p>Cannot book with less than 8 people ');						
						$( "#slot_dialog" ).dialog();
				}
			});
			$('#grp_size1').blur(function() {
				$('div.group_slot_availability1').html('<img src="/images/ajax-loader.gif" />');
				
				$.getJSON('/forms/visit_available_slots.php?validity=true&group_size='+$('#grp_size1').val()+'&date='+$('#date_visit').val()+'&time='+$('#prop_time').val()+'&burst='+Math.floor(Math.random() * 1000), function(data) {
					
						//$('div.group_slot_availability').html('<img src="/images/tick-icon.png" width="16" height="16" />');	
					$('div.group_slot_availability1').html('');
					if(data.status == 0) {
						//$('div.group_slot_availability').html('<img src="/images/Cross-icon.png" width="16" height="16" />');
						$( "#slot_dialog" ).html('<p>Only '+data.available_slots+' people can visit at this time. Please select a different time or different group size');						
						$( "#slot_dialog" ).dialog();
						
					}
						
				});
				
			});
			$('#grp_size2').blur(function() {
				$('div.group_slot_availability1').html('<img src="/images/ajax-loader.gif" />');
				
				$.getJSON('/forms/visit_available_slots.php?validity=true&group_size='+$('#grp_size2').val()+'&date='+$('#date_visit').val()+'&time='+$('#prop_time').val()+'&burst='+Math.floor(Math.random() * 1000), function(data) {
					
						//$('div.group_slot_availability').html('<img src="/images/tick-icon.png" width="16" height="16" />');	
					$('div.group_slot_availability1').html('');
					if(data.status == 0) {
						//$('div.group_slot_availability').html('<img src="/images/Cross-icon.png" width="16" height="16" />');
						$( "#slot_dialog" ).html('<p>Only '+data.available_slots+' people can visit at this time. Please select a different time or different group size');						
						$( "#slot_dialog" ).dialog();
						
					}
						
				});
				
			});
			$('div#slot_dialog').bind('dialogclose', function(event) {
     			//$( "#grp_size" ).focus();
			 });
			
			
		
		var default_classes = new Array();
		
		jQuery.validator.addMethod("greaterThanEight", function(value, element) {
   
		if(parseFloat(value) >= 8)
		{
			var sta=null;
			$.ajaxSetup( { "async": false } );
			$.getJSON('/forms/visit_available_slots.php?validity=true&group_size='+value+'&date='+$('#date_visit').val()+'&time='+$('#prop_time').val()+'&burst='+Math.floor(Math.random() * 1000), function(data) {
				
					//$('div.group_slot_availability').html('<img src="/images/tick-icon.png" width="16" height="16" />');	
				$('div.group_slot_availability').html('');
				
				if(data.status == 0) {
					sta =  false;
				} 
				else
				{
					sta =  true;
				}	
			});
		}
		else
		{
			$('div.group_slot_availability').html('');
			$( "#slot_dialog" ).html('<p>Cannot book with less than 8 people ');						
			$( "#slot_dialog" ).dialog();
			return false;
		}
		$.ajaxSetup( { "async": true } );
		return sta;
	
	}, "");
		
		jQuery.validator.addMethod("greaterThanEight1", function(value, element) {
   
		
			var sta=null;
			$.ajaxSetup( { "async": false } );
			$.getJSON('/forms/visit_available_slots.php?validity=true&group_size='+value+'&date='+$('#date_visit').val()+'&time='+$('#prop_time').val()+'&burst='+Math.floor(Math.random() * 1000), function(data) {
				
					//$('div.group_slot_availability').html('<img src="/images/tick-icon.png" width="16" height="16" />');	
				$('div.group_slot_availability').html('');
				
				if(data.status == 0) {
					sta =  false;
				} 
				else
				{
					sta =  true;
				}	
			});
		
		$.ajaxSetup( { "async": true } );
		return sta;
	
	}, "");
	
		jQuery.validator.addMethod("delegate", function(value, element) { 
  			return value != 'Ambassador/M. Ahmad سفير/محمد أحمد'; 
		}, "*");
		
		$('.m_number').keydown(function (e) {
    if (e.ctrlKey || e.altKey) { // if shift, ctrl or alt keys held down
        e.preventDefault();         // Prevent character input
    } else {
        var n = e.keyCode;
		
        if (!((n == 8)             
        || (n == 46)               
		|| (n == 219)               
		|| (n == 221)               
		|| (n == 189)               
		|| (n == 107)
		|| (n == 173)                
		|| (n == 109)                
		|| (n == 32)                		
        || (n >= 35 && n <= 40)    
        || (n >= 48 && n <= 57)    
        || (n >= 96 && n <= 105))  
        ) {
			
            e.preventDefault();     // Prevent character input
        }
    }
});
			$('#countryCode').keydown(function (e) {
    if (e.ctrlKey || e.altKey) { // if shift, ctrl or alt keys held down
        e.preventDefault();         // Prevent character input
    } else {
        var n = e.keyCode;
		
        if (!((n == 8)             
        || (n == 46)               
		|| (n == 219)               
		|| (n == 221)               
		|| (n == 189)               
		|| (n == 107)
		|| (n == 173)                
		|| (n == 109)                
		|| (n == 32)                		
        || (n >= 35 && n <= 40)    
        || (n >= 48 && n <= 57)    
        || (n >= 96 && n <= 105))  
        ) {
			
            e.preventDefault();     // Prevent character input
        }
    }
});
$('#gov_phone').keydown(function (e) {
    if (e.ctrlKey || e.altKey) { // if shift, ctrl or alt keys held down
        e.preventDefault();         // Prevent character input
    } else {
        var n = e.keyCode;
		
        if (!((n == 8)             
        || (n == 46)               
		|| (n == 219)               
		|| (n == 221)               
		|| (n == 189)               
		|| (n == 107)
		|| (n == 173)                
		|| (n == 109)                
		|| (n == 32)                		
        || (n >= 35 && n <= 40)    
        || (n >= 48 && n <= 57)    
        || (n >= 96 && n <= 105))  
        ) {
			
            e.preventDefault();     // Prevent character input
        }
    }
});
$('#embassy_phone').keydown(function (e) {
    if (e.ctrlKey || e.altKey) { // if shift, ctrl or alt keys held down
        e.preventDefault();         // Prevent character input
    } else {
        var n = e.keyCode;
		
        if (!((n == 8)             
        || (n == 46)               
		|| (n == 219)               
		|| (n == 221)               
		|| (n == 189)               
		|| (n == 107)
		|| (n == 173)                
		|| (n == 109)                
		|| (n == 32)                		
        || (n >= 35 && n <= 40)    
        || (n >= 48 && n <= 57)    
        || (n >= 96 && n <= 105))  
        ) {
			
            e.preventDefault();     // Prevent character input
        }
    }
});
$('#operator_phone').keydown(function (e) {
    if (e.ctrlKey || e.altKey) { // if shift, ctrl or alt keys held down
        e.preventDefault();         // Prevent character input
    } else {
        var n = e.keyCode;
		
        if (!((n == 8)             
        || (n == 46)               
		|| (n == 219)               
		|| (n == 221)               
		|| (n == 189)               
		|| (n == 107)
		|| (n == 173)                
		|| (n == 109)                
		|| (n == 32)                		
        || (n >= 35 && n <= 40)    
        || (n >= 48 && n <= 57)    
        || (n >= 96 && n <= 105))  
        ) {
			
            e.preventDefault();     // Prevent character input
        }
    }
});
$('#guide_mobile').keydown(function (e) {
    if (e.ctrlKey || e.altKey) { // if shift, ctrl or alt keys held down
        e.preventDefault();         // Prevent character input
    } else {
        var n = e.keyCode;
		
        if (!((n == 8)             
        || (n == 46)               
		|| (n == 219)               
		|| (n == 221)               
		|| (n == 189)               
		|| (n == 107)
		|| (n == 173)                
		|| (n == 109)                
		|| (n == 32)                		
        || (n >= 35 && n <= 40)    
        || (n >= 48 && n <= 57)    
        || (n >= 96 && n <= 105))  
        ) {
			
            e.preventDefault();     // Prevent character input
        }
    }
});
$('#guide_mobile_code').keydown(function (e) {
    if (e.ctrlKey || e.altKey) { // if shift, ctrl or alt keys held down
        e.preventDefault();         // Prevent character input
    } else {
        var n = e.keyCode;
		
        if (!((n == 8)             
        || (n == 46)               
		|| (n == 219)               
		|| (n == 221)               
		|| (n == 189)               
		|| (n == 107)
		|| (n == 173)                
		|| (n == 109)                
		|| (n == 32)                		
        || (n >= 35 && n <= 40)    
        || (n >= 48 && n <= 57)    
        || (n >= 96 && n <= 105))  
        ) {
			
            e.preventDefault();     // Prevent character input
        }
    }
});
$('#org_phone').keydown(function (e) {
    if (e.ctrlKey || e.altKey) { // if shift, ctrl or alt keys held down
        e.preventDefault();         // Prevent character input
    } else {
        var n = e.keyCode;
		
        if (!((n == 8)             
        || (n == 46)               
		|| (n == 219)               
		|| (n == 221)               
		|| (n == 189)               
		|| (n == 107)
		|| (n == 173)                
		|| (n == 109)                
		|| (n == 32)                		
        || (n >= 35 && n <= 40)    
        || (n >= 48 && n <= 57)    
        || (n >= 96 && n <= 105))  
        ) {
			
            e.preventDefault();     // Prevent character input
        }
    }
});
$('#hotel_phone').keydown(function (e) {
    if (e.ctrlKey || e.altKey) { // if shift, ctrl or alt keys held down
        e.preventDefault();         // Prevent character input
    } else {
        var n = e.keyCode;
		
        if (!((n == 8)             
        || (n == 46)               
		|| (n == 219)               
		|| (n == 221)               
		|| (n == 189)               
		|| (n == 107)
		|| (n == 173)                
		|| (n == 109)                
		|| (n == 32)                		
        || (n >= 35 && n <= 40)    
        || (n >= 48 && n <= 57)    
        || (n >= 96 && n <= 105))  
        ) {
			
            e.preventDefault();     // Prevent character input
        }
    }
});
		$("#tourForm").validate({
			rules: {
				name: "required",
				contact_person_group:"required",
				group_category:"required",
				grp_size:"required greaterThanEight",
				grp_size1:"required greaterThanEight1",
				grp_size2:"required greaterThanEight1",
				country:"required",
				language:"required",
				gov_delegates:"required",
				gov_name:"required",
				gov_phone:"required",
				embassy_phone: "required",
				'delegates[]': "required delegate",
				'gov_delegates[]': "required delegate",
				company_name: "required",
				operator_phone: "required",
				purpose: "required",
				type: "required",
				guide_name: "required",
				guide_mobile: "required",
				guide_mobile_code: "required",
				prop_time: "required",
				bus: "required",
				grade: "required",
				org_name: "required",
				org_phone: "required",
				gender: "required",
				hotel_name: "required",
				hotel_phone: "required",
				email: {
					required: true,
					email: true
				},
				
				m_number: "required",
				countryCode: "required",
				captcha: "required",
				emirate: "required",
				date_visit: "required",
				prop_time: "required"
				
			}
		});

});

function check_additional_fileds(val)
{

			
if(val!=" ")
{
$(document).ready(function () {



if((val=='Education') || (val=='Educationarb'))
{
$('.edu').show();
$('.country').hide();
$('.emirate').hide();
changeClasses();
}
else
{
$('.edu').hide();
$('.country').show();
changeClasses();
}



if((val=='Hotel') || (val=='Hotelarb'))
{
$('.hotel').show();
changeClasses();
}
else
{
$('.hotel').hide();
changeClasses();
}
if((val=='Embassy') || (val=='Embassyarb'))
{
$('.emb').show();
$('.gov').hide();
$('.grp_size1').show();
$('.grp_size').hide();
$('.grp_size2').hide();
changeClasses();
}
else if((val=='Government') || (val=='Governmentarb') )
{
$('.gov').show();
$('.emb').hide();
$('.grp_size2').show();
$('.grp_size').hide();
$('.grp_size1').hide();
changeClasses();
}
else if((val=='Corporate') || (val=='Corporatearb'))
{
$('.gov').show();
$('.emb').hide();
$('.grp_size2').show();
$('.grp_size').hide();
$('.grp_size1').hide();
changeClasses();
}
else 
{
$('.emb').hide();
$('.gov').hide();
$('.grp_size1').hide();
$('.grp_size2').hide();
$('.grp_size').show();
changeClasses();
}
if((val=='Tour Operator') || (val=='Tour Operatorarb'))
{
$('.language').hide();
$('.contact_person_group').hide();
$('.m_number').hide();
$('.tour_operator_div').show();
changeClasses();
}
else
{
$('.tour_operator_div').hide();
$('.language').show();
$('.contact_person_group').show();
$('.m_number').show();
changeClasses();
}

});
}
}
function changeClasses() {
		var even = true;
		$('#tourForm div.input_row:visible').each(function(index) {
			$(this).removeClass('odd');
			$(this).removeClass('even');
			if(even) {
    			$(this).addClass('even');
			} else {
				$(this).addClass('odd');
			}
			even = !even;
		});
	}
	
	function check_additional_fileds1(val)
{
if(val!=" ")
{
$(document).ready(function () {



if((val=='United Arab Emirates') || (val=='United Arab Emiratesarb'))
{
$('.emirate').show();
changeClasses();
}
else
{
$('.emirate').hide();
changeClasses();
}



});
}
}
  </script>
<style>
#dataTable{ margin-left:0;}
#dataTable1{ margin-left:0;}


div.ui-dialog {
width: 500px;
height:100px;
}


</style>
</head>
<body onLoad="changeClasses();">
<?php include 'includes/menus/banner_header3.php'; ?>		
			<!-- Banner Start -->
<div class="banner">
	<img src="<?php echo $site_path;?>images/visiting_the_mosque_banner.jpg">     
</div>
<!-- Banner Close -->	
<!-- Content Start -->
	<div class="main_box_content">
	 	 <?php include 'includes/menus/left_menu.php'; ?>
        <div class="clear"></div>
        <div class="content">
        	<div class="brad_cram">
            	<ul>
            	   <li><a href="<?php echo $site_path; ?>">الصفحة الرئيسية</a></li>
                    <li><a href="#" class="active">الحجوزات السياحية</a></li>
                </ul>
            </div>
      
      		<div class="content-left">
			 <?php //include 'includes/menus/left_menu1.php'; 
				?>
				
                <br class="clear"/></div>
                <?php 
				include 'includes/menus/ministry_logos.php'; 
				?>
            <?php include 'includes/menus/rightsidebanner.php';?>
            </div>
	  		<div class="content-right" style="margin-right:30px">
			<div class="single_middle">
		            <p class="heading"> الحجوزات السياحية</p>
<div class="page_item_single">
                    	<div class="page_content" >
                            <div class="general_body_content">
							 <div class="reg_form">
  

<div id="slot_dialog" title="العدد المسموح للزيارة"></div>
<?php //include './forms/tours_booking_form.php'; ?>

    <div class="clear"></div>
</div>
							</div>
                            <br class="clear" />
                            
                        </div>

                    </div> 
                   
            </div>
			</div>

        </div>
    
   <div class="clear"></div>
	</div>


<?php include 'includes/footer.php'; ?>

</body>
</html>
