<?php
include './includes/database.php';
include './includes/functions.php';
include './includes/config.php';
include_once('./includes/generic_functions.php');
$conf = new Configuration();
$db = new MyDatabase();
$site_path = $conf->site_url;

$slug = explode('/',$_SERVER['REQUEST_URI']);
$slug = end($slug);

$enq_mail_from='info@szgmc.ae';
$enq_mail_to='manikandansambu@gmail.com';
$enq_mail_cc= 'rashi@timegroup.ae';
$enq_mail_cc1= 'it@szgmc.ae ';
$enq_mail_cc2= 'ab.almarzooqi@szgmc.ae';

define("ENQ_FROM",$enq_mail_from);
define("ENQ_TO",$enq_mail_to);
define("ENQ_CC",$enq_mail_cc);
$msg="";
$current_page = 'ftplogin';
 
if(isset($_POST['a_name']) && $_POST['a_name']!='' ){

    foreach($_POST as $name => $value) {
        $$name=mysql_real_escape_string($value);
    }
	$fn = "./regid.txt";
	$hits = file($fn);
    $hits[0] ++;
	
	
	$fp = fopen($fn , "w");
fputs($fp , "$hits[0]");
fclose($fp);  
$ref_id=str_pad($hits[0],3,"0",STR_PAD_LEFT); 

    $msg='success';
    $file_flag=0;
    
       
    
    if($msg == 'success'){

		$fromName='Quran Recitation';
        $fromEmail=ENQ_FROM;
		$toName='SZGMC';
		$toEmail=ENQ_TO;
        $subject = "Quran Recitation";
        $content = getregistationContent($_POST,$ref_id);		
        
		$email_sql="select email,name from service_email where frm_id='63'";
			  $email_querry= mysql_query($email_sql);
			  while($email_res=mysql_fetch_object($email_querry))
			  {
				  $toName = $email_res->name;
				  $toEmail = $email_res->email;	
         sendEmail($toName, $fromName, $toEmail, $fromEmail, $subject, $content);
			  }

    }

}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>استمارة التسجيل في دورة تجويد القرآن</title>
<link href="<?php echo $site_path; ?>css/calendar.css" rel="stylesheet" type="text/css" />
<?php include 'includes/common_header.php'; ?>
<script type="text/javascript" src="<?php echo $site_path; ?>js/jquery.validate.js"></script>
<link href="<?php echo $site_path; ?>css/jquery-ui-1.8.24.custom.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo $site_path; ?>js/jquery-ui-1.8.24.custom.min.js"></script>

<script type="text/javascript">

  $(document).ready(function(){
    $("#mediaForm").validate({
			rules: {
				a_name: "required",
				a_age: "required",
				a_gender: "required",
				a_nationality: "required",
				a_city: "required",
				a_email: {
					required: true,
					email: true
				},	
				a_telephone: "required",
				desc: "required"
	  								
			}
		});
  });
  </script>

<style>
.heading{float:none;padding-right:0;text-align:right}
#table caption{margin-bottom:10px;text-align:right}
#table table{margin:20px 0 10px;width:100%}
</style>
</head>

<body>
<?php include 'includes/menus/banner_header3.php'; ?>		
			<!-- Banner Start -->
<div class="banner">
	<img src="<?php echo $site_path;?>images/visiting_the_mosque_banner.jpg">     
</div>
<!-- Banner Close -->	
<!-- Content Start -->
	<div class="main_box_content visiting_page_height">
	 	 <?php include 'includes/menus/left_menu.php'; ?>
        <div class="clear"></div>
        <div class="content">
        	<div class="brad_cram">
            	<ul>
            	   <li><a href="<?php echo $site_path; ?>">الصفحة الرئيسية</a></li>
                    <li><a href="#" class="active">استمارة التسجيل في دورة تجويد القرآن</a></li>
                </ul>
            </div>
      
      		<div class="content-left">
			 <?php //include 'includes/menus/left_menu1.php'; 
				?>
				
                <br class="clear"/>
                <?php 
				include 'includes/menus/ministry_logos.php'; 
				?>
                <?php include 'includes/menus/rightsidebanner.php';?>
			</div>
	  		<div class="content-right" style="margin-right:30px">
			<div class="single_middle">
		            <p class="heading"><span dir="rtl">استمارة التسجيل في دورة تجويد القرآن</span></p>
					
					<div id="table">
						<table>
							<caption>انطلاق دورة تحفيظ وتجويد القرآن الكريم لليافعين (ذكور وإناث)، حيث يتم في الدورة تحفيظ القرآن وتعليم أحكام التجويد،<br/>وسوف يتم تحديد مستوى الطالب وذلك من قبل لجنة إدارية مختصه من هيئة الأوقاف وذلك لضمان تقسيم المستويات بشكل صحيح</caption>
							<tr>
								<th>الفئة</th>
								<th>الأيام</th>
								<th>التوقيت</th>
							</tr>
							<tr>
								<td>الذكور</td>
								<td>الأحد و الثلاثاء</td>
								<td>6:30 إلى 8:30</td>
							</tr>
							<tr>
								<td>الإناث</td>
								<td>الاثنين والأربعاء</td>
								<td>4:15 إلى 6:15</td>
							</tr>
						</table>
					</div>
                    <div class="page_item_single">
                    	<div class="page_content" >
                            <div class="general_body_content">
							 <div class="reg_form">
                              <p><span dir="rtl">يرجى تعبئة النموذج أدناه ، سيتم التواصل معكم خلال يومان عمل.</span></p>
    							<?php include './forms/registration.php'; ?>

    <div class="clear"></div>
</div>
							</div>
                            <br class="clear" />
                            
                        </div>
                    </div> 
                   
            </div>
			</div>
 <br class="clear" />      
        </div>
    
    </div>

<div class="content_bottom">&nbsp;</div>
<?php require 'includes/footer.php'; ?>
<?php
	
	   if($msg == 'success'){
	  
		 ?>
		<script>
			$(function() {
				$( "#dialog" ).dialog();
			});
		</script>
        <script type="text/javascript">
		$(function(){
			$('#confirm').click(function() {
				//$('#dialog').hide();
				window.location = "/quran_recitation_course";
				$( "#dialog" ).dialog( "close" );
				
				return false;
				
			});
		});
		
        </script>
        <div id="dialog">
        	<p style="text-align:right;">لقد تم تقديم طلبك بنجاح ، وسوف نقوم بالتواصل معك قريبا </p>
            <p style="direction:ltr;">Your request has been submitted successfully. We will contact you shortly.</p>
            <p style=""><input type="button" value="OK" style="float:right;width:40px" id="confirm"></p>
        </div>
        
        <?php
		 
		}
		else if($msg == 'failed')
		{
		?>
		  <script> alert("Cannot process your request"); </script>
		 <?
		 print('<script type="text/javascript">window.location = "./quran_recitation_course";</script>');
exit(); 
		}
	  
	?>
</body>
</html>
