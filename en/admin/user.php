<?php
include_once ('includes/commons.php');
do_header();

?>	
		
		


	<table width="98%" border="0" align="center">
       
  		<tr>
   		 <td align="center" valign="top">
		 

<script type="text/javascript" charset="utf-8">			
		var oTable;
				$(document).ready(function() {
				oTable=$('#my_table').dataTable( {
					"bProcessing": true,					
					"bJQueryUI": true,
					"sPaginationType": "full_numbers",
					"iDisplayLength": 10,
					"bServerSide": true,					 
					/*"sAjaxSource": 'src/common/json/json_source_customers.php',*/
					"sAjaxSource": 'src/common/json/server_side_json_user.php',
					"aaSorting": [[0,'desc']],
					"aoColumns": [						
						{ bSortable: true, sWidth: '20px' },
						{ bSortable: true, sWidth: '50px' },						
						{ bSortable: true, sWidth: '50px' },						
						{ bSortable: false, sWidth: '30px' },
					]
					
				} );
				
			} );			
			
		</script>


<div id="container">
<table cellpadding="0" cellspacing="0" border="0" class="display" id="my_table">
	<thead>
		<tr>
			<th>Name</th>
			<th>Username</th>
			<th>Created Date</th>
            <th>Action</th>
						
		</tr>
	</thead>
	<tbody>
	</tbody>
</table>	
</div>

</td>
		 </tr>
	  </table>
		
		
		</td>
      </tr>
    </table>
	</td>
  </tr>
  

<?php

do_footer();
?>
