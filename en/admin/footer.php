<?php
include_once ('includes/commons.php');
$request_url=$_SERVER['REQUEST_URI'];

if(isset($_POST['new_menu_name']) && trim($_POST['new_menu_name'])!='' && $_REQUEST['action']=='addfootermenu' )
{
$m_name=mysql_real_escape_string(trim($_POST['new_menu_name']));
$m_title=mysql_real_escape_string(trim($_POST['new_menu_title']));
$m_page=mysql_real_escape_string(trim($_POST['new_menu_page']));
$m_visibility=mysql_real_escape_string(trim($_POST['new_menu_visibility']));
$m_live=mysql_real_escape_string(trim($_POST['new_menu_live']));
$sql="insert into footer_nav(f_name,title,p_id,visibility,live)values('$m_name','$m_title','$m_page','$m_visibility','$m_live')";
$rs=$mydb->query($sql);
header("location: ".$request_url);
exit();
}

if(isset($_POST['new_menu_name']) && trim($_POST['new_menu_name'])!='' && isset($_REQUEST['action']) && trim($_REQUEST['action'])=='editfootermenu' && isset($_REQUEST['menu_id']) && $_REQUEST['menu_id']!='' )
{
$edit_m_id=mysql_real_escape_string(trim($_REQUEST['menu_id']));
$m_name=mysql_real_escape_string(trim($_POST['new_menu_name']));
$m_title=mysql_real_escape_string(trim($_POST['new_menu_title']));
$m_page=mysql_real_escape_string(trim($_POST['new_menu_page']));
$m_visibility=mysql_real_escape_string(trim($_POST['new_menu_visibility']));
$m_live=mysql_real_escape_string(trim($_POST['new_menu_live']));
$sql="update footer_nav set f_name='$m_name',title='$m_title',p_id='$m_page',visibility='$m_visibility',live='$m_live' where f_id='$edit_m_id' limit 1";
$rs=$mydb->query($sql);
header("location: ".$request_url);
exit();
}

/* menu delete functions */
if(CURRENT_FILE=='footer.php' && isset($_REQUEST['action']) && trim($_REQUEST['action'])=='delete' && isset($_REQUEST['mid']) && trim($_REQUEST['mid'])!='')
{
$mid=trim($_REQUEST['mid']);
		$q2="delete from footer_nav where f_id='$mid' limit 1";
		$mydb->query($q2);

header("location: /en/admin/footer.php");		
exit(0);
}

//$_REQUEST['action']=='addfootermenu'

$rs_pages=$mydb->query("select title,p_id,url from pages");
$all_pages_obj=$mydb->loadResult();

do_header();
?>

<div class="content_wrapper admin_white_bg">
<div class="menu_div">
<table class="menu_list_table" id="footer_table"  id="footer_table">
<tr class="nodrag"><td colspan="4"><a href="/en/admin/footer.php?action=addfootermenu">Add menu</a></td></tr>
<tr class="nodrag">
<th></th><th>Menu name</th><th>Actions</th><th>visibility / status</th>
</tr>
<?php
$rs=$mydb->query("select * from footer_nav order by pos");
$r_obj=$mydb->loadResult();
foreach($r_obj as $key => $rs_obj){


switch($rs_obj->visibility)
{
case 'A':
$visible='Admin';
break;
case 'P':
$visible='Public';
break;
case 'B':
$visible='Both';
break;
}




$live_txt=($rs_obj->live=='Y')?'live':'down';
$delete_lnk="<a href='/en/admin/footer.php?action=delete&mid=$rs_obj->f_id' onclick='confirm_delete();'>Delete</a>";
$menu_lnk="<a href='/en/admin/menu_step1.php?menu_id=$rs_obj->f_id'>$rs_obj->f_name</a>";
echo "<tr id='header_top_nav#".$rs_obj->f_id."'><td class='dragHandle'></td><td>$menu_lnk</td><td><a href='/en/admin/footer.php?action=editfootermenu&menu_id=".$rs_obj->f_id."'>edit</a> / $delete_lnk </td><td>$visible / $live_txt </td></tr>";
}
?>
</table>
</form>


</div>
<div class="menu_div">
<?php
if(isset($_REQUEST['action']) && $_REQUEST['action']=='addfootermenu')
{
?>



<script type="text/javascript">
  $(document).ready(function(){
    $("#add_new_menu_1").validate({
			rules: {
			new_menu_name: "required",
			new_menu_title: "required"
				}

		});
  });
  </script>

<form action="/en/admin/footer.php?action=addfootermenu" method="post" id="add_new_menu_1">
<table class="menu_list_table">
<tr><td>Menu name</td><td><input type="text" class="input-text" name="new_menu_name" id="new_menu_name"/></td></tr>
<tr><td>Title</td><td><input type="text" class="input-text" name="new_menu_title" id="new_menu_title"/></td></tr>
<tr><td>Linked to Page</td><td>
<select name="new_menu_page">
<option value=""></option>
<?php
foreach($all_pages_obj as $key => $all_p_obj){
echo '<option value="'.$all_p_obj->p_id.'">'.$all_p_obj->title.'('.$all_p_obj->url.')</option>';
}
?>
</select>
</td></tr>
<tr><td>Active</td><td><select name="new_menu_visibility">
<option value="B">Both</option>
<option value="A">Admin</option>
<option value="P">Public</option>
</select></td></tr>
<tr><td>Make this item</td><td><select name="new_menu_live"><option value="Y">Live</option><option value="N">Down</option></select></td></tr>
<tr><td colspan="2"><input type="submit" value="Save" class="btn_admin"/></td></tr>
</table>
<?php
}
?>
<?php

if(isset($_REQUEST['action']) && $_REQUEST['action']=='editfootermenu' && isset($_REQUEST['menu_id']) && $_REQUEST['menu_id']!='')
{
$menu_id=trim($_REQUEST['menu_id']);
$rs=$mydb->query("select * from footer_nav where f_id='$menu_id'");
$r_obj=$mydb->loadResult();
foreach($r_obj as $key => $rs_obj){
?>
<form action="/en/admin/footer.php?action=editfootermenu&menu_id=<?php echo $rs_obj->f_id;?>" method="post">
<table class="menu_list_table">
<tr><td>Menu name</td><td><input type="text" class="input-text" name="new_menu_name" value="<?php echo html($rs_obj->f_name); ?>"/></td></tr>
<tr><td>Title</td><td><input type="text" class="input-text" name="new_menu_title" value="<?php echo html($rs_obj->title); ?>"/></td></tr>
<tr><td>Linked to Page</td><td>
<select name="new_menu_page">
<option value=""></option>
<?php
foreach($all_pages_obj as $key => $all_p_obj){
$sel_txt='';
if($all_p_obj->p_id!=0 && $all_p_obj->p_id!='' && $rs_obj->p_id==$all_p_obj->p_id)
$sel_txt="SELECTED";
echo '<option '.$sel_txt.' value="'.$all_p_obj->p_id.'">'.$all_p_obj->title.'('.$all_p_obj->url.')</option>';
}
?>
</select></td></tr>

<tr><td>Active</td><td>
<select name="new_menu_visibility">
<option value="A" <?php echo ($rs_obj->visibility=='A')?'SELECTED':'' ?>>Admin</option>
<option value="P" <?php echo ($rs_obj->visibility=='P')?'SELECTED':'' ?>>Public</option>
<option value="B" <?php echo ($rs_obj->visibility=='B')?'SELECTED':'' ?>>Both</option>
</select></td></tr>

<tr><td>Make this item</td><td>
<select name="new_menu_live">
<option value="Y" <?php echo ($rs_obj->live=='Y')?'SELECTED':'' ?>>Live</option>
<option value="N" <?php echo ($rs_obj->live=='N')?'SELECTED':'' ?>>Down</option>
</select></td></tr>
<tr><td colspan="2"><input type="submit" value="Save" class="btn_admin"/></td></tr>
</table>
</form>
<?php
}
}
?>

</div>
<div class="clear"></div>
</div>
<script type="text/javascript">
$(document).ready(function() {
   $('#footer_table').tableDnD({
	    onDragClass: "myDragClass",
        onDrop: function(table, row) {
			var dataString=$.tableDnD.serialize();  
			
		var answer=confirm("Are you want to change the order of menus?\n This will effect the live website. Please confirm.")
		if(answer)
		{
				$.ajax({
				type: "POST",
				url: '/en/admin/src/position_footer.php',
				data: dataString,
				success: function(data) {
				}
				});
		}
        },
        dragHandle: "dragHandle"
    });
	
$("#footer_table tr").hover(function() {
			
			if ($(this).is('.nodrag')) /* crack the first two rows*/
			{
			return;
			}

          $(this.cells[0]).addClass('showDragHandle');
    }, function() {
          $(this.cells[0]).removeClass('showDragHandle');
    });
});
</script>



<?php

do_footer();
?>