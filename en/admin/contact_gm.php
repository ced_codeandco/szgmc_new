<?php
include_once ('includes/commons.php');
do_header();
?>
		<script type="text/javascript" charset="utf-8">
				var oTable;
				$(document).ready(function() {
				oTable=$('#my_table').dataTable( {
					"bProcessing": true,
					"bJQueryUI": true,
					"sPaginationType": "full_numbers",
					"iDisplayLength": 10,
					"bServerSide": true,
					/*"sAjaxSource": 'src/common/json/json_source_customers.php',*/
					"sAjaxSource": 'src/common/json/server_side_json_gm.php',
					"aaSorting": [[0,'desc']],
					"aoColumns": [
					{ bSortable: true, sWidth: '10px' },
						{ bSortable: true, sWidth: '20px' },
						{ bSortable: true, sWidth: '120px' },
						{ bSortable: true, sWidth: '200px' },
						{ bSortable: true, sWidth: '100px' },
						
						{ bSortable: true, sWidth: '200px' },
						{ bSortable: false, sWidth: '10px' }
						

					]

				} );

			} );

		</script>
<div class="content_wrapper admin_white_bg">
    <h2>Contact General Manager</h2>
<table width="100%"  cellpadding="3" cellspacing="3" align="center">
<tr><td align="center" valign="top" >
<div id="container">
<div id="show_all" title="Contact General Manager">
<div id="detail_inner"></div>
</div>
<div align="center" style="width:50px; float:right;"><a href="export_gm.php"><img src="images/excel_icon.jpg" border="0" style="width:30px"></a></div>
<table cellpadding="0" cellspacing="0" border="0" class="display" id="my_table">
	<thead>
		<tr>
			<th>Ref No.</th>
			<th>Name</th>
            <th>Email</th>
           
			<th>Telephone</th>
			
			<th>Mobile</th>
			<th>Date</th>
			
			
			<th>Full Detail</th>

		</tr>
	</thead>
	<tbody>
	</tbody>
</table>
</div>
</td></tr>
</table>
</div>
<?php
do_footer();
?>


