<?php
include_once ('includes/commons.php');
do_header();
?>
		<script type="text/javascript" charset="utf-8">			
				var oTable;
				$(document).ready(function() {
				oTable=$('#my_table').dataTable( {
					"bProcessing": true,					
					"bJQueryUI": true,
					"sPaginationType": "full_numbers",
					"iDisplayLength": 10,
					"bServerSide": true,					 
					/*"sAjaxSource": 'src/common/json/json_source_customers.php',*/
					"sAjaxSource": 'src/common/json/server_side_json_savedNewsletter.php',
					"aaSorting": [[0,'desc']],
					"aoColumns": [						
						{ bSortable: true, sWidth: '40px' },
						{ bSortable: true, sWidth: '20px' },						
						{ bSortable: true, sWidth: '80px' },
						{ bSortable: true, sWidth: '80px' }
						
					]
					
				} );
				
			} );			
			
			function show_tour_approved(p_id)
			{
			$("#show_all").dialog({width: 750});
			$( "#show_all" ).dialog( 'option', 'position', [340, 90]  );
			//var position = $( ".selector" ).dialog( "option", "position" );
			//alert(position);
			$('#detail_inner').html('<img src="images/loading.gif" />');
			$('#show_all').dialog('open')
			
				$.ajax({
			   type: "POST",
			   url: "/admin/previewEmailer.php",
			   data: "request_id="+p_id,
			   success: function(result){
				   $('#detail_inner').html(result);
			   },
			   complete: function(){
			
			   },
			   error: function(){
			$('#detail_inner').html('Error occured. Please try later');
			   }
			
			 });
			}
		</script>
<h2 style="padding-left:20px;">Saved Newsletters</h2>
<table width="100%"  cellpadding="3" cellspacing="3" align="center">
<tr><td align="center" valign="top" >
<div id="container">
<div id="show_all" title="Preview">
<div id="detail_inner"></div>
</div>
<table cellpadding="0" cellspacing="0" border="0" class="display" id="my_table">
	<thead>
		<tr>
			<th>ID</th>
			<th>Subject</th>
			<th>Date</th>
            <th>Actions</th>
			
			
						
		</tr>
	</thead>
	<tbody>
	</tbody>
</table>	
</div>
</td></tr>
</table>
<?php
do_footer();
?>


