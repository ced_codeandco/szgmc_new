<?php
include_once ('includes/commons.php');
do_header();
?>
		<script type="text/javascript" charset="utf-8">
				var oTable;
				$(document).ready(function() {
				oTable=$('#my_table').dataTable( {
					"bProcessing": true,
					"bJQueryUI": true,
					"sPaginationType": "full_numbers",
					"iDisplayLength": 10,
					"bServerSide": true,
					/*"sAjaxSource": 'src/common/json/json_source_customers.php',*/
					"sAjaxSource": 'src/common/json/server_side_json_tours_request.php',
					"aaSorting": [[0,'desc']],
					"aoColumns": [
					{ bSortable: true, sWidth: '40px' },
						{ bSortable: true, sWidth: '100px' },
						{ bSortable: true, sWidth: '100px' },
						{ bSortable: true, sWidth: '120px' },
						{ bSortable: true, sWidth: '100px' },
						{ bSortable: true, sWidth: '70px' },
						{ bSortable: true, sWidth: '100px' },
						{ bSortable: true, sWidth: '40px' },
						{ bSortable: false, sWidth: '100px' }
						

					]

				} );

			} );

		</script>
<div class="content_wrapper admin_white_bg">
    <h2>Lost & Found</h2>
	<div align="center" style="width:50px; float:right;"><a href="lostandfound.php"><img src="images/excel_icon.jpg" border="0" style="width:30px"></a></div> </div>
<table width="100%"  cellpadding="3" cellspacing="3" align="center">
<tr><td align="center" valign="top" >
<div id="container">
<div id="show_all" title="Lost & Found">
<div id="detail_inner"></div>
</div>
<table cellpadding="0" cellspacing="0" border="0" class="display" id="my_table">
	<thead>
		<tr>
			<th>Ref No.</th>
			<th>Date</th>
			<th>Name</th>
            <th>Passport No./ID</th>
			<th>Nationality</th>
			<th>Mobile </th>
			<th>Email</th>
			<th>P.O.Box </th>
            
			
			<th>Full Detail</th>

		</tr>
	</thead>
	<tbody>
	</tbody>
</table>
</div>
</td></tr>
</table>
</div>
<?php
do_footer();
?>


