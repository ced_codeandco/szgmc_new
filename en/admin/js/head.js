function showLoader() {
	$('#loader').css('display','block');
	$('#loader-modal').css('display','block');
}
function hideLoader() {
	$('#loader').css('display','none');
	$('#loader-modal').css('display','none');
}
function updateTips(t,tips) {
	tips
		.text(t)
		.addClass('ui-state-highlight');
	setTimeout(function() {
		tips.removeClass('ui-state-highlight', 1500);
	}, 500);
}

function checkLength(o,n,min,max,tips) {
	if ( o.val().length > max || o.val().length < min ) {
		o.addClass('ui-state-error');
		updateTips("Length of " + n + " must be between "+min+" and "+max+".",tips);
		return false;
	} else {
		return true;
	}
}

function checkRegexp(o,regexp,n,tips) {
	if ( !( regexp.test( o.val() ) ) ) {
		o.addClass('ui-state-error');
		updateTips(n,tips);
		return false;
	} else {
		return true;
	}
}

$(function(){
	$('#navbar ul.menu a.flyout').each(function(){
		$(this).next().wrap('<div style="display: none;"/>');
		$(this).menu({
			content: $(this).next().html(),
			width: 165,
			positionOpts: { posX: 'left', 
			posY: 'bottom',
			offsetX: 0,
			offsetY: 0,
			directionH: 'right',
			directionV: 'down', 
			detectH: false, // do horizontal collision detection  
			detectV: false, // do vertical collision detection
			linkToFront: false },
			flyOut: true
		});
	});
	$('#block-header-menu ul.menu a.flyout').each(function(){
		$(this).next().wrap('<div style="display: none;"/>');
		$(this).menu({
			content: $(this).next().html(),
			width: 165,
			positionOpts: { posX: 'left', 
			posY: 'bottom',
			offsetX: 0,
			offsetY: 0,
			directionH: 'right',
			directionV: 'down', 
			detectH: false, // do horizontal collision detection  
			detectV: false, // do vertical collision detection
			linkToFront: false },
			flyOut: true
		});
	});
});

$(document).ready(function(){
	$("label.inlined + input.input-text").each(function (type) {

		$(this).focus(function () {
			$(this).prev("label.inlined").addClass("focus");
			if($(this).attr("id") == "datepicker") {
				$(this).prev("label.inlined").addClass("has-text").removeClass("focus");
			}
		});
		
		$(this).keypress(function () {
			$(this).prev("label.inlined").addClass("has-text").removeClass("focus");
		});
		
		$(this).blur(function () {
			if($(this).val() == "") {
				$(this).prev("label.inlined").removeClass("has-text").removeClass("focus");
			}
		});
	});
});

/* <![CDATA[ */
function makeSublist(parent,child,isSubselectOptional,childVal,optionalName)
{
	$("body").append("<select style=\"display:none\" id=\""+parent+child+"\"></select>");
	$('#'+parent+child).html($("#"+child+" option"));

	var parentValue = $('#'+parent).attr('value');
	parentValue = parentValue.replace(/\s/g,'');
	parentValue = parentValue.toLowerCase();
	$('#'+child).html($("#"+parent+child+" .sub_"+parentValue).clone());

	childVal = (typeof childVal == "undefined")? "" : childVal ;
	$("#"+child).val(childVal).attr('selected','selected');

	$('#'+parent).change(function(){
		var parentValue = $('#'+parent).attr('value');
		parentValue = parentValue.replace(/\s/g,'');
		parentValue = parentValue.toLowerCase();
		$('#'+child).html($("#"+parent+child+" .sub_"+parentValue).clone());
		if(isSubselectOptional) {
			$('#'+child).prepend("<option value=\"any\" selected=\"selected\">" + optionalName + "</option>");
			$('#'+child+' option:first').attr('selected', 'selected');
		}

		$('#'+child).trigger("change");
		$('#'+child).focus();
	});
}

function makeSublist2(parent,child,isSubselectOptional,optionalVal,optionalName) {
	$("body").append("<select style=\"display:none\" id=\""+parent+child+"\"></select>");
	$('#'+parent+child).html($("#"+child+" option:not(." + optionalVal + ")"));
	
	var parentValue = $('#'+parent).attr('value');
	parentValue = parentValue.replace(/\s/g,'');
	parentValue = parentValue.toLowerCase();
	$('#'+child).html($("#"+parent+child+" .sub_"+parentValue).clone());
	if(isSubselectOptional) {
		$('#'+child).prepend("<option value=\"" + optionalVal + "\">" + optionalName + "</option>");
		$('#'+child+' option:first').attr('selected', 'selected');
	}
	
	$('#'+child).attr('disabled', 'disabled');
	$('#'+child).addClass('disabled');
	
	$('#'+parent).change(function() {
		sublistChangeChild(parent,child,isSubselectOptional,optionalVal,optionalName,true);
	});
}

function sublistChangeChild(parent,child,isSubselectOptional,optionalVal,optionalName,focusOptional) {
	var parentValue = $('#'+parent).attr('value');
	parentValue = parentValue.replace(/\s/g,'');
	parentValue = parentValue.toLowerCase();
	
	var childHTML = $("#"+parent+child+" .sub_"+parentValue).clone();
	$('#'+child).html(childHTML);
	if(isSubselectOptional) {
		$('#'+child).prepend("<option value=\"" + optionalVal + "\">" + optionalName + "</option>");
		if (focusOptional) {
			$('#'+child+' option:first').attr('selected', 'selected');
		}
	}
	$('#'+child).focus();
	
	if (parentValue != optionalVal) {
		$('#'+child).removeAttr('disabled');
		$('#'+child).removeClass('disabled');
	} else {
		$('#'+child).attr('disabled', 'disabled');
		$('#'+child).addClass('disabled');
	}
}

function sublistChangeParent(parent,child,childValue,isSubselectOptional,optionalVal,optionalName,nextParent) {
	childValue = childValue.replace(/\s/g,'');
	childValue = childValue.toLowerCase();

	var childClasses = $('#'+parent+child+' option.'+childValue).attr('class');
	if (childClasses != null) {
		childClasses = childClasses.split(" ");
		var childClass = childClasses[0];
		childClass = childClass.substring(4);
		var selector = $('#'+parent+' option.'+childClass);
		if (selector.attr('class') != null) {
			sublistTriggerParent(selector,parent,child,childValue,isSubselectOptional,optionalVal,optionalName);
		} else {
			sublistChangeParent(nextParent,parent,childClass,isSubselectOptional,optionalVal,optionalName);
			selector = $('#'+parent+' option.'+childClass);
			if (selector.attr('class') != null) {
				sublistTriggerParent(selector,parent,child,childValue,isSubselectOptional,optionalVal,optionalName);
			}
		}
	}
}

function sublistTriggerParent(selector,parent,child,childValue,isSubselectOptional,optionalVal,optionalName) {
	selector.attr('selected', 'selected');
	//$('#'+parent).trigger("change");
	sublistChangeChild(parent,child,isSubselectOptional,optionalVal,optionalName,false);
	var childOption = $('#'+child+' option.'+childValue);
	if (childOption != null) {
		childOption.attr('selected', 'selected');
	}
}

	

/* ]]> */