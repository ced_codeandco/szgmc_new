<?php
include_once ('includes/commons.php');
do_header();
?>
		<script type="text/javascript" charset="utf-8">
				var oTable;
				$(document).ready(function() {
				oTable=$('#my_table').dataTable( {
					"bProcessing": true,
					"bJQueryUI": true,
					"sPaginationType": "full_numbers",
					"iDisplayLength": 10,
					"bServerSide": true,
					/*"sAjaxSource": 'src/common/json/json_source_customers.php',*/
					"sAjaxSource": 'src/common/json/workshop.php',
					"aaSorting": [[0,'desc']],
					"aoColumns": [
						{ bSortable: true, sWidth: '100px' },
						{ bSortable: true, sWidth: '100px' },
						{ bSortable: true, sWidth: '100px' },
						{ bSortable: true, sWidth: '100px' },
						{ bSortable: true, sWidth: '100px' },
						{ bSortable: true, sWidth: '100px' },
						{ bSortable: true, sWidth: '60px' },
						{ bSortable: false, sWidth: '100px' },
						

					]

				} );

			} );

		</script>
<div class="content_wrapper admin_white_bg">
    <h2>Tourist Guide Workshop</h2>
<table width="100%"  cellpadding="3" cellspacing="3" align="center">
<tr><td align="center" valign="top" >
<div id="container">
<div id="show_all" title="Tourist Guide Workshop">
<div id="detail_inner"></div>
</div>
<table cellpadding="0" cellspacing="0" border="0" class="display" id="my_table">
	<thead>
		<tr>
			
			<th>First Name</th>
			<th>Last Name</th>
            <th>Nationality</th>
			<th>Passport Number</th>
			<th>Date of Birth  </th>
			<th>Sponsor  </th>
			<th>Gender</th>
            <th>Full Detail </th>
            
		</tr>
	</thead>
	<tbody>
	</tbody>
</table>
</div>
</td></tr>
</table>
</div>
<?php
do_footer();
?>


