<?php
//error_reporting(E_ALL); 
error_reporting(0);
$file = $_SERVER["SCRIPT_NAME"];
preg_match("/[^\/]+$/",$file,$matches);
$current_file_name= $matches[0];

$userAgent = strtolower($_SERVER['HTTP_USER_AGENT']); 
if (preg_match('/msie/', $userAgent))
$browser_name = 'msie';
if (preg_match('/.+(?:rv|it|ra|ie)[\/: ]([\d.]+)/', $userAgent, $matches))
$version = $matches[1];
if($browser_name=='msie' && $version <=6)
echo "<div style='width:100%; text-align:center; color:#DD0000;'>Incompatible browser found. please use ie 7+ or firefox for better performance</div>";
session_start();

include_once '.././inc/conf.php';
include_once '.././inc/mysql.lib.php';
$mydb=new connect;



function dirImages($dir) {
$path=$dir;// change the path here related to this page
$handle=opendir($path);
while (($file = readdir($handle))!==false) {
//if(strlen($file)>3){echo "<img src=$path$file> <br>";}

$extension = substr($file, strrpos($file, '.')); // Gets the File Extension
if($extension == ".jpg" || $extension == ".jpeg" || $extension == ".gif" || $extension == ".png" || $extension == ".swf" ) 
{
//$fsize=(filesize($dir.$file))/1024;
$fsize=filesize($dir.$file);
$fsize=get_size($fsize,$precision=2,$long_name=true,$real_size=true);
//$fmtime=date("F d Y H:i:s", filemtime($dir.$file));
$fmtime=filemtime($dir.$file);


list($width, $height) = getimagesize($dir.$file); 
{
if($width!='' && $height!='')
$dimensions=$width.'px / '.$height.'px';
}
$images[$file] = array('file'=>$file,'size'=>$fsize,'mtime'=>$fmtime,'dim'=>$dimensions);
}
}
closedir($handle);
foreach ($images as $key => $image_obj) {
    $file[$key]  = $image_obj['file'];
    $size[$key] = $image_obj['size'];
    $dim[$key] = $image_obj['dim'];	
    $mtime[$key] = $image_obj['mtime'];	
	
}

array_multisort($mtime, SORT_DESC,$file, SORT_ASC,$size, SORT_ASC,$dim, SORT_ASC,$images);/* sor according to date modified*/
/*  change date format*/
foreach ($images as $key => $image_obj) {
$images[$key]['mtime']=date("F d Y H:i:s",$images[$key]['mtime']);
}
return $images;
}


/**
 * Get the human-readable size for an amount of bytes
 * @param int  $size      : the number of bytes to be converted
 * @param int $precision  : number of decimal places to round to;
 *                          optional - defaults to 2
 * @param bool $long_name : whether or not the returned size tag should
 *                          be unabbreviated (ie "Gigabytes" or "GB");
 *                          optional - defaults to true
 * @param bool $real_size : whether or not to use the real (base 1024)
 *                          or commercial (base 1000) size;
 *                          optional - defaults to true
 * @return string         : the converted size
 */
function get_size($size,$precision=2,$long_name=true,$real_size=true) {
   $base=$real_size?1024:1000;
   $pos=0;
   while ($size>$base) {
      $size/=$base;
      $pos++;
   }
   $prefix=get_size_prefix($pos);
  // $size_name=$long_name?$prefix."bytes":$prefix[0].'B';
   $size_name=$long_name?$prefix."":$prefix[0].'B';
   return round($size,$precision).' '.ucfirst($size_name);
}
/**
 * @param int $pos : the distence along the metric scale relitive to 0
 * @return string  : the prefix
 */
function get_size_prefix($pos) {
   switch ($pos) {
      case 00: return "";
      case 01: return "KB";
      case 02: return "MB";
      case 03: return "GB";
      case 04: return "tera";
      case 05: return "peta";
      case 06: return "exa";
      case 07: return "zetta";
      case 08: return "yotta";
      case 09: return "xenna";
      case 10: return "w-";
      case 11: return "vendeka";
      case 12: return "u-";
      default: return "?-";
   }
}



function do_header () {
if(!isset($_SESSION['powers']) )
header("Location: logout_handler.php");
	$signed_in = false;
	if (!isset($_SESSION['signed_in']))
	{
		$_SESSION['signed_in'] = $signed_in;
	}
	else
	{
		$signed_in = $_SESSION['signed_in'];
	}
	if(!($signed_in))
	{
            header("Location: /en/admin/");
            exit;
	}
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); 
header('Cache-Control: no-store, no-cache, must-revalidate'); 
header('Cache-Control: post-check=0, pre-check=0', FALSE); 
header('Pragma: no-cache'); 
$UserId =  $_SESSION['username'];
$SiteCode = '10001';
$ip = $_SERVER['REMOTE_ADDR'];
$session_id = session_id();
$username = $_SESSION['username'];
$fullname = $_SESSION['fullname'];
$rpt_access=$_SESSION['report_access'];
?>

<?php
$file = $_SERVER["SCRIPT_NAME"];
preg_match("/[^\/]+$/",$file,$matches);
$current_file_name= $matches[0];
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SZGMC :admin</title>
<link href="/en/admin/styles/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="/en/admin/styles/chrometheme/chromestyle.css" />

		<style type="text/css" title="currentStyle">
			@import "media/css/table_jui.css";
			@import "themes/smoothness/jquery-ui-1.7.2.custom.css";
			/*@import "themes/tpc/jquery-ui-1.7.2.custom.css";*/
			?>
		</style>
		
		
<script type="text/javascript" src="/en/admin/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" language="javascript" src="/en/admin/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/en/admin/styles/chrometheme/chromejs/chrome.js"></script>
<script type="text/javascript" src="/en/admin/js/PrintArea.js"></script>	
<script type="text/javascript" src="/en/admin/js/common.js"></script>	
<script type="text/javascript" src="/en/admin/js/jquery.tablednd_0_5.js"></script>	
<script type="text/javascript" src="/en/admin/js/jquery.validate.js"></script>
<script type="text/javascript" src="/en/admin/js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="/en/admin/js/common.js"></script>


</head><body>
<table width="98%" border="0" cellspacing="2" cellpadding="2" align="center">
<tr>
<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td width="100%"> 

<table width="100%" border="0" align="right" cellpadding="0" cellspacing="0">
<tr >
<td style="border:1px #ccc solid;">
<div class="header_div">
<div class="header_left">


<img class="header_img" src="../img/logo.jpg" />


</div>
<div class="header_right1">
<?php //print date("F jS, Y");  ?>
<!--&nbsp;&nbsp;&#124;&nbsp;&nbsp;Logged in :-->
<?php print $fullname; ?>
</div>

<div class="header_right2">
<a href="logout_handler.php"><img src="images/icons/log.jpg" alt="Logout" /></a> <br/> <a href="change_password.php">Change password</a>
</div>


</div>
</td>
</tr>

<tr style="background-color:#FFFFFF" ><td colspan="6"  >
<div class="chromestyle" id="chromemenu">
<ul>
<?php
if($_SESSION['powers']==1)
{
 
}
else
{
 $mydb=new connect;
 $sql="select at.tab_id,at.link,at.m_id from admin_tabs at,admin_tab_access ata where ata.tab_id=at.tab_id and ata.admin_id='".(string)$_SESSION['uid']."'";
 $tab_sql ="select at.tab_name,at.link,at.m_id from admin_tabs at,admin_tab_access ata where ata.tab_id=at.tab_id and ata.admin_id='".(string)$_SESSION['uid']."' group by at.m_id";
 
 $mydb->query($sql);
$rs_obj=$mydb->loadResult();
$result=array();
foreach($rs_obj as $key => $r_obj){
 $result[]=(string)$r_obj->tab_id;

}
 $mydb->query($tab_sql);
$rs_obj1=$mydb->loadResult();
$result1=array();
foreach($rs_obj1 as $key => $r_obj1){
 $result1[]=(string)$r_obj1->m_id;

}
$_SESSION["tab_access"]=$result;	

$_SESSION["tab"]=$result1;
 $tab=$_SESSION["tab"];
$tab_access=$_SESSION["tab_access"];
}
?>


<?php if($_SESSION['uid'] == '1'){ ?>

<li><a href="pages.php" rel="dropmenu1" 
<?php

if($current_file_name=="pages.php" || $current_file_name=="add_page.php") 
echo ' class="current_menu" ';
?>
 >Manage pages</a></li>
 
<li><a href="news.php" rel="dropmenu4"
<?php
if($current_file_name=="news.php" || $current_file_name=="manage_news.php")
echo ' class="current_menu" ';
?>
 >News</a></li>
 
 <li><a href="activities.php" rel="dropmenu14"
<?php
if($current_file_name=="activities.php" || $current_file_name=="manage_activities.php")
echo ' class="current_menu" ';
?>
 >Center Activities</a></li>
  <li><a href="manage_polls.php" rel="dropmenu18"
<?php
if($current_file_name=="manage_polls.php" || $current_file_name=="view_polls.php")
echo ' class="current_menu" ';
?>
 >Polls</a></li>
 <li><a href="manage_menus.php" rel="dropmenu3"
<?php
if($current_file_name=="manage_menus.php")
echo ' class="current_menu" ';
?>
 >Manage Menu</a></li>
  <li><a href="file.php" rel="dropmenu9"
<?php
if($current_file_name=="group_tour.php" || $current_file_name=="tours_request.php" || $current_file_name=="media-tour.php" || $current_file_name=="filming_request.php"|| $current_file_name=="workshop.php"|| $current_file_name=="job.php"|| $current_file_name=="lost.php"|| $current_file_name=="suggestion.php")
echo ' class="current_menu" ';
?>
 >Enquiries</a></li>
  <li><a href="news.php" rel="dropmenu10"
<?php
if($current_file_name=="publication.php" || $current_file_name=="manage_publication.php")
echo ' class="current_menu" ';
?>
 >Publication</a></li>
 
 <li><a href="cultural_guide.php" rel="dropmenu5"
<?php
if($current_file_name=="cultural_guide.php" || $current_file_name=="manage_guide.php")
echo ' class="current_menu" ';
?>
 >Culture Guide</a></li>
 <li><a href="report_hourly.php" rel="dropmenu15"
<?php
if($current_file_name=="report_hourly.php" || $current_file_name=="report_summary.php" || $current_file_name=="approved_booking.php" || $current_file_name=="disapproved_booking.php" || $current_file_name=="all_booking.php")
echo ' class="current_menu" ';
?>
 >Report</a></li>
<li><a href="calendar.php" rel="dropmenu16"
<?php
if($current_file_name=="calendar.php" )
echo ' class="current_menu" ';
?>
 >Booking Calendar</a></li>
 <li><a href="add_edit_user.php" rel="dropmenu17" <?php
if($current_file_name=="add_edit_user.php" || $current_file_name=="disable.php" )
echo ' class="current_menu" ';
?>
 >Settings</a></li>
 <li><a href="view_vacancy.php" rel="dropmenu19" <?php
if($current_file_name=="view_vacancy.php" || $current_file_name=="manage_vacancy.php" )
echo ' class="current_menu" ';
?>
 >Manage Vacancy</a></li>
 <li><a href="view_vacancy.php" rel="dropmenu20" <?php
if($current_file_name=="news_letter.php" || $current_file_name=="news_letter.php" )
echo ' class="current_menu" ';
?>
 >Newsletter</a></li>
 <li><a href="view_vacancy.php" rel="dropmenu21" <?php
if($current_file_name=="feedback.php" || $current_file_name=="feedback.php" )
echo ' class="current_menu" ';
?>
 >Feedback</a></li>
  <li><a href="view_registered_users.php" rel="dropmenu22" <?php
if($current_file_name=="view_registered_users.php")
echo ' class="current_menu" ';
?>
 >User Profiles</a></li>
  <li><a href="abaya.php" rel="dropmenu23" <?php
if($current_file_name=="abaya.php")
echo ' class="current_menu" ';
?>

 >Abaya</a></li>
    <li><a href="attendance.php" rel="dropmenu24" <?php
if($current_file_name=="attendance.php")
echo ' class="current_menu" ';
?>

 >Attendance</a></li>
  <li><a href="visitors.php" rel="dropmenu25" <?php
if($current_file_name=="visitors.php")
echo ' class="current_menu" ';
?>

 >Visitors Count</a></li>
  <li><a href="marquee.php" rel="dropmenu26" <?php
if($current_file_name=="marquee.php")
echo ' class="current_menu" ';
?>

 >Marquee</a></li>
 <li><a href="banner.php" rel="dropmenu27" <?php
if($current_file_name=="banner.php")
echo ' class="current_menu" ';
?>

 >Banner</a></li>
  <li><a href="send_login_cred.php" rel="dropmenu28" <?php
if($current_file_name=="send_login_cred.php")
echo ' class="current_menu" ';
?>

 >Send login credentials</a></li>
<?php } 
else
{


if(in_array('1',$tab))
{
?>
<li><a href="pages.php" rel="dropmenu1" 
<?php

if($current_file_name=="pages.php" || $current_file_name=="add_page.php") 
echo ' class="current_menu" ';
?>
 >Manage pages</a></li>
<?
}
if(in_array('2',$tab))
{
?> 
<li><a href="news.php" rel="dropmenu4"
<?php
if($current_file_name=="news.php" || $current_file_name=="manage_news.php")
echo ' class="current_menu" ';
?>
 >News</a></li>
 <?
 }
if(in_array('3',$tab))
{
?>
 <li><a href="activities.php" rel="dropmenu14"
<?php
if($current_file_name=="activities.php" || $current_file_name=="manage_activities.php")
echo ' class="current_menu" ';
?>
 >Center Activities</a></li>
 <?
 }
if(in_array('4',$tab))
{
?>
 <li><a href="manage_menus.php" rel="dropmenu3"
<?php
if($current_file_name=="manage_menus.php")
echo ' class="current_menu" ';
?>
 >Manage Menu</a></li>
 <?
 }
if(in_array('5',$tab))
{
?>
  <li><a href="file.php" rel="dropmenu9"
<?php
if($current_file_name=="group_tour.php" || $current_file_name=="tours_request.php" || $current_file_name=="media-tour.php" || $current_file_name=="filming_request.php"|| $current_file_name=="workshop.php"|| $current_file_name=="job.php"|| $current_file_name=="lost.php"|| $current_file_name=="suggestion.php")
echo ' class="current_menu" ';
?>
 >Enquiries</a></li>
 <?
 }
if(in_array('7',$tab))
{
?>
  <li><a href="publication.php" rel="dropmenu10"
<?php
if($current_file_name=="publication.php" || $current_file_name=="manage_publication.php")
echo ' class="current_menu" ';
?>
 >Publication</a></li>
 <?
 }
if(in_array('12',$tab))
{
?>
 <li><a href="cultural_guide.php" rel="dropmenu5"
<?php
if($current_file_name=="cultural_guide.php" || $current_file_name=="manage_guide.php")
echo ' class="current_menu" ';
?>
 >Culture Guide</a></li>
 <?
 }
if(in_array('8',$tab))
{
?>
 <li><a href="report_hourly.php" rel="dropmenu15"
<?php
if($current_file_name=="report_hourly.php" || $current_file_name=="report_summary.php" || $current_file_name=="approved_booking.php" || $current_file_name=="disapproved_booking.php" || $current_file_name=="all_booking.php")
echo ' class="current_menu" ';
?>
 >Report</a></li>
<?
}
if(in_array('21',$tab))
{
?>
<li><a href="calendar.php" rel="dropmenu16"
<?php
if($current_file_name=="calendar.php" )
echo ' class="current_menu" ';
?>
 >Booking Calendar</a></li>
 <?
}
if(in_array('26',$tab))
{
?>
<li><a href="view_registered_users.php" rel="dropmenu22"
<?php
if($current_file_name=="view_registered_users.php" )
echo ' class="current_menu" ';
?>
 >User Profiles</a></li>
 <?
}
if(in_array('28',$tab))
{
?>
<li><a href="send_login_cred.php" rel="dropmenu28"
<?php
if($current_file_name=="send_login_cred.php" )
echo ' class="current_menu" ';
?>
 >Send login credentials</a></li>
 <?
}
}
?>
</ul>
</div>
 <?php if($_SESSION['uid'] == '1'){ ?>
                                               
<div id="dropmenu1" class="dropmenudiv">
<a href="pages.php">View all pages</a>
<a href="add_page.php">Add a new page</a>
</div>
                                                  
<div id="dropmenu2" class="dropmenudiv">
<a href="menus.php">Manage Menus</a>
</div>
<div id="dropmenu3" class="dropmenudiv">
<a href="manage_menus.php">View/Edit Menus</a>
</div>
<div id="dropmenu4" class="dropmenudiv">
<a href="news.php">Manage News</a>
<a href="manage_news.php">Add News</a>
</div>
<div id="dropmenu14" class="dropmenudiv">
<a href="activities.php">Manage Activities</a>
<a href="manage_activities.php">Add Activities</a>
</div>
<div id="dropmenu5" class="dropmenudiv">
<a href="cultural_guide.php">Manage Guide</a>
<a href="manage_guide.php">Add Guide</a>
</div>
<div id="dropmenu15" class="dropmenudiv">
<a href="group_tour_cancelled.php">Cancelled bookings</a>
<a href="report_hourly.php">Hourly Report</a>
<a href="report_summary.php">Summary Report</a>
<a href="approved_booking.php">All Approved Booking</a>
<a href="disapproved_booking.php">All Disapproved Booking</a>
<a href="all_booking.php">All Booking</a>
<a href="advance_report.php">Advance Report</a>
<!--<a href="news_letter.php">Newsletter</a>-->
</div>
<div id="dropmenu6" class="dropmenudiv">
<a href="user.php">Manage User</a>
<a href="manage_user.php">Add New User</a>
</div>
<div id="dropmenu7" class="dropmenudiv">
<a href="file.php">Manage Files</a>
<a href="manage_file.php">Add File</a>
</div>
<div id="dropmenu9" class="dropmenudiv">
<a href="group_tour.php">Group Tour Booking</a>
<!--<a href="tours_request.php">Tour Operator Booking</a>
<a href="media-tour.php">Media Tour</a>-->
<a href="filming_request.php">Filming permission</a>
<!--<a href="workshop.php">Culture Guide Workshop</a>-->
<a href="job.php">Careers</a>
<a href="lost.php">Lost And Found</a>
<a href="suggestion.php">Suggestions & Complaints</a>
<a href="contact_gm.php">Contact General Manager</a>
</div>
<div id="dropmenu10" class="dropmenudiv">
<a href="publication.php">Publication</a>
<a href="manage_publication.php">Manage Publication</a>
</div>
<div id="dropmenu16" class="dropmenudiv">
<a href="calendar.php">Booking Calendar</a>
</div>
<div id="dropmenu17" class="dropmenudiv">
<a href="add_edit_user.php">Add/Edit User</a>
<a href="disable.php">Disable Tour booking dates</a>
<a href="user.php">View/Edit FTP User</a>
<a href="manage_user.php">Add FTP User</a>
<a href="file.php">View FTP Files</a>

</div>
<div id="dropmenu18" class="dropmenudiv">
<a href="manage_polls.php">Create Polls</a>
<a href="view_polls.php">Manage Polls</a>
</div>
<div id="dropmenu19" class="dropmenudiv">
<a href="view_vacancy.php">Manage Vacancy</a>
<a href="manage_vacancy.php">Add Vacancy</a> </div>
<div id="dropmenu20" class="dropmenudiv">
<a href="news_letter.php">Newletter Subscribers</a>
<a href="manage_newsletter.php">Send Newsletter</a> 
<a href="view_newsletters.php">View Newsletter</a>
<a href="view_savedNewsletters.php">Saved Newsletters</a></div>
<div id="dropmenu21" class="dropmenudiv">
<a href="feedback.php?type=1">Tour Booking Form Feedback</a>
<a href="feedback.php?type=2">Career Form Feedback</a> 
<a href="feedback.php?type=3">Filming form Feedback</a>
<a href="feedback.php?type=4">Lost and found Form Feedback</a></div>
<div id="dropmenu22" class="dropmenudiv">
<a href="view_registered_users.php">View Registered Users</a>
<a href="view_registered_users.php?type=T">Temporary Approved Registered Users</a>
<a href="view_registered_users.php?type=A">Approved Registered Users</a>
<a href="view_registered_users.php?type=D">Disapproved Registered Users</a>
</div>
<div id="dropmenu23" class="dropmenudiv">
<a href="abaya.php">Abaya</a>
<a href="view_abaya.php">View Abaya</a></div>
<div id="dropmenu24" class="dropmenudiv">
<a href="attendance.php">Attendance</a>
<a href="view_attendance.php">View Attendance</a></div>
<div id="dropmenu25" class="dropmenudiv">
<a href="visitors.php">Visitors Count</a></div>
<div id="dropmenu26" class="dropmenudiv">
<a href="manage_marquee.php">Add Marquee</a>
<a href="marquee.php">View Marquee</a></div>
<div id="dropmenu27" class="dropmenudiv">
<a href="manage_banner.php">Add Banner</a>
<a href="banner.php">View Banner</a></div>
<div id="dropmenu28" class="dropmenudiv">
<a href="send_login_cred.php">Send Login credentials</a></div>
 <?php }
 else
 
 { ?>
                                               
<div id="dropmenu1" class="dropmenudiv">
<?
if(in_array('1',$tab_access))
{
?>
<a href="pages.php">View all pages</a>
<?
}
if(in_array('2',$tab_access))
{
?>
<a href="add_page.php">Add a new page</a>
<?
}
?>
</div>
                                                  
<div id="dropmenu3" class="dropmenudiv">
<?
if(in_array('7',$tab_access))
{
?>
<a href="manage_menus.php">View/Edit Menus</a>
<?
}
?>
</div>
<div id="dropmenu4" class="dropmenudiv">
<?
if(in_array('3',$tab_access))
{
?>
<a href="news.php">Manage News</a>
<?
}
if(in_array('4',$tab_access))
{
?>
<a href="manage_news.php">Add News</a>
<?
}
?>
</div>
<div id="dropmenu14" class="dropmenudiv">
<?
if(in_array('6',$tab_access))
{
?>
<a href="activities.php">Manage Activities</a>
<?
}
if(in_array('7',$tab_access))
{
?>
<a href="manage_activities.php">Add Activities</a>
<?
}
?>
</div>
<div id="dropmenu5" class="dropmenudiv">
<?
if(in_array('15',$tab_access))
{
?>
<a href="cultural_guide.php">Manage Guide</a>
<?
}
if(in_array('16',$tab_access))
{
?>
<a href="manage_guide.php">Add Guide</a>
<?
}?>
</div>
<div id="dropmenu15" class="dropmenudiv">
<?
if(in_array('17',$tab_access))
{
?>
<a href="report_hourly.php">Hourly Report</a>
<?
}
if(in_array('18',$tab_access))
{
?>
<a href="report_summary.php">Summary Report</a>
<?
}
if(in_array('22',$tab_access))
{
?>
<a href="approved_booking.php">All Approved Booking</a>
<?
}
if(in_array('23',$tab_access))
{
?>
<a href="disapproved_booking.php">All Disapproved Booking</a>
<?
}
if(in_array('24',$tab_access))
{
?>
<a href="all_booking.php">All Booking</a>
<?
}
if(in_array('25',$tab_access))
{
?>
<a href="advance_report.php">Advance Report</a>
<?
}
?>
</div>


<div id="dropmenu9" class="dropmenudiv">
<?
if(in_array('8',$tab_access))
{
?>
<a href="group_tour.php">Group Tour Booking</a>
<?
}
?>
<!--<a href="tours_request.php">Tour Operator Booking</a>-->
<!--<a href="media-tour.php">Media Tour</a>-->
<?
if(in_array('9',$tab_access))
{
?>
<a href="filming_request.php">Filming permission</a>
<?
}
?>
<!--<a href="workshop.php">Culture Guide Workshop</a>-->
<?
if(in_array('10',$tab_access))
{
?>
<a href="job.php">Careers</a>
<?
}
if(in_array('11',$tab_access))
{
?>
<a href="lost.php">Lost And Found</a>
<?
}
if(in_array('12',$tab_access))
{
?>
<a href="suggestion.php">Suggestions & Complaints</a>
<? } ?>
</div>
<div id="dropmenu10" class="dropmenudiv">
<?
if(in_array('13',$tab_access))
{
?>
<a href="publication.php">Publication</a>
<?
}
if(in_array('14',$tab_access))
{
?>
<a href="manage_publication.php">Manage Publication</a> 
<? } ?>
</div>
<div id="dropmenu16" class="dropmenudiv">
<?php
if(in_array('21',$tab_access))
{
?>
<a href="calendar.php">Booking Calendar</a>
<?php
}
?>
</div>
<div id="dropmenu22" class="dropmenudiv">
<?php
if(in_array('26',$tab_access))
{
?>
<a href="view_registered_users.php">User Profiles</a>
<?php
}
?>
<!---->
<?php
if(in_array('27',$tab_access))
{
?>
<a href="view_registered_users.php">View Registered Users</a>
<?php
}
?>
<!---->
<?php
if(in_array('28',$tab_access))
{
?>
<a href="view_registered_users.php?type=T">Temporary Approved Registered Users</a>
<?php
}
?>
<!---->
<?php
if(in_array('29',$tab_access))
{
?>
<a href="view_registered_users.php?type=A">Approved Registered Users</a>
<?php
}
?>
<!---->
<?php
if(in_array('30',$tab_access))
{
?>
<a href="view_registered_users.php?type=D">Disapproved Registered Users</a>
<?php
}
?>
<!---->
<?php
if(in_array('31',$tab_access))
{
?>
<a href="send_login_cred.php">Send Login credentials</a>
<?php
}
?>
<!---->
</div>
 <?php } 
 
 ?>

			  </td></tr>
            </table></td>
        </tr>
        <tr>
          <td height="500" class="ouline" valign="top">
<?php
}

function do_header_login(){
$signed_in = false;
if (!isset($_SESSION['signed_in']))
{
	$_SESSION['signed_in'] = $signed_in;
}
else
{
	$signed_in = $_SESSION['signed_in'];
}
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); 
header('Cache-Control: no-store, no-cache, must-revalidate'); 
header('Cache-Control: post-check=0, pre-check=0', FALSE); 
header('Pragma: no-cache'); 
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SZGMC :admin</title>
<link href="styles/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-1.3.2.min.js"></script>
</head><body>
 <table width="98%" border="0" cellspacing="2" cellpadding="2" align="center">
<tr >
<td style="border:1px #ccc solid;">
<div class="header_div">
<div class="header_left">
<!--
<img class="header_img" src="images/cms.jpg" />
-->
</div>
<div class="header_right1">
<?php print date("F jS, Y");  ?>
</div>
<div class="header_right2">
</div>


</div>
        <tr>
          <td height="500" class="ouline">
<?php
}
function do_footer () {
?>  
	</td>
        </tr>
        <tr>
          <td height="30" bgcolor="#F0F0F0"><div style="border:1px #ccc solid;" align="center">&copy; Copyright 2012 by clicks N hits. All rights reserved.  </div>
		  </td>
        </tr>
      </table></td>
  </tr>
</table>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>		
<script type="text/javascript">
<?php
			$file = $_SERVER["SCRIPT_NAME"];
			$flag=preg_match("/index.php/",$file);
			if(!$flag)
			echo 'cssdropdown.startchrome("chromemenu")';
			
			?>

</script>
<script>
function confirmAction1()
 {
	 var answer = confirm("Are you sure? you want to delete.")
	if (answer){
	return true;
	}
	else{
	return false;
	}

	return false;
 }
</script>
</body>
</html>
<?php
}
?>
