<?php
include_once ('includes/commons.php');
do_header();
?>
		<script type="text/javascript" charset="utf-8">			
				var oTable;
				$(document).ready(function() {
				oTable=$('#my_table').dataTable( {
					"bProcessing": true,					
					"bJQueryUI": true,
					"sPaginationType": "full_numbers",
					"iDisplayLength": 10,
					"bServerSide": true,					 
					/*"sAjaxSource": 'src/common/json/json_source_customers.php',*/
					"sAjaxSource": 'src/common/json/server_side_json_attendance.php',
					"aaSorting": [[0,'desc']],
					"aoColumns": [						
						{ bSortable: true, sWidth: '40px' },
						{ bSortable: true, sWidth: '20px' },						
						{ bSortable: true, sWidth: '80px' },
						{ bSortable: true, sWidth: '80px' },
						{ bSortable: true, sWidth: '80px' },
						{ bSortable: true, sWidth: '80px' },
						{ bSortable: true, sWidth: '80px' },
						{ bSortable: true, sWidth: '80px' },
						{ bSortable: true, sWidth: '80px' }
						
					]
					
				} );
				
			} );			
		$(function() {
$("#from_date").datepicker({dateFormat: 'dd-mm-yy'});
$("#to_date").datepicker({dateFormat: 'dd-mm-yy'});
});	
		</script>
<h2 style="padding-left:20px;">View Attendance</h2>
<div style="margin:0 auto; float:right">
<div align="center" style="width:50px; float:left;"><a href="export_attendance.php"><img src="images/excel_icon.jpg" border="0" style="width:30px"></a></div> </div>
<form action="attendance_datewise.php" method="post">
<table align="center" style="margin-top:15px;"  >
<tr><td valign="top"> From : </td> <td>
<input type="text" id="from_date" name="from_date" style="width:80px;" value="<?php echo $from_date;?>"/> </td>
<td> To : </td> <td>
<input type="text" id="to_date" name="to_date" style="width:80px;" value="<?php echo $to_date;?>"/> </td>  <td> <input type="submit" value="submit"> </td></tr>
</table>
</form>
<table width="100%"  cellpadding="3" cellspacing="3" align="center">
<tr><td align="center" valign="top" >
<div id="container">

<table cellpadding="0" cellspacing="0" border="0" class="display" id="my_table">
	<thead>
		<tr>
			<th>Tour Id</th>
            <th>Booking Date</th>
            <th>Date of Visit</th>
            <th>Organisation Name</th>
			<th>Visit Time</th>
			<th>Attendance Time</th>
			
            <th>Accept/Reject</th>
            <th>Status</th>
		<th>Group size</th>	
						
		</tr>
	</thead>
	<tbody>
	</tbody>
</table>	
</div>
</td></tr>
</table>
<?php
do_footer();
?>


