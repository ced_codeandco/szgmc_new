<?php
ob_start();
session_start();
$username = $_SESSION['username'];
include_once ('includes/commons.php');
do_header();

?>
		<script type="text/javascript" charset="utf-8">
				var oTable;
				$(document).ready(function() {
				oTable=$('#my_table').dataTable( {
					"bProcessing": true,
					"bJQueryUI": true,
					"sPaginationType": "full_numbers",
					"iDisplayLength":30,
					"bServerSide": true,
					/*"sAjaxSource": 'src/common/json/json_source_customers.php',*/
					"sAjaxSource": 'src/common/json/all.php',
					"aaSorting": [[8,'desc']],
					"aoColumns": [
					    { bSortable: true, sWidth: '80px' },
						{ bSortable: true, sWidth: '100px' },
						{ bSortable: true, sWidth: '100px' },
						{ bSortable: true, sWidth: '100px' },
						{ bSortable: true, sWidth: '100px' },
						{ bSortable: true, sWidth: '100px' },
						{ bSortable: false, sWidth: '100px' },
						{ bSortable: false, sWidth: '150px' },
						{ bSortable: false, sWidth: '100px' },
						
						{ bSortable: false, sWidth: '150px' },
						{ bSortable: false, sWidth: '100px' },
						{ bSortable: false, sWidth: '50px' }
							
						
						

					]

				} );

			} );

		</script>
		<script language="javascript" type="text/javascript">

function getXMLHTTP() { //fuction to return the xml http object
		var xmlhttp=false;	
		try{
			xmlhttp=new XMLHttpRequest();
		}
		catch(e)	{		
			try{			
				xmlhttp= new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch(e){
				try{
				xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
				}
				catch(e1){
					xmlhttp=false;
				}
			}
		}
		 	
		return xmlhttp;
    }
	
function guide(gid,id) {		
		
		var strURL="guide.php?gid="+gid+"&id="+id;
		var req = getXMLHTTP();
		msg = 'Are you sure you want to send mail to ' + gid;
		if (confirm(msg)){


		if (req) {
			
			req.onreadystatechange = function() {
				if (req.readyState == 4) {
					// only if "OK"
					if (req.status == 200) {						
						document.getElementById('guide').innerHTML=req.responseText;	
						window.location = "/en/admin/all_booking.php"	;						
					} else {
						alert("There was a problem while using XMLHTTP:\n" + req.statusText);
					}
				}				
			}			
			req.open("GET", strURL, true);
			req.send(null);
		} }
		else
		{
		window.location = "/en/admin/all_booking.php";
		}		
	
}
	</script>
<div class="content_wrapper admin_white_bg">
    <h2>All Booking</h2>
	<div id="guide">&nbsp;</div>
<table width="100%"  cellpadding="3" cellspacing="3" align="center">
<tr><td align="center" valign="top" >
<div id="container">
<div id="show_all" title="Group Tour Booking Form">
<div id="detail_inner"></div>
</div>
 <div style="margin:0 auto; float:right">

<div align="center" style="width:50px; float:left;"><a href="approved_user.php?status=all"><img src="images/excel_icon.jpg" border="0" style="width:30px"></a></div> </div>
<table cellpadding="0" cellspacing="0" border="0" class="display" id="my_table">
	<thead>
		<tr>
			<th>Ref No.</th>
			<th>Date of Enquiry</th>
			<th>Name</th>
			<th>Group Name</th>
            <th>Email</th>
			<th>Visit date</th>
			<th>Culture Guide </th>
			<th>Culture Guide Status </th>
			
			<th>Approved By </th>
			<th>Approval/Disapproval Date </th>
			<th>Type Of Enquiry </th>
			<th>Print View </th>
			
			
            
		</tr>
	</thead>
	<tbody>
	</tbody>
</table>
</div>
</td></tr>
</table>
</div>
<?php
do_footer();
?>


