<?php
include_once ('includes/commons.php');
do_header();

?>
<?php
		/*
		echo '<style type="text/css">';
		echo '@import "/js/admin/media/css/table_jui.css";';
		//echo '@import "/css/themes/smoothness/jquery-ui-1.7.2.custom.css";';
		echo '</style>';
		*/
?>
		<script type="text/javascript" charset="utf-8">			
				var oTable;
		
				$(document).ready(function() {
				oTable=$('#my_table_list_pages').dataTable( {
					"bProcessing": true,					
					"bJQueryUI": true,
					"sPaginationType": "full_numbers",
					"iDisplayLength": 10,
					"bServerSide": true,					 					
					"sAjaxSource": '/en/admin/src/common/json/json_view_edit_pages.php',
					"aaSorting": [[6,'desc']],
					"aoColumns": [						
						{ bSortable: true, sWidth: '150px' },
						{ bSortable: true, sWidth: '150px' },						
						{ bSortable: true, sWidth: '60px' },						
						{ bSortable: true, sWidth: '30px' },
						{ bSortable: true, sWidth: '80px' },
						{ bSortable: true, sWidth: '90px' },
						{ bSortable: true, sWidth: '90px' },
						{ bSortable: false, sWidth: '40px' }
						
					]
					
				} );
				
			} );			
			
		</script>
<div class="content_wrapper admin_white_bg">
<h2>View/Edit Pages</h2>
<table width="100%"  cellpadding="3" cellspacing="3" align="center">
<tr><td align="center" valign="top" >
<div id="container">
<table cellpadding="0" cellspacing="0" border="0" class="display" id="my_table_list_pages">
	<thead>
		<tr>
			<th>Page</th>
			<th>URL</th>
			<th>Visibility</th>
			<th>Live</th>
			<th>Created by</th>
			<th>Created date</th>
			<th>Updated date</th>
			<th>Actions</th>
						
		</tr>
	</thead>
	<tbody>
	</tbody>
</table>
</div>
</td></tr>
</table>
</div>

<?php

do_footer();
?>