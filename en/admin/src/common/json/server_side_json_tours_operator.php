<?php
  /* MySQL connection */
session_start();
if(!isset($_SESSION['powers']) )
header("Location: /logout_handler.php");

include_once($_SERVER['DOCUMENT_ROOT'].'/en/inc/conf.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/en/inc/mysql.lib.php');
$mydb=new connect;
	/* Paging */
	$sLimit = "";
	if ( isset( $_GET['iDisplayStart'] ) )
	{
		$sLimit = "LIMIT ".mysql_real_escape_string( $_GET['iDisplayStart'] ).", ".
			mysql_real_escape_string( $_GET['iDisplayLength'] );
	}

	/* Ordering */
	if ( isset( $_GET['iSortCol_0'] ) )
	{
		$sOrder = "ORDER BY  ";
		for ( $i=0 ; $i<mysql_real_escape_string( $_GET['iSortingCols'] ) ; $i++ )
		{
			$sOrder .= fnColumnToField(mysql_real_escape_string( $_GET['iSortCol_'.$i] ))."
			 	".mysql_real_escape_string( $_GET['iSortDir_'.$i] ) .", ";
		}
		$sOrder = substr_replace( $sOrder, "", -2 );
	}else{
            $sOrder = "ORDER BY created DESC";
        }



	/* Filtering - NOTE this does not match the built-in DataTables filtering which does it
	 * word by word on any field. It's possible to do here, but concerned about efficiency
	 * on very large tables, and MySQL's regex functionality is very limited
	 */
	$sWhere = "";
	if ( $_GET['sSearch'] != "" )
	{

					$sWhere = "WHERE id LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ".
					    "first_name LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ".
		                "last_name LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ".
						"nationality LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ".
						"passport_number LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ".
						"dob LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ".
						"sponser LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ".
						"`gender` LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ".
						"`appear` LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ".
						"`adta` LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ".
						"`dtcm` LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ".
						"`sctda` LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ".
						"`other` LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ".
						"`workshop` LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ".
						"`pobox` LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ".
						"`phone` LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ".
						"`mobile` LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ".
		                "`email` LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%'";



	}
//        $sOrder = 'ORDER BY id DESC';
	 $sQuery = "SELECT SQL_CALC_FOUND_ROWS * FROM touroperator
		$sWhere
		$sOrder
		$sLimit	";



$myFile = "logs.txt";
$fh = fopen($myFile, 'w') or die("can't open file");
fwrite($fh,$sQuery);
fclose($fh);



	$rResult = $mydb->query($sQuery);



	$sQuery = "SELECT FOUND_ROWS()";
	$rResultFilterTotal = $mydb->query($sQuery);
	$aResultFilterTotal = mysql_fetch_array($rResultFilterTotal);
	$iFilteredTotal = $aResultFilterTotal[0];


	$sQuery = "SELECT COUNT(*) FROM touroperator";
	$rResultTotal =$mydb->query($sQuery);
	$aResultTotal = mysql_fetch_array($rResultTotal);
	$iTotal = $aResultTotal[0];

	echo '{';
	echo '"sEcho": '.$_GET['sEcho'].', ';
	echo '"iTotalRecords": '.$iTotal.', ';
	echo '"iTotalDisplayRecords": '.$iFilteredTotal.', ';
	echo '"aaData": [ ';


$jsonData="";

while($obj=mysql_fetch_object($rResult)){
//    print_r($obj);


        $firstname=$obj->first_name;
		$lastname=$obj->last_name;
        $ref="TO-".str_pad($obj->id,4,"0",STR_PAD_LEFT);
        $nationality=$obj->nationality;
        $passport=$obj->passport_number;
	
        $sponser=$obj->sponser;


	$gender=$obj->gender;
	$dob=$obj->dob;
	
	

   $actions_txt="<a href='#' onclick='show_tour_operator(\"".$obj->id."\");return false;'><img src='images/edit_icon.gif' title='Edit Customer Details' class='icon'/></a>&nbsp;
   <a href='#' onclick='make_approve(".$obj->id.");return false;'>Approve</a>&nbsp;<a href='#' onclick='make_disapprove(".$obj->id.");return false;'>Disapprove</a>&nbsp;<a href='#' onclick='show_disapproved_comments(\"".$obj->id."\");return false;'>Disapprove with comments</a>
   ";
    if($obj->status=='Y')
$active_txt="<img src='images/green_ball.jpg' alt='Yes' title='Approved' />";
else
$active_txt="<img src='images/red_ball.jpg' alt='No' title='Disapproved' />";
    $actions_txt=mysql_real_escape_string($actions_txt);
	$active_txt=addslashes($active_txt);
    if($jsonData!="")
    $jsonData.=",";
    $jsonData.="['".$ref."','".$firstname."','".$lastname."','".$nationality."','".$passport."','".$gender."','".$active_txt."','".$actions_txt."']";
//    $jsonData.="['12','test','rest.jpg','Title','News','date','last_updated','actions_txt']";
}

echo $jsonData;
	echo '] }';

	function fnColumnToField( $i )
	{
		if ( $i == 0 )
			return "id";
		if ( $i == 1 )
			return "first_name";
		else if ( $i == 2 )
			return "last_name";
		else if ( $i == 3 )
			return "nationality";
		else if ( $i == 4 )
			return "passport_number";
		else if ( $i == 5 )
			return "sponser";
                else if ( $i == 6 )
			return "gender";
			 
	}
?>
