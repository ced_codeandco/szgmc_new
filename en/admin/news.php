<?php
include_once ('includes/commons.php');
do_header();
?>
		<script type="text/javascript" charset="utf-8">			
				var oTable;
				$(document).ready(function() {
				oTable=$('#my_table').dataTable( {
					"bProcessing": true,					
					"bJQueryUI": true,
					"sPaginationType": "full_numbers",
					"iDisplayLength": 10,
					"bServerSide": true,					 
					/*"sAjaxSource": 'src/common/json/json_source_customers.php',*/
					"sAjaxSource": 'src/common/json/server_side_json_news.php',
					"aaSorting": [[5,'desc']],
					"aoColumns": [						
						{ bSortable: true, sWidth: '40px' },
						{ bSortable: true, sWidth: '20px' },						
						{ bSortable: false, sWidth: '50px' },						
						{ bSortable: true, sWidth: '150px' },
						{ bSortable: true, sWidth: '400px' },
						{ bSortable: true, sWidth: '80px' },
						{ bSortable: true, sWidth: '80px' },
						{ bSortable: false, sWidth: '40px' }
						
					]
					
				} );
				
			} );			
			
		</script>
<h2 style="padding-left:20px;">News</h2>
<table width="100%"  cellpadding="3" cellspacing="3" align="center">
<tr><td align="center" valign="top" >
<div id="container">
<table cellpadding="0" cellspacing="0" border="0" class="display" id="my_table">
	<thead>
		<tr>
			<th>News ID</th>
			<th>Live</th>
			<th>Image</th>
			<th>News Title</th>
			<th>News</th>
			<th>News Date</th>
			<th>Last Updated</th>
			<th>Actions</th>
						
		</tr>
	</thead>
	<tbody>
	</tbody>
</table>	
</div>
</td></tr>
</table>
<?php
do_footer();
?>


