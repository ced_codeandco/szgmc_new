<?php
ob_start();
include_once ('includes/commons.php');;
do_header();
?>
<script type="text/javascript">
$(function() {
$("#from_date").datepicker({dateFormat: 'dd-mm-yy'});
$("#to_date").datepicker({dateFormat: 'dd-mm-yy'});
});
</script>
<style>
.whiteBackground { background-color: #fff; }
.grayBackground { background-color: #ccc; }

</style>
<?php
if(isset($_POST["from_date"]))
{
$from_date=$_POST["from_date"];
$from_date1 = explode("-",$from_date);
$fromdate = $from_date1[2]."-".$from_date1[1]."-".$from_date1[0];

}
else
{
//$from_date=date("d-m-Y");
}
if(isset($_POST["to_date"]))
{
$to_date=$_POST["to_date"];
$to_date1 = explode("-",$to_date);
$todate = $to_date1[2]."-".$to_date1[1]."-".$to_date1[0];
}
else
{
	//$to_date=date("d-m-Y");
}
?>
<script type="text/javascript" src="/en/admin/js/common.js"></script>
<script>
function check_additional_fileds(val)
{
	if((val=='Category') )
	{
     $('.gcat').show();
	}
	else
	{
	  $('.gcat').hide();
	}
	if((val=='guide') )
	{
     $('.guide').show();
	}
	else
	{
	  $('.guide').hide();
	}
	if((val=='edited') || (val=='approved') || (val=='disapproved') )
	{
     $('.user').show();
	}
	else
	{
	  $('.user').hide();
	}
	if((val=='Enquiry') || (val=='Name') || (val=='Group') || (val=='Email') || (val=='visit') || (val=='Category') || (val=='guide') || (val=='edited') || (val=='approved') || (val=='disapproved') || (val=='status') || (val=='type_of_booking') || (val=='attendance') || (val=='attendance_time') )
	{
     $('.search').show();
	}
	else
	{
	 $('.search').hide();
	}  
}
</script>
<?php

if($_POST["cat"] == "Category")
{
$cat = $_POST["group_category"];
}
else
{
	$cat = "";
}
$cat1 = $_POST["cat"];
$search_item = trim($_POST["search_item"]);
$guide_id = $_POST["guide"];
$user_name=$_POST["user"];
?>
<form  action="<?php echo $_SERVER['PHP_SELF']?>" method="post"    >
<h1 style="margin-left:25px;">Advance Report </h1>
 <div id='toursPrintDiv' style='clear:both;'>
 <table align="center" style="margin-top:15px;"  >
 <tr><td><table align="center" style="margin-top:15px;"  >
   <tr>
     <td>From <br/>
       <input type="text" id="from_date" name="from_date" style="width:80px;" value="<?php echo $from_date;?>"/></td>
     <td>To<br/>
       <input type="text" id="to_date" name="to_date" style="width:80px;" value="<?php echo $to_date;?>"/></td>
     <td>Select<br/>
       <select onchange="check_additional_fileds(this.options[this.selectedIndex].value);" name="cat">
         <option>Select </option>
         <option value="Category" <?php if($cat1=="Category") { ?> selected="selected" <?php } ?>>Group Category</option>
         <option value="Enquiry" <?php if($cat1=="Enquiry") { ?> selected="selected" <?php } ?>>Date of Enquiry</option>
         <option value="Name" <?php if($cat1=="Name") { ?> selected="selected" <?php } ?>>Name</option>
         <option value="Group" <?php if($cat1=="Group") { ?> selected="selected" <?php } ?>>Group Name</option>
         <option value="Email" <?php if($cat1=="Email") { ?> selected="selected" <?php } ?>>Email </option>
         <option value="visit" <?php if($cat1=="visit") { ?> selected="selected" <?php } ?>>Visit Date</option>
         <option value="guide" <?php if($cat1=="guide") { ?> selected="selected" <?php } ?>>Culture Guide</option>
         <option value="edited" <?php if($cat1=="edited") { ?> selected="selected" <?php } ?>>Edited By</option>
         <option value="approved" <?php if($cat1=="approved") { ?> selected="selected" <?php } ?>>Approved By</option>
         <option value="disapproved" <?php if($cat1=="disapproved") { ?> selected="selected" <?php } ?>>Disapproved By</option>
         <option value="booking" <?php if($cat1=="booking") { ?> selected="selected" <?php } ?>>Total Booking</option>
         <option value="fast_track" <?php if($cat1=="fast_track") { ?> selected="selected" <?php } ?>>Temporary Pass</option>
         <option value="status" <?php if($cat1=="status") { ?> selected="selected" <?php } ?>>Status</option>
         <option value="type_of_booking" <?php if($cat1=="type_of_booking") { ?> selected="selected" <?php } ?>>Type of booking</option>
         <option value="attendance" <?php if($cat1=="attendance") { ?> selected="selected" <?php } ?>>Accept / Reject</option>
         <option value="attendance_time" <?php if($cat1=="attendance_time") { ?> selected="selected" <?php } ?>>Attendance time</option>
       </select></td>
     <td class="gcat" style=" <?php if($cat1!="Category") { ?>display:none; <?php } ?>">Select<br/>
       <select class="input_cls" name="group_category" id="group_category"  >
         <option value="">Please select - &#1610;&#1585;&#1580;&#1610; &#1575;&#1582;&#1578;&#1610;&#1575;&#1585;</option>
         <optgroup label="----------------------------------------" > </optgroup>
         <option value="General Public" <?php if($cat=="General Public"){ ?> selected="selected"  <?php } ?>>&#1586;&#1610;&#1575;&#1585;&#1577; &#1593;&#1575;&#1605;&#1607;</option>
         <option value="General Public" <?php if($cat=="General Public"){ ?> selected="selected"  <?php } ?>>General Public</option>
         <optgroup label="----------------------------------------" > </optgroup>
         <option value="Government" <?php if($cat=="Government"){ ?> selected="selected"  <?php } ?>>&#1575;&#1604;&#1607;&#1610;&#1574;&#1575;&#1578; &#1575;&#1604;&#1581;&#1603;&#1608;&#1605;&#1610;&#1577;</option>
         <option value="Government" <?php if($cat=="Government"){ ?> selected="selected"  <?php } ?>>Government Entity</option>
         <optgroup label="----------------------------------------" > </optgroup>
         <option value="Embassy" <?php if($cat=="Embassy"){ ?> selected="selected"  <?php } ?>>&#1575;&#1604;&#1587;&#1601;&#1575;&#1585;&#1575;&#1578;</option>
         <option value="Embassy" <?php if($cat=="Embassy"){ ?> selected="selected"  <?php } ?>>Embassy</option>
         <optgroup label="----------------------------------------" > </optgroup>
         <option value="Tour Operator" <?php if($cat=="Tour Operator"){ ?> selected="selected"  <?php } ?>>&#1575;&#1604;&#1588;&#1585;&#1603;&#1575;&#1578; &#1575;&#1604;&#1587;&#1610;&#1575;&#1581;&#1610;&#1577;</option>
         <option value="Tour Operator" <?php if($cat=="Tour Operator"){ ?> selected="selected"  <?php } ?>>Tour Operator</option>
         <optgroup label="----------------------------------------" > </optgroup>
         <option value="Education" <?php if($cat=="Education"){ ?> selected="selected"  <?php } ?>>&#1575;&#1604;&#1605;&#1572;&#1587;&#1587;&#1575;&#1578; &#1575;&#1604;&#1578;&#1593;&#1604;&#1610;&#1605;&#1610;&#1577;</option>
         <option value="Education" <?php if($cat=="Education"){ ?> selected="selected"  <?php } ?>>Educational Institution</option>
         <optgroup label="----------------------------------------" > </optgroup>
         <option value="Hotel" <?php if($cat=="Hotel"){ ?> selected="selected"  <?php } ?>>&#1575;&#1604;&#1601;&#1606;&#1583;&#1602;</option>
         <option value="Hotel" <?php if($cat=="Hotel"){ ?> selected="selected"  <?php } ?>>Hotel</option>
         <optgroup label="----------------------------------------" > </optgroup>
         <option value="Corporate">&#1575;&#1604;&#1602;&#1591;&#1575;&#1593; &#1575;&#1604;&#1582;&#1575;&#1589;</option>
         <option value="Corporate">Corporate Sector</option>
         <optgroup label="----------------------------------------" > </optgroup>
         <option value="All" <?php if($cat=="All"){ ?> selected="selected"  <?php } ?>>&#1580;&#1605;&#1610;&#1593;</option>
         <option value="All" <?php if($cat=="All"){ ?> selected="selected"  <?php } ?>>All</option>
       </select></td>
     <td class="guide" style=" <?php if($cat1!="guide") { ?>display:none; <?php } ?>">Select<br/>
       <?php
$csql = "SELECT * FROM `cultural_guide`";
$cquery = mysql_query($csql);
?>
       <select class="input_cls" name="guide" id="guide"  >
         <option value="">Select </option>
         <?php
while($cres = mysql_fetch_object($cquery))
{
?>
         <option value=<?php echo $cres->g_id; if($cres->g_id==$guide_id){ ?> selected="selected" <?php } ?> > <?php echo $cres->g_name ; ?></option>
         <?php
}
?>
       </select></td>
     <td class="user" style=" <?php if(($cat1!="edited") && ($cat1!="approved") && ($cat1!="disapproved") ) { ?>display:none; <?php } ?>">Select<br/>
       <?php
$csql = "SELECT * FROM `admin_login` where id>11";
$cquery = mysql_query($csql);
?>
       <select class="input_cls" name="user" id="user"  >
         <option value="">Select </option>
         <?php
while($cres = mysql_fetch_object($cquery))
{
?>
         <option value=<?php echo $cres->user_name; if($cres->user_name==$user_name){ ?> selected="selected" <?php } ?> > <?php echo $cres->user_name ; ?></option>
         <?php
}
?>
       </select></td>
     <td class="search" style=" <?php if($cat1=="") { ?> display:none; <?php } ?> ">Keyword<br/>
       <input type="text" name="search_item" value="<?php echo $search_item; ?>" /></td>
     <td>&nbsp;<br/>
       <input type="submit" value="Submit" name="submit" /></td>
   </tr>
 </table></td>
 </tr>
</table></form>


<?php
if(isset($_POST["submit"]))
{
	?>
<div style="width:150px; margin:0 auto; float:right">
<div align="center" style="width:50px; float:left;"><a href='#' onclick='printFilming("toursPrintDiv"); return false;'><img src='images/print.jpg' alt='Print'/></a></div>
<div align="center" style="width:50px; float:left;"><a href="export_user_old.php?from_date=<?php echo $from_date; ?>&to_date=<?php echo $to_date; ?>&cat=<?php echo $cat1; ?>&user=<?php echo $user_name; ?>&group_category=<?php echo $cat; ?>&search_item=<?php echo $search_item; ?>&guide=<?php echo $guide_id; ?>"><img src="images/excel_icon.jpg" border="0" style="width:30px" align="middle"></a></div>
</div>     
<table align="center" class="menu_list_table" style="margin-top:15px;" width="97%">
<?php if($cat1=="booking"){?>
<tr  class="nodrag"><th>No.</th><th>Organization Name</th><th>Total No. of Bookings</th><th>Total No. of Visitors</th></tr>
<?php }else{ ?>
<tr  class="nodrag"><th>Enquiry date</th><th>Visit Time</th><th>Date of visit</th><th>Reference No.</th><th>Company Username</th><th>Organization Name</th><th>Country</th><th>Contact name </th><th>Group size</th><th>Attendance Time</th><th>Accept/Reject</th><th>status</th><th>Type of Booking</th><th>Category</th><th>Type of Visit</th><th>Mobile No.</th><th>Email Address</th><th>Type Of Enquiry</th></tr>

<?php } ?>
<tr>
<?php

 if($cat!="")
 { 
      if($cat=="All")
	  {
		   $where = "(`group_cat` is not null or `group_cat` <> '')";
		   if($_POST["search_item"]!="")
	   {
		    $where .=  " and (`group` like '%$search_item%' or `name` like '%$search_item%' or `phone`like '%$search_item%' or `email`like '%$search_item%' or `contact_phone` like '%$search_item%' or `contact_name` like '%$search_item%' or `group_cat` like '%$search_item%'  or `type_visit` like '%$search_item%'  or `needs` like '%$search_item%' or `edited` like '%$search_item%' or `reference` like '%$search_item%'  or `emirate` like '%$search_item%' or `language`like '%$search_item%' or `purpose` like '%$search_item%' or `country` like '%$search_item%')";
	   }
	  }
	  else
	  {
	  $where = "group_cat='".$cat ."'";
	   if($_POST["search_item"]!="")
	   {
		    $where .=  " and (`group` like '%$search_item%' or `name` like '%$search_item%' or `phone`like '%$search_item%' or `email`like '%$search_item%' or `contact_phone`like '%$search_item%' or `contact_name`like '%$search_item%' or `type_visit`like '%$search_item%'  or `needs`like '%$search_item%' or `edited` like '%$search_item%' or `reference` like '%$search_item%'  or `emirate`like '%$search_item%' or `language`like '%$search_item%' or `purpose` like '%$search_item%' or `country` like '%$search_item%' )";
	   }
	  }
	  if($_POST["from_date"]!="" &&  $_POST["to_date"]!="")
	   {
		 $where .=  " and  (`purposed_date` BETWEEN '$fromdate' AND '$todate')";
	   }
	  
	  
 }
 if($cat1=="Enquiry")
 {
	 if($_POST["from_date"]!="" &&  $_POST["to_date"]!="")
	   {
		 $where=  "(`submitted_date` BETWEEN '$fromdate' AND '$todate')";
	   }
	  
	   if($_POST["search_item"]!="")
	   {
		    $where .=  " or (`group` like '%$search_item%' or `name` like '%$search_item%' or `phone`like '%$search_item%' or `email` like '%$search_item%' or `contact_phone` like '%$search_item%' or `contact_name`like '%$search_item%' or `group_cat` like '%$search_item%'  or `type_visit` like '%$search_item%'  or `needs` like '%$search_item%' or `edited` like '%$search_item%' or `reference` like '%$search_item%'  or `emirate` like '%$search_item%' or `language` like '%$search_item%' or `purpose` like '%$search_item%' or `country` like '%$search_item%'  )";
	   }
 }
  if($cat1=="visit")
 {
	  if($_POST["from_date"]!="" &&  $_POST["to_date"]!="")
	   {
		 $where=  "(`purposed_date` BETWEEN '$fromdate' AND '$todate')";
	   }
	   
	   if($_POST["search_item"]!="")
	   {
		   $where .=  " or (`group` like '%$search_item%' or `name` like '%$search_item%' or `phone`like '%$search_item%' or `email` like '%$search_item%' or `contact_phone` like '%$search_item%' or `contact_name` like '%$search_item%' or `group_cat` like '%$search_item%'  or `type_visit` like '%$search_item%'  or `needs` like '%$search_item%' or `edited` like '%$search_item%' or `reference` like '%$search_item%'  or `emirate` like '%$search_item%' or `language` like '%$search_item%' or `purpose` like '%$search_item%' or `country` like '%$search_item%' )";
	   }
 }
  if($cat1=="Name")
 {
	  
	  $where = "name like '$search_item%' ";
	   if($_POST["from_date"]!="" &&  $_POST["to_date"]!="")
	   {
		 $where .=  "and (`purposed_date` BETWEEN '$fromdate' AND '$todate')";
	   }
	   
	   if($_POST["search_item"]!="")
	   {
		    $where .=  " or (`group` like '%$search_item%' or `name` like '%$search_item%' or `phone` like '%$search_item%' or `email` like '%$search_item%' or `contact_phone` like '%$search_item%' or `contact_name` like '%$search_item%' or `group_cat` like '%$search_item%'  or `type_visit`like '%$search_item%'  or `needs` like '%$search_item%' or `edited` like '%$search_item%' or `reference` like '%$search_item%'  or `emirate` like '%$search_item%' or `language` like '%$search_item%' or `purpose` like '%$search_item%' or `country` like '%$search_item%' )";
	   }
 }
  if($cat1=="booking")
 {
	  
	  $booking_sql="SELECT `group`,count(*)as total_bookings,sum(size) as total_visitors FROM tours WHERE (`purposed_date` BETWEEN '$fromdate' AND '$todate' and `group` like '%$search_item%')  group by `group`";
	  
 }
   if($cat1=="Group")
 {
	  
	  $where = "(`group` like '%$search_item%') ";
	   if($_POST["from_date"]!="" &&  $_POST["to_date"]!="")
	   {
		 $where .=  " and (`purposed_date` BETWEEN '$fromdate' AND '$todate')";
	   }
	  	 
	   if($_POST["search_item"]!="")
	   {
		    $where .=  " or (`name` like '%$search_item%' or `phone`like '%$search_item%' or `email` like '%$search_item%' or `contact_phone` like '%$search_item%' or `contact_name` like '%$search_item%' or `group_cat` like '%$search_item%'  or `type_visit` like '%$search_item%'  or `needs`like '%$search_item%' or `edited` like '%$search_item%' or `reference` like '%$search_item%'  or `emirate`like '%$search_item%' or `language`like '%$search_item%' or `purpose` like '%$search_item%' or `country` like '%$search_item%' )";
	   }
 }
    if($cat1=="Email")
 {
	  
	  $where = "`email` like '%$search_item%' ";
	   if($_POST["from_date"]!="" &&  $_POST["to_date"]!="")
	   {
		 $where .=  "or (`purposed_date` BETWEEN '$fromdate' AND '$todate')";
	   }
 }
  if($cat1=="guide")
 {
	   if($guide_id!="")
	   {
	   $where = "((`guide_id` = '$guide_id' || `c_id`='$guide_id') and (`e_status`='Confirmed' or `capprove`='Confirmed') and (culture_guide_email.g_id=tours.id)) ";
	   }
	   else
	   {
	   $where = "((`guide_id` != '' || `c_id`!='' ) and (`e_status`='Confirmed' or `capprove`='Confirmed')) ";
	   }
	   if($_POST["from_date"]!="" &&  $_POST["to_date"]!="")
	   {
		 $where .=  "and (`purposed_date` BETWEEN '$fromdate' AND '$todate')";
	   }
	   
	   if($_POST["search_item"]!="")
	   {
		  $where .=  " and (`group` like '%$search_item%' or `name` like '%$search_item%' or `phone` like '%$search_item%' or `email` like '%$search_item%' or `contact_phone` like '%$search_item%' or `contact_name` like '%$search_item%' or `group_cat` like '%$search_item%'  or `type_visit`like '%$search_item%'  or `needs` like '%$search_item%' or `edited` like '%$search_item%' or `reference` like '%$search_item%'  or `emirate` like '%$search_item%' or `language` like '%$search_item%' or `purpose` like '%$search_item%' or `country` like '%$search_item%' )";
	   }
 }
 if($cat1=="edited")
 {
	  
	  $where = " `edited` = '$user_name' ";
	   if($_POST["from_date"]!="" &&  $_POST["to_date"]!="")
	   {
		 $where .=  "and (`purposed_date` BETWEEN '$fromdate' AND '$todate')";
	   }
	   
	   if($_POST["search_item"]!="")
	   {
		   $where .=  " or (`group` like '%$search_item%' or `name` like '%$search_item%' or `phone`like '%$search_item%' or `email`like '%$search_item%' or `contact_phone`like '%$search_item%' or `contact_name`like '%$search_item%' or `group_cat` like '%$search_item%'  or `type_visit`like '%$search_item%'  or `needs`like '%$search_item%' or `edited` like '%$search_item%' or `reference` like '%$search_item%'  or `emirate`like '%$search_item%' or `language`like '%$search_item%' or `purpose` like '%$search_item%' or `country` like '%$search_item%' )";
	   }
	   
 }
 if($cat1=="approved")
 {
	  
	  $where = " `approved` = '$user_name' and status='Y' ";
	   if($_POST["from_date"]!="" &&  $_POST["to_date"]!="")
	   {
		 $where .=  "and (`purposed_date` BETWEEN '$fromdate' AND '$todate')";
	   }
	  
	   if($_POST["search_item"]!="")
	   {
		  $where .=  " or (`group` like '%$search_item%' or `name` like '%$search_item%' or `phone` like '%$search_item%' or `email` like '%$search_item%' or `contact_phone` like '%$search_item%' or `contact_name` like '%$search_item%' or `group_cat` like '%$search_item%'  or `type_visit` like '%$search_item%'  or `needs` like '%$search_item%' or `edited` like '%$search_item%' or `reference` like '%$search_item%'  or `emirate` like '%$search_item%' or `language` like '%$search_item%' or `purpose` like '%$search_item%' or `country` like '%$search_item%' )";
	   }
 }
 if($cat1=="disapproved")
 {
	  
	  $where = " `approved` = '$user_name' and status='N' ";
	   if($_POST["from_date"]!="" &&  $_POST["to_date"]!="")
	   {
		 $where .=  "and (`purposed_date` BETWEEN '$fromdate' AND '$todate')";
	   }
	  
	   if($_POST["search_item"]!="")
	   {
		   $where .=  " or (`group` like '%$search_item%' or `name` like '%$search_item%' or `phone` like '%$search_item%' or `email` like '%$search_item%' or `contact_phone` like '%$search_item%' or `contact_name` like '%$search_item%' or `group_cat` like '%$search_item%'  or `type_visit`like '%$search_item%'  or `needs` like '%$search_item%' or `edited` like '%$search_item%' or `reference` like '%$search_item%'  or `emirate` like '%$search_item%' or `language` like '%$search_item%' or `purpose` like '%$search_item%' or `country` like '%$search_item%' )";
	   }
 }
  if($cat1=="fast_track")
 {
	  
	  $where = " enq_type='Fast Track' ";
	   if($_POST["from_date"]!="" &&  $_POST["to_date"]!="")
	   {
		 $where .=  "and (`purposed_date` BETWEEN '$fromdate' AND '$todate')";
	   }
	  
	   if($_POST["search_item"]!="")
	   {
		   $where .=  " or (`group` like '%$search_item%' or `name` like '%$search_item%' or `phone` like '%$search_item%' or `email` like '%$search_item%' or `contact_phone` like '%$search_item%' or `contact_name` like '%$search_item%' or `group_cat` like '%$search_item%'  or `type_visit`like '%$search_item%'  or `needs` like '%$search_item%' or `edited` like '%$search_item%' or `reference` like '%$search_item%'  or `emirate` like '%$search_item%' or `language` like '%$search_item%' or `purpose` like '%$search_item%' or `country` like '%$search_item%' )";
	   }
 }
 
  if($cat1=="status")
 {
	  if($_POST["search_item"]!="")
	   {
		   if(($_POST["search_item"]=="Absent") || $_POST["search_item"]=="absent")
		   {
			    $where = " visit_status IS Null";
		   }
		   else
		   {
		    $where = " visit_status = '$search_item'";
		   }
	   }
	   else
	   {
	  $where = " visit_status!='' ";
	   }
	   if($_POST["from_date"]!="" &&  $_POST["to_date"]!="")
	   {
		 $where .=  " and (`purposed_date` BETWEEN '$fromdate' AND '$todate')";
	   }
	  
	   
 }
 
  if($cat1=="type_of_booking")
 {
	  if($_POST["search_item"]!="")
	   {
		   $where = " enq_type like '%$search_item%'";
	   }
	   else
	   {
	  $where = " enq_type!='' ";
	   }
	   if($_POST["from_date"]!="" &&  $_POST["to_date"]!="")
	   {
		 $where .=  "and (`purposed_date` BETWEEN '$fromdate' AND '$todate')";
	   }
	  
	   
 }
 
 if($cat1=="attendance")
 {
	  if($_POST["search_item"]!="")
	   {
		   $where = " attendance_status like '%$search_item%'";
	   }
	   else
	   {
	  $where = " attendance_status!='' ";
	   }
	   if($_POST["from_date"]!="" &&  $_POST["to_date"]!="")
	   {
		 $where .=  "and (`purposed_date` BETWEEN '$fromdate' AND '$todate')";
	   }
	  
	   
 }
 
  if($cat1=="attendance_time")
 {
	  if($_POST["search_item"]!="")
	   {
		   $where = "arr_time = '$search_item'";
	   }
	   else
	   {
	  $where = " arr_time!='' ";
	   }
	   if($_POST["from_date"]!="" &&  $_POST["to_date"]!="")
	   {
		 $where .=  "and (`purposed_date` BETWEEN '$fromdate' AND '$todate')";
	   }
	  
	  
 }
 
 if($cat1=="guide")
 {
	 $sql_size = "SELECT sum(`size`) as count_size FROM tours,culture_guide_email WHERE  $where order by purposed_date,time";
 }
 else
 {
	 
 $sql_size = "SELECT sum(`size`) as count_size FROM tours WHERE  $where order by purposed_date,time";
 }
 $query_size = mysql_query($sql_size);
 if ($res_size=mysql_fetch_object($query_size))
 {
  $countsize = $res_size->count_size; 
 }
 if($cat1=="guide")
 {
	  $sql_detail = "SELECT * FROM tours,culture_guide_email WHERE  $where order by purposed_date,time ";
 }else if($cat1=="booking"){
	  
	  $sql_detail = $booking_sql;
 }
 
 else
 {
 $sql_detail = "SELECT * FROM tours WHERE  $where order by purposed_date,time ";
 }
 //print_r($sql_detail);
 $query_detail = mysql_query($sql_detail);
 $count = mysql_num_rows($query_detail);
 $x=1;
 if($count>0)
 {
 while ($res1=mysql_fetch_object($query_detail))
 {
  if($x%2==0)
{
$class="#999";
}
else
{
$class="#ddd;";
}
  $count_size = $res1->size; 
    $tourid = $res1->id;
  $user_email="";
  $abaya_received="";

  $abaya_taken="";
  $visit_time="";
  $username=$res1->user_id;
  if($username!=""){
  	$sql_username="select * from site_users where id=$username limit 1";
	$sql_result=mysql_query($sql_username);
	$count_result=mysql_num_rows($sql_result);
	if($count_result==1){
		while($obj = mysql_fetch_object($sql_result)){
			$user_email=$obj->email;
		}
	}
  }
  if($tourid!=""){
  	$sql_attendance="select * from attendance where tour_id=$tourid limit 1";
	$sql_result_attendance=mysql_query($sql_attendance);
	$count_result_attendance=mysql_num_rows($sql_result_attendance);
	if($count_result_attendance==1){
		while($obj = mysql_fetch_object($sql_result_attendance)){
			$visit_time=$obj->arr_time;
		}
	}
  }
  if($tourid!=""){
  	$sql_abaya="select * from abaya where tour_id=$tourid limit 1";
	$sql_result_abaya=mysql_query($sql_abaya);
	$count_result_abaya=mysql_num_rows($sql_result_abaya);
	if($count_result_abaya==1){
		while($obj = mysql_fetch_object($sql_result_abaya)){
			$abaya_received=$obj->abaya_ret;
			$abaya_taken=$obj->abaya_rec;
			
		}
	}
  }
  
  $arr_time=$res1->arr_time;
  $status=$res1->attendance_status;
  $visit_status=$res1->visit_status;
  $enq_type=$res1->enq_type;
  
  $name = $res1->contact_name;
  $phone = $res1->contact_phone;
  $group = $res1->group;
   $country = $res1->country;
  $email = $res1->email;
  $bus = $res1->bus;
  $type_visit = $res1->type_visit;
  $ref = $res1->reference;
   $enq_type = $res1->enq_type;
  $time = $res1->time;
  $date = $res1->purposed_date;
  $date1 = explode("-",$date);
$date = $date1[2]."-".$date1[1]."-".$date1[0];
$res1->submitted_date = explode(" ",$res1->submitted_date);
	$date_visit = explode("-",$res1->submitted_date[0]);
	$enq_date = $date_visit[2] . "/".$date_visit[1] . "/".$date_visit[0];
	$enq_date .= '  '.$res1->submitted_date[1];
	if($visit_status=="")
	{
		$visit_status = "Absent";
	}
  if($res1->group_cat=="Governmentarb")
   {
   $cat = "&#1575;&#1604;&#1607;&#1610;&#1574;&#1575;&#1578; &#1575;&#1604;&#1581;&#1603;&#1608;&#1605;&#1610;&#1577;";
   }else if ($res1->group_cat=="Embassyarb")
   {
   $cat = "&#1575;&#1604;&#1587;&#1601;&#1575;&#1585;&#1575;&#1578;";
   }
   else if ($res1->group_cat=="Travel Industryarb")
   {
   $cat = "&#1602;&#1591;&#1575;&#1593; &#1575;&#1604;&#1587;&#1610;&#1575;&#1581;&#1577;";
   }
   else if ($res1->group_cat=="Educationarb")
   {
   $cat = "&#1575;&#1604;&#1605;&#1572;&#1587;&#1587;&#1575;&#1578; &#1575;&#1604;&#1578;&#1593;&#1604;&#1610;&#1605;&#1610;&#1577;";
   }
   else if ($res1->group_cat=="Otherarb")
   {
   $cat = "&#1571;&#1582;&#1585;&#1609;";
   }
    else if ($res1->group_cat=="General Publicarb")
   {
   $cat = "&#1586;&#1610;&#1575;&#1585;&#1577; &#1593;&#1575;&#1605;&#1607;";
   }
    else if ($res1->group_cat=="Tour Operatorarb")
   {
   $cat = "&#1575;&#1604;&#1588;&#1585;&#1603;&#1575;&#1578; &#1575;&#1604;&#1587;&#1610;&#1575;&#1581;&#1610;&#1577;";
     }
	  else if ($res1->group_cat=="Hotelarb")
   {
   $cat = "&#1575;&#1604;&#1601;&#1606;&#1575;&#1583;&#1602;";
     }
	 else if ($res1->group_cat=="Corporatearb")
   {
   $cat = "&#1575;&#1604;&#1602;&#1591;&#1575;&#1593; &#1575;&#1604;&#1582;&#1575;&#1589;";
     }
   else
   {
   $cat=$res1->group_cat;
   }
   if($enq_type=="Fast Track")
   {
	   $enq_type = "Temporary Pass";
   }
 if($cat1=="booking"){
  	$content.="<tr><td style='background:".$class.";'>".$x ."&nbsp;</td><td style='background:".$class.";'>".$group ."&nbsp;</td><td style='background:".$class.";'>".$res1->total_bookings ."&nbsp;</td><td style='background:".$class.";'>".$res1->total_visitors ."&nbsp;</td></tr>";
  }else{
 
  $content.="<tr><td style='background:".$class.";'>".$enq_date ."&nbsp;</td><td style='background:".$class.";'>".$time ."&nbsp;</td><td style='background:".$class.";'>".$date ."&nbsp;</td><td style='background:".$class.";'>".$ref ."&nbsp;</td><td style='background:".$class.";'>".$user_email ."&nbsp;</td><td style='background:".$class.";'>".$group ."&nbsp;</td><td style='background:".$class.";'>".$country ."&nbsp;</td><td style='background:".$class.";'>".$name ."&nbsp;</td><td style='background:".$class.";'>".$count_size ."&nbsp;</td><td style='background:".$class.";'>".$arr_time ."&nbsp;</td><td style='background:".$class.";'>".$status ."&nbsp;</td><td style='background:".$class.";'>".$visit_status ."&nbsp;</td><td style='background:".$class.";'>".$enq_type ."&nbsp;</td><td style='background:".$class.";'>".$cat ."&nbsp;</td><td style='background:".$class.";'>".$type_visit ."&nbsp;</td><td style='background:".$class.";'>".$phone ."&nbsp;</td><td style='background:".$class.";'>".$email ."&nbsp;</td><td style='background:".$class.";'>".$enq_type ."&nbsp;</td></tr>";
  }
 $x++;
 
 }
 }
echo $content;
?>

</tr> 
	
</table> 
<?php if($cat1!="booking"){ ?>
<div align="center" style="margin-top:10px;"><span style="font-size:16px; font-weight:bold;">Total Visitors=<?php echo $countsize; ?></span></div>
<?php } ?>
<?php
}
?>
</div>
<?
do_footer();
?>