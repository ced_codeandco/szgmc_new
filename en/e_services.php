<?php 
session_start();
?>
<style>
	.input_row_dob #pub_date{
		width: 240px;
	}
</style>
<?php
include './includes/database.php';
include './includes/functions.php';
include './includes/config.php';
include_once('./includes/generic_functions.php');
$conf = new Configuration();
$db = new MyDatabase();
$site_path = $conf->site_url;
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php include 'includes/common_header.php'; ?>
    <title>Sheikh Zayed Grand Mosque Center+</title>
</head>
<body>
    
    <?php include 'includes/menus/banner_header.php'; ?>
    <!-- Banner start -->
    <div class="banner">
	<img src="<?php echo $site_path; ?>images/visiting_the_mosque_banner.jpg">     
    </div>   
    <!-- Banner Close -->
    
    <div class="main_box_content">
            <?php include 'includes/menus/nav_menu.php'; ?>
        <div class="clear"></div>
        <div class="content">
        	<div class="brad_cram">
            	<ul>
            	   <li><a href="<?php echo $site_path; ?>">Home</a></li>
                    <li><a href="#" class="active">E-Services</a></li>
                </ul>
            </div>
      
      
      	<div class="inside_conter">
            <div class="Visiting_box mosquemanner">
                <ul>
                    <li class="fist_tham">
                        <a href="<?php echo $site_path; ?>tour-booking-form">
                            <img src="<?php echo $site_path; ?>images/Visiting-The -osque/group-tour-booking.jpg">
                            <span><img src="<?php echo $site_path; ?>images/Visiting-The -osque/group-tour-booking-icon.png">Group Tour Booking</span>
                        </a>
                    </li>

                    <li>
                        <a href="<?php echo $site_path; ?>careers">
                            <img src="<?php echo $site_path; ?>images/Visiting-The -osque/careers-image.jpg">
                            <span><img src="<?php echo $site_path; ?>images/Visiting-The -osque/career-icon.png">Careers</span>
                        </a>
                    </li>

                    <li>
                        <a href="<?php echo $site_path; ?>media-form">
                            <img src="<?php echo $site_path; ?>images/Visiting-The -osque/filming-permission-image.jpg">
                            <span><img src="<?php echo $site_path; ?>images/Visiting-The -osque/filming-permission-icon.png">Filming Permission</span>
                        </a>
                    </li>

               
                    <li class="fist_tham">
                        <a href="<?php echo $site_path; ?>lost-found">
                            <img src="<?php echo $site_path; ?>images/Visiting-The -osque/lost-and-found-image.jpg">
                            <span><img src="<?php echo $site_path; ?>images/Visiting-The -osque/lost-and-found-icon.png">Lost and found</span>
                        </a>
                    </li>

                    <li>
                        <a href="<?php echo $site_path; ?>suggestion-complaint">
                            <img src="<?php echo $site_path; ?>images/Visiting-The -osque/suggestion_complaints.jpg">
                            <span><img src="<?php echo $site_path; ?>images/Visiting-The -osque/suggestion_complaints.png">Feedback & suggestions</span>
                        </a>
                    </li>

                    <li>
                        <a href="<?php echo $site_path; ?>juniorculturalguide.php">
                            <img src="<?php echo $site_path; ?>images/Visiting-The -osque/jcg-banner.jpg">
                            <span><img src="<?php echo $site_path; ?>images/Visiting-The -osque/jcg-icon.png">Junior Cultural Guide   </span>
                        </a>
                    </li>
               
                    <li class="fist_tham">
                        <a href="<?php echo $site_path; ?>quran_recitation_course">
                            <img src="<?php echo $site_path; ?>images/Visiting-The -osque/quran-recitation-banner.jpg">
                            <span><img src="<?php echo $site_path; ?>images/Visiting-The -osque/quran-recitation-banner-icon.png">Quran Recitation Course</span>
                        </a>
                    </li>
                </ul>



            </div>
        </div>
      
      
     
      
        </div>
     <div class="clear"></div> </div>
    
    
    
    
    
 
	<div class="content_bottom">&nbsp;</div>
	<?php include 'includes/footer.php'; ?> 
	<?php
	
	   if($msg == 'success'){
	  
		 ?>
		<script>
			$(function() {
				$( "#dialog" ).dialog({maxWidth:480,modal:true,width:'90%'});
			});
		</script>
        <script type="text/javascript">
		$(function(){
			$('#confirm').click(function() {
				//$('#dialog').hide();
				$( "#dialog" ).dialog( "close" );
				window.location = "/en/careers";
				return false;
			});
		});
		
        </script>
        <div id="dialog" class="lostpopup">
        	<p style="text-align:center;"><span dir="rtl">لقد تم تقديم طلبك بنجاح ، وسوف نقوم بالتواصل معك قريبا.</span></p>
            <p>Your request has been submitted successfully. We will contact you shortly.</p>
            <form name="feedback_frm" method="post" id="fbk_frm">
                <div>
                   <label for="comments">Kindly provide us your feedback on this e-service.</label>
                   <label for="comments" class="arabic">يرجى تقديم ملاحظاتك عن هذه الخدمة الإلكترونية.</label>
                   <textarea name="comments" cols="50" rows="4"></textarea>
                   <input type="hidden" name="path" value="careers"/>
				    <input type="hidden" name="tour_ref" value="<?php echo $ref;?>"/>
                </div>
                <div align="center" class="clear">
                  <input type="button" style="direction:ltr;" id="submitTour" name="submitTour" class="cmt_btn" value="Submit ﺍرسل" onClick="postFeedback();" />
                  <input type="button" style="direction:ltr;" id="confirm" class="cmt_btn" value="No Comments لا تعليق" />
                </div>
	   		</form>
        </div>
		 <?
		
		}
		else if($msg == 'failed')
		{
		?>
		  <script> alert("Cannot process your request"); </script>
		 <?
		  print('<script type="text/javascript">window.location = "/en/careers";</script>');
exit(); 
		}else if($msg == 'Invalid')
		{
		?>
			<script> alert("Invalid captcha"); </script>
		<?php
			print('<script type="text/javascript">window.location = "/en/carrers.php";</script>');
			exit(); 
		}
	  
	?>
</body>
</html>