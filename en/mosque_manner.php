<?php
include 'includes/database.php';
include 'includes/functions.php';
include 'includes/config.php';

$slug = explode('/',$_SERVER['REQUEST_URI']);
$slug = end($slug);
$conf = new Configuration();
$db = new MyDatabase();

$site_path = $conf->site_url;
$main_menu = $conf->getCurrentMainPage($slug);
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Mosque Manner - SZGMC</title>
    <?php include 'includes/common_header.php'; ?>
</head>
<body>
<?php include 'includes/menus/banner_header.php'; ?>
<!-- Banner start -->
<div class="banner">
    <img src="<?php echo $site_path; ?>images/visiting_the_mosque_banner.jpg">
</div>
<!-- Banner Close -->

<div class="main_box_content">
    <?php include 'includes/menus/nav_menu.php'; ?>
    <div class="clear"></div>
    <div class="content">
        <div class="brad_cram">
            <ul>
                <li><a href="<?php echo $site_path; ?>">Home</a></li>
                <li><a href="<?php echo $site_path; ?>visiting-the-mosque">Plan Your Visit</a></li>
                <li><a href="#" class="active">Mosque manners</a></li>
            </ul>
        </div>
        <div class="content-left">
            <?php // include 'includes/menus/left_menu.php';
            include 'includes/ads/ad_216_240.php';
            ?>
            <?php
            include 'includes/menus/ministry_logos.php';
            include 'includes/menus/left_menu.php';
            ?>
        </div>
        <div class="content-right" style="margin-left:20px">
            <div class="single_middle">
                <h2>Mosque manners</h2>
                <p>Welcome to the Sheikh Zayed Grand Mosque and we hope you have a wonderful time visiting this spectacular monument. As it is a holy place, intended for prayer and worship, we request that you follow the Mosque Manners noted below, which also include instructions concerning photography.</p>
                <!--p>Visitors are kindly requested:</p-->
                <ul style="color: #464646; padding-right: 15px;">
                    <li>Avoid any intimate gestures while posing for photos.</li>
                    <li>Ladies should keep their heads covered at all times.</li>
					<li>Ensure that the dress code is followed at all times.</li>
					<li>Avoid any poses that might offend others.</li>
					<li>Avoid any positions or gestures that imply a specific meaning.</li>
					<li>Avoid photographing other visitors and ensure that photos taken show only the beauty of architecture and personal images.</li>
					<li>Visitors should ensure that the pictures taken do not include other visitors.</li>

                </ul>
                <!--p>Mosques hold a religious and sacred position for all Muslims. Visitors to mosques should therefore abide by specific ethics and rules and show their full respect by following the instructions indicated below:</p>
                <p>Visitors are kindly requested:</p>
                <ul style="color: #464646; padding-right: 15px;">
                    <li>Not to eat on the premises.</li>
                    <li>Not to smoke on the premises.</li>
                    <li>To keep quiet.</li>
                    <li>Not to wear revealing or translucent clothes.</li>
                    <li>Not to wear short pants and skirts.</li>
                    <li>Not to wear sleeveless shirts.</li>
                    <li>Not to wear clothes with indecent illustrations.</li>
                    <li>Not to wear tight clothes or swimming apparel.</li>
                </ul-->
                <div class="mosque">
                    <img class="desktop_view" src="<?php echo $site_path; ?>img/Mosque-Manners.jpg" style="width: 690px;" />
                    <div class="mobile_view manners_div">
                        <span>Allowed<label>مسموح</label></span>
                        <img src="<?php echo $site_path; ?>img/mobile/Mosque-Manners_moba.png" style="width: 690px;" />
                        <span>Not Allowed<label>غير مسموح</label></span>
                        <img src="<?php echo $site_path; ?>img/mobile/Mosque-Manners_mobna.png" style="width: 690px;" />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="clear"></div> </div>
<div class="content_bottom">&nbsp;</div>
<?php include 'includes/footer.php'; ?>
</body>
</html>