<?php
include_once 'inc/conf.php';
include_once 'inc/mysql.lib.php';
include 'includes/config.php';
$conf = new Configuration();
$site_path = $conf->site_url;
$mydb=new connect;

header("Content-Type: text/xml;charset=utf-8");
$main_title="SZGMC";
$main_title=htmlentities($main_title, ENT_QUOTES, 'UTF-8');
echo "<?xml version=\"1.0\" encoding=\"utf-8\" ?>\n";
echo "<rss version=\"2.0\">\n";
echo "<channel>\n";
echo "<title>$main_title</title>\n";
echo "<link>http://www.szgmc.ae</link>\n";
echo "<description>Sheikh Zayed Mosque, Abu Dhabi Mosque, Grand Mosque Abu Dhabi.</description>\n";
echo "<language>Arb</language>\n";
$sql = "select * from news order by news_id desc";
$query = $mydb->query($sql); 
while($res_obj = mysql_fetch_object($query))
{
	                
                    $news_short=($res_obj->news_text);
					$news_short=preg_replace('/\s+?(\S+)?$/', '', substr($news_short, 0, 500));
					$news_short = strip_tags($news_short);
					$news_short=htmlentities($news_short, ENT_QUOTES, 'UTF-8');
					$news_url= $site_path."news-detail/".string_to_filename($res_obj->news_title).'-'.$res_obj->news_id;
					$date = date('d,M Y', strtotime($res_obj->news_date));
						
echo "<item>\n";
echo "<title>".$res_obj->news_title."</title>\n";
if($news_short!='')
echo "<description>".$news_short."</description>\n";
echo "<link>".$news_url."</link>\n";
echo "<pubDate>".$date."</pubDate>\n";
echo "</item>\n";
}
echo  "</channel>\n";
echo  "</rss>\n";
?>