<?php
include 'includes/database.php';
include 'includes/dal/news.php';
include 'includes/functions.php';
include 'includes/config.php';

$slug = explode('/',$_SERVER['REQUEST_URI']);
$slug = end($slug);
$conf = new Configuration();
$db = new MyDatabase();

$site_path = $conf->site_url;

$dal_news = new ManageNews();
$total_news = $dal_news->countNews();
$current_page = isset($_GET['current_page'])?mysql_real_escape_string($_GET['current_page']):1;

$news_per_page = 6;

$starts_with = ($current_page-1) * $news_per_page;

$slug = 'news-list';
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">        
<head>
    <title>News - Page <?php echo $current_page; ?> - SZGMC</title>
	<?php include 'includes/common_header.php'; ?>
    <link href="<?php echo $site_path; ?>css/news.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript">
    $(document).ready(function() {
    });
    </script>
    <style type="text/css">
	div.single_middle {
		/* width: 92% !important;*/
	}
	</style>
</head>
<body>
<?php include 'includes/menus/banner_header.php'; ?>
    <!-- Banner start -->
    <div class="banner">
	<img src="<?php echo $site_path; ?>images/visiting_the_mosque_banner.jpg">     
    </div>   
    <!-- Banner Close -->
    
    <div class="main_box_content">
            <?php include 'includes/menus/nav_menu.php'; ?>
        <div class="clear"></div>
       

        <div class="content">
             <div class="brad_cram">
            	<ul>
            	   <li><a href="<?php echo $site_path; ?>">Home</a></li>
                   <li><a href="javascript:void(0);" class="">Media Corner</a></li>
                    <li><a href="#" class="active">News</a></li>
                </ul>
            </div>
            <div class="content">
        	<div class="content-left">
                <?php // include 'includes/menus/left_menu.php'; 
				include 'includes/ads/ad_216_240.php';
				?>
                <?php 
				include 'includes/menus/ministry_logos.php';
                include 'includes/menus/left_menu.php';
				?>
				 <?php  
	 	include 'includes/news_letter.php'; 
	 ?>
            </div>
                <div class="content-right" style="margin-left:10px">
				<div class="single_middle" >
			<?php include 'includes/menus/marquee.php';?>
                <h2>News</h2>
                <?php
					$first = false;
					$bk_class = '';
					$news_list = $dal_news->getNews('', $news_per_page, $starts_with);
					//print_r($news_list);
					foreach($news_list as $news) {
					$news_short=strip_tags($news->news_text);
					$news_short=substr($news_short,0,260);
					$news_short=substr($news_short,0,strripos($news_short,' '));
					$news_short=UTF8ToHTML($news_short);
					$news_url= $site_path."news-detail/".string_to_filename($news->news_title).'-'.$news->news_id;
					$bk_class = $first ? 'first' : 'second';
					$first = !$first;
				  ?>
                <div class="news_story <?php echo $bk_class; ?>">
				<?php
							if($news->image!="")
							{
							?>
                	<a href="<?php echo $news_url; ?>"><img src="<?php echo $site_path; ?>news_images/<?php echo $news->image; ?>" height="111" width="111" />
                    </a>
					<?php
					}
					else
					{
					?>
					<a href="<?php echo $news_url; ?>"><img src="<?php echo $site_path; ?>images/News_english.jpg" height="111" width="111" />
                    </a>
					<?php
						}
					?>
                    <div class="date"><?php echo date('d M Y', strtotime($news->news_date)); ?></div>
                    <h3><a href="<?php echo $news_url; ?>"><?php echo $news->news_title; ?></a></h3>
                	<p><?php echo $news_short; ?></p>
                    <div class="read_more"><a href="<?php echo $news_url; ?>">Read more </a>&gt;&gt;</div>
                </div>
                <br class="clear" />
                <?php } ?>          
            </div>
			
				</div>
				</div>
				</div>
		 <div class="clear"></div>
		 
    <div id="news_controller">
                	<span class="<?php echo ($current_page>1)?'previous_active':'previous_inactive'; ?>">
					<?php if($current_page>1) { ?>
                    	<a href="<?php echo $site_path; ?>news-list/<?php echo $current_page-1; ?>" >Previous</a>
                    <?php } else { echo 'Previous'; } ?>
                    </span>
                    
					
					<?php 
					$last_page = $total_news % $news_per_page == 0 ? $total_news / $news_per_page : $total_news / $news_per_page + 1;
					if($last_page > 7) {
					if($current_page == 1){						
						$displayArray = array(1,2,3,4,5,6,7);						
					}else if($current_page == 2){
						$displayArray = array(1,2,3,4,5,6,7);
					}else if($current_page == 3){
						$displayArray = array(1,2,3,4,5,6,7);
					}else {
						$displayArray = array($current_page,$current_page-1,$current_page-2,$current_page-3,$current_page+1,$current_page+2,$current_page+3);
					}
					}
					for($i=1;$i<=($last_page);$i++) {
					?>
                	<span <?php  if($last_page > 7) { if(!in_array($i,$displayArray)){echo 'style="display:none;"';} }?> <?php echo $i == $current_page ? 'class="current"' : ''; ?>><a href="<?php echo $site_path; ?>news-list/<?php echo $i; ?>" ><?php echo $i; ?></a></span>
                    <?php } ?>
                    <span class="<?php echo ($current_page<($total_news/$news_per_page)+1)?'next_active':'next_inactive'; ?>">

					<?php if($current_page<($total_news/$news_per_page)) { ?>
                    	<a href="<?php echo $site_path; ?>news-list/<?php echo $current_page+1; ?>" >Next</a>
                    <?php } else { echo 'Next'; } ?>
                    </span>
                </div></div>
			
				 <br class="clear" />
				 
    
	<div class="content_bottom">&nbsp;</div>
	<?php include 'includes/footer.php'; ?> 
</body>
</html>