<?php
function getIp(){
	
	$ip = $_SERVER['REMOTE_ADDR'].uniqid();
	if (!isset($_COOKIE['userId'])) {
		
		setcookie(
              "userId", $ip, time() + (10 * 365 * 24 * 60 * 60), '/'
      );
	}
	else{
		
		$ip = $_COOKIE['userId'];
	}
	return $ip;
}

session_start();
include './includes/database.php';
include './includes/functions.php';
include './includes/config.php';
include_once('./includes/generic_functions.php');
$conf = new Configuration();
$db = new MyDatabase();
$site_path = $conf->site_url;

$slug = explode('/',$_SERVER['REQUEST_URI']);
$slug = end($slug);
if ($_REQUEST['action'] == "vote"){
	    $poll_id= $_REQUEST['poll_id'];
		$ip= getIp();
	    $pollAnswerID=$_REQUEST['pollAnswerID'.$poll_id];
		 $query  = "UPDATE pollanswers SET pollanswerPoints = pollAnswerPoints + 1 WHERE pollAnswerID = ".$pollAnswerID."";
		mysql_query($query) or die('Error, insert query failed');
		
		$query="INSERT INTO poll_tracking(`pollid`,`ip`) VALUES ('$poll_id','$ip')";
		mysql_query($query) or die('Error, insert query failed');
		header("location:/en/poll-archive");
		
	}
$current_page = isset($_GET['current_page'])?mysql_real_escape_string($_GET['current_page']):1;

$polls_per_page = 6;

$starts_with = ($current_page-1) * $polls_per_page;
$offset =$starts_with;
$limit = $polls_per_page;
 $query_id1  = "SELECT * FROM polls where pollStatus='0' order by pollID desc ";
$result_id1 = mysql_query($query_id1);
$total_polls = mysql_num_rows($result_id1);
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php include 'includes/common_header.php'; ?>
    <title>Poll Results</title>
    <style>
	#news_controller{ width:500px; }
	.pollElemWrap{
		min-height: 356px;
		background: #F7F8FA;
		border: 1px solid #E5E5E5;
		width: 697px;
		padding: 13px 0px 20px 15px;
		margin-top: 13px;
	}

	</style>
	<link href="<?php echo $site_path; ?>css/news.css" rel="stylesheet" type="text/css" />

</head>
<body>

<?php include 'includes/menus/banner_header.php'; ?>
    <!-- Banner start -->
    <div class="banner">
	<img src="<?php echo $site_path; ?>images/visiting_the_mosque_banner.jpg">     
    </div>   
    <!-- Banner Close -->
    
    <div class="main_box_content" style="height:2300px;" >
            <?php include 'includes/menus/nav_menu.php'; ?>
        <div class="clear"></div>
       

        <div class="content">
             <div class="brad_cram">
            	<ul>
            	   <li><a href="<?php echo $site_path; ?>">Home</a></li>
                   <li><a href="javascript:void(0);" class="">Open Data</a></li>
                    <li><a href="#" class="active">Poll Results</a></li>
                </ul>
            </div>
            <div class="content">
        	<div class="content-left">
                <?php // include 'includes/menus/left_menu.php'; 
				include 'includes/ads/ad_216_240.php';
				?>
                <?php 
				include 'includes/menus/ministry_logos.php';
                include 'includes/menus/left_menu.php';
				?>
            </div>
                <div class="content-right" style="margin-left:30px">
				<div class="page_item_single">
                    	<div class="page_content" >
                            <div class="general_body_content">
							<h2>Poll Results</h2>
								<div style="width:690px">
									<div id="pollWrapper">
									
									
									
									
									<?php 
									include("./includes/poll_archive.php");
									
									$query_id  = "SELECT * FROM polls where pollStatus='0' order by pollID desc limit $offset, $limit ";
									$result_id = mysql_query($query_id);
									$counter = 0;//var for wrapping the every three elements in a div so that div might be given a backgruond color.
									while($row_id = mysql_fetch_array($result_id))
									{
										
										if($counter % 3 == 0){
											echo '<div class="pollElemWrap">';
										}
											$poll_id = $row_id['pollID'];
											echo "<div class='poll_archive'>";
											getpoll_archive($poll_id);
											echo "</div>";
										if($counter % 3 == 2 ){
											echo '<div class="clear"></div>';
											echo '</div>';
										}
										$counter++;
									}
									
									if($counter % 3 != 2){
											echo '<div class="clear"></div>';
											echo '</div>';
									}
									
									?>
									</div>
									
									<div class="clear">&nbsp;</div>
									
									<div id="news_controller">
										<span class="<?php echo ($current_page>1)?'previous_active':'previous_inactive'; ?>">
										<?php if($current_page>1) { ?>
											<a href="<?php echo $site_path; ?>poll-archive/<?php echo $current_page-1; ?>" >Previous</a>
										<?php } else { echo 'Previous'; } ?>
										</span>
										
										
										<?php 
										$last_page = $total_polls % $polls_per_page == 0 ? $total_polls / $polls_per_page : $total_polls / $polls_per_page + 1;
										for($i=1;$i<=($last_page);$i++) {
										?>
										<span <?php echo $i == $current_page ? 'class="current"' : ''; ?>><a href="<?php echo $site_path; ?>poll-archive/<?php echo $i; ?>" ><?php echo $i; ?></a></span>
										<?php } ?>
										<span class="<?php echo ($current_page<($total_polls/$polls_per_page)+1)?'next_active':'next_inactive'; ?>">

										<?php if($current_page<($total_polls/$polls_per_page)) { ?>
											<a href="<?php echo $site_path; ?>poll-archive/<?php echo $current_page+1; ?>" >Next</a>
										<?php } else { echo 'Next'; } ?>
										</span>
									</div>
								</div>
							</div>
                            <br class="clear" />
                            
                        </div>
                    </div>
		</div>
		</div>
		
		<div class="clear"></div></div>
    
	<div class="content_bottom">&nbsp;</div>
	<?php include 'includes/footer.php'; ?> 
	

</body>
</html>