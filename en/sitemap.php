<?php
include 'includes/database.php';
include 'includes/functions.php';
include 'includes/config.php';

$slug = explode('/',$_SERVER['REQUEST_URI']);
$slug = end($slug);
$conf = new Configuration();
$db = new MyDatabase();

$site_path = $conf->site_url;

$main_menu = $conf->getCurrentMainPage($slug);

$conf->site_description = 'What others like King Fahad, Queen Elizbeth, Jimmy Carter and other prominent leaders say about Sheikh Zayed. Find all about the Sheikh Zayed Grand Mosque in Abu Dhabi including, visiting timings, how to get to the mosque, dress code, tours, history, architecture and more.';

$conf->site_keywords = 'grand mosque, sheikh zyed grand mosque, mosque in adu dhabi, what to wear in mosque, how to get to mosque, history of grand mosque, architecture of grand mosque, grand mosque photos';

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
        
<head>
     <title>Site Map</title>
       <?php include 'includes/common_header.php'; ?>
    <link href="<?php echo $site_path; ?>css/sitemap.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<?php echo $site_path; ?>js/jquery-ui-1.8.21.custom.min.js"></script>
    <script type="text/javascript">
		$(document).ready(function() {
			//$("#accordionGiftLelo").msAccordion({vertical:true});
			$( "#accordionGiftLelo" ).accordion({ autoHeight: false });
		});
	</script>
<style>
div.single_middle div.saidby {
border-right: #ccc solid 1px;
border-left: #ccc solid 1px;
border-bottom: #ccc solid 1px;

background: #fff; 

}
div.single_middle div.saidby ul li{color:#464646; padding:8px; list-style:inside;}
div.single_middle div.saidby ul li a{color:#464646;  text-decoration:none;}
</style>
</head>
<body>
    <?php include 'includes/menus/banner_header.php'; ?>
    <div class="banner">
	<img src="<?php echo $site_path; ?>images/visiting_the_mosque_banner.jpg">     
    </div>
   
    <!-- Banner Close -->
<!-- Content Start -->
    
    
    <div class="main_box_content">
        <?php include 'includes/menus/nav_menu.php'; ?>
        <div class="clear"></div>
        <div class="content">
            <div class="brad_cram">
                <ul>
                   <li><a href="<?php echo $site_path; ?>">Home</a></li>
                    <li><a href="#" class="active">Sitemap</a></li>
                </ul>
            </div>
            <div class="content">
                <div class="content-left">

                <?php
                include 'includes/ads/ad_216_240.php';
                include 'includes/menus/ministry_logos.php';
                include 'includes/menus/left_menu.php';?>

                </div>
                <div class="content-right" style="margin-left:30px">
                    <div class="single_middle">
                        <div id="accordionGiftLelo">


                            <h3><a href="<?php echo $site_path; ?>about-szgmc">About SZGMC</a></h3>
                            <div class="saidby">
                                <ul>
                                    <li><a href="<?php echo $site_path; ?>minister-hh">Speech of H.H. Sheikh Mansoor Bin Zayed</a></li>
                                    <li><a href="<?php echo $site_path; ?>chairmans-message">Chairman's  Message</a></li>
                                    <li><a href="<?php echo $site_path; ?>DG-message">DG's  Message</a></li>
                                    <!--<li><a href="<?php echo $site_path; ?>board-members">Board  Members</a></li>-->
                                    <li><a href="<?php echo $site_path; ?>organizational-structure">Organizational Structure</a></li>
                                    <li><a href="<?php echo $site_path; ?>vision-mission-values">Vision, Mission & Values</a></li>
                                    <li><a href="<?php echo $site_path; ?>sheikh-zayed-and-grand-mosque">Sheikh Zayed and the Grand Mosque</a></li>
                                    <li><a href="<?php echo $site_path; ?>theory-and-implementation">Phases of Construction</a></li>
                                </ul>
                            </div>

                                                        <h3><a href="<?php echo $site_path; ?>visiting-the-mosque">Visiting The Mosque – Prayers</a></h3>
							<div class="saidby">
							<ul>
								<li><a href="<?php echo $site_path; ?>visitor-services">Services</a></li>
								<li><a href="<?php echo $site_path; ?>mosque-opening-hours">Prayer Timings</a></li>
								<li><a href="<?php echo $site_path; ?>friday-sermon">Friday Sermon</a></li>
								<li><a href="<?php echo $site_path; ?>getting-to-the-mosque">Getting To The Mosque</a></li>
								<li><a href="<?php echo $site_path; ?>religious-programs">Religious Programs</a></li>
							</ul>
							</div>
							
							<h3><a href="<?php echo $site_path; ?>visiting-the-mosque">Visiting The Mosque – Visitors</a></h3>
							<div class="saidby">
							<ul>
								<li><a href="<?php echo $site_path; ?>mosque-manner">Mosque Manners</a></li>
								<li><a href="<?php echo $site_path; ?>mosque-opening-hours">Visiting Times</a></li>
								<li><a href="<?php echo $site_path; ?>cultural-tours">Cultural Tours</a></li>
								<li><a href="<?php echo $site_path; ?>electronic-services">eguide</a></li>
								<li><a href="<?php echo $site_path; ?>getting-to-the-mosque">Getting To The Mosque</a></li>
								<li><a href="https://www.google.com/maps/views/streetview/sheikh-zayed-grand-mosque?gl=us" target="_blank">Google Virtual Tour</a></li>
							</ul>
							</div>	
							
							<h3><a href="<?php echo $site_path; ?>visiting-the-mosque">Visiting The Mosque – – Delegates and Tour Operators</a></h3>
							<div class="saidby">
							<ul>
								<li><a href="<?php echo $site_path; ?>mosque-manner">Mosque Manners</a></li>
								<li><a href="<?php echo $site_path; ?>mosque-opening-hours">Visiting Times</a></li>
								<li><a href="<?php echo $site_path; ?>getting-to-the-mosque">Getting To The Mosque</a></li>
								<li><a href="https://www.google.com/maps/views/streetview/sheikh-zayed-grand-mosque?gl=us" target="_blank">Virtual Tour</a></li>
								<li><a href="<?php echo $site_path; ?>faqs">FAQs</a></li>
								<li><a href="<?php echo $site_path; ?>services">Services</a></li>
								<li><a href="<?php echo $site_path; ?>tour-booking-form">Book Your Tour</a></li>
							</ul>
							</div>

                            <!--h3><a href="<?php echo $site_path; ?>visiting-the-mosque">Plan Your Visit</a></h3>
                           <ul>
                             <li><a href="<?php echo $site_path; ?>mosque-manner">Mosque Manners</a></li>
                             <li><a href="<?php echo $site_path; ?>mosque-opening-hours">Visiting Times</a></li>
                             <li><a href="<?php echo $site_path; ?>visitor-services">Visitor Services</a></li>
                             <li><a href="<?php echo $site_path; ?>electronic-services">e-guide</a></li>
                             <li><a href="<?php echo $site_path; ?>video-gallery">What is Tour</a></li>
                             <li><a href="<?php echo $site_path; ?>tour-booking-form">Book Your Tour</a></li>
                             <li><a href="<?php echo $site_path; ?>getting-to-the-mosque" >Getting Here</a></li>
                             <li><a href="https://www.google.com/maps/views/streetview/sheikh-zayed-grand-mosque?gl=us" target="_blank">Google Virtual Tour</a></li>
                             <li><a href="http://www.tripadvisor.com/Attraction_Review-g294013-d1492221-Reviews-Sheikh_Zayed_Grand_Mosque_Center-Abu_Dhabi_Emirate_of_Abu_Dhabi.html" target="_blank">Rate Us on Trip Advisor</a></li>
                             <li><a href="<?php echo $site_path; ?>questions">FAQs</a></li>
                             <li><a href="<?php echo $site_path; ?>important-information">Important Information</a></li>
                             <li><a href="<?php echo $site_path; ?>esurvey">eSurvey</a></li>
                           </ul-->

                            <h3><a href="<?php echo $site_path; ?>e_services.php">E-Services</a></h3>
                            <div class="saidby">
                                <ul>
                                     <li><a href="<?php echo $site_path; ?>tour-booking-form">Group Tour Booking</a></li>
                                     <li><a href="<?php echo $site_path; ?>careers">Careers</a></li>
                                     <li><a href="<?php echo $site_path; ?>media-form">Filming Permission</a></li>
                                     <li><a href="<?php echo $site_path; ?>lost-found">Lost and found</a></li>
                                     <li><a href="<?php echo $site_path; ?>suggestion-complaint">Feedback & suggestions</a></li>
                                     <!--li><a href="<?php echo $site_path; ?>juniorculturalguide.php">Junior Cultural Guide Program</a></li-->
                                      <!--<li><a href="<?php /*echo $site_path; */?>log_book.php">Daily Log Book </a></li>-->
                                 </ul>
                            </div>

                            <h3><a href="javascript:void(0);">Media Corner</a></h3>
                            <div class="saidby">
                                <ul>
                                     <li><a href="<?php echo $site_path; ?>news-list">News</a></li>
                                     <li><a href="<?php echo $site_path; ?>press-kit">Press Kit</a></li>
                                     <li><a href="<?php echo $site_path; ?>eparticipation">E-Participation</a></li>
                                 </ul>
                            </div>

                            </li>
                            <h3><a href="javascript:void(0);">Library</a></h3>
                            <div class="saidby">
                               <ul>
                                     <li><a href="<?php echo $site_path; ?>about-the-library">About the library</a></li>
                                     <li><a href="<?php echo $site_path; ?>library-resources">Resources</a></li>
                                     <li><a href="<?php echo $site_path; ?>library-services">Library Services</a></li>
                                     <li><a href="<?php echo $site_path; ?>search-library">Search</a></li>
                                     <li><a href="<?php echo $site_path; ?>publications">Publications</a></li>
                                 </ul>
                            </div>


                            <h3><a href="<?php echo $site_path; ?>">Events & Activities</a></li></h3>
                            <div class="saidby">
                            	<ul>
                                      <li><a href="<?php echo $site_path; ?>exhibitions">Exhibitions</a></li>
                                      <li><a href="<?php echo $site_path; ?>activities">Activities</a></li>
                                      <li><a href="<?php echo $site_path; ?>social-initiatives">Social Initiatives</a></li>
                                      <li><a href="<?php echo $site_path; ?>spaces-of-light">Spaces of Light</a></li>
                                  </ul>
                            </div>
                            
                            <h3><a href="<?php echo $site_path; ?>">Religious Programs</a></li></h3>
                            <div class="saidby">
                            	<ul>
                                    <li><a href="<?php echo $site_path; ?>religious-courses">Religious Courses</a></li>
                                    <li><a href="<?php echo $site_path; ?>zikr-al-hakeem">Al Thikr Al Hakeem</a></li>
                                    <!--li><a href="<?php echo $site_path; ?>zikr-al-hakeem">Zikr Al Hakeem</a></li-->
                                    <li><a href="<?php echo $site_path; ?>friday-sermon">Friday Sermon</a></li>
                                    <li><a href="<?php echo $site_path; ?>ramadan-activities">Ramadan Activities</a></li>
                                    <!--li><a href="<?php echo $site_path; ?>prayer-information">Prayer Information</a></li-->
                                </ul>
                            </div>
                            
                            
                            
                            <h3><a href="<?php echo $site_path; ?>suggestion-complaint" onclick="var win = window.open('http://szgmc2016.codeandcode.co/en/suggestion-complaint', '_blank');win.focus();">Suggestions</a></li></h3>
                            <div class="saidby"></div>
                            <h3><a href="<?php echo $site_path; ?>architecture">Architecture</a></h3>
                            <div class="saidby">
                               <ul>
                                 <li><a href="<?php echo $site_path; ?>general-architecture">General Architecture</a></li>
                                 <li><a href="<?php echo $site_path; ?>domes">Domes</a></li>
                                 <li><a href="<?php echo $site_path; ?>marbles">Marble</a></li>
                                 <li><a href="<?php echo $site_path; ?>lunar-illumination">Lunar Illumination</a></li>
                                 <li><a href="<?php echo $site_path; ?>carpets">Carpets</a></li>
                                 <li><a href="<?php echo $site_path; ?>chandeliers">Chandeliers</a></li>
                                 <li><a href="<?php echo $site_path; ?>pulpit">Pulpit&nbsp;(Menbar)</a></li>
                    
                                 <li><a href="<?php echo $site_path; ?>minaret">Minaret </a></li>
                                 <li><a href="<?php echo $site_path; ?>reflective-pools"> Reflective Pools</a></li>
                                 <li><a href="<?php echo $site_path; ?>columns">Columns </a></li>
                                 <li><a href="<?php echo $site_path; ?>mihrab">Mihrab </a></li>
                                 <li><a href="<?php echo $site_path; ?>sahan">The Sahan</a></li>
                                 </ul>
                            </div>

                            <h3><a href="javascript:void(0);" >Open Data</a></h3>
                            <div class="saidby">
                                <ul>
                                    <li><a href="<?php echo $site_path; ?>poll-archive" style="width:116px;">Poll Results</a></li>
                                    <li><a href="<?php echo $site_path; ?>open-data"  style="width:116px;">Statistics</a></li>
                                </ul>
                            </div>

                            <h3><a href="https://webmail.szgmc.ae/" target="_blank" onclick="var win = window.open('https://webmail.szgmc.ae/', '_blank');win.focus();")"> Staff Only</a></h3>
                            <div class="saidby" style="margin:0; padding:0;"> </div>

                            <h3><a href="https://webmail.szgmc.ae/" target="_blank" onclick="var win = window.open('http://szgmc2016.codeandcode.co/en/contact-us', '_self');win.focus();")">Contact Us</a></h3>
                            <div class="saidby" style="margin:0; padding:0;"> </div>

                            <h3><a href="https://webmail.szgmc.ae/" target="_blank" onclick="var win = window.open('http://szgmc2016.codeandcode.co/en/help', '_self');win.focus();")">Help</a></h3>
                            <div class="saidby" style="margin:0; padding:0;"> </div>

                            <h3><a href="https://webmail.szgmc.ae/" target="_blank" onclick="var win = window.open('http://szgmc2016.codeandcode.co/en/gallery.php', '_self');win.focus();")">Photo Album</a></h3>
                            <div class="saidby" style="margin:0; padding:0;"> </div>

                            <h3><a href="https://webmail.szgmc.ae/" target="_blank" onclick="var win = window.open('http://szgmc2016.codeandcode.co/en/videos_album.php', '_self');win.focus();")">Video Gallery</a></h3>
                            <div class="saidby" style="margin:0; padding:0;"> </div>
                        </div>
                </div>
            </div>
            </div>
        <div class="clear"></div></div>
    </div>              
     <div class="clear"></div>  
<!-- Content Close -->

    <div class="content_bottom">&nbsp;</div>
	<?php include 'includes/footer.php'; ?> 
</body>
</html>