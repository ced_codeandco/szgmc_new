<?php
include './includes/database.php';
include './includes/functions.php';
include './includes/config.php';
include_once('./includes/generic_functions.php');
function generatePassword($length = 8) {
	
    $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    $count = strlen($chars);


    for ($i = 0, $result = ''; $i < $length; $i++) {
        $index = rand(0, $count - 1);
        $result .= substr($chars, $index, 1);
    }
    return $result;
}
$conf = new Configuration();
$db = new MyDatabase();
$site_path = $conf->site_url;

$slug = explode('/',$_SERVER['REQUEST_URI']);
$slug = end($slug);
$msg="";
$current_page = 'ftplogin';
session_start();

if (!empty($_REQUEST['captcha'])) {
    if (empty($_SESSION['captcha']) || trim(strtolower($_REQUEST['captcha'])) != $_SESSION['captcha']) {
        if(isset($_REQUEST['mode']) && $_REQUEST['mode'] == "edit")
            $msg="Invalid Action";
        else
            $msg ="Invalid";
    }else{
        if(isset($_POST['a_fname']) && $_POST['a_fname']!='' && isset($_POST['submitRegistration']) ){

            foreach($_POST as $name => $value) {
                $$name=mysql_real_escape_string($value);
            }
            $a_op_phoneno = '971'.'-'.$a_op_city_code.'-'.$a_op_phoneno_new;
            $_SESSION['user_phoneno'] = $a_op_phoneno;
            $a_phoneno = '971'.'-'.$a_op_city_code2.'-'.$a_op_phoneno_new2;
            $countryCode=$_POST["countryCode"];
            $areaCode=$_POST["areacode"];
            $countryCode = $countryCode .$areaCode;
            $op_countryCode=$_POST["countryCode"];
            $a_op_areacode=$_POST["a_op_areacode"];
            $op_countryCode = $op_countryCode .$a_op_areacode;

            if(isset($_REQUEST['mode']) && $_REQUEST['mode'] == "edit"){
                if((isset($_SESSION['user_id'])) && $_SESSION['user_id']!=''){
                    $user_id=$_SESSION['user_id'];
                    $sql="update site_users set name='$a_fname',phoneno='$a_phoneno',countryCode='$countryCode',gm_email='$a_email_gm',op_fname='$a_op_fname',op_phoneno='$a_op_phoneno',op_countryCode='$op_countryCode',op_mobile='$a_op_mobile',op_email='$a_email_op' where id='$user_id' and active=1 limit 1";
                    mysql_query($sql);
                    $msg='updated';
                    $signatory_file_name = '';
                    $pro_card_file_name = '';
                    $trade_license_file_name = '';
                    if($_POST['signatory_fname_full']!="") {
                        $signatory_file_name = $_POST["signatory_fname_full"];
                    }
                    if($_POST['pro_card_fname_full']!="") {
                        $pro_card_file_name = $_POST["pro_card_fname_full"];
                    }
                    if($_POST['c_up_fname_full']!=""){
                        $trade_license_file_name = $_POST["c_up_fname_full"];
                    }

                    if (!empty($signatory_file_name) OR !empty($pro_card_file_name) OR !empty($trade_license_file_name)) {

                        $sql = "update site_users set ";
                        $fields = '';
                        if (!empty($trade_license_file_name)) {

                            $fields .=  (!empty($fields) ? ',' : '')." trade_license_img='$trade_license_file_name' ";
                        }
                        if (!empty($pro_card_file_name)) {
                            $fields .= (!empty($fields) ? ',' : '')." pro_card_img='$pro_card_file_name' ";
                        }
                        if (!empty($signatory_file_name)) {
                            $fields .= (!empty($fields) ? ',' : '')." signatory_img='$signatory_file_name' ";
                        }

                        $sql .= $fields." where id='$user_id' limit 1";
                        mysql_query($sql);

                    }
                    $_SESSION['logged_in'] = "true";
                }
            }else{

                if($authority=="Dubai" || $authority == "دبي"){
                    $authority_ar="'Dubai'";
                    $authority_ar.=",'دبي'";
                }else if($authority=="Abu Dhabi" || $authority == "أبو ظبي"){
                    $authority_ar="'أبو ظبي'";
                    $authority_ar.=",'Abu Dhabi'";
                }else if($authority=="Sharjah" || $authority == "الشارقة"){
                    $authority_ar="'Sharjah'";
                    $authority_ar.=",'الشارقة'";
                }else if($authority=="Ras Al Khaimah" || $authority == "رأس الخيمة"){
                    $authority_ar="'Ras Al Khaimah'";
                    $authority_ar.=",'رأس الخيمة'";
                }else if($authority=="Fujairah" || $authority == "الفجيرة"){
                    $authority_ar="'Fujairah'";
                    $authority_ar.=",'الفجيرة'";
                }else if($authority=="Umm Al Quwain" || $authority == "أم القيوين"){
                    $authority_ar="'Umm Al Quwain'";
                    $authority_ar.=",'أم القيوين'";
                }else if($authority=="Ajman" || $authority == "عجمان"){
                    $authority_ar="'Ajman'";
                    $authority_ar.=",'عجمان'";
                }

                $sql="select * from site_users where trade_license='$a_tradelicense' and authority in ($authority_ar)";
                $result=mysql_query($sql);
                $count=mysql_num_rows($result);
                if($count>=1 && 0){
                    if($obj = mysql_fetch_object($result)){
                        $msg='license exists';
                    }
                }else{

                    if((!isset($_SESSION['user_id'])) && $_SESSION['user_id']==''){
                        $hash = md5( rand(0,1000) );
                        $pswd=generatePassword();
                        $doe=$_POST["pub_date"];

                        $sql_str="insert into site_users(`name`,`company_name`,`trade_license`,`date_of_expiry`,`phoneno`,`countryCode`,`mobile`,`password`,`authority`,`gm_email`,`op_fname`,`op_phoneno`,`op_countryCode`,`op_mobile`,`op_email`,`key`,`active`,`creation_date`)values('$a_fname','$a_companyname','$a_tradelicense','$doe','$a_phoneno','$countryCode','$a_mobile','$pswd','$authority','$a_email_gm','$a_op_fname','$a_op_phoneno','$op_countryCode','$a_op_mobile','$a_email_op','$hash',0,now())";
                        //$msg='success';

                        $in_id=0;
                        if(mysql_query($sql_str)){

                            $in_id=mysql_insert_id();
                            $ref = "TO".$in_id;
                            $sql_update = "update site_users set email='$ref' where id=$in_id";
                            mysql_query($sql_update);
                            $signatory_file_name = '';
                            $pro_card_file_name = '';
                            $trade_license_file_name = '';
                            if($_POST['signatory_fname_full']!="") {
                                $signatory_file_name = $_POST["signatory_fname_full"];
                            }
                            if($_POST['pro_card_fname_full']!="") {
                                $pro_card_file_name = $_POST["pro_card_fname_full"];
                            }
                            if($_POST['c_up_fname_full']!=""){
                                $trade_license_file_name = $_POST["c_up_fname_full"];
                            }

                            if (!empty($signatory_file_name) OR !empty($pro_card_file_name) OR !empty($trade_license_file_name)) {
                               /* echo "<pre>";
                                print_r($_POST);*/

                                $sql = "update site_users set ";

                                $fields = '';
                                if (!empty($trade_license_file_name)) {

                                    $fields .=  (!empty($fields) ? ',' : '')." trade_license_img='$trade_license_file_name' ";
                                }
                                if (!empty($pro_card_file_name)) {
                                    $fields .= (!empty($fields) ? ',' : '')." pro_card_img='$pro_card_file_name' ";
                                }
                                if (!empty($signatory_file_name)) {
                                    $fields .= (!empty($fields) ? ',' : '')." signatory_img='$signatory_file_name' ";
                                }

                                $sql .= $fields." where id='$in_id' limit 1";

                                mysql_query($sql);
                            }
                        }
                        if($in_id!=0){


                            $msg='success';

                            //sendEmail($a_email_op, $fromName, $a_email_op, $fromEmail, $subject, $content);
                        }//$in_id!=0
                    }
                }

            }

            if ($msg == 'success' OR $msg == 'updated') {

                $content = "<html>
                        <head><meta http-equiv='Content-Type' content='text/html;charset=UTF-8'></head>
                            <body>
                                <table width='600' cellspacing='2' cellpadding='0' style='direction:ltr;font-family: Arial,Verdana,Helvetica,sans-serif;'>
                                    <tr>
                                         <td style='background-color: #D2A118;color: #FFFFFF;font-weight: bold;height: 30px;padding-left:30px;padding-right:30px'>
                                            <table width='600' cellspacing='0' cellpadding='0'>
                                                <tr>
                                                    <td valign='middle' style='color: #FFFFFF' ><b style='font-size:16px;'>Registration </b></td>
                                                    <td valign='middle' align='right' style='color:#FFFFFF'> <b style='direction:rtl; font-size:16px;'></b></td>
                                                </tr>
                                           </table>
                                         </td>
                                    </tr>
                                    <tr>
                                        <td valign='top' align='center' style='border:1px solid #D2A118;padding-left:30px;padding-right:30px'>
                                            <table cellspacing='0' cellpadding='0'>
                                                <tr><td colspan='2' style='padding:12px'></td></tr>
                                                <tr>
                                                    <td width='50%' style='color:#C7A317'><b style='font-size:14px'>Dear User,</b></td>
                                                    <td width='50%' style='color:#C7A317;direction:rtl;'><b style='font-size:15px'>عزيزي المستخدم،</b></td>
                                                </tr>
                                                <tr><td colspan='2' style='padding:14px'></td></tr>";
                if ($msg == 'updated') {
                    $content .= "
                                                <tr>
                                                    <td style='color:#000;'><span style='font-size:14px'>You're receiving this email  because you updated registration form on our website</span></td>
                                                    <td align='right' style='color:#000' dir='rtl'><span style='font-size:15px;'> لقد قمت بتعبئة الإستمارة الخاصة بالتسجيل على موقعنا الإلكتروني</span> </td>
                                                </tr>";
                } else  {
                    $content .= "
                                                <tr>
                                                    <td style='color:#000;'><span style='font-size:14px'>You're receiving this email  because you filled out a registration form on our website</span></td>
                                                    <td align='right' style='color:#000' dir='rtl'><span style='font-size:15px;'> لقد قمت بتعبئة الإستمارة الخاصة بالتسجيل على موقعنا الإلكتروني</span> </td>
                                                </tr>";
                }
                if ($msg != 'updated') {
                    $content .= "
                                                <tr><td colspan='2' style='padding:12px'></td></tr>
                                                <tr>
                                                    <td style='color:#000;'><span style='font-size:14px'>To complete your registration, please click on the link below:</span></td>
                                                    <td style='color:#000;direction:rtl;'><span style='font-size:15px'>لإكمال عملية التسجيل يرجى الضغط على الرابط التالي:</span></td>
                                                </tr>
                                                <tr><td colspan='2' style='padding:12px'></td></tr>
                                                <tr>
                                                    <td style='color:#C7A317;' colspan='2' align='center'><a href='http://".$_SERVER['HTTP_HOST']."/en/userlogin.php?email=" . $a_email_op . "&hash=" . $hash . "' style='font-size:14px;text-decoration:none;color:#C7A317;'>http://".$_SERVER['HTTP_HOST']."/en/userlogin.php?email=" . $a_email_op . "&hash=" . $hash . "</a></span></td>

                                                </tr>";
                }
                $content .= "
                                                <tr><td colspan='2' style='padding:14px'></td></tr>
                                                <tr>
                                                    <td style='color:#000;'><span style='font-size:14px'>Please print the attached PDF, with all the details entered, and show it at our office, in person, for verification.</span></td>
                                                    <td style='color:#000;direction:rtl;'><span style='font-size:15px'>يرجى طباعة PDF المرفقة مع كل التفاصيل دخلت وتبين أنها في مكتبنا، شخصيا، للتحقق منها.</span></td>
                                                </tr>
                                                <tr><td colspan='2' style='padding:14px'></td></tr>
                                                <tr>
                                                    <td style='color:#C7A317;font-family: Arial;padding-bottom: 19px;'><span style='font-size:14px'>Kind Regards,<br>SZGMC</span></td>
                                                    <td style='color:#C7A317;direction:rtl;font-family: Arial;padding-bottom: 19px;'><span style='font-size:15px'>مع تحيات<br>مركز جامع الشيخ زايد الكبير</span></td>
                                                </tr>
                                                <tr><td colspan='2'>&nbsp;</td></tr>
                                            </table>
                                        </td>
                                    </tr>
                              </table>
                        </body>
                    </html>";
                $subject = 'Verification';
                $fromName = 'SZGMC';
                $fromEmail = 'tour@szgmc.ae';
                $file_name = include_once 'libraries/generatepdf.php';
                /*echo "<br>";
                var_dump($file_name);
                echo "<br>";*/
                sendEmail($a_email_op, $fromName, $a_email_op, $fromEmail, $subject, $content, $file_name, '', '');
                //sendEmail($a_email_op, $fromName, 'anas.muhammed@gmail.com', $fromEmail, $subject, $content, $file_name);
                //die("here");
            }
        }
    }
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<?php include 'includes/common_header.php'; ?>
    <title>User Registration</title>

    <link href="<?php echo $site_path; ?>css/calendar.css" rel="stylesheet" type="text/css" />


    <script type="text/javascript" src="<?php echo $site_path; ?>js/ajaxupload.3.5.js"></script>
    <script type="text/javascript" src="<?php echo $site_path; ?>js/jquery.validate.js"></script>
    <script type="text/javascript" src="<?php echo $site_path; ?>js/main.js"></script>
    <script type="text/javascript" src="<?php echo $site_path; ?>js/cal.js"></script>
    <link href="<?php echo $site_path; ?>css/jquery-ui-1.8.24.custom.css" rel="stylesheet" type="text/css" />

<style>
	.input_row #authority{
		width: 243px;
	}
	.input_row #c_up_fname {
		width: 164px;
	}
	.iwidth {
		width: 270px;
	}
</style>

    <script type="text/javascript" src="<?php echo $site_path; ?>js/jquery-ui-1.8.24.custom.min.js"></script>
    <script type="text/javascript">
        function check_username(val)
        {
            $.getJSON('/forms/checkEmail.php?email='+val, function(data) {
                //alert(data.user_stat);
                if(data.user_stat == 0) {
                    sta =  true;//not existing
                }
                else
                {
                    sta =  false;//existing user
                    $('div.group_slot_availability').html('');
                    $( "#slot_dialog" ).html('<p>This Email id is taken already</p>');
                    $( "#slot_dialog" ).dialog();
                    return false;
                }
            });
        }
$(document).ready(function(){
    function showAjaxError(obj,m){
	obj.show().text(m)
}
function setAjaxNotifier(obj){
	var textField=$('input[type="text"]',obj.parent()).attr('id');
	if( !$('label.error',obj.parent()).length ){
		obj.parent().append('<label for="'+ textField +'" generated="true" class="error" style=""></label>');
	}
	return textField;
}
function check_username2(v){
	if( v==$('#a_email_op1').val() ){return true}
	var html = $.ajax({
		type: "GET",
		url: '/forms/checkEmail.php?email',
		data: {email:v},
		async: false
	}).responseText;
	eval('json='+html+';');
	return json.user_stat==0?true:false;
}
function check_username(value){
	var re = /\S+@\S+\.\S+/;
	
	if( re.test(value) ){
		var cvText=setAjaxNotifier($('#a_email_op'));
		if( !check_username2(value) )
			showAjaxError($('label[for="'+cvText+'"]'),'This Email id is taken already');
			// $('div.group_slot_availability').html('');
			// $( "#slot_dialog" ).html('<p>This Email id is taken already</p>');						
			// $( "#slot_dialog" ).dialog();
	}
}
jQuery.validator.addMethod("checkEmailAvailability", function(value, element) { 
	return check_username2(value);
}, "This Email id is taken already");					   
	// $('#a_phoneno').keydown(keydownNum);
	// $('#countryCode').keydown(keydownNum);			 
	// $('#a_mobile').keydown(keydownNum);
	// $('#a_op_mobile').keydown(keydownNum);
	// $('#phoneno').keydown(keydownNum);
	// //$('#a_op_fname').keydown(checkAlpha);
    
	$('#a_phoneno').keydown(keydownNum);
	$('#countryCode').keydown(keydownNum);			 
	$('#a_mobile').keydown(keydownNum);	
	$('#a_op_mobile').keydown(keydownNum);		
	$('#phoneno').keydown(keydownNum);
	$('#a_op_fname').keydown(checkAlpha);
	$('#a_fname').keydown(checkAlpha);
	$('#a_op_mobile').keydown(checkLength);
	$('#a_op_phoneno_new').keydown(keydownNum);
	$('#a_op_phoneno_new2').keydown(keydownNum);
	$('#a_op_phoneno_new').keydown(checkLength);
	$('#a_op_phoneno_new2').keydown(checkLength);
	
	$('#a_op_phoneno_new2').keydown(preventZero);
	$('#a_op_phoneno_new').keydown(preventZero);
	$('#a_op_mobile').keydown(preventZero);
	
	$("#userRegistrationForm").validate({
		rules: {
				a_fname: "required",
			captcha: "required",
			a_tradelicense: "required",
			c_up_fname: "required",			
			authority: "required",
			a_phoneno: "required",
			a_companyname: "required",
			a_email_gm: {
				required: true,
				email: true
			},
			a_email_op: {
				required: true,
				email: true,
				checkEmailAvailability: true
			},
			/*a_mobile: {
					required: true,
					number: true
			},*/
			a_op_mobile: {
					required: true,
					number: true
			},
			a_op_phoneno: "required",
			a_op_fname: "required"
		}
	});
	var btnUpload=$('#c_passport_btn');
		var status=$('#c_status_msg');
		new AjaxUpload(btnUpload, {
			action: '/en/upload-files.php',
			name: 'uploadfile',
			onSubmit: function(file, ext){
				
				status.text('Uploading...');
			
			},
			onComplete: function(file, response){
				//On completion clear the status
				status.text('Processing..');
				response=response.split("#",2);
				if(response[0]==="success"){
				fnme=response[1];
	           
					s_pos=fnme.indexOf('_',0)+1;
					len=fnme.length;
					//fnme=fnme.substring(s_pos,len);					
					$("#c_up_fname").val(fnme);
					$("#c_up_fname_full").val(response[1]);					
				status.text('File has been uploaded successfully..');	
										
                                       
				}else if(response==="file_big"){
									status.text('File is too big...');	
                    
                                }
                                else{
								status.text('Error Found.. Please try later');	
					
				}
			}
		});
    var btnUpload=$('#btn_signatory');
        var status=$('#btn_signatory_status_msg');
        new AjaxUpload(btnUpload, {
            appendParent: btnUpload.parent(),
                action: './upload-file.php',
                name: 'uploadfile',
                onSubmit: function(file, ext){

                        status.text('Uploading...');

                    },
            onComplete: function(file, response){
                    //On completion clear the status
                        status.text('Processing..');
                    response=response.split("#",2);
                    if(response[0]==="success"){
                        //alert("here");
                            fnme=response[1];

                                s_pos=fnme.indexOf('_',0)+1;
                            len=fnme.length;
                            //fnme=fnme.substring(s_pos,len);
                                $("#signatory_fname").val(fnme);
                            $("#signatory_fname_full").val(response[1]);
                            status.text('File has been uploaded successfully..');


                                }else if(response==="file_big"){
                            status.text('File is too big...');

                            }
                    else{
                            status.text('Error Found.. Please try later');
                        //alert("here - error");
                        }
                }
        });
    var btnUpload=$('#btn_pro_card');
    var status=$('#btn_pro_card_status_msg');
    new AjaxUpload(btnUpload, {
        appendParent: btnUpload.parent(),
            action: './upload-file.php',
            name: 'uploadfile',
            onSubmit: function(file, ext){

                    status.text('Uploading...');

                },
        onComplete: function(file, response){
                //On completion clear the status
                    status.text('Processing..');
                response=response.split("#",2);
                if(response[0]==="success"){
                        fnme=response[1];

                            s_pos=fnme.indexOf('_',0)+1;
                        len=fnme.length;
                        console.log($("#pro_card_fname").length);
                        $("#pro_card_fname").val(fnme);
                        $("#pro_card_fname_full").val(response[1]);
                        status.text('File has been uploaded successfully..');


                            }else if(response==="file_big"){
                        status.text('File is too big...');

                        }
                else{
                        status.text('Error Found.. Please try later');

                        }
            }
    });
	$('#a_op_phoneno_new').blur(checkNumber);
	$('#a_op_phoneno_new2').blur(checkNumber);
	$('#a_op_mobile').blur(checkNumber);
	function checkNumber(){
		
		if($(this).val().length < 7){
			
			$(this).css('border-color', 'red');
		}
		else{
			
			$(this).css('border-color', '#CEBA69');
		}
	}
	$('#submitRegistration').click(function(event){
        if($('#a_password').val() != $('#a_rptpassword').val()) {
            alert("Password and Confirm Password don't match");
            // Prevent form submission
            event.preventDefault();
        }
         else if($('#a_op_phoneno_new2').val().length < 7){
			
			alert("Telephone no not valid");
			$('#a_op_phoneno_new2').focus();
			$('#a_op_phoneno_new2').css('border-color', 'red');
			event.preventDefault();
		}
		else if($('#a_op_phoneno_new').val().length < 7){
			
			alert("Telephone no not valid");
			$('#a_op_phoneno_new').focus();
			$('#a_op_phoneno_new').css('border-color', 'red');
			event.preventDefault();
		}
    });
    $( "#pub_date" ).datepicker({
        minDate: new Date(2015, 11 - 1 , 10 + 1),
        dateFormat: "dd/mm/yy"

    });

});
function preventZero(e){

	var val = $(this).val();
	if(val.indexOf(0) == 0){
		
		$(this).val(val.substring(1,val.length));
	}
}
function checkLength(e){
//return true;
	if(e.keyCode > 36 & e.keyCode < 41)return true;
	if(e.keyCode > 47 & e.keyCode < 58 & $(this).val().length < 7){// for digits
		
		if(e.keyCode == 48){
		
			if($(this).val().length == 0){
				
				return false;
			}
		}
		return true;
	}
	if(e.keyCode > 95 & e.keyCode < 107 & $(this).val().length < 7){// for digits on num pad
		
		if(e.keyCode == 96){
		
			if($(this).val().length == 0){
				
				return false;
			}
		}
		return true;
	}
	if(e.keyCode == 8 | e.keyCode == 9 | e.keyCode == 46){// for backspace,tab,delete

		return true;
	}
	e.preventDefault();
	return false;
}
function checkAlpha(e){
	//alert(e.keyCode);
	if (e.keyCode == 9) {
		
		return true;
	}
	if (e.shiftKey & e.keyCode == 9) {

		return true;
	}
	if((e.keyCode > 36 & e.keyCode < 41) | (e.keyCode > 64 & e.keyCode < 91) | e.keyCode == 8 | e.keyCode == 32 | e.keyCode == 46){
		
		return true;
	}
	e.preventDefault();
	return false;
}
function keydownNum(e){
	var n = e.keyCode;
	if( n==9&&$(this).val()!='' ){
		return
	}else{
		if (e.ctrlKey || e.altKey || e.shiftKey) { // if shift, ctrl or alt keys held down
			e.preventDefault();	// Prevent character input
		} else {
			if (!((n == 8)             
			|| (n == 46)               
			|| (n == 219)               
			|| (n == 221)               
			|| (n == 189)               
			|| (n == 107)
			|| (n == 173)                
			|| (n == 109)                
			|| (n == 32)                		
			|| (n >= 35 && n <= 40)    
			|| (n >= 48 && n <= 57)    
			|| (n >= 96 && n <= 105))  
			) {
				
				e.preventDefault();     // Prevent character input
			}
		}
	}
} 
</script>
<style>
td{text-align:center;}
textarea{width:660px;}
</style>

</head>
<body>
    <?php include 'includes/menus/banner_header.php'; ?>
    <!-- Banner start -->
    <div class="banner">
	<img src="<?php echo $site_path; ?>images/visiting_the_mosque_banner.jpg">     
    </div>   
    <!-- Banner Close -->
    
    <div class="main_box_content">
            <?php include 'includes/menus/nav_menu.php'; ?>
        <div class="clear"></div>
       <div class="content">
             <div class="brad_cram">
            	<ul>
            	   <li><a href="<?php echo $site_path; ?>">Home</a></li>
                    <li><a href="#" class="active"><?php
					if(isset($_REQUEST['action']) && $_REQUEST['action'] == "edit"){
						echo "Edit Info";
					}else{ echo "Register";}
				?></a></li>
                </ul>
            </div>
            <div class="content">
        	<div class="content-left">
                <?php // include 'includes/menus/left_menu.php'; 
				include 'includes/ads/ad_216_240.php';
				?>
                <br class="clear"/>
                <?php 
				include 'includes/menus/ministry_logos.php';
                include 'includes/menus/left_menu.php';
				?>
            </div>
                <div class="content-right" style="margin-left:30px">
            <div class="single_middle">
                
                    <br class="clear" />
                    <h2> <?php
					if(isset($_REQUEST['action']) && $_REQUEST['action'] == "edit"){
					}else{ echo "Register";}
				?></h2>
                    <div class="page_item_single">
                    	<div class="page_content" >
                            <div class="general_body_content">
                            <div class="reg_form">
                            	 <div >
                                 <?php
                                 	if(isset($_REQUEST['action']) && $_REQUEST['action'] == "edit"){
					}else{?>
                                      <p>This form is for Tour Operators only.</p>
                                     <p>Kindly fill out the form below and hand it to the tour operator service desk at the Sheikh Zayed Grand Mosque Center.</p>
                                      <p>Please note that your personal information will appear in the tour booking form.</p>
<!--                            		 <p>
                                     	<ul id="points">
                                        	<li>View  your pending booking</li>
                                            <li>View  your future booking</li>
                                            <li>View  your today’s booking</li>
                                            <li>View  your previous booking</li>
                                            <li>Your personal information provided in the form will be pre-filled in the booking form.</li>
                                        </ul>
                            		 </p> -->
                                     <p class="clear"></p><?php }?>
                                </div>
                                 
                                 <div id="slot_dialog" title=""></div>
									<?php include '../forms/user_registration3.php'; ?>                                
                                    <div class="clear"></div>
                                </div>
							</div>
                            <br class="clear" />
                            
                        </div>
                    </div> 
                   
            </div>
            <br class="clear" />
                </div>
            </div>
        </div>
	<div class="clear"></div></div>
	<div class="content_bottom">&nbsp;</div>
	<?php include 'includes/footer.php'; ?> 
	<?php
	
	   if($msg == 'success'){
	  // print_r($_SESSION);
		 ?>
		 <script>
			$(function() {
				$( "#dialog" ).dialog();
				$("#dialog").dialog( "option", "width", 350 );
			});
		</script>
        <script type="text/javascript">
		$(function(){
			$('#confirm').click(function() {
				$( "#dialog" ).dialog( "close" );
				window.location = "/en/register.php";
				return false;
			});
		});
		
        </script>
        <div id="dialog">
        	<p style="text-align:right;"><span dir="rtl">نشكركم على التسجيل,
تم إرسال بريد إلكتروني على عنوان البريد الخاص بكم، يرجى تأكيد التسجيل حتى تكمل العملية بنجاح.</span></p>
            <p>Thank you for registering.</p><p>A verification mail has been sent to your operations email address. Please verify to complete the registration. </p>
            <p style=""><input type="button" value="OK" style="float:right;width:40px" id="confirm"></p>
        </div>
		 <?php

		}
		else if($msg == 'updated'){
	  
		 ?>
		 <script>
			$(function() {
				$( "#dialog" ).dialog();
			});
		</script>
        <script type="text/javascript">
		$(function(){
			$('#confirm').click(function() {
				$( "#dialog" ).dialog( "close" );
				window.location = "/en/user_profile.php";
				return false;
			});
		});
		
        </script>
        <div id="dialog">
        	<p style="text-align:right;"><span dir="rtl">تم تحديث المعلومات</span></p>
            <p>Information Updated</p>
            <p style=""><input type="button" value="OK" style="float:right;width:40px" id="confirm"></p>
        </div>
		 <?php

		}
		else if($msg == 'failed')
		{
		?>
		  <script> alert("Cannot process your request"); </script>
		 <?php
		  print('<script type="text/javascript">window.location = "/en/register.php";</script>');
exit(); 
		}
	    else if($msg == 'Invalid Action')
		{
		?>
		  <script> alert("Invalid captcha"); </script>
		 <?php
		  	print('<script type="text/javascript">window.location = "/en/register.php?action=edit";</script>');

		}
		else if($msg == 'Invalid')
		{
		?>
		  <script> alert("Invalid captcha"); </script>
		 <?php
		  	print('<script type="text/javascript">window.location = "/en/register.php";</script>');

		}
		
		else if($msg == 'user exists')
		{
		?>
		  <script>
			$(function() {
				$( "#dialog" ).dialog();
			});
		</script>
        <script type="text/javascript">
		$(function(){
			$('#confirm').click(function() {
				$( "#dialog" ).dialog( "close" );
				window.location = "./register.php";
				return false;
			});
		});
		
        </script>
        <div id="dialog">
        	<p style="text-align:right;"><span dir="rtl">إن البريد الإلكتروني الذي أدخلته مسجل مسبقا ، يرجى التسجيل ببريد الكتروني آخر.</span></p>
            <p>This email id is already registered. Please register with a different email id</p>
            <p style=""><input type="button" value="OK" style="float:right;width:40px" id="confirm"></p>
        </div>
		 <?php
		  
		}else if($msg == 'license exists')
		{
		?>
		  <script>
			$(function() {
				$( "#dialog" ).dialog();
			});
		</script>
        <script type="text/javascript">
		$(function(){
			$('#confirm').click(function() {
				$( "#dialog" ).dialog( "close" );
				window.location = "./register.php";
				return false;
			});
		});
		
        </script>
        <div id="dialog">
        	<p style="text-align:right;"><span dir="rtl">إن رقم الرخصة التجارية الذي أدخلته مسجل مسبقا</span></p>
            <p>This trade license no. is already registered.</p>
            <p style=""><input type="button" value="OK" style="float:right;width:40px" id="confirm"></p>
        </div>
		 <?php
		  
		}
	?>
    
</body>
</html>