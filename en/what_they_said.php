<?php
include 'includes/database.php';
include 'includes/functions.php';
include 'includes/config.php';

$slug = explode('/',$_SERVER['REQUEST_URI']);
$slug = end($slug);
$conf = new Configuration();
$db = new MyDatabase();

$site_path = $conf->site_url;

$main_menu = $conf->getCurrentMainPage($slug);

$conf->site_description = 'What others like King Fahad, Queen Elizbeth, Jimmy Carter and other prominent leaders say about Sheikh Zayed. Find all about the Sheikh Zayed Grand Mosque in Abu Dhabi including, visiting timings, how to get to the mosque, dress code, tours, history, architecture and more.';

$conf->site_keywords = 'grand mosque, sheikh zyed grand mosque, mosque in adu dhabi, what to wear in mosque, how to get to mosque, history of grand mosque, architecture of grand mosque, grand mosque photos';

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>What others said about Sheikh Zayed</title>
    <?php include 'includes/common_header.php'; ?>
    <link href="<?php echo $site_path; ?>css/jquery-ui-1.8.21.custom.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="<?php echo $site_path; ?>js/jquery-ui-1.8.21.custom.min.js"></script>
    <script type="text/javascript">
		$(document).ready(function() {
			//$("#accordionGiftLelo").msAccordion({vertical:true});
			$( "#accordionGiftLelo" ).accordion({ autoHeight: false });
		});
	</script>
</head>
<body>
    <?php include 'includes/menus/banner_header.php'; ?>
    <!-- Banner start -->
    <div class="banner">
	<img src="<?php echo $site_path; ?>images/visiting_the_mosque_banner.jpg">     
    </div>   
    <!-- Banner Close -->
    
    <div class="main_box_content">
            <?php include 'includes/menus/nav_menu.php'; ?>
        <div class="clear"></div>
        <div class="content">
             <div class="brad_cram">
            	<ul>
            	   <li><a href="<?php echo $site_path; ?>">Home</a></li>
                    <li><a href="<?php echo $site_path; ?>founder">Founding Father</a></li>
                    <li><a href="#" class="active">They said about Sheikh Zayed</a></li>
                </ul>
            </div>
            <div class="content-left">
                <?php // include 'includes/menus/left_menu.php'; 
				include 'includes/ads/ad_216_240.php';
				?>
                <br class="clear"/>
                <?php 
				include 'includes/menus/ministry_logos.php';
                include 'includes/menus/left_menu.php';

                ?>
            </div>
                <div class="content-right" style="margin-left:10px">
                       <div class="single_middle">
                	
                    <h2 style="color:#BC8545">They said about Sheikh Zayed</h2>
                    <br class="clear" />
                    
                	<div id="accordionGiftLelo">
                      <h3><a href="#">King Fahad bin Abdul Aziz<span>(The custodian of the two Holy Mosques of Saudi Arabia)</span></a></h3>
                      
                      <div class="saidby">
                      <p id="theysaid_tab1"><img src="images/what_they_said/1.jpg" align="left" />&ldquo;We understand the significant role played by HH Sheikh Zayed bin Sultan Al Nahyan in serving the interests of the Arab nation and emphasizing the role of the UAE in the world community. The UAE plays a pivotal role providing motivation and support to our brothers&rdquo;</p>
                      </div>
					  
					   <h3><a href="#">Jimmy Carter <span>(The American President)</span></a></h3>
                      <div class="saidby">
                	<p id="theysaid_tab3"><img src="images/what_they_said/3.jpg" align="left" />&ldquo;One should express his admiration for the leadership of HH Sheikh Zayed al-Nahyan.  Due to his wisdom and political expertise, he was able to achieve amazing cultural and constructional contributions in an extra-ordinary time span&rdquo;</p>
                    
                    </div>
					  
					  
                       <h3><a href="#">HM Queen Elizabeth II <span>(Queen of England)</span></a></h3>
                      <div class="saidby">
                		<p id="theysaid_tab2"><img src="images/what_they_said/2.jpg" align="left" />&ldquo;We admire your expertise and wise leadership as a president. We can see the economic contributions you achieved for your people.  We can see it in the beauty and order of your cities, in the excellent transportation networks, in the airports and harbors, in the modern communication system which links your people with the outside world, in the green environment enjoyed  by your citizens, in the places which used to be barren deserts.  You utilized  the energy, the thought and the wealth of your country not only to fulfill materialistic  contributions but also you laid the cornerstone of a modern and comprehensive educational system to confront the requirements of the twenty first century&rdquo;</p>
                    	</div>
                       
                    <h3><a href="#">Jacques Rene Chirac <span>(The French President)</span></a></h3>
                    <div class="saidby">
                	<p id="theysaid_tab4"><img src="images/what_they_said/4.jpg" align="left" />&ldquo;Your Highness enjoys the privileges and blessings of the genuine leader including wisdom, authority, courage, justice and generosity&rdquo;</p>
                    
                    </div>
                    
                    <h3><a href="#">Kurt Waldheim <span>(The Austrian President)</span></a></h3>
                     <div class="saidby">
                	<p id="theysaid_tab7"><img src="images/what_they_said/5.jpg" align="left" />&ldquo;The age of nations is not measured by the number of decades and years but by accomplishments and contributions.  If we take these criteria for granted, we can confirm that the post-independence UAE was able to occupy a paramount position within the Arab nation within a short period of time.  The UAE did not gain this remarkable position just over night but it was the result of a series of decisive decisions and situations even prior to the independence era .  The man behind the outstanding position, occupied by the UAE in Arab politics, is Sheikh Zayed al-Nahyan, a man with an insightful vision&rdquo;</p>
                    
                    </div> 
                    
                    <h3><a href="#">HE Queen Margaret <span>(The Second of Denmark)</span></a></h3>
                    <div class="saidby">
                	<p id="theysaid_tab8"><img src="images/what_they_said/11.jpg" align="left" />&ldquo;My appreciation and greetings to HH Sheikh Zayed al-Nahyan who transformed his country into a modern nation occupying a paramount position in the international community.  I am deeply impressed by the UAE&rsquo;s foreign policy.  I have read about the unprecedented progress and comprehensive contributions in the UAE in all fields.  I wish to have the opportunity to visit this promising and rising country in order to see its civilizational hallmarks.&rdquo;</p>
                    
                    </div>
                                       
                	<h3><a href="#">Francois Mitterrand <span>(The French President)</span></a></h3>
                    <div class="saidby">
                	<p id="theysaid_tab5"><img src="images/what_they_said/6.jpg" align="left" />&ldquo;We know that your Highness wants Right to prevail over Might and legitimacy to prevail over accomplished fact&rdquo;</p>
                    
                    </div>
                    
                    <h3><a href="#">Jos&eacute; L&oacute;pez Portillo <span>(The President of Mexico)</span></a></h3>
                    <div class="saidby">
                	<p id="theysaid_tab9"><img src="images/what_they_said/7.jpg" align="left" />&ldquo;We highly appreciate the tremendous efforts of Sheikh Zayed which altered the life of the Emirati people.  Through his leadership, the UAE achieved miraculous contributions in a short period of time&rdquo;</p>
                    
                    </div>
                    
                	<h3><a href="#">Abdou Diouf <span>(President of the Senegal Republic)</span></a></h3>
                    <div class="saidby">
                	<p id="theysaid_tab10"><img src="images/what_they_said/8.jpg" align="left" />&ldquo;I consider HH Sheikh Zayed al-Nahyan a successful statesman of the highest caliber.  He is characterized by wisdom and he is a talented politician.  I am overwhelmed with happiness whenever I have a chance to meet with him and be acquainted with his wide experience in the field of politics&rdquo;</p>
                    
                    </div>
                    
                    <h3><a href="#">Mohamed Anwar Al Sadat <span>(Former Egyptian President)</span></a></h3>
                     <div class="saidby">
                	<p id="theysaid_tab6"><img src="images/what_they_said/10.jpg" align="left" />&ldquo;Your father works in silence, and participates in Arab construction silently. He works with us, with his Arab brethren everywhere, hand in hand from his heart, against what threatens our nation.&rdquo;</p>
                    
                    </div>
                    
                    <h3><a href="#">Dr. Ismat Abdul Majeed <span>(Secretary General of the League of the Arab States)</span></a></h3>
                    <div class="saidby">
                	<p id="theysaid_tab11"><img src="images/what_they_said/9.jpg" align="left" />&ldquo;Sheikh Zayed is a genuine and undisputed Arab leader who diligently works to achieve Arab solidarity bridging the gaps between conflicting parties&rdquo;</p>
                    
                    </div>
                    
                    
                	
                    
                    </div>
                    
                    <div class="clear bottom_line"> &nbsp; </div>
            </div>
                </div>
        </div>
    </div>
    
   
	<div class="content_bottom">&nbsp;</div>
	<?php include 'includes/footer.php'; ?> 
</body>
</html>