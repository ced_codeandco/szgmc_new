<?php
session_start();
include './includes/database.php';
include './includes/functions.php';
include './includes/config.php';
include_once('./includes/generic_functions.php');
$conf = new Configuration();
$db = new MyDatabase();
$site_path = $conf->site_url;
$slug = explode('/',$_SERVER['REQUEST_URI']);
$slug = end($slug);
$enq_mail_from='media@szgmc.ae';
//$enq_mail_to='tour@szgmc.ae';
//$enq_mail_cc= 'it@szgmc.ae';
$enq_mail_to='filming@szgmc.ae';
define("ENQ_FROM",$enq_mail_from);
define("ENQ_TO",$enq_mail_to);
//define("ENQ_CC",$enq_mail_cc);
$msg="";
$current_page = 'ftplogin';
if (!empty($_REQUEST['captcha'])) {
    if (empty($_SESSION['captcha']) || trim(strtolower($_REQUEST['captcha'])) != $_SESSION['captcha']) {
        $msg="Invalid";
    } else {
        if(isset($_POST['a_fname']) && $_POST['a_fname']!='' && isset($_POST['agree']) && $_POST['agree']=='1'){

            foreach($_POST as $name => $value) {
                $$name=mysql_real_escape_string($value);
            }



            $file_flag=0;

            //$pub_date=date("Y-m-d",strtotime($pub_date)).' 00:00:00';
            $proposed_date_time=$prop_time;
            $ip=$_SERVER['REMOTE_ADDR'];


            $sql_str="insert into filming_permissions(a_fname,nationality,h_f_crew_fname,h_crew_nationality,company,company_commissioning,purpose,exp_date_pub,proposed_dt_visit,contact_num,email,special_requirements,other_info,created_date,ip,num_persons_expected,short_intro,equipment,short_info,category)values('$a_fname','$a_nationality','$a_h_fname','$a_h_nationality','$a_company','$a_comp_comm','$purpose','$prop_date','$proposed_date_time','$contact_num','$email','$special_req','$other_info',NOW(),'$ip','$num_persons_expected','$short_intro','$equipments','$short_info','$group_category')";

            $in_id=0;
            if(mysql_query($sql_str)){
                $msg='success';
                $in_id=mysql_insert_id();
                $ref = "FP"."-".$in_id;

                $sql_str3="UPDATE temp_table_crew SET filming_permissions_id ='".$in_id."' WHERE session='".SESS_ID."'";
                mysql_query($sql_str3);

                $sql="delete  from  table_crew where session='".SESS_ID."'";
                mysql_query($sql);

                $sql_str2="insert into table_crew(session,fname,title,address,telephone,email,file_name,date_time,filming_permissions_id) select session,fname,title,address,telephone,email,file_name,date_time,filming_permissions_id from temp_table_crew where session='".SESS_ID."'";
                mysql_query($sql_str2);


                if($_POST['a_doc']!=""){
                    $file_name=$_POST["a_pass_fname"];
                    $sql="update filming_permissions set p_doc1='$file_name' where f_id='$in_id' limit 1";
                    mysql_query($sql);
                }
                if($_POST['f_crew_fname']!=""){
                    $file_name1=$_POST["f_crew_fname"];
                    $sql="update filming_permissions set p_doc2='$file_name1' where f_id='$in_id' limit 1";
                    mysql_query($sql);
                }
                $sql="delete  from  temp_table_crew where session='".SESS_ID."'";
                mysql_query($sql);
                $sql = "select * from table_crew where session='".SESS_ID."'";
                mysql_query($sql);
                $array = array();
                while ($row = @mysql_fetch_object($sql )) {
                    $array[] = $row;
                }
                @mysql_free_result( $sql);
                $result = $array;




                $fromName='Tours';
                $fromEmail=ENQ_FROM;
                $toName='SZGMC';
                $toEmail=ENQ_TO;
                $subject = "SZGMC Filming Media Request";
                $content = getContent($_POST, $result, $file_name, $file_name1,$site_path,$ref);

                $output = mysql_real_escape_string($content);
                $sql="update filming_permissions set details='$output' where f_id='$in_id' limit 1";
                mysql_query($sql);
                $email_sql="select email,name from service_email where frm_id='21'";
                $email_querry= mysql_query($email_sql);
                while($email_res=mysql_fetch_object($email_querry))
                {
                    $toName = $email_res->name;
                    $toEmail = $email_res->email;
                    sendEmail($toName, $fromName, $toEmail, $fromEmail, $subject, $content);
                }


            }
            else{

                $msg='failed';
            }
        }}}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php include 'includes/common_header.php'; ?>
    <title>Filming Permission Application Online</title>




    <script type="text/javascript" src="<?php echo $site_path; ?>js/ajaxupload.3.5.js"></script>
    <link href="<?php echo $site_path; ?>css/calendar.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $site_path; ?>css/jquery-ui-1.8.24.custom.css" rel="stylesheet" type="text/css" />

    <style>
        .input_row .input_cls{
            width: 251px;
        }
    </style>
    <script type="text/javascript" src="<?php echo $site_path; ?>js/jquery.validate.js"></script>
    <script type="text/javascript" src="<?php echo $site_path; ?>js/main.js"></script>
    <script type="text/javascript" src="<?php echo $site_path; ?>js/jquery-ui-1.8.24.custom.min.js"></script>

    <script type="text/javascript">
        $(function(){
            $('#confirmnew').click(function() {
                //$('#dialog').hide();
                $( "#dialognew" ).dialog( "close" );
                window.location = "/en/media-form";
                return false;
            });
        });

    </script>
    <script type="text/javascript">
        function postFeedback()
        {


            $.post("./includes/eServices_feedback.php",$("#fbk_frm").serialize(),function(response) {

                if(response == true) {
                    alert("Thank you for your feedback")
                } else {
                    $(function() {
                        $( "#dialognew" ).dialog();
                    });
                }
                $( "#dialog" ).dialog( "close" );

            });


        }


        $(document).ready(function () {
            $("#mediaForm").validate({
                rules: {
                    a_fname: "required",
                    email: {
                        required: true,
                        email: true
                    },
                    a_nationality: "required",
                    a_h_fname: "required",
                    a_h_nationality: "required",
                    a_company: "required",
                    num_persons_expected: "required",
                    purpose: "required",
                    a_doc_fname: "required",
                    short_info: "required",
                    group_category: "required",
                    prop_date: "required",
                    //prop_time: "required",
                    contact_num: "required",
//				special_req: "required",
                    agree: "required",
                    captcha: "required"


                }
            });

            $('#contact_num').keydown(keydownNum);
            function keydownNum(e){
                var n = e.keyCode;
                if( n==9&&$(this).val()!='' ){
                    return
                }else{
                    if (e.ctrlKey || e.altKey || e.shiftKey) { // if shift, ctrl or alt keys held down
                        e.preventDefault();	// Prevent character input
                    } else {
                        if (!((n == 8)
                            || (n == 46)
                            || (n == 219)
                            || (n == 221)
                            || (n == 189)
                            || (n == 107)
                            || (n == 173)
                            || (n == 109)
                            || (n == 32)
                            || (n >= 35 && n <= 40)
                            || (n >= 48 && n <= 57)
                            || (n >= 96 && n <= 105))
                        ) {

                            e.preventDefault();     // Prevent character input
                        }
                    }
                }
            }

            $( "#prop_date" ).datepicker({
                beforeShowDay: function(day) {
                    var day = day.getDay();
                    if (day == 5 || day == 6) {
                        return [false, ""]
                    } else {
                        return [true, ""]
                    }
                },
                minDate: new Date(<?php echo date('Y'); ?>, <?php echo date('m'); ?> - 1 , <?php echo date('d'); ?> + 14),
                maxDate: new Date(<?php echo date('Y'); ?>, <?php echo date('m'); ?> - 1 , <?php echo date('d'); ?> + 104),
                dateFormat: "dd/mm/yy"
            });
            var validate_file_type = false;
            initializeFileUploadGlobal('f_crew', validate_file_type);
            var status=$('#c_status_msg');


            //$('#prop_date').simpleDatepicker({ chosendate: '1/1/1980', startdate: '1/1/1980', enddate: '1/1/2005' });
            //$('#pub_date').simpleDatepicker({ chosendate: '1/1/1980', startdate: '1/1/1980', enddate: '1/1/2005' });
            $('#prop_date').focus(function() {
                $("#prop_date").click();
            });


            $("#c_add_crew").click(function (){

                v_fname=$("#c_fname").val();
                v_ftitle=$("#c_ftitle").val();
                v_address=$("#c_address").val();
                v_tel=$("#c_telephone").val();


                v_email=$("#c_email").val();
                v_file=$("#c_up_fname_full").val();
//            alert(v_email);
                if(v_fname == ''){
                    if(v_ftitle == '')
                        $("#c_ftitle_error").css({'display' : ''});
                    $("#c_fname_error").css({'display' : ''});
                    return false;
                }
                if(v_ftitle == ''){
                    $("#c_ftitle_error").css({'display' : ''});
                    return false;
                }


                pars='fname='+v_fname;
                pars+='&ftitle='+v_ftitle;
                pars+='&address='+v_address;
                pars+='&tel='+v_tel;
                pars+='&email='+v_email;
                pars+='&cfile='+v_file;


                var status=$('#c_status_msg');
                status.text('Please wait...');

                crew_submit(pars);
                //alert(pars);
                //p_file_name=$("#c_passport").val();
                //$("#temp_c_passport").val(p_file_name);
            });

        });

        function crew_submit(pars)
        {
            var status=$('#c_status_msg');
            $.ajax({
                type: "POST",
                url: "/en/submit_crew.php",
                data: pars,
                success: function(result){
                    if(result!='error'){
                        $('#crew_table').html(result)
                        if(pars!='')
                            status.text('New entry has been added');
                        $("#c_fname").val('');
                        $("#c_ftitle").val('');
                        $("#c_address").val('');
                        $("#c_telephone").val('');
                        $("#c_email").val('');
                        $("#c_up_fname_full").val('');
                        $("#c_up_fname").val('');

                    }
                },
                complete: function(){   },
                error: function(){   }
            });
        }

        $(document).ready(function () {
            crew_submit('');
        });

        function delete_crew(c_id)
        {
            var status=$('#c_status_msg');
            $.ajax({
                type: "POST",
                url: "/en/delete_crew.php",
                data: 'crew_id_del='+c_id,
                success: function(result){
                    if(result!='error')
                    {
                        status.text('Crew deleted');
                    }
                },
                complete: function(){  crew_submit(''); },
                error: function(){ status.text('Could not detele at this moment');   }
            });

        }
        function check_additional_fileds(val)
        {
            if((val=='International Media Organization') || (val=='مؤسسة إعلامية دولية'))
            {
                $('.adnmc').show();
                initializeFileUploadGlobal('a_pass');
                changeClasses();
            }
            else
            {
                $('.adnmc').hide();
                changeClasses();
            }

        }
        function changeClasses() {
            var even = true;
            $('#mediaForm div.input_row:visible').each(function(index) {
                $(this).removeClass('odd');
                $(this).removeClass('even');
                if(even) {
                    $(this).addClass('even');
                } else {
                    $(this).addClass('odd');
                }
                even = !even;
            });
        }

    </script>




</head>
<body>

<?php include 'includes/menus/banner_header.php'; ?>
<!-- Banner start -->
<div class="banner">
    <img src="<?php echo $site_path; ?>images/visiting_the_mosque_banner.jpg">
</div>
<!-- Banner Close -->

<div class="main_box_content">
    <?php include 'includes/menus/nav_menu.php'; ?>
    <div class="clear"></div>
    <div class="content">
        <div class="brad_cram">
            <ul>
                <li><a href="<?php echo $site_path; ?>">Home</a></li>
                <li><a href="<?php echo $site_path; ?>e_services.php">E-Services</a></li>
                <li><a href="#" class="active">Filming permission</a></li>
            </ul>
        </div>
        <div class="content-left">
            <?php // include 'includes/menus/left_menu.php';
            include 'includes/ads/ad_216_240.php';
            ?>
            <?php
            include 'includes/menus/ministry_logos.php';
            include 'includes/menus/left_menu.php';
            ?>
        </div>
        <div class="content-right" style="margin-left:10px">

            <div class="single_middle">
                <div id="dialognew" style="display:none;">
                    <p style="text-align:right;">لقد تم تقديم طلبكم بنجاح ،</p>
                    <p><span dir="ltr">Your request has been submitted successfully</span></p>
                    <p style=""><input type="button" value="OK" style="float:right;width:40px" id="confirmnew"></p>
                </div>

                <?php
                include 'includes/menus/marquee.php';
                ?>
                <h2>Filming Permission</h2>
                <div class="page_item_single">
                    <div class="page_content" >
                        <div class="general_body_content">
                            <div class="reg_form">
                                <div class="text">

                                    <p>

                                        Thank you for your interest in the Sheikh Zayed Grand Mosque. In order to facilitate your media request, we ask you to complete the form below with all of the required details. All permissions must be made at least 14 days prior to the date of visit.
                                    </p>
                                    <p>
                                        You request will be answered via e-mail once we have received your application. Please note that our office business hours are from 8 a.m. to 4 p.m., from Sunday to Thursday, and all requests will be answered within 14 working days.
                                    </p>

                                </div>
                                <!--<div class="login_sidebar"><img src="/en/images/login_sidebar.jpg"></div>-->
                                <?php include '../forms/filming_permission.php'; ?>

                                <div class="clear"></div>
                            </div>
                        </div>
                        <br class="clear" />

                    </div>
                </div>

            </div>

        </div>
    </div>
    <div class="clear"></div>		</div>



<div class="content_bottom">&nbsp;</div>
<?php include 'includes/footer.php'; ?>
<?php


if($msg == 'success'){

    ?>
    <script>
        $(function() {
            $( "#dialog" ).dialog({maxWidth:480,modal:true,width:'90%'});
        });
    </script>
    <script type="text/javascript">
        $(function(){
            $('#confirm').click(function() {
                //$('#dialog').hide();
                $( "#dialog" ).dialog( "close" );
                return false;
            });
        });

    </script>
    <div id="dialog" class="lostpopup">
        <p style="text-align:center;"><span dir="rtl">لقد تم تقديم طلبك بنجاح ، وسوف نقوم بالتواصل معك قريبا.</span></p>
        <p>Your request has been submitted successfully. We will contact you shortly.</p>
        <form name="feedback_frm" method="post" id="fbk_frm">
            <div>
                <label for="comments">Kindly provide us your feedback on this e-service.</label>
                <label for="comments" class="arabic">يرجى تقديم ملاحظاتك عن هذه الخدمة الإلكترونية.</label>


                <textarea name="comments" cols="50" rows="4"></textarea>
                <input type="hidden" name="path" value="media-form"/>
                <input type="hidden" name="tour_ref" value="<?php echo $ref;?>"/>
            </div>
            <div align="center" class="clear">
                <input type="button" style="direction:ltr;" id="submitTour" name="submitTour" class="cmt_btn" value="Submit ﺍرسل" onClick="postFeedback();" />
                <input type="button" style="direction:ltr;" id="confirm" class="cmt_btn" value="No Comments لا تعليق" />
            </div>
        </form>
    </div>
<?php

}
else if($msg == 'failed')
{
?>
    <script> alert("Cannot process your request"); </script>
<?php
print('<script type="text/javascript">window.location = "/en/media-form";</script>');
exit();
}
else if($msg == 'Invalid')
{
?>
    <script> alert("Invalid captcha"); </script>
    <?php
    print('<script type="text/javascript">window.location = "/en/media-form";</script>');
    exit();
}

?>
</body>
</html>