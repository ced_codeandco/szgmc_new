<?php
include './includes/database.php';
include './includes/functions.php';
include './includes/config.php';
include_once('./includes/generic_functions.php');
$conf = new Configuration();
$db = new MyDatabase();
$site_path = $conf->site_url;

$slug = explode('/',$_SERVER['REQUEST_URI']);
$slug = end($slug);
session_start();

$msg="";
$current_page = 'ftplogin';

?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

	<?php include 'includes/common_header.php'; ?>
    <title>Your Account</title>

	<link href="<?php echo $site_path; ?>css/calendar.css" rel="stylesheet" type="text/css" />

 

	<script type="text/javascript" src="<?php echo $site_path; ?>js/ajaxupload.3.5.js"></script>
	<script type="text/javascript" src="<?php echo $site_path; ?>js/jquery.validate.js"></script>
	<script type="text/javascript" src="<?php echo $site_path; ?>js/main.js"></script>
	<script type="text/javascript" src="<?php echo $site_path; ?>js/cal.js"></script>
    
<link href="<?php echo $site_path; ?>css/jquery-ui-1.8.24.custom.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo $site_path; ?>js/jquery-ui-1.8.24.custom.min.js"></script>


<style>
td{text-align:center;}
textarea{width:660px;}
</style>

</head>
<body>
    <?php include 'includes/menus/banner_header.php'; ?>
    <!-- Banner start -->
    <div class="banner">
	<img src="<?php echo $site_path; ?>images/visiting_the_mosque_banner.jpg">     
    </div>   
    <!-- Banner Close -->
    
    <div class="main_box_content">
            <?php include 'includes/menus/nav_menu.php'; ?>
        <div class="clear"></div>
        <div class="content">
             <div class="brad_cram">
            	<ul>
            	   <li><a href="<?php echo $site_path; ?>">Home</a></li>
                   <li><a href="#" class="active">Your Account</a></li>
                </ul>
            </div>
            <div class="content-left">
                <?php // include 'includes/menus/left_menu.php'; 
				include 'includes/ads/ad_216_240.php';
				?>
                <br class="clear"/>
                <?php 
				include 'includes/menus/ministry_logos.php';
                include 'includes/menus/left_menu.php';

                ?>
            </div>
                <div class="content-right" style="margin-left:10px">
                    <div class="single_middle">
                    <h2>Your Account</h2>
                    <div class="page_item_single">
                    	<div class="page_content" >
                            <div class="general_body_content">
                                 <div class="reg_form">
                                 <!--<div class="clear"><h2 style="font-size:13px;color:#000;float:left;">Your Tour Bookings</h2><h2 style="font-size:13px;color:#000;float:right;width:150px">الحجوزات السياحية الخاصة بك</h2></div>
                                 -->
                                 
                                  <?php
								  //print_r($_SESSION);
if((isset($_SESSION['user_id'])) && $_SESSION['user_id']!=''){
 	include '../forms/update_user_profile2.php'; ?>
	<br/>
    <div style="float:left;width:357x;">
        <ul id="links_en">
             <li><a href="/en/register.php?action=edit">Edit&nbsp;Info</a></li>
             <li><a href="/en/changePassword.php">Change Password</a></li>
			 <li><span><a href="/en/user_profile.php">Your Account</a></span></li>
        <li><span><a href="/en/userlogout.php"> Logout </a></span></li>
        </ul>      
    </div> 
	<div style="float:right;width:289x">
        <ul id="links_ar">
            
			<li style="background: none;float: left;width: 30px;padding-top: 0;"><a href="/userlogout.php"> خروج</a></li>
			<li style="float: left;width: 30px;padding-top: 0;background-position:0px 5px;"><a href="/user_profile.php">حسابك</a> </li>
           <li style="float:left;width: 80px;padding-top: 0;background-position:0px 5px;"><a href="/changePassword.php">تغيير كلمة المرور</a></li>
			
         <li style="float:left;width: 80px;padding-top: 0;background-position:0px 5px;"><a href="/register.php?action=edit">تحرير بيانات</a></li>
        </ul>    
	</div>  
    <div class="clear"></div>
	<?php
    $date_parts = explode('/', $obj->date_of_expiry);
    $expDate = strtotime($date_parts[2].'-'.$date_parts[1].'-'.$date_parts[0]);
    if (strtotime(date('Y-m-d')) > $expDate) {
        echo '<div style="color: red;border: 1px solid red;min-height: 40px;padding: 4px 9px 0px 7px;">Your trade licence has been expired and you will not be able to book the tour. Kindly contact the management for further clarifications.</div>';
    } else {
        if ($obj->approve == 'D') {

            echo '<div style="color: red;border: 1px solid red;min-height: 40px;padding: 4px 9px 0px 7px;">Your registration has been blocked and you will not be able to book the tour. Kindly contact the management for further clarifications.</div>';
        } else if ($obj->approve == 'E') {
            echo '<div style="color: red;border: 1px solid red;min-height: 40px;padding: 4px 9px 0px 7px;">Your registration has been expired and you will not be able to book the tour. Kindly contact the management for further clarifications.</div>';
        } else if (empty($obj->approve)) {
            echo '<div style="color: red;border: 1px solid red;min-height: 40px;padding: 4px 9px 0px 7px;">Your account is not approved and you will not be able to book the tour. Kindly contact the management for further clarifications.</div>';
        } else if (!empty($obj->approve)) {?>
            <div class="book" style="margin-top: 15px;">
                <div><a href="/en/tours-booking-form">Book Your Tour&nbsp;&nbsp احجز جولة سياحية</a></div>
            </div>
            <div class="book" style="margin-top: 15px;">
                <div><a href="/en/bookings.php?type=edit">Edit Tour Booking&nbsp;&nbsp تعديل على الحجز</a></div>
            </div>
        <?php
        }//ending session var check block
    }
}
?>
                                    <div class="clear"></div>
                                </div>
							</div>
                            <br class="clear" />
                            
                        </div>
                    </div> 
                   
            </div>
                </div>
        </div>
        <div class="clear"></div>
        </div>
                    
              
	<div class="content_bottom">&nbsp;</div>
	<?php include 'includes/footer.php'; ?> 
	
</body>
</html>