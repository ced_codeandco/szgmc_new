<?php
include 'includes/database.php';
include 'includes/functions.php';
include 'includes/config.php';
$conf = new Configuration();
$db = new MyDatabase();
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Sheikh Zayed Grand Mosque</title>
    <?php include 'includes/common_header.php'; ?>

</head>
<body>
<?php include 'includes/menus/banner_header.php'; ?>
    <!-- Banner start -->
    <div class="banner">
	<img src="<?php echo $site_path; ?>images/visiting_the_mosque_banner.jpg">     
    </div>   
    <!-- Banner Close -->
    
    <div class="main_box_content">
            <?php include 'includes/menus/nav_menu.php'; ?>
        <div class="clear"></div>
        <div class="content">
             <div class="brad_cram">
            	<ul>
            	   <li><a href="<?php echo $site_path; ?>">Home</a></li>
                    <li><a href="#" class="active">Newsletter Subscription</a></li>
                </ul>
            </div>
			<div class="content-left">
                <?php // include 'includes/menus/left_menu.php'; 
				include 'includes/ads/ad_216_240.php';
				?>
                <br class="clear"/>
                <?php 
				include 'includes/menus/ministry_logos.php';
                include 'includes/menus/left_menu.php';
				?>
            </div>
                <div class="content-right" style="margin-left:10px">
					<div class="single_middle" style="text-align:center">
                <br class="clear" />
                
                <h2>Newsletter Subscription</h2>
				
				<?php
				if(isset($_REQUEST["mail_id"]))
				{
				
				$email = $_REQUEST["mail_id"];
				$email = base64_decode($email);
				$sql= "select * from newsletter where email_address='$email'";
				$query = mysql_query($sql);
				$count = mysql_num_rows($query);
				if($count==0)
				{
				$query="INSERT INTO newsletter(`email_address`,`submitted_date`) VALUES ('$email',now())";
                mysql_query($query);
				}
				
				$emsg="<p>Thank you for subscribing to the SZGMC newsletter.<br/>
Your email address has been confirmed and will start receiving our newsletters</p>
";
				//$amsg = "&#1606;&#1588;&#1603;&#1585;&#1603; &#1593;&#1604;&#1609; &#1578;&#1571;&#1603;&#1610;&#1583; &#1575;&#1604;&#1581;&#1580;&#1586;";
				
				
				}
				?>
				<p>&nbsp;</p>
				<div style="width:450px; float:left;margin-top:10px">
				<h3><?php echo $emsg; ?></h3> </div>
				<div style="width:350px; float:right;">
				<h3><?php echo $amsg; ?></h3> 
</div>
            	<br class="clear" />
                
            <br class="clear" />
        </div>
				</div>
				</div>
				</div>
				
    
	<div class="content_bottom">&nbsp;</div>
	<?php include 'includes/footer.php'; ?> 
</body>
</html>