<?php 
session_start();
?>
<style>
	.input_row_dob #pub_date{
		width: 240px;
	}
</style>
<?php
include './includes/database.php';
include './includes/functions.php';
include './includes/config.php';
include_once('./includes/generic_functions.php');
$conf = new Configuration();
$db = new MyDatabase();
$site_path = $conf->site_url;
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php include 'includes/common_header.php'; ?>
    <title>Sheikh Zayed Grand Mosque Center+</title>
</head>
<body>
    
    <?php include 'includes/menus/banner_header.php'; ?>
    <!-- Banner start -->
    <div class="banner">
	<img src="<?php echo $site_path; ?>images/visiting_the_mosque_banner.jpg">     
    </div>   
    <!-- Banner Close -->
    
    <div class="main_box_content">
            <?php include 'includes/menus/nav_menu.php'; ?>
        <div class="clear"></div>
        <div class="content">
        	<div class="brad_cram">
            	<ul>
            	   <li><a href="<?php echo $site_path; ?>">Home</a></li>
                    <!--<li><a href="visiting-the-mosque" >Visiting The Mosque</a></li>-->
                    <li><a href="#" class="active">Delegates & Tour Operators</a></li>
                </ul>
            </div>
      
      
      	<div class="inside_conter">
        		<div class="Visiting_box mosquemanner">
                    <ul>
                        <li class="fist_tham">
                            <a href="<?php echo $site_path; ?>mosque-manner">
                                <img src="<?php echo $site_path; ?>images/Visiting-The -osque/Visiting_The_Mosque_01.jpg">
                                <span><img src="<?php echo $site_path; ?>images/Visiting-The -osque/tham01.png">Mosque Manners</span>
                            </a>
                        </li>

                        <li>
                            <a href="<?php echo $site_path; ?>mosque-opening-hours">
                                <img src="<?php echo $site_path;?>images/Visiting-The -osque/Visiting_The_Mosque_02.jpg">
                                <span><img src="<?php echo $site_path; ?>images/Visiting-The -osque/tham02.png">Visiting Times</span>
                            </a>
                        </li>

                        <li>
                            <a href="<?php echo $site_path; ?>getting-to-the-mosque">
                                <img src="<?php echo $site_path; ?>images/Visiting-The -osque/Visiting_The_Mosque_07.jpg">
                                <span><img src="<?php echo $site_path; ?>images/Visiting-The -osque/tham07.png">Getting To The Mosque</span>
                            </a>
                        </li>

                   
                        <li class="fist_tham">
                            <!--a target="_blank" href="https://www.google.com/maps/views/streetview/sheikh-zayed-grand-mosque?hl=ar&amp;gl=us"-->
                            <a target="_blank" href="https://www.google.com/maps/streetview/#sheikh-zayed-grand-mosque">
                                <img src="<?php echo $site_path; ?>images/Visiting-The -osque/Visiting_The_Mosque_08.jpg">
                                <span><img src="<?php echo $site_path; ?>images/visiting_the_mosque/virtual_tour.png"> Virtual Tour </span>
                            </a>
                        </li>

                        <li>
                            <a href="<?php echo $site_path; ?>questions">
                                <img src="<?php echo $site_path; ?>images/Visiting-The -osque/Visiting_The_Mosque_10.jpg">
                                <span><img src="<?php echo $site_path; ?>images/Visiting-The -osque/tham10.png"> FAQs </span>
                            </a>
                        </li>

                        <li>
                            <a href="<?php echo $site_path; ?>services">
                                <img src="<?php echo $site_path; ?>images/Visiting-The -osque/Visiting_The_Mosque_11.jpg">
                                <span><img src="<?php echo $site_path; ?>images/Visiting-The -osque/info-thumb.jpg">Services</span>
                            </a>
                        </li>

                  
                        <li class="fist_tham">
                            <a href="<?php echo $site_path; ?>tour-booking-form">
                                <img src="<?php echo $site_path; ?>images/Visiting-The -osque/Visiting_The_Mosque_06.jpg">
                                <span><img src="<?php echo $site_path; ?>images/Visiting-The -osque/tham06.png">Book your tour</span>
                            </a>
                        </li>

                        <li>
                            <a href="<?php echo $site_path; ?>register.php">
                                <img src="<?php echo $site_path; ?>images/visiting_the_mosque/tour_operator_regn.jpg">
                                <span><img class="big-text-thumb" src="<?php echo $site_path; ?>images/Visiting-The -osque/tham06.png">Tour Operator Registration</span>
                            </a>
                        </li>
                    </ul>
                </div>
        </div>
      
      
     
      
        </div>
     <div class="clear"></div> </div>
    
    
    
    
    
 
	<div class="content_bottom">&nbsp;</div>
	<?php include 'includes/footer.php'; ?> 
	<?php
	
	   if($msg == 'success'){
	  
		 ?>
		<script>
			$(function() {
				$( "#dialog" ).dialog({maxWidth:480,modal:true,width:'90%'});
			});
		</script>
        <script type="text/javascript">
		$(function(){
			$('#confirm').click(function() {
				//$('#dialog').hide();
				$( "#dialog" ).dialog( "close" );
				window.location = "/en/careers";
				return false;
			});
		});
		
        </script>
        <div id="dialog" class="lostpopup">
        	<p style="text-align:center;"><span dir="rtl">لقد تم تقديم طلبك بنجاح ، وسوف نقوم بالتواصل معك قريبا.</span></p>
            <p>Your request has been submitted successfully. We will contact you shortly.</p>
            <form name="feedback_frm" method="post" id="fbk_frm">
                <div>
                   <label for="comments">Kindly provide us your feedback on this e-service.</label>
                   <label for="comments" class="arabic">يرجى تقديم ملاحظاتك عن هذه الخدمة الإلكترونية.</label>
                   <textarea name="comments" cols="50" rows="4"></textarea>
                   <input type="hidden" name="path" value="careers"/>
				    <input type="hidden" name="tour_ref" value="<?php echo $ref;?>"/>
                </div>
                <div align="center" class="clear">
                  <input type="button" style="direction:ltr;" id="submitTour" name="submitTour" class="cmt_btn" value="Submit ﺍرسل" onClick="postFeedback();" />
                  <input type="button" style="direction:ltr;" id="confirm" class="cmt_btn" value="No Comments لا تعليق" />
                </div>
	   		</form>
        </div>
		 <?
		
		}
		else if($msg == 'failed')
		{
		?>
		  <script> alert("Cannot process your request"); </script>
		 <?
		  print('<script type="text/javascript">window.location = "/en/careers";</script>');
exit(); 
		}else if($msg == 'Invalid')
		{
		?>
			<script> alert("Invalid captcha"); </script>
		<?php
			print('<script type="text/javascript">window.location = "/en/carrers.php";</script>');
			exit(); 
		}
	  
	?>
</body>
</html>