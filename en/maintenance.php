<?php
ob_start();
include 'includes/database.php';
include 'includes/dal/news.php';
include 'includes/dal/pages.php';
include 'includes/functions.php';
include 'includes/config.php';
$current_page = 'index';
$db = new MyDatabase();
$conf = new Configuration();
$site_path = $conf->site_url;
$msql = "select * from maintenance";
$mquery = mysql_query($msql);
while($mres= mysql_fetch_object($mquery))
{
	$mstatus = $mres->m_status;
}
if($mstatus==0)
{
	header("location:/en/");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Site Under Maintenance</title>
<link href="<?php echo $site_path; ?>css/style.css" rel="stylesheet" type="text/css" />
</head>

<body>
<div class="wrapper" style="background:none">
<div style="width:1000px; margin:0 auto;">
<img src="/images/maintenance_page.jpg" alt="Site Under Maintenance"> </div>

<div class="clear footer" style="direction:ltr; text-align:center; margin-top:5px; background:none;">Copyright &copy; 2012 by Sheikh Zayed Grand Mosque Centre. All rights reserved.</div>
</div>
</body>
</html>