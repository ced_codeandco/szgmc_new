<?php
include 'includes/database.php';
include 'includes/dal/activities.php';
include 'includes/functions.php';
include 'includes/config.php';

$id = $_REQUEST['id'];
$conf = new Configuration();
$db = new MyDatabase();
$dal_news = new ManageNews();
$news = $dal_news->getSingleNews($id);

$slug = 'center-activities';

$site_path = $conf->site_url;

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><?php echo $news->news_title; ?> - Sheikh Zayed Grand Mosque Center</title>
    <?php include 'includes/common_header.php'; ?>
    <link href="<?php echo $site_path; ?>css/news.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <?php include 'includes/menus/banner_header.php'; ?>
    <!-- Banner start -->
    <div class="banner">
	<img src="<?php echo $site_path; ?>images/visiting_the_mosque_banner.jpg">     
    </div>   
    <!-- Banner Close -->
    
    <div class="main_box_content">
            <?php include 'includes/menus/nav_menu.php'; ?>
        <div class="clear"></div>
        <div class="content">
             <div class="brad_cram">
            	<ul>
            	   <li><a href="<?php echo $site_path; ?>">Home</a></li>
                    <li><a href="#" class="active">Events & Activities</a></li>
                </ul>
            </div>
            <div class="content-left">
                <?php // include 'includes/menus/left_menu.php'; 
				include 'includes/ads/ad_216_240.php';
				?>
                <br class="clear"/>
                <?php 
				include 'includes/menus/ministry_logos.php';
                include 'includes/menus/left_menu.php';
				?>
            </div>
                <div class="content-right" style="margin-left:10px">
             <div class="single_middle" >
                <br class="clear" />
                    <?php
                    $news_url= $site_path."news-detail/".string_to_filename($news->news_title).'-'.$news->news_id;
                    ?>
                    <div class="news_item_single">
                    	<div class="news_content" >
				        	<div class="general_body_title"><?php echo $news->news_title; ?></div>
                        	<br class="clear" />
                            <img src="<?php echo $site_path; ?>news_images/<?php echo $news->image; ?>" class="content_inside_image" >
                            <div class="general_body_content"><?php echo $news->news_text; ?></div>
                            <br class="clear" />
                            
                        </div>
                    </div> 
                    <div class="clear bottom_line"> &nbsp </div>
            </div>
                </div>
        </div>
            
    </div>
                    
                    
          
	<div class="content_bottom">&nbsp;</div>
	<?php include 'includes/footer.php'; ?> 
</body>
</html>