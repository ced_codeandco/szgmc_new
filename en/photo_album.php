<?php
include 'includes/database.php';
include 'includes/functions.php';
include 'includes/config.php';
include 'includes/flickr.php';

$conf = new Configuration();
$db = new MyDatabase();
$site_path = $conf->site_url;

$slug = explode('/',$_SERVER['REQUEST_URI']);
$slug = mysql_real_escape_string(end($slug));
$conf = new Configuration();
$db = new MyDatabase();

$site_path = $conf->site_url;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>SZG</title>
    <link rel="stylesheet" href="<?php echo $site_path; ?>css/nivo-slider.css" type="text/css" media="screen" />
    <link href="<?php echo $site_path; ?>css/style2.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $site_path; ?>css/photo_gallery/jquery.ad-gallery.css">
    <!--[if IE]>
    <style>
    .header .top_menu .contactus{width:114px;float:left;margin-top:22px;}
    .header .top_menu .contactus a{ background:url(<?php echo $site_path; ?><?php echo $site_path; ?>images/contactus.png) no-repeat; color:#000000; padding:10px 20px 10px 15px; text-decoration:none; font-weight:bold;height:36px;}
    .header .top_menu .contactus a:hover{ background:url(<?php echo $site_path; ?>images/contactus_hover.png) no-repeat; color:#fff; padding:10px 20px 10px 15px; text-decoration:none; font-weight:bold;}
    .header .top_menu .contactus .current{ background:url(<?php echo $site_path; ?>images/contactus_hover.png) no-repeat; color:#fff; padding:10px 20px 10px 15px; text-decoration:none; font-weight:bold;}
    </style>
    <![endif]--> 
	<script type="text/javascript" src="<?php echo $site_path; ?>js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="<?php echo $site_path; ?>js/jquery.nivo.slider.js"></script>
    <script type="text/javascript" src="<?php echo $site_path; ?>js/site.js"></script>
    <script type="text/javascript">
		var main_menu = '<?php echo (isset($main_menu) && $main_menu != '')? $main_menu : ''; ?>';
		var current_tab = '<?php echo (isset($slug) && $slug != '')? $slug : ''; ?>';
	</script>
  <script type="text/javascript" src="<?php echo $site_path; ?>js/photo_gallery_lib/jquery.ad-gallery.js"></script>
  <script type="text/javascript">
  $(function() {
    /*$('img.image1').data('ad-desc', 'Whoa! This description is set through elm.data("ad-desc") instead of using the longdesc attribute.<br>And it contains <strong>H</strong>ow <strong>T</strong>o <strong>M</strong>eet <strong>L</strong>adies... <em>What?</em> That aint what HTML stands for? Man...');*/
    /*$('img.image1').data('ad-title', 'Title through $.data');
    $('img.image4').data('ad-desc', 'This image is wider than the wrapper, so it has been scaled down');
    $('img.image5').data('ad-desc', 'This image is higher than the wrapper, so it has been scaled down');*/
    var galleries = $('.ad-gallery').adGallery();
    /*setTimeout(function() {
      galleries[0].addImage("images/thumbs/t7.jpg", "images/7.jpg");
    }, 1000);
    setTimeout(function() {
      galleries[0].addImage("images/thumbs/t8.jpg", "images/8.jpg");
    }, 2000);
    setTimeout(function() {
      galleries[0].addImage("images/thumbs/t9.jpg", "images/9.jpg");
    }, 3000);
    setTimeout(function() {
      galleries[0].removeImage(1);
    }, 4000);*/
    
    $('#switch-effect').change(
      function() {
        galleries[0].settings.effect = $(this).val();
        return false;
      }
    );
    $('#toggle-slideshow').click(
      function() {
        galleries[0].slideshow.toggle();
        return false;
      }
    );
    $('#toggle-description').click(
      function() {
        if(!galleries[0].settings.description_wrapper) {
          galleries[0].settings.description_wrapper = $('#descriptions');
        } else {
          galleries[0].settings.description_wrapper = false;
        }
        return false;
      }
    );
  });
  </script>
    
    
    
</head>
<body>
    <div class="wrapper">
        <div class="header">
        	<?php include 'includes/menus/banner_header.php'; ?>
        </div> 
        <div class="content">
            <div class="left">
                <?php // include 'includes/menus/left_menu.php'; 
				include 'includes/ads/ad_216_240.php';
				?>
                <br class="clear"/>
                <?php 
				include 'includes/menus/ministry_logos.php';
                include 'includes/menus/left_menu.php';
				?>
            </div>
            <div class="single_middle">
                <br class="clear" />
                
                <div id="gallery" class="ad-gallery">
      <div class="ad-image-wrapper">
      </div>
      <div class="ad-controls">
      </div>
      <div class="ad-nav">
        <div class="ad-thumbs">
          <ul class="ad-thumb-list">
          
          	<?php
			
			$flickr = new flickr('82984482@N07','7ed59ac53cd1f823d6690008f08848d5');
			$images = $flickr->getImages(50);
			if($images === false) {
				echo 'Flickr Feed Unavailable';
			}
			else {
				foreach($images->photos->photo as $photo) {
					print_r($photo);
					continue;
					//echo '<a href="http://flickr.com/photos/' . $flickr->username . '/' . $photo->attributes()->id . '"><img src="http://farm' . $photo->attributes()->farm . '.static.flickr.com/' . $photo->attributes()->server . '/' . $photo->attributes()->id . '_' . $photo->attributes()->secret . '_z.jpg" /></a> ';
				?>
                <li>
                  <a href="<?php echo 'http://farm' . $photo->attributes()->farm . '.static.flickr.com/' . $photo->attributes()->server . '/' . $photo->attributes()->id . '_' . $photo->attributes()->secret . '_z.jpg'; ?>">
                    <img src="<?php echo 'http://farm' . $photo->attributes()->farm . '.static.flickr.com/' . $photo->attributes()->server . '/' . $photo->attributes()->id . '_' . $photo->attributes()->secret . '_s.jpg'; ?>" class="image0">
                  </a>
                </li>
                
                <?php
				}
			}
			?>
          </ul>
        </div>
      </div>
    </div>
                
                
                
                
                
            </div>
            <br class="clear" />
        </div>
	</div>
	<div class="content_bottom">&nbsp;</div>
	<?php include 'includes/footer.php'; ?> 
</body>
</html>