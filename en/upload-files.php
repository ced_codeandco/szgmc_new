<?php
if(isset($_FILES['uploadfile']['tmp_name']))
{
function string_to_filename($word) {
       $tmp = preg_replace('/^\W+|\W+$/', '', $word); // remove all non-alphanumeric chars at begin & end of string
	   $tmp = preg_replace('/[^a-zA-Z0-9-]/', ' ', $tmp);	  
       $tmp = preg_replace('/\s+/', '_', $tmp); // compress internal whitespace and replace with _	   
       return strtolower($tmp);
       }


$uploaddir = '../crew_documents/'; 
$time_t = date("Gis");  
$basename=$_FILES['uploadfile']['name'];
$extension = end(explode('.', $basename));
$filename = substr($basename, 0, strlen($basename) - strlen($extension) - 1); 
$filename=string_to_filename($filename).'.'.$extension;
$file = $uploaddir.$time_t.'_'.$filename; 
$file_name=$time_t.'_'.$filename;
$f_size=$_FILES["uploadfile"]["size"] / 1024;
if($f_size > 10240)
{
echo "file_big#".$file_name;
}
else 
{
if(move_uploaded_file($_FILES['uploadfile']['tmp_name'], $file)) { 
echo "success#".$file_name;
} 
else {
echo "error#".$file_name;
}
}
}
?>