<?php
include './includes/database.php';
include './includes/functions.php';
include './includes/config.php';
include_once('./includes/generic_functions.php');
$conf = new Configuration();
$db = new MyDatabase();
$site_path = $conf->site_url;
$slug = 'tours-booking-form';
$enq_mail_from='tour@szgmc.ae';
//$enq_mail_to='tour@szgmc.ae';
$enq_mail_cc= 'rashi@timegroup.ae';
$enq_mail_cc1= 'tahir@szgmc.ae';
$enq_mail_cc2= 'm.alali@szgmc.ae';
$enq_mail_to='manikandan@timegroup.ae';
define("ENQ_FROM",$enq_mail_from);
define("ENQ_TO",$enq_mail_to);
define("ENQ_CC",$enq_mail_cc);
$msg="";
$current_page = 'ftplogin';
if(isset($_POST['submitTour']) && isset($_POST['agree']) && $_POST['agree']=='1'){
    foreach($_POST as $name => $value) {
       $$name=mysql_real_escape_string($value);
    }
    saveToursData($_POST);
    $msg='before';
    $file_flag=0;
    if($msg == 'before'){
        
		/*
		$fromEmail = $_POST['email'];
        $fromName = ucfirst(strtolower($_POST['name']));
		*/
		$fromName='Tours';
        $fromEmail=ENQ_FROM;
		$toName='SZGMC';
		$toEmail="manikandansambu@gmail.com";
        $subject = "Group Tour Booking Request";
		
        $content_mail = getContentTours($_POST);
		
	   $content = getcatdetails($_POST);


		$output = mysql_real_escape_string($content);
		$date_visit = explode("/",$date_visit);
		$date_visit1 = $date_visit[2] . "-".$date_visit[1] . "-".$date_visit[0];
	          $name= $_REQUEST["name"];
              $sql_str="insert into tours(`name`,`group`,email,details,phone,contact_name,purposed_date,time,size,needs,contact_phone)values('$name','$o_g_name','$email','$output','$org_number','$contact_person_group','$date_visit1','$prop_time','$grp_size','$other_info','$m_number')";
              mysql_query($sql_str);	
        if(sendEmail($toName, $fromName, $toEmail, $fromEmail, $subject, $content_mail))
            $msg='success';
        else
            $msg='failed';
			
		//sendEmail($toName, $fromName, $enq_mail_cc1, $fromEmail, $subject, $content);
	   // sendEmail($toName, $fromName, $enq_mail_cc, $fromEmail, $subject, $content);
		//sendEmail($toName, $fromName, $enq_mail_cc2, $fromEmail, $subject, $content);
    }

}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?php include 'includes/common_header.php'; ?>
    <title>Online Group Tour Booking Form</title>
	 <link href="<?php echo $site_path; ?>css/calendar.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="<?php echo $site_path; ?>js/jquery.validate.js"></script>
	<script type="text/javascript" src="<?php echo $site_path; ?>js/main.js"></script>
	<script type="text/javascript" src="<?php echo $site_path; ?>js/cal.js"></script>
    <script type="text/javascript">
		var main_menu = '<?php echo (isset($main_menu) && $main_menu != '')? $main_menu : ''; ?>';
		var current_tab = '<?php echo (isset($slug) && $slug != '')? $slug : ''; ?>';
	</script>
	<script language="javascript" type="text/javascript">
function getDayOfDate() {
var givenDate=document.tourForm.date_visit.value;
weekDays=["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]
var dArray = givenDate.split("/");
// You have to write code that will check whether the date is valid or not because February cannot be 30 or 31, etc

// months in Javascript are 0-11
myDate=new Date(dArray[2],dArray[1]-1,dArray[0]);
var dayCode = myDate.getDay(); // dayCode 0-6
var dayIs=weekDays[dayCode]; //It will contain the required day, in this example it will be friday

$(document).ready(function () {
if(dayIs=='Friday')
$('.fri').show();
else
$('.fri').hide();

if(dayIs!='Friday')
$('.other').show();
else
$('.other').hide();

});


}

</script>
	<script type="text/javascript">
var myDate = new Date();
start_date=(myDate.getMonth() + 1)+ "/"+myDate.getDate()+ "/" + myDate.getFullYear();
end_date=(myDate.getMonth() + 1)+ "/" + myDate.getDate() + "/" + (myDate.getFullYear()+10);


$(document).ready(function () {
        $('#date_visit').simpleDatepicker({ chosendate: start_date, startdate: start_date, enddate: end_date });
		
        //$('#pub_date').simpleDatepicker({ chosendate: '1/1/1980', startdate: '1/1/1980', enddate: '1/1/2005' });
        $('#date_visit').focus(function() {
        $("#date_visit").click();
		 
        });

            $("#tourForm").validate({
			rules: {
				name: "required",
				contact_person_group:"required",
				group_category:"required",
				o_g_name:"required",
				grp_size:"required",
				org_number:"required",
				other_info:"required",
				email: {
					required: true,
					email: true
				},
				
				m_number: "required",
				date_visit: "required",
				prop_time: "required"
			}
		});



});
function check_additional_fileds(val)
{

if(val!=" ")
{
$(document).ready(function () {
if((val=='Education') || (val=='Educationarb'))
$('.edu').show();
else
$('.edu').hide();

if((val=='Government') || (val=='Governmentarb'))
$('.gov').show();
else
$('.gov').hide();
if((val=='Other') || (val=='Otherarb'))
$('.ot').show();
else
$('.ot').hide();
if((val=='Embassy') || (val=='Embassyarb'))
$('.emb').show();
else
$('.emb').hide();
if((val=='Travel Industry') || (val=='Travel Industryarb'))
$('.travel_industry_div').show();
else
$('.travel_industry_div').hide();


});
}
}

  </script>

</head><body>
    <?php include 'includes/menus/banner_header.php'; ?>
    <!-- Banner start -->
    <div class="banner">
	<img src="<?php echo $site_path; ?>images/visiting_the_mosque_banner.jpg">     
    </div>   
    <!-- Banner Close -->
    
    <div class="main_box_content">
            <?php include 'includes/menus/nav_menu.php'; ?>
        <div class="clear"></div>
        <div class="content">
             <div class="brad_cram">
            	<ul>
            	   <li><a href="<?php echo $site_path; ?>">Home</a></li>
                    <li><a href="#" class="active">Online Group Tour Booking Form</a></li>
                </ul>
            </div>
            <div class="content-left">
                <?php // include 'includes/menus/left_menu.php'; 
				include 'includes/ads/ad_216_240.php';
				?>
                <br class="clear"/>
                <?php 
				include 'includes/menus/ministry_logos.php';
                include 'includes/menus/left_menu.php';
				?>
            </div>
                <div class="content-right" style="margin-left:10px">
                      <div class="single_middle">
	            <h2>Tour Operator</h2>
                    <div class="page_item_single">
                    	<div class="page_content" >
                            <div class="general_body_content">
							 <div class="reg_form">
   
			<?php include '../forms/group_tour_booking.php'; ?>

    <div class="clear"></div>
</div>
							</div>
                            <br class="clear" />
                            
                        </div>
                    </div> 
                   
            </div> 
                    
                </div>
        </div>
    </div>

	<div class="content_bottom">&nbsp;</div>
	<?php include 'includes/footer.php'; ?> 
	<?php
	
	   if($msg == 'success'){
	  
		 ?>
		 <script> 
		  alert("لقد تم تقديم طلبك بنجاح ، وسوف نقوم بالتواصل معك قريبا \n Your request has been submitted successfully. We will contact you shortly."); 
          </script>
		 <?php
		}
		else if($msg == 'failed')
		{
		?>
		  <script> alert("Cannot process your request"); </script>
		 <?php
		}
	  
	?>
</body>
</html>