<?php


include 'includes/database.php';
include 'includes/functions.php';
include 'includes/config.php';
$conf = new Configuration();
$db = new MyDatabase();

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Sheikh Zayed Grand Mosque</title>
    <?php include 'includes/common_header.php'; ?>

</head>
<body>
<?php include 'includes/menus/banner_header.php'; ?>
    <!-- Banner start -->
    <div class="banner">
	<img src="<?php echo $site_path; ?>images/visiting_the_mosque_banner.jpg">     
    </div>   
    <!-- Banner Close -->
    
    <div class="main_box_content">
            <?php include 'includes/menus/nav_menu.php'; ?>
        <div class="clear"></div>
        <div class="content">
             <div class="brad_cram">
            	<ul>
            	   <li><a href="<?php echo $site_path; ?>">Home</a></li>
                    <li><a href="#" class="active">Cultural Guide</a></li>
                </ul>
            </div>
			<div class="content-left">
                <?php // include 'includes/menus/left_menu.php'; 
				include 'includes/ads/ad_216_240.php';
				?>
                <br class="clear"/>
                <?php 
				include 'includes/menus/ministry_logos.php';
                include 'includes/menus/left_menu.php';
				?>
            </div>
                <div class="content-right" style="margin-left:10px">
				<div class="single_middle" style="text-align:center">
                <br class="clear" />
                
                <h2>Cultural Guide</h2>
					<?php
				if(isset($_REQUEST["status"]))
				{
				$status = $_REQUEST["status"];
				$tour_id = $_REQUEST["tour_id"];
				$g_id = $_REQUEST["guid_id"];
				$sql = "select * from culture_guide_email where g_id=$tour_id and c_id=$g_id";
				$query = mysql_query($sql);
				if($res=mysql_fetch_object($query))
				{
				$cstatus = $res->e_status;
				}
				/*if($cstatus=="Mail sent")
				{*/
				
				if($status=="yes")
				{
				 $update_sql = "update culture_guide_email set e_status='Confirmed' where g_id=$tour_id and c_id=$g_id";
                 mysql_query($update_sql);
				 $emsg="Thank you for the confirmation";
				$amsg = "&#1606;&#1588;&#1603;&#1585;&#1603; &#1593;&#1604;&#1609; &#1578;&#1571;&#1603;&#1610;&#1583; &#1575;&#1604;&#1581;&#1580;&#1586;";
				}
				else
				{
				$update_sql = "update culture_guide_email set e_status='Declined' where g_id=$tour_id and c_id=$g_id";
                mysql_query($update_sql);
				$emsg="You have declined the tour guide request";
				$amsg = "&#1604;&#1602;&#1583; &#1602;&#1605;&#1578; &#1576;&#1585;&#1601;&#1590; &#1591;&#1604;&#1576; &#1575;&#1604;&#1573;&#1585;&#1588;&#1575;&#1583;";
				}
				
				//}
				/*else if($cstatus=="Confirmed")
				{
				$emsg="You have already confirmed this tour request";
				$amsg = "&#1606;&#1588;&#1603;&#1585;&#1603; &#1593;&#1604;&#1609; &#1578;&#1571;&#1603;&#1610;&#1583; &#1575;&#1604;&#1581;&#1580;&#1586;";
				}
				else if($cstatus=="Not Confirmed")
				{
				$emsg="You have already declined this tour request";
				$amsg = "&#1606;&#1588;&#1603;&#1585;&#1603; &#1593;&#1604;&#1609; &#1578;&#1571;&#1603;&#1610;&#1583; &#1575;&#1604;&#1581;&#1580;&#1586;";
				}*/
				}
				?>
				<p>&nbsp;</p>
				<div style="width:350px; float:left;">
				<h3><?php echo $emsg; ?></h3> </div>
				<div style="width:350px; float:right;">
				<h3><?php echo $amsg; ?></h3> 
</div>
            	<br class="clear" />
                
            <br class="clear" />
        </div>
				</div>
				</div>
				</div>
				
				
				
    
	<div class="content_bottom">&nbsp;</div>
	<?php include 'includes/footer.php'; ?> 
</body>
</html>