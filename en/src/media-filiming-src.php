<script type="text/javascript">
var myDate = new Date();
start_date=(myDate.getMonth() + 1)+ "/"+myDate.getDate()+ "/" + myDate.getFullYear();	
end_date=(myDate.getMonth() + 1)+ "/" + myDate.getDate() + "/" + (myDate.getFullYear()+10);	
//
//$(document).ready(function () {
//    $('#pub_date').simpleDatepicker({ chosendate: start_date, startdate: start_date, enddate: end_date });
//    //$('#pub_date').simpleDatepicker({ chosendate: '1/1/1980', startdate: '1/1/1980', enddate: '1/1/2005' });
//    $('#pub_date').focus(function() {
//    $("#pub_date").click();
//    });
//
//
//    $('#prop_date').simpleDatepicker({ chosendate: start_date, startdate: start_date, enddate: end_date });
//    //$('#pub_date').simpleDatepicker({ chosendate: '1/1/1980', startdate: '1/1/1980', enddate: '1/1/2005' });
//    $('#prop_date').focus(function() {
//    $("#prop_date").click();
//    });
//});


$(document).ready(function () {	
		var btnUpload=$('#c_passport_btn');
		var status=$('#c_status_msg');
		new AjaxUpload(btnUpload, {
			action: '/en/upload-file.php',
			name: 'uploadfile',
			onSubmit: function(file, ext){
				 if (! (ext && /^(jpg|png|jpeg|gif|pdf|doc|docx)$/.test(ext))){ 
                    // extension is not allowed 
					status.text('Only PDF,JPG, PNG, GIF, DOC or DOCX files are allowed');
				
					
					return false;
				}
				status.text('Uploading...');
			},
			onComplete: function(file, response){
				//On completion clear the status
				status.text('Processing..');
				response=response.split("#",2);
				if(response[0]==="success"){
				fnme=response[1];
	
					s_pos=fnme.indexOf('_',0)+1;
					len=fnme.length;
					fnme=fnme.substring(s_pos,len);					
					$("#c_up_fname").val(fnme);
					$("#c_up_fname_full").val(response[1]);					
				status.text('File has been uploaded successfully..');	
										
                                       
				}else if(response==="file_big"){
									status.text('File is too big...');	
                    
                                }
                                else{
								status.text('Error Found.. Please try later');	
					
				}
			}
		});

        $('#pub_date').simpleDatepicker({ chosendate: start_date, startdate: start_date, enddate: end_date });
        //$('#pub_date').simpleDatepicker({ chosendate: '1/1/1980', startdate: '1/1/1980', enddate: '1/1/2005' });
        $('#pub_date').focus(function() {
        $("#pub_date").click();
        });


        $('#prop_date').simpleDatepicker({ chosendate: start_date, startdate: start_date, enddate: end_date });
        //$('#pub_date').simpleDatepicker({ chosendate: '1/1/1980', startdate: '1/1/1980', enddate: '1/1/2005' });
        $('#prop_date').focus(function() {
        $("#prop_date").click();
        });


        $("#c_add_crew").click(function (){

            v_fname=$("#c_fname").val();
            v_ftitle=$("#c_ftitle").val();
            v_address=$("#c_address").val();
            v_tel=$("#c_telephone").val();
            v_email=$("#c_email").val();
            v_file=$("#c_up_fname_full").val();
//            alert(v_email);
            if(v_fname == ''){
                if(v_ftitle == '')
                    $("#c_ftitle_error").css({'display' : ''});
                $("#c_fname_error").css({'display' : ''});
                return false;
            }
            if(v_ftitle == ''){
                $("#c_ftitle_error").css({'display' : ''});
                return false;
            }


            pars='fname='+v_fname;
            pars+='&ftitle='+v_ftitle;
            pars+='&address='+v_address;
            pars+='&tel='+v_tel;
            pars+='&email='+v_email;
            pars+='&cfile='+v_file;


            var status=$('#c_status_msg');
            status.text('Please wait...');

            crew_submit(pars);
            //alert(pars);
            //p_file_name=$("#c_passport").val();
            //$("#temp_c_passport").val(p_file_name);
        });

});

function crew_submit(pars)
{
var status=$('#c_status_msg');
$.ajax({
   type: "POST",
   url: "/en/src/submit_crew.php",
   data: pars,
   success: function(result){       
	    if(result!='error'){
                    $('#crew_table').html(result)
                    if(pars!='')
                        status.text('New entry has been added');
                    $("#c_fname").val('');
                    $("#c_ftitle").val('');
                    $("#c_address").val('');
                    $("#c_telephone").val('');
                    $("#c_email").val('');
                    $("#c_up_fname_full").val('');
                    $("#c_up_fname").val('');					
					
		}
   },
   complete: function(){   },
   error: function(){   }   
 });
}

$(document).ready(function () {	
crew_submit('');
});

function delete_crew(c_id)
{
var status=$('#c_status_msg');
$.ajax({
   type: "POST",
   url: "/en/src/delete_crew.php",
   data: 'crew_id_del='+c_id,
   success: function(result){       
	    if(result!='error')
		{
		status.text('Crew deleted');
		}
   },
   complete: function(){  crew_submit(''); },
   error: function(){ status.text('Could not detele at this moment');   }   
 });

}


</script>


<script type="text/javascript">

  $(document).ready(function(){
    $("#mediaForm").validate({
			rules: {
				a_fname: "required",
				email: {
					required: true,
					email: true
				},
				a_nationality: "required",
				a_h_fname: "required",
				a_h_nationality: "required",
				a_company: "required",
				num_persons_expected: "required",
				purpose: "required",
				//a_doc: "required",
				pub_date: "required",
				prop_date: "required",
				prop_time: "required",
				contact_num: "required",
//				special_req: "required",
				agree: "required",
                                c_email:{
                                    email: true
                                }

	  								
			}
		});
  });
  </script>




<div class="right_panel">

<div class="reg_form">
    <div style="padding: 2px;">
 
<p>            
Thank you for your interest in the Sheikh Zayed Grand Mosque. In order to facilitate your media request, we ask you to fill out the form below with all of the required details. All bookings must be madeat least 7 days before date of visit. Please follow the booking instructions below:
</p>
<p><b>
We will respond to your request by email once we have received your application. Please note that our office working hours are from Sunday to Thursday, 8am – 4pm and all requests will be answered within 48 working hours.
</b></p>

    </div>

<form action="/en/media-form" method="post" id="mediaForm" name="mediaForm" enctype="multipart/form-data">


<div class="input_row even">
<div class="input_label_div"><span>Applicant Full Name </span><span class="red">*</span><br/><span><i>(as appears on passport)</i></span></div>
<div class="input_div"> <input type="text" name="a_fname" id="a_fname" class="input_cls"/>  </div>
</div>

<div class="input_row odd">
<div class="input_label_div"><span>Applicant Nationality</span><span class="red">*</span> </div>
<div class="input_div"> <input type="text" name="a_nationality" id="a_nationality" class="input_cls"/>  </div>
</div>

<div class="input_row even">
<div class="input_label_div"><span>Head of Film Crew Full Name</span><span class="red">*</span><br/><span><i>(as appears on passport)</i></span></div>
<div class="input_div"> <input type="text" name="a_h_fname" id="a_h_fname" class="input_cls"/>  </div>
</div>


<div class="input_row odd">
<div class="input_label_div"><span>Head of Film Crew Nationality</span><span class="red">*</span></div>
<div class="input_div"> <input type="text" name="a_h_nationality" id="a_h_nationality" class="input_cls"/>  </div>
</div>

<div class="input_row even">
<div class="input_label_div"><span>Number of persons expected to be in the crew</span><span class="red">*</span></div>
<div class="input_div"> <input type="text" name="num_persons_expected" id="num_persons_expected" class="input_cls"/>  </div>
</div>


<div class="input_row odd">
<div class="input_label_div"><span>Company Name</span><span class="red">*</span></div>
<div class="input_div"> <input type="text" name="a_company" id="a_company" class="input_cls"/>  </div>
</div>

<div class="input_row even">
<div class="input_label_div">Contact Number </div>
<div class="input_div"> <input type="text" name="contact_num" id="contact_num" class="input_cls"/>  </div>
</div>

<div class="input_row odd">
<div class="input_label_div">Email Address</div>
<div class="input_div"> <input type="text" name="email" id="email" class="input_cls"/>  </div>
</div>


<div class="input_row even">
<div class="input_label_div"><span>Company Name commissioning the film</span><span class="red">*</span> <br/><span><i>(if different from above)</i></span></div>
<div class="input_div"> <input type="text" name="a_comp_comm" id="a_comp_comm" class="input_cls"/>  </div>
</div>

<div class="input_row odd">
<div class="input_label_div"><span>Purpose of the Film</span><span class="red">*</span> <br/>
<span><i>Note: Please include a brief description, language, circulation, target audience, and editorial angle.</i></span>
</div>
<div class="input_div">  <textarea class="input_cls" style="width:250px;" name="purpose" id="purpose" cols="25" rows="8" ></textarea></div>
</div>

<div class="input_row even">
<div class="input_label_div"><span>A short introduction for the content of the film:</span><span class="red">*</span> 
</div>
<div class="input_div">  <textarea class="input_cls" style="width:250px;" name="short_intro" id="short_intro" cols="25" rows="8" ></textarea></div>
</div>



<div class="input_row odd" >

<div class="input_label_div"><span>Equipment &ndash; provide details of any equipment being used during the shoot
<img src="/img/questions.png" style="cursor:pointer; vertical-align:middle;" class="vtip" alt="?" title="<b>The architecture of the Sheikh Zayed Grand Mosque such as marble, carpets are extremely delicate, so we must ensure that the equipment is not going to destroy the surfaces.</b><br/>
Mandatory consideration are:<br/>
<span>&bull; All equipment must have protective coasting along the bottom (such as felt etc.) – this means tripods, cameras, equipment boxes)</span>
<span>&bull; Tracking and other similar equipment must be pre-approved before allowed.</span>
<span>&bull; Any extremely heavy equipment must also be pre-approved</span>" />

</span></div>
<div class="input_div">  <textarea class="input_cls" style="width:250px;" name="equipments" id="equipments" cols="25" rows="8" ></textarea></div>
</div>





<div class="input_row even">
<div class="input_label_div">Upload attachment </div>
<div class="input_div"> 
    <input type="file" name="a_doc" id="a_doc" style="border : 0px outset #88A0C8; font-family: Arial;  font-size: 11px;  color: #003068;  text-decoration: none;  background-color: #E9EDF0;"/>
</div>
</div>


<div class="input_row odd">
<div class="input_label_div">Expected coverage date</div>
<div class="input_div"> <input dir="ltr" type="text"  name="pub_date" id="pub_date" readonly="readonly"  class="input_cls"/>  </div>
</div>


<div class="input_row even">
<div class="input_label_div"><span>Proposed Date and Time of Visit</span><span class="red">*</span> <br/><span><i>(Between Sun – Thurs, 9am-4pm) </i></span> </div>
<div class="input_div1"> 
<div style="float:left; width:125px;"><input type="text" name="prop_date" id="prop_date" class="input_cls_2" />  Date</div>
<div style="float:right; width:125px;">
<select name="prop_time" id="prop_time" class="input_cls_2" >
<option value=""></option>
<option value="09:00:00">9:00</option>
             <option value="09:30:00">9:30</option>
            <option value="10:00:00">10:00</option>
            <option value="10:30:00">10:30</option>
            <option value="11:00:00">11:00</option>
            <option value="11:30:00">11:30</option>
            <option value="12:00:00">12:00</option>
            <option value="12:30:00">12:30</option>
            <option value="13:00:00">13:00</option>
            <option value="13:30:00">13:30</option>
           <option value="14:00:00">14:00</option>
           <option value="14:30:00">14:30</option>
           <option value="15:00:00">15:00</option>
           <option value="15:30:00">15:30</option>
           <option value="16:00:00">16:00</option>
           <option value="16:30:00">16:30</option>
             <option value="17:00:00">17:00</option>
           <option value="17:30:00">17:30</option>
           <option value="18:00:00">18:00</option>
           <option value="18:30:00">18:30</option>
           <option value="19:00:00">19:00</option>
           <option value="19:30:00">19:30</option>
           <option value="20:00:00">20:00</option>
</select> Time
 </div>
</div>
</div>


<div class="input_row odd">
<div class="input_label_div"><span>Other information
<img src="/img/questions.png" style="cursor:pointer; vertical-align:middle;" class="vtip" alt="?" title="Include any other details or requests that will support your application."/>
</span>
</div>
<div class="input_div">  <textarea class="input_cls" style="width:250px;" name="other_info" id="other_info" cols="25" rows="8" ></textarea></div>
</div>




<div class="special_dev" >
<div class="input_row_special">
<b>Names of all persons and job roles in filming crew: </b>
</div>


<div class="input_row_special">
<div class="input_label_div">Full name </div>
<div class="input_div">
    <input type="text" name="c_fname" id="c_fname" class="input_cls"/>
    <label id="c_fname_error" style="display: none; color:#CC0000; direction:ltr; float:left; font-size:12px;" >*</label>
</div>
</div>

<div class="input_row_special">
<div class="input_label_div">Job title </div>
<div class="input_div">
    <input type="text" name="c_ftitle" id="c_ftitle" class="input_cls"/>
    <label id="c_ftitle_error" style="display: none; color:#CC0000; direction:ltr; float:left; font-size:12px;" >*</label>

</div>
</div>

<div class="input_row_special">
<div class="input_label_div">Address </div>
<div class="input_div"> <input type="text" name="c_address" id="c_address" class="input_cls"/>  </div>
</div>

<div class="input_row_special">
<div class="input_label_div">Telephone number </div>
<div class="input_div"> <input type="text" name="c_telephone" id="c_telephone" class="input_cls"/>  </div>
</div>

<div class="input_row_special">
<div class="input_label_div">Email address</div>
<div class="input_div"> <input type="text" name="c_email" id="c_email" class="input_cls"/>  </div>
</div>

<div class="input_row_special">
    <div class="input_label_div">Copies of passport for each crew member </div>
    <div class="input_div">
        <input type="text" class="input_cls" name="c_up_fname" id="c_up_fname" readonly="readonly" disabled/>
        <input type="hidden" name="c_up_fname_full" id="c_up_fname_full"/>
        <input type="button" value="Select" name="c_passport_btn" id="c_passport_btn" style="width:75px;"/>
    </div>
</div>
<div style="padding:20px 130px 10px 0px;float: right;"><input type="button" name="c_add_crew" id="c_add_crew" value="Add"/></div>
<div class="clear" style=" color:#996600; text-align:left; direction:ltr" id="c_status_msg"></div>

<div class="clear" id="crew_table" style="width:500px; overflow:auto;"></div>



</div>




<div class="input_row even" style="text-align:left">
   
*Please note that this media request does not constitute a confirmed booking. We will inform you when your media visit application has been accepted and should we need any further information. 
<div class="clear"></div>
        I agree to the <a href="Terms_and_Conditions.pdf" target="_blank" style="color: #88A0C8">terms and conditions</a>

        

<input type="checkbox" name="agree" id="agree" value="1" style="float:left"/>
</div>


<div class="input_row odd">
<div style="padding-left:230px;">
<input type="submit" value="Submit" />
</div>
</div>


</form>
<div class="clear"></div>
</div>

<div class="clear"></div>
</div>