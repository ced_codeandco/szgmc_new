<?php
$mydb=new connect;
$p_id='';
//if(CURRENT_URL === '/tours.php' || CURRENT_URL === '/media-filiming-information-registration.php'){
//    define('CURRENT_URL', '/gramd-mosque-tourist-information');
//}
$p_obj=get_page_by_page_id(CURRENT_URL);
//print_r(CURRENT_URL);
//var_dump($p_obj);
if(isset($_SESSION['admin_logged']) && $_SESSION['admin_logged']!='')
$admin_common_sql="m.visibility='A' || m.visibility='B' ";
else
$admin_common_sql="m.visibility='P' || m.visibility='B' ";

$sql="select m.*,p.url,p.p_id from header_top_nav m LEFT JOIN pages p ON m.p_id=p.p_id where ($admin_common_sql)  GROUP BY m.hm_id ORDER BY m.pos desc";
$rs=$mydb->query($sql);
$top_menu_obj=$mydb->loadResult();

$sql="select m.*,p.url,p.p_id from sub_menu_step1 m LEFT JOIN pages p ON m.p_id=p.p_id where ($admin_common_sql)  GROUP BY m.sm1_id ORDER BY m.pos";
$rs=$mydb->query($sql);
$sub_menu_obj_1=$mydb->loadResult();

$sql="select m.*,p.url from sub_menu_step2 m LEFT JOIN pages p ON m.p_id=p.p_id where ($admin_common_sql)  GROUP BY m.sm2_id ORDER BY m.pos";
$rs=$mydb->query($sql);
$sub_menu_obj_2=$mydb->loadResult();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo htmlspecialchars(TITLE); ?></title>
<meta name="description" content="<?php echo DESC; ?>"/>
<meta name="keywords" content="<?php echo KEYWORD; ?>"/>
<meta name="copyright" content="Grand Mosque"/>
<meta name="author" content="Grand Mosque" />
<meta name="copyright" content="2010 Grand Mosque" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link href="/en/css/styles.css" rel="stylesheet" type="text/css" />
<link href="/en/css/calendar.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/en/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript" src="/en/js/ajaxupload.3.5.js"></script>
<script type="text/javascript" src="/en/js/jquery.validate.js"></script>
<script type="text/javascript" src="/en/js/cal.js"></script>
<script type="text/javascript" src="/en/js/main.js"></script>
<script type="text/javascript" src="/en/js/AC_RunActiveContent.js"></script>
<script type="text/javascript" src="/en/js/swfobject.js"></script>
</head>
<body>

<div class="container">
<div class="header_top">

<div style="float:right">
<div class="date_wheather">
<div class="date_time_now">
<?php
$dt=date("D, M j Y h:i a ");
$todayDate = date("Y-m-d g:i a");// current date
$currentTime = time($todayDate); //Change date into time
$new_time = $currentTime+60*180;
echo date("D, M j Y h:i a",$new_time);
//echo $dt;
?>
</div>
<div class="clear"></div>
<div class="wheather">
<div class="wheather_inner"><?php echo weather();?></div>
</div>
</div>
<div style="padding:4px;float: right; font-weight: bold;"><a href="/index" >Ø¹Ø±Ø¨ÙŠ</a></div>

<div class="clear"></div>
<div style="float:left; padding-top:15px;" dir="rtl">
<h1>
<a href="/index"><img src="/en/img/grand_mosque.jpg" alt="Sheikh Zayed Grand Mosque Center"/></a>
<!--
Ù…Ø³Ø¬Ø¯ Ø§Ù„Ø´ÙŠØ® Ø²Ø§ÙŠØ¯ Ø§Ù„ÙƒØ¨ÙŠØ± -->
</h1>
</div>
</div>

<div class="grand_mosque_logo">
<a href="/index"><img src="/en/img/logo.jpg" alt="Grand Mosque Logo"/></a>
</div>
</div>
<div class="clear"></div>
</div>


<div class="container">
<div class="top_nav">
<div class="right">
<form method="get" action="/en/search.php">
<label><input type="submit" value="Search" class="search_btn" dir="ltr"></label><input type="text" name="key">
</form>
</div>

<div class="left">
<div class="menu_bar">
<ul id="menu_top">
<li><a title="Home" style="border-left: 0px none;" href="/index">Home</a></li>
<li><a href="/en/contact-us">Contact Us</a></li>
<!--
<li><a  href="#">Ø®Ø§Ø±Ø·Ø© Ø§Ù„Ù…ÙˆÙ‚Ø¹</a></li>
-->

</ul>
</div>
</div>
<div class="clear"></div>
</div>

<div class="clear"></div>
<div id="flash_banner"></div>
<script type="text/javascript">	
var fo = new SWFObject("/flash/header.swf", "viewer", "753", "293", "9.0.28", "#D1CAAD");	
fo.addParam("allowFullScreen","true");
fo.write("flash_banner");			
</script>
<div class="clear"></div>

<div class="top_nav">
<div class="left">
<div class="menu_bar">
<ul id="menu_top2">
<?php
$sql="select m.*,p.url,p.p_id from header_top_nav m LEFT JOIN pages p ON m.p_id=p.p_id  where ($admin_common_sql) and m.live='Y'  GROUP BY m.hm_id ORDER BY m.pos ASC";
$rs=$mydb->query($sql);
$top_menu_obj=$mydb->loadResult();
foreach($top_menu_obj as $key => $m1_obj){ /* looop through all parent menu*/
if($m1_obj->url=='')
$m1_obj->url='/';
echo '<li><a  title="'.$m1_obj->title.'"  href="/en'.$m1_obj->url.'">'.$m1_obj->hm_name.'</a></li>';
}
?>
</ul>
</div>
</div>
<div class="clear"></div>
</div>

<div class="clear"></div>
<div id="content_inner">

<div class="right arrow"><a title="Index" href="/en/index">Home</a></div>
<div class="right arrow"><a href="/en/grand-mosque-in-media">Media</a></div>
<div class="right arrow"><a href="/en/news-list">News</a></div>

<div class="clear"></div>
<div class="left_panel <?php echo $sp_cls;?>">
<div id="left_navs">
<ul>
<li><a href="/en/news-list" title="News">News</a></li><li><a href="/en/media" title="Media Tour">Media Tour</a><ul><li><a href="/en/media-bokking-form" title="Book a Media  Tour">Book a Media  Tour</a></li></ul></li><li><a href="/en/media-filiming-information" title="Filming Information">Filming Information</a><ul><li><a href="/en/media-form" title="Filming Permission Application Online">Filming Permission Application Online</a></li></ul></li><li><a href="/en/grand-mosque-in-media" title="Grand Mosque in Media">Grand Mosque in Media</a></li><li><a href="/en/video-gallery" title="Video Gallery">Video Gallery</a></li><li><a href="/en/gallery.php" title="Photo Gallery">Photo Gallery</a></li></ul>
</div>
</div>