<?php
error_reporting(0);
include './includes/database.php';
include './includes/functions.php';
include './includes/config.php';
include_once('./includes/generic_functions.php');
$conf = new Configuration();
$db = new MyDatabase();
$site_path = $conf->site_url;

$slug = explode('/',$_SERVER['REQUEST_URI']);
$slug = end($slug);
$msg="";
session_start();
$current_page = 'ftplogin';
if((isset($_SESSION['user_id'])) && $_SESSION['user_id']!=''){
		if(isset($_POST['cur_password']) && $_POST['cur_password']!='' && isset($_POST['a_password']) && $_POST['a_password']!='' && isset($_POST['submitchangePassword']) ){
		
			foreach($_POST as $name => $value) {
				$$name=mysql_real_escape_string($value);
			}
			$user_id=$_SESSION['user_id'];
			$sql="select * from site_users where id='$user_id' and active=1";
			$result=mysql_query($sql);
			while($obj=mysql_fetch_object($result)){
				$pswd=$obj->password;
			}

			if($pswd == $_POST['cur_password'] ){			
				
				$sql="update site_users set password='$a_password' where id='$user_id' and active=1 limit 1";
				mysql_query($sql);
				$msg='success';
				$_SESSION['user_password']= $a_password;
			}else{
				$msg="Invalid";
			}
			
			
		}
	
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php include 'includes/common_header.php'; ?>
    <title>Change Password</title>

	<link href="<?php echo $site_path; ?>css/calendar.css" rel="stylesheet" type="text/css" />

 

	<script type="text/javascript" src="<?php echo $site_path; ?>js/ajaxupload.3.5.js"></script>
	<script type="text/javascript" src="<?php echo $site_path; ?>js/jquery.validate.js"></script>
	<script type="text/javascript" src="<?php echo $site_path; ?>js/main.js"></script>
	<script type="text/javascript" src="<?php echo $site_path; ?>js/cal.js"></script>
    
<link href="<?php echo $site_path; ?>css/jquery-ui-1.8.24.custom.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo $site_path; ?>js/jquery-ui-1.8.24.custom.min.js"></script>

<script type="text/javascript">

  $(document).ready(function(){
    $("#changePasswordForm").validate({
			rules: {
				cur_password: "required",
				a_password: "required",
				a_rptpassword: "required"
			}
		});
  });
   $(document).ready(function(){
	 $('#submitchangePassword').click(function(event){
        if($('#a_password').val() != $('#a_rptpassword').val()) {
            alert("Password and Confirm Password don't match");
            // Prevent form submission
            event.preventDefault();
        }
         
    });							  
   });
 
</script>
<style>
td{text-align:center;}
textarea{width:660px;}
</style>

</head>
<body>
<?php include 'includes/menus/banner_header.php'; ?>
    <!-- Banner start -->
    <div class="banner">
	<img src="<?php echo $site_path; ?>images/visiting_the_mosque_banner.jpg">     
    </div>   
    <!-- Banner Close -->
    
    <div class="main_box_content">
            <?php include 'includes/menus/nav_menu.php'; ?>
        <div class="clear"></div>
        <div class="content">
             <div class="brad_cram">
            	<ul>
            	   <li><a href="<?php echo $site_path; ?>">Home</a></li>
                    <li><a href="#" class="active">Change Password</a></li>
                </ul>
            </div>
			<div class="content-left">
                <?php // include 'includes/menus/left_menu.php'; 
				include 'includes/ads/ad_216_240.php';
				?>
                <br class="clear"/>
                <?php 
				include 'includes/menus/ministry_logos.php';
                include 'includes/menus/left_menu.php';
				?>
            </div>
                <div class="content-right" style="margin-left:10px">
				<div class="single_middle">
                    <h2>Change Password</h2>
                    <div class="page_item_single">
                    	<div class="page_content" >
                            <div class="general_body_content">
                                 <div class="reg_form">
                                 
									<?php include '../forms/changePassword.php'; ?>                                
                                    <div class="clear"></div>
                                </div>
							</div>
                            <br class="clear" />
                            
                        </div>
                    </div> 
                   
            </div>
				</div>
				</div>
				</div>
				


    
	<div class="content_bottom">&nbsp;</div>
	<?php include 'includes/footer.php'; ?> 
	<?php
	
	   if($msg == 'success'){
	  
		 ?>
		<script>
			$(function() {
				$( "#dialog" ).dialog();
			});
		</script>
        <script type="text/javascript">
		$(function(){
			$('#confirm').click(function() {
				//$('#dialog').hide();
				$( "#dialog" ).dialog( "close" );
				window.location = "/en/user_profile.php";
				return false;
			});
		});
		
        </script>
        <div id="dialog">
        	<p style="text-align:right;">لقد تم تغيير كلمة المرور بنجاح</p>
            <p style="padding-top:5px;"><span dir="ltr">Your password has been changed successfully</span></p>
            <p style="padding-top:5px;"><input type="button" value="OK" style="float:right;width:40px" id="confirm"></p>
        </div>
		 <?php
		 
		}
		else if($msg == 'failed')
		{
		?>
		  <script> alert("Cannot process your request"); </script>
		 <?php
		  print('<script type="text/javascript">window.location = "/en/user_profile.php";</script>');
exit(); 
		}
		 
		else if($msg == 'Invalid')
		{
		?>
		  <script> alert("The Old password you gave is incorrect"); </script>
		 <?php
		  print('<script type="text/javascript">window.location = "/en/user_profile.php";</script>');
exit(); 
		}
	  
	?>
</body>
</html>