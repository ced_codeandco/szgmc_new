<?php
include 'includes/database.php';
include 'includes/functions.php';
include 'includes/config.php';
include 'includes/flickr.php';




$slug = explode('/',$_SERVER['REQUEST_URI']);
$slug = end($slug);
$conf = new Configuration();
$db = new MyDatabase();

$site_path = $conf->site_url;

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
        
<head>
     <title>Photo Gallery - Sheikh Zayed Grand Mosque Center</title>
    <link rel="stylesheet" type="text/css" href="<?php echo $site_path; ?>css/photo_gallery/jquery.ad-gallery.css">
    <?php include 'includes/common_header.php'; ?>
    <script type="text/javascript" src="<?php echo $site_path; ?>js/photo_gallery_lib/jquery.ad-gallery.js"></script>
</head>


<body>
      	<?php include 'includes/menus/banner_header.php'; ?>
        
       


<!-- Banner Start -->
<div class="banner">
	<img src="<?php echo $site_path; ?>images/visiting_the_mosque_banner.jpg">     
</div>

<!-- Content Start -->
	<div class="main_box_content">
            <?php include 'includes/menus/nav_menu.php'; ?>
        <div class="clear"></div>
        <div class="content">        	
            <div class="brad_cram">
            	<ul>
            	   <li><a href="<?php echo $site_path; ?>">Home</a></li>
                    <li><a href="#" class="active">Photo Gallery</a></li>
                </ul>
            </div>
	       <div class="gallery" id="gallery" >
		    
		    <?php
			
			$flickr = new flickr('82986622@N03','7ed59ac53cd1f823d6690008f08848d5');
			$images = $flickr->getImages(100);
			if($images === false) {
				echo 'Flickr Feed Unavailable';
			}
			else {
			$count = 0;
			foreach($images->photos->photo as $photo) {
				$count++;
			}
			
			$totrow = ceil($count / 4);
			?>
            <div class="gallery-div">
				<?php 
				$countInner = 0;
				$countMail = 0;
				foreach($images->photos->photo as $photo) { 
				$countInner++;
				//echo 'http://farm' . $photo->attributes()->farm . '.static.flickr.com/' . $photo->attributes()->server . '/' . $photo->attributes()->id . '_' . $photo->attributes()->secret . '_z.jpg';
				?>
					  <a href="<?php echo 'http://farm' . $photo->attributes()->farm . '.static.flickr.com/' . $photo->attributes()->server . '/' . $photo->attributes()->id . '_' . $photo->attributes()->secret . '_z.jpg'; ?>" class="lightbox_popup"><img src="<?php echo 'http://farm' . $photo->attributes()->farm . '.static.flickr.com/' . $photo->attributes()->server . '/' . $photo->attributes()->id . '_' . $photo->attributes()->secret . '_m.jpg'; ?>" title="<?php echo $photo->attributes()->title; ?>"></a>              
                <?php 
					if($countInner == $totrow){
						$countInner = 0;
						$countMail++;
						if($countMail == 3){
							$endDiv = ' gallery-div-end';
						}else{
							$endDiv = '';
						}
						echo '</div><div class="gallery-div'.$endDiv.'">';				
					}
				}?>
            </div>
            <?php } ?>
		    
	       
	       </div>
    
      
        </div>
    
    <div class="clear"></div>
	</div>
    
 <div class="clear"></div>  
<!-- Content Close -->
<?php    include 'includes/menus/ministry_logos_without_mosque.php';    ?>
<div class="content_bottom">&nbsp;</div>
	<?php include 'includes/footer.php'; ?> 
        
</body>
    
   
</html>