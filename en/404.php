<?php
include 'includes/database.php';
include 'includes/dal/news.php';
include 'includes/dal/pages.php';
include 'includes/functions.php';
include 'includes/config.php';

$current_page = 'index';

$db = new MyDatabase();
$conf = new Configuration();
$site_path = $conf->site_url;


$conf->site_description = 'The board members of the Sheikh Zayed Grand Mosque in Abu Dhabi. Find all about the Sheikh Zayed Grand Mosque in Abu Dhabi including, visiting timings, how to get to the mosque, dress code, tours, history, architecture and more.';

$conf->site_keywords = 'grand mosque, sheikh zyed grand mosque, mosque in adu dhabi, what to wear in mosque, how to get to mosque, history of grand mosque, architecture of grand mosque, grand mosque photos, board members';

$dal_news = new ManageNews();
$dal_pages = new ManagePages(); //for message from minister of presedential affairs
$page = $dal_pages->getPage('', 70); // id must be 119 for message from minister, if different then use that
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Sheikh Zayed Grand Mosque Centre Abu Dhabi</title>
    <?php include 'includes/common_header.php'; ?>
</head>

<body>
<?php include 'includes/menus/banner_header.php'; ?>
    <!-- Banner start -->
    <div class="banner">
	<img src="<?php echo $site_path; ?>images/visiting_the_mosque_banner.jpg">     
    </div>   
    <!-- Banner Close -->
    
    <div class="main_box_content">
            <?php include 'includes/menus/nav_menu.php'; ?>
        <div class="clear"></div>
        <div class="content">
             <div class="brad_cram">
            	<ul>
            	   <li><a href="<?php echo $site_path; ?>">Home</a></li>
                    <li><a href="#" class="active">Page Not Found</a></li>
                </ul>
            </div>
			<div class="content-left">
				<div class="content-left">
					<div class="content-left-box">
					<div class="content-left-box-txt-menu">
					<div class="content-left-box-txt-menu-left"><a href="<?php echo $site_path; ?>visiting-the-mosque">Visiting The Mosque</a></div>
					<div class="content-left-box-txt-menu-right"><a href="<?php echo $site_path; ?>visiting-the-mosque"><img src="<?php echo $site_path; ?>images/button.png" width="33" height="38" /></a></div>
					</div>
					<img src="<?php echo $site_path; ?>images/visiting-mosque-image.jpg" width="210" height="98" />
					</div>
					
					<div class="content-left-box-round-corner">
        	<div class="prayer-timing-title-div">Prayer Timings</div>
			<div class="prayer-timing-div prayer_timing">Abhu Dhabbi Timings</div>
            <div class="prayer-timing-div prayer_timing">Fajr <span>05:19 am</span></div>
			<div class="prayer-timing-div prayer_timing">Sunrise <span>06:45 am</span></div>
			<div class="prayer-timing-div prayer_timing">Dhuhr <span>12:09 pm</span></div>
			<div class="prayer-timing-div prayer_timing">Asr <span>03:19 pm</span></div>
			<div class="prayer-timing-div prayer_timing">Maghrib <span>05:35 pm</span></div>
			<div class="prayer-timing-div prayer_timing">Isha <span>07:05 pm</span></div>
            <div class="current-whether-title-div">Current Whether</div>
            <div class="current-whether-div"><?php include 'includes/menus/sub_menu_weather.php'; ?><div class="weather"></div>39 &deg;C <span><img src="<?php echo $site_path; ?>images/sun-ico.png" width="23" height="23" /></span></div>
        </div>
        
        <div class="content-left-box">
        <div class="content-left-box-txt-menu">
        <div class="content-left-box-txt-menu-left"><a href="<?php echo $site_path; ?>religious-programs">Religious Programs</a></div>
        <div class="content-left-box-txt-menu-right"><a href="<?php echo $site_path; ?>religious-programs"><img src="<?php echo $site_path; ?>images/button.png" width="33" height="38" /></a></div>
        </div>
        <img src="<?php echo $site_path; ?>images/religious-pograms-inside-image.jpg" width="210" height="98" />
        </div>
        
        <div class="content-left-box">
        <div class="content-left-box-txt-menu">
        <div class="content-left-box-txt-menu-left"><a href="<?php echo $site_path; ?>">Tour Operators Login</a></div>
        <div class="content-left-box-txt-menu-right"><a href="<?php echo $site_path; ?>"><img src="<?php echo $site_path; ?>images/button.png" width="33" height="38" /></a></div>
        </div>
        <img src="<?php echo $site_path; ?>images/tour-operators-login-image.jpg" width="210" height="98" />
        </div>
        
        <div class="content-left-box">
        <div class="content-left-box-txt-menu">
        <div class="content-left-box-txt-menu-left"><a href="<?php echo $site_path; ?>">Zayed Mosque App</a></div>
        <div class="content-left-box-txt-menu-right"><a href="<?php echo $site_path; ?>"><img src="<?php echo $site_path; ?>images/button.png" width="33" height="38" /></a></div>
        </div>
        <img src="<?php echo $site_path; ?>images/zayed-mosque-app-image.jpg" width="210" height="98" />
        </div>
    </div>
					
                <?php // include 'includes/menus/left_menu.php'; 
				include 'includes/ads/ad_216_240.php';
				?>
                <br class="clear"/>
                <?php 
				include 'includes/menus/ministry_logos.php'; 
				include 'includes/menus/left_menu.php';  
				?>
            </div>
                <div class="content-right" style="margin-left:30px">
					<div class="general_body_content" style="background:none; border:0; margin-left:10px;">
	   <h2>Page Not Found</h2>

       <p style="color:#656565; font-size:16px; font-weight:bold; padding-top:20px;">Sorry, we can't find the page you requested.</p>
	   <p style="color:#656565; font-size:16px; font-weight:bold; padding-top:10px;">Now what?</p>
       <p style="padding-top:10px;color:#646464;">Here are some things you can try:</p>
	   <ul style="color:#646464; margin-left:20px;"  >
           <li style="background:none; border:0; padding:5px; list-style:disc;">If you typed the URL, check your accuracy and try again </li>
           <li style="background:none; border:0;padding:5px;list-style:disc;">Take a look at the <a href="/en/sitemap" style="color:#e93b45; text-decoration:none;">Site Map</a></li>
           <li style="background:none; border:0;padding:5px;list-style:disc;"> <a href="/en/contact-us" style="color:#e93b45; text-decoration:none;">Contact Us</a> for help</li>
           <li style="background:none; border:0;padding:5px;list-style:disc;"> Go to the <a href="/en/" style="color:#e93b45; text-decoration:none;">Home Page</a></li>
       </ul>    
	 </div><div class="clear"></div>
				</div>
				</div><div class="clear"></div>
				</div>



<div class="content_bottom">&nbsp;</div>
<!-- Bottom Gallery Start -->
 <?php 
   include 'includes/menus/ministry_logos_without_mosque.php'; 
   ?><div class="clear"></div>
<!-- Bottom Gallery Close -->
<?php include 'includes/footer.php'; ?>
</body>
</html>