<?php
error_reporting(0);
include './includes/database.php';
include './includes/functions.php';
include './includes/config.php';
include_once('./includes/generic_functions.php');
$conf = new Configuration();
$db = new MyDatabase();
$site_path = $conf->site_url;

$slug = explode('/',$_SERVER['REQUEST_URI']);
$slug = end($slug);
$msg="";
session_start();
$current_page = 'ftplogin';
if((isset($_SESSION['user_id'])) && $_SESSION['user_id']!=''){
				if(isset($_POST['cur_username']) && $_POST['cur_username']!='' && isset($_POST['new_username']) && $_POST['new_username']!='' && isset($_POST['submitchangeUsername']) ){
		
			foreach($_POST as $name => $value) {
				$$name=mysql_real_escape_string($value);
			}
			$user_id=$_SESSION['user_id'];
			$sql="select * from site_users where id='$user_id' and active=1";
			$result=mysql_query($sql);
			while($obj=mysql_fetch_object($result)){
				$email=$obj->email;
			}

			if($email == $_POST['cur_username'] ){			
				$sql="select * from site_users where email='$new_username'";
				$result=mysql_query($sql);
				$count=mysql_num_rows($result);			
				if($count==1){
					if($obj = mysql_fetch_object($result)){	
						$msg='user exists';
					}
				}else{
					$sql="update site_users set email='$new_username' where id='$user_id' and active=1 limit 1";
					mysql_query($sql);
					$msg='success';
				}
			}else{
				$msg="Invalid";
			}
			
			
		}
	
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<?php include 'includes/common_header.php'; ?>
    <title>Change Username</title>

	<link href="<?php echo $site_path; ?>css/calendar.css" rel="stylesheet" type="text/css" />

 

	<script type="text/javascript" src="<?php echo $site_path; ?>js/ajaxupload.3.5.js"></script>
	<script type="text/javascript" src="<?php echo $site_path; ?>js/jquery.validate.js"></script>
	<script type="text/javascript" src="<?php echo $site_path; ?>js/main.js"></script>
	<script type="text/javascript" src="<?php echo $site_path; ?>js/cal.js"></script>
    
<link href="<?php echo $site_path; ?>css/jquery-ui-1.8.24.custom.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo $site_path; ?>js/jquery-ui-1.8.24.custom.min.js"></script>

<script type="text/javascript">

 $(document).ready(function(){
    $("#changeUsernameForm").validate({
			rules: {
				cur_username: {
					required: true,
					email: true
				},
				new_username: {
					required: true,
					email: true
				}
			}
		});
  });
</script>
<style>
td{text-align:center;}
textarea{width:660px;}
</style>

</head>
<body>

<?php include 'includes/menus/banner_header.php'; ?>
    <!-- Banner start -->
    <div class="banner">
	<img src="<?php echo $site_path; ?>images/visiting_the_mosque_banner.jpg">     
    </div>   
    <!-- Banner Close -->
    
    <div class="main_box_content">
            <?php include 'includes/menus/nav_menu.php'; ?>
        <div class="clear"></div>
        <div class="content">
             <div class="brad_cram">
            	<ul>
            	   <li><a href="<?php echo $site_path; ?>">Home</a></li>
                    <li><a href="#" class="active">Change Username</a></li>
                </ul>
            </div>
			<div class="content-left">
                <?php // include 'includes/menus/left_menu.php'; 
				include 'includes/ads/ad_216_240.php';
				?>

                <?php 
				include 'includes/menus/ministry_logos.php';
                include 'includes/menus/left_menu.php';
				?>
            </div>
                <div class="content-right" style="margin-left:10px">
				<div class="single_middle">
                    <h2>Change Username</h2>
                    <div class="page_item_single">
                    	<div class="page_content" >
                            <div class="general_body_content">
                                 <div class="reg_form">
                                 
									<?php include '../forms/changeUserName.php'; ?>                                
                                    <div class="clear"></div>
                                </div>
							</div>
                            <br class="clear" />
                            
                        </div>
                    </div> 
                   
            </div>
				</div>
				</div>
				</div>
				
    
	<div class="content_bottom">&nbsp;</div>
	<?php include 'includes/footer.php'; ?> 
	<?php
	
	   if($msg == 'success'){
	  
		 ?>
		<script>
			$(function() {
				$( "#dialog" ).dialog();
			});
		</script>
        <script type="text/javascript">
		$(function(){
			$('#confirm').click(function() {
				//$('#dialog').hide();
				$( "#dialog" ).dialog( "close" );
				window.location = "/en/user_profile.php";
				return false;
			});
		});
		
        </script>
        <div id="dialog">
        	<p style="text-align:right;">لقد تم تغيير كلمة المرور بنجاح</p>
            <p style="padding-top:5px;"><span dir="ltr">Your username has been changed successfully</span></p>
            <p style="padding-top:5px;"><input type="button" value="OK" style="float:right;width:40px" id="confirm"></p>
        </div>
		 <?php
		 
		}
		else if($msg == 'failed')
		{
		?>
		  <script> alert("Cannot process your request"); </script>
		 <?php
		  print('<script type="text/javascript">window.location = "/en/user_profile.php";</script>');
exit(); 
		}
		 
		else if($msg == 'Invalid')
		{
		?>
		  <script> alert("The Old username you gave is incorrect"); </script>
		 <?php
		  print('<script type="text/javascript">window.location = "/en/user_profile.php";</script>');
exit(); 
		}else if($msg == 'user exists')
		{
		?>
		  <script>
			$(function() {
				$( "#dialog" ).dialog();
			});
		</script>
        <script type="text/javascript">
		$(function(){
			$('#confirm').click(function() {
				$( "#dialog" ).dialog( "close" );
				window.location = "/en/changeUserName.php";
				return false;
			});
		});
		
        </script>
        <div id="dialog">
        	<p style="text-align:right;"><span dir="rtl">إن البريد الإلكتروني الذي أدخلته مسجل مسبقا ، يرجى التسجيل ببريد الكتروني آخر.</span></p>
            <p>This email id is already registered. Please use a different email id</p>
            <p style=""><input type="button" value="OK" style="float:right;width:40px" id="confirm"></p>
        </div>
		 <?php
		  
		}
	  
	?>
</body>
</html>