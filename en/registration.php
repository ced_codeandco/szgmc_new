<?php
include './includes/functions.php';
include './includes/config.php';
include_once('./includes/generic_functions.php');
$conf = new Configuration();
$site_path = $conf->site_url;
session_start();
$slug = explode('/',$_SERVER['REQUEST_URI']);
$slug = end($slug);
if (!empty($_REQUEST['a_captcha'])) {


	if (empty($_SESSION['captcha']) || trim(strtolower($_REQUEST['a_captcha'])) != $_SESSION['captcha']) {
		$msg="wrong_captcha";
    } else {
        $enq_mail_from='info@szgmc.ae';
        $enq_mail_to='ab.almarzooqi@szgmc.ae';
        $enq_mail_cc= 'it@szgmc.ae';
        define("ENQ_FROM",$enq_mail_from);
        define("ENQ_TO",$enq_mail_to);
        define("ENQ_CC",$enq_mail_cc);
        $msg="";
        $current_page = 'ftplogin';
        if(isset($_POST['a_name']) && $_POST['a_name']!='' ){
            include '../includes/database.php';
            $db = new MyDatabase();
            foreach($_POST as $name => $value) {
                $$name=mysql_real_escape_string($value);
            }

            error_reporting(0);
            ini_set("display_errors", 1);
            $msg='success';
            $msg='success';
            $file_flag=0;

            $sql_str="INSERT INTO `quran_recitation_form` ( `name`, `age`, `gender`, `nationality`, `city_name`, `contact_no`, `email`, `education_level`, `memorization_level`, `already_participated`,`created_date_time`, `created_ip`) VALUES(
              '$a_name','$a_age','$a_gender','$a_nationality','$a_city','$a_telephone','$a_email','$desc','$memorization_level','$course_partticipation','".date('Y-m-d H:i:s')."', '".getClientIp()."'
            )";

            $conn = $db->getConnection();
            mysql_query("set names 'utf8'", $conn);
            //mysql_query("set collation_connection = @@collation_database", $conn);
            $in_id=0;


            mysql_query($sql_str, $conn) or die("Query error");//die();
            $in_id=mysql_insert_id();

            if (!empty($in_id)) {
                $ref_id = "QR" . "-" . str_pad($in_id, 4, '0', STR_PAD_LEFT);;
                $mailContent = getregistationContent($_POST, $ref_id);
                $sql = "update quran_recitation_form set details='" . addslashes($mailContent) . "' where id='$in_id' ";
                mysql_query($sql, $conn);

                if ($msg == 'success') {

                    $fromName = 'Quran Recitation';
                    $fromEmail = ENQ_FROM;
                    $toName = 'SZGMC';
                    $toEmail = ENQ_TO;
                    $subject = "Quran Recitation";

                    $toEmail = 'ab.almarzooqi@szgmc.ae';
                    $cc = 'it@szgmc.ae';
                    $bcc = ''; 
                    sendEmail($toName, $fromName, $toEmail, $fromEmail, $subject, $mailContent, '', $cc, $bcc);
                }
                header("Location:".$site_path.'quran_recitation_course?success=1');
                exit;
            }

        }
    }
}
include './includes/database.php';
$db = new MyDatabase();
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <?php include 'includes/common_header.php'; ?>
    <title>Quran Recitation Course Registration Form</title>
	<link href="<?php echo $site_path; ?>css/calendar.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="<?php echo $site_path; ?>js/jquery.validate.js"></script>
	<link href="<?php echo $site_path; ?>css/jquery-ui-1.8.24.custom.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript" src="<?php echo $site_path; ?>js/jquery-ui-1.8.24.custom.min.js"></script>

<script type="text/javascript">

  $(document).ready(function(){
    $("#mediaForm").validate({
			rules: {
				a_name: "required",
				a_age: "required",
				a_gender: "required",
				a_nationality: "required",
				a_captcha: "required",
				a_city: "required",
				a_email: {
					required: true,
					email: true
				},	
				a_telephone: "required",	
				
				desc: "required",
				course_partticipation: "required"	  								
			}
		});
  });
  </script>

<style>
#table{margin-left:0px}
#table table{width:100%}
</style>
</head>
<body>
    <?php include 'includes/menus/banner_header.php'; ?>
    <!-- Banner start -->
    <div class="banner">
	<img src="<?php echo $site_path; ?>images/visiting_the_mosque_banner.jpg">     
    </div>   
    <!-- Banner Close -->
    
    <div class="main_box_content">
            <?php include 'includes/menus/nav_menu.php'; ?>
        <div class="clear"></div>
        <div class="content test">
             <div class="brad_cram">
            	<ul>
            	   <li><a href="<?php echo $site_path; ?>">Home</a></li>
                    <li><a href="#" class="active">Quran Recitation Course Registration Form</a></li>
                </ul>
            </div>
            <div class="content-left">
                <?php // include 'includes/menus/left_menu.php'; 
				include 'includes/ads/ad_216_240.php';
				?>
                <br class="clear"/>
                <?php 
				include 'includes/menus/ministry_logos.php';
                include 'includes/menus/left_menu.php';
				?>
            </div>
                <div class="content-right" style="margin-left:30px">
                          <div class="single_middle">
			 
                    <h2>Quran Quran Memorization & Recitation Course</h2>

                              <div id="table">
                                  <p>Registration in Quran memorization and recitation course is open for males and females. They will be taught to memorize Quran and abide by the recitation rules. </p>
                                  <p>Student's level will  be determined by a competent administrative committee designated by the General Authority of Islamic Affairs and Endowments (Awqaf) to ensure  appropriate grouping for different levels. </p>
                                  <table>
                                      <tbody><tr>
                                          <th colspan="2">Session Date</th>
                                      </tr>
                                      <tr>
                                          <th>From</th>
                                          <th>To</th>
                                      </tr>
                                      <tr>
                                          <td>February 21, 2016</td>
                                          <td>May 19, 2016</td>
                                      </tr>
                                      </tbody></table>
                                  <table>
                                      <tbody><tr>
                                          <th>Category</th>
                                          <th>Days</th>
                                          <th>Time</th>
                                      </tr>
                                      <tr>
                                          <td>Male</td>
                                          <td>Sunday &amp; Tuesday</td>
                                          <td>06:00 PM to 07:30 PM</td>
                                      </tr>
                                      <tr>
                                          <td>Female</td>
                                          <td>Monday &amp; Wednesday</td>
                                          <td>04:30 PM to 06:00 PM</td>
                                      </tr>
                                      </tbody></table>
                              </div>


                    <div class="page_item_single">
                    	<div class="page_content" >
                            <div class="general_body_content">
                            	
                                <div class="reg_form">
                                    <p>Kindly fill out the form below to participate in the Qur’an Memorization and Intonation Session </p>
                                    <?php //include '../forms/registration.php'; ?>
									<?php include '../forms/registration_2.php'; ?>
                                    <div class="clear"></div>
                                </div>
							</div>
                            <br class="clear" />
                        </div>
                    </div> 
                   
            </div>
                </div>
            <br class="clear" />
            <div class="clear"></div>
    </div>
    </div>
	<div class="content_bottom">&nbsp;</div>
	<?php include 'includes/footer.php'; ?> 
	<?php
	
	   if(!empty($_GET['success']) && $_GET['success'] ==1){
	  
		 ?>
		  <script>
			$(function() {
				$( "#dialog" ).dialog();
			});
		</script>
        <script type="text/javascript">
		$(function(){
			$('#confirm').click(function() {
				//$('#dialog').hide();
					window.location = "/en/quran_recitation_course";
				$( "#dialog" ).dialog( "close" );
			
				return false;
			});
		});
		
        </script>
        <div id="dialog">
        	<p style="text-align:right;">لقد تم تقديم طلبك بنجاح ، وسوف نقوم بالتواصل معك قريبا </p>
            <p>Your request has been submitted successfully. We will contact you shortly</p>
            <p style=""><input type="button" value="OK" style="float:right;width:40px" id="confirm"></p>
        </div>
        
		 <?
		 
		}
		else if($msg == 'failed')
		{
		?>
		  <script> alert("Cannot process your request"); </script>
		 <?php
		 print('<script type="text/javascript">window.location = "./quran_recitation_course";</script>');
exit(); 
		}
	  else if($msg == 'wrong_captcha')
		{
		?>
		<script> alert("You entered wrong captcha"); </script>
		<?php
				 print('<script type="text/javascript">window.location = "./quran_recitation_course";</script>');
exit(); 
		}
	?>
	
	

</body>
</html>
