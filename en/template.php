<?php
include 'includes/database.php';
include 'includes/functions.php';
include 'includes/config.php';

$slug = explode('/',$_SERVER['REQUEST_URI']);
$slug = mysql_real_escape_string(end($slug));
$conf = new Configuration();
$db = new MyDatabase();

$site_path = $conf->site_url;

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>SZG</title>
    <?php include 'includes/common_header.php'; ?>
    <link rel="stylesheet" href="<?php echo $site_path; ?>css/nivo-slider.css" type="text/css" media="screen" />
    <link href="<?php echo $site_path; ?>css/style2.css" rel="stylesheet" type="text/css" />
    <!--[if IE]>
    <style>
    .header .top_menu .contactus{width:114px;float:left;margin-top:22px;}
    .header .top_menu .contactus a{ background:url(<?php echo $site_path; ?><?php echo $site_path; ?>images/contactus.png) no-repeat; color:#000000; padding:10px 20px 10px 15px; text-decoration:none; font-weight:bold;height:36px;}
    .header .top_menu .contactus a:hover{ background:url(<?php echo $site_path; ?>images/contactus_hover.png) no-repeat; color:#fff; padding:10px 20px 10px 15px; text-decoration:none; font-weight:bold;}
    .header .top_menu .contactus .current{ background:url(<?php echo $site_path; ?>images/contactus_hover.png) no-repeat; color:#fff; padding:10px 20px 10px 15px; text-decoration:none; font-weight:bold;}
    </style>
    <![endif]--> 
	<script type="text/javascript" src="<?php echo $site_path; ?>js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="<?php echo $site_path; ?>js/jquery.nivo.slider.js"></script>
    <script type="text/javascript" src="<?php echo $site_path; ?>js/site.js"></script>
    <script type="text/javascript">
		var main_menu = '<?php echo (isset($main_menu) && $main_menu != '')? $main_menu : ''; ?>';
		var current_tab = '<?php echo (isset($slug) && $slug != '')? $slug : ''; ?>';
	</script>
</head>
<body>
    <?php include 'includes/menus/banner_header.php'; ?>
    <!-- Banner start -->
    <div class="banner">
	<img src="<?php echo $site_path; ?>images/visiting_the_mosque_banner.jpg">     
    </div>   
    <!-- Banner Close -->
    
    <div class="main_box_content">
            <?php include 'includes/menus/nav_menu.php'; ?>
        <div class="clear"></div>
        <div class="content">
             <div class="brad_cram">
            	<ul>
            	   <li><a href="<?php echo $site_path; ?>">Home</a></li>
                   <li><a href="#" class="active">King Fahad bin Abdul Aziz</a></li>
                </ul>
            </div>
            <div class="content-left">
                <?php // include 'includes/menus/left_menu.php'; 
				include 'includes/ads/ad_216_240.php';
				?>
                <br class="clear"/>
                <?php 
				include 'includes/menus/ministry_logos.php';
                include 'includes/menus/left_menu.php';
				?>
            </div>
                <div class="content-right" style="margin-left:10px">
                     <div class="single_middle">
                <br class="clear" />
                	<div style="float:left">
                	<span style="font-size:18px">King Fahad bin Abdul Aziz</span>
                    <p>&ldquo;We understand the significant role played by HH Sheikh Zayed bin Sultan Al Nahyan in serving the interests of the Arab nation and emphasizing the role of the UAE in the world community. The UAE plays a pivotal role providing motivation and support to our brothers&rdquo;</p>
                    </div>
                
                
                    <!--<?php
                    //$news_url= $site_path."news-detail/".string_to_filename($page->title).'-'.$page->p_id;
                    ?>
                    <div class="page_item_single">
                    	<div class="page_content" >
                            <div class="general_body_content"><?php //echo $page->content; ?></div>
                            <br class="clear" />
                            
                        </div>
                    </div> 
                    <div class="clear bottom_line"> <img src="<?php //echo $site_path; ?>images/line.jpg" > </div>-->
            </div>
                </div>
        </div>
    </div>
    <div class="content_bottom">&nbsp;</div>
	<?php include 'includes/footer.php'; ?> 
</body>
</html>