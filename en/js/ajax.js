function refreshContent() {
    $("#content").fadeOut("slow", function(){
	    var mon = document.month.month.value;
        $("#content").load("calendar/"+mon+".php",false, function() {
            $("#content").fadeIn("slow");
        });
    })
    return false;
}