<style>

.hotspot { cursor:pointer; width:300px; padding:10px; text-align:left;}
#tt {position:absolute; display:block; padding:1px;}

#tttop {display:block;overflow:hidden; }
#ttbot {display:block;overflow:hidden;}
#ttcont {display:block; padding:2px 12px 3px 7px; background:#ffffff;}

#tt ul{list-style-type:none; margin:0px; padding:5px; list-style:none;}
#tt ul li{ padding-bottom:8px; }
#tt ul li span{ float:left; text-align:left;font-weight:bold; white-space:nowrap; padding-right:5px; width:80px;}
#tt ul li span.tip_header{ float:left; font-weight:bold; padding:5px 0px 15px 0px; width:100%; }

</style>

<script src="js/ajax.js" type="text/javascript"></script>
<script>
var tooltip=function(){
	var id = 'tt';
	var top = 3;
	var left = 3;
	var maxw = 200;
	var speed = 10;
	var timer = 20;
	var endalpha = 95;
	var alpha = 0;
	var tt,t,c,b,h;
	var ie = document.all ? true : false;
	return{
		show:function(v,clr,w){
			if(tt == null){
				tt = document.createElement('div');
				tt.setAttribute('id',id);
				t = document.createElement('div');
				t.setAttribute('id',id + 'top');
				c = document.createElement('div');
				c.setAttribute('id',id + 'cont');
				b = document.createElement('div');
				b.setAttribute('id',id + 'bot');
				tt.appendChild(t);
				tt.appendChild(c);
				tt.appendChild(b);
				document.body.appendChild(tt);
				tt.style.opacity = 0;
				tt.style.filter = 'alpha(opacity=0)';
				document.onmousemove = this.pos;
			}
			tt.style.display = 'block';
			tt.style.color = clr;
			tt.style.background=clr;
			
			c.innerHTML = v;
			tt.style.width = w ? w + 'px' : 'auto';
			if(!w && ie){
				t.style.display = 'none';
				b.style.display = 'none';
				tt.style.width = tt.offsetWidth;
				t.style.display = 'block';
				b.style.display = 'block';
			}
			if(tt.offsetWidth > maxw){tt.style.width = maxw + 'px'}
			h = parseInt(tt.offsetHeight) + top;
			clearInterval(tt.timer);
			tt.timer = setInterval(function(){tooltip.fade(1)},timer);
		},
		pos:function(e){
			var u = ie ? event.clientY + document.documentElement.scrollTop : e.pageY;
			var l = ie ? event.clientX + document.documentElement.scrollLeft : e.pageX;
			tt.style.top = (u - h) + 'px';
			tt.style.left = (l + left) + 'px';
		},
		fade:function(d){
			var a = alpha;
			if((a != endalpha && d == 1) || (a != 0 && d == -1)){
				var i = speed;
				if(endalpha - a < speed && d == 1){
					i = endalpha - a;
				}else if(alpha < speed && d == -1){
					i = a;
				}
				alpha = a + (i * d);
				tt.style.opacity = alpha * .01;
				tt.style.filter = 'alpha(opacity=' + alpha + ')';
			}else{
				clearInterval(tt.timer);
				if(d == -1){tt.style.display = 'none'}
			}
		},
		hide:function(){
			clearInterval(tt.timer);
			tt.timer = setInterval(function(){tooltip.fade(-1)},timer);
		}
	};
}();

function navigate() {
var menuIndex = document.formMenu.selectMenu.selectedIndex;
location = document.formMenu.selectMenu.options[menuIndex].value;
}
</script>
<link href="calendar/calendar.css" rel="stylesheet" type="text/css" />
<?php

if (isset($_POST["month"]))
{
$month=$_POST["month"];
}
else
{
$month=strtolower(date("M"));
}
?>
<div style="float:left; margin-left:2px; margin-bottom:10px; border:1px solid #ccc; padding:20px;">
<form id="sort" name="month" method="post" action="calendar.php"> 
      
      <select name="month" onchange="refreshContent();" style="float:left; margin-left:10px;">
	           <option value="jan" <?php if ($month=="jan") { ?> selected="selected" <?php } ?>>January - Muharram 1432 - Shafar 1432</option>
              <option value="feb" <?php if ($month=="feb") { ?> selected="selected" <?php } ?>>February - Shafar 1432 - Rabi' I 1432</option>

              <option value="mar" <?php if ($month=="mar") { ?> selected="selected" <?php } ?> >March - Rabi' I 1432 - Rabi' II 1432</option>
              <option value="apr" <?php if ($month=="apr") { ?> selected="selected" <?php } ?> > April - Rabi' II 1432 - Jumada I 1432 </option>
              <option value="may" <?php if ($month=="may") { ?> selected="selected" <?php } ?> > May - Jumada I 1432 - Jumada II 1432 </option>
              <option value="jun" <?php if ($month=="jun") { ?> selected="selected" <?php } ?> > June - Jumada II 1432 - Rajab 1432 </option>
              <option value="jul" <?php if ($month=="jul") { ?> selected="selected" <?php } ?> > July - Rajab 1432 - Sha'ban 1432 </option>
              <option value="aug" <?php if ($month=="aug") { ?> selected="selected" <?php } ?> > August - Ramadan 1432 - Shawwal 1432 </option>
              <option value="sep" <?php if ($month=="sep") { ?> selected="selected" <?php } ?> > September - Shawwal 1432 - Dh-Qa'da 1432 </option>
              <option value="oct" <?php if ($month=="oct") { ?> selected="selected" <?php } ?> > October - Dh-Qa'da 1432 - Dh-Hijja 1432 </option>
              <option value="nov" <?php if ($month=="nov") { ?> selected="selected" <?php } ?> > November - Dh-Hijja 1432 - Muharram 1433 </option>
              <option value="dec" <?php if ($month=="dec") { ?> selected="selected" <?php } ?> > December - Muharram 1433 - Shafar 1433 </option>
        </select>
       
        </form> 
        <div id="content">
              
<?

	include_once('calendar/'.$month.'.php');
?>   
</div>  </div>      
