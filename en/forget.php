<?php
error_reporting(0);
include './includes/database.php';
include './includes/functions.php';
include './includes/config.php';
include_once('./includes/generic_functions.php');
$conf = new Configuration();
$db = new MyDatabase();
$site_path = $conf->site_url;

$slug = explode('/',$_SERVER['REQUEST_URI']);
$slug = end($slug);
$msg="";
session_start();
		
$current_page = 'ftplogin';
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include 'includes/common_header.php'; ?>
    <title>Forgot Password</title>

	<link href="<?php echo $site_path; ?>css/calendar.css" rel="stylesheet" type="text/css" />

 

	<script type="text/javascript" src="<?php echo $site_path; ?>js/ajaxupload.3.5.js"></script>
	<script type="text/javascript" src="<?php echo $site_path; ?>js/jquery.validate.js"></script>
	<script type="text/javascript" src="<?php echo $site_path; ?>js/main.js"></script>
	<script type="text/javascript" src="<?php echo $site_path; ?>js/cal.js"></script>
    
<link href="<?php echo $site_path; ?>css/jquery-ui-1.8.24.custom.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo $site_path; ?>js/jquery-ui-1.8.24.custom.min.js"></script>

<script type="text/javascript">

  $(document).ready(function(){
    $("#forgotPswdForm").validate({
			rules: {
				a_email: {
					required: true,
					email: true
				}

	  								
			}
		});
  });
  
</script>
<style>
td{text-align:center;}
textarea{width:660px;}
</style>

</head>
<body>
<?php include 'includes/menus/banner_header.php'; ?>
    <!-- Banner start -->
    <div class="banner">
	<img src="<?php echo $site_path; ?>images/visiting_the_mosque_banner.jpg">     
    </div>   
    <!-- Banner Close -->
    
    <div class="main_box_content">
            <?php include 'includes/menus/nav_menu.php'; ?>
        <div class="clear"></div>
        <div class="content">
             <div class="brad_cram">
            	<ul>
            	   <li><a href="<?php echo $site_path; ?>">Home</a></li>
                    <li><a href="#" class="active">Forgot Password</a></li>
                </ul>
            </div>
			<div class="content-left">
                <?php // include 'includes/menus/left_menu.php'; 
				include 'includes/ads/ad_216_240.php';
				?>
                <br class="clear"/>
                <?php 
				include 'includes/menus/ministry_logos.php';
                include 'includes/menus/left_menu.php';
				?>
            </div>
                <div class="content-right" style="margin-left:10px">
					<div class="single_middle">
                    <h2>Register</h2>
                    <div class="page_item_single">
                    	<div class="page_content" >
                            <div class="general_body_content">
                                 <div class="reg_form">
                                   <div class="text">
	       
     <p>Your password has been mailed to your registered email address.</p><p>Please check your mail.
      </p>
      <p style="text-align:center"><a href="/en/userlogin.php">Login</a>
      </p>
 
    </div>
									                                
                                    <div class="clear"></div>
                                </div>
							</div>
                            <br class="clear" />
                            
                        </div>
                    </div> 
                   
            </div>
				</div>
				</div>
				</div>
				
    
	<div class="content_bottom">&nbsp;</div>
	<?php include 'includes/footer.php'; ?> 
	
</body>
</html>