<?php
ob_start();
include 'includes/database.php';
include 'includes/dal/pages.php';
include 'includes/functions.php';
include 'includes/config.php';

$slug = explode('/',$_SERVER['REQUEST_URI']);
$slug = end($slug);
//echo $slug;
//print_r($slug);
//die();
$conf = new Configuration();

$db = new MyDatabase();
$dal_pages = new ManagePages();
$page = $dal_pages->getPage($slug);
$site_path = $conf->site_url;

$main_menu = $conf->getCurrentMainPage($slug);
if($page)
	$stat_page= "true";
else
 	$stat_page=  "false";
if(($page->p_id=="") || ($page->p_id=="67"))
					{
						header("location:/en/404.php");
					}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">        
<head>
	<?php include 'includes/common_header.php'; ?>
    <title><?php echo $page->title; ?> - Sheikh Zayed Grand Mosque Center</title>
    
    <script type="text/javascript">
$( document ).ready(function() {
$('dd').hide();
$('dt').click(function() {
var toggle = $(this).nextUntil('dt');
toggle.slideToggle();
$('dd').not(toggle).slideUp();
});
});
</script>
    <script src="<?php echo $site_path;?>js/colorbox/jquery.colorbox.js"></script>
    <link rel="stylesheet" href="<?php echo $site_path;?>css/colorbox/colorbox.css" type="text/css"/>
</head>
<body>

<?php include 'includes/menus/banner_header.php'; ?>
    <!-- Banner start -->
    <div class="banner">
	<img src="<?php echo $site_path; ?>images/visiting_the_mosque_banner.jpg">     
    </div>   
    <!-- Banner Close -->
    
    <div class="main_box_content">
            <?php include 'includes/menus/nav_menu.php'; ?>
        
       

        <div class="content">
             <div class="brad_cram">
              <?php 
				
				
				if($main_menu=="szgmc")
				{
					$main_menu1 = "About SZGMC";
					$main_menu2= "about-szgmc";
				}
				else if($main_menu=="founder")
				{
					$main_menu1 = "Founding Father";
					$main_menu2 = $main_menu;
				}
				else if($main_menu=="about")
				{
					$main_menu1 = "About the Mosque";
					$main_menu2= "vision-dream-sheikh-zayed-mosque";
				}
				else if($main_menu=="archi")
				{
					$main_menu1 = "Architecture";
					$main_menu2= "general-architecture";
				}
				else if($main_menu=="planvisit")
				{
					$main_menu1 = "Visiting The Mosque";
					$main_menu2= "visiting-the-mosque";
				}
				else if($main_menu=="events")
				{
					$main_menu1 = "Events & Activities";
					$main_menu2= "events-and-activities";
				}
				else if($main_menu=="mediacenter")
				{
					$main_menu1 = "Media Corner";
					$main_menu2= "javascript:void(0);";
				}
				else if($main_menu=="library")
				{
					$main_menu1 = "Library";
					$main_menu2= "javascript:void(0);";
				}
				else
				{
					$main_menu1 = $main_menu;
					$main_menu2 = $main_menu;
				}

				?>
            	<ul>
            	   <li><a href="<?php echo $site_path; ?>">Home</a></li>
                    <li><?php if($main_menu1 == '') { ?>
                    <a href="#" class="active"><?php echo $page->title; ?></a>
                    <?php } else { 
					if($slug!="founder")
					{
					?>
                   <a href="<?php echo $main_menu2; ?>"><?php echo ucfirst($main_menu1);  ?></a>
                    <?php
					} 
					?>
                     
                    <a href="#" class="active"><?php echo $page->title; ?></a>
                    <?php } ?></li>
                </ul>
            </div>
            <div class="content">
        	<div class="content-left">
                <?php  
				include 'includes/ads/ad_216_240.php';
				include 'includes/menus/ministry_logos.php';
				include 'includes/menus/left_menu.php';
				if(($page->p_id=="255") || ($page->p_id=="256"))
					{
						include 'includes/news_letter.php'; 
					}
				?>
               
            </div>
                <div class="content-right" style="margin-left:10px">
				<div class="single_middle">
					
			 <?php include 'includes/menus/marquee.php';
			 
			 
							 ?>
                	<div class="page_item_single dynamic-page-wrap">
                    	<div class="page_content" >
                            <div class="general_body_content <?php if($page->p_id==69){ echo 'contact_us_page'; }?>"><?php 
							if($page->php_content=='Y' && $page->p_id!='')
							echo eval("?>".$page->content."<?"); 
							else
							echo $page->content; ?></div>
                            <!--<br class="clear" />-->
                            
                        </div>
                    </div> 
                    <div class="clear"></div>
            </div>
	</div>
	</div>
	</div>
	<div class="clear"></div>
	</div>
	</div>
    
	<div class="content_bottom height_20px"></div>
	<?php include 'includes/footer.php'; ?>

<script type="text/javascript">
    $(document).ready(function(){
        if (current_tab == 'getting-to-the-mosque' || current_tab == 'contact-us' ) {
            $(".ajax").colorbox({maxWidth:'710px', width:'90%'});
        }
    })
</script>
</body>
</html>