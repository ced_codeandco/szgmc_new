<?php
include 'includes/database.php';
include 'includes/functions.php';
include 'includes/config.php';

$slug = explode('/',$_SERVER['REQUEST_URI']);
$slug = end($slug);
$conf = new Configuration();
$db = new MyDatabase();

$site_path = $conf->site_url;
$main_menu = $conf->getCurrentMainPage($slug);
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Organizational structure - SZGMC</title>
	<?php include 'includes/common_header.php'; ?>
</head>
<body>
    <?php include 'includes/menus/banner_header.php'; ?>
    <!-- Banner start -->
    <div class="banner">
	<img src="<?php echo $site_path; ?>images/visiting_the_mosque_banner.jpg">     
    </div>   
    <!-- Banner Close -->
    
    <div class="main_box_content">
            <?php include 'includes/menus/nav_menu.php'; ?>
        <div class="clear"></div>
        <div class="content">
             <div class="brad_cram">
            	<ul>
            	   <li><a href="<?php echo $site_path; ?>">Home</a></li>
                   <li><a href="<?php echo $site_path; ?>about-szgmc">About SZGMC</a></li>
                    <li><a href="#" class="active">Organizational structure</a></li>
                </ul>
            </div>
            <div class="content-left">
                <?php // include 'includes/menus/left_menu.php'; 
				include 'includes/ads/ad_216_240.php';
				?>
                <?php 
				include 'includes/menus/ministry_logos.php';
                include 'includes/menus/left_menu.php';
				?>
            </div>
                <div class="content-right" style="margin-left:20px">
                       <div class="single_middle">            	 
                <h2>Organizational structure</h2>
                <div class="orgnization" style="float:right;">
                <img src="../images/Hierarchy-en-2016.png" />
                </div>
                <!--div class="orgnization">
                <img src="images/Hierarchy-en-02.png" />
                </div -->
          </div>
                </div>
                </div>

       <div class="clear"></div> </div>
	<div class="content_bottom">&nbsp;</div>
	<?php include 'includes/footer.php'; ?> 
</body>
</html>