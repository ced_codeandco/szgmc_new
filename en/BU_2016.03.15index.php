<?php
define("ERROR_LOG",true);
include 'includes/database.php';
include 'includes/dal/news.php';
include 'includes/dal/pages.php';
include 'includes/functions.php';
include 'includes/config.php';

$current_page = 'index';

$db = new MyDatabase();
$conf = new Configuration();
$site_path = $conf->site_url;


$conf->site_description = 'The board members of the Sheikh Zayed Grand Mosque in Abu Dhabi. Find all about the Sheikh Zayed Grand Mosque in Abu Dhabi including, visiting timings, how to get to the mosque, dress code, tours, history, architecture and more.';

$conf->site_keywords = 'grand mosque, sheikh zyed grand mosque, mosque in adu dhabi, what to wear in mosque, how to get to mosque, history of grand mosque, architecture of grand mosque, grand mosque photos, board members';

$dal_news = new ManageNews();
$dal_pages = new ManagePages(); //for message from minister of presedential affairs
$page = $dal_pages->getPage('', 70); // id must be 119 for message from minister, if different then use that
$stat_page= "home";
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Sheikh Zayed Grand Mosque Centre Abu Dhabi</title>
    <?php include 'includes/common_header.php'; ?>
<!--   <script src="/js/jquery.marquee.js"></script>-->
<script type="text/javascript">
		$(document).ready(function(){
                        
                        $(".prayer_seeless").css({'display':'none'})
			$("#flip").click(function(){
                            $("#prayertiming").slideDown("slow");
                            $(".prayer_seemore").css({'display':'none'});
                            $(".prayer_seeless").css({'display':'block'});
			});
                        
                        $("#flip1").click(function(){
                            $("#prayertiming").slideUp("slow");
                           $(".prayer_seemore").css({'display':'block'});
                            $(".prayer_seeless").css({'display':'none'});
			});
			
			$('#js-news').ticker({
				speed: 0.3,          
				ajaxFeed: false,      
				feedUrl: false,       
				feedType: 'xml',    
				htmlFeed: true,        
				debugMode: true,      
				controls: true,        
				titleText: false,   
				displayType: 'reveal', 
				direction: 'ltr',       
				pauseOnItems: 2000,    
				fadeInSpeed: 600,     
				fadeOutSpeed: 300
			});
			
			
			
		});
	</script>
</head>
<body class="index"  onLoad="$('.page_spinner').animate({opacity:0}, 900).hide(0);">
<!--<body>-->
<div class="page_spinner" style="
                display:block;
                position: fixed;
                background: url(<?php echo $site_path; ?>images/footer-mosque-image.png) 50% 50% no-repeat #fff; 
				z-index: 9999999999999999999999999999999!important; 
                width: 100%;
                height: 100%;
                top: 0;
                left: 0; background-color: #fff;">
</div>
<!-- Header Start -->
<?php include 'includes/menus/banner_header.php'; ?>
<!-- Header Start Close -->
<div class="clear"></div>
<!-- Banner Start -->
<div class="banner" >
	<div id="rev_slider_1_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" style="margin:0px auto;padding:0px;margin-top:0px;margin-bottom:0px;max-height:359px;height:359px;">
					<div id="rev_slider_1_1" class="rev_slider fullwidthabanner" style="display:none;max-height:359px;height:359px;">						
                        <ul>
                            <li data-masterspeed="2000"><img src="<?php echo $site_path;?>images/slider_01.jpg"></li>
                            <!--<li><img src="<?php /*echo $site_path;*/?>images/slider_02.jpg"></li>-->
                            <li data-masterspeed="2000"><img src="<?php echo $site_path;?>images/slider_03.jpg"></li>
                            <li data-masterspeed="2000"><img src="<?php echo $site_path;?>images/slider_04.jpg"></li>
                        </ul>
					</div>
				</div>          
</div>

<!-- Banner Close -->
<!-- Content Start -->
	<div class="main_box_content" style="height:1445px;">
            <?php include 'includes/menus/nav_menu.php'; ?>
        <div class="clear"></div>
        <div class="content">
        <div class="content-left">
        	 <?php include 'includes/menus/left_menu.php'; ?>
      </div>
      <div class="content-right">
      			<div class="right_banner">
                	<div class="flexslider">
                        <ul class="slides">
                            <li>
                                <div class="caption_right_banner">
                                    <h2><!--Architectures:--> <strong>General architecture</strong>
                                    </h2>
                                    <span class="more_btn"><a href="<?php echo $site_path;?>general-architecture">Read More</a></span>
                                </div>
                                <img src="<?php echo $site_path;?>images/home_slider/general-architecture.jpg"/>
                            </li>

                            <li>
                                <div class="caption_right_banner">
                                    <h2><!--Architectures:--> <strong>Domes</strong>
                                    </h2>
                                    <span class="more_btn"><a href="<?php echo $site_path;?>domes">Read More</a></span>
                                </div>
                                <img src="<?php echo $site_path;?>images/home_slider/domes.jpg"/>
                            </li>


                            <li>
                                <div class="caption_right_banner">
                                    <h2><!--Architectures:--> <strong>Marble</strong>
                                    </h2>
                                    <span class="more_btn"><a href="<?php echo $site_path;?>marbles">Read More</a></span>
                                </div>
                                <img src="<?php echo $site_path;?>images/home_slider/marbles.jpg"/>
                            </li>

                            <li>
                                <div class="caption_right_banner">
                                    <h2><!--Architectures:--> <strong>Lunar illumination</strong>
                                    </h2>
                                    <span class="more_btn"><a href="<?php echo $site_path;?>lunar-illumination">Read More</a></span>
                                </div>
                                <img src="<?php echo $site_path;?>images/home_slider/lunar-illumination.jpg"/>
                            </li>

                            <li>
                                <div class="caption_right_banner">
                                    <h2>Architecture: <strong>Carpets</strong> </h2>
                                    <span class="more_btn"><a href="<?php echo $site_path;?>carpets">Read More</a></span>
                                </div>
                                <img src="<?php echo $site_path;?>images/home_slider/carpets.jpg"/>
                            </li>

                            <li>
                                <div class="caption_right_banner">
                                    <h2><!--Architectures:--> <strong>Chandeliers</strong>
                                    </h2>
                                    <span class="more_btn"><a href="<?php echo $site_path;?>chandeliers">Read More</a></span>
                                </div>
                                <img src="<?php echo $site_path;?>images/home_slider/chandeliers.jpg"/>
                            </li>

                            <li>
                                <div class="caption_right_banner">
                                    <h2><!--Architectures:--> <strong>Pulpit (Menbar)</strong>
                                    </h2>
                                    <span class="more_btn"><a href="<?php echo $site_path;?>pulpit">Read More</a></span>
                                </div>
                                <img src="<?php echo $site_path;?>images/home_slider/pulpit.jpg"/>
                            </li>

                            <li>
                                <div class="caption_right_banner">
                                    <h2><!--Architectures:--> <strong>Minaret</strong>
                                    </h2>
                                    <span class="more_btn"><a href="<?php echo $site_path;?>minaret">Read More</a></span>
                                </div>
                                <img src="<?php echo $site_path;?>images/home_slider/minaret.jpg"/>
                            </li>

                            <li>
                                <div class="caption_right_banner">
                                    <h2><!--Architectures:--> <strong>Reflective Pools</strong>
                                    </h2>
                                    <span class="more_btn"><a href="<?php echo $site_path;?>reflective-pools">Read More</a></span>
                                </div>
                                <img src="<?php echo $site_path;?>images/home_slider/reflective-pools.jpg"/>
                            </li>

                            <li>
                                <div class="caption_right_banner">
                                    <h2><!--Architectures:--> <strong>Columns</strong>
                                    </h2>
                                    <span class="more_btn"><a href="<?php echo $site_path;?>columns">Read More</a></span>
                                </div>
                                <img src="<?php echo $site_path;?>images/home_slider/columns.jpg"/>
                            </li>


                            <li>
                                <div class="caption_right_banner">
                                    <h2><!--Architectures:--> <strong>Mihrab</strong>
                                    </h2>
                                    <span class="more_btn"><a href="<?php echo $site_path;?>mihrab">Read More</a></span>
                                </div>
                                <img src="<?php echo $site_path;?>images/home_slider/mihrab.jpg"/>
                            </li>

                            <li>
                                <div class="caption_right_banner">
                                    <h2><!--Architectures:--> <strong>Sahan</strong>
                                    </h2>
                                    <span class="more_btn"><a href="<?php echo $site_path;?>sahan">Read More</a></span>
                                </div>
                                <img src="<?php echo $site_path;?>images/home_slider/sahan.jpg"/>
                            </li>
                        </ul>
                        <!--<ul class="slides">
                            <li>
                            <div class="caption_right_banner">
                                <h2>Architectures: <strong>Biggest Carpet in the World</strong></h2>
                                <span class="more_btn"><a href="<?php /*echo $site_path; */?>carpets">Read More</a></span>
                            </div>
                            <img src="<?php /*echo $site_path; */?>images/right_banner_01.jpg"/>
                            </li>
                            
                            <li>
                            <div class="caption_right_banner">
                                <h2>Architectures: <strong>Lunar illumination</strong></h2>
                                <span class="more_btn"><a href="<?php /*echo $site_path; */?>lunar-illumination">Read More</a></span>
                            </div>
                            <img src="<?php /*echo $site_path; */?>images/right_banner_02.jpg"/>
                            </li>
                            
                            <li>
                            <div class="caption_right_banner">
                                <h2>Architectures: <strong>Domes</strong></h2>
                                <span class="more_btn"><a href="<?php /*echo $site_path; */?>domes">Read More</a></span>
                            </div>
                            <img src="<?php /*echo $site_path; */?>images/right_banner_03.jpg"/>
                            </li>
                         </ul>-->
	 				</div>
            </div>
       
       		<div class="menu_for_mobile mobile_view">
            	<ul>
                	<li><a href="<?php echo $site_path; ?>visit-mosque-visitors"><img src="<?php echo $site_path; ?>img/mobile/menu/icon.png"><label>Visiting Services</label></a></li>
                    <li><a href="<?php echo $site_path; ?>visiting-the-mosque"><img src="<?php echo $site_path; ?>img/mobile/menu/icon1.png"><label>Plan your Visit</label></a></li>
                    <li><a href="<?php echo $site_path; ?>about-szgmc"><img src="<?php echo $site_path; ?>img/mobile/menu/icon2.png"><label>About the Mosque</label></a></li>
                    <li><a href="<?php echo $site_path; ?>prayer-timings"><img src="<?php echo $site_path; ?>img/mobile/menu/icon3.png"><label>Prayer Timing</label></a></li>
                    <li><a href="<?php echo $site_path; ?>e_services.php"><img src="<?php echo $site_path; ?>img/mobile/menu/icon4.png"><label>eServices</label></a></li>
                    <li><a href="<?php echo $site_path; ?>events-and-activities"><img src="<?php echo $site_path; ?>img/mobile/menu/icon5.png"><label>Events & Activities</label></a></li>
                </ul>
            </div>
       
            <div class="widge_box" >
            	<ul>
                	<li class="fist_tham"><img src="<?php echo $site_path; ?>images/book_your_tour.jpg">
                    <a href="<?php echo $site_path; ?>juniorculturalguide.php"><!--Book <br />  Your Tour--><label>Junior</label> Cultural Guide</a></li>
                    
                    <li class=""><img src="<?php echo $site_path; ?>images/cg-part-time-job-english.jpg" width="166px" height="86px">
                    <a href="<?php echo $site_path; ?>cg-part-time-job"><label>Part Time</label> Culture Guide</a></li>
                    
                    <li><img src="<?php echo $site_path; ?>images/mosque.jpg">
                    <a href="<?php echo $site_path; ?>mosque-manner"><label>Mosque</label> Manners</a></li>
                    
                    <li><img src="<?php echo $site_path; ?>images/vertual.jpg">
                    <a href="https://www.google.com/maps/streetview/#sheikh-zayed-grand-mosque"><label>Virtual</label> Tour</a></li>
                 </ul>
                 <!--<div class="widge_news">
			<ul id="js-news" class="js-hidden">
			<?php
					$news_list = $dal_news->getNews();
					//echo count($news_list);exit;
					foreach($news_list as $news) {
                        $news_short=strip_tags($news->news_text);
                        $news_short=substr($news_short,0,220);
                        $news_short=substr($news_short,0,strripos($news_short,' '));
                        $news_short=UTF8ToHTML($news_short);
                        $news_url= $site_path."news-detail/".string_to_filename($news->news_title).'-'.$news->news_id;
                        $cls='';
                        $news_title=substr($news->news_title,0,120);
                        ?>
                        <li class="news-item"><a href="<?php echo $news_url;?>"><?php echo $news_title;?></a></li><?php
                    }?>
			</ul>
			
                 	
                 </div>-->
                 
                  <div class="widge_archive">
                  	<span class="widge_archive_title">News
                    <a href="<?php echo $site_path; ?>news-list">News Archive</a></span>
                  </div>
				  
				  <div class="video_gallery">
                        <ul id="vertical-ticker">
                  <?php
					$counter = 0;
					$news_list = $dal_news->getNews();
					//echo count($news_list);exit;
					foreach($news_list as $news) {
					$news_short=strip_tags($news->news_text);
					$news_short=substr($news_short,0,260);
					$news_short=substr($news_short,0,strripos($news_short,' '));
					$news_short=UTF8ToHTML($news_short);
					$news_url= $site_path."news-detail/".string_to_filename($news->news_title).'-'.$news->news_id;
					$cls='';
					$news_title=substr($news->news_title,0,60);
					
					if($counter % 2 == 0){
						echo '<li>';
					}
					if($counter % 2 == 1 ){
						$cls='Right_Side';
					}
						
						echo '<div class="left_vidoe '.$cls.'">
                                	<img width="270" height="162" src="'.$site_path.'news_images/'.$news->image.'">
                                    <p>'.$news_title.'
									<a href="'.$news_url.'" class="left_more_btn">Read more >></a></p>
                                </div>';
					if($counter % 2 == 1 ){
						echo '</li>';
					}
					$counter++;
					
				  ?>
				
        <?php }
		
		if($counter % 2 != 1){
				echo '</li>';
		}
		 ?>
    
				  
                          
                        </ul>
                        <p><a href="<?php echo $site_path; ?>" id="ticker-previous">Previous</a>  <a href="<?php echo $site_path; ?>" id="ticker-next">Next</a></p>
                  </div>
                  
                  <span class="vedio_link"><a href="gallery.php"><img src="<?php echo $site_path; ?>images/photo_albem.jpg"></a></span>
                  <span class="vedio_link vedio_link2"><a href="videos_album.php"><img  src="<?php echo $site_path; ?>images/vedio_gallery.jpg"></a></span>
                 
            </div>
      </div>
      
     
      
        </div>
    
    </div>
    
 <div class="clear"></div>  
<!-- Content Close -->
<!-- Bottom Gallery Start -->
 <?php 
   include 'includes/menus/ministry_logos_without_mosque.php'; 
   ?>
<!-- Bottom Gallery Close -->
<!-- footer Start -->
<?php include 'includes/footer.php'; ?>
<!-- Footer End -->


</body>
</html>
