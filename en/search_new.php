<?php
include 'includes/database.php';
include 'includes/dal/search.php';
include 'includes/functions.php';
include 'includes/config.php';

$conf = new Configuration();
$db = new MyDatabase();
$site_path = $conf->site_url;

$search_title = 'Search';
$results = null;
if(isset($_GET['key'])) {
	$search_title = mysql_real_escape_string($_GET['key']);
	$search_title = trim($search_title);
	$search = new Search();
	$results = $search->searchAll($search_title);
}


/*print_r($results);
die();*/

?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title><?php echo $search_title; ?> - <?php echo $conf->site_title; ?></title>
    <?php include 'includes/common_header.php'; ?>
    <link href="<?php echo $site_path; ?>css/news.css" rel="stylesheet" type="text/css" />
</head>
<body>
	<?php include 'includes/menus/banner_header.php'; ?>
    <!-- Banner start -->
    <div class="banner">
	<img src="<?php echo $site_path; ?>images/visiting_the_mosque_banner.jpg">     
    </div>   
    <!-- Banner Close -->
    
    <div class="main_box_content">
            <?php include 'includes/menus/nav_menu.php'; ?>
        <div class="clear"></div>
        <div class="content">
             <div class="brad_cram">
            	<ul>
            	   <li><a href="<?php echo $site_path; ?>">Home</a></li>
                    <li><a href="#" class="active">Search<?php echo $search_title!='' ? ' - '.$search_title : ''; ?></a></li>
                </ul>
            </div>
			<div class="content-left">
                <?php // include 'includes/menus/left_menu.php'; 
				include 'includes/ads/ad_216_240.php';
				?>
                <br class="clear"/>
                <?php 
				include 'includes/menus/ministry_logos.php';
                include 'includes/menus/left_menu.php';
				?>
            </div>
                <div class="content-right" style="margin-left:10px">
				<div class="single_middle">
                <br class="clear" />
                <h2>Search<?php echo $search_title!='' ? ' - '.$search_title : ''; ?></h2>
                <br class="clear" />
                <h4 class="search">Your search for term "<?php echo $search_title!='' ? $search_title : ''; ?>" found <?php echo count($results); ?> records</h4>
                <br class="clear" />
                <?php
					$first = false;
					$bk_class = '';
					foreach($results as $content) {
						$content = (object)$content;
						$con_short=strip_tags($content->content);
						$con_short=substr($con_short,0,100);
						$con_short=substr($con_short,0,strripos($con_short,' ')).'...';
						$con_short=UTF8ToHTML($con_short);
						$bk_class = $first ? 'first' : 'second';
						$first = !$first;
						if($content->type == 'news') {
							$url= $site_path."news-detail/".string_to_filename($content->title).'-'.$content->id;
						} else {
							$url= $site_path.ltrim($content->url, '/'); 
						}
				  	?>
                        <div class="news_story_search <?php echo $bk_class; ?>">
                        	<div class="bullet">&nbsp;</div>
                            <h3><a href="<?php echo $url; ?>"><?php echo $content->title; ?></a></h3>
                            <p><?php echo $con_short; ?></p>
                        </div>
                        <br class="clear" />
                    <?php 
					} 
					?>
                <br class="clear" />
                <div class="clear bottom_line"> &nbsp </div>
            </div>
			</div>
			</div>
			</div>

    
	<div class="content_bottom">&nbsp;</div>
	<?php include 'includes/footer.php'; ?> 
</body>
</html>