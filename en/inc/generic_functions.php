<?php
include_once 'constants.php';
function h($str)
{
echo htmlspecialchars($str);
}

function get_page_by_page_id($current_url)
{
$mydb=new connect;
$sql="SELECT p.p_id,p.title,p.meta_title,p.meta_desc,p.meta_keywords,p.content,p.header_image_flash,p.bottom_image,p.php_content from pages p where url='$current_url' LIMIT 1";
$rs=$mydb->query($sql);
$r_obj=$mydb->loadResult($rs);
$r_obj=$r_obj[0];
return $r_obj;
}

function weather()
{

$xml = simplexml_load_file('http://www.google.com/ig/api?weather=dubai');
			$information = $xml->xpath("/xml_api_reply/weather/forecast_information");
			$current = $xml->xpath("/xml_api_reply/weather/current_conditions");
			$forecast_list = $xml->xpath("/xml_api_reply/weather/forecast_conditions");
			$current_temp= round(($current[0]->temp_f['data']-32)*(5/9)) ;

echo '<span><strong>Current temperature</strong> &nbsp;'.$current_temp.'&deg;C</span>';

}

function get_prayers(){

$url =
'http://www.islamicfinder.org/prayer_service.php?country=united_arab_emirates&city=abu_dhabi&state=01&zipcode=&latitude=24.4667&longitude=54.3667&timezone=4&HanfiShafi=1&pmethod=4&fajrTwilight1=10&fajrTwilight2=10&ishaTwilight=10&ishaInterval=30&dhuhrInterval=1&maghribInterval=1&dayLight=0&simpleFormat=xml';

$xmlDoc = new DOMDocument();
$xmlDoc->load($url);

$x = $xmlDoc->documentElement;

foreach ($x->childNodes AS $item)
  {
	$arr[$item->nodeName]=$item->nodeValue;
  }
 
  return $arr;

}

function saveToursData($data=array()){
    if(isset($_POST['date_visit']) && isset($_POST['prop_time'])){
        $proposed_date_time=date("Y-m-d",strtotime($_POST['date_visit'])).' '.$_POST['prop_time'];
    }
    $mydb=new connect;
    $sql = "INSERT INTO tours_request(contact_person_group,group_booking_category,education_level,gender,ages,grades,contact_name,contact_number,org_name,org_number,purposed_date,group_size,email,other_info,created,travel_industry)
        VALUES('".$_POST['contact_person_group']."','".$_POST['group_booking_category']."','".$_POST['education_level']."','".$_POST['gender']."','".$_POST['ages']."','".$_POST['grades']."','".$_POST['name']."','".$_POST['m_number']."','".$_POST['o_g_name']."','".$_POST['org_number']."','$proposed_date_time','".$_POST['grp_size']."','".$_POST['email']."','".$_POST['other_info']."',now(),'".$_POST['travel_industry']."')";
    if($mydb->query($sql))
        return 1;
    else
        return 0;

}

// -----------------   Send Email  ---------------------

function sendEmail($toName, $fromName, $toEmail, $fromEmail, $subject, $content){
//    print "<pre>";
//    print_r($_SERVER);
    require_once 'phpmailer/class.phpmailer.php';

$fromName='SZGMC';
$fromEmail='tour@szgmc.ae';	
	
    $mailer = new PHPMailer();
    $mailer->IsHTML(true);
    $mailer->CharSet = 'UTF-8';
    $mailer->SetLanguage('ar');
    $mailer->From = $fromEmail;  // This HAVE TO be your gmail adress
    $mailer->FromName = $fromName; // This is the from name in the email, you can put anything you like here
    $mailer->Body = $content;
    $mailer->AltBody = "HTML Content not supported";
    $mailer->Subject = $subject;
    $mailer->AddAddress($toEmail);  
	// This is where you put the email adress of the person you want to mail
   // $mailer->AddCC('rashi@clicksnhits.ae', 'Rashi');
//    $mailer->AddCC('Adeelyounas@hotmail.co.uk', 'Adeel');
   // $mailer->AddCC('vyshak.p@timegroup.ae', 'Vyshakh');
    if(!$mailer->Send())
    {
	return TRUE;
      // echo "Message was not sent<br/ >";
      // echo "Mailer Error: " . $mailer->ErrorInfo;
    }else{
        return TRUE;
    }
}

function getContent($post, $result, $file){
    $content = "<html>
                    <head><meta http-equiv='Content-Type' content='text/html; charset=utf-8' /></head>
                    <body>
                <table width='600px' cellspacing='2' style='direction:ltr;'>
                    <tr>
                        <td colspan='2' ><h1>Filming Media Request</h1></td></tr>";
    $content .="<tr><td>Applicant Full Name: </td><td>".$post['a_fname']."</td></tr>";
    $content .="<tr><td>Applicant Nationality:</td><td>".$post['a_nationality']."</td></tr>";
    $content .="<tr><td>Head of Film Crew Full Name: </td><td>".$post['a_h_fname']."</td></tr>";
    $content .="<tr><td>Head of Film Crew Nationality : </td><td>".$post['a_h_nationality']."</td></tr>";
    $content .="<tr><td>Company Name : </td><td>".$post['a_company']."</td></tr>";
    $content .="<tr><td>Company Name commissioning the film: </td><td>".$post['a_comp_comm']."</td></tr>";
    $content .="<tr><td>Purpose of the Film: </td><td>".$post['purpose']."</td></tr>";
    $content .="<tr><td>Files: </td><td><a href='".CREW_DOCS.$file."'>".getImageName($file)."</a></td></tr>";
    $content .="<tr><td>Expected date film will be 'published', 'aired': </td><td>".$post['pub_date']."</td></tr>";
    $content .="<tr><td>Proposed Date of Visit: </td><td>".$post['prop_date']."</td></tr>";
    $content .="<tr><td>Proposed Time: </td><td>".$post['prop_time']."</td></tr>";

    $content .="<tr><td>Number of persons expected to be in the crew: </td><td>".$post['num_persons_expected']."</td></tr>";
	if($post['short_intro']!='')
    $content .="<tr><td>A short introduction for the content of the film: </td><td>".$post['short_intro']."</td></tr>";	
	if($post['equipments']!='')
    $content .="<tr><td>Equipment ﾖ provide details of any equipment being used during the shoot: </td><td>".$post['equipments']."</td></tr>";		

    $content .="<tr><td>Contact Number: </td><td>".$post['contact_num']."</td></tr>";
    $content .="<tr><td>Email Address: </td><td>".$post['email']."</td></tr>";
    $content .="<tr><td>Special request or requirements: </td><td>".$post['special_req']."</td></tr>";
    $content .="<tr><td>Other information: </td><td>&nbsp;".$post['other_info']."</td></tr>";


    $content .="<tr><td colspan='2'>"
                ."<table style='text-align:center;' align='center' width='600px' cellspacing='2'><thead><tr>"
                ."<th>Full name</th><th>Job Title</th><th>Job Title</th><th>Telephone num</th><th>Email</th><th>File name</th></tr></thead><tbody>";
    if(!empty ($result)){
        foreach($result as $res){ 
            $content .= "<tr><td> ".$res->fname."</td><td> ".$res->title."</td><td> ".$res->address."</td><td> ".$res->telephone."</td><td> ".$res->email."</td><td> <a href='".CREW_DOCS.$res->file_name."'>".getImageName($res->file_name)."</a></td></tr>";
        }
    }
    $content .= "</tbody></table>";

    $content .="</table></body></html>";
    return $content;

}

function getContent1($post, $result, $file){
    $content = "<html>
                    <head><meta http-equiv='Content-Type' content='text/html; charset=utf-8' /></head>
                    <body>
                <table width='600px' cellspacing='2' style='direction:ltr;'>
                    <tr>
                        <td colspan='2' ><h1>Filming Media Request</h1></td></tr>";
    $content .="<tr><td>First Name: </td><td>".$post['fname']."</td></tr>";
	 $content .="<tr><td>Last Name: </td><td>".$post['lname']."</td></tr>";
	  $content .="<tr><td>Passport Number: </td><td>".$post['p_num']."</td></tr>";
    $content .="<tr><td> Nationality:</td><td>".$post['nationality']."</td></tr>";
   
    $content .="<tr><td>Date of Birth : </td><td>".$post['dob']."</td></tr>";
    $content .="<tr><td>Company/Sponsor< : </td><td>".$post['company']."</td></tr>";
     $content .="<tr><td>Files: </td><td><a href='".CREW_DOCS.$file."'>".getImageName($file)."</a></td></tr>";
     $content .="<tr><td>Name to appear on certificate : </td><td>".$post['appear']."</td></tr>";
	 $content .="<tr><td>Male/Female : </td><td>".$post['sex']."</td></tr>";
	 $content .="<tr><td>Tourist Guide Licenses in the UAE : </td><td>".$post['license']."</td></tr>";
	  $content .="<tr><td>P.O. Box : </td><td>".$post['pob']."</td></tr>";
	  $content .="<tr><td>Mobile : </td><td>".$post['mobile']."</td></tr>";
	  $content .="<tr><td>Residence Telephone : </td><td>".$post['phone']."</td></tr>";
	    $content .="<tr><td>Email Address : </td><td>".$post['c_email']."</td></tr>";
    $content .= "</tbody></table>";

    $content .="</table></body></html>";
    return $content;

}

function getContentTours($post){
//    print_r($post);die();
    $content = "<html>
                    <head><meta http-equiv='Content-Type' content='text/html; charset=utf-8' /></head>
                    <body><table width='600px' cellspacing='2' style='direction:ltr;'>
                    <tr>
                    <td colspan='2' ><h1>SZGMC Tour Request</h1></td></tr>";
    $content .="<tr><td> Contact name:</td><td>".$post['name']."</td></tr>";
    $content .="<tr><td>Email:</td><td>".$post['email']."</td></tr>";
    $content .="<tr><td>Organization/Group Name:</td><td>".$post['o_g_name']."</td></tr>";			
    $content .="<tr><td>Organization phone number:</td><td>".$post['org_number']."</td></tr>";

    $content .="<tr><td>Contact name of person accompanying the group:</td><td>".$post['contact_person_group']."</td></tr>";
    $content .="<tr><td>Contact mobile number of person accompanying the group:</td><td>".$post['m_number']."</td></tr>";	

$content .="<tr><td>Group booking category:</td><td>".$post['group_category']."</td></tr>";

if($post['group_category']=='Education')
{
$content .="<tr><td>Gender:</td><td>".$post['gender']."</td></tr>";
$content .="<tr><td>Grade(s):</td><td>".$post['grades']."</td></tr>";
$content .="<tr><td>Age(s):</td><td>".$post['ages']."</td></tr>";
}
if($post['group_category']=='Government')
{
$content .="<tr><td>&nbsp;</td><td>".$post['government']."</td></tr>";
}
if($post['group_category']=='Embassy')
{
$content .="<tr><td>Country:</td><td>".$post['country']."</td></tr>";
$content .="<tr><td>Officials attending tour:</td><td>".$post['officials']."</td></tr>";
}
if($post['travel_industry']!='')
{
$content .="<tr><td>Travel Industry:</td><td>".$post['travel_industry']."</td></tr>";
}

if($post['group_category']=='Media')
{
$content .="<tr><td>Country:</td><td>".$post['media']."</td></tr>";
$content .="<tr><td>Officials attending tour:</td><td>".$post['country1']."</td></tr>";
}



    $content .="<tr><td>Proposed date of visit:</td><td>".$post['date_visit']."</td></tr>";
    $content .="<tr><td>Proposed time of visit:</td><td>".$post['prop_time']."</td></tr>";

    $content .="<tr><td>Group Size:</td><td>".$post['grp_size']."</td></tr>";
    $content .="<tr><td>Specify any special interests or needs for the group:</td><td>&nbsp;".$post['other_info']."</td></tr>";
    $content .="</table></body></html>";
    return $content;

}

function getContentByFieldFromTable($table, $field, $value){
    $mydb=new connect;
    $sql="SELECT * FROM $table where $field='$value'";
    $rs=$mydb->query($sql);
    $r_obj=$mydb->loadResult($rs);
    return $r_obj;
}

function ifExist($idName,$table, $id){
    $mydb=new connect;
    $sql="SELECT $idName from $table where $idName='$id' LIMIT 1";
    $rs=$mydb->query($sql);
    if($row =  mysql_fetch_object($rs)){
        return 1;
    }else{
        return 0;
    }
}

function getImageName($imageName){
    $name = explode("_", $imageName);
    return $name[1];
}
?>