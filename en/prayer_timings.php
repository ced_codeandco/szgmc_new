<?php 
session_start();
?>
<style>
	.input_row_dob #pub_date{
		width: 240px;
	}
</style>
<?php
include './includes/database.php';
include './includes/functions.php';
include './includes/config.php';
include_once('./includes/generic_functions.php');
$conf = new Configuration();
$db = new MyDatabase();
$site_path = $conf->site_url;
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php include 'includes/common_header.php'; ?>
    <title>Sheikh Zayed Grand Mosque Center+</title>
</head>
<body>
    
    <?php include 'includes/menus/banner_header.php'; ?>
    <!-- Banner start -->
    <div class="banner">
	<img src="<?php echo $site_path; ?>images/visiting_the_mosque_banner.jpg">     
    </div>   
    <!-- Banner Close -->
    
    <div class="main_box_content">
            <?php include 'includes/menus/nav_menu.php'; ?>
        <div class="clear"></div>
        <div class="content">
        	<div class="brad_cram">
            	<ul>
            	   <li><a href="<?php echo $site_path; ?>">Home</a></li>
                    <li><a href="#" class="active">Prayer Timings</a></li>
                </ul>
            </div>
      <div class="content-left">
                <?php // include 'includes/menus/left_menu.php'; 
				include 'includes/ads/ad_216_240.php';
				?>
                <?php 
				include 'includes/menus/ministry_logos.php'; 
				include 'includes/menus/left_menu.php'; 
				?>
            </div>
      
      	<div class="content-right" style="margin-left:37px">
            <?php $prayerTimes = getPrayerPublicApi();?>
            <div class="content-middle-box-round-corner prayer-rund">
                <div class="prayer-timing-title-div-page">Prayer Timings</div>
                <div class="prayer-timing-div-page prayer_timing abu">Abu Dhabi Timings</div>
                <div class="prayer-timing-div-page prayer_timing odd"><span class="time-title">Fajr</span> <span><?php echo $prayerTimes['Fajr'];?></span></div>
                <div id="prayertiming" style="display: none+;">
                    <div class="prayer-timing-div-page prayer_timing"><span class="time-title">Sunrise</span> <span><?php echo $prayerTimes['Shurooq'];?></span></div>
                    <div class="prayer-timing-div-page prayer_timing odd"><span class="time-title">Dhuhr</span> <span><?php echo $prayerTimes['Zuhr'];?></span></div>
                    <div class="prayer-timing-div-page prayer_timing"><span class="time-title">Asr</span> <span><?php echo $prayerTimes['Asr'];?></span></div>
                    <div class="prayer-timing-div-page prayer_timing odd"><span class="time-title">Maghrib</span> <span><?php echo $prayerTimes['Maghrib'];?></span></div>
                    <div class="prayer-timing-div-page prayer_timing"><span class="time-title">Isha</span> <span><?php echo $prayerTimes['Isha'];?></span></div>
                </div>
            </div>
       
        </div>
      
      
     
      
        </div>
     <div class="clear"></div> </div>
    
    
    
    
    
 
	<div class="content_bottom">&nbsp;</div>
	<?php include 'includes/footer.php'; ?> 
	<?php
	
	   if($msg == 'success'){
	  
		 ?>
		<script>
			$(function() {
				$( "#dialog" ).dialog({maxWidth:480,modal:true,width:'90%'});
			});
		</script>
        <script type="text/javascript">
		$(function(){
			$('#confirm').click(function() {
				//$('#dialog').hide();
				$( "#dialog" ).dialog( "close" );
				window.location = "/en/careers";
				return false;
			});
		});
		
        </script>
        <div id="dialog" class="lostpopup">
        	<p style="text-align:center;"><span dir="rtl">لقد تم تقديم طلبك بنجاح ، وسوف نقوم بالتواصل معك قريبا.</span></p>
            <p>Your request has been submitted successfully. We will contact you shortly.</p>
            <form name="feedback_frm" method="post" id="fbk_frm">
                <div>
                   <label for="comments">Kindly provide us your feedback on this e-service.</label>
                   <label for="comments" class="arabic">يرجى تقديم ملاحظاتك عن هذه الخدمة الإلكترونية.</label>
                   <textarea name="comments" cols="50" rows="4"></textarea>
                   <input type="hidden" name="path" value="careers"/>
				    <input type="hidden" name="tour_ref" value="<?php echo $ref;?>"/>
                </div>
                <div align="center" class="clear">
                  <input type="button" style="direction:ltr;" id="submitTour" name="submitTour" class="cmt_btn" value="Submit ﺍرسل" onClick="postFeedback();" />
                  <input type="button" style="direction:ltr;" id="confirm" class="cmt_btn" value="No Comments لا تعليق" />
                </div>
	   		</form>
        </div>
		 <?
		
		}
		else if($msg == 'failed')
		{
		?>
		  <script> alert("Cannot process your request"); </script>
		 <?
		  print('<script type="text/javascript">window.location = "/en/careers";</script>');
exit(); 
		}else if($msg == 'Invalid')
		{
		?>
			<script> alert("Invalid captcha"); </script>
		<?php
			print('<script type="text/javascript">window.location = "/en/carrers.php";</script>');
			exit(); 
		}
	  
	?>
</body>
</html>