<script language="javascript" type="text/javascript">
var min=12;
var max=14;
function increaseFontSize() {
 
   var p = document.getElementsByTagName('body');
   var l= document.getElementsByTagName('li');
   for(i=0;i<p.length;i++) {
 
      if(p[i].style.fontSize) {
         var s = parseInt(p[i].style.fontSize.replace("px",""));
      } else {
 
         var s = 12;
      }
      if(s!=max) {
 
         s += 1;
      }
      p[i].style.fontSize = s+"px"
 
   }

}
function decreaseFontSize() {
   var p = document.getElementsByTagName('body');
   var l= document.getElementsByTagName('li');
   for(i=0;i<p.length;i++) {
 
      if(p[i].style.fontSize) {
         var s = parseInt(p[i].style.fontSize.replace("px",""));
      } else {
 
         var s = 12;
      }
      if(s!=min) {
 
         s -= 1;
      }
      p[i].style.fontSize = s+"px"
 
   }

}
</script>
<?php
		
 $eurl = explode('/',$_SERVER['REQUEST_URI']);
if(($eurl[2]=="publications") && end($eurl)!== "publications")
{
$eurl = "publications/".end($eurl);
}
else if(end($eurl)== "theory-and-implementation")
{
$eurl = "achievement";
}
else if(end($eurl)== "message-of-the-mosque")
{
$eurl = "grand-mosque-message";
}
else if(($eurl[2]=="news-detail") )
{
$eurl = "news-list";
}
else if(($eurl[2]=="news-list") )
{
$eurl = "news-list";
}
else if(($eurl[2]=="2") )
{
header("location:/en/news-list");
}
else if(($eurl[2]=="activities-detail") )
{
$eurl = "center-activities";
}
else if(($eurl[2]=="video-gallery") )
{
$eurl = "video-gallery";
}
else
{
 $eurl = end($eurl);
}


ob_start();
 $main_menu = isset($main_menu) ? $main_menu : '' ?>
            <div class="logo" style="width:480px;"> 
			<img src="<?php echo $site_path; ?>images/logo.jpg" /> <span style="padding-left:10px;">Sheikh Zayed Grand Mosque Center</span> </div>
            <div style="float:left;margin-left:0px;width:220px;"> &nbsp; </div>
            <div class="date" style="margin-top:98px;"><?php
            $dt=date("D, M j Y h:i a ");
            $todayDate = date("Y-m-d g:i a");// current date
            $currentTime = time($todayDate); //Change date into time
            $new_time = $currentTime;
            echo date("D, M j Y h:i a",$new_time);
            //echo $dt;
            ?><!--<div class="img">&nbsp;</div>--></div>
			<div class="logo2"><img src="<?php echo $site_path; ?>images/logo_2.jpg" /> </div>
            <div class="clear top_menu">
                <div class="home"> <a href="<?php echo $site_path; ?>"> Home </a></div>
                <div class="contactus"> <a href="<?php echo $site_path; ?>contact-us" style="padding:10px 0;" > Contact us </a></div>
                <div class="contactus  "> <a href="<?php echo $site_path; ?>sitemap" class="<?php if($eurl=='sitemap'){ ?> current <?php } ?>" style="padding:10px 6px"> Sitemap&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </a></div> 
                <div class="contactus" style="width:114px;"> <a href="<?php echo $site_path; ?>help" class="<?php if($eurl=='help'){ ?> current <?php } ?>"> Help&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </a></div> 
                <div class="search">
                <div style="margin-left:20px;">
                 <div style="float:left;width:23px; margin-top:22px; margin-left:0px; padding-top:2px;"><a title="Decrease font" style="margin-left:10px;width:23px; " href="javascript:decreaseFontSize();">A-</a></div><div style="float:left;width:20px; margin-top:19px; margin-left:0px; padding-top:2px;"><a title="Increase font" style="font-size:16px;; margin-left:5px;" href="javascript:increaseFontSize();">A+</a></div>
                 </div>
                    <form action="<?php echo $site_path; ?>search.php" method="get">
                        <input class="top-search-box" name="key" value="<?php echo isset($search_title)? $search_title : 'Search...'; ?>" id="search_text">
                        <input type="submit" alt="Search" class="submitbutton" value="">
	
                        <div class="languages"><a href="/<?php echo $eurl;?>">&#1593;&#1585;&#1576;&#1610;</a></div>	  
                  </form>
                  
                </div>
            </div>
        <div class="clear banner">
            <div class="clear menu">
                <div id="mydroplinemenu" class="droplinebar">
                    <ul >
                      <li id="szgmc" > <a <?php echo $main_menu == 'szgmc'? 'class="current"' : ''; ?> href="<?php echo $site_path; ?>about-szgmc" > About SZGMC </a> </li>
                      <li id="founder"> <a <?php echo $main_menu == 'founder'? 'class="current"' : ''; ?> href="<?php echo $site_path; ?>founder" > Founding Father </a>  </li>
                      <li id="about"> <a <?php echo $main_menu == 'about'? 'class="current"' : ''; ?> href="<?php echo $site_path; ?>vision-dream-sheikh-zayed-mosque"> About the Mosque </a> </li>
                      <li id="archi"> <a <?php echo $main_menu == 'archi'? 'class="current"' : ''; ?> href="<?php echo $site_path; ?>general-architecture"> Architecture </a> </li>
                      <li id="contact" > <a <?php echo $main_menu == 'contact'? 'class="current"' : ''; ?> href="https://webmail.szgmc.ae/"> Employee Login</a> </li>
                    </ul> 
                </div>	 
                <div class="social_media">
               
                	<a id="facebook" target='blank' href="<?php echo $conf->fb_url; ?>" >&nbsp;</a>
                    <a id="youtube" target='blank' href="http://www.youtube.com/szgmc" >&nbsp;</a>
                    <a id="rss" target='blank' href="http://feeds.feedburner.com/szgmc/DmyK" >&nbsp;</a>
                    <!--<div id="twitter" href="<?php echo $conf->twitter_url; ?>">&nbsp;</div>-->
                </div>
                <div class="top">
                    	<div class="weather"></div>
                    <div class="vision"><a href="<?php echo $site_path; ?>vision-mission-values">Vision, Mission & Values</a></div>
					<?php include 'includes/menus/sub_menu_weather.php'; ?>
                </div>
            </div>
            <div class="slider"> 
                <div id="slider" class="nivoSlider">
                    <img src="<?php echo $site_path; ?>images/mainbanner1.png" /> 
                   <!-- <img src="<?php echo $site_path; ?>images/mainbanner2.png" />
                    <img src="<?php echo $site_path; ?>images/mainbanner3.png" /> 
                    <img src="<?php echo $site_path; ?>images/mainbanner4.png" /> 
                    <img src="<?php echo $site_path; ?>images/mainbanner5.png" /> 
                   <img src="<?php echo $site_path; ?>images/mainbanner6.png" />  -->
                </div>
            </div>
        </div>