		   <div id="szgmc_menu" style="display:none" class="dropdown_menu szgmc">
            <div class="top_arrow">&nbsp;</div>
			<ul>
			 <li class="no_divider"><a href="<?php echo $site_path; ?>about-szgmc"><span>About <br/> SZGMC</span></a></li>
			 <li><a href="<?php echo $site_path; ?>minister-hh"><span>Speech of His Highness <br/> Sheikh Mansoor Bin Zayed</span></a></li>
			 <li><a href="<?php echo $site_path; ?>chairmans-message"><span>Chairman's <br/> Message</span></a></li>
			 <li><a href="<?php echo $site_path; ?>DG-message"><span>DG's <br/> Message</span></a></li>
			 <li><a href="<?php echo $site_path; ?>board-members"><span>Board <br/> Members</span></a></li>
			 <li><a href="<?php echo $site_path; ?>organizational-structure"><span>Organizational <br />Structure</span></a></li>
			 <li><a href="<?php echo $site_path; ?>vision-mission-values"><span>Vision, <br>Mission</span></a></li>
			</ul>
           </div>

           <div id="founder_menu" style="display:none" class="dropdown_menu founder">
           <div class="top_arrow">&nbsp;</div>
		   <ul>
			 <li class="no_divider"><a href="<?php echo $site_path; ?>founder"><span>Founding <br/>Father</span></a></li>
			 <li><a href="<?php echo $site_path; ?>selected-sayings-by-sheikh-zayed-al-nahyan"><span>Selected Sayings <br/>by Sheikh Zayed Al Nahyan</span></a></li>
			 <li><a href="<?php echo $site_path; ?>they-said-about-zayed"><span>They said <br/>about Sheikh Zayed</span></a></li>
			 <li><a href="<?php echo $site_path; ?>sheikh-zayed-and-grand-mosque"><span>Sheikh Zayed <br/>and the Grand Mosque</span></a></li>
			
			</ul>
           </div> 
		   
		   <div id="about_menu" style="display:none" class="dropdown_menu about">
           <div class="top_arrow">&nbsp;</div>
		   <ul>
			 <li class="no_divider"><a href="<?php echo $site_path; ?>vision-dream-sheikh-zayed-mosque"><span>Vision and <br />Dream</span></a></li>
             <li ><a href="<?php echo $site_path; ?>theory-and-implementation"><span>Theory and <br/>Implementation</span></a></li>
			 <!-- <li><a href="<?php echo $site_path; ?>message-of-the-mosque"><span>Message of the <br/> Mosque</span></a></li> -->
             		
			
			</ul>
           </div> 
		   
		   <div id="archi_menu" style="display:none" class="dropdown_menu archi">
           <div class="top_arrow">&nbsp;</div>
		   <ul>
			 <li class="no_divider"><a href="<?php echo $site_path; ?>general-architecture"><span>General<br/> Architecture</span></a></li>
             <li><a href="<?php echo $site_path; ?>domes"><span class="p_top">Domes<br/>&nbsp;</span></a></li>
			 <li><a href="<?php echo $site_path; ?>marbles"><span class="p_top">Marble<br/>&nbsp;</span></a></li>
			 <li><a href="<?php echo $site_path; ?>lunar-illumination"><span class="p_top">Lunar Illumination</span></a></li>
			 <li><a href="<?php echo $site_path; ?>carpets"><span class="p_top">Carpets<br/>&nbsp;</span></a></li>
			 <li><a href="<?php echo $site_path; ?>chandeliers"><span class="p_top">Chandeliers<br/>&nbsp;</span></a></li>
			 <li><a href="<?php echo $site_path; ?>pulpit"><span>Pulpit <br/>(Menbar)</span></a></li>
			</ul>
           </div>