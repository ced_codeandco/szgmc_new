<div class="nav">
        	<div class="navigation">
 <div id="smoothmenu1" class="ddsmoothmenu mosquemanner">
    	<ul>
        	 <li><a href="<?php echo $site_path; ?>about-szgmc">About SZGMC</a>
                 <ul class="plan_your_visit">
                 <li><a href="<?php echo $site_path; ?>minister-hh">Speech of H.H. Sheikh Mansoor Bin Zayed</a></li>
                 <li><a href="<?php echo $site_path; ?>chairmans-message">Chairman's  Message</a></li>
                 <li><a href="<?php echo $site_path; ?>DG-message">DG's  Message</a></li>
                 <!--<li><a href="<?php echo $site_path; ?>board-members">Board  Members</a></li>-->
                 <li><a href="<?php echo $site_path; ?>organizational-structure">Organizational Structure</a></li>
                 <li><a href="<?php echo $site_path; ?>vision-mission-values">Vision, Mission & Values</a></li>
                 <li><a href="<?php echo $site_path; ?>sheikh-zayed-and-grand-mosque">Sheikh Zayed and the Grand Mosque</a></li>
                 <li><a href="<?php echo $site_path; ?>theory-and-implementation">Phases of Construction</a></li>
               </ul>
          </li>
             
             <li><a href="<?php echo $site_path; ?>visiting-the-mosque">Visiting The Mosque</a>
              <ul>
                <li><a href="<?php echo $site_path; ?>visit-mosque-prayers">Prayers</a></li>
				<li><a href="<?php echo $site_path; ?>visit-mosque-visitors">Visitors</a></li>
				<li><a href="<?php echo $site_path; ?>visit-mosque-delegates">Delegates & Tour Operators</a></li>
                 <?php /*?><li><a href="<?php echo $site_path; ?>mosque-manner">Mosque Manners</a></li>
                 <li><a href="<?php echo $site_path; ?>mosque-opening-hours">Visiting Times</a></li>
                 <li><a href="<?php echo $site_path; ?>visitor-services">Visitor Services</a></li>
                 <li><a href="<?php echo $site_path; ?>electronic-services">e-guide</a></li>
                 <li><a href="<?php echo $site_path; ?>what-is-tour">What is Tour</a></li>
                 <li><a href="<?php echo $site_path; ?>tour-booking-form">Book Your Tour</a></li>
                 <li><a href="<?php echo $site_path; ?>getting-to-the-mosque" >Getting Here</a></li>
                 <li><a href="https://www.google.com/maps/views/streetview/sheikh-zayed-grand-mosque?gl=us" target="_blank">Google Virtual Tour</a></li>
                 <li><a href="http://www.tripadvisor.com/Attraction_Review-g294013-d1492221-Reviews-Sheikh_Zayed_Grand_Mosque_Center-Abu_Dhabi_Emirate_of_Abu_Dhabi.html" target="_blank">Rate Us on Trip Advisor</a></li>
                 <li><a href="<?php echo $site_path; ?>questions">FAQs</a></li>
                 <li><a href="<?php echo $site_path; ?>important-information">Important Information</a></li>
                 <li><a href="<?php echo $site_path; ?>esurvey">eSurvey</a></li><?php */?>
               </ul>
             </li>
             
             <li><a href="<?php echo $site_path; ?>e_services.php">E-Services</a>
             <ul>
                 <li><a href="<?php echo $site_path; ?>tour-booking-form">Group Tour Booking</a></li>
                 <li><a href="<?php echo $site_path; ?>careers">Careers</a></li>
                 <li><a href="<?php echo $site_path; ?>media-form">Filming Permission</a></li>
                 <li><a href="<?php echo $site_path; ?>lost-found">Lost and found</a></li>
                 <li><a href="<?php echo $site_path; ?>suggestion-complaint">Feedback & suggestions</a></li>
                 <?php /*<li><a href="<?php echo $site_path; ?>juniorculturalguide.php">Junior Cultural Guide Program</a></li>
                 <li><a href="<?php echo $site_path; ?>quran_recitation_course">Quran Recitation Course</a></li>*/?>
                  <!--<li><a href="<?php /*echo $site_path; */?>log_book.php">Daily Log Book </a></li>-->
             </ul>
             </li>
             
             <li><a href="javascript:void(0);">Media Corner</a>
             <ul>
                 <li><a href="<?php echo $site_path; ?>news-list">News</a></li>
                 <li><a href="<?php echo $site_path; ?>press-kit">Press Kit</a></li>
                 <li><a href="<?php echo $site_path; ?>eparticipation">E-Participation</a></li>
             </ul>
             
             </li>
             <li><a href="javascript:void(0);">Library</a>
             <ul>
                 <li><a href="<?php echo $site_path; ?>about-the-library">About the library</a></li>
                 <li><a href="<?php echo $site_path; ?>library-resources">Resources</a></li>
                 <li><a href="<?php echo $site_path; ?>library-services">Library Services</a></li>
                 <li><a href="<?php echo $site_path; ?>search-library">Search</a></li>
                 <li><a href="<?php echo $site_path; ?>publications">Publications</a></li>
             </ul>
             
             </li>
              <li><a href="<?php echo $site_path; ?>events-and-activities">Events & Activities</a>
                  <ul>
                      <li><a href="<?php echo $site_path; ?>exhibitions">Exhibitions</a></li>
                      <li><a href="<?php echo $site_path; ?>activities">Activities</a></li>
                      <li><a href="<?php echo $site_path; ?>social-initiatives">Social Initiatives</a></li>
                      <li><a href="<?php echo $site_path; ?>spaces-of-light">Spaces of Light</a></li>
                  </ul>
              </li>
              
              <li><a href="<?php echo $site_path;?>religious-programs">Religious Programs</a>
                <ul>
                    <li><a href="<?php echo $site_path; ?>religious-courses">Religious Courses</a></li>
                    <?php /*?><li><a href="<?php echo $site_path; ?>religious-lecture">Religious Lecture</a></li><?php */?>
                    <li><a href="<?php echo $site_path; ?>zikr-al-hakeem">Al Thikr Al Hakeem</a></li>
                    <li><a href="<?php echo $site_path; ?>friday-sermon">Friday Sermon</a></li>
                    <li><a href="<?php echo $site_path; ?>ramadan-activities">Ramadan Activities</a></li>
                    <?php /*?><li><a href="<?php echo $site_path; ?>prayer-information">Prayer Information</a></li><?php */?>
                </ul>
            </li>
              
              
             <li><a href="<?php echo $site_path; ?>suggestion-complaint">Suggestions</a></li>
	     
	     <li><a href="<?php echo $site_path; ?>architecture">Architecture</a>
           <ul>
             <li><a href="<?php echo $site_path; ?>general-architecture">General Architecture</a></li>
             <li><a href="<?php echo $site_path; ?>domes">Domes</a></li>
			 <li><a href="<?php echo $site_path; ?>marbles">Marble</a></li>
			 <li><a href="<?php echo $site_path; ?>lunar-illumination">Lunar Illumination</a></li>
			 <li><a href="<?php echo $site_path; ?>carpets">Carpets</a></li>
			 <li><a href="<?php echo $site_path; ?>chandeliers">Chandeliers</a></li>
			 <li><a href="<?php echo $site_path; ?>pulpit">Pulpit&nbsp;(Menbar)</a></li>

			 <li><a href="<?php echo $site_path; ?>minaret">Minaret </a></li>
			 <li><a href="<?php echo $site_path; ?>reflective-pools"> Reflective Pools</a></li>
			 <li><a href="<?php echo $site_path; ?>columns">Columns </a></li>
			 <li><a href="<?php echo $site_path; ?>mihrab">Mihrab </a></li>
			 <li><a href="<?php echo $site_path; ?>sahan">The Sahan</a></li>
             </ul>
         </li>
             
             <li><a href="javascript:void(0);" >Open Data</a>
             <ul style="width:116px;">
                 <?php /*?><li><a href="<?php echo $site_path; ?>poll-archive" style="width:116px;">Poll Results</a></li><?php */?>
                 <li><a href="<?php echo $site_path; ?>open-data"  style="width:116px;">Statistics</a></li>
             </ul>
             </li>
     </ul>
    	
    </div>
 </div>
        </div>