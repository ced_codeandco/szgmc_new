
<div class="content-left-box">
  <div class="content-left-box-txt-menu">
    <div class="content-left-box-txt-menu-left"><a href="<?php echo $site_path; ?>visiting-the-mosque">Visiting The Mosque</a></div>
    <div class="content-left-box-txt-menu-right"><a href="<?php echo $site_path; ?>visiting-the-mosque"><img src="<?php echo $site_path; ?>images/button.png" width="33" height="38" /></a></div>
  </div>
  <img src="<?php echo $site_path; ?>images/visiting-the-mosque-image.jpg" width="210" height="198" /> </div>
<div class="content-left-box">
  <div class="content-left-box-txt-menu">
    <div class="content-left-box-txt-menu-left"><a href="<?php echo $site_path; ?>religious-programs">Religious Programs</a></div>
    <div class="content-left-box-txt-menu-right"><a href="<?php echo $site_path; ?>religious-programs"><img src="<?php echo $site_path; ?>images/button.png" width="33" height="38" /></a></div>
  </div>
  <img src="<?php echo $site_path; ?>images/religious-programs-image.jpg" width="210" height="198" /></div>
<div class="content-left-box-round-corner prayer-rund">
  <div class="prayer-timing-title-div">Prayer Timings</div>
    <?php $prayerTimes = getPrayerPublicApi();?>
  <div class="prayer-timing-div prayer_timing">Abu Dhabi Timings</div>
  <div class="prayer-timing-div prayer_timing">Fajr <span><?php echo $prayerTimes['Fajr'];?></span></div>
  <div id="prayertiming" style="display: none+;">
      <div class="prayer-timing-div prayer_timing">Sunrise <span><?php echo $prayerTimes['Shurooq'];?></span></div>
      <div class="prayer-timing-div prayer_timing">Dhuhr <span><?php echo $prayerTimes['Zuhr'];?></span></div>
      <div class="prayer-timing-div prayer_timing">Asr <span><?php echo $prayerTimes['Asr'];?></span></div>
      <div class="prayer-timing-div prayer_timing">Maghrib <span><?php echo $prayerTimes['Maghrib'];?></span></div>
      <div class="prayer-timing-div prayer_timing">Isha <span><?php echo $prayerTimes['Isha'];?></span></div>
  </div>
  <!--<span  class="prayer_seemore"><a  id="flip"  href="javascript:void(0);">See more</a></span> <span  class="prayer_seeless"><a  id="flip1"  href="javascript:void(0);">See less</a></span>-->
</div>
<div class="content-left-box tour-operators-log">
  <div class="content-left-box-txt-menu">
    <div class="content-left-box-txt-menu-left"><a href="<?php echo $site_path; ?>userlogin.php">Tour Operators Login</a></div>
    <div class="content-left-box-txt-menu-right"><a href="<?php echo $site_path; ?>userlogin.php"><img src="<?php echo $site_path; ?>images/button.png" width="33" height="38" /></a></div>
  </div>
  <img src="<?php echo $site_path; ?>images/tour-operators-login-image.jpg" width="210" height="90" /></div>

<!--<div class="content-left-box tour-operators-log">
    <div class="content-left-box-txt-menu">
        <div class="content-left-box-txt-menu-left"><a href="<?php /*echo $site_path; */?>juniorculturalguide.php">Junior Cultural Guide </a></div>
        <div class="content-left-box-txt-menu-right"><a href="<?php /*echo $site_path; */?>userlogin.php"><img src="<?php /*echo $site_path; */?>images/button.png" width="33" height="38" /></a></div>
    </div>
    <img src="<?php /*echo $site_path; */?>images/jcg-right-banner.jpg" width="210" height="90" /></div>-->

<div class="content-left-box">
  <div class="content-left-box-txt-menu">
    <div class="content-left-box-txt-menu-left"><a href=" https://itunes.apple.com/us/app/sheikh-zayed-grand-mosque/id715431100?mt=8" target="_blank">Install our App</a></div>
    <div class="content-left-box-txt-menu-right"><a href=" https://itunes.apple.com/us/app/sheikh-zayed-grand-mosque/id715431100?mt=8" target="_blank"><img src="<?php echo $site_path; ?>images/button.png" width="33" height="38" /></a></div>
  </div>
  <img src="<?php echo $site_path; ?>images/zayed-mosque-app-image.jpg" width="210" height="98" /></div>
<div class="poll_box_left"> <span class="poll_title">Polls <img src="<?php echo $site_path; ?>images/poll_right_bg.png"></span>
  <p>What is your opinion about our e-Guide devices?</p>
  <div class="chk_box">
    <table>
      <tr>
        <td><input type="radio" name="radiog_lite" id="radio1" class="css-checkbox" />
          <label for="radio1" class="css-label">Useful & Informative</label></td>
      </tr>
      <tr>
        <td><div class="percentdage_box">
            <div class="box_01"> <span class="gold_box1"></span> </div>
            <span class="box_right_02"><strong>(91%)</strong></span> </div></td>
      </tr>
      <tr>
        <td><input type="radio" name="radiog_lite" id="radio2" class="css-checkbox" />
          <label for="radio2" class="css-label">Non Practical</label></td>
      </tr>
      <tr>
        <td><div class="percentdage_box">
            <div class="box_01"> <span class="gold_box2"></span> </div>
            <span class="box_right_02"><strong>(1%)</strong></span> </div></td>
      </tr>
      
        <td><input type="radio" name="radiog_lite" id="radio3" class="css-checkbox" />
          <label for="radio3" class="css-label">Needs Improvement</label></td>
      </tr>
      <tr>
        <td><div class="percentdage_box">
            <div class="box_01"> <span class="gold_box3"></span> </div>
            <span class="box_right_02"><strong>(7%)</strong></span> </div></td>
      </tr>
    </table>
  </div>
  <div class="left_vote_box">
    <p>111 votes cast</p>
    <a href="<?php echo $site_path; ?>" class="vote_btn"><img src="<?php echo $site_path; ?>images/vote_btn.jpg"></a> </div>
  <span class="previous_poll"><a href="<?php echo $site_path; ?>poll-archive">Previous Polls</a></span> </div>
