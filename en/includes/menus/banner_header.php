<script language="javascript" type="text/javascript">
    var min=12;
    var max=20;
    var fontSize = 12;
    function increaseFontSize() {

        var p = document.getElementsByTagName('p');
        var li= document.getElementsByTagName('li');
        var div= document.getElementsByTagName('div');
        var a= document.getElementsByTagName('a');
        magnifieFont(p);
        magnifieFont(li);
        magnifieFont(div);
        magnifieFont(a);
    }
    function magnifieFont(p){
        for(i=0;i<p.length;i++) {

            if(p[i].style.fontSize) {
                var s = parseInt(p[i].style.fontSize.replace("px",""));
            } else {
                var s = 12;
            }
            if(s!=max) {

                s += 1;
            }
            p[i].style.fontSize = s+"px"
        }
    }
    function decreaseFontSize() {
        var p = document.getElementsByTagName('p');
        var li= document.getElementsByTagName('li');
        var div= document.getElementsByTagName('div');
        var a= document.getElementsByTagName('a');
        deMagnifieFont(p);
        deMagnifieFont(li);
        deMagnifieFont(div);
        deMagnifieFont(a);
    }
    function deMagnifieFont(p)
    {
        for(i=0;i<p.length;i++) {

            if(p[i].style.fontSize) {
                var s = parseInt(p[i].style.fontSize.replace("px",""));
            } else {

                var s = 12;
            }
            if(s!=min) {

                s -= 1;
            }
            p[i].style.fontSize = s+"px"

        }
    }

function submitform(){
   document.formID.submit();
}

$(document).ready(function(){
	$('.search_link').click(function(){
		$('.search_form_top').toggleClass('active_search');
	});
});

</script>
<?php
error_reporting(0);
		
 $eurl = explode('/',$_SERVER['REQUEST_URI']);
 //echo $eurl[2];
if(($eurl[2]=="publications") && end($eurl)!== "publications")
{
$eurl = "publications/".end($eurl);
}
else if(end($eurl)== "theory-and-implementation")
{
$eurl = "achievement";
}
else if(end($eurl)== "message-of-the-mosque")
{
$eurl = "grand-mosque-message";
}
else if(($eurl[2]=="poll-archive") && ($eurl[3]!="") )
{
$eurl = "poll-archive/".end($eurl);
}
else if(($eurl[2]=="poll-archive"))
{

$eurl = "poll-archive/";
}
else if(($eurl[2]=="news-detail") )
{
$eurl = "news-list";
}

else if(($eurl[2]=="news-list") && ($eurl[3]!="") )
{
$eurl = "news-list/".end($eurl);
}
else if(($eurl[2]=="news-list") )
{
$eurl = "news-list";
}
else if(($eurl[2]=="activities-detail") )
{
$eurl = "center-activities";
}
else if(($eurl[2]=="video-gallery") )
{
$eurl = "video-gallery";
}

else
{
 $eurl = end($eurl);
}

//echo $eurl[2];
ob_start();
 $main_menu = isset($main_menu) ? $main_menu : '' ?>
            <?php
	    date_default_timezone_set("Asia/Dubai");
            $dt=date("D, M j Y h:i a ");
            $todayDate = date("Y-m-d g:i a");// current date
            $currentTime = time($todayDate); //Change date into time
            $new_time = $currentTime;
            //echo $_SERVER['PHP_SELF'];
            //echo $dt;
            phpiu
            ?>
            <?php include 'includes/menus/hijri.php'; ?>
<!-- Header Start -->
<header>
	<h1><a href="<?php echo $site_path; ?>"><img src="<?php echo $site_path; ?>images/logo.png"></a></h1>
    	<div class="grid_1">
        <span class="title">Sheikh Zayed Grand Mosque Center</span>
            <ul>
            <li><a href="javascript:void(0);" style="text-decoration:none;cursor: initial;"><?php echo  date("D, j M Y",$new_time)?></a></li>
            <li><a href="javascript:void(0);"style="text-decoration:none;cursor: initial;"><?php  echo ($hijri[1]).' '.HijriCalendar::monthName($hijri[0]).' '.$hijri[2]; ?></a></li>
            <li><a href="javascript:void(0);" class="last" style="text-decoration:none;cursor: initial;"> Time: <?php echo date("h:i a",$new_time);?></a></li>
           </ul>
       </div>
    <div class="grid_2">
    <img src="<?php echo $site_path; ?>images/Top_Right_Logo.jpg">
    </div>
    <span class="pull-left mobile_view" id="menuclick"><div id="nav-icon3"> <span></span> <span></span> <span></span> <span></span> </div></span> 	
</header>
<!-- Header Start Close -->
<!-- Navigation Strart --><?php $actual_link = "$_SERVER[REQUEST_URI]"; ?>
<div class="social_box">
	<div class="sub_social_box">
    	<ul>
        	<li><a href="<?php echo $site_path; ?>" title="Home" class="home <?php if($eurl==''){ ?> active <?php } ?>"></a></li>
            <li><a href="<?php echo $site_path; ?>contact-us" title="Contact us" class="phone <?php if($eurl=='contact-us'){ ?> active <?php } ?>"></a></li>
            <li><a href="<?php echo $site_path; ?>sitemap" title="Sitemap" class="social_media <?php if($eurl=='sitemap'){ ?> active <?php } ?>"></a></li>
              <li><a title="Help" href="<?php echo $site_path; ?>help" class="qustion"></a></li>
            <li><a title="Decrease font" href="javascript:decreaseFontSize();" class="small_a"></a></li>
            <li><a title="Increase font" href="javascript:increaseFontSize();" class="cap_a"></a></li>
        </ul>
        
        <ul class="Right_Side">
            <a href="<?php echo rtrim($site_path, '/en/').''.str_replace("/en", "", $actual_link); ?>"><img src="<?php echo $site_path; ?>images/arbi_title.png" title="Switch Language" class="arbi_title" /></a>
            <li class="reesMr"><a href="http://feeds.feedburner.com/szgmc/DmyK" target="blank" title="RSS" id="rss" class="rss"></a></li>
            <li><a href="http://www.youtube.com/szgmc" target="blank" title="YouTube" id="youtube" class="youtube">&nbsp;</a></li>
            <li class="mobile_view"><img src="<?php echo $site_path; ?>img/mobile/search.png" class="search_link" /></li>
             <!--li class="reesMr"><a href="#" class="facebook"></a></li-->
            <form class="search_form_top"  method="post" action="<?php echo $site_path; ?>search.php" name="formID" id="formID">
              <span class="search_btn"><a href="#" onclick="javascript:submitform();"></a></span>
             <input type="text" class="validate[required]" id="search_text" onblur="if(this.value=='') { this.value='Search...'}" onfocus="if(this.value=='Search...') { this.value=''; }" value="Search..." name="key">
         </form>
        </ul>
        
    </div>
	
</div>
<!-- Navigation Close -->