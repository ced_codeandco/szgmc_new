<?php
if(isset($_SESSION["user"]))
{
$user=$_SESSION["user"];
}


$dir = new SplFileInfo(getcwd());

$stat=  date('d-m-Y H:i:s', $dir->getMTime());
$sql_stat = "select * from site_options where option_name='last_updated'";
$query_stat=mysql_query($sql_stat);
while($res_stat=mysql_fetch_object($query_stat))
{
	$db_stat = $res_stat->option_value;
}

if($db_stat>$stat)
{
	$stat = $db_stat;
}
else
{
	//$stat = $stat;
	$stat = $db_stat;
}
$updated = explode(" ", $stat);
$udate = time($updated[0]);
$utime = $updated[1];
$udate = date('M d Y',$udate);
?>
<div class="footer">
	<span class="pull-left mobile_view" id="menuclickfooter"><div id="nav-icon3"> <span></span> <span></span> <span></span> <span></span> </div></span>
	<div class="footer-address">
    <div class="footer-div">
    	<div class="footer-address-left"><img src="<?php echo $site_path; ?>images/footer-mosque-image.png" width="174" height="159" /></div>
        <div class="footer-address-middle">
        	<ul>
            <li class="telephone-background">Tel. No.: +971 2 4191919</li>
            <li class="writing-background">Feedback to <a href="mailto:info@szgmc.ae" class="bold">info@szgmc.ae</a> or <br />
our <a href="<?php echo $eng_site_url; ?>contact-us" class="underline">online form</a></li>
            </ul>
            
        </div>
        <div class="footer-address-right">
        <p>Page last updated on:  <?php echo $udate; ?> at <?php echo $utime; ?></p>
        <p>This site is best viewed in 1024*786 resolution<br />
Supports Microsoft Internet Explorer 7.0+, Firefox 1.0+, Safari 1.2+, Chrome</p>
        </div>
    </div>
    </div>
    <div class="footer-menu">
    <div class="footer-div menu">
        <a href="<?php echo $site_path; ?>accessibility">Accessibility</a><span>I</span>
        <a href="<?php echo $site_path; ?>privacy-policy">Privacy Policy</a>
        <span>I</span><a href="<?php echo $site_path; ?>info">Terms & Conditions</a>
        <span>I</span><a href="<?php echo $site_path; ?>disclaimer">Disclaimer</a>
        <span>I</span><a href="<?php echo $site_path; ?>copyrights">Copyrights</a>
        <span>I</span><a href="<?php echo $site_path; ?>sitemap">Sitemap</a>
        <span>I</span><a href="<?php echo $site_path; ?>help">Help</a>
        <span>I</span><a href="https://webmail.szgmc.ae/" taget="_blank" >Webmail</a>
        <span>I</span><a href="<?php echo $site_path; ?>contact-us">Contact Us</a>
    </div>
    </div>
    <div class="footer-copyright">
    <div class="footer-div copyright copyright_nomobile">Copyright © 2015 by Sheikh Zayed Grand Mosque Centre. All rights reserved.</div>
    <div class="footer-div copyright copyright_mobile">Copyright © 2015 by Sheikh Zayed Grand Mosque Centre.<br /> All rights reserved.</div>
    </div>
</div>

<script type="text/javascript" src="<?php echo $site_path; ?>js/jquery.nivo.slider.js"></script> 
 <script type="text/javascript">
    $(window).load(function() {
        $('#slider').nivoSlider();
    });
	
	
	$("#side_menu > li > div").click(function(){
     
    if(false == $(this).next().is(':visible')) {
        $('#side_menu ul').slideUp(300);
    }
    $(this).next().slideToggle(300);
});
    </script>
    <?php
	//print_r($hijri);
$db->closeDb();
?>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-22417140-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>