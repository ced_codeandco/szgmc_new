<?php
	function UTF8ToHTML($str)
    {
        $search = array();
        $search[] = "/([\\xC0-\\xF7]{1,1}[\\x80-\\xBF]+)/e";
        $search[] = "/&#228;/";
        $search[] = "/&#246;/";
        $search[] = "/&#252;/";
        $search[] = "/&#196;/";
        $search[] = "/&#214;/";
        $search[] = "/&#220;/";
        $search[] = "/&#223;/";

        $replace = array();
        $replace[] = 'Helper::_UTF8ToHTML("\\1")';
        $replace[] = "�";
        $replace[] = "�";
        $replace[] = "�";
        $replace[] = "�";
        $replace[] = "�";
        $replace[] = "�";
        $replace[] = "�";

        $str = preg_replace($search, $replace, $str);

        return $str;
    }
	
	function string_to_filename($word) {
       $tmp = preg_replace('/^\W+|\W+$/', '', $word); // remove all non-alphanumeric chars at begin & end of string
	   $tmp = preg_replace('/[^a-zA-Z0-9-]/', ' ', $tmp);	  
       $tmp = preg_replace('/\s+/', '_', $tmp); // compress internal whitespace and replace with _	   
       return strtolower($tmp);
	}
	
	function getWeather()
	{
		$weather_feed = file_get_contents("http://weather.yahooapis.com/forecastrss?w=1940330&u=c");
		try
		{
			if(!$weather_feed) die('weather failed, check feed URL');
			if($weather = @simplexml_load_string($weather_feed)) {
				$item_yweather = $weather->channel->item->children("http://xml.weather.yahoo.com/ns/rss/1.0");
				foreach($item_yweather as $x => $yw_item) {
					foreach($yw_item->attributes() as $k => $attr) {
						if($k == 'day') $day = $attr;
						if($x == 'forecast') { $yw_forecast[$x][$day . ''][$k] = $attr;	} 
						else { $yw_forecast[$x][$k] = $attr; }
					}
				}
				return array('temp' => $yw_forecast['condition']['temp'], 'condition' => strtolower($yw_forecast['condition']['code']));
			} else {
				return array();		
			}
		}
		catch(Exception $ex) {
			return array();	
		}
	}
	
	function getWeather_bk() {
		
		/*if($xml = @simplexml_load_file('http://www.google.com/ig/api?weather=abu%20dhabi')) {
			try {
				$information = $xml->xpath("/xml_api_reply/weather/forecast_information");
				$current = $xml->xpath("/xml_api_reply/weather/current_conditions");
				$forecast_list = $xml->xpath("/xml_api_reply/weather/forecast_conditions");
				$current_temp= round(($current[0]->temp_f['data']-32)*(5/9)) ;
				$icon_array = $current[0]->icon->attributes();
				$icon = $icon_array['data'];
				$icon = explode('/', $icon);
				$icon = rtrim(end($icon), '.gif');
				return array('temp' => $current_temp, 'condition' => $icon);
			} catch(Exception $ex) {
				return array();	
			}
		}
		return array();*/
		$weather_feed = file_get_contents("http://weather.yahooapis.com/forecastrss?w=1940330&u=c");
		try
		{
		if(!$weather_feed) die('weather failed, check feed URL');
	$weather = simplexml_load_string($weather_feed);
			
			$item_yweather = $weather->channel->item->children("http://xml.weather.yahoo.com/ns/rss/1.0");


			

foreach($item_yweather as $x => $yw_item) {
	foreach($yw_item->attributes() as $k => $attr) {
		if($k == 'day') $day = $attr;
		if($x == 'forecast') { $yw_forecast[$x][$day . ''][$k] = $attr;	} 
		else { $yw_forecast[$x][$k] = $attr; }
	}
}

		return array('temp' => $yw_forecast['condition']['temp'], 'condition' => strtolower($yw_forecast['condition']['code']));
		}
		catch(Exception $ex) {
				return array();	
			}

/*
// see the output!
echo '<pre>';
print_r($yw_forecast);
//echo($yw_forecast['condition']['temp']);
//var_dump($item_yweather[0]->attributes);
echo '</pre>';
				/*
				$information = $xml->xpath("/xml_api_reply/weather/forecast_information");
				$current = $xml->xpath("/xml_api_reply/weather/current_conditions");
				$forecast_list = $xml->xpath("/xml_api_reply/weather/forecast_conditions");
				$current_temp= round(($current[0]->temp_f['data']-32)*(5/9)) ;
				$icon_array = $current[0]->icon->attributes();
				$icon = $icon_array['data'];
				$icon = explode('/', $icon);
				$icon = rtrim(end($icon), '.gif');
				return array('temp' => $current_temp, 'condition' => $icon);\
				*/
			
		
		//return array();
		
		
		
		
		//return array('temp' => $current_temp, 'condition' => 'mostly_cloudy');
	
	}

    function getPrayerPublicApi() {
        $json = file_get_contents('http://www.szgmc.ae/API/PrayerTimes.php?l=EN');
        $obj = json_decode($json);

        return !empty($obj) ? (array) $obj : false;
    }
	
	function getPrayers(){

$url =
'http://www.islamicfinder.org/prayer_service.php?country=united_arab_emirates&city=abu_dhabi&state=01&zipcode=&latitude=24.4667&longitude=54.3667&timezone=4&HanfiShafi=1&pmethod=4&fajrTwilight1=10&fajrTwilight2=10&ishaTwilight=10&ishaInterval=30&dhuhrInterval=1&maghribInterval=1&dayLight=0&simpleFormat=xml';

$xmlDoc = new DOMDocument();
$xmlDoc->load($url);

$x = $xmlDoc->documentElement;

foreach ($x->childNodes AS $item)
  {
	$arr[$item->nodeName]=$item->nodeValue;
  }
 
 	//print_r($arr);
	//die();
  return $arr;

}

	function getPrayers_newapi(){
		//return array();
		$client = new SoapClient("http://www.awqaf.ae:8085/PrayerTimes.asmx?WSDL");
		$date = date("Y-m-d");
		$result = $client->GetPrayerTimesByCity(array('date' => $date, 'cityName' => 'Abu Dhabi'));
		$arr = (array)$result->GetPrayerTimesByCityResult->PrayerTimeStruct;
		$arr = array_map('filtertime', $arr);
		return $arr;
	}
	
	function filtertime($date) {
		return date('h:i a', strtotime($date));
	}
	/*	Added by irfan Ahmed on 4/12/13 to check the operater landline humber format
		this function is for temporary use.
	*/
	function isValidTelFormat($phoneNum){
			
		if($phoneNum[0] == '9' & $phoneNum[1] == '7' & $phoneNum[2] == '1' & $phoneNum[3] == '-' & $phoneNum[5] == '-'){
			
			return true;
		}
		return false;
	}

	

	
	
	

?>