<?php
class flickr {
 
    function flickr($username,$api_key) {
        $this->username = $username;
        $this->api_key  = $api_key;
        $this->user_id  = '';
        $this->response = array();
        $this->timeout  = 5;
    }
 
    function getUserId() {
        $url = 'https://api.flickr.com/services/rest/?api_key=' . urlencode($this->api_key) . '&method=flickr.urls.lookupUser&url=http://www.flickr.com/photos/' . urlencode($this->username) . '/';
        $return = $this->fetch($url);
        return $return->user->attributes()->id;
    }
 
    function getImages($count) {
        $url = 'https://api.flickr.com/services/rest/?api_key=' . urlencode($this->api_key) . '&method=flickr.photos.search&user_id=' . $this->getUserId() . '&per_page=' . urlencode($count);
        return $this->fetch($url);
    }
 
    function fetch($url,$post = false) {
        $ch = curl_init($url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_NOBODY, 0);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Flickr');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, $this->timeout);
        $output = curl_exec($ch);
        $this->response = curl_getinfo($ch);
        curl_close($ch);
        if((int)$this->response['http_code'] == 200) {
            return new SimpleXMLElement($output);
        }
        else {
            return false;
        }
    }
 
}

?>