<?php

class Configuration 
{
	private $title, $site_description, $site_keywords, $site_url, $arabic_site_url, $eng_site_url, $fb_url, $twitter_url;
	
	function __construct() 
	{

        $this->site_url = 'http://'.$_SERVER['HTTP_HOST'].'/en/'; // for cnh development
		$this->arabic_site_url = 'http://'.$_SERVER['HTTP_HOST'].'/';
		$this->eng_site_url = 'http://'.$_SERVER['HTTP_HOST'].'/en/';
		$this->fb_url = 'http://www.facebook.com/pages/Sheikh-Zayed-Grand-Mosque-Center/122788994440564';
		$this->twitter_url = 'http://twitter.com/szgmc'; //for development server
		$this->site_title = 'Sheikh Zayed Grand Mosque Center';
		$this->site_description = 'Sheikh Zayed Grand Mosque Center';
		$this->site_keywords = 'Sheikh Zayed Mosque, Abu Dhabi Mosque, Grand Mosque Abu Dhabi';
	}
	
	public function __get($property) 
	{
		if (property_exists($this, $property)) {
		  return $this->$property;
		}
	}
	
	public function __set($property, $value) 
	{
		if (property_exists($this, $property)) {
		  $this->$property = $value;
		}
		return $this;
	}
	
	public function getCurrentMainPage($slug) {
		$list = array(
				'about-szgmc' => 'szgmc',
				'minister-hh' => 'szgmc',
				'board-members' => 'szgmc',
				'chairmans-message' => 'szgmc',
				'DG-message' => 'szgmc',
				'organizational-structure' => 'szgmc',
				'vision-mission-values' => 'szgmc',
				'sheikh-zayed-and-grand-mosque' => 'szgmc',
				'theory-and-implementation' => 'szgmc',
				
				'mosque-opening-hours' => 'planvisit',
				'visitor-services' => 'planvisit',
				'getting-to-the-mosque' => 'planvisit',
				'questions' => 'planvisit',
				'e-guide' => 'planvisit',
				'esurvey' => 'planvisit',
				'what-is-tour' => 'planvisit',
				
				'press-kit' => 'mediacenter',
				'eparticipation' => 'mediacenter',
				
				'about-the-library' => 'library',
				'library-resources' => 'library',
				'search-library' => 'library',
				
				'exhibitions' => 'events',		
				'activities' => 'events',			
				'social-initiatives' => 'events',
				'spaces-of-light' => 'events',				
				
				
				'founder' => 'founder',
				'selected-sayings-by-sheikh-zayed-al-nahyan' => 'founder',
				'they-said-about-zayed' => 'founder',
				
				'achievement' => 'szgmc',
				'vision-dream-sheikh-zayed-mosque' => 'about',
				'grand-mosque-message' => 'about',
				'abudhabi' => 'about',
				
				'message-of-the-mosque' => 'about',
				'general-architecture' => 'archi',
				'domes' => 'archi',
				'marbles' => 'archi',
				'lunar-illumination' => 'archi',
				'carpets' => 'archi',
				'chandeliers' => 'archi',
				'minaret' => 'archi',
				'reflective-pools' => 'archi',
				'columns' => 'archi',
				'mihrab' => 'archi',
				'sahan' => 'archi',
				'pulpit' => 'archi'	);
		
		if(in_array($slug, array_keys($list))) {
			//die($list[$slug]);
			return $list[$slug];
		}
		else
			return '';
	}
	
	
	public function getMenuList($parent_id) {
		$menu_list = array();
		$sql = sprintf("SELECT ifnull(sub_menu.name, '') as sub_menu_name, sub_menu.url as sub_menu_url, main_menu.name as main_menu_name, main_menu.url as main_menu_url FROM `menu` as sub_menu right join `menu` as main_menu on main_menu.menu_id = sub_menu.parent_id and sub_menu.visible = 1 WHERE main_menu.visible = 1 and main_menu.parent_id = %d ORDER BY main_menu.show_order, sub_menu.show_order asc", $parent_id);
		if($menu_rs = mysql_query($sql)) {
			while($menu = mysql_fetch_object($menu_rs)) {
				if(!isset($menu_list[$menu->main_menu_name]))
					$menu_list[$menu->main_menu_name] = array(
								'name' => $menu->main_menu_name,
								'url' => $menu->main_menu_url,
								'sub_menus' => array()
								);	
				if($menu->sub_menu_name != '')
					$menu_list[$menu->main_menu_name]['sub_menus'][] = array(
									'name' => $menu->sub_menu_name, 
									'url' => $menu->sub_menu_url);
			}	
		}		
		/*echo '<pre>';
		print_r($menu_list);
		echo '</pre>';
		die();*/
		return $menu_list;
	}
	
	
	
}
