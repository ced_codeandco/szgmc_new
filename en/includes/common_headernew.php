<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<meta name="description" content="<?php echo $conf->site_description; ?>" />
<meta name="keywords" content="<?php echo $conf->site_keywords; ?>" />
<script type="text/javascript">
		var main_menu = '<?php echo (isset($main_menu) && $main_menu != '')? $main_menu : ''; ?>';
		var current_tab = '<?php echo (isset($slug) && $slug != '')? $slug : ''; ?>';
		var site_url = '<?php echo $site_path; ?>';
		//console.log(current_tab);
	</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="<?php echo $site_path; ?>images/szgmc_favicon.ico">
<link rel="stylesheet" href="<?php echo $site_path; ?>css/nivo-slider.css" type="text/css" media="screen" />
<link href="<?php echo $site_path; ?>css/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $site_path; ?>css/jquery.lightbox-0.5.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo $site_path; ?>css/colorbox/colorbox.css" rel="stylesheet" title="text/css"/>
<!--[if IE]>
    <style>
    .header .top_menu .contactus{width:114px;float:left;margin-top:22px;}
    .header .top_menu .contactus a{ background:url(<?php echo $site_path; ?><?php echo $site_path; ?>images/contactus.png) no-repeat; color:#000000; padding:10px 20px 10px 15px; text-decoration:none; font-weight:bold;height:36px;}
    .header .top_menu .contactus a:hover{ background:url(<?php echo $site_path; ?>images/contactus_hover.png) no-repeat; color:#fff; padding:10px 20px 10px 15px; text-decoration:none; font-weight:bold;}
    .header .top_menu .contactus .current{ background:url(<?php echo $site_path; ?>images/contactus_hover.png) no-repeat; color:#fff; padding:10px 20px 10px 15px; text-decoration:none; font-weight:bold;}
    </style>
    <![endif]--> 
	<script type="text/javascript" src="<?php echo $site_path; ?>js/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="<?php echo $site_path; ?>js/jquery.lightbox-0.5.min.js"></script>
    
    <script type="text/javascript" src="<?php echo $site_path; ?>js/jquery.nivo.slider.js"></script>
    <script type="text/javascript" src="<?php echo $site_path; ?>js/site.js"></script>
	<script src="<?php echo $site_path; ?>js/colorbox/jquery.colorbox.js"></script>
    <link rel="stylesheet" href="<?php echo $site_path; ?>css/responsive.css" />