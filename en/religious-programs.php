<?php 

session_start();

?>

<style>

	.input_row_dob #pub_date{

		width: 240px;

	}

</style>

<?php

include './includes/database.php';

include './includes/functions.php';

include './includes/config.php';

include_once('./includes/generic_functions.php');

$conf = new Configuration();

$db = new MyDatabase();

$site_path = $conf->site_url;

?>



<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

	<?php include 'includes/common_header.php'; ?>

    <title>Sheikh Zayed Grand Mosque Center+</title>

</head>

<body>

    

    <?php include 'includes/menus/banner_header.php'; ?>

    <!-- Banner start -->

    <div class="banner">

	<img src="<?php echo $site_path; ?>images/visiting_the_mosque_banner.jpg">     

    </div>   

    <!-- Banner Close -->

    

    <div class="main_box_content">

            <?php include 'includes/menus/nav_menu.php'; ?>

        <div class="clear"></div>

        <div class="content">

        	<div class="brad_cram">

            	<ul>

            	   <li><a href="<?php echo $site_path; ?>">Home</a></li>

                    <li><a href="#" class="active">Religious & Cultural Programs</a></li>

                </ul>

            </div>

      

      <?php /*?><div class="punctuation-div">

    <div class="punctuation-div-inner">Vice President of Gambia, H.E. Dr. Isatou Njie-Saidy visited the majestic Sheikh Zayed Grand Mosque in Abu Dhabi  •  Vice President of Gambia, H.E.</div>

    </div><?php */?>

    

      

      	<div class="inside_conter">

        <p>The Sheikh Zayed Grand Mosque holds several religious and cultural programs and activities throughout the year that target all social classes. SZGMC's Religious and Cultural Programs Department organizes a variety of events that characterize this great edifice, and that rendered it as a renowned destination for those seeking knowledge and culture. </p>

        <?php /*<p>To learn more about our events and programs, <a href="#">click here.</a></p>*/?>

        		<div class="Visiting_box">

                	<ul>

                    	<li class="fist_tham">

                            <a href="<?php echo $site_path; ?>religious-courses">

                                <img src="<?php echo $site_path; ?>images/course-image.jpg">

                                <span><img src="<?php echo $site_path; ?>images/Visiting-The -osque/courses-ico.png">Religious Courses</span>

                            </a>

                        </li>

                        

                        <!--li>

                            <a href="#">

                                <img src="<?php echo $site_path; ?>images/religious-lectures-image.jpg">

                                <span><img src="<?php echo $site_path; ?>images/Visiting-The -osque/religious-lectures-ico.png">Religious Lectures</span>

                            </a>

                        </li-->

                        

                        <li><a href="<?php echo $site_path; ?>zikr-al-hakeem"><img src="<?php echo $site_path; ?>images/zikr-al-hakeem-image.jpg">

                        <span><img src="<?php echo $site_path; ?>images/Visiting-The -osque/zikr-al-hakeem-ico.png">Al Thikr Al Hakeem</span></a></li>

                        

                   

                        <li>

                            <a href="<?php echo $site_path; ?>friday-sermon">

                                <img src="<?php echo $site_path;?>images/religious/friday-sermon.jpg">

                                <span class="icon-wrap"><img src="<?php echo $site_path;?>images/religious/friday-sermon.png"></span>

                                <span>

                                    Friday Sermon

                                </span>

                            </a>

                        </li>

                        <li class="fist_tham">

                            <a href="<?php echo $site_path; ?>ramadan-activities">

                                <img src="<?php echo $site_path;?>images/religious/ramadan-activities.jpg">

                                <span class="icon-wrap"><img src="<?php echo $site_path;?>images/religious/ramadan-activities.png"></span>

                                <span>

                                    Ramadan Activities

                                </span>

                            </a>

                        </li>

                        <?php /*<li>

                            <a href="<?php //echo $site_path.'prayer-information';?>">

                                <img src="<?php echo $site_path;?>images/religious/prayer-information.jpg">

                                <span class="icon-wrap"><img src="<?php echo $site_path;?>images/religious/prayer-information.png"></span><span>

                                    Prayer-Information

                                </span>

                            </a>

                        </li>*/?>



                    </ul>

                  

                </div>

                

                <div class="whatsapp-div">

                    <div class="textbox-label" style="margin-top: -47px;margin-left: 95px;">Add your mobile number in order to join our WhatsApp Service</div>

                    <div class="texbox-div+">

                        <input style="border: 1px solid;" type="text" value="Add your mobile number. Ex: +97150 123 4567" name="search" class="whatsapp-txtbox" onblur="if(this.value=='') { this.value='Add your mobile number. Ex: +97150 123 4567'}" onfocus="if(this.value=='Add your mobile number. Ex: +97150 123 4567') { this.value=''; }" />



                    </div>

                    <div style="float:left; margin:0 5px;">

                        <img src="captcha.php" id="captcha" style="background:#CCC;outline:none;

transition: all 0.25s ease-in-out;

-webkit-transition: all 0.25s ease-in-out;

-moz-transition: all 0.25s ease-in-out;

border: solid 2px #D7D9E5;

border-radius: 3px;

margin-left: 10px;margin-bottom: 5px; width: 95px;" />

                        <a class="refresh" href="javascript:void(0)" onclick="

								document.getElementById('captcha').src='captcha.php?'+Math.random();

								document.getElementById('captcha-form').focus();"

                           id="change-image"><img src="images/refresh.png" alt="" style="width: 20px;margin-top: 7px;float: right;" /></a>

                    </div>

                    <div>

                        <input type="text" class="whatsapp-txtbox001" >

                    </div>

                    <input type="submit" value="" class="whatsapp-btn" />

                </div>

        </div>

      

      

     

      

        </div>

   <div class="clear"></div>

 </div>

    

    

    

    

    

 

	<div class="content_bottom">&nbsp;</div>

	<?php include 'includes/footer.php'; ?> 

	<?php

	

	   if($msg == 'success'){

	  

		 ?>

		<script>

			$(function() {

				$( "#dialog" ).dialog({maxWidth:480,modal:true,width:'90%'});

			});

		</script>

        <script type="text/javascript">

		$(function(){

			$('#confirm').click(function() {

				//$('#dialog').hide();

				$( "#dialog" ).dialog( "close" );

				window.location = "/en/careers";

				return false;

			});

		});

		

        </script>

        <div id="dialog" class="lostpopup">

        	<p style="text-align:center;"><span dir="rtl">لقد تم تقديم طلبك بنجاح ، وسوف نقوم بالتواصل معك قريبا.</span></p>

            <p>Your request has been submitted successfully. We will contact you shortly.</p>

            <form name="feedback_frm" method="post" id="fbk_frm">

                <div>

                   <label for="comments">Kindly provide us your feedback on this e-service.</label>

                   <label for="comments" class="arabic">يرجى تقديم ملاحظاتك عن هذه الخدمة الإلكترونية.</label>

                   <textarea name="comments" cols="50" rows="4"></textarea>

                   <input type="hidden" name="path" value="careers"/>

				    <input type="hidden" name="tour_ref" value="<?php echo $ref;?>"/>

                </div>

                <div align="center" class="clear">

                  <input type="button" style="direction:ltr;" id="submitTour" name="submitTour" class="cmt_btn" value="Submit ﺍرسل" onClick="postFeedback();" />

                  <input type="button" style="direction:ltr;" id="confirm" class="cmt_btn" value="No Comments لا تعليق" />

                </div>

	   		</form>

        </div>

		 <?

		

		}

		else if($msg == 'failed')

		{

		?>

		  <script> alert("Cannot process your request"); </script>

		 <?

		  print('<script type="text/javascript">window.location = "/en/careers";</script>');

exit(); 

		}else if($msg == 'Invalid')

		{

		?>

			<script> alert("Invalid captcha"); </script>

		<?php

			print('<script type="text/javascript">window.location = "/en/carrers.php";</script>');

			exit(); 

		}

	  

	?>

</body>

</html>