<?php

session_start();

include './includes/database.php';

include './includes/functions.php';

include './includes/config.php';

include_once('./includes/generic_functions.php');

$conf = new Configuration();

$db = new MyDatabase();

$site_path = $conf->site_url;



$slug = explode('/',$_SERVER['REQUEST_URI']);

$slug = end($slug);

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <?php include 'includes/common_header.php'; ?>

    <title> أنشطة وفعاليات</title>

</head>

<body >



<?php include 'includes/menus/banner_header3.php'; ?>

<!-- Banner Start -->

<div class="banner">

    <img src="<?php echo $site_path;?>images/visiting_the_mosque_banner.jpg">

</div>

<!-- Banner Close -->

<!-- Content Start -->

<div class="main_box_content visiting_page_height">



    <?php include 'includes/menus/left_menu.php'; ?>

    <div class="clear"></div>

    <div class="content">

        <div class="brad_cram">

            <ul>

                <li><a href="<?php echo $site_path; ?>">الصفحة الرئيسية</a></li>

                <li><a href="<?php echo $site_path; ?>" class="active"> أنشطة وفعاليات </a></li>

            </ul>

        </div>




        <p style="margin:27px 0 17px 14px; text-align: justify;">
            يحرص مركز جامع الشيخ زايد الكبير على تعزيز قيم التسامح والحوار بين الثقافات المختلفة من خلال الأنشطة والفعاليات التي يحرص على تنظيمها أو المشاركة فيها، مما يسهم في تعزيز الدور الحضاري والثقافي للمركز، حيث يشارك سنوياً في العديد من المعارض المحلية المقامة في الدولة أو المعارض الدولية التي تقام في كافة أقطار العالم.
        </p>

        <p style="margin:27px 0 17px 14px; text-align: justify;">
            وتهدف هذه المشاركات إلى تعريف الجمهور بالخدمات التي يقدمها المركز للزوار، وإبراز الدور الثقافي الذي يقوم به، وذلك عبر كوكبة من المرشدين الثقافيين والموظفين الذي يمثلون المركز في هذه الفعاليات العالمية، كما تتضمن المشاركة عرض عدد من إصدارات المركز المتنوعة.
        </p>




        <div class="inside_conter">

            <div class="Visiting_box">

                <ul>

                    <li class="fist_tham">
                        <a href="<?php echo $site_path; ?>exhibitions">
                            <img src="<?php echo $site_path;?>images/events/exhibitions.jpg">
                            <span class="icon-wrap"><img src="<?php echo $site_path;?>images/events/exhibitions-icon.png"></span>
                            <span>
المعارض
                            </span>
                        </a>
                    </li>



                    <li>
                        <a href="<?php echo $site_path; ?>activities">
                            <img src="<?php echo $site_path;?>images/events/activities.jpg">
                            <span class="icon-wrap"><img src="<?php echo $site_path;?>images/events/activities-icon.png"></span>
                            <span>
                                الأنشطة
                            </span>
                        </a>
                    </li>



                    <li>
                        <a href="<?php echo $site_path; ?>social-initiatives">
                            <img src="<?php echo $site_path;?>images/events/social-initiatives.jpg">

                            <span class="icon-wrap"><img src="<?php echo $site_path;?>images/events/social-initiatives-icon.png"></span><span>
المبادرات المجتمعية
                            </span>
                        </a>
                    </li>

                </ul>

                <ul>

                    <li class="fist_tham">
                        <a href="<?php echo $site_path; ?>spaces-of-light">
                            <img src="<?php echo $site_path;?>images/events/spaces-of-light.jpg">
                            <span class="icon-wrap"><img src="<?php echo $site_path;?>images/events/spaces-of-light-icon.png"></span>
                            <span>
جائزة فضاءات من نور
                            </span>
                        </a>
                    </li>

                </ul>


            </div>





        </div>





        <br class="clear" />

    </div>



</div>

<!-- Content Close -->

<?php include 'includes/menus/marquees_partner.php';?>

<div class="content_bottom">&nbsp;</div>

<?php include 'includes/footer.php'; ?>



</body>

</html>