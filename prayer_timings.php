<?php
session_start();
include './includes/database.php';
include './includes/functions.php';
include './includes/config.php';
include_once('./includes/generic_functions.php');
$conf = new Configuration();
$db = new MyDatabase();
$site_path = $conf->site_url;

$slug = explode('/',$_SERVER['REQUEST_URI']);
$slug = end($slug);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <?php include 'includes/common_header.php'; ?>
    <title>زيارة المسجد</title>
</head>
<body >

<?php include 'includes/menus/banner_header3.php'; ?>
<!-- Banner Start -->
<div class="banner">
    <img src="<?php echo $site_path;?>images/visiting_the_mosque_banner.jpg">
</div>
<!-- Banner Close -->
<!-- Content Start -->
<div class="main_box_content visiting_page_height">
    <?php include 'includes/menus/left_menu.php'; ?>
    <div class="clear"></div>
    <div class="content">
        <div class="brad_cram">
            <ul>
                <li><a href="<?php echo $site_path; ?>">الصفحة الرئيسية</a></li>
                <li><a href="<?php echo $site_path; ?>" class="active">مواقيت الصلاة </a></li>
            </ul>
        </div>
<div class="content-left">

                <?php 
				include 'includes/menus/ministry_logos.php'; 
				?>
                 <?php include 'includes/menus/rightsidebanner.php';?>
			</div>
            <div class="content-right" style="margin-right:37px">
			<div class="single_middle">
            
        <!--<div class="inside_conter">-->
                <?php $prayerTimes = getPrayerPublicApi();?>
            <div class="content-middle-box-round-corner prayer-rnd ">
                <div class="prayer-timing-title-div-page">مواقيت الصلاة</div>
                <div class="prayer-timing-div-page abu"> مدينة أبوظبي </div>
                <div class="prayer-timing-div-page odd"><span class="time-title">الفجر</span> <span><?php echo $prayerTimes['Fajr'];?></span></div>
                <div id="prayertiming" style="display: none+;">
                    <div class="prayer-timing-div-page"><span class="time-title">الشروق</span> <span><?php echo $prayerTimes['Shurooq'];?></span></div>
                    <div class="prayer-timing-div-page odd"><span class="time-title">الظهر</span> <span><?php echo $prayerTimes['Zuhr'];?></span></div>
                    <div class="prayer-timing-div-page"><span class="time-title">العصر</span> <span><?php echo $prayerTimes['Asr'];?></span></div>
                    <div class="prayer-timing-div-page odd"><span class="time-title">المغرب</span> <span><?php echo $prayerTimes['Maghrib'];?></span></div>
                    <div class="prayer-timing-div-page"><span class="time-title">العشاء</span> <span><?php echo $prayerTimes['Isha'];?></span></div>

                </div>
                <!--<span  class="prayer_seemore"><a  id="flip"  href="javascript:void(0);">كل الصلوات&rlm;</a></span>
                <span  class="prayer_seeless"><a  id="flip1"  href="javascript:void(0);">الصلاة القادمة</a></span>-->
            </div>
            <div class="Visiting_box">

            </div>
        <!--</div>-->
 </div>
        </div>


        <br class="clear" />
    </div>

</div>
<!-- Content Close -->
<?php include 'includes/menus/marquees_partner.php';?>
<div class="content_bottom">&nbsp;</div>
<?php include 'includes/footer.php'; ?>

</body>
</html>