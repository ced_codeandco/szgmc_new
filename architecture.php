<?php
session_start();
include './includes/database.php';
include './includes/functions.php';
include './includes/config.php';
include_once('./includes/generic_functions.php');
$conf = new Configuration();
$db = new MyDatabase();
$site_path = $conf->site_url;

$slug = explode('/',$_SERVER['REQUEST_URI']);
$slug = end($slug);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<?php include 'includes/common_header.php'; ?>
    <title>زيارة المسجد</title>
</head>
<body >

<?php include 'includes/menus/banner_header3.php'; ?>		
			<!-- Banner Start -->
<div class="banner">
	<img src="<?php echo $site_path;?>images/visiting_the_mosque_banner.jpg">     
</div>
<!-- Banner Close -->	
<!-- Content Start -->
	<div class="main_box_content visiting_page_height">
	 	 <?php include 'includes/menus/left_menu.php'; ?>
        <div class="clear"></div>
        <div class="content">
        	<div class="brad_cram">
            	<ul>
            	   <li><a href="<?php echo $site_path; ?>">الصفحة الرئيسية</a></li>
                    <li><a href="<?php echo $site_path; ?>" class="active">الجماليات والعمارة</a></li>
                </ul>
            </div>
      
      		<div class="inside_conter">
        		<div class="Visiting_box">
                    <ul>
                        <li class="fist_tham">
                            <a href="<?php echo $site_path; ?>general-architecture">
                                <img src="<?php echo $site_path;?>images/architecture/general-architecture.jpg">
                                <span><img src="<?php echo $site_path;?>images/architecture/general-architecture.png"> نظرة عامة</span>
                            </a>
                        </li>

                        <li>
                            <a href="<?php echo $site_path; ?>domes">
                                <img src="<?php echo $site_path;?>images/architecture/Domes1.jpg">
                                <span><img src="<?php echo $site_path;?>images/architecture/dome.png">&#1575;&#1604;&#1602;&#1576;&#1575;&#1576;</span>
                            </a>
                        </li>

                        <li>
                            <a href="<?php echo $site_path; ?>marbles">
                                <img src="<?php echo $site_path;?>images/architecture/marble1.jpg">
                                <span><img src="<?php echo $site_path;?>images/architecture/marble.png"> &#1575;&#1604;&#1585;&#1582;&#1575;&#1605; </span>
                            </a>
                        </li>

                    
                        <li class="fist_tham">
                            <a href="<?php echo $site_path; ?>lunar-illumination">
                                <img src="<?php echo $site_path;?>images/architecture/lunar-illumination.jpg">
                                <span><img src="<?php echo $site_path;?>images/architecture/lunar.png">&#1575;&#1604;&#1573;&#1590;&#1575;&#1569;&#1577; &#1575;&#1604;&#1602;&#1605;&#1585;&#1610;&#1577;</span>
                            </a>
                        </li>

                        <li>
                            <a href="<?php echo $site_path; ?>carpets">
                                <img src="<?php echo $site_path;?>images/architecture/carpet.jpg">
                                <span><img src="<?php echo $site_path;?>images/architecture/carpet.png">&#1575;&#1604;&#1587;&#1580;&#1575;&#1583;&#1577;</span>
                            </a>
                        </li>

                        <li>
                            <a href="<?php echo $site_path; ?>chandeliers">
                                <img src="<?php echo $site_path;?>images/architecture/chandellier.jpg">
                                <span><img src="<?php echo $site_path;?>images/architecture/chandellier.png">&#1575;&#1604;&#1579;&#1585;&#1610;&#1617;&#1575;&#1578;</span>
                            </a>
                        </li>

                    
                        <li class="fist_tham">
                            <a href="<?php echo $site_path; ?>pulpit">
                                <img src="<?php echo $site_path;?>images/architecture/pulpit.jpg">
                                <span><img src="<?php echo $site_path;?>images/architecture/pulpit.png">&#1575;&#1604;&#1605;&#1606;&#1576;&#1585;</span>
                            </a>
                        </li>

                        <li>
                            <a href="<?php echo $site_path; ?>minaret">
                                <img src="<?php echo $site_path;?>images/architecture/minaret.jpg">
                                <span><img src="<?php echo $site_path;?>images/architecture/minaret.png">المنارة   </span>
                            </a>
                        </li>

                        <li>
                            <a href="<?php echo $site_path; ?>reflective-pools">
                                <img src="<?php echo $site_path;?>images/architecture/reflective-pools.jpg">
                                <span><img src="<?php echo $site_path;?>images/architecture/reflective-pools.png">الأحواض العاكسة</span>
                            </a>
                        </li>


                    
                    	<li class="fist_tham">
                            <a href="<?php echo $site_path; ?>columns">
                                <img src="<?php echo $site_path;?>images/architecture/columns.jpg">
                                <span><img src="<?php echo $site_path;?>images/architecture/columns.png">الأعمدة   </span>
                            </a>
                        </li>

                        <li>
                            <a href="<?php echo $site_path; ?>mihrab">
                                <img src="<?php echo $site_path;?>images/architecture/mihrab.jpg">
                                <span><img src="<?php echo $site_path;?>images/architecture/mihrab.png">المحراب  </span>
                            </a>
                        </li>

                        <li>
                            <a href="<?php echo $site_path; ?>sahan">
                                <img src="<?php echo $site_path;?>images/architecture/sahan.jpg">
                                <span><img src="<?php echo $site_path;?>images/architecture/sahan.png">الصحن</span>
                            </a>
                        </li>
                    </ul>

                </div>
        </div>
	  		
	  
            <br class="clear" />      
        </div>
    
    </div>
<!-- Content Close -->
	<?php include 'includes/menus/marquees_partner.php';?>
	<div class="content_bottom">&nbsp;</div>
	<?php include 'includes/footer.php'; ?> 

</body>
</html>