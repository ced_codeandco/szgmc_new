<?php

ini_set('display_errors', 1);
error_reporting(0);

include 'includes/database.php';
include 'includes/functions.php';
include 'includes/config.php';
/*include 'includes/youtube.php';
$yt = new YoutubeAPI();*/

$slug = explode('/',$_SERVER['REQUEST_URI']);
//$slug = end($slug);
$slug = "video-gallery";
$conf = new Configuration();
$db = new MyDatabase();

$site_path = $conf->site_url;

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Video Gallery :: Sheikh Zayed Grand Mosque</title>
<?php include 'includes/common_header.php'; ?>
    <style type="text/css">
		.video_item {
			float:right;
			width: 151px;
			margin: 7px 3px 7px 7px;
		}
		.video_item img{width:145px;}
		.video_item p {
			color: #BC8545;
		}
		.video_top{background: url(/images/video_frame_top.jpg) no-repeat; width:697px; height:23px;}
		.video_bottom{background: url(/images/video_frame_bottom.jpg) left bottom no-repeat; width:697px; height:23px;}
		#list_videos {
			width: 622px;
			padding: 15px 38px 7px;
			background: url(/images/video_frame_centre.jpg) repeat-y;
		}
		#vidsCont{
			position:relative;
			margin: 15px 0 0;
			width: 631px;
		}
		#carousel .mask {
			direction: ltr;
			width:622px;
			position:relative;
		    overflow:hidden
		}
		.carousel ul {

			left:0;
		    overflow:hidden;
		    margin:0;
		    padding:0;
		    list-style:none;
		}
		.carousel ul li {
			float: left;
			width: 306px;
			height: 200px;
			color: #fff;
			font-size: 8em;
			text-align: center;
			margin: 0 0px 8px 8px;
		}
		.carousel>a{
			position: absolute;
			top: 40%;
			z-index:99999;
			width: 22px;
			height: 21px;
			cursor: pointer;
			text-indent:-9999px
		}
		.carousel>a.disabled{
			-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=40)";
			filter: alpha(opacity=40);
			-moz-opacity:0.4;
			-khtml-opacity: 0.4;
			opacity: 0.4;
		}
		.carousel .prev{
			left: -11px;
			background: transparent url(/css/photo_gallery/ad_scroll_back.png) 0 0;
		}
		.carousel .next{
			right: -11px;
			background: transparent url(/css/photo_gallery/ad_scroll_forward.png) 0 0;
		}
		.carousel li a{
			display: block;
			height: 100%;
			text-decoration: none;
		}
		.carousel li a>span{
			display: block;
			margin-top: 4px;
			color: #BC8545;
			font-size: 11px;
		}
	.register_box2 {
			padding: 21px 0px 0px 46px;
			margin-right: 208px;
			background: url("/en/images/arabic_bt.png") no-repeat;
			border: 0;
			height: 45px;
			width: 158px;
		}
		.register_box2 span{

			margin-right: 88px;
		}
	</style>
	<!--[if lte IE 7]>
<style>
.register_box2 {

			margin-left: 226px;
		}
		.register_box2 span{

			margin-right: -99px;
		}
</style>
<![endif]-->
    <link href="<?php echo $site_path;?>/css/video-gallery.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $site_path;?>/en/css/skin.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo $site_path;?>/en/js/jquery.carousel.js"></script>
<!--<script type="text/javascript" src="<?php /*echo $site_path;*/?>/en/js/swfobject.js"></script>-->
</head>

<body>
<?php include 'includes/menus/banner_header3.php'; ?>		
			<!-- Banner Start -->
<div class="banner">
	<img src="<?php echo $site_path;?>images/visiting_the_mosque_banner.jpg">     
</div>
<!-- Banner Close -->	
<!-- Content Start -->
	<div class="main_box_content">
	 	 <?php include 'includes/menus/left_menu.php'; ?>
        <div class="clear"></div>
        <div class="content">
        	<div class="brad_cram">
            	<ul>
            	   <li><a href="<?php echo $site_path; ?>">الصفحة الرئيسية</a></li>
                   <!--li><a href="<?php echo $site_path; ?>visiting-the-mosque">زيارة الجامع</a></li-->
                    <li><a href="#" class="active">مكتبة الفيديو</a></li>
                </ul>
            </div>
      
      		<div class="content-left">
				
                <br class="clear"/>
                <?php 
				include 'includes/menus/ministry_logos.php'; 
				?>
			</div>
	  		<div class="content-right" style="">
			    <div class="single_middle" style="text-align:right">
                <?php
                include 'includes/menus/marquee.php';
                ?>
			    <br class="clear" />
			
			    <h2> مكتبة الفيديو </h2>
			    <div class="video_top">&nbsp;</div>
                <div id="list_videos">
                        <div id="single_video" style="margin:0 auto;">
                            <div class="gallery_video">
                                <div class="player-wrap">
                                    <div id="player" style="width:100%; overflow:hidden;"></div>
                                </div>
                                <div id="vidsCont" class="carousel module">
                                    <ul>
                                    </ul>
                                    <div class="clear"></div>
                                    <div class="more_videos"><a href="http://www.youtube.com/user/szgmc/videos" style="font-size: 17px;color: black;text-decoration: none;" target="_blank">مزيد </a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                <div class="video_bottom">&nbsp;</div>
                <br class="clear" />
		        </div>
			</div>
            <br class="clear" />
        </div>
    
    </div>

<div class="content_bottom">&nbsp;</div>

<?php require 'includes/footer.php'; ?>
<script type="text/javascript">
    $(document).ready(function() {
        $.ajax({
            dataType: "jsonp",
            url: "https://www.googleapis.com/youtube/v3/channels?part=contentDetails&forUsername=szgmc&key=AIzaSyBMfsb-MkvJSxcdgJr-LJT2KlJv4w_jjfk",
            success: function(data, textStatus, jqXHR) {
                var playlistId = data.items[0].contentDetails.relatedPlaylists.uploads;
                //console.log(playlistId);
                loadThumbNails(playlistId);
                return;
            }
        });
    });
    function loadThumbNails(playlistId){
        $.ajax({
            dataType: "jsonp",
            url: "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&playlistId="+playlistId+"&key=AIzaSyBMfsb-MkvJSxcdgJr-LJT2KlJv4w_jjfk&maxResults=50",
            success: function(data, textStatus, jqXHR) {
                //console.log(data.items);
                //console.log(data.pageInfo.totalResults);

                //pages = data.feed.openSearch$totalResults.$t;
                var xi=false;
                var counter = 0;

                $.each(data.items, function(i, e) {
                    var snippet = e.snippet;
                    var videoId = snippet.resourceId.videoId;
                    $('#vidsCont ul').append('<li><a class="videoLinks" href="<?php echo $site_path; ?>video-gallery?video_id='+videoId+'" title="'+snippet.title+'"><img class="play-btn" src="/images/play.png" /><img src="'+snippet.thumbnails.medium.url+'" width="270" height="162" alt="'+snippet.title+'"/><span>'+snippet.title+'</span></a></li>');
                    if( !xi ){
                        video_first=videoId;
                        console.log(video_first);
                        var params = { allowScriptAccess: "always" };
                        var atts = { id: "myytplayer" };
                        <?php
                        if(isset($_REQUEST['video_id']) && $_REQUEST['video_id']!='')
                        echo "video_first='$_REQUEST[video_id]';";
                        ?>
                        //swfobject.embedSWF("http://www.youtube.com/e/"+video_first+"?enablejsapi=1&playerapiid=ytplayer", "player", "100%", "400", "8", null, null, params, atts);
                        var youtube_link = '<iframe width="100%" src="http://www.youtube.com/embed/' +video_first+ '" frameborder="0" allowfullscreen></iframe>';
                        $('#player').html(youtube_link);
                        xi=true;
                    }
                    counter++;
                    if(counter == 15){
                        //return false;
                    }
                });
            }
        });
    }
</script>
</body>
</html>
