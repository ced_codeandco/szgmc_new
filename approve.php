<?php
include './en/includes/database.php';
include 'includes/dal/news.php';
include 'includes/functions.php';
include 'includes/config.php';

$id = $_REQUEST['id'];
$conf = new Configuration();
$db = new MyDatabase();


$site_path = $conf->site_url;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sheikh Zayed Grand Mosque in Abu Dhabi</title>
<?php include 'includes/common_header.php'; ?>
</head>
<body>
<?php include 'includes/menus/banner_header3.php'; ?>		
			<!-- Banner Start -->
<div class="banner">
	<img src="<?php echo $site_path;?>images/visiting_the_mosque_banner.jpg">     
</div>
<!-- Banner Close -->	
<!-- Content Start -->
	<div class="main_box_content visiting_page_height">
	 <div id="dialognew" style="display:none;">
        	<p style="text-align:right;">لقد تم تقديم طلبكم بنجاح ،</p>
            <p><span dir="ltr">Your request has been submitted successfully</span></p>
            <p style=""><input type="button" value="OK" style="float:right;width:40px" id="confirmnew"></p>
        </div>
		 <?php include 'includes/menus/left_menu.php'; ?>
        <div class="clear"></div>
        <div class="content">
        	<div class="brad_cram">
            	<ul>
            	   <li><a href="<?php echo $site_path; ?>">الصفحة الرئيسية</a></li>
                    <li><a href="#" class="active">دليل الثقافة</a></li>
                </ul>
            </div>
      
      		<div class="content-left">
			 <?php //include 'includes/menus/left_menu1.php'; 
				?>
				
                <br class="clear"/>
                <?php 
				include 'includes/menus/ministry_logos.php'; 
				?>
                <?php include 'includes/menus/rightsidebanner.php';?>
			</div>
	  		<div class="content-right" style="margin-right:30px">
			<div class="single_middle" >
                <br class="clear" />
                   <h2>دليل الثقافة</h2>
<div class="news_item_single">
                    	<?php
				if(isset($_REQUEST["status"]))
				{
				$status = $_REQUEST["status"];
				$tour_id = $_REQUEST["tour_id"];
				$g_id = $_REQUEST["guid_id"];
				$sql = "select * from tours where id=$tour_id and guide_id=$g_id";
				$query = mysql_query($sql);
				if($res=mysql_fetch_object($query))
				{
				$cstatus = $res->capprove;
				}
				/*if($cstatus=="Mail sent")
				{*/
				if($status=="yes")
				{
				 $update_sql = "update tours set capprove='Confirmed' where id=$tour_id and guide_id=$g_id";
                 mysql_query($update_sql);
				 $emsg="Thank you for the confirmation";
				$amsg = "&#1606;&#1588;&#1603;&#1585;&#1603; &#1593;&#1604;&#1609; &#1578;&#1571;&#1603;&#1610;&#1583; &#1575;&#1604;&#1581;&#1580;&#1586;";
				}
				else
				{
				$update_sql = "update tours set capprove='Declined' where id=$tour_id and guide_id=$g_id";
                 mysql_query($update_sql);
				 $emsg="You have declined the tour guide request";
				$amsg = "&#1604;&#1602;&#1583; &#1602;&#1605;&#1578; &#1576;&#1585;&#1601;&#1590; &#1591;&#1604;&#1576; &#1575;&#1604;&#1573;&#1585;&#1588;&#1575;&#1583;";
				} 
				
				}
				/*else if($cstatus=="Confirmed")
				{
				$emsg="You have already confirmed this tour request";
				$amsg = "&#1606;&#1588;&#1603;&#1585;&#1603; &#1593;&#1604;&#1609; &#1578;&#1571;&#1603;&#1610;&#1583; &#1575;&#1604;&#1581;&#1580;&#1586;";
				}
				else if($cstatus=="Not Confirmed")
				{
				$emsg="You have already declined this tour request";
				$amsg = "&#1606;&#1588;&#1603;&#1585;&#1603; &#1593;&#1604;&#1609; &#1578;&#1571;&#1603;&#1610;&#1583; &#1575;&#1604;&#1581;&#1580;&#1586;";
				}*/
				//}
				?>
				<p>&nbsp;</p>
				<div style="width:350px; float:left;">
				<h3><?php echo $emsg; ?></h3> </div>
				<div style="width:350px; float:right;">
				<h3><?php echo $amsg; ?></h3>
</div>
                    </div> 
                    <div class="clear bottom_line"> &nbsp </div>
     </div>
			</div>
 <br class="clear" />      
        </div>
    
    </div>


<div class="content_bottom">&nbsp;</div>
<?php require 'includes/footer.php'; ?>
</body>
</html>
