<?php
include 'includes/database.php';
include 'includes/functions.php';
include 'includes/config.php';

$slug = explode('/',$_SERVER['REQUEST_URI']);
$slug = @mysql_real_escape_string(end($slug));
$conf = new Configuration();
$db = new MyDatabase();

$site_path = $conf->site_url;
$slug = explode('/',$_SERVER['REQUEST_URI']);
$slug = end($slug);
$main_menu = $conf->getCurrentMainPage($slug);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sheikh Zayed Grand Mosque in Abu Dhabi</title>
<?php include 'includes/common_header.php'; ?>
</head>

<body>
<?php include 'includes/menus/banner_header3.php'; ?>		
			<!-- Banner Start -->
<div class="banner">
	<img src="<?php echo $site_path;?>images/visiting_the_mosque_banner.jpg">     
</div>
<!-- Banner Close -->	
<!-- Content Start -->
	<div class="main_box_content">
	 	 <?php include 'includes/menus/left_menu.php'; ?>
        <div class="clear"></div>
        <div class="content">
        	<div class="brad_cram">
            	<ul>
            	   <li><a href="<?php echo $site_path; ?>">الصفحة الرئيسية</a></li>
                   <li><a href="<?php echo $site_path; ?>about-szgmc">عن المركز</a></li>
                    <li><a href="#" class="active">الهيكل التنظيمي</a></li>
                </ul>
            </div>
      
      		<div class="content-left">

                <?php 
				include 'includes/menus/ministry_logos.php'; 
				?>
                <?php include 'includes/menus/rightsidebanner.php';?>
			</div>
	  		<div class="content-right" style="margin-right:30px">
			<div class="single_middle">
		        <h2>الهيكل التنظيمي</h2>
				<div class="orgnization" style="margin: 0 auto 0 -50px; float:left;">
                <img src="<?php echo $site_path;?>images/Hierarchy-ar-2016.png"/> </div>
                <!--div class="orgnization">
                <img src="<?php echo $site_path;?>images/Hierarchy-ar-02.png"/> </div-->
     </div>			
			</div>
 <br class="clear" />      
        </div>
    
    </div>

<div class="content_bottom">&nbsp;</div>
<?php require 'includes/footer.php'; ?>
</body>
</html>
