<?php
include './includes/database.php';
include './includes/functions.php';
include './includes/config.php';
include_once('./includes/generic_functions.php');
$conf = new Configuration();
$db = new MyDatabase();
$site_path = $conf->site_url;

$slug = explode('/',$_SERVER['REQUEST_URI']);
$slug = end($slug);

$current_page = 'ftplogin';
$current_page = isset($_GET['current_page'])?mysql_real_escape_string($_GET['current_page']):1;


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>مركز الشيخ زايد المسجد الكبير</title>
<?php include 'includes/common_header.php'; ?>
</head>
<body>

<?php include 'includes/menus/banner_header3.php'; ?>		
			<!-- Banner Start -->
<div class="banner">
	<img src="<?php echo $site_path;?>images/visiting_the_mosque_banner.jpg">     
</div>
<!-- Banner Close -->	
<!-- Content Start -->
	<div class="main_box_content visiting_page_height">
	 	 <?php include 'includes/menus/left_menu.php'; ?>
        <div class="clear"></div>
        <div class="content">
        	<div class="brad_cram">
            	<ul>
            	   <li><a href="<?php echo $site_path; ?>">الصفحة الرئيسية</a></li>
                    <li><a href="#" class="active">مركز الشيخ زايد المسجد الكبير</a></li>
                </ul>
            </div>
      
      		<div class="content-left">
			 <?php //include 'includes/menus/left_menu1.php'; 
				?>
				
                <br class="clear"/>
                <?php 
				include 'includes/menus/ministry_logos.php'; 
				?>
                <?php include 'includes/menus/rightsidebanner.php';?>
			</div>
	  		<div class="content-right" style="margin-right:30px">
			<div class="single_middle">
            	<div class="page_item_single">
                    	<div class="page_content" >
                            <div class="general_body_content">
                            <br class="clear" />
              <p class="heading">About the Sheikh Zayed Grand Mosque Center</p>
              <p>
	<img alt="Ministry Officials" src="/images/inside_pages/inside_mosque.jpg" style="padding: 5px; width: 198px; height: 147px; float: right; " /></p>
              <p>The Sheikh Zayed Grand Mosque Center (which will be subsequently referred to as the SZGMC) was established by decree number 18 issued by HH Minister of Presidential Affairs in 2008.&nbsp; According to the decree, the SZGMC aims to:</p>
              
              <ol >
                <li style="margin-bottom: 10px; font-size: 14px;">Emphasize on the late Sheikh Zayed bin Sultan Al Nahyan&rsquo;s noble deeds and contributions to humanity, in a way that commemorates his legacy and memory.</li>
                <li style="margin-bottom: 10px; font-size: 14px;">Effectively operating and managing the SZGMC according to best practice criteria.</li>
                <li style="margin-bottom: 10px; font-size: 14px;">Conduct educational programs and cultural activities that reflect the true essence and meaning of the Islamic faith which promotes freedom, justice, equality, understanding while simultaneously embracing diversity and progress.</li>
                <li style="margin-bottom: 10px; font-size: 14px;">Cooperating with research centers as well as religious, educational and cultural institutions in the light of the SZGMC&rsquo;s designated objectives.&nbsp;&nbsp; In accordance with the founding decree, this will include:
                <ul  style="margin-top: 10px; color:#6D6D6D; font-size: 12px;">
                <li>Participating in academic conferences and scientific panels which achieve the SZGMC&rsquo;s objectives.</li>
                <li>Supporting social, religious and cultural initiatives aiming to promote mutual respect and understanding between different religions.</li>
                <li>Initiating cultural partnerships with religious institutions which share the SZGMC&rsquo;s objectives both inside the UAE and abroad.</li>
                <li>Establishing exhibitions to commemorate the contributions of the late Sheikh Zayed bin Sultan Al Nahyan and highlight his historical and civic achievements.</li>
                <li>Establish an exhibition that celebrates the social and cultural achievements of the late Sheikh Zayed bin Sultan Al Nahyan.</li>
                <li>Following up the SZGMC&rsquo;s daily business as well as preserving its artifacts and maintaining its grounds, features and landmarks.</li>
                <li>Organizing frequent conferences with topics related to the objectives of the SZGMC.</li>
                <li>Launching &ldquo;Sheikh Zayed Grand Mosque Center Award&rdquo; for projects that promote tolerance and religious co-existence and respect.&nbsp;&nbsp;</li>
                <li>Founding a fully equipped state-of the-art library with documents, manuscripts and reference books.</li>
                <li>Supporting the printing and translation of the Holy Quran in addition to the printing of books, research works, magazines, manuals and providing the library with audio-visual publications in addition to the translation of books pertaining to the SZGMC&rsquo;s objectives. Organizing lectures, seminars, forums and sessions for the memorization and recitation of the Holy Quran. Organizing seminars on Islamic architecture, Arabic calligraphy and&nbsp; Arabic syntax as well as a competition on Quran recitation and Adhan (Call to prayer).</li>
                <li>Promoting Academic field trips to the Mosque by schools institutions and universities and liaising with government departments and institutions. Utilizing media, including the internet, to achieve the objectives of the SZGMC. &nbsp;</li>
              </ul>
                
                </li>
              </ol>
              
              </div>
              <br class="clear" />
                            
                        </div>
                    </div> 
              <div class="clear bottom_line"> &nbsp; </div>
              <br class="clear" />
                
          </div>
			</div>
 <br class="clear" />      
        </div>
    
    </div>

<div class="content_bottom">&nbsp;</div>
<?php include 'includes/footer.php'; ?>
</body>
</html>
