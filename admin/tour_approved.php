﻿<?php
include_once ('includes/commons.php');

function getContentByFieldFromTable($table, $field, $value){
    $mydb=new connect;
    $sql="SELECT * FROM $table where $field='$value'";
    $rs=$mydb->query($sql);
    $r_obj=$mydb->loadResult($rs);
    return $r_obj;
}
?>
<script type="text/javascript">
			//<![CDATA[
				CKEDITOR.replace( 'approved',
					{
					fullPage : true					
					});

				CKEDITOR.config.protectedSource.push( /<\?[\s\S]*?\?>/g );  //PHP code
//CKEDITOR.config.toolbar = 'Full';
CKEDITOR.config.toolbar_Full =
[
	['Source','-','NewPage','Preview','-','Templates'],
    ['Cut','Copy','Paste','PasteText','PasteFromWord','-','Print', 'SpellChecker', 'Scayt'],
    ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
    ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField'],
    '/',
    ['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
    ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote','CreateDiv'],
    ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
    ['Link','Unlink','Anchor'],
    ['Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak'],
    '/',
    ['Styles','Format','Font','FontSize'],
    ['TextColor','BGColor'],
    ['Maximize', 'ShowBlocks','-','About']
];

				
			//]]>
			</script>
<?php if($_SESSION['view_type'] == 'ge'){ ?>
    <script>
        $(document).ready(function(){
            $('.details_form').submit(function(){
                if($('.comments').val() == ''){
                    alert('Please fill the comments field');
                    $('.comments').focus();
                    return false;
                }
            });
        });
    </script>
<?php } ?>
<script type="text/javascript" src="js/common.js"></script>
<?php

if(isset($_REQUEST['request_id']) ){
    $requestId = $_REQUEST['request_id'];
    $resultObj = getContentByFieldFromTable("tours", "id", $requestId);
    $resultObj = $resultObj[0];
 $needs=$resultObj->needs;
$needs=str_replace(array("\r\n", "\r", "\n", "\t"),'<br>',$needs);
//    $resultCrew = getContentByFieldFromTable("table_crew", "filming_permissions_id", $requestId);
if($resultObj->group_cat=="Governmentarb")
   {
   $cat = "&#1575;&#1604;&#1607;&#1610;&#1574;&#1575;&#1578; &#1575;&#1604;&#1581;&#1603;&#1608;&#1605;&#1610;&#1577;";
   }else if ($resultObj->group_cat=="Embassyarb")
   {
   $cat = "&#1575;&#1604;&#1587;&#1601;&#1575;&#1585;&#1575;&#1578;";
   }
   else if ($resultObj->group_cat=="Travel Industryarb")
   {
   $cat = "&#1602;&#1591;&#1575;&#1593; &#1575;&#1604;&#1587;&#1610;&#1575;&#1581;&#1577;";
   }
   else if ($resultObj->group_cat=="Educationarb")
   {
   $cat = "&#1575;&#1604;&#1605;&#1572;&#1587;&#1587;&#1575;&#1578; &#1575;&#1604;&#1578;&#1593;&#1604;&#1610;&#1605;&#1610;&#1577;";
   }
   else if ($resultObj->group_cat=="Otherarb")
   {
   $cat = "&#1571;&#1582;&#1585;&#1609;";
   }
    else if ($resultObj->group_cat=="Genaral Publicarb")
   {
   $cat = "&#1586;&#1610;&#1575;&#1585;&#1577; &#1593;&#1575;&#1605;&#1607;";
   }
   else if ($resultObj->group_cat=="Corporatearb")
   {
   $cat = "&#1575;&#1604;&#1602;&#1591;&#1575;&#1593; &#1575;&#1604;&#1582;&#1575;&#1589;";
   }
    else if ($resultObj->group_cat=="Tour Operatorarb")
   {
   $cat = "&#1575;&#1604;&#1588;&#1585;&#1603;&#1575;&#1578; &#1575;&#1604;&#1587;&#1610;&#1575;&#1581;&#1610;&#1577;";
     }
	  else if ($resultObj->group_cat=="Hotelarb")
   {
   $cat = "الفنادق";
     }
   else
   {
   $cat=$resultObj->group_cat;
   }
    if($resultObj->country=="United Arab Emiratesarb")
   {
   $country = "الأمارات العربية المتحدة";
   }
   else
   {
   $country = $resultObj->country;
   }
   $date_visit = explode("-",$resultObj->purposed_date);
		$date_visit1 = $date_visit[2] . "/".$date_visit[1] . "/".$date_visit[0];
 /*This view is for the admin*/
 $content = "<html>
                    <head><meta http-equiv='Content-Type' content='text/html; charset=utf-8' /></head>
                    <body>
                 <div style='direction:ltr;border:1px solid #CEBA69; padding-top:10px; padding-bottom:10px; color:#D2A118;font-size:13px;padding-left:20px;width:600px;text-align:left' >";
 if($_SESSION['view_type'] != 'ge'){ 
              $content .= " <table celpadding='0' cellspacing='0'>
			   <tr><td width='300'><p style='color:#D2A118; font-weight:bold; font-size:12px; font-family:Arial, Helvetica, sans-serif;'>
			   Warmest Greetings to you from the Sheikh Zayed Grand Mosque Center! </p>
<p style='color:#D2A118; font-weight:bold; font-size:12px; font-family:Arial, Helvetica, sans-serif;'>
We are pleased to confirm your "; 
if($resultObj->group_cat=='Tour Operator' || $resultObj->group_cat=='Tour Operatorarb'){
	$content.="Visit (Access permission) ";
}
$content.="booking as per your request, ".$date_visit1." at ".$resultObj->time.". </p>";
	$content.="<p style='color:#D2A118; font-weight:bold; font-size:12px; font-family:Arial, Helvetica, sans-serif;'>Kindly note that the arrival time is 15 minutes before</p>";
$content.="<p style='color:#D2A118; font-weight:bold; font-size:12px; font-family:Arial, Helvetica, sans-serif;'>
Please find the following information below: </p>

			   </td> <td width='300' valign='top' style='float:right;text-align:right'> <p style='color:#D2A118; font-weight:bold; font-size:13px; font-family:tahoma;'><span dir='RTL'>&#1610;&#1607;&#1583;&#1610;&#1603;&#1605; &#1605;&#1585;&#1603;&#1586; &#1580;&#1575;&#1605;&#1593;  &#1575;&#1604;&#1588;&#1610;&#1582; &#1586;&#1575;&#1610;&#1583; &#1575;&#1604;&#1603;&#1576;&#1610;&#1585; &#1571;&#1591;&#1610;&#1576; &nbsp;&#1578;&#1581;&#1610;&#1575;&#1578;&#1607;&#1548;</span></p>
			   <p style='color:#D2A118; font-weight:bold; font-size:13px; font-family:tahoma;'><span dir='RTL'>ويسرنا إبلاغكم أنه تم تأكيد ";
			   if($resultObj->group_cat=='Tour Operator' || $resultObj->group_cat=='Tour Operatorarb'){
			   $content.="زيارتكم (تصريح دخول ) ليوم ";
			   }
			   $content.="".$date_visit1." &#1593;&#1606;&#1583; &#1575;&#1604;&#1587;&#1575;&#1593;&#1577; ".$resultObj->time."</span></p>";
	$content.="<p style='color:#D2A118; font-weight:bold; font-size:12px; font-family:tahoma;'><span dir='RTL'>&#1610;&#1585;&#1580;&#1609; &#1575;&#1604;&#1581;&#1590;&#1608;&#1585;  &#1602;&#1576;&#1604; &#1605;&#1608;&#1593;&#1583; &#1575;&#1604;&#1586;&#1610;&#1575;&#1585;&#1577; &#1576; 15 &#1583;&#1602;&#1610;&#1602;&#1577;</span></p>";
$content.="<p style='color:#D2A118; font-weight:bold; font-size:12px; font-family:tahoma;'><span dir='RTL'>&#1610;&#1585;&#1580;&#1609; &#1575;&#1604;&#1575;&#1591;&#1604;&#1575;&#1593; &#1593;&#1604;&#1609;  &#1575;&#1604;&#1605;&#1593;&#1604;&#1608;&#1605;&#1575;&#1578; &#1575;&#1604;&#1578;&#1575;&#1604;&#1610;&#1577; &#1571;&#1583;&#1606;&#1575;&#1607;:</span></p>
			   </td> </tr></table>";
}

 $content .= "<table width='600px' cellspacing='0' style='direction:ltr;'>
                    	
					
					<tr><td valign='top' align='center'>

    <table align='center' width='600' cellpadding='5' cellspacing='2' style='border-top:0px solid #D2A118;border-bottom:0px solid #D2A118; border-left:0px solid #D2A118; border-right:0px solid #D2A118' >";
	$content .="<tr><td style='color:#743303; font-weight:bold;'>Barcode:</td><td><img src='http://www.barcodesinc.com/generator/image.php?code=".$resultObj->reference."&style=197&type=C128B&width=200&height=50&xres=1&font=3'/></td><td style='color:#743303; font-weight:bold; text-align:right;'>الباركود</td>
   </tr>";
	$content .="<tr><td style='color:#743303; font-weight:bold;'>Reference number:</td><td>".$resultObj->reference."</td><td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>&#1585;&#1602;&#1605; &#1575;&#1604;&#1605;&#1585;&#1580;&#1593;:</span></td>
</tr>";
	$content .="<tr><td style='color:#743303; font-weight:bold;'>Group booking category:</td><td>".$cat."</td><td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>فئات الحجوزات الجماعية: </span></td>
</tr>";
if($resultObj->group_cat!='Education' && $resultObj->group_cat!='Educationarb')
{
$content .="<tr><td style='color:#743303; font-weight:bold;'>Country:</td><td>".$country."</td>
    <td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>&#1575;&#1604;&#1576;&#1604;&#1583;:</span></td>
    </tr>";	

if($resultObj->country=='United Arab Emirates' || $resultObj->country=='United Arab Emiratesarb')
{
$content .="<tr><td style='color:#743303; font-weight:bold;'>Emirate:</td><td>".$resultObj->emirate."</td>
    <td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>الإمارة:</span></td>
    </tr>";	
	}
	}
	if($resultObj->group_cat!='Tour Operator' && $resultObj->group_cat!='Tour Operatorarb')
{
	$content .="<tr><td style='color:#743303; font-weight:bold;'>Language:</td><td>".$resultObj->language."</td>
    <td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>&#1575;&#1582;&#1578;&#1610;&#1575;&#1585; &#1575;&#1604;&#1604;&#1594;&#1577; &#1575;&#1604;&#1605;&#1591;&#1604;&#1608;&#1576;&#1577; &#1604;&#1604;&#1580;&#1608;&#1604;&#1577;:</span></td>
    </tr>";
	}
	
	if($resultObj->group_cat=='Education' || $resultObj->group_cat=='Educationarb')
{
$content .="<tr><td style='color:#743303; font-weight:bold;'>Grade(s):</td><td>".$resultObj->grade."</td><td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>المستويات الدراسية:</span></td>
</tr>";
$content .="<tr><td style='color:#743303; font-weight:bold;'>Organization/Company Name:</td><td>".$resultObj->group."</td><td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>اسم المؤسسة /الشركة:</span></td>
</tr>";
$content .="<tr><td style='color:#743303; font-weight:bold;'>Organization/Company phone number:</td><td>".$resultObj->phone."</td><td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>رقم هاتف المؤسسة/الشركة:</span></td>
</tr>";
$content .="<tr><td style='color:#743303; font-weight:bold;'>Gender:</td><td>".$resultObj->gender."</td><td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>النوع الاجتماعي:</span></td>
</tr>";

}
if($resultObj->group_cat=='Hotel' || $resultObj->group_cat=='Hotelarb')
{
$content .="<tr><td style='color:#743303; font-weight:bold;'>Name of the hotel:</td><td>".$resultObj->group."</td><td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>اسم الفندق:</span></td>
</tr>";
$content .="<tr><td style='color:#743303; font-weight:bold;'>Hotel phone number:</td><td>".$resultObj->phone."</td><td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>رقم هاتف الفندق:</span></td>
</tr>";
}
if($resultObj->group_cat=='Government' || $resultObj->group_cat=='Governmentarb')
{

$content .="<tr><td style='color:#743303; font-weight:bold;'>Names and Titles of delegates or officials :</td><td>". $resultObj->deligates."</td><td style='color:#743303; font-weight:bold; text-align:right;'><div> <span dir='RTL'>الوفود ومناصبهم:</span> </div></td></tr>";
$content .="<tr><td style='color:#743303; font-weight:bold;'>Organization/Company Name:</td><td>".$resultObj->group."</td><td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>اسم المؤسسة /الشركة:</span></td>
</tr>";
$content .="<tr><td style='color:#743303; font-weight:bold;'>Organization/Company phone number:</td><td>".$resultObj->phone."</td><td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>رقم هاتف المؤسسة/الشركة:</span></td>
</tr>";
}
if($resultObj->group_cat=='Corporate' || $resultObj->group_cat=='Corporatearb')
{

$content .="<tr><td style='color:#743303; font-weight:bold;'>Names and Titles of delegates or officials :</td><td>". $resultObj->deligates."</td><td style='color:#743303; font-weight:bold; text-align:right;'><div> <span dir='RTL'>الوفود ومناصبهم:</span> </div></td></tr>";
}
if($resultObj->group_cat=='Embassy' || $resultObj->group_cat=='Embassyarb')
{

  
$content .="<tr><td style='color:#743303; font-weight:bold;'>Names and Titles of delegates or officials :</td><td>". $resultObj->deligates."</td><td style='color:#743303; font-weight:bold; text-align:right;'><div> <span dir='RTL'>الوفود ومناصبهم:</span> </div></td></tr>";
$content .="<tr><td style='color:#743303; font-weight:bold;'>Embassy phone number:</td><td>".$resultObj->phone."</td><td style='color:#743303; font-weight:bold; text-align:right;'><div> <span dir='rtl'>رقم هاتف السفارة:</span></div></td></tr>";
}
if($resultObj->group_cat=='Tour Operator' || $resultObj->group_cat=='Tour Operatorarb')
{
$content .="<tr><td style='color:#743303; font-weight:bold;'>Company name of the tour operator:</td><td>".$resultObj->group."</td><td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>اسم الشركة السياحية:</span></td>
</tr>";
$content .="<tr><td style='color:#743303; font-weight:bold;'>Phone no. of tour operator:</td><td>".$resultObj->phone."</td><td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>رقم هاتف الشركة السياحية:</span></td>
</tr>";
}
$content .="<tr><td style='color:#743303; font-weight:bold;'>Purpose of the visit:</td><td>".(!empty($resultObj->purpose) ? $resultObj->purpose : '--')."</td><td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>الهدف من الزيارة:</span></td>
</tr>";

if($resultObj->group_cat=='Tour Operator' || $resultObj->group_cat=='Tour Operatorarb')
{
$content .="<tr><td style='color:#743303; font-weight:bold;'>Type of visit:</td><td>".$resultObj->type_visit."</td><td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>نوع الزيارة:</span></td>
</tr>";
}
    $content .="<tr><td style='color:#743303; font-weight:bold;'> Contact Name:</td><td >".$resultObj->name."</td><td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>الاسم:</span></td>
    </tr>";
    $content .="<tr><td style='color:#743303; font-weight:bold;'>Email:</td><td>".$resultObj->email."</td><td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>البريد الإلكتروني:</span></td>
    </tr>";
   
	
if($resultObj->group_cat!='Tour Operator' && $resultObj->group_cat!='Tour Operatorarb')
{
    $content .="<tr><td style='color:#743303; font-weight:bold;'>Contact name of person accompanying the group:</td><td>".$resultObj->contact_name."</td>
    <td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>اسم الشخص الذي يرافق المجموعة:</span> </td>
    </tr>";
    $content .="<tr><td style='color:#743303; font-weight:bold;'>Contact mobile number of person accompanying the group:</td><td>".$resultObj->countryCode.$resultObj->contact_phone."</td>
    <td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>رقم الهاتف الجوال للشخص الذي سيرافق:</span></td>
    </tr>";	
}

if($resultObj->group_cat=='Tour Operator' || $resultObj->group_cat=='Tour Operatorarb')
{
$content .="<tr><td style='color:#743303; font-weight:bold;'>Name of Tourist Guide:</td><td>".$resultObj->contact_name."</td><td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>إسم المرشد السياحي المرافق:</span></td>
</tr>";
$content .="<tr><td style='color:#743303; font-weight:bold;'>Mobile no. of Tourist Guide:</td><td>".$resultObj->countryCode.$resultObj->contact_phone."</td><td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>رقم الهاتف النقال للمرشد المرافق:</span></td>
</tr>";
}


    $content .="<tr><td style='color:#743303; font-weight:bold;'>Proposed date of visit:</td><td>".$date_visit1."</td><td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>التاريخ المقترح للزيارة:</span></td>
    </tr>";
    $content .="<tr><td style='color:#743303; font-weight:bold;'>Proposed time of visit:</td><td>".$resultObj->time."</td><td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>وقت الزيارة :</span></td>
    </tr>";

    $content .="<tr><td style='color:#743303; font-weight:bold;'>Group Size:</td><td>".$resultObj->size."</td><td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>عدد أفراد المجموعة:</span></td>
    </tr>";
	/*if($resultObj->group_cat=='Tour Operator' || $resultObj->group_cat=='Tour Operatorarb')
{
$content .="<tr><td style='color:#743303; font-weight:bold;'>Number of Buses:</td><td>".$resultObj->bus."</td><td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>&#1593;&#1583;&#1583; &#1575;&#1604;&#1581;&#1575;&#1601;&#1604;&#1575;&#1578;:</span></td>
</tr>";
}*/
	 $content .="<tr><td style='color:#743303; font-weight:bold;'>Library visit:</td><td>".$resultObj->library."</td><td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>زيارة المكتبة:</span></td>
    </tr>";
    $content .="<tr><td style='color:#743303; font-weight:bold;'>Specify any special interests or needs for the group:</td><td>&nbsp;".$needs."</td><td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>تحديد أية اهتمامات واحتياجات خاصة<br>
      للمجموعة :</span></td>
    </tr>";
    
    $content .="</table></td>
    </tr></table>";  
	$content .="</div>";
    
    $_SESSION['tour_approved']['admin_mail_contents'] = $content;
    
/*This view is for the user*/
        $content = "<html>
                    <head><meta http-equiv='Content-Type' content='text/html; charset=utf-8' /></head>
                    <body>
                 <div style='direction:ltr;border:1px solid #CEBA69; padding-top:10px; padding-bottom:10px; color:#D2A118;font-size:13px;padding-left:20px;width:600px;text-align:left' >
               <table celpadding='0' cellspacing='0'>
			   <tr><td width='300'><p style='color:#D2A118; font-weight:bold; font-size:12px; font-family:Arial, Helvetica, sans-serif;'>
			   Warmest Greetings to you from the Sheikh Zayed Grand Mosque Center! </p>
<p style='color:#D2A118; font-weight:bold; font-size:12px; font-family:Arial, Helvetica, sans-serif;'>
We are pleased to confirm your "; 
if($resultObj->group_cat=='Tour Operator' || $resultObj->group_cat=='Tour Operatorarb'){
	$content.="Visit (Access permission) ";
}
$content.="booking as per your request, ".$date_visit1." at ".$resultObj->time.". </p>";
	$content.="<p style='color:#D2A118; font-weight:bold; font-size:12px; font-family:Arial, Helvetica, sans-serif;'>Kindly note that the arrival time is 15 minutes before</p>";
$content.="<p style='color:#D2A118; font-weight:bold; font-size:12px; font-family:Arial, Helvetica, sans-serif;'>
Please find the following information below: </p>

			   </td> <td width='300' valign='top' style='float:right;text-align:right'> <p style='color:#D2A118; font-weight:bold; font-size:13px; font-family:tahoma;'><span dir='RTL'>&#1610;&#1607;&#1583;&#1610;&#1603;&#1605; &#1605;&#1585;&#1603;&#1586; &#1580;&#1575;&#1605;&#1593;  &#1575;&#1604;&#1588;&#1610;&#1582; &#1586;&#1575;&#1610;&#1583; &#1575;&#1604;&#1603;&#1576;&#1610;&#1585; &#1571;&#1591;&#1610;&#1576; &nbsp;&#1578;&#1581;&#1610;&#1575;&#1578;&#1607;&#1548;</span></p>
			   <p style='color:#D2A118; font-weight:bold; font-size:13px; font-family:tahoma;'><span dir='RTL'>ويسرنا إبلاغكم أنه تم تأكيد ";
			   if($resultObj->group_cat=='Tour Operator' || $resultObj->group_cat=='Tour Operatorarb'){
			   $content.="زيارتكم (تصريح دخول ) ليوم ";
			   }
			   $content.="".$date_visit1." &#1593;&#1606;&#1583; &#1575;&#1604;&#1587;&#1575;&#1593;&#1577; ".$resultObj->time."</span></p>";
	$content.="<p style='color:#D2A118; font-weight:bold; font-size:12px; font-family:tahoma;'><span dir='RTL'>&#1610;&#1585;&#1580;&#1609; &#1575;&#1604;&#1581;&#1590;&#1608;&#1585;  &#1602;&#1576;&#1604; &#1605;&#1608;&#1593;&#1583; &#1575;&#1604;&#1586;&#1610;&#1575;&#1585;&#1577; &#1576; 15 &#1583;&#1602;&#1610;&#1602;&#1577;</span></p>";
$content.="<p style='color:#D2A118; font-weight:bold; font-size:12px; font-family:tahoma;'><span dir='RTL'>&#1610;&#1585;&#1580;&#1609; &#1575;&#1604;&#1575;&#1591;&#1604;&#1575;&#1593; &#1593;&#1604;&#1609;  &#1575;&#1604;&#1605;&#1593;&#1604;&#1608;&#1605;&#1575;&#1578; &#1575;&#1604;&#1578;&#1575;&#1604;&#1610;&#1577; &#1571;&#1583;&#1606;&#1575;&#1607;:</span></p>
			   </td> </tr></table>";


 $content .= "<table width='600px' cellspacing='0' style='direction:ltr;'>
                    	
					
					<tr><td valign='top' align='center'>

    <table align='center' width='600' cellpadding='5' cellspacing='2' style='border-top:0px solid #D2A118;border-bottom:0px solid #D2A118; border-left:0px solid #D2A118; border-right:0px solid #D2A118' >";
	$content .="<tr><td style='color:#743303; font-weight:bold;'>Barcode:</td><td><img src='http://www.barcodesinc.com/generator/image.php?code=".$resultObj->reference."&style=197&type=C128B&width=200&height=50&xres=1&font=3'/></td><td style='color:#743303; font-weight:bold; text-align:right;'>الباركود</td>
   </tr>";
	$content .="<tr><td style='color:#743303; font-weight:bold;'>Reference number:</td><td>".$resultObj->reference."</td><td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>&#1585;&#1602;&#1605; &#1575;&#1604;&#1605;&#1585;&#1580;&#1593;:</span></td>
</tr>";
	$content .="<tr><td style='color:#743303; font-weight:bold;'>Group booking category:</td><td>".$cat."</td><td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>فئات الحجوزات الجماعية: </span></td>
</tr>";
if($resultObj->group_cat!='Education' && $resultObj->group_cat!='Educationarb')
{
$content .="<tr><td style='color:#743303; font-weight:bold;'>Country:</td><td>".$country."</td>
    <td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>&#1575;&#1604;&#1576;&#1604;&#1583;:</span></td>
    </tr>";	

if($resultObj->country=='United Arab Emirates' || $resultObj->country=='United Arab Emiratesarb')
{
$content .="<tr><td style='color:#743303; font-weight:bold;'>Emirate:</td><td>".$resultObj->emirate."</td>
    <td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>الإمارة:</span></td>
    </tr>";	
	}
	}
	if($resultObj->group_cat!='Tour Operator' && $resultObj->group_cat!='Tour Operatorarb')
{
	$content .="<tr><td style='color:#743303; font-weight:bold;'>Language:</td><td>".$resultObj->language."</td>
    <td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>&#1575;&#1582;&#1578;&#1610;&#1575;&#1585; &#1575;&#1604;&#1604;&#1594;&#1577; &#1575;&#1604;&#1605;&#1591;&#1604;&#1608;&#1576;&#1577; &#1604;&#1604;&#1580;&#1608;&#1604;&#1577;:</span></td>
    </tr>";
	}
	
	if($resultObj->group_cat=='Education' || $resultObj->group_cat=='Educationarb')
{
$content .="<tr><td style='color:#743303; font-weight:bold;'>Grade(s):</td><td>".$resultObj->grade."</td><td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>المستويات الدراسية:</span></td>
</tr>";
$content .="<tr><td style='color:#743303; font-weight:bold;'>Organization/Company Name:</td><td>".$resultObj->group."</td><td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>اسم المؤسسة /الشركة:</span></td>
</tr>";
$content .="<tr><td style='color:#743303; font-weight:bold;'>Organization/Company phone number:</td><td>".$resultObj->phone."</td><td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>رقم هاتف المؤسسة/الشركة:</span></td>
</tr>";
$content .="<tr><td style='color:#743303; font-weight:bold;'>Gender:</td><td>".$resultObj->gender."</td><td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>النوع الاجتماعي:</span></td>
</tr>";

}
if($resultObj->group_cat=='Hotel' || $resultObj->group_cat=='Hotelarb')
{
$content .="<tr><td style='color:#743303; font-weight:bold;'>Name of the hotel:</td><td>".$resultObj->group."</td><td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>اسم الفندق:</span></td>
</tr>";
$content .="<tr><td style='color:#743303; font-weight:bold;'>Hotel phone number:</td><td>".$resultObj->phone."</td><td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>رقم هاتف الفندق:</span></td>
</tr>";
}
if($resultObj->group_cat=='Government' || $resultObj->group_cat=='Governmentarb')
{

$content .="<tr><td style='color:#743303; font-weight:bold;'>Names and Titles of delegates or officials :</td><td>". $resultObj->deligates."</td><td style='color:#743303; font-weight:bold; text-align:right;'><div> <span dir='RTL'>الوفود ومناصبهم:</span> </div></td></tr>";
$content .="<tr><td style='color:#743303; font-weight:bold;'>Organization/Company Name:</td><td>".$resultObj->group."</td><td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>اسم المؤسسة /الشركة:</span></td>
</tr>";
$content .="<tr><td style='color:#743303; font-weight:bold;'>Organization/Company phone number:</td><td>".$resultObj->phone."</td><td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>رقم هاتف المؤسسة/الشركة:</span></td>
</tr>";
}
if($resultObj->group_cat=='Corporate' || $resultObj->group_cat=='Corporatearb')
{

$content .="<tr><td style='color:#743303; font-weight:bold;'>Names and Titles of delegates or officials :</td><td>". $resultObj->deligates."</td><td style='color:#743303; font-weight:bold; text-align:right;'><div> <span dir='RTL'>الوفود ومناصبهم:</span> </div></td></tr>";
}
if($resultObj->group_cat=='Embassy' || $resultObj->group_cat=='Embassyarb')
{

  
$content .="<tr><td style='color:#743303; font-weight:bold;'>Names and Titles of delegates or officials :</td><td>". $resultObj->deligates."</td><td style='color:#743303; font-weight:bold; text-align:right;'><div> <span dir='RTL'>الوفود ومناصبهم:</span> </div></td></tr>";
$content .="<tr><td style='color:#743303; font-weight:bold;'>Embassy phone number:</td><td>".$resultObj->phone."</td><td style='color:#743303; font-weight:bold; text-align:right;'><div> <span dir='rtl'>رقم هاتف السفارة:</span></div></td></tr>";
}
if($resultObj->group_cat=='Tour Operator' || $resultObj->group_cat=='Tour Operatorarb')
{
$content .="<tr><td style='color:#743303; font-weight:bold;'>Company name of the tour operator:</td><td>".$resultObj->group."</td><td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>اسم الشركة السياحية:</span></td>
</tr>";
$content .="<tr><td style='color:#743303; font-weight:bold;'>Phone no. of tour operator:</td><td>".$resultObj->phone."</td><td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>رقم هاتف الشركة السياحية:</span></td>
</tr>";
$content .="<tr><td style='color:#743303; font-weight:bold;'>Purpose of the visit:</td><td>".$resultObj->purpose."</td><td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>الهدف من الزيارة:</span></td>
</tr>";
$content .="<tr><td style='color:#743303; font-weight:bold;'>Type of visit:</td><td>".$resultObj->type_visit."</td><td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>نوع الزيارة:</span></td>
</tr>";
}
    $content .="<tr><td style='color:#743303; font-weight:bold;'> Contact Name:</td><td >".$resultObj->name."</td><td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>الاسم:</span></td>
    </tr>";
    $content .="<tr><td style='color:#743303; font-weight:bold;'>Email:</td><td>".$resultObj->email."</td><td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>البريد الإلكتروني:</span></td>
    </tr>";
   
	
if($resultObj->group_cat!='Tour Operator' && $resultObj->group_cat!='Tour Operatorarb')
{
    $content .="<tr><td style='color:#743303; font-weight:bold;'>Contact name of person accompanying the group:</td><td>".$resultObj->contact_name."</td>
    <td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>اسم الشخص الذي يرافق المجموعة:</span> </td>
    </tr>";
    $content .="<tr><td style='color:#743303; font-weight:bold;'>Contact mobile number of person accompanying the group:</td><td>".$resultObj->countryCode.$resultObj->contact_phone."</td>
    <td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>رقم الهاتف الجوال للشخص الذي سيرافق:</span></td>
    </tr>";	
}

if($resultObj->group_cat=='Tour Operator' || $resultObj->group_cat=='Tour Operatorarb')
{
$content .="<tr><td style='color:#743303; font-weight:bold;'>Name of Tourist Guide:</td><td>".$resultObj->contact_name."</td><td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>إسم المرشد السياحي المرافق:</span></td>
</tr>";
$content .="<tr><td style='color:#743303; font-weight:bold;'>Mobile no. of Tourist Guide:</td><td>".$resultObj->countryCode.$resultObj->contact_phone."</td><td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>رقم الهاتف النقال للمرشد المرافق:</span></td>
</tr>";
}


    $content .="<tr><td style='color:#743303; font-weight:bold;'>Proposed date of visit:</td><td>".$date_visit1."</td><td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>التاريخ المقترح للزيارة:</span></td>
    </tr>";
    $content .="<tr><td style='color:#743303; font-weight:bold;'>Proposed time of visit:</td><td>".$resultObj->time."</td><td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>وقت الزيارة :</span></td>
    </tr>";

    $content .="<tr><td style='color:#743303; font-weight:bold;'>Group Size:</td><td>".$resultObj->size."</td><td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>عدد أفراد المجموعة:</span></td>
    </tr>";
	/*if($resultObj->group_cat=='Tour Operator' || $resultObj->group_cat=='Tour Operatorarb')
{
$content .="<tr><td style='color:#743303; font-weight:bold;'>Number of Buses:</td><td>".$resultObj->bus."</td><td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>&#1593;&#1583;&#1583; &#1575;&#1604;&#1581;&#1575;&#1601;&#1604;&#1575;&#1578;:</span></td>
</tr>";
}*/
	 $content .="<tr><td style='color:#743303; font-weight:bold;'>Library visit:</td><td>".$resultObj->library."</td><td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>زيارة المكتبة:</span></td>
    </tr>";
    $content .="<tr><td style='color:#743303; font-weight:bold;'>Specify any special interests or needs for the group:</td><td>&nbsp;".$needs."</td><td style='color:#743303; font-weight:bold; text-align:right;'><span dir='rtl'>تحديد أية اهتمامات واحتياجات خاصة<br>
      للمجموعة :</span></td>
    </tr>";
    
    $content .="</table></td>
    </tr></table>";  
	$content .="</body></html>";

    $content2 = "<html>
                    <head><meta http-equiv='Content-Type' content='text/html; charset=utf-8' /></head>
                    <body>
                    
                    <div id='toursPrintDiv' style='clear:both;'>
					<form action='group_tour.php?t=".$_SESSION['view_type']."' method='post' class='details_form'>
                    <table border='0' width='700px' cellspacing='2' align='center' style='direction:ltr;border:0'> ";
    $content2 .="<tr><td> <textarea name='approved' style='width:700px'> ".$content."	</textarea> 
	
	<input type='hidden' value=' ".$requestId."' name='id'>
	</td></tr>";
   if($_SESSION['view_type'] == 'ge'){
       $content2 .="<tr><td style='text-align:right;font-size: 15px;color: red;'> <b>(يرجى كتابة الملاحظات هنا</b><b> (سيتم إرسال هذه الملاحظات الى رئيس القسم</b></td></tr>";
       $content2 .="<tr><td> <textarea class='comments' name='comments' style='width:700px;height: 150px;'></textarea>  </td></tr>";
   }
   $content2 .="<tr><td> <input type='submit' value='Submit'> </td></tr>";
	
	
    $content2 .="</table></body></html>";
    print $content2;

//    var_dump($resultObj);
}
?>
