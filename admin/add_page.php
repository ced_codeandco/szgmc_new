<?php
include_once ('includes/commons.php');
$request_url=$_SERVER['REQUEST_URI'];

/* page creation and   post handling*/
if(preg_match('/\/admin\/add_page.php/',$request_url) && isset($_POST['p_title']) && $_POST['p_title']!='' && isset($_POST['p_url']) && $_POST['p_url']!='' )
{
$title=mysql_real_escape_string(trim($_POST['p_title']));
$m_title=mysql_real_escape_string(trim($_POST['m_title']));
$m_keywords=mysql_real_escape_string(trim($_POST['m_keywords']));
$m_desc=mysql_real_escape_string(trim($_POST['m_desc']));
$p_url=mysql_real_escape_string(trim($_POST['p_url']));
$new_page_visibility=trim($_POST['new_page_visibility']);
$new_page_live=trim($_POST['new_page_live']);
$new_page_php=trim($_POST['new_page_php']);
$img_flash_name=trim($_POST['header_img_flash']);

$q="insert into pages(php_content,title,meta_title,meta_desc,meta_keywords,url,header_image_flash,visibility,live,created_by,updated_date,created_date)values('$new_page_php','$title','$m_title','$m_desc','$m_keywords','$p_url','$img_flash_name','$new_page_visibility','$new_page_live','$_SESSION[admin_id]',now(),CURRENT_TIMESTAMP)";
$mydb->query($q);
if($mydb->Errno==0)
{
$page_id=mysql_insert_id();
header("location: /admin/edit_page.php?page_id=".$page_id);	
exit(0);
}
}
do_header();
?>



<script type="text/javascript">
  $(document).ready(function(){
    $("#cms_page_frm").validate({
			rules: {
			p_title: "required",
			m_title: "required",
			m_keywords: "required",
			m_desc: "required",
				p_url:
				{ 
				required: true,				
				remote: "/admin/misc/check-url-exists.php",
				minlength: 4
				}
				
			},messages: {
				p_url:{
				required: "This field is required",
				minlength: "minimum 5 characters long",
				remote:"This URL already exists. Try another"
				}
				}

		});
  });
  </script>


<form action="/admin/add_page.php" method="post" id="cms_page_frm">
<table class="menu_list_table" align="center">
<tr><td colspan="2"><h2>Add new Page</h2></td></tr>
<tr><td>Name[For reference only]</td><td><input class="input-text" type="text" name="p_title"/></td></tr>
<tr><td>Meta Title[SEO]</td><td><input class="input-text" type="text" name="m_title"/></td></tr>
<tr><td>Meta Keywords[SEO]</td><td><input class="input-text" type="text" name="m_keywords"/></td></tr>
<tr><td>Meta Description[SEO]</td><td><input class="input-text" type="text" name="m_desc"/></td></tr>
<tr><td>Page URL</td><td>
<input class="input-text" type="text" name="p_url"/>
</td></tr>
<tr><td>Header(image/flash) </td><td>
<select name="header_img_flash">
<option value=''></option>
<?php
$array_files = dirImages('../flash_and_images/');
foreach ($array_files as $key => $image) // Display Images
{
echo "<option value='$image[file]'>$image[file]</option>";
} 
?>
</select>
</td></tr>
<tr><td>Active</td><td><select name="new_page_visibility">
<option value="B">Both</option>
<option value="A">Admin</option>
<option value="P">Public</option>
</select></td></tr>
<tr><td>Make this item</td><td>
<select name="new_page_live">
<option value="Y">Live</option>
<option value="N">Down</option>
</select></td></tr>
<tr><td>PHP</td><td><select name="new_page_php">
<option value="N">No</option>
<option value="Y">Yes</option>
</select></td></tr>
<tr><td colspan="2"><input type="submit" value="Save and Continue" class="btn_admin"/></td></tr>
</table>
</form>



<?php

do_footer();
?>