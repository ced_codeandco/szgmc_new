﻿<?php
ob_start();
include_once ('includes/commons.php');;
do_header();

?>
<script type="text/javascript" src="/en/admin/js/common.js"></script>
<script type="text/javascript" src="/en/admin/js/jquery.js"></script>
<link href="http://www.szgmc.ae/en/css/jquery-ui-1.8.24.custom.css" rel="stylesheet" type="text/css" />
<style>
	.status{
		
		cursor: pointer;
		font-size: 18px;
	}
</style>
<script type="text/javascript">
$(document).ready(function(){
	
	$('.status').live('click',function(){

		var status = $('.status').text();
		
		if(confirm('Sure, Want to change?')){
			
			if(status == 'Active'){
			
				$('.status').text('Not Active');
				$('.status').css('color','red');
			}
			else{
				
				$('.status').text('Active');
				$('.status').css('color','green');
			}
			
			$.post('login_cred_actions.php',{'rcv_email':$('#receiver_email').val(),'status': status,'actionType': 'update status'},function(data){
			
			});
		}

	});
	
	$('#receiver_email').keyup(getLoginCred);
	function getLoginCred(e){
		
		if($('#receiver_email').val() != ''){
			
			$('.status').css('color','black');
			$('.status').text('No record found');
		}
		else{
			
			$('.status').text('');
		}

		$('#username').val('');
		$('#password').val('');
		$.post('login_cred_actions.php',{'rcv_email':$('#receiver_email').val(),'actionType': 'get Login'},function(data){
			
			var response = $.parseJSON(data);
			if(response.msg == 'found'){
				
				$('.status').css('color',response.color);
				$('.status').text(response.status);
				$('#username').val(response.username);
				$('#password').val(response.password);
			}
			
		});
	}
		
	$("#send").click(function(){

		if($('#receiver_email').val() == '' | $('#username').val() == '' | $('#password').val() == ''){
			$(function() {
				$( ".errorMsg" ).show();
			});
			return false;
		}
		var rcv_email = $('#receiver_email').val();
		var bcc = $('#bcc').val();
		var user = $('#username').val();
		var pass = $('#password').val();
		
		$.post('send_login_cred_action.php',{'rcv_email':rcv_email,'bcc':bcc,'user':user,'pass':pass},function(data){

			if(data.indexOf('ok') > -1){
				
				$('.responseMsg .ar_msg').text('أرسلت اعتماد تسجيل الدخول بنجاح');
				$('.responseMsg p span').text('Login Credentials sent successfully');
			}
			else{
				$('.responseMsg .ar_msg').text('خطأ في إرسال بيانات الدخول');
				$('.responseMsg p span').text('Error in sending Login Credentials');
			}
			$( ".responseMsg" ).show();
		});
		
	});
	$(function(){
			$('#confirm,.close').click(function() {
				$( ".errorMsg, .responseMsg" ).hide();
				return false;
			});
		});

});
</script>
<h2 style="padding-left:20px;margin: 10px 0px 30px 0px;">Send Login Credentials</h2>
<form  action="send_login_cred_action.php" method="post" >
    <table align="center" style="margin-top:15px;"  >

  
            <tr>
           		<td>Enter Receiver email</td> 
                 <td>
                    <input type="text" id="receiver_email" name="receiver_email"  size="30" /> 
                </td>
            </tr>    
            <tr>
            	<td>Add BCC (optional)</td>
                <td>
                    <input type="text" id="bcc" name="bcc" size="30" /> 
                </td>
             </tr> 
			<tr>
            	<td>Status</td>
                <td>
                    <span class="status"></span>
                </td>
             </tr> 			 
             <tr>
            	<td>Username</td>
                <td>
                    <input type="text" id="username" name="username" size="30" /> 
                </td>
             </tr>   
		   <tr>
            	<td>Password</td>
                <td>
                    <input type="text" id="password" name="password" size="30" /> 
                </td>
            </tr>    
           <tr>
                  <td> 
                    <input type="button" value="Send" name="send" id="send">
                </td>
            </tr>
    </table> 
</form>
<?php
do_footer();
?>
<div class="responseMsg ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable ui-resizable" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog" style="display: none; position: absolute; overflow: hidden; z-index: 1002; outline: 0px; height: auto; width: 300px; top: 381.5px; left: 486px;"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on">&nbsp;</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on"><span class="ui-icon ui-icon-closethick close" unselectable="on">close</span></a></div><div id="dialog" style="height: auto; min-height: 102px; width: auto;" class="ui-dialog-content ui-widget-content">
			<p style="text-align:right;" class="ar_msg">ي</p>
            <p><span dir="ltr"></span></p>
            <p style=""><input type="button" value="OK" style="float:right;width:40px" id="confirm"></p>
        </div><div class="ui-resizable-handle ui-resizable-n" unselectable="on"></div><div class="ui-resizable-handle ui-resizable-e" unselectable="on"></div><div class="ui-resizable-handle ui-resizable-s" unselectable="on"></div><div class="ui-resizable-handle ui-resizable-w" unselectable="on"></div><div class="ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se ui-icon-grip-diagonal-se" unselectable="on" style="z-index: 1001;"></div><div class="ui-resizable-handle ui-resizable-sw" unselectable="on" style="z-index: 1002;"></div><div class="ui-resizable-handle ui-resizable-ne" unselectable="on" style="z-index: 1003;"></div><div class="ui-resizable-handle ui-resizable-nw" unselectable="on" style="z-index: 1004;"></div></div>
<div class="errorMsg ui-dialog ui-widget ui-widget-content ui-corner-all  ui-draggable ui-resizable" tabindex="-1" role="dialog" aria-labelledby="ui-dialog-title-dialog" style="display: none; position: absolute; overflow: hidden; z-index: 1002; outline: 0px; height: auto; width: 300px; top: 381.5px; left: 486px;"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" unselectable="on"><span class="ui-dialog-title" id="ui-dialog-title-dialog" unselectable="on">&nbsp;</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button" unselectable="on"><span class="ui-icon ui-icon-closethick close" unselectable="on">close</span></a></div><div id="dialog" style="height: auto; min-height: 102px; width: auto;" class="ui-dialog-content ui-widget-content">
			<p style="text-align:right;">يرجى ملء الحقول بشكل صحيح</p>
            <p><span dir="ltr">Please fill up the fields correctly</span></p>
            <p style=""><input type="button" value="OK" style="float:right;width:40px" id="confirm"></p>
        </div><div class="ui-resizable-handle ui-resizable-n" unselectable="on"></div><div class="ui-resizable-handle ui-resizable-e" unselectable="on"></div><div class="ui-resizable-handle ui-resizable-s" unselectable="on"></div><div class="ui-resizable-handle ui-resizable-w" unselectable="on"></div><div class="ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se ui-icon-grip-diagonal-se" unselectable="on" style="z-index: 1001;"></div><div class="ui-resizable-handle ui-resizable-sw" unselectable="on" style="z-index: 1002;"></div><div class="ui-resizable-handle ui-resizable-ne" unselectable="on" style="z-index: 1003;"></div><div class="ui-resizable-handle ui-resizable-nw" unselectable="on" style="z-index: 1004;"></div></div>