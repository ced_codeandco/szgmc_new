﻿<?php
ob_start();
include_once ('includes/commons.php');;
do_header();

?>
<script type="text/javascript">
$(function() {
$("#from_date").datepicker({dateFormat: 'dd-mm-yy'});
$("#to_date").datepicker({dateFormat: 'dd-mm-yy'});
});
</script>
<style>
.whiteBackground { background-color: #fff; }
.grayBackground { background-color: #ccc; }

</style>
<?php
if(isset($_POST["from_date"]))
{
$from_date=$_POST["from_date"];
$from_date1 = explode("-",$from_date);
$fromdate = $from_date1[2]."-".$from_date1[1]."-".$from_date1[0];

}
else
{
$from_date=date("d-m-Y");
$from_date = date('d-m-Y', strtotime($from_date .' -1 day'));
$from_date1 = explode("-",$from_date);
$fromdate = $from_date1[2]."-".$from_date1[1]."-".$from_date1[0];
}
if(isset($_POST["to_date"]))
{
$to_date=$_POST["to_date"];
$to_date1 = explode("-",$to_date);
$todate = $to_date1[2]."-".$to_date1[1]."-".$to_date1[0];
}
else
{
	$to_date=date("d-m-Y");
	$to_date1 = explode("-",$to_date);
$todate = $to_date1[2]."-".$to_date1[1]."-".$to_date1[0];
}
if (!empty($fromdate)) {
    $fromdate .= ' 00:00:00';
}
if (!empty($todate)) {
    $todate .= ' 23:59:59';
}
?>
<script type="text/javascript" src="/en/admin/js/common.js"></script>
<script>
function check_additional_fileds(val)
{
	if((val=='Category') )
	{
     $('.gcat').show();
	}
	else
	{
	  $('.gcat').hide();
	}
	if((val=='guide') )
	{
     $('.guide').show();
	}
	else
	{
	  $('.guide').hide();
	}
	if((val=='edited') || (val=='approved') || (val=='disapproved') )
	{
     $('.user').show();
	}
	else
	{
	  $('.user').hide();
	}
	if((val=='Enquiry') || (val=='Name') || (val=='Group') || (val=='Email') || (val=='visit') || (val=='Category') || (val=='guide') || (val=='edited') || (val=='approved') || (val=='disapproved') || (val=='status') || (val=='type_of_booking') || (val=='attendance') || (val=='attendance_time') )
	{
     $('.search').show();
	}
	else
	{
	 $('.search').hide();
	}  
}
function expXLS(){
	window.location.href="export_user.php?"+$('#frmAdvanceReport').serialize();
	return false
}
</script>
<?php

$cat = $_POST["group_category"];
$cat1 = $_POST["cat"];
$search_item = trim($_POST["search_item"]);
$guide_id = $_POST["guide"];
$user_name=$_POST["user"];
?>
<h1 style="margin-left:25px;">Advance Report </h1>
<div id='toursPrintDiv' style='clear:both;'>

<style>
.menu_list_table thead tr{background:transparent}
.menu_list_table thead td{padding:0!important;border:0;text-align:center}
.menu_list_table input,.menu_list_table select{width:auto!important;height:auto!important}
.menu_list_table thead select,.menu_list_table thead input[type="text"]{width:99%!important}
.menu_list_table thead input[type="submit"]{width:100px!important;text-align:center}
.menu_list_table th{padding-left:10px;padding-right:10px}
</style>

<div style="width:150px; margin:0 auto; float:right">
<div align="center" style="width:50px; float:left;"><a href='#' onclick='printFilming("toursPrintDiv"); return false;'><img src='images/print.jpg' alt='Print'/></a></div>
<div align="center" style="width:50px; float:left;"><a href="javascript:expXLS()"><img src="images/excel_icon.jpg" border="0" style="width:30px" align="middle"></a></div>
</div>

<form  action="<?php echo $_SERVER['PHP_SELF']?>" method="post" id="frmAdvanceReport">
    
<table align="center" class="menu_list_table" style="margin-top:15px;" width="97%">

<thead>
	<tr>
		<td colspan="17">
		
<table align="center">
<tr><td>From <br/><input type="text" id="from_date" name="from_date" style="width:80px;" value="<?php echo $from_date;?>"/> </td>
<td>To<br/><input type="text" id="to_date" name="to_date" style="width:80px;" value="<?php echo $to_date;?>"/> </td> 
      <td class="gcat" style=" <?php if($cat1!="Category") { ?>display:none; <?php } ?>">Select<br/><select class="input_cls" name="group_category" id="group_category"  >
<option value="">Please select - &#1610;&#1585;&#1580;&#1610; &#1575;&#1582;&#1578;&#1610;&#1575;&#1585;</option>
<optgroup label="----------------------------------------" > </optgroup>
<option value="General Public" <?php if($cat=="General Public"){ ?> selected="selected"  <?php } ?>>&#1586;&#1610;&#1575;&#1585;&#1577; &#1593;&#1575;&#1605;&#1607;</option>
<option value="General Public" <?php if($cat=="General Public"){ ?> selected="selected"  <?php } ?>>General Public</option>
<optgroup label="----------------------------------------" > </optgroup>
<option value="Government" <?php if($cat=="Government"){ ?> selected="selected"  <?php } ?>>&#1575;&#1604;&#1607;&#1610;&#1574;&#1575;&#1578; &#1575;&#1604;&#1581;&#1603;&#1608;&#1605;&#1610;&#1577;</option>
<option value="Government" <?php if($cat=="Government"){ ?> selected="selected"  <?php } ?>>Government Entity</option>
<optgroup label="----------------------------------------" > </optgroup>
<option value="Embassy" <?php if($cat=="Embassy"){ ?> selected="selected"  <?php } ?>>&#1575;&#1604;&#1587;&#1601;&#1575;&#1585;&#1575;&#1578;</option>
<option value="Embassy" <?php if($cat=="Embassy"){ ?> selected="selected"  <?php } ?>>Embassy</option>
<optgroup label="----------------------------------------" > </optgroup>
<option value="Tour Operator" <?php if($cat=="Tour Operator"){ ?> selected="selected"  <?php } ?>>&#1575;&#1604;&#1588;&#1585;&#1603;&#1575;&#1578; &#1575;&#1604;&#1587;&#1610;&#1575;&#1581;&#1610;&#1577;</option>
<option value="Tour Operator" <?php if($cat=="Tour Operator"){ ?> selected="selected"  <?php } ?>>Tour Operator</option>
<optgroup label="----------------------------------------" > </optgroup>
<option value="Education" <?php if($cat=="Education"){ ?> selected="selected"  <?php } ?>>&#1575;&#1604;&#1605;&#1572;&#1587;&#1587;&#1575;&#1578; &#1575;&#1604;&#1578;&#1593;&#1604;&#1610;&#1605;&#1610;&#1577;</option>
<option value="Education" <?php if($cat=="Education"){ ?> selected="selected"  <?php } ?>>Educational Institution</option>
<optgroup label="----------------------------------------" > </optgroup>
<option value="Hotel" <?php if($cat=="Hotel"){ ?> selected="selected"  <?php } ?>>&#1575;&#1604;&#1601;&#1606;&#1583;&#1602;</option>
<option value="Hotel" <?php if($cat=="Hotel"){ ?> selected="selected"  <?php } ?>>Hotel</option>
<optgroup label="----------------------------------------" > </optgroup>
<option value="Corporate">&#1575;&#1604;&#1602;&#1591;&#1575;&#1593; &#1575;&#1604;&#1582;&#1575;&#1589;</option>
<option value="Corporate">Corporate Sector</option>
<optgroup label="----------------------------------------" > </optgroup>
<option value="All" <?php if($cat=="All"){ ?> selected="selected"  <?php } ?>>&#1580;&#1605;&#1610;&#1593;</option>
<option value="All" <?php if($cat=="All"){ ?> selected="selected"  <?php } ?>>All</option>
</select> </td>    

<td class="guide" style=" <?php if($cat1!="guide") { ?>display:none; <?php } ?>">Select<br/>
<?php
$csql = "SELECT * FROM `cultural_guide`";
$cquery = mysql_query($csql);
?>
<select class="input_cls" name="guide" id="guide"  >
<option value="">Select </option>
<?php
while($cres = mysql_fetch_object($cquery))
{
?>
    <option value=<?php echo $cres->g_id; if($cres->g_id==$guide_id){ ?> selected="selected" <?php } ?> > <?php echo $cres->g_name ; ?> </option>
<?php
}
?>
</select>
 </td> 
 
 <td class="user" style=" <?php if(($cat1!="edited") && ($cat1!="approved") && ($cat1!="disapproved") ) { ?>display:none; <?php } ?>">Select<br/>
<?php
$csql = "SELECT * FROM `admin_login` where id>11";
$cquery = mysql_query($csql);
?>
<select class="input_cls" name="user" id="user"  >
<option value="">Select </option>
<?php
while($cres = mysql_fetch_object($cquery))
{
?>
    <option value=<?php echo $cres->user_name; if($cres->user_name==$user_name){ ?> selected="selected" <?php } ?> > <?php echo $cres->user_name ; ?> </option>
<?php
}
?>
</select>
 </td>

<td class="search" style=" <?php if($cat1=="") { ?> display:none; <?php } ?> ">Keyword<br/> <input type="text" name="search_item" value="<?php echo $search_item; ?>" /> </td>
 <td>&nbsp;<br/> <input type="submit" value="Submit" name="submit"> </td>
</tr></table> 
		
		</td>
	</tr>
	<tr valign="bottom">
		<td><input type="checkbox" name="enquiry_date_check" value="1" <?php if($_POST['enquiry_date_check']==1) { ?>  checked="checked" <?php } ?>></td>
		<td>
			<select id="enq_time_hour" name="enq_time_hour" style="width: 100% !important;float: left;">
			<option value="">Hour</option>
			<option value="08"  <?php if(!empty($_POST['enq_time_check']) && $_POST['enq_time_hour']=='08') { ?> selected="selected" <?php } ?>>8:00</option>
			<option value="09"  <?php if(!empty($_POST['enq_time_check']) && $_POST['enq_time_hour']=='09') { ?> selected="selected" <?php } ?>>9:00</option>
			<option value="10"  <?php if(!empty($_POST['enq_time_check']) && $_POST['enq_time_hour']=='10') { ?> selected="selected" <?php } ?>>10:00</option>
			<option value="11" <?php if(!empty($_POST['enq_time_check']) && $_POST['enq_time_hour']=='11') { ?> selected="selected" <?php } ?>>11:00</option>
			<option value="12" <?php if(!empty($_POST['enq_time_check']) && $_POST['enq_time_hour']=='12') { ?> selected="selected" <?php } ?>>12:00</option>
			<option value="13" <?php if(!empty($_POST['enq_time_check']) && $_POST['enq_time_hour']=='13') { ?> selected="selected" <?php } ?>>13:00</option>
			<option value="14" <?php if(!empty($_POST['enq_time_check']) && $_POST['enq_time_hour']=='14') { ?> selected="selected" <?php } ?>>14:00</option>
			<option value="15" <?php if(!empty($_POST['enq_time_check']) && $_POST['enq_time_hour']=='15') { ?> selected="selected" <?php } ?>>15:00</option>
			<option value="16" <?php if(!empty($_POST['enq_time_check']) && $_POST['enq_time_hour']=='16') { ?> selected="selected" <?php } ?>>16:00</option>
			<option value="17" <?php if(!empty($_POST['enq_time_check']) && $_POST['enq_time_hour']=='17') { ?> selected="selected" <?php } ?>>17:00</option>
			<option value="18" <?php if(!empty($_POST['enq_time_check']) && $_POST['enq_time_hour']=='18') { ?> selected="selected" <?php } ?>>18:00</option>
			<option value="19" <?php if(!empty($_POST['enq_time_check']) && $_POST['enq_time_hour']=='19') { ?> selected="selected" <?php } ?>>19:00</option>
			<option value="20" <?php if(!empty($_POST['enq_time_check']) && $_POST['enq_time_hour']=='20') { ?> selected="selected" <?php } ?>>20:00</option>
			<option value="21" <?php if(!empty($_POST['enq_time_check']) && $_POST['enq_time_hour']=='21') { ?> selected="selected" <?php } ?>>21:00</option>
			</select>
            <select id="enq_time_minute" name="enq_time_minute" style="width: 100% !important;float: left;">
			<option value="">Minute</option>
			<?php for($i = 1; $i < 60; $i++) {
                $minute = sprintf("%02s", $i);?>
                <option value="<?php echo $minute;?>"  <?php if(!empty($_POST['enq_time_check']) && $_POST['enq_time_minute']== $minute) { ?> selected="selected" <?php } ?>><?php echo $minute;?></option><?php
            }?>
			</select><br/>
			<input type="checkbox" name="enq_time_check" value="1" <?php if($_POST['enq_time_check']==1) { ?>  checked="checked" <?php } ?>>
		</td>
        <td>
			<select id="time" name="time">
			<option value="">Select time</option>
			<option value="08:00:00"  <?php if($_POST['time']=='08:00:00') { ?> selected="selected" <?php } ?>>8:00</option>
			<option value="08:30:00"  <?php if($_POST['time']=='08:30:00') { ?> selected="selected" <?php } ?>>8:30</option>
			<option value="09:00:00"  <?php if($_POST['time']=='09:00:00') { ?> selected="selected" <?php } ?>>9:00</option>
			<option value="09:30:00"  <?php if($_POST['time']=='09:30:00') { ?> selected="selected" <?php } ?>>9:30</option>
			<option value="10:00:00"  <?php if($_POST['time']=='10:00:00') { ?> selected="selected" <?php } ?>>10:00</option>
			<option value="10:30:00" <?php if($_POST['time']=='10:30:00') { ?> selected="selected" <?php } ?>>10:30</option>
			<option value="11:00:00" <?php if($_POST['time']=='11:00:00') { ?> selected="selected" <?php } ?>>11:00</option>
			<option value="11:30:00" <?php if($_POST['time']=='11:30:00') { ?> selected="selected" <?php } ?>>11:30</option>
			<option value="12:00:00" <?php if($_POST['time']=='12:00:00') { ?> selected="selected" <?php } ?>>12:00</option>
			<option value="12:30:00" <?php if($_POST['time']=='12:30:00') { ?> selected="selected" <?php } ?>>12:30</option>
			<option value="13:00:00" <?php if($_POST['time']=='13:00:00') { ?> selected="selected" <?php } ?>>13:00</option>
			<option value="13:30:00" <?php if($_POST['time']=='13:30:00') { ?> selected="selected" <?php } ?>>13:30</option>
			<option value="14:00:00" <?php if($_POST['time']=='14:00:00') { ?> selected="selected" <?php } ?>>14:00</option>
			<option value="14:30:00" <?php if($_POST['time']=='14:30:00') { ?> selected="selected" <?php } ?>>14:30</option>
			<option value="15:00:00" <?php if($_POST['time']=='15:00:00') { ?> selected="selected" <?php } ?>>15:00</option>
			<option value="15:30:00" <?php if($_POST['time']=='15:30:00') { ?> selected="selected" <?php } ?>>15:30</option>
			<option value="16:00:00" <?php if($_POST['time']=='16:00:00') { ?> selected="selected" <?php } ?>>16:00</option>
			<option value="16:30:00" <?php if($_POST['time']=='16:30:00') { ?> selected="selected" <?php } ?>>16:30</option>
			<option value="17:00:00" <?php if($_POST['time']=='17:00:00') { ?> selected="selected" <?php } ?>>17:00</option>
			<option value="17:15:00" <?php if($_POST['time']=='17:15:00') { ?> selected="selected" <?php } ?>>17:15</option>
			<option value="18:00:00" <?php if($_POST['time']=='18:00:00') { ?> selected="selected" <?php } ?>>18:00</option>
			<option value="18:45:00" <?php if($_POST['time']=='18:45:00') { ?> selected="selected" <?php } ?>>18:45</option>
			<option value="19:00:00" <?php if($_POST['time']=='19:00:00') { ?> selected="selected" <?php } ?>>19:00</option>
			<option value="19:30:00" <?php if($_POST['time']=='19:30:00') { ?> selected="selected" <?php } ?>>19:30</option>
			<option value="19:45:00" <?php if($_POST['time']=='19:45:00') { ?> selected="selected" <?php } ?>>19:45</option>
			<option value="20:00:00" <?php if($_POST['time']=='20:00:00') { ?> selected="selected" <?php } ?>>20:00</option>
			<option value="20:30:00" <?php if($_POST['time']=='20:30:00') { ?> selected="selected" <?php } ?>>20:30</option>
			<option value="21:00:00" <?php if($_POST['time']=='21:00:00') { ?> selected="selected" <?php } ?>>21:00</option>
			</select><br/>
			<input type="checkbox" name="visit_time_check" value="1" <?php if($_POST['visit_time_check']==1) { ?>  checked="checked" <?php } ?>>
		</td>
		<td><input type="checkbox" name="visit_date_check" value="1" <?php if($_POST['visit_date_check']==1) { ?>  checked="checked" <?php } ?>></td>
		<td><input type="text" name="reference_no" style="width:110px;" value="<?php echo $_POST['reference_no']; ?>"> <br/> <input type="checkbox" name="reference_no_check" value="1" <?php if($_POST['reference_no_check']==1) { ?>  checked="checked" <?php } ?>></td>
		<td><input type="text" name="user_name" style="width:110px;" value="<?php echo $_POST['user_name']; ?>"> <br/> <input type="checkbox" name="username_check" value="1" <?php if($_POST['username_check']==1) { ?>  checked="checked" <?php } ?>></td>
		<td><input type="text" name="org_name" style="width:110px;" value="<?php echo $_POST['org_name']; ?>"> <br/> <input type="checkbox" name="org_name_check" value="1" <?php if($_POST['org_name_check']==1) { ?>  checked="checked" <?php } ?>></td>
		<td>
        <?php
$country_sql = "select * from country_t order by eng_name";
$country_query = mysql_query($country_sql);
?>
<select class="input_cls" name="country_name" id="country_name" >
<option value="">Please select - يرجى اختر</option>
<option value="United Arab Emirates"  <?php if($_POST['country_name']=="United Arab Emirates") { ?> selected="selected" <?php } ?>>United Arab Emirates</option>
<?php while ($_countries_res = mysql_fetch_object($country_query))
{
?>
<option value="<?php echo $_countries_res->eng_name;?>" <?php if($_POST['country_name']==$_countries_res->eng_name) { ?> selected="selected" <?php } ?>><?php echo $_countries_res->eng_name;?></option>
<?php
}
?>
<option value="Other" <?php if($_POST['country_name']=="Other") { ?> selected="selected" <?php } ?>>Other</option>
</select>
   
        <br/> <input type="checkbox" name="country_name_check" value="1" <?php if($_POST['country_name_check']==1) { ?>  checked="checked" <?php } ?>></td>
		<td><input type="text" name="contact_name" style="width:90px;" value="<?php echo $_POST['contact_name']; ?>" > <br/> <input type="checkbox" name="contact_name_check" value="1" <?php if($_POST['contact_name_check']==1) { ?>  checked="checked" <?php } ?>></td>
		<td><input type="text" name="group_size" style="width:90px;" value="<?php echo $_POST['group_size']; ?>" > <br/> <input type="checkbox" name="group_size_check" value="1" <?php if($_POST['group_size_check']==1) { ?>  checked="checked" <?php } ?>></td>
		<td>
       
        
        <select id="time" name="attendance_time">
			<option value="">Select time</option>
			<option value="08"  <?php if($_POST['attendance_time']=='08') { ?> selected="selected" <?php } ?>>08:00:00 - 08:59:59</option>
			<option value="09"  <?php if($_POST['attendance_time']=='09') { ?> selected="selected" <?php } ?>>09:00:00 - 09:59:59</option>
			<option value="10"  <?php if($_POST['attendance_time']=='10') { ?> selected="selected" <?php } ?>>10:00:00 - 10:59:59</option>
            <option value="11"  <?php if($_POST['attendance_time']=='11') { ?> selected="selected" <?php } ?>>11:00:00 - 11:59:59</option>
            <option value="12"  <?php if($_POST['attendance_time']=='12') { ?> selected="selected" <?php } ?>>12:00:00 - 12:59:59</option>
            <option value="13"  <?php if($_POST['attendance_time']=='13') { ?> selected="selected" <?php } ?>>13:00:00 - 13:59:59</option>
            <option value="14"  <?php if($_POST['attendance_time']=='14') { ?> selected="selected" <?php } ?>>14:00:00 - 14:59:59</option>
            <option value="15"  <?php if($_POST['attendance_time']=='15') { ?> selected="selected" <?php } ?>>15:00:00 - 15:59:59</option>
            <option value="16"  <?php if($_POST['attendance_time']=='16') { ?> selected="selected" <?php } ?>>16:00:00 - 16:59:59</option>
            <option value="17"  <?php if($_POST['attendance_time']=='17') { ?> selected="selected" <?php } ?>>17:00:00 - 17:59:59</option>
            <option value="18"  <?php if($_POST['attendance_time']=='18') { ?> selected="selected" <?php } ?>>18:00:00 - 18:59:59</option>
            <option value="19"  <?php if($_POST['attendance_time']=='19') { ?> selected="selected" <?php } ?>>19:00:00 - 19:59:59</option>
            <option value="20"  <?php if($_POST['attendance_time']=='20') { ?> selected="selected" <?php } ?>>20:00:00 - 20:59:59</option>
            <option value="21"  <?php if($_POST['attendance_time']=='21') { ?> selected="selected" <?php } ?>>21:00:00 - 21:59:59</option>
			</select>
        
        
        <br/> <input type="checkbox" name="attendance_time_check" value="1" <?php if($_POST['attendance_time_check']==1) { ?>  checked="checked" <?php } ?>></td>

        <td>
            <select name="booking_status">
                <option value="">Select</option>
                <option <?php echo (!empty($_POST['booking_status_check']) && !empty($_POST['booking_status']) && $_POST['booking_status'] == 'Y') ? 'selected="selected"' : '';?> value="Y">Approved</option>
                <option <?php echo (!empty($_POST['booking_status_check']) && !empty($_POST['booking_status']) && $_POST['booking_status'] == 'N') ? 'selected="selected"' : '';?> value="N">Disapproved</option>
                <option <?php echo (!empty($_POST['booking_status_check']) && !empty($_POST['booking_status']) && $_POST['booking_status'] == 'C') ? 'selected="selected"' : '';?> value="C">Cancelled</option>
            </select>
            <br/> <input type="checkbox" name="booking_status_check" value="1" <?php if($_POST['booking_status_check']==1) { ?>  checked="checked" <?php } ?>>
        </td>
        <td><input type="text" name="booking_status_user" style="width:90px;" value="<?php echo $_POST['booking_status_user']; ?>" > <br/> <input type="checkbox" name="booking_status_user_check" value="1" <?php if($_POST['booking_status_user_check']==1) { ?>  checked="checked" <?php } ?>></td>

        <td><input type="text" name="attendance_status" style="width:90px;" value="<?php echo $_POST['attendance_status']; ?>" > <br/> <input type="checkbox" name="attendance_status_check" value="1" <?php if($_POST['attendance_status_check']==1) { ?>  checked="checked" <?php } ?>></td>

		<td><input type="text" name="visit_status" style="width:90px;" value="<?php echo $_POST['visit_status']; ?>" > <br/> <input type="checkbox" name="visit_status_check" value="1" <?php if($_POST['visit_status_check']==1) { ?>  checked="checked" <?php } ?>></td>
		<td><input type="text" name="enquiry_type" style="width:90px;" value="<?php echo $_POST['enquiry_type']; ?>" > <br/> <input type="checkbox" name="enquiry_type_check" value="1" <?php if($_POST['enquiry_type_check']==1) { ?>  checked="checked" <?php } ?>></td>
		<td>
			<select class="input_cls" name="group_category" id="group_category" style="width:150px;"  >
			<option value="">Please select - &#1610;&#1585;&#1580;&#1610; &#1575;&#1582;&#1578;&#1610;&#1575;&#1585;</option>
			<optgroup label="----------------------------------------" > </optgroup>
			<option value="General Public" <?php if($cat=="General Public"){ ?> selected="selected"  <?php } ?>>&#1586;&#1610;&#1575;&#1585;&#1577; &#1593;&#1575;&#1605;&#1607;</option>
			<option value="General Public" <?php if($cat=="General Public"){ ?> selected="selected"  <?php } ?>>General Public</option>
			<optgroup label="----------------------------------------" > </optgroup>
			<option value="Government" <?php if($cat=="Government"){ ?> selected="selected"  <?php } ?>>&#1575;&#1604;&#1607;&#1610;&#1574;&#1575;&#1578; &#1575;&#1604;&#1581;&#1603;&#1608;&#1605;&#1610;&#1577;</option>
			<option value="Government" <?php if($cat=="Government"){ ?> selected="selected"  <?php } ?>>Government Entity</option>
			<optgroup label="----------------------------------------" > </optgroup>
			<option value="Embassy" <?php if($cat=="Embassy"){ ?> selected="selected"  <?php } ?>>&#1575;&#1604;&#1587;&#1601;&#1575;&#1585;&#1575;&#1578;</option>
			<option value="Embassy" <?php if($cat=="Embassy"){ ?> selected="selected"  <?php } ?>>Embassy</option>
			<optgroup label="----------------------------------------" > </optgroup>
			<option value="Tour Operator" <?php if($cat=="Tour Operator"){ ?> selected="selected"  <?php } ?>>&#1575;&#1604;&#1588;&#1585;&#1603;&#1575;&#1578; &#1575;&#1604;&#1587;&#1610;&#1575;&#1581;&#1610;&#1577;</option>
			<option value="Tour Operator" <?php if($cat=="Tour Operator"){ ?> selected="selected"  <?php } ?>>Tour Operator</option>
			<optgroup label="----------------------------------------" > </optgroup>
			<option value="Education" <?php if($cat=="Education"){ ?> selected="selected"  <?php } ?>>&#1575;&#1604;&#1605;&#1572;&#1587;&#1587;&#1575;&#1578; &#1575;&#1604;&#1578;&#1593;&#1604;&#1610;&#1605;&#1610;&#1577;</option>
			<option value="Education" <?php if($cat=="Education"){ ?> selected="selected"  <?php } ?>>Educational Institution</option>
			<optgroup label="----------------------------------------" > </optgroup>
			<option value="Hotel" <?php if($cat=="Hotel"){ ?> selected="selected"  <?php } ?>>&#1575;&#1604;&#1601;&#1606;&#1583;&#1602;</option>
			<option value="Hotel" <?php if($cat=="Hotel"){ ?> selected="selected"  <?php } ?>>Hotel</option>
			<optgroup label="----------------------------------------" > </optgroup>
			<option value="Corporate">&#1575;&#1604;&#1602;&#1591;&#1575;&#1593; &#1575;&#1604;&#1582;&#1575;&#1589;</option>
			<option value="Corporate">Corporate Sector</option>
			<optgroup label="----------------------------------------" > </optgroup>
			<option value="All" <?php if($cat=="All"){ ?> selected="selected"  <?php } ?>>&#1580;&#1605;&#1610;&#1593;</option>
			<option value="All" <?php if($cat=="All"){ ?> selected="selected"  <?php } ?>>All</option>
			</select><br/>
			<input type="checkbox" name="group_category_check" value="1" <?php if($_POST['group_category_check']==1) { ?>  checked="checked" <?php } ?>>  
		</td>
        <?php echo (isset($_POST['group_category_check']) && $_POST['group_category_check'] == 1 && $cat=="Education") ? '<td><input type="text" name="edu_grades" style="width:90px;" value="'.$_POST['edu_grades'].'" > <br/> <input type="checkbox" name="edu_grades_check" value="1" '.(($_POST['edu_grades_check']==1) ? 'checked="checked"' : '').' ></td>' : '';?>
		<td><input type="text" name="type_visit" style="width:90px;" value="<?php echo $_POST['type_visit']; ?>" > <br/> <input type="checkbox" name="type_visit_check" value="1" <?php if($_POST['type_visit_check']==1) { ?>  checked="checked" <?php } ?>></td>
		<td><input type="text" name="purpose" style="width:90px;" value="<?php echo $_POST['purpose']; ?>" > <br/> <input type="checkbox" name="purpose_check" value="1" <?php if($_POST['purpose_check']==1) { ?>  checked="checked" <?php } ?>></td>
		<td><input type="text" name="phone" style="width:90px;" value="<?php echo $_POST['phone']; ?>" > <br/> <input type="checkbox" name="phone_check" value="1" <?php if($_POST['phone_check']==1) { ?>  checked="checked" <?php } ?>></td>
		<td><input type="text" name="email" style="width:90px;" value="<?php echo $_POST['email']; ?>" > <br/> <input type="checkbox" name="email_check" value="1" <?php if($_POST['email_check']==1) { ?>  checked="checked" <?php } ?>></td>
	</tr>
</thead>

<?php if($cat1=="booking"){?>
<tr  class="nodrag"><th>No.</th><th>Organization Name</th><th>Total No. of Bookings</th><th>Total No. of Visitors</th></tr>
<?php }else{ ?>
<tr  class="nodrag"><th>Enquiry date</th><th>Enquiry Time</th><th>Visit Time</th><th>Date of visit</th><th>Reference No.</th><th>Company Username</th><th>Organization Name</th><th>Country</th><th>Contact name </th><th>Group size</th><th>Attendance Time</th><th>Booking Status</th><th>Booking Status Decision By</th><th>Accept/Reject</th><th>status</th><th>Type of Booking</th><th>Category</th>
    <?php echo (isset($_POST['group_category_check']) && $_POST['group_category_check'] == 1 && $cat=="Education") ? '<th>Grades</th>' : '';?>
    <th>Type of Visit</th><th>Purpose of Visit</th><th>Mobile No.</th><th>Email Address</th></tr>

<?php } ?>
<tr>
<?php

//Filter if  visit date checkbox is checked
if((isset($_POST['visit_date_check'])) && $_POST['visit_date_check']==1 ) {
    $where .=  " and (`purposed_date` BETWEEN '$fromdate' AND '$todate')";
}
//Filter if  enquiry date checkbox is checked
if((isset($_POST['enquiry_date_check'])) && $_POST['enquiry_date_check']==1 ){
    $where .=  " and (`submitted_date` BETWEEN '$fromdate' AND '$todate')";
} else if ((!isset($_POST['visit_date_check'])) && $_POST['visit_date_check'] != 1 ) {
	//Filter if  no date check is checked is checked
    if ($fromdate!="" &&  $todate!="") {
        $where .=  " and (`submitted_date` BETWEEN '$fromdate' AND '$todate')";
    }
}
 
 if((isset($_POST['visit_time_check'])) && $_POST['visit_time_check']==1 )

 {
	    $time = $_POST['time'];
	   
	    if($_POST["from_date"]!="" &&  $_POST["to_date"]!="")
	   {
	 
		 $where .=  "and (`purposed_date` BETWEEN '$fromdate' AND '$todate')";
	   }
	   $where .=  " and  (`time`='$time') "; 
	  
 }

 if((isset($_POST['enq_time_check'])) && $_POST['enq_time_check']==1 )

 {
     $enq_time_hour = $_POST['enq_time_hour'];
     $enq_time_minute = $_POST['enq_time_minute'];

	    //`submitted_date` BETWEEN '$fromdate' AND '$todate'
     if (!empty($enq_time_hour)) {
         $where .= " and  (DATE_FORMAT(submitted_date,'%H')='$enq_time_hour') ";
     }
     if (!empty($enq_time_minute)) {
         $where .= " and  (DATE_FORMAT(submitted_date,'%i')='$enq_time_minute') ";
     }
 }
 
 
 if((isset($_POST['reference_no_check'])) && $_POST['reference_no_check']==1 )

 {
	     $reference = $_POST['reference_no'];
		 $where .=  " and (`reference`='$reference')";
	     
	  
 }
 
 if((isset($_POST['username_check'])) && $_POST['username_check']==1 )

 {
	     $username = $_POST['user_name'];
		 $where .=  " and tours.`user_id`=site_users.`id` and site_users.`email`='$username'";
	     
	  
 }
 
 if((isset($_POST['org_name_check'])) && $_POST['org_name_check']==1 )

 {
	     $org_name = $_POST['org_name'];
		  if($_POST["from_date"]!="" &&  $_POST["to_date"]!="")
	   {
	 
		 $where .=  "and (`purposed_date` BETWEEN '$fromdate' AND '$todate')";
	   }
		 $where .=  " and (`group`='$org_name')";
	     
	  
 }
 
 if((isset($_POST['country_name_check'])) && $_POST['country_name_check']==1 )

 {
	     $country_name = $_POST['country_name'];
		  if($_POST["from_date"]!="" &&  $_POST["to_date"]!="")
	   {
	 
		 $where .=  "and (`purposed_date` BETWEEN '$fromdate' AND '$todate')";
	   }
		 $where .=  " and (`country`='$country_name')";
	     
	  
 }
 if((isset($_POST['contact_name_check'])) && $_POST['contact_name_check']==1 )

 {
	     $contact_name = $_POST['contact_name'];
		  if($_POST["from_date"]!="" &&  $_POST["to_date"]!="")
	   {
	 
		 $where .=  "and (`purposed_date` BETWEEN '$fromdate' AND '$todate')";
	   }
		 $where .=  " and (`contact_name`='$contact_name')";
	     
	  
 }
 if((isset($_POST['group_size_check'])) && $_POST['group_size_check']==1 )

 {
	     $group_size = $_POST['group_size'];
		  if($_POST["from_date"]!="" &&  $_POST["to_date"]!="")
	   {
	 
		 $where .=  "and (`purposed_date` BETWEEN '$fromdate' AND '$todate')";
	   }
		 $where .=  " and (`size`='$group_size')";
	     
	  
 }
  if((isset($_POST['attendance_time_check'])) && $_POST['attendance_time_check']==1 )

 {
	     $attendance_time = $_POST['attendance_time'];
		  if($_POST["from_date"]!="" &&  $_POST["to_date"]!="")
	   {
	 
		 $where .=  "and (`purposed_date` BETWEEN '$fromdate' AND '$todate')";
	   }
		 $where .=  " and (`arr_time` like '$attendance_time%')";
	     
	  
 }


 
 if((isset($_POST['attendance_status_check'])) && $_POST['attendance_status_check']==1 )

 {
	     $attendance_status = $_POST['attendance_status'];
		  if($_POST["from_date"]!="" &&  $_POST["to_date"]!="")
	   {
	 
		 $where .=  "and (`purposed_date` BETWEEN '$fromdate' AND '$todate')";
	   }
		 $where .=  " and (`attendance_status`='$attendance_status')";
	     
	  
 }

if((isset($_POST['booking_status_check'])) && $_POST['booking_status_check']==1 && !empty($_POST['booking_status']))
{
    $booking_status = $_POST['booking_status'];

    $where .=  " and (`status` = '$booking_status' AND (approved != '' OR user_id != 0))";
}

if((isset($_POST['booking_status_user_check'])) && $_POST['booking_status_user_check']==1 && !empty($_POST['booking_status_user']))
{
    $booking_status_user = trim($_POST['booking_status_user']);

    $where .=  " and (( status != 'C' AND `approved` LIKE '%$booking_status_user%' )OR ( status = 'C' AND user_id LIKE '%$booking_status_user%') )";
}

 if((isset($_POST['visit_status_check'])) && $_POST['visit_status_check']==1 )

 {
		 $visit_status = $_POST['visit_status'];
		 
		  if($_POST["from_date"]!="" &&  $_POST["to_date"]!="")
	   {
	 
		 $where .=  "and (`purposed_date` BETWEEN '$fromdate' AND '$todate')";
	   }
	    if(($visit_status=="Absent") || ($visit_status=="absent"))
		 {
			 
			 $where .=  " and (`visit_status` IS NULL)";
		 }
		 else
		 {
		 $where .=  " and (`visit_status`='$visit_status')";
		 }
	     
	  
 }
  if((isset($_POST['enquiry_type_check'])) && $_POST['enquiry_type_check']==1 )

 {
	     $enquiry_type = $_POST['enquiry_type'];
		 
		 if( strtolower($enquiry_type) == 'temporary pass' )
		 {
			$enquiry_type = 'fast track';
		 }
		 
		  if($_POST["from_date"]!="" &&  $_POST["to_date"]!="")
	   {
	 
		 $where .=  "and (`purposed_date` BETWEEN '$fromdate' AND '$todate')";
	   }
		 $where .=  " and (`enq_type`='$enquiry_type')";
	     
	  
 }
 
 if((isset($_POST['group_category_check'])) && $_POST['group_category_check']==1 )
 { 
      $cat = $_POST["group_category"];
      if($cat=="All")
	  {
		   $where .= " and (`group_cat` is not null or `group_cat` <> '')";
		   
	  }
	  else
	  {
	  $where .= " and group_cat='".$cat ."'";
	   
	  }
	  if($_POST["from_date"]!="" &&  $_POST["to_date"]!="")
	   {
		 $where .=  " and  (`purposed_date` BETWEEN '$fromdate' AND '$todate')";
	   }
	  
	  
 }

 if((isset($_POST['edu_grades_check'])) && $_POST['edu_grades_check']==1 )
 {
      $edu_grades = $_POST["edu_grades"];

	  $where .= " and grade LIKE '%".$edu_grades ."%' ";
 }

 
  if((isset($_POST['type_visit_check'])) && $_POST['type_visit_check']==1 )

 {
	     $type_visit = $_POST['type_visit'];
		  if($_POST["from_date"]!="" &&  $_POST["to_date"]!="")
	   {
	 
		 $where .=  "and (`purposed_date` BETWEEN '$fromdate' AND '$todate')";
	   }
		 $where .=  " and (`type_visit`='$type_visit')";
	     
	  
 }
 
  if((isset($_POST['phone_check'])) && $_POST['phone_check']==1 )

 {
	     $phone = $_POST['phone'];
		  if($_POST["from_date"]!="" &&  $_POST["to_date"]!="")
	   {
	 
		 $where .=  "and (`purposed_date` BETWEEN '$fromdate' AND '$todate')";
	   }
		 $where .=  " and (`contact_phone`='$phone')";
	     
	  
 }

  if((isset($_POST['purpose_check'])) && $_POST['purpose_check']==1 )

 {
	     $purpose = $_POST['purpose'];
		 $where .=  " and (`purpose` LIKE '%$purpose%')";
 }
  if((isset($_POST['email_check'])) && $_POST['email_check']==1 )

 {
	     $email = trim($_POST['email']);
		  if($_POST["from_date"]!="" &&  $_POST["to_date"]!="")
	   {
	 
		 $where .=  "and (`purposed_date` BETWEEN '$fromdate' AND '$todate')";
	   }
		 $where .=  " and (tours.`email`='$email')";
	     
	  
 }
 
 if((isset($_POST['username_check'])) && $_POST['username_check']==1 )

 {
	 $sql_size = "SELECT sum(`size`) as count_size FROM tours,site_users WHERE 1 $where order by purposed_date,time";
 }
 else
 {
 $sql_size = "SELECT sum(`size`) as count_size FROM tours WHERE 1 $where order by purposed_date,time";
 }
 
 if(isset($_POST['submit']))
 {
 $query_size = mysql_query($sql_size);
 }
 if ($res_size=mysql_fetch_object($query_size))
 {
  $countsize = $res_size->count_size; 
 }
 if($cat1=="guide")
 {
	  $sql_detail = "SELECT * FROM tours,culture_guide_email WHERE 1 $where order by purposed_date,time ";
 }else if($cat1=="booking"){
	  
	  $sql_detail = $booking_sql;
 }
 else if((isset($_POST['username_check'])) && $_POST['username_check']==1 )

 {
	  $sql_detail = "SELECT * ,site_users.`email`, site_users.`id`,tours.`email` AS e FROM tours,site_users WHERE 1 $where order by purposed_date,time ";
 }
 else
 {
    $sql_detail = "SELECT * FROM tours WHERE 1 $where order by purposed_date,time ";
 }
 /**
  * Un comment  this line to view the Query
  */
// print_r($sql_detail);
 /*if(isset($_POST['submit']))
 {*/
 $query_detail = mysql_query($sql_detail);
  $count = mysql_num_rows($query_detail);
// }
 $x=1;
 if($count>0)
 {
 while ($res1=mysql_fetch_object($query_detail))
 {
  if($x%2==0)
{
$class="#999";
}
else
{
$class="#ddd;";
}
  $count_size = $res1->size; 
    $tourid = $res1->id;
  $user_email="";
  $abaya_received="";

  $abaya_taken="";
  $visit_time="";
  $username=$res1->user_id;
  if($username!=""){
  	$sql_username="select * from site_users where id=$username limit 1";
	$sql_result=mysql_query($sql_username);
	$count_result=mysql_num_rows($sql_result);
	if($count_result==1){
		while($obj = mysql_fetch_object($sql_result)){
			$user_email=$obj->email;
		}
	}
  }
  if($tourid!=""){
  	$sql_attendance="select * from attendance where tour_id=$tourid limit 1";
	$sql_result_attendance=mysql_query($sql_attendance);
	$count_result_attendance=mysql_num_rows($sql_result_attendance);
	if($count_result_attendance==1){
		while($obj = mysql_fetch_object($sql_result_attendance)){
			$visit_time=$obj->arr_time;
		}
	}
  }
  if($tourid!=""){
  	$sql_abaya="select * from abaya where tour_id=$tourid limit 1";
	$sql_result_abaya=mysql_query($sql_abaya);
	$count_result_abaya=mysql_num_rows($sql_result_abaya);
	if($count_result_abaya==1){
		while($obj = mysql_fetch_object($sql_result_abaya)){
			$abaya_received=$obj->abaya_ret;
			$abaya_taken=$obj->abaya_rec;
			
		}
	}
  }
  
  $arr_time=$res1->arr_time;
  $status=$res1->attendance_status;
  $visit_status=$res1->visit_status;
  $enq_type=$res1->enq_type;
  
  $name = $res1->contact_name;
  $phone = $res1->contact_phone;
  $purpose = $res1->purpose;
  $group = $res1->group;
   $country = $res1->country;
   if((isset($_POST['username_check'])) && $_POST['username_check']==1 )

 {
	 $email = $res1->e;
 }
 else
 {
  $email = $res1->email;
 }
     $bus = $res1->bus;
     $type_visit = $res1->type_visit;
     $ref = $res1->reference;
     $enq_type = $res1->enq_type;
     $time = $res1->time;
     $date = date('d/m/Y', strtotime($res1->purposed_date));
     $date1 = explode("-",$date);
     //$date = $date1[2]."-".$date1[1]."-".$date1[0];
     /*$submitted_date = explode(" ",$res1->submitted_date);
     $date_visit = explode("-",$submitted_date[0]);*/
     $enq_date = date('d/m/Y', strtotime($res1->submitted_date));//$date_visit[2] . "/".$date_visit[1] . "/".$date_visit[0];
     $enq_time = date('H:i:s', strtotime($res1->submitted_date));;

	//$enq_date .= '  '.$res1->submitted_date[1];
	if($visit_status=="")
	{
		$visit_status = "Absent";
	}
  if($res1->group_cat=="Governmentarb")
   {
   $cat = "&#1575;&#1604;&#1607;&#1610;&#1574;&#1575;&#1578; &#1575;&#1604;&#1581;&#1603;&#1608;&#1605;&#1610;&#1577;";
   }else if ($res1->group_cat=="Embassyarb")
   {
   $cat = "&#1575;&#1604;&#1587;&#1601;&#1575;&#1585;&#1575;&#1578;";
   }
   else if ($res1->group_cat=="Travel Industryarb")
   {
   $cat = "&#1602;&#1591;&#1575;&#1593; &#1575;&#1604;&#1587;&#1610;&#1575;&#1581;&#1577;";
   }
   else if ($res1->group_cat=="Educationarb")
   {
   $cat = "&#1575;&#1604;&#1605;&#1572;&#1587;&#1587;&#1575;&#1578; &#1575;&#1604;&#1578;&#1593;&#1604;&#1610;&#1605;&#1610;&#1577;";
   }
   else if ($res1->group_cat=="Otherarb")
   {
   $cat = "&#1571;&#1582;&#1585;&#1609;";
   }
    else if ($res1->group_cat=="General Publicarb")
   {
   $cat = "&#1586;&#1610;&#1575;&#1585;&#1577; &#1593;&#1575;&#1605;&#1607;";
   }
    else if ($res1->group_cat=="Tour Operatorarb")
   {
   $cat = "&#1575;&#1604;&#1588;&#1585;&#1603;&#1575;&#1578; &#1575;&#1604;&#1587;&#1610;&#1575;&#1581;&#1610;&#1577;";
     }
	  else if ($res1->group_cat=="Hotelarb")
   {
   $cat = "&#1575;&#1604;&#1601;&#1606;&#1575;&#1583;&#1602;";
     }
	 else if ($res1->group_cat=="Corporatearb")
   {
   $cat = "&#1575;&#1604;&#1602;&#1591;&#1575;&#1593; &#1575;&#1604;&#1582;&#1575;&#1589;";
     }
   else
   {
   $cat=$res1->group_cat;
   }
     $grade = $res1->grade;
   if($enq_type=="Fast Track")
   {
	   $enq_type = "Temporary Pass";
   }
     $bookingStatus = '--';
     if ($res1->status == 'Y') {
         $bookingStatus = 'Approved';
     } else if ($res1->status == 'N' && empty($res1->approved)) {
         $bookingStatus = 'Not Approved';
     } else if ($res1->status == 'N' && !empty($res1->approved)) {
         $bookingStatus = 'Disapproved';
     } else if ($res1->status == 'C') {
         $bookingStatus = 'Cancelled';
     }
     $bookingStatusAuthor = '--';
     if ($res1->status == 'Y' && !empty($res1->approved)) {
         $bookingStatusAuthor = $res1->approved;
     } else if ($res1->status == 'N' && !empty($res1->approved)) {
         $bookingStatusAuthor = $res1->approved;
     } else if ($res1->status == 'C' && !empty($res1->user_id)) {
         $bookingStatusAuthor = 'TO'.$res1->user_id;
     }
     if($cat1=="booking"){
  	$content.="<tr><td style='background:".$class.";'>".$x ."&nbsp;</td><td style='background:".$class.";'>".$group ."&nbsp;</td><td style='background:".$class.";'>".$res1->total_bookings ."&nbsp;</td><td style='background:".$class.";'>".$res1->total_visitors ."&nbsp;</td></tr>";
  }else{
 
  $content.="<tr><td style='background:".$class.";'>".$enq_date ."&nbsp;</td><td style='background:".$class.";'>".$enq_time ."&nbsp;</td><td style='background:".$class.";'>".$time ."&nbsp;</td><td style='background:".$class.";'>".$date ."&nbsp;</td><td style='background:".$class.";'>".$ref ."&nbsp;</td><td style='background:".$class.";'>".$user_email ."&nbsp;</td><td style='background:".$class.";'>".$group ."&nbsp;</td><td style='background:".$class.";'>".$country ."&nbsp;</td><td style='background:".$class.";'>".$name ."&nbsp;</td><td style='background:".$class.";'>".$count_size ."&nbsp;</td><td style='background:".$class.";'>".$arr_time ."&nbsp;</td><td style='background:".$class.";'>".$bookingStatus ."&nbsp;</td><td style='background:".$class.";'>".$bookingStatusAuthor ."&nbsp;</td><td style='background:".$class.";'>".$status ."&nbsp;</td><td style='background:".$class.";'>".$visit_status ."&nbsp;</td><td style='background:".$class.";'>".$enq_type ."&nbsp;</td><td style='background:".$class.";'>".$cat ."&nbsp;</td>";
    $content.= ($cat=="Education") ? "<td style='background:".$class.";'>".$grade ."&nbsp;</td>" : '';
    $content.= "<td style='background:".$class.";'>".$type_visit ."&nbsp;</td><td style='background:".$class.";'>".$purpose ."&nbsp;</td><td style='background:".$class.";'>".$phone ."&nbsp;</td><td style='background:".$class.";'>".$email ."&nbsp;</td></tr>";
  }
 $x++;
 
 }
 }
echo $content;
?>

</tr> 
	
</table> 
<?php if($cat1!="booking"){ ?>
<div align="center" style="margin-top:10px;"><span style="font-size:16px; font-weight:bold;">Total Visitors=<?php echo $countsize; ?></span></div>
<?php } ?>


</form>
</div>
<?
do_footer();
?>