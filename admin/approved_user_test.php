<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/en/inc/conf.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/en/inc/mysql.lib.php');
$mydb=new connect;
ini_set('memory_limit', '512M');
set_time_limit(0);
error_reporting(E_ALL);
require_once dirname(__FILE__) . '/../libraries/PHPExcel/PHPExcel.php';
ini_set('display_errors', 1);
//PhpExcel Initialization
$objPHPExcel = new PHPExcel();
$objPHPExcel->setActiveSheetIndex(0);
$ActiveSheet = $objPHPExcel->getActiveSheet();
$contents = '';
$headerStyle = array(
    'font' => array(
        'color' => array(
            'rgb' => '13138D'
        ),
    ),
);
$rowStyle = array(
    'font' => array(
        'color' => array(
            'rgb' => '000000'
        ),
    ),
);

$output = array('Email Address', 'Mobile No.', 'Group size', 'Type of Visit', 'Arrival Time', 'Organization Name', 'Purpose of visit', 'Contact Name', 'Reference #', 'Date');
/**
 * Apply header row style - color
 */
$objPHPExcel->getDefaultStyle()->applyFromArray($headerStyle);
$first_letter = PHPExcel_Cell::stringFromColumnIndex(0);
$last_letter = PHPExcel_Cell::stringFromColumnIndex(count($output)-1);
$header_range = "{$first_letter}1:{$last_letter}1";
$objPHPExcel->getActiveSheet()->getStyle($header_range)->getFont()->setBold(true);

//Set width of coulmns
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
$objPHPExcel->getActiveSheet()->getDefaultColumnDimension()->setWidth(35);


$index = 1;
$objPHPExcel->getDefaultStyle()->applyFromArray($rowStyle);
$index++;
$objPHPExcel->getActiveSheet()->fromArray($output, NULL, "A$index");


/**
 * Query data from DB
 */
$status = !empty($_GET["status"]) ? $_GET["status"] : '';
if($status=="approved")
{
    $result=mysql_query("SELECT * FROM tours WHERE status='Y' and approved!=''");
}
else if($status=="disapproved")
{
    $result=mysql_query("SELECT * FROM tours WHERE status='N' and approved!=''");
}
else
{
    $result=mysql_query("SELECT * FROM tours ");
}
if ($result) {
    while($row=mysql_fetch_array($result)) {
        if($row["group_cat"]=="Governmentarb") {
            $cat = "الهيئات الحكومية";
        } else if ($row["group_cat"]=="Embassyarb") {
            $cat = "السفارات";
        } else if ($row["group_cat"]=="Travel Industryarb") {
            $cat = "&#1602;&#1591;&#1575;&#1593; &#1575;&#1604;&#1587;&#1610;&#1575;&#1581;&#1577;";
        } else if ($row["group_cat"]=="Educationarb") {
            $cat = "المؤسسات التعليمية";
        } else if ($row["group_cat"]=="Otherarb") {
            $cat = "&#1571;&#1582;&#1585;&#1609;";
        } else if ($row["group_cat"]=="General Publicarb") {
            $cat = "زيارة عامه";
        } else if ($row["group_cat"]=="Tour Operatorarb") {
            $cat = "الشركات السياحية";
        } else if ($row["group_cat"]=="Hotelarb") {
            $cat = "الفنادق";
        } else {
            $cat=$row["group_cat"];
        }
        $from_date1 = explode("-",$row['purposed_date']);
        $fromdate = $from_date1[2]."-".$from_date1[1]."-".$from_date1[0];

        $output = array($row['email'] , $row['phone'] , $row['size'], $cat, $row['time'],  $row['group'],  $row['purpose'],  $row['name'], $row['reference'], $fromdate);
        $objPHPExcel->getActiveSheet()->fromArray($output, NULL, "A$index");
        $index++;
    }
}

//Writing - Downloading
$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel , 'Excel5');
//$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="ApprovedBooking.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
$objWriter->save('php://output');
exit;


/*
$status = $_GET["status"];
if($status=="approved")
{
$result=mysql_query("SELECT * FROM tours WHERE status='Y' and approved!=''");
}
else if($status=="disapproved")
{
$result=mysql_query("SELECT * FROM tours WHERE status='N' and approved!=''");
}
else
{
	$result=mysql_query("SELECT * FROM tours ");
}
 
  $contents = '<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" /></head><body>
	<table align="center" class="menu_list_table" style="margin-top:15px;">
<tr  class="nodrag"><th>Email Address</th><th>Mobile No.</th><th>Group size</th><th>Type of Visit</th><th>Arrival Time</th><th>Organization Name</th><th>Purpose of visit</th><th>Contact Name</th><th>Reference #</th><th>Date</th> </tr>';
    while($row=mysql_fetch_array($result))
    {

   

	
	$from_date1 = explode("-",$row['purposed_date']);
$fromdate = $from_date1[2]."-".$from_date1[1]."-".$from_date1[0];
	$contents.= "<tr><td>".$row['email'] ."</td><td>".$row['phone'] ."</td><td>".$row['size']."</td><td>".$cat."</td><td>".$row['time']."</td><td>". $row['group']."</td><td>". $row['purpose']."</td><td>". $row['name']."</td><td>".$row['reference']."</td><td>".$fromdate."</td></tr>";
	}
	$contents.="</table>";
	$fname=$status.".xls";
	header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Content-Type: application/force-download");
	header('Content-type: text/html; charset=utf-8');
	header('Content-type: application/ms-excel');
    header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");
	header("Content-Disposition: attachment;filename=".$fname);
    header("Content-Transfer-Encoding: binary ");
	

print $contents; 
exit;*/