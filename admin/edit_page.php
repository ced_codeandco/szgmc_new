<?php
include_once ('includes/commons.php');
$request_url=$_SERVER['REQUEST_URI'];


if(preg_match('/\/admin\/edit_page.php/',$request_url)  && isset($_POST['p_title']) && $_POST['p_title']!='' && isset($_POST['page_id']) && $_POST['page_id']!='' )
{
$p_id=trim($_POST['page_id']);
$title=mysql_real_escape_string(trim($_POST['p_title']));
$m_title=mysql_real_escape_string(trim($_POST['m_title']));
$m_keywords=mysql_real_escape_string(trim($_POST['m_keywords']));
$m_desc=mysql_real_escape_string(trim($_POST['m_desc']));
$p_url=mysql_real_escape_string(trim($_POST['p_url']));
$new_page_visibility=trim($_POST['new_page_visibility']);
$new_page_live=trim($_POST['new_page_live']);
$new_page_php=trim($_POST['new_page_php']);
$img_flash_name=trim($_POST['header_img_flash']);
$bottom_image_flash=trim($_POST['bottom_image_flash']);

$p_content=trim($_POST['page_content']);

$p_content=preg_replace('/firebugversion="1.5.4" id="_firebugConsole" style="display: none;"/'," ",$p_content);
$p_content=preg_replace('/<br \/>/',"",$p_content);
// issue with CK editor and firebug. tha above text appending to the ck editor repeatedly
$p_content=stripslashes($p_content);
$p_content=addslashes($p_content);

$q="update pages set php_content='$new_page_php',title='$title',meta_title='$m_title',meta_desc='$m_desc',meta_keywords='$m_keywords',url='$p_url',visibility='$new_page_visibility',live='$new_page_live',header_image_flash='$img_flash_name',updated_by='$_SESSION[admin_id]',updated_date=CURRENT_TIMESTAMP,content='$p_content',bottom_image='$bottom_image_flash' where p_id='$p_id' limit 1";


$mydb->query($q);
$today = $stat=  date('d-m-Y H:i:s');
$stat_sql="update site_options set option_value='$today' where option_name ='last_updated'";
$mydb->query($stat_sql);
header("location: /admin/edit_page.php?msg=success&page_id=".$p_id);	
exit(0);
}

do_header();
?>

<?php

if(isset($_REQUEST['page_id']) && $_REQUEST['page_id']!='')
{
$p_id=$_REQUEST['page_id'];
$q="select * from pages where p_id='$p_id'";
$mydb->query($q);
$rs_obj=$mydb->loadResult();
$rs_obj=$rs_obj[0];
}
?>
<div style="float:left;">

<?php
if($rs_obj)
{
?>
<form action="/admin/edit_page.php" name="page_frm_1" id="page_frm_1" method="post"   >
<div style="float:left;">
<input type="hidden" name="page_id" value="<?php echo $p_id; ?>"/>	
<textarea cols="80" id="page_content" name="page_content" rows="10" dir="rtl">
<?php 
$rs_obj->content=preg_replace('/firebugversion="1.5.4" id="_firebugConsole" style="display: none;"/'," ",$rs_obj->content);
// issue with CK editor and firebug. tha above text appending to the ck editor repeatedly
echo $rs_obj->content; 

?>
</textarea>

			<script type="text/javascript">
			//<![CDATA[
				CKEDITOR.replace( 'page_content',
					{
					fullPage : false,
			width: '700',
			height: '400',
			filebrowserBrowseUrl : 'js/ckfinder/ckfinder.html',
			filebrowserImageBrowseUrl : 'js/ckfinder/ckfinder.html?type=Images',
			filebrowserFlashBrowseUrl : 'js/ckfinder/ckfinder.html?type=Flash',
			filebrowserImageUploadUrl : 'js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
			filebrowserFlashUploadUrl : 'js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
			filebrowserWindowWidth : '1000',
			filebrowserWindowHeight : '700'					
					});

				CKEDITOR.config.protectedSource.push( /<\?[\s\S]*?\?>/g );  //PHP code
				CKEDITOR.ContentLangDirection = 'rtl' ;

				CKEDITOR.config.toolbar_Full =
[
	['Source','-','NewPage','Preview','-','Templates'],
    ['Cut','Copy','Paste','PasteText','PasteFromWord','-','Print', 'SpellChecker', 'Scayt'],
    ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
    ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField'],
    '/',
    ['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
    ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote','CreateDiv'],
    ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
    ['Link','Unlink','Anchor'],
    ['Image','Flash','Table','HorizontalRule','Smiley','SpecialChar','PageBreak'],
    '/',
    ['Styles','Format','Font','FontSize'],
    ['TextColor','BGColor'],
    ['Maximize', 'ShowBlocks','-','About']
];
			</script>			

</div>




<script type="text/javascript">
  $(document).ready(function(){
    $("#page_frm_1").validate({
			rules: {
				p_url:
				{ 
				required: true,				
				minlength: 4
				}
				
			},messages: {
				p_url:{
				required: "This field is required",
				minlength: "minimum 5 characters long"
				}
				}

		});
  });
  </script>

<div style="float:left;">

<input type="hidden" name="page_id" value="<?php echo $p_id; ?>"/>
<table class="menu_list_table">
<tr><td>Name[For reference only]</td><td><input class="input-text" type="text" name="p_title" value="<?php echo $rs_obj->title; ?>"/></td></tr>
<tr><td>Meta Title[SEO]</td><td><input class="input-text" type="text" name="m_title" value="<?php echo $rs_obj->meta_title; ?>"/></td></tr>
<tr><td>Meta Keywords[SEO]</td><td><input class="input-text" type="text" name="m_keywords" value="<?php echo $rs_obj->meta_keywords; ?>"/></td></tr>
<tr><td>Meta Description[SEO]</td><td><input class="input-text" type="text" name="m_desc" value="<?php echo $rs_obj->meta_desc; ?>"/></td></tr>
<tr><td>Page URL</td><td><input class="input-text" type="text" name="p_url" value="<?php echo $rs_obj->url; ?>"/></td></tr>
<tr><td>Header(image/flash) </td><td>
<select name="header_img_flash">
<option value=''></option>
<?php
$array_files = dirImages('../flash_and_images/');
foreach ($array_files as $key => $image) // Display Images
{
echo "<option value='".$image['file']."' ";
echo ($rs_obj->header_image_flash==$image['file'])?'SELECTED':'';
echo">$image[file]</option>";
} 
?>
</select>
</td></tr>


<tr><td>Visibility</td><td><select name="new_page_visibility">
<option value="A" <?php echo ($rs_obj->visibility=='A')?'SELECTED':'' ?>>Admin</option>
<option value="P" <?php echo ($rs_obj->visibility=='P')?'SELECTED':'' ?>>Public</option>
<option value="B" <?php echo ($rs_obj->visibility=='B')?'SELECTED':'' ?>>Both</option>
</select></td></tr>
<tr><td>Make this item</td><td>
<select name="new_page_live">
<option value="Y" <?php echo ($rs_obj->live=='Y')?'SELECTED':'' ?>>Live</option>
<option value="N" <?php echo ($rs_obj->live=='N')?'SELECTED':'' ?>>Down</option>
</select>
</td></tr>
<tr><td>PHP</td><td><select name="new_page_php">
<option value="Y" <?php echo ($rs_obj->php_content=='Y')?'SELECTED':'' ?>>Yes</option>
<option value="N" <?php echo ($rs_obj->php_content=='N')?'SELECTED':'' ?>>No</option>
</select></td></tr>

<tr><td>Bottom flash/image</td><td>
<select name="bottom_image_flash">
<option value=''></option>
<?php
$array_files = dirImages('../banners/');
foreach ($array_files as $key => $image) // Display Images
{
echo "<option value='".$image['file']."' ";
echo ($rs_obj->bottom_image==$image['file'])?'SELECTED':'';
echo">$image[file]</option>";
} 
?>
</select></td></tr>
</table>
</div>
<div class="clear"></div>
<div style="clear:both; width:95%; text-align:center; margin-top:40px;">
<div id="info_text" style="float:left;padding:0px 5px 0px 5px; width:100px;"></div>
<input type="submit" value="Save" class="btn_admin"/>
</div>

</form>	

	

<div class="clear"></div>
<?php
}else
{
echo "Page not found";
}
?>
</div>

<?php

do_footer();
?>