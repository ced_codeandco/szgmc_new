<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 1/7/2016
 * Time: 5:46 PM
 */
include_once($_SERVER['DOCUMENT_ROOT'].'/inc/conf.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/inc/mysql.lib.php');
require_once dirname(__FILE__) . '/../libraries/Excel/php-export-data.class.php';
error_reporting(0);
$mydb=new connect;
mysql_query("SET NAMES utf8");

if(!empty($_GET["from_date"]))
{
    $from_date=$_GET["from_date"];
    $from_date1 = explode("-",$from_date);
    $fromdate = $from_date1[2]."-".$from_date1[1]."-".$from_date1[0].' 00:00:00';

}

if(!empty($_GET["to_date"]))
{
    $to_date=$_GET["to_date"];
    $to_date1 = explode("-",$to_date);
    $todate = $to_date1[2]."-".$to_date1[1]."-".$to_date1[0].' 23:59:59';
}


if (!empty($fromdate) && !empty($todate)) {

    $where = "";

    if (!empty($fromdate)) {
        $where .= !empty($fromdate) ? " created_date_time >= '".$fromdate."' " : '';
    }
    if (!empty($todate)) {
        $where .= !empty($where) ? ' AND ' : '';
        $where .= !empty($todate) ? " created_date_time <= '".$todate."' " : '';
    }

    $sql_detail = " SELECT  * FROM quran_recitation_form ".(!empty($where) ? ' WHERE '. $where : '')." order by created_date_time DESC ";
    $query_detail = mysql_query($sql_detail);
}

//die($sql_detail);

/*echo "<br /><br />";print_r($result_list1);
echo "<br /><br />";print_r($result_list2);
echo "<br /><br />";print_r($result_list3);
echo "<br /><br />";print_r($result_list4);
*/



$excel = new ExportDataExcel('browser', "quran_recitation_form.xls");
$outputData = array("SI","Reference number","Name","Age","Gender","Nationality","City","Contact No.","Email","Education level","Memorization Level","Participated Previously Courses?", "Time of Registration");

$excel->initialize();
$excel->addRow($outputData);

$x = 1;
if (!empty($fromdate) && !empty($todate)) {
    //die("Bottom");

    while ($data_row = mysql_fetch_object($query_detail)) {
        $date_index = strtotime(date('Y-m-d', $iDateFrom));
        $outputData = array($x);
        $outputData[] = 'QR-'.str_pad($data_row->id, 4, '0', STR_PAD_LEFT);
        $outputData[] = $data_row->name;
        $outputData[] = $data_row->age;
        $outputData[] = $data_row->gender;
        $outputData[] = $data_row->nationality;
        $outputData[] = $data_row->city_name;
        $outputData[] = $data_row->contact_no;
        $outputData[] = $data_row->email;
        $outputData[] = $data_row->education_level;
        $outputData[] = $data_row->memorization_level;
        $outputData[] = $data_row->already_participated;
        $outputData[] = date('d-m-Y', strtotime($data_row->created_date_time));

        //echo "<br /><br />";print_r($outputData);
        //$outputData = array_map("clean_output", $outputData);
        $excel->addRow($outputData);
        $x++;
    }
}

$excel->finalize();