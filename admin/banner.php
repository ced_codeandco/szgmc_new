<?php
include_once ('includes/commons.php');
do_header();
?>
		<script type="text/javascript" charset="utf-8">			
				var oTable;
				$(document).ready(function() {
				oTable=$('#my_table').dataTable( {
					"bProcessing": true,					
					"bJQueryUI": true,
					"sPaginationType": "full_numbers",
					"iDisplayLength": 5,
					"bServerSide": true,					 
					/*"sAjaxSource": 'src/common/json/json_source_customers.php',*/
					"sAjaxSource": 'src/common/json/server_side_json_banner.php',
					"aaSorting": [[0,'desc']],
					"aoColumns": [						
						{ bSortable: true, sWidth: '40px' },
						{ bSortable: true, sWidth: '20px' },
						{ bSortable: true, sWidth: '20px' },
						{ bSortable: true, sWidth: '80px' },
						{ bSortable: true, sWidth: '80px' },
						{ bSortable: true, sWidth: '80px' }
					]
					
				} );
				
			} );			
			function make_active(n_id){
				check=confirm("Are you sure. You want to make this banner live on the homepage?");
				news_change_status(n_id,'Y');
			}
			function make_deactive(n_id){
				check=confirm("Are you sure. You want to hide this banner from the homepage?");
				news_change_status(n_id,'N');
			}
			function news_change_status(n_id,status_txt)
			{
			
			  $.ajax({
			   type: "POST",
			   url: "src/common/change_status_banner.php",
			   data: "banner_id="+n_id+",&status="+status_txt,
			   success: function(result){
				   result=trim(result);
					if(result=='success')
					{
			
					}else if(result=='already live')
					{
						alert("You can make only one banner live at a time.")
					}else
					{
						alert("Error occured")
			
					}
			   },
			   complete: function(){
			   oTable.fnReloadAjax(oTable.fnSettings());
			   },
			   error: function(){
						alert("Server error occured. Please try later");
			   }
			   
			 });
			
			}	
			//Function added on 18/11/2013 by Irfan Ahmed

function delBanner(n_id)
			{
			
				if(confirm("Are you sure, You want to delete this banner?")){

					  $.ajax({
					   type: "POST",
					   url: "src/common/del_banner.php",
					   data: "banner_id="+n_id,
					   success: function(result){
						   result=trim(result);
							if(result=='success')
							{
					
							}
							else
							{
								alert("Error occured")
					
							}
					   },
					   complete: function(){
					   oTable.fnReloadAjax(oTable.fnSettings());
					   },
					   error: function(){
								alert("Server error occured. Please try later");
					   }
					   
					 });
				}
			}
//Ending code snippet added on 18/11/2013			

function trim(str) {
	return ltrim(rtrim(str));
}
function ltrim(str) { 
	for(var k = 0; k < str.length && isWhitespace(str.charAt(k)); k++);
	return str.substring(k, str.length);
}
function rtrim(str) {
	for(var j=str.length-1; j>=0 && isWhitespace(str.charAt(j)) ; j--) ;
	return str.substring(0,j+1);
}
function isWhitespace(charToCheck) {
	var whitespaceChars = " \t\n\r\f";
	return (whitespaceChars.indexOf(charToCheck) != -1);
}	
		</script>
<h2 style="padding-left:20px;">Banner</h2>
<table width="100%"  cellpadding="3" cellspacing="3" align="center">
<tr><td align="center" valign="top" >
<div id="container">

<table cellpadding="0" cellspacing="0" border="0" class="display" id="my_table">
	<thead>
		<tr>
			<th>Banner Id</th>
            <th>Live</th>
            <th>Banner Homepage Image</th>
            <th>Banner Popup Image</th>
			<th>Date</th>
			<th>Actions</th>
						
		</tr>
	</thead>
	<tbody>
	</tbody>
</table>	
</div>
</td></tr>
</table>
<?php
do_footer();
?>


