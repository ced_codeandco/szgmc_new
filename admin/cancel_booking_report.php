<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/en/inc/conf.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/en/inc/mysql.lib.php');
$mydb=new connect;

$result=mysql_query("SELECT * FROM tours WHERE status='C' ");
/** Error reporting */
error_reporting(E_ALL);

/** Include PHPExcel */
require_once dirname(__FILE__) . '/../libraries/PHPExcel/PHPExcel.php';
//PhpExcel Initialization
$objPHPExcel = new PHPExcel();
$objPHPExcel->setActiveSheetIndex(0);
$ActiveSheet = $objPHPExcel->getActiveSheet();
$contents = '';
$headerStyle = array(
    'font' => array(
        'color' => array(
            'rgb' => '13138D'
        ),
    ),
);
$rowStyle = array(
    'font' => array(
        'color' => array(
            'rgb' => '000000'
        ),
    ),
);
//$ActiveSheet->getStyle('A1')->applyFromArray($fontConfig );
$objPHPExcel->getDefaultStyle()->applyFromArray($headerStyle);
$output = array('Ref No.', 'Date of Enquiry', 'Name', 'Group Name', 'Email', 'Visit date', 'Culture Guide Status', 'Purpose of visit', 'Approved By', 'Cancelled By', 'Cancel Time');
$index = 1;
$objPHPExcel->getActiveSheet()->fromArray($output, NULL, "A$index");
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
$objPHPExcel->getActiveSheet()->getDefaultColumnDimension()->setWidth(35);


$first_letter = PHPExcel_Cell::stringFromColumnIndex(0);
$last_letter = PHPExcel_Cell::stringFromColumnIndex(count($output)-1);
$header_range = "{$first_letter}1:{$last_letter}1";
$objPHPExcel->getActiveSheet()->getStyle($header_range)->getFont()->setBold(true);



$objPHPExcel->getDefaultStyle()->applyFromArray($rowStyle);
$index++;
$objPHPExcel->getActiveSheet()->fromArray($output, NULL, "A$index");
if ($result) {
    while ($row = mysql_fetch_array($result)) {
        if ($row["group_cat"] == "Governmentarb") {
            $cat = "الهيئات الحكومية";
        } else if ($row["group_cat"] == "Embassyarb") {
            $cat = "السفارات";
        } else if ($row["group_cat"] == "Travel Industryarb") {
            $cat = "&#1602;&#1591;&#1575;&#1593; &#1575;&#1604;&#1587;&#1610;&#1575;&#1581;&#1577;";
        } else if ($row["group_cat"] == "Educationarb") {
            $cat = "المؤسسات التعليمية";
        } else if ($row["group_cat"] == "Otherarb") {
            $cat = "&#1571;&#1582;&#1585;&#1609;";
        } else if ($row["group_cat"] == "General Publicarb") {
            $cat = "زيارة عامه";
        } else if ($row["group_cat"] == "Tour Operatorarb") {
            $cat = "الشركات السياحية";
        } else if ($row["group_cat"] == "Hotelarb") {
            $cat = "الفنادق";
        } else {
            $cat = $row["group_cat"];
        }
        $from_date1 = explode("-", $row['purposed_date']);
        $fromdate = $from_date1[2] . "-" . $from_date1[1] . "-" . $from_date1[0];
        $output = array($row['reference'], $row['submitted_date'], $row['name'], $row['group'], $row['email'], $fromdate, $row['edited'], $row['purpose'], $row['approved'], "TO" . $row['user_id'], $row['cancel_time']);
        $objPHPExcel->getActiveSheet()->fromArray($output, NULL, "A$index");
        $index++;
    }
}

//Writing
$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel , 'Excel5');
//$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="ExportReport.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0
$objWriter->save('php://output');
exit;

/*while($row=mysql_fetch_array($result))
{
    if($row["group_cat"]=="Governmentarb")
    {
        $cat = "الهيئات الحكومية";
    }else if ($row["group_cat"]=="Embassyarb")
    {
        $cat = "السفارات";
    }
    else if ($row["group_cat"]=="Travel Industryarb")
    {
        $cat = "&#1602;&#1591;&#1575;&#1593; &#1575;&#1604;&#1587;&#1610;&#1575;&#1581;&#1577;";
    }
    else if ($row["group_cat"]=="Educationarb")
    {
        $cat = "المؤسسات التعليمية";
    }
    else if ($row["group_cat"]=="Otherarb")
    {
        $cat = "&#1571;&#1582;&#1585;&#1609;";
    }
    else if ($row["group_cat"]=="General Publicarb")
    {
        $cat = "زيارة عامه";
    }
    else if ($row["group_cat"]=="Tour Operatorarb")
    {
        $cat = "الشركات السياحية";
    }
    else if ($row["group_cat"]=="Hotelarb")
    {
        $cat = "الفنادق";
    }
    else
    {
        $cat=$row["group_cat"];
    }



    $from_date1 = explode("-",$row['purposed_date']);
    $fromdate = $from_date1[2]."-".$from_date1[1]."-".$from_date1[0];
    $contents.= "<tr><td>".$row['reference']."</td> <td>".$row['submitted_date']."</td> <td>".$row['name']."</td> <td>".$row['group']."</td> <td>".$row['email']."</td> <td>".$fromdate."</td> <td>".$row['edited']."</td> <td>".$row['purpose']."</td><td>".$row['approved']."</td> <td>TO".$row['user_id']."</td> <td>".$row['cancel_time']."</td></tr>";
}*/
//$contents.="</table>";




//Color Applying



/*die();
$fname="cancelled_bookings.xls";
header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header('Content-type: text/html; charset=utf-8');
header('Content-type: application/ms-excel');
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Disposition: attachment;filename=".$fname);
header("Content-Transfer-Encoding: binary ");


print $contents;*/


