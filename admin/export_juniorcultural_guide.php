<?php
/**
 * Created by PhpStorm.
 * User: Anas
 * Date: 5/11/2015
 * Time: 3:43 PM
 */
include '../includes/database.php';
$db = new MyDatabase();
$conn = $db->getConnection();

/**
 * Filter vip lists
 */
$where = '';
if (!empty($_GET['from_date'])) {
    $dateParts = explode('-', $_GET['from_date']);
    if (!empty($dateParts[2]) && !empty($dateParts[1]) && !empty($dateParts[0])) {
        $from_date = $dateParts[2] . '-' . $dateParts[1] . '-' . $dateParts[0];
    }
    $where .= !empty($from_date) ? " created_date_time >= '".$from_date."' " : '';
}
if (!empty($_GET['to_date'])) {
    $dateParts = explode('-', $_GET['to_date']);
    if (!empty($dateParts[2]) && !empty($dateParts[1]) && !empty($dateParts[0])) {
        $to_date = $dateParts[2] . '-' . $dateParts[1] . '-' . $dateParts[0];
    }

    $where .= !empty($where) ? ' AND ' : '';
    $where .= !empty($to_date) ? " created_date_time <= '".$to_date."' " : '';
}

    $sql_detail = " SELECT  * FROM junior_cultural_guide ".(!empty($where) ? ' WHERE '. $where : '')." order by created_date_time DESC ";
    $query_detail = mysql_query($sql_detail, $conn);
    $count        = mysql_num_rows($query_detail);
    $contents     = '<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" /></head><body>
	<table align="center" class="menu_list_table" style="margin-top:15px;">';
    $contents .= '<tr><th>SI Number</th><th>Reference number</th><th>Name</th><th>Date of birth</th><th>Gender</th><th>School</th><th>Grade</th><th>Percentage</th><th>Parent contact number1</th><th>Parent contact number2</th><th>Email</th><th>Transportation</th><th>Residential Area</th><th>Interests</th><th>Added date</th><tr>';

    $x = 1;
    while ($visitor = mysql_fetch_object($query_detail)) {
        $contents .= '<tr></tr><td>'.$x.'</td><td>JCG'.str_pad($visitor->id, 4, '0', STR_PAD_LEFT).'</td><td>'.$visitor->fname.'</td><td>'.$visitor->dob.'</td><td>'.$visitor->gender.'</td>
                      <td>'.$visitor->school_name.'</td><td>'.$visitor->grade.'</td><td>'.$visitor->percentage.'</td>
                      <td>'.$visitor->parent_mobile_1.'</td><td>'.$visitor->parent_mobile_2.'</td><td>'.$visitor->email.'</td>
                      <td>'.$visitor->transportation_required.'</td><td>'.$visitor->residential_area.'</td><td>'.$visitor->interests.'</td>
                      <td>'.date('d-m-Y', strtotime($visitor->created_date_time)).'</td></tr>';
        $x++;
    }

    $contents .= "<tr><td colspan='15' style='font-size:16px;font-weight:bold;' align='center'> Number of Applications= " . ($x-1) . "</td></tr>";
    $contents .= "</table>";

header("Pragma: public");
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Content-Type: application/force-download");
header('Content-type: text/html; charset=utf-8');
header('Content-type: application/ms-excel');
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");
header("Content-Disposition: attachment;filename=report.xls");
header("Content-Transfer-Encoding: binary ");


print $contents;
exit();