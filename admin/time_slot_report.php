﻿<?php
ob_start();
include_once ('includes/commons.php');;
do_header();
error_reporting(1);
?>
<script type="text/javascript">
$(function() {
$("#from_date").datepicker({dateFormat: 'dd-mm-yy'});
$("#to_date").datepicker({dateFormat: 'dd-mm-yy'});
});
function expXLS(){
    window.location.href="export_timeslot.php?"+$('#filterForm').serialize();
    return false
}
</script>
<style>
.whiteBackground { background-color: #fff; }
.grayBackground { background-color: #ccc; }

</style>
<?php
if(isset($_POST["from_date"]))
{
    $from_date=$_POST["from_date"];
    $from_date1 = explode("-",$from_date);
    $fromdate = $from_date1[2]."-".$from_date1[1]."-".$from_date1[0].' 00:00:00';

}
else
{
    $from_date=date("d-m-Y");
    $from_date = date('d-m-Y', strtotime($from_date .' -6 day'));
    $from_date1 = explode("-",$from_date);
    $fromdate = $from_date1[2]."-".$from_date1[1]."-".$from_date1[0];
}
if(isset($_POST["to_date"]))
{
    $to_date=$_POST["to_date"];
    $to_date1 = explode("-",$to_date);
    $todate = $to_date1[2]."-".$to_date1[1]."-".$to_date1[0].' 23:59:59';
}
else
{
    $todate=date("Y-m-d 23:59:59");
    $to_date=date("d-m-Y");
}
?>
<script type="text/javascript" src="/en/admin/js/common.js"></script>

<?php


$cat1 = $_POST["cat"];
$field = 'submitted_date';
if (!empty($fromdate) && !empty($todate)) {
    $time_group_start1 = '09:00:00';
    $time_group_end1 = '16:00:00';
    $time_group_start2 = '16:00:00';
    $time_group_end2 = '22:00:00';

    $field = 'submitted_date';

    $to_report_sql1 = "SELECT COUNT(*) as booking_count, submitted_date, DATE_FORMAT(submitted_date, '%Y-%m-%d') AS date_index FROM `tours` WHERE
        `group_cat` = 'Tour Operator'
        AND submitted_date BETWEEN '$fromdate' AND '$todate'
        AND DATE_FORMAT(submitted_date,'%H') BETWEEN '$time_group_start1' AND '$time_group_end1'
        GROUP BY date_index
        ORDER BY `submitted_date` ASC";
    $result_list1 = get_timeslot_array($to_report_sql1);

    $to_report_sql2 = "SELECT COUNT(*) as booking_count, submitted_date, DATE_FORMAT(submitted_date, '%Y-%m-%d') AS date_index FROM `tours` WHERE
        `group_cat` != 'Tour Operator'
        AND submitted_date BETWEEN '$fromdate' AND '$todate'
        AND DATE_FORMAT(submitted_date,'%H') BETWEEN '$time_group_start1' AND '$time_group_end1'
        GROUP BY date_index
        ORDER BY `submitted_date` ASC";
    $result_list2 = get_timeslot_array($to_report_sql2);

    $to_report_sql3 = "SELECT COUNT(*) as booking_count, submitted_date, DATE_FORMAT(submitted_date, '%Y-%m-%d') AS date_index FROM `tours` WHERE
        `group_cat` = 'Tour Operator'
        AND submitted_date BETWEEN '$fromdate' AND '$todate'
        AND DATE_FORMAT(submitted_date,'%H') BETWEEN '$time_group_start2' AND '$time_group_end2'
        GROUP BY date_index
        ORDER BY `submitted_date` ASC";
    $result_list3 = get_timeslot_array($to_report_sql3);

    $to_report_sql4 = "SELECT COUNT(*) as booking_count, submitted_date, DATE_FORMAT(submitted_date, '%Y-%m-%d') AS date_index FROM `tours` WHERE
        `group_cat` != 'Tour Operator'
        AND submitted_date BETWEEN '$fromdate' AND '$todate'
        AND DATE_FORMAT(submitted_date,'%H') BETWEEN '$time_group_start2' AND '$time_group_end2'
        GROUP BY date_index
        ORDER BY `submitted_date` ASC";
    $result_list4 = get_timeslot_array($to_report_sql4);
}
//print_r($result_list1);
function get_timeslot_array($sql) {
//echo "<br />". $sql;

    $query_sql = mysql_query($sql);


    $result_array = array();
    if ($query_sql) {

        while ($result = mysql_fetch_object($query_sql)) {

            $result_array[strtotime($result->date_index)] = $result;
        }
        //die("<br /><br />here");
    }

    return $result_array;
}
?>

<h1 style="margin-left:25px;">Enquiry Time Slot Report </h1>
<div id='toursPrintDiv' style='clear:both;'>
    <form  action="<?php echo $_SERVER['PHP_SELF']?>" method="post" id="filterForm"    >
        <table align="center" style="margin-top:15px;"  >
            <tr>
                <td>From <br/><input type="text" id="from_date" name="from_date" style="width:80px;" value="<?php echo $from_date;?>"/> </td>
                <td>To<br/><input type="text" id="to_date" name="to_date" style="width:80px;" value="<?php echo $to_date;?>"/> </td>
                <td>
                    &nbsp;<br/>
                    <input type="submit" value="Submit" name="submit">
                </td>
            </tr>
        </table>
    </form>


<?php
if (!empty($fromdate) && !empty($todate))
{
    echo $cat1;
    ?>
    <div style="width:150px; margin:0 auto; float:right">
        <div align="center" style="width:50px; float:left;"><a href='#' onclick='printFilming("toursPrintDiv"); return false;'><img src='images/print.jpg' alt='Print'/></a></div>
        <div align="center" style="width:50px; float:left;"><a href="javascript:expXLS();"><img src="images/excel_icon.jpg" border="0" style="width:30px" align="middle"></a></div>
    </div>
    <table align="center" class="menu_list_table" style="margin-top:15px;" width="97%">

        <tr  class="nodrag"><th>No.</th><th>Date</th><th>Number of Booking requests 9am – 4pm (TO Category)</th><th>Number of Booking request 9am – 4pm (Others)</th><th>Number of Booking requests 4pm – 10pm (TO Category)</th><th>Number of Booking requests 4pm – 10pm (Others)</th></tr>

        <tr>
            <?php
            $x = 1;
            if (!empty($fromdate) && !empty($todate)) {
                $iDateFrom = strtotime($fromdate);
                $iDateTo = strtotime($todate);
                while ($iDateTo>=$iDateFrom) {
                    $date_index = strtotime(date('Y-m-d', $iDateFrom));
                    $class = ($x%2==0) ? "#999" : "#ddd;";
                    echo "<tr>";
                    echo "<td style='background:$class; text-align: center;'>$x&nbsp;</td>";

                    echo "<td style='background:$class; text-align: center;'>";
                    echo date('d-m-Y', $date_index);
                    echo "&nbsp;</td>";

                    echo "<td style='background:$class; text-align: center;'>";
                    echo  !empty($result_list1[$date_index]->booking_count) ? $result_list1[$date_index]->booking_count : 0;
                    echo "&nbsp;</td>";

                    echo "<td style='background:$class; text-align: center;'>";
                    echo  !empty($result_list2[$date_index]->booking_count) ? $result_list2[$date_index]->booking_count : 0;
                    echo "&nbsp;</td>";

                    echo "<td style='background:$class; text-align: center;'>";
                    echo  !empty($result_list3[$date_index]->booking_count) ? $result_list3[$date_index]->booking_count : 0;
                    echo "&nbsp;</td>";

                    echo "<td style='background:$class; text-align: center;'>";
                    echo  !empty($result_list4[$date_index]->booking_count) ? $result_list4[$date_index]->booking_count : 0;
                    echo "&nbsp;</td>";


                    echo "</tr>";

                    $iDateFrom+=86400;
                    $x++;
                }
            }
            ?>

        </tr>

    </table>
<?php
}
?>
</div>
<?
do_footer();
?>