<?php
include_once ('includes/commons.php');
do_header();
?>
		<script type="text/javascript" charset="utf-8">
				var oTable;
				$(document).ready(function() {
				oTable=$('#my_table').dataTable( {
					"bProcessing": true,
					"bJQueryUI": true,
					"sPaginationType": "full_numbers",
					"iDisplayLength": 10,
					"bServerSide": true,
					/*"sAjaxSource": 'src/common/json/json_source_customers.php',*/
					"sAjaxSource": 'src/common/json/server_side_json_media.php',
					"aaSorting": [[8,'desc']],
					"aoColumns": [
						{ bSortable: true, sWidth: '100px' },
						{ bSortable: true, sWidth: '100px' },
						{ bSortable: true, sWidth: '100px' },
						{ bSortable: true, sWidth: '50px' },
						{ bSortable: true, sWidth: '100px' },
						{ bSortable: true, sWidth: '90px' },
						{ bSortable: true, sWidth: '100px' },
						{ bSortable: true, sWidth: '100px' },
						{ bSortable: true, sWidth: '50px' },
						{ bSortable: true, sWidth: '50px' },
						{ bSortable: true, sWidth: '30px' },
						{ bSortable: true, sWidth: '100px' },

					]

				} );

			} );

		</script>
<div class="content_wrapper admin_white_bg">
    <h2>Book a Media Tour</h2>
<table width="100%"  cellpadding="3" cellspacing="3" align="center">
<tr><td align="center" valign="top" >
<div id="container">
<div id="show_all" title="Tour Request">
<div id="detail_inner"></div>
</div>
<table cellpadding="0" cellspacing="0" border="0" class="display" id="my_table">
	<thead>
		<tr>
			
			<th>Name</th>
			<th>Email</th>
            <th>Organization/Group</th>
			<th>Phone</th>
			<th>Contact Person  </th>
			<th>Mobile  </th>
			<th>Category</th>
            <th>Country </th>
            <th>Date </th>
			<th>Time </th>
			<th>Size </th>
			<th>Needs </th>
		</tr>
	</thead>
	<tbody>
	</tbody>
</table>
</div>
</td></tr>
</table>
</div>
<?php
do_footer();
?>


