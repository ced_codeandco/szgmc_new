var win = null;
function NewWindow(mypage,myname,w,h,scroll){ 
LeftPosition = (screen.width) ? (screen.width-w)/2 : 0;
TopPosition = (screen.height) ? (screen.height-h)/2 : 0;
settings =
'height='+h+',width='+w+',top='+TopPosition+',left='+LeftPosition+',scrollbars='+scroll+',resizable'
win = window.open(mypage,myname,settings)
}

function hide_select_box()
{
document.body.className +=' hideSelects'
}
function show_select_box()
{
document.body.className = document.body.className.replace(' hideSelects', '');
}

//trim functions
function trim(str) {
	return ltrim(rtrim(str));
}
function ltrim(str) { 
	for(var k = 0; k < str.length && isWhitespace(str.charAt(k)); k++);
	return str.substring(k, str.length);
}
function rtrim(str) {
	for(var j=str.length-1; j>=0 && isWhitespace(str.charAt(j)) ; j--) ;
	return str.substring(0,j+1);
}
function isWhitespace(charToCheck) {
	var whitespaceChars = " \t\n\r\f";
	return (whitespaceChars.indexOf(charToCheck) != -1);
}


function make_active2(n_id)
{
check=confirm("Are you sure. You want to make this live?");
if(check)
news_change_status2(n_id,'Y');

}
function make_deactive2(n_id)
{
check=confirm("Are you sure. You want to hide this from the live website?");
if(check)
news_change_status2(n_id,'N');
}

function news_change_status2(n_id,status_txt)
{

  $.ajax({
   type: "POST",
   url: "src/common/change_status2.php",
   data: "news_id="+n_id+",&status="+status_txt,
   success: function(result){
	   result=trim(result);
	    if(result=='success')
		{
			//if(status_txt=='Y') 
			//alert("Status has been changed successfully");			
		}else
		{
			alert("Error occured")

		}
   },
   complete: function(){
   oTable.fnReloadAjax(oTable.fnSettings());
   },
   error: function(){
			alert("Server error occured. Please try later");
   }
   
 });

}
function show_tour_disapproved(p_id)
{

$("#show_all").dialog({width: 720});
$( "#show_all" ).dialog( 'option', 'position', [340, 90]  );
//var position = $( ".selector" ).dialog( "option", "position" );
//alert(position);
$('#detail_inner').html('<img src="images/loading.gif" />');
$('#show_all').dialog('open')

    $.ajax({
   type: "POST",
   url: "/admin/tour_disapprove.php",
   data: "request_id="+p_id,
   success: function(result){
	   $('#detail_inner').html(result);
   },
   complete: function(){

   },
   error: function(){
$('#detail_inner').html('Error occured. Please try later');
   }

 });
}

function show_tour_approved(p_id)
{

$("#show_all").dialog({width: 750});
$( "#show_all" ).dialog( 'option', 'position', [340, 90]  );
//var position = $( ".selector" ).dialog( "option", "position" );
//alert(position);
$('#detail_inner').html('<img src="images/loading.gif" />');
$('#show_all').dialog('open')

    $.ajax({
   type: "POST",
   url: "/admin/tour_approved.php",
   data: "request_id="+p_id,
   success: function(result){
	   $('#detail_inner').html(result);
   },
   complete: function(){

   },
   error: function(){
$('#detail_inner').html('Error occured. Please try later');
   }

 });
}
function news_change_status1(n_id,status_txt)
{

  $.ajax({
   type: "POST",
   url: "src/common/change_status1.php",
   data: "news_id="+n_id+",&status="+status_txt,
   success: function(result){
	   result=trim(result);
	    if(result=='success')
		{
			//if(status_txt=='Y') 
			//alert("Status has been changed successfully");			
		}else
		{
			alert("Error occured")

		}
   },
   complete: function(){
   oTable.fnReloadAjax(oTable.fnSettings());
   },
   error: function(){
			alert("Server error occured. Please try later");
   }
   
 });

}

function make_deactive1(n_id)
{
check=confirm("Are you sure. You want to hide this publication from the live website?");
if(check)
news_change_status1(n_id,'N');
}
function make_active1(n_id)
{
check=confirm("Are you sure. You want to make this publication live?");
if(check)
news_change_status1(n_id,'Y');

}
function show_participants(p_id)
{

$("#show_all").dialog({width: 650});
$( "#show_all" ).dialog( 'option', 'position', [340, 90]  );
//var position = $( ".selector" ).dialog( "option", "position" );
//alert(position);
$('#detail_inner').html('<img src="images/loading.gif" />');
$('#show_all').dialog('open')

    $.ajax({
   type: "POST",
   url: "/admin/filming_request_view.php",
   data: "request_id="+p_id,
   success: function(result){
	   $('#detail_inner').html(result);
   },
   complete: function(){

   },
   error: function(){
$('#detail_inner').html('Error occured. Please try later');
   }

 });
}

function show_tours_request(p_id)
{

$("#show_all").dialog({width: 650});
$( "#show_all" ).dialog( 'option', 'position', [340, 90]  );
//var position = $( ".selector" ).dialog( "option", "position" );
//alert(position);
$('#detail_inner').html('<img src="images/loading.gif" />');
$('#show_all').dialog('open')

    $.ajax({
   type: "POST",
   url: "/admin/tours_request_view.php",
   data: "request_id="+p_id,
   success: function(result){
	   $('#detail_inner').html(result);
   },
   complete: function(){

   },
   error: function(){
$('#detail_inner').html('Error occured. Please try later');
   }

 });
}

function show_work_request(p_id)
{

$("#show_all").dialog({width: 650});
$( "#show_all" ).dialog( 'option', 'position', [340, 90]  );
//var position = $( ".selector" ).dialog( "option", "position" );
//alert(position);
$('#detail_inner').html('<img src="images/loading.gif" />');
$('#show_all').dialog('open')

    $.ajax({
   type: "POST",
   url: "/admin/workshop_view.php",
   data: "request_id="+p_id,
   success: function(result){
	   $('#detail_inner').html(result);
   },
   complete: function(){

   },
   error: function(){
$('#detail_inner').html('Error occured. Please try later');
   }

 });
}
function show_job(p_id)
{

$("#show_all").dialog({width: 650});
$( "#show_all" ).dialog( 'option', 'position', [340, 90]  );
//var position = $( ".selector" ).dialog( "option", "position" );
//alert(position);
$('#detail_inner').html('<img src="images/loading.gif" />');
$('#show_all').dialog('open')

    $.ajax({
   type: "POST",
   url: "/admin/job_view.php",
   data: "request_id="+p_id,
   success: function(result){
	   $('#detail_inner').html(result);
   },
   complete: function(){

   },
   error: function(){
$('#detail_inner').html('Error occured. Please try later');
   }

 });
}
function show_tour_operator(p_id)
{

$("#show_all").dialog({width: 650});
$( "#show_all" ).dialog( 'option', 'position', [340, 90]  );
//var position = $( ".selector" ).dialog( "option", "position" );
//alert(position);
$('#detail_inner').html('<img src="images/loading.gif" />');
$('#show_all').dialog('open')

    $.ajax({
   type: "POST",
   url: "/admin/tour_operator_view.php",
   data: "request_id="+p_id,
   success: function(result){
	   $('#detail_inner').html(result);
   },
   complete: function(){

   },
   error: function(){
$('#detail_inner').html('Error occured. Please try later');
   }

 });
}

function show_disapproved_comments(p_id)
{

$("#show_all").dialog({width: 650});
$( "#show_all" ).dialog( 'option', 'position', [340, 90]  );
//var position = $( ".selector" ).dialog( "option", "position" );
//alert(position);
$('#detail_inner').html('<img src="images/loading.gif" />');
$('#show_all').dialog('open')

    $.ajax({
   type: "POST",
   url: "/admin/comment_view.php",
   data: "request_id="+p_id,
   success: function(result){
	   $('#detail_inner').html(result);
   },
   complete: function(){

   },
   error: function(){
$('#detail_inner').html('Error occured. Please try later');
   }

 });
}

function mark_as_read(p_id){
//alert(1);
	$.post("markReadStatus.php?p_id="+p_id, $('#frmEmail').serialize(),  function(data) {
			//alert(data);																			 
   });
}
function show_tour_request(p_id)
{

$("#show_all").dialog({width: 650});
$( "#show_all" ).dialog( 'option', 'position', [340, 90]  );
//var position = $( ".selector" ).dialog( "option", "position" );
//alert(position);
$('#detail_inner').html('<img src="images/loading.gif" />');
$('#show_all').dialog('open')

    $.ajax({
   type: "POST",
   url: "/admin/tour_view.php",
   data: "request_id="+p_id,
   success: function(result){
	   $('#detail_inner').html(result);
   },
   complete: function(){

   },
   error: function(){
$('#detail_inner').html('Error occured. Please try later');
   }

 });
}
$('#show_all').bind('dialogclose', function(event) {
     location.reload();
 });
function show_lost_request(p_id)
{

$("#show_all").dialog({width: 650});
$( "#show_all" ).dialog( 'option', 'position', [340, 90]  );
//var position = $( ".selector" ).dialog( "option", "position" );
//alert(position);
$('#detail_inner').html('<img src="images/loading.gif" />');
$('#show_all').dialog('open')

    $.ajax({
   type: "POST",
   url: "/admin/lost_view.php",
   data: "request_id="+p_id,
   success: function(result){
	   $('#detail_inner').html(result);
   },
   complete: function(){

   },
   error: function(){
$('#detail_inner').html('Error occured. Please try later');
   }

 });
}
function show_sug_request(p_id)
{

$("#show_all").dialog({width: 650});
$( "#show_all" ).dialog( 'option', 'position', [340, 90]  );
//var position = $( ".selector" ).dialog( "option", "position" );
//alert(position);
$('#detail_inner').html('<img src="images/loading.gif" />');
$('#show_all').dialog('open')

    $.ajax({
   type: "POST",
   url: "/admin/suggestion_view.php",
   data: "request_id="+p_id,
   success: function(result){
	   $('#detail_inner').html(result);
   },
   complete: function(){

   },
   error: function(){
$('#detail_inner').html('Error occured. Please try later');
   }

 });
}
//
//function printContent(req_id){
//str=document.getElementById(req_id).innerHTML
//newwin=window.open('','printwin','left=100,top=100,width=700,height=600')
//newwin.document.write('<HTML>\n<HEAD>\n')
//newwin.document.write('<TITLE>Print Page</TITLE>\n')
//newwin.document.write('<script>\n')
//newwin.document.write('function chkstate(){\n')
//newwin.document.write('if(document.readyState=="complete"){\n')
//newwin.document.write('window.close()\n')
//newwin.document.write('}\n')
//newwin.document.write('else{\n')
//newwin.document.write('setTimeout("chkstate()",2000)\n')
//newwin.document.write('}\n')
//newwin.document.write('}\n')
//newwin.document.write('function print_win(){\n')
//newwin.document.write('window.print();\n')
//newwin.document.write('chkstate();\n')
//newwin.document.write('}\n')
//newwin.document.write('<\/script>\n')
//newwin.document.write('</HEAD>\n')
//newwin.document.write('<BODY onload="print_win()">\n')
//newwin.document.write(str)
//newwin.document.write('</BODY>\n')
//newwin.document.write('</HTML>\n')
//newwin.document.close()
//}

function printFilming(req_id)
{
	/*
w=window.open();
w.document.write($('#detail_inner').html());
w.print();
w.close();*/


$('#'+req_id).printArea();



}
function print_details()
{
	/*
w=window.open();
w.document.write($('#detail_inner').html());
w.print();
w.close();*/


$('#print_area').printArea();



}


function make_active(n_id)
{
check=confirm("Are you sure. You want to make this news live?");
if(check)
news_change_status(n_id,'Y');

}
function make_deactive(n_id)
{
check=confirm("Are you sure. You want to hide this news from the live website?");
if(check)
news_change_status(n_id,'N');
}

function make_approve(n_id)
{
check=confirm("Are you sure. You want to approve?");
if(check)
approve_status(n_id,'N');
}

function make_disapprove(n_id)
{
check=confirm("Are you sure. You want to disapprove?");
if(check)
disapprove_status(n_id,'N');
}



function news_change_status(n_id,status_txt)
{

  $.ajax({
   type: "POST",
   url: "src/common/change_status.php",
   data: "news_id="+n_id+",&status="+status_txt,
   success: function(result){
	   result=trim(result);
	    if(result=='success')
		{
			//if(status_txt=='Y') 
			//alert("Status has been changed successfully");			
		}else
		{
			alert("Error occured")

		}
   },
   complete: function(){
   oTable.fnReloadAjax(oTable.fnSettings());
   },
   error: function(){
			alert("Server error occured. Please try later");
   }
   
 });

}

function approve_status(n_id,status_txt)
{

  $.ajax({
   type: "POST",
   url: "src/common/approve_status.php",
   data: "id="+n_id+",&status="+status_txt,
   success: function(result){
	   result=trim(result);
	    if(result=='success')
		{
			//if(status_txt=='Y') 
			//alert("Status has been changed successfully");			
		}else
		{
			alert("Error occured")

		}
   },
   complete: function(){
   oTable.fnReloadAjax(oTable.fnSettings());
   },
   error: function(){
			alert("Server error occured. Please try later");
   }
   
 });

}


function disapprove_status(n_id,status_txt)
{

  $.ajax({
   type: "POST",
   url: "src/common/disapprove_status.php",
   data: "id="+n_id+",&status="+status_txt,
   success: function(result){
	   result=trim(result);
	    if(result=='success')
		{
			//if(status_txt=='Y') 
			//alert("Status has been changed successfully");			
		}else
		{
			alert("Error occured")

		}
   },
   complete: function(){
   oTable.fnReloadAjax(oTable.fnSettings());
   },
   error: function(){
			alert("Server error occured. Please try later");
   }
   
 });

}



function make_active_programmes(n_id)
{
check=confirm("Are you sure. You want to make this live?");
if(check)
programmes_change_status(n_id,'Y');

}
function make_deactive_programmes(n_id)
{
check=confirm("Are you sure. You want to hide this from the live website?");
if(check)
programmes_change_status(n_id,'N');
}


function programmes_change_status(n_id,status_txt)
{

  $.ajax({
   type: "POST",
   url: "src/common/change_status_programme.php",
   data: "p_id="+n_id+",&status="+status_txt,
   success: function(result){
	   result=trim(result);
	    if(result=='success')
		{
			//if(status_txt=='Y') 
			//alert("Status has been changed successfully");			
		}else
		{
			alert("Error occured")

		}
   },
   complete: function(){
   oTable.fnReloadAjax(oTable.fnSettings());
   },
   error: function(){
			alert("Server error occured. Please try later");
   }
   
 });

}



