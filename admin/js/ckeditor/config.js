/*
Copyright (c) 2003-2010, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
//config.contentsCss = ['/js/admin/ckeditor/contents.css','/css/default/style.css']; 

config.contentsCss = ['/en/css/styles.css']; 
config.fullPage = false;
config.scayt_autoStartup = false;
config.protectedSource.push(/<\?[\s\S]*?\?>/g);

//config.protectedSource.push( /<!-- src -->[\s\S]*<!-- end src-->/gi );
/*config.FormatSource=false ;*/
/*FCKConfig.EnterMode = 'br' ; */
//config.contentsCss = '/js/admin/ckeditor/contents.css'; 
	
};
