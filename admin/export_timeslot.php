<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 1/7/2016
 * Time: 5:46 PM
 */
include_once($_SERVER['DOCUMENT_ROOT'].'/en/inc/conf.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/en/inc/mysql.lib.php');
require_once dirname(__FILE__) . '/../libraries/Excel/php-export-data.class.php';
error_reporting(0);
$mydb=new connect;
mysql_query("SET NAMES utf8");

if(!empty($_GET["from_date"]))
{
    $from_date=$_GET["from_date"];
    $from_date1 = explode("-",$from_date);
    $fromdate = $from_date1[2]."-".$from_date1[1]."-".$from_date1[0].' 00:00:00';

}

if(!empty($_GET["to_date"]))
{
    $to_date=$_GET["to_date"];
    $to_date1 = explode("-",$to_date);
    $todate = $to_date1[2]."-".$to_date1[1]."-".$to_date1[0].' 23:59:59';
}


if (!empty($fromdate) && !empty($todate)) {

    $time_group_start1 = '09:00:00';
    $time_group_end1 = '16:00:00';
    $time_group_start2 = '16:00:00';
    $time_group_end2 = '22:00:00';

    $field = 'submitted_date';

    $to_report_sql1 = "SELECT COUNT(*) as booking_count, submitted_date, DATE_FORMAT(submitted_date, '%Y-%m-%d') AS date_index FROM `tours` WHERE
        `group_cat` = 'Tour Operator'
        AND submitted_date BETWEEN '$fromdate' AND '$todate'
        AND DATE_FORMAT(submitted_date,'%H') BETWEEN '$time_group_start1' AND '$time_group_end1'
        GROUP BY date_index
        ORDER BY `submitted_date` ASC";
    $result_list1 = get_timeslot_array($to_report_sql1);

    $to_report_sql2 = "SELECT COUNT(*) as booking_count, submitted_date, DATE_FORMAT(submitted_date, '%Y-%m-%d') AS date_index FROM `tours` WHERE
        `group_cat` != 'Tour Operator'
        AND submitted_date BETWEEN '$fromdate' AND '$todate'
        AND DATE_FORMAT(submitted_date,'%H') BETWEEN '$time_group_start1' AND '$time_group_end1'
        GROUP BY date_index
        ORDER BY `submitted_date` ASC";
    $result_list2 = get_timeslot_array($to_report_sql2);

    $to_report_sql3 = "SELECT COUNT(*) as booking_count, submitted_date, DATE_FORMAT(submitted_date, '%Y-%m-%d') AS date_index FROM `tours` WHERE
        `group_cat` = 'Tour Operator'
        AND submitted_date BETWEEN '$fromdate' AND '$todate'
        AND DATE_FORMAT(submitted_date,'%H') BETWEEN '$time_group_start2' AND '$time_group_end2'
        GROUP BY date_index
        ORDER BY `submitted_date` ASC";
    $result_list3 = get_timeslot_array($to_report_sql3);

    $to_report_sql4 = "SELECT COUNT(*) as booking_count, submitted_date, DATE_FORMAT(submitted_date, '%Y-%m-%d') AS date_index FROM `tours` WHERE
        `group_cat` != 'Tour Operator'
        AND submitted_date BETWEEN '$fromdate' AND '$todate'
        AND DATE_FORMAT(submitted_date,'%H') BETWEEN '$time_group_start2' AND '$time_group_end2'
        GROUP BY date_index
        ORDER BY `submitted_date` ASC";
    $result_list4 = get_timeslot_array($to_report_sql4);
}
/*echo "<br /><br />";print_r($result_list1);
echo "<br /><br />";print_r($result_list2);
echo "<br /><br />";print_r($result_list3);
echo "<br /><br />";print_r($result_list4);
*/

function get_timeslot_array($sql) {


    $query_sql = mysql_query($sql);


    $result_array = array();
    if ($query_sql) {

        while ($result = mysql_fetch_object($query_sql)) {

            $result_array[strtotime($result->date_index)] = $result;
        }
        //die("<br /><br />here");
    }

    return $result_array;
}


$excel = new ExportDataExcel('browser', "time_slot_report.xls");
$outputData = array("No.","Date","Number of Booking requests 9am – 4pm (TO Category)","Number of Booking request 9am – 4pm (Others)","Number of Booking requests 4pm – 10pm (TO Category)","Number of Booking requests 4pm – 10pm (Others)");

$excel->initialize();
$excel->addRow($outputData);

$x = 1;
if (!empty($fromdate) && !empty($todate)) {
    //die("Bottom");
    $iDateFrom = strtotime($fromdate);
    $iDateTo = strtotime($todate);
    while ($iDateTo>=$iDateFrom) {
        $date_index = strtotime(date('Y-m-d', $iDateFrom));
        $outputData = array($x);
        $outputData[] = date('d-m-Y', $date_index);
        $outputData[] = !empty($result_list1[$date_index]->booking_count) ? $result_list1[$date_index]->booking_count : 0;
        $outputData[] = !empty($result_list2[$date_index]->booking_count) ? $result_list2[$date_index]->booking_count : 0;
        $outputData[] = !empty($result_list3[$date_index]->booking_count) ? $result_list3[$date_index]->booking_count : 0;
        $outputData[] = !empty($result_list4[$date_index]->booking_count) ? $result_list4[$date_index]->booking_count : 0;

        //echo "<br /><br />";print_r($outputData);
        //$outputData = array_map("clean_output", $outputData);
        $excel->addRow($outputData);
        $iDateFrom+=86400;
        $x++;
    }
}

$excel->finalize();