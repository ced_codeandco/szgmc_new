<?php
/**
 * Created by PhpStorm.
 * User: Anas
 * Date: 5/11/2015
 * Time: 9:54 AM
 */
ob_start();
include_once ('includes/commons.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/en/includes/generic_functions.php');
include '../includes/database.php';
include '../forms/notification_email.php';
$db = new MyDatabase();
$conn = $db->getConnection();

addNotificationEmail($conn);
deleteNotificationEmail($conn);

do_header();
ob_end_flush();?>
<script type="text/javascript">
$(function() {
    $("#from_date").datepicker({dateFormat: 'dd-mm-yy'});
    $("#to_date").datepicker({dateFormat: 'dd-mm-yy'});
});
function expXLS(){
    window.location.href="export_juniorcultural_guide.php?"+$('#frmAdvanceReport').serialize();
    return false
}
</script>
<?php
$notificationEmailList = getAllNotificationEmail($conn, 'JCG_NOTIFICATION');
$count = count($notificationEmailList);

?>
    <h1 style="margin-left:25px;">Junior Cultural Guide Program - Email Notification</h1>
    <div id="toursPrintDiv" style="clear:both;">

        <style>
            .menu_list_table thead tr{background:transparent}
            .menu_list_table thead td{padding:0!important;border:0;text-align:center}
            .menu_list_table input,.menu_list_table select{width:auto!important;height:auto!important}
            .menu_list_table thead select,.menu_list_table thead input[type="text"]{width:99%!important}
            .menu_list_table thead input[type="submit"]{width:100px!important;text-align:center}
            .menu_list_table th{padding-left:10px;padding-right:10px}
            .pagination div {  width: 13px; float: left;  }
            .pagination{  margin-left: auto;  margin-right: auto;  width: 200px;  }
            .pagination div.main-page{width: 40px;}
        </style>
        


            <table align="center" class="menu_list_table" style="margin-top:15px;" width="50%">
                <form action="" method="post" id="frmAdvanceReport">
                    <input type="hidden" name="add-email-notification" value="1">
                    <input type="hidden" name="notification_type" value="JCG_NOTIFICATION">
                    <thead>
                        <tr>
                            <td colspan="14">
                                <?php if (!empty($_SESSION['success'])){ echo '<div style="text-align: center;color: green;">'.$_SESSION['success'].'</div>'; unset($_SESSION['success']); }?>
                                <?php if (!empty($_SESSION['error'])){ echo '<div style="text-align: center;color: red;">'.$_SESSION['error'].'</div>'; unset($_SESSION['error']); }?>
                                <table align="center" style="width: 30%;">
                                    <tbody>
                                    <tr>
                                        <td style="font-weight: bold;">JCG - Add Email Notification</td>
                                    </tr>
                                    <tr>
                                        <td>Name: <input required type="text" id="name" name="name" style="width:200px !important;" value="<?php echo !empty($_POST['name']) ? $_POST['name'] : '';?>"> </td>
                                    </tr>
                                    <tr>
                                        <td>Email: <input required type="email" id="email" name="email" style="width:200px !important;" value="<?php echo !empty($_POST['email']) ? $_POST['email'] : '';?>"> </td>
                                    </tr>
                                    <tr>
                                        <td>Type:
                                        <select name="email_type" style="width: 200px !important; direction: rtl;" required>
                                            <option value="">Select</option>
                                            <option <?php echo (!empty($_POST['email_type']) && $_POST['email_type'] == 'to') ? 'selected' : ''?> value="to">To</option>
                                            <option <?php echo (!empty($_POST['email_type']) && $_POST['email_type'] == 'cc') ? 'selected' : ''?> value="cc">Cc</option>
                                            <option <?php echo (!empty($_POST['email_type']) && $_POST['email_type'] == 'bcc') ? 'selected' : ''?> value="bcc">Bcc</option>
                                        </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;<br> <input type="submit" value="Add" name="submit"> </td>
                                    </tr>
                                    </tbody></table>

                            </td>
                        </tr>
                    </thead>
                </form>
                <?php
                if ($count > 0) {?>

                <tbody>
                    <tr class="nodrag">
                        <th>SI</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Type</th>
                        <th>Added date</th>
                        <th>Last Updated</th>
                        <th>Action</th>
                    </tr>

                <tr></tr>
                <?php
                $counter = 1;
                foreach ($notificationEmailList as $visitor) {?>
                    <tr>
                        <td style="background:<?php echo (($counter%2) == 0 ? '#999' : '#ddd');?>;"><?php echo $counter;?></td/>
                        <td style="background:<?php echo (($counter%2) == 0 ? '#999' : '#ddd');?>;"><?php echo $visitor->name;?>&nbsp;</td/>
                        <td style="background:<?php echo (($counter%2) == 0 ? '#999' : '#ddd');?>;"><?php echo $visitor->email;?>&nbsp;</td>
                        <td style="background:<?php echo (($counter%2) == 0 ? '#999' : '#ddd');?>;"><?php echo $visitor->email_type;?>&nbsp;</td>
                        <td style="background:<?php echo (($counter%2) == 0 ? '#999' : '#ddd');?>;"><?php echo $visitor->updated_date_time;?>&nbsp;</td/>
                        <td style="background:<?php echo (($counter%2) == 0 ? '#999' : '#ddd');?>;"><?php echo $visitor->created_date_time;?>&nbsp;</td/>
                        <td style="background:<?php echo (($counter%2) == 0 ? '#999' : '#ddd');?>;"><a href="configure-jcg-email.php?action=del&id=<?php echo $visitor->id;?>" onclick="return confirm('Are you sure you want to delete this notification email')">DELETE</a></td/>
                    </tr><?php
                    $counter++;
                }?>
                    <tr>
                        <td colspan="14"><?php

                            ?>
                        </td>
                    </tr>
                </tbody>
                <?php }?>
            </table>
    </div>
<?php
do_footer();
?>