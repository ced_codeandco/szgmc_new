<?php
  /* MySQL connection */
session_start();
if(!isset($_SESSION['powers']) )
header("Location: ../../../logout_handler.php");

include_once '../../../../inc/conf.php';
include_once '../../../../inc/mysql.lib.php';
$mydb=new connect;
	/* Paging */
	$sLimit = "";
	if ( isset( $_GET['iDisplayStart'] ) )
	{
		$sLimit = "LIMIT ".mysql_real_escape_string( $_GET['iDisplayStart'] ).", ".
			mysql_real_escape_string( $_GET['iDisplayLength'] );
	}
	
	/* Ordering */
	if ( isset( $_GET['iSortCol_0'] ) )
	{
		$sOrder = "ORDER BY  ";
		for ( $i=0 ; $i<mysql_real_escape_string( $_GET['iSortingCols'] ) ; $i++ )
		{
			$sOrder .= fnColumnToField(mysql_real_escape_string( $_GET['iSortCol_'.$i] ))."
			 	".mysql_real_escape_string( $_GET['iSortDir_'.$i] ) .", ";
		}
		$sOrder = substr_replace( $sOrder, "", -2 );
	}
	

	
	/* Filtering - NOTE this does not match the built-in DataTables filtering which does it
	 * word by word on any field. It's possible to do here, but concerned about efficiency
	 * on very large tables, and MySQL's regex functionality is very limited
	 */
	$sWhere = "";
	if ( $_GET['sSearch'] != "" )
	{

					$sWhere = "WHERE pub_id LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ".
					    "pub_title LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ".
		                "pub_text LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ".
		                "pub_date LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%'";


	
	}

	$sQuery = "SELECT SQL_CALC_FOUND_ROWS pub_id,image,pub_title,pub_text,pub_date,last_updated,live from publication  
		$sWhere 
		$sOrder 		
		$sLimit	";	
		
	
/*
$myFile = "logs.txt";
$fh = fopen($myFile, 'w') or die("can't open file");
fwrite($fh,$sQuery);
fclose($fh);
*/
	
	
	$rResult = $mydb->query($sQuery);

	

	$sQuery = "SELECT FOUND_ROWS()";
	$rResultFilterTotal = $mydb->query($sQuery);
	$aResultFilterTotal = mysql_fetch_array($rResultFilterTotal);
	$iFilteredTotal = $aResultFilterTotal[0];

	

	
	
	$sQuery = "SELECT COUNT(pub_id) FROM publication";
	$rResultTotal =$mydb->query($sQuery);
	$aResultTotal = mysql_fetch_array($rResultTotal);
	$iTotal = $aResultTotal[0];
	
	echo '{';
	echo '"sEcho": '.$_GET['sEcho'].', ';
	echo '"iTotalRecords": '.$iTotal.', ';
	echo '"iTotalDisplayRecords": '.$iFilteredTotal.', ';
	echo '"aaData": [ ';
	

$jsonData="";

		while($obj=mysql_fetch_object($rResult)){

	$id_txt=$obj->pub_id;


if($obj->live=='Y')
$active_txt="<a href='#' onclick='make_deactive1(".$obj->pub_id.");'><img src='images/green_ball.jpg' alt='Yes' title='Live' /></a>";
else
$active_txt="<a href='#' onclick='make_active1(".$obj->pub_id.");' ><img src='images/red_ball.jpg' alt='No' title='Not Live' /></a>";

$image_txt='';
if($obj->image!='' && $obj->image!=null)
$image_txt="<img src='../images/news/".$obj->image."' style='width:90px;height:55px;'/>";








	$id_txt=addslashes($id_txt);
	$active_txt=addslashes($active_txt);
	$image_txt=addslashes($image_txt);	
	
	
	$news_title=addslashes($obj->pub_title);


	//$news_text=addslashes($obj->news_text);
$news_text=substr($obj->pub_text,0,150)."..."; 		
	$news_text=mysql_real_escape_string($news_text);
	$news_text=Helper::UTF8ToHTML($news_text);
	
	$news_date=$obj->pub_date;	
	$last_updated=$obj->last_updated;		
	
	
	$actions_txt="<a href='manage_publication.php?news_id=".$obj->pub_id."'><img src='images/edit_icon.gif' title='Edit' class='icon'/></a>&nbsp;<a href='manage_publication.php?news_id=".$obj->pub_id."&action=delete'><img src='images/recycle-bin.jpg' title='Delete' onclick='return confirmAction1()' class='icon'/></a>";

$actions_txt=mysql_real_escape_string($actions_txt);
if($jsonData!="")
$jsonData.=",";
$jsonData.="['".$id_txt."','".$active_txt."','".$image_txt."','".$news_title."','".$news_text."','".$news_date."','".$last_updated."','".$actions_txt."']";
}

echo $jsonData;
	echo '] }';
	
	function fnColumnToField( $i )
	{
		if ( $i == 0 )
			return "pub_id";
		if ( $i == 1 )
			return "live";			
		else if ( $i == 3 )
			return "pub_title";
		else if ( $i == 4 )
			return "pub_text";
		else if ( $i == 5 )
			return "pub_date";
		else if ( $i == 6 )
			return "pub_updated";
	}
?>