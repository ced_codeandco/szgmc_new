<?php
  /* MySQL connection */
session_start();
if(!isset($_SESSION['powers']) )
header("Location: /en/admin/logout_handler.php");

include_once($_SERVER['DOCUMENT_ROOT'].'/inc/conf.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/inc/mysql.lib.php');
$mydb=new connect;
	/* Paging */
	$sLimit = "";
	if ( isset( $_GET['iDisplayStart'] ) )
	{
		$sLimit = "LIMIT ".mysql_real_escape_string( $_GET['iDisplayStart'] ).", ".
			mysql_real_escape_string( $_GET['iDisplayLength'] );
	}
	
	/* Ordering */
	if ( isset( $_GET['iSortCol_0'] ) )
	{
		$sOrder = "ORDER BY  ";
		for ( $i=0 ; $i<mysql_real_escape_string( $_GET['iSortingCols'] ) ; $i++ )
		{
			$sOrder .= fnColumnToField(mysql_real_escape_string( $_GET['iSortCol_'.$i] ))."
			 	".mysql_real_escape_string( $_GET['iSortDir_'.$i] ) .", ";
		}
		$sOrder = substr_replace( $sOrder, "", -2 );
	}
	

	
	/* Filtering - NOTE this does not match the built-in DataTables filtering which does it
	 * word by word on any field. It's possible to do here, but concerned about efficiency
	 * on very large tables, and MySQL's regex functionality is very limited
	 */
	$sWhere = "";
	if ( $_GET['sSearch'] != "" )
	{

					$sWhere = "WHERE fname LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ".
					    "uname LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ".
		                "type LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ";


	
	}

	$sQuery = "SELECT SQL_CALC_FOUND_ROWS uid,fname,uname,created,expiry,type from user  
		$sWhere 
		$sOrder 		
		$sLimit	";	
		

	
	
	$rResult = $mydb->query($sQuery);

	

	$sQuery = "SELECT FOUND_ROWS()";
	$rResultFilterTotal = $mydb->query($sQuery);
	$aResultFilterTotal = mysql_fetch_array($rResultFilterTotal);
	$iFilteredTotal = $aResultFilterTotal[0];

	

	
	
	$sQuery = "SELECT COUNT(uid) FROM user";
	$rResultTotal =$mydb->query($sQuery);
	$aResultTotal = mysql_fetch_array($rResultTotal);
	$iTotal = $aResultTotal[0];
	
	echo '{';
	echo '"sEcho": '.$_GET['sEcho'].', ';
	echo '"iTotalRecords": '.$iTotal.', ';
	echo '"iTotalDisplayRecords": '.$iFilteredTotal.', ';
	echo '"aaData": [ ';
	

$jsonData="";

		while($obj=mysql_fetch_object($rResult)){

	
    
	$uname=$obj->uname;
	$fname=$obj->fname;
	$created_date=$obj->created;	
	$expiry=$obj->expiry;		
	$type=$obj->type;
	if($type=="user")
	{
	$type = "Prequalification Document";
	}
	
	$actions_txt="<a href='manage_user.php?uid=".$obj->uid."'><img src='images/edit_icon.gif' title='Edit User Details' class='icon'/></a>
	<a href='manage_user.php?uid=".$obj->uid."&action=delete'><img src='images/delete.png' title='Delete' class='icon'/></a>
	";

$actions_txt=mysql_real_escape_string($actions_txt);
if($jsonData!="")
$jsonData.=",";
$jsonData.="['".$fname."','".$uname."','".$created_date."','".$actions_txt."']";
}

echo $jsonData;
	echo '] }';
	
	function fnColumnToField( $i )
	{
		
		if ( $i == 0 )
			return "fname";			
		else if ( $i == 1 )
			return "uname";
		else if ( $i == 2 )
			return "created";
		
								
	}
?>