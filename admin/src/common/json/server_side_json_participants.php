<?php
  /* MySQL connection */
session_start();
if(!isset($_SESSION['powers']) )
header("Location: /logout_handler.php");

include_once($_SERVER['DOCUMENT_ROOT'].'/en/inc/conf.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/en/inc/mysql.lib.php');
$mydb=new connect;
	/* Paging */
	$sLimit = "";
	if ( isset( $_GET['iDisplayStart'] ) )
	{
		$sLimit = "LIMIT ".mysql_real_escape_string( $_GET['iDisplayStart'] ).", ".
			mysql_real_escape_string( $_GET['iDisplayLength'] );
	}
	
	/* Ordering */
	if ( isset( $_GET['iSortCol_0'] ) )
	{
		$sOrder = "ORDER BY  ";
		for ( $i=0 ; $i<mysql_real_escape_string( $_GET['iSortingCols'] ) ; $i++ )
		{
			$sOrder .= fnColumnToField(mysql_real_escape_string( $_GET['iSortCol_'.$i] ))."
			 	".mysql_real_escape_string( $_GET['iSortDir_'.$i] ) .", ";
		}
		$sOrder = substr_replace( $sOrder, "", -2 );
	}
	

	
	/* Filtering - NOTE this does not match the built-in DataTables filtering which does it
	 * word by word on any field. It's possible to do here, but concerned about efficiency
	 * on very large tables, and MySQL's regex functionality is very limited
	 */
	$sWhere = "";
	if ( $_GET['sSearch'] != "" )
	{

					$sWhere = "WHERE t.id LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ".
					    "t.team_type LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ".
		                "t.g_name LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ".
		                "t.email LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ".
		                "t.emirate LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ".						
		                "t.interest LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ".												
		                "t.talent LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ".																		
		                "p.name LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ".																								
		                "t.phone_number LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%'";


	
	}

	$sQuery = "SELECT SQL_CALC_FOUND_ROWS t.id,t.team_type,t.g_name,t.phone_number,t.email,t.emirate,t.interest,t.created_date,p.name from team t left join participants p on t.id=p.team_id
		$sWhere 
		GROUP BY t.id 
		$sOrder 		
		$sLimit	";	
		
	
/*
$myFile = "logs.txt";
$fh = fopen($myFile, 'w') or die("can't open file");
fwrite($fh,$sQuery);
fclose($fh);

*/
	
	$rResult = $mydb->query($sQuery);

	

	$sQuery = "SELECT FOUND_ROWS()";
	$rResultFilterTotal = $mydb->query($sQuery);
	$aResultFilterTotal = mysql_fetch_array($rResultFilterTotal);
	$iFilteredTotal = $aResultFilterTotal[0];

	
	
	$sQuery = "SELECT COUNT(id) FROM team";
	$rResultTotal =$mydb->query($sQuery);
	$aResultTotal = mysql_fetch_array($rResultTotal);
	$iTotal = $aResultTotal[0];
	
	echo '{';
	echo '"sEcho": '.$_GET['sEcho'].', ';
	echo '"iTotalRecords": '.$iTotal.', ';
	echo '"iTotalDisplayRecords": '.$iFilteredTotal.', ';
	echo '"aaData": [ ';
	

$jsonData="";

		while($obj=mysql_fetch_object($rResult)){



/*
if($obj->live=='Y')
$active_txt="<a href='#' onclick='make_deactive(".$obj->news_id.");'><img src='images/green_ball.jpg' alt='Yes' title='Live' /></a>";
else
$active_txt="<a href='#' onclick='make_active(".$obj->news_id.");' ><img src='images/red_ball.jpg' alt='No' title='Not Live' /></a>";
*/

/*
$image_txt='';
if($obj->image!='' && $obj->image!=null)
$image_txt="<img src='../news_images/".$obj->image."' style='width:90px;height:55px;'/>";
*/




	//$news_text=addslashes($obj->news_text);
//$news_text=substr($obj->news_text,0,150)."..."; 		
	//$news_text=mysql_real_escape_string($news_text);
	


$actions_txt="<a href='#' onclick='show_participants(\"".$obj->id."\");return false;'><img src='images/new_window.gif' title='View Details' class='icon'/></a>";
	
//	$actions_txt.="<a href='manage_participants.php?id=".$obj->id."'><img src='images/edit_icon.gif' title='Edit Details' class='icon'/></a>";

	

	
	
$actions_txt=mysql_real_escape_string($actions_txt);

if($obj->team_type=='Individual')
$obj->g_name=$obj->name; //if the participant is individual show participant name instead of group name

if($jsonData!="")
$jsonData.=",";
$jsonData.="['".$obj->id."','".$obj->team_type."','".$obj->g_name."','".$obj->phone_number."','".$obj->email."','".$obj->emirate."','".$obj->interest."','".$obj->created_date."','".$actions_txt."']";
}

echo $jsonData;
	echo '] }';
	
	function fnColumnToField( $i )
	{
	
		if ( $i == 0 )
			return "t.id";
		if ( $i == 1 )
			return "t.team_type";			
		else if ( $i == 2 )
			return "t.g_name";
		else if ( $i == 3 )
			return "t.phone_number";
		else if ( $i == 4 )
			return "t.email";
		else if ( $i == 5 )
			return "t.emirate";
		else if ( $i == 6 )
			return "t.interest";
		else if ( $i == 7 )
			return "t.created_date";
			
		
	}
?>