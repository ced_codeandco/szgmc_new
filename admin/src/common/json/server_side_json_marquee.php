<?php
  /* MySQL connection */
session_start();
if(!isset($_SESSION['powers']) )
header("Location: /admin/logout_handler.php");

include_once($_SERVER['DOCUMENT_ROOT'].'/inc/conf.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/inc/mysql.lib.php');
$mydb=new connect;
	/* Paging */
	$sLimit = "";
	if ( isset( $_GET['iDisplayStart'] ) )
	{
		$sLimit = "LIMIT ".mysql_real_escape_string( $_GET['iDisplayStart'] ).", ".
			mysql_real_escape_string( $_GET['iDisplayLength'] );
	}
	
	/* Ordering */
	if ( isset( $_GET['iSortCol_0'] ) )
	{
		$sOrder = "ORDER BY  ";
		for ( $i=0 ; $i<mysql_real_escape_string( $_GET['iSortingCols'] ) ; $i++ )
		{
			$sOrder .= fnColumnToField(mysql_real_escape_string( $_GET['iSortCol_'.$i] ))."
			 	".mysql_real_escape_string( $_GET['iSortDir_'.$i] ) .", ";
		}
		$sOrder = substr_replace( $sOrder, "", -2 );
	}
	

	
	/* Filtering - NOTE this does not match the built-in DataTables filtering which does it
	 * word by word on any field. It's possible to do here, but concerned about efficiency
	 * on very large tables, and MySQL's regex functionality is very limited
	 */
	$sWhere = "";
	if ( $_GET['sSearch'] != "" )
	{

					$sWhere = "WHERE marquee_id LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ".
		                "marquee_text LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%'";


	
	}

	$sQuery = "SELECT SQL_CALC_FOUND_ROWS marquee_id,marquee_text,inserted_date,live,opening_stat from marquee  
		$sWhere		
		$sLimit	";	
		
	
/*
$myFile = "logs.txt";
$fh = fopen($myFile, 'w') or die("can't open file");
fwrite($fh,$sQuery);
fclose($fh);
*/
	
	
	$rResult = $mydb->query($sQuery);
	

	$sQuery = "SELECT FOUND_ROWS()";
	$rResultFilterTotal = $mydb->query($sQuery);
	$aResultFilterTotal = mysql_fetch_array($rResultFilterTotal);
	$iFilteredTotal = $aResultFilterTotal[0];

	

	
	
	$sQuery = "SELECT COUNT(marquee_id) FROM marquee";
	$rResultTotal =$mydb->query($sQuery);
	$aResultTotal = mysql_fetch_array($rResultTotal);
	$iTotal = $aResultTotal[0];
	
	echo '{';
	echo '"sEcho": '.$_GET['sEcho'].', ';
	echo '"iTotalRecords": '.$iTotal.', ';
	echo '"iTotalDisplayRecords": '.$iFilteredTotal.', ';
	echo '"aaData": [ ';
	

$jsonData="";

while($obj=mysql_fetch_object($rResult)){
	$id_txt=$obj->marquee_id;
	if($obj->live=='Y')
		$active_txt="<a href='#' onclick='make_deactive(".$obj->marquee_id.");'><img src='images/green_ball.jpg' alt='Yes' title='Live' /></a>";
	else
		$active_txt="<a href='#' onclick='make_active(".$obj->marquee_id.");' ><img src='images/red_ball.jpg' alt='No' title='Not Live' /></a>";
	if($obj->opening_stat=='Y')
		$active2_txt="<a href='#' onclick='make_deactive_stat(".$obj->marquee_id.");'><img src='images/green_ball.jpg' alt='Yes' title='Live' /></a>";
	else
		$active2_txt="<a href='#' onclick='make_active_stat(".$obj->marquee_id.");' ><img src='images/red_ball.jpg' alt='No' title='Not Live' /></a>";	
	$id_txt=addslashes($id_txt);
	$active_txt=addslashes($active_txt);
	$active2_txt=addslashes($active2_txt);
	$marquee_text=mysql_real_escape_string($obj->marquee_text);
	$inserted_date=$obj->inserted_date;		
	$actions_txt="<a href='manage_marquee.php?marquee_id=".$obj->marquee_id."'><img src='images/edit_icon.gif' title='Edit Marquee' class='icon'/></a>";
	$actions_txt.="<a href='manage_marquee.php?marquee_id=".$obj->marquee_id."&action=delete' onclick=\"return confirm('Are you sure you want to delete this marquee')\"><img src='images/recycle-bin.jpg' title='Delete Marquee' class='icon'/></a>";

$actions_txt=mysql_real_escape_string($actions_txt);
if($jsonData!="")
$jsonData.=",";
$jsonData.="['".$id_txt."','".$marquee_text."','".$inserted_date."','".$actions_txt."','".$active_txt."']";
}

echo $jsonData;
	echo '] }';
	
	function fnColumnToField( $i )
	{
		if ( $i == 0 )
			return "marquee_id";
	}
?>