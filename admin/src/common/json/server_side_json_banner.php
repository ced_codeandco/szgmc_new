<?php
  /* MySQL connection */
session_start();
if(!isset($_SESSION['powers']) )
header("Location: /admin/logout_handler.php");

include_once($_SERVER['DOCUMENT_ROOT'].'/inc/conf.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/inc/mysql.lib.php');
$mydb=new connect;
	/* Paging */
	$sLimit = "";
	if ( isset( $_GET['iDisplayStart'] ) )
	{
		$sLimit = "LIMIT ".mysql_real_escape_string( $_GET['iDisplayStart'] ).", ".
			mysql_real_escape_string( $_GET['iDisplayLength'] );
	}
	
	/* Ordering */
	if ( isset( $_GET['iSortCol_0'] ) )
	{
		$sOrder = "ORDER BY  ";
		for ( $i=0 ; $i<mysql_real_escape_string( $_GET['iSortingCols'] ) ; $i++ )
		{
			$sOrder .= fnColumnToField(mysql_real_escape_string( $_GET['iSortCol_'.$i] ))."
			 	".mysql_real_escape_string( $_GET['iSortDir_'.$i] ) .", ";
		}
		$sOrder = substr_replace( $sOrder, "", -2 );
	}
	

	
	/* Filtering - NOTE this does not match the built-in DataTables filtering which does it
	 * word by word on any field. It's possible to do here, but concerned about efficiency
	 * on very large tables, and MySQL's regex functionality is very limited
	 */
	$sWhere = "";
	if ( $_GET['sSearch'] != "" )
	{

					$sWhere = "WHERE banner_id LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ".
		                "banner_img LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%'";


	
	}

	$sQuery = "SELECT SQL_CALC_FOUND_ROWS banner_id,banner_popup_img,banner_img,inserted_date,live from banner  
		$sWhere		
		$sLimit	";	
		
	

	
	$rResult = $mydb->query($sQuery);
	

	$sQuery = "SELECT FOUND_ROWS()";
	$rResultFilterTotal = $mydb->query($sQuery);
	$aResultFilterTotal = mysql_fetch_array($rResultFilterTotal);
	$iFilteredTotal = $aResultFilterTotal[0];

	

	
	
	$sQuery = "SELECT COUNT(banner_id) FROM banner";
	$rResultTotal =$mydb->query($sQuery);
	$aResultTotal = mysql_fetch_array($rResultTotal);
	$iTotal = $aResultTotal[0];
	
	echo '{';
	echo '"sEcho": '.$_GET['sEcho'].', ';
	echo '"iTotalRecords": '.$iTotal.', ';
	echo '"iTotalDisplayRecords": '.$iFilteredTotal.', ';
	echo '"aaData": [ ';
	

$jsonData="";

while($obj=mysql_fetch_object($rResult)){
	$id_txt=$obj->banner_id;
	if($obj->live=='Y')
		$active_txt="<a href='#' onclick='make_deactive(".$obj->banner_id.");'><img src='images/green_ball.jpg' alt='Yes' title='Live' /></a>";
	else
		$active_txt="<a href='#' onclick='make_active(".$obj->banner_id.");' ><img src='images/red_ball.jpg' alt='No' title='Not Live' /></a>";
	$id_txt=addslashes($id_txt);
	$active_txt=addslashes($active_txt);
	$banner_img=mysql_real_escape_string($obj->banner_img);
	$banner_popup_img=mysql_real_escape_string($obj->banner_popup_img);
	$inserted_date=$obj->inserted_date;		
	$actions_txt="<a href='manage_banner.php?banner_id=".$obj->banner_id."'><img src='images/edit_icon.gif' title='Edit Customer Details' class='icon'/></a>&nbsp;<a href='#' onclick='delBanner(".$obj->banner_id.");'><img src='images/recycle-bin.jpg' title='Delete'  class='icon'/></a>";
$image_txt='';
if($obj->banner_img!='' && $obj->banner_img!=null)
$image_txt="<img src='../news_images/".$obj->banner_img."' style='width:90px;height:55px;'/>";
$image_txt=addslashes($image_txt);	
$image2_txt='';
if($obj->banner_popup_img!='' && $obj->banner_popup_img!=null)
$image2_txt="<img src='../news_images/".$obj->banner_popup_img."' style='width:90px;height:55px;'/>";
$image2_txt=addslashes($image2_txt);	
$actions_txt=mysql_real_escape_string($actions_txt);
if($jsonData!="")
$jsonData.=",";
$jsonData.="['".$id_txt."','".$active_txt."','".$image_txt."','".$image2_txt."','".$inserted_date."','".$actions_txt."']";
}

echo $jsonData;
	echo '] }';
	
	function fnColumnToField( $i )
	{
		if ( $i == 0 )
			return "banner_id";
	}
?>