﻿<?php

include_once ('includes/commons.php');
include_once '../includes/config.php';
include_once('../includes/generic_functions.php');


$conf = new Configuration();
?>
<?php
$content="
<!DOCTYPE html>
<html>
	<head><meta http-equiv='Content-Type' content='text/html; charset=utf-8' /></head>
		<body>
			<table width='600px' cellspacing='0' style='direction:ltr;font-family: Arial,Verdana,Helvetica,sans-serif;'>
            	<tr>
					 <td>
					 	<table width='600px' cellspacing='0' style='direction:ltr;' cellpadding='0'>
					    	<tr style='background-color: #D2A118;color: #FFFFFF;font-weight: bold;height: 30px;'>
					        	<td valign='top' style='padding-top:8px;background-color: #D2A118;color: #FFFFFF;font-weight: bold;height: 30px;padding-left: 10px;width:300px;' ><span style='float:left;font-size:16px;padding-left:20px;'>Registration </span></td>
                                <td align='right' style='padding-top:8px;background-color: #D2A118;color: #FFFFFF;font-weight: bold;height: 30px;padding-right: 10px; width:300px;'> <span style='float:right; padding-right:10px; text-align:right; direction:rtl; font-size:16px;'></span></td> 
                            </tr>
					   </table>
					 </td> 
				</tr>
				<tr>
                	<td valign='top' align='center'>
					    <table align='center' width='600' cellpadding='5' cellspacing='2' style='border-top:1px solid #D2A118;border-bottom:1px solid #D2A118; border-left:1px solid #D2A118; border-right:1px solid #D2A118;padding-left:20px;' >
                        	<tr><td  style='color:#C7A317; '> </td><td></td></tr>
                        	<tr><td  style='color:#C7A317;font-weight:bold; '>Dear User, </td><td></td></tr>
                            <tr><td  style='color:#C7A317; '> </td><td></td></tr>
                            <tr><td style='color:#000; ' ><p style='width:500px;color:#000;'>You're receiving this email because you filled out a registration form on <a href='http://www.szgmc.gov.ae/' style='text-decoration:none;color:#C7A317;'>http://www.szgmc.gov.ae/</a> </p></td></tr>
                            <tr><td  style='color:#C7A317; '> </td><td></td></tr>
                            <tr><td style='color:#000; '>Your login details are below: </td></tr>
                            <tr><td  style='color:#C7A317; '> </td><td></td></tr>
                             <tr><td style='color:#C7A317; '>Username: ".$_POST['user']."</td></tr>
                            <tr><td style='color:#C7A317; '>Password: ".$_POST['pass']."</td></tr>
							<tr><td style='color:#000; ' ><p style='width:500px;color:#000;'>Please click <br> <a href='http://www.szgmc.ae/userlogin.php' style='text-decoration:none;color:#C7A317;'>http://www.szgmc.ae/userlogin.php</a> </p></td></tr>
                            <tr><td  style='color:#C7A317; '> </td><td></td></tr>
                            <tr><td style='color:#C7A317; font-family: Arial;padding-bottom: 19px;'>Kind Regards,<br>SZGMC</td></tr>
                            <tr><td  style='color:#C7A317; font-family: Arial;'></td><td></td></tr>
                            
                      	</table>
                      </td> 
                </tr> 
              </table>      
		</body>
</html>";		

		//echo 'test';die();
		$subject = 'Login Details'; 
		$fromName = 'SZGMC';
		$fromEmail = 'tour@szgmc.ae'; 
		$msg = 'ok';
		session_start();
		if($_SESSION['admin_logged']){
			if(!sendLoginCrendentials($_POST['rcv_email'], $fromName, $_POST['rcv_email'], $fromEmail, $subject, $content, $_POST['bcc'])){
				
				$msg = 'not ok';
			}
			echo $msg;
		}
		else{
			
			header('Location: index.php');
		}
?>

