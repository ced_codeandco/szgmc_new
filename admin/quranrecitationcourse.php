<?php
/**
 * Created by PhpStorm.
 * User: Anas
 * Date: 5/11/2015
 * Time: 9:54 AM
 */

include_once ('includes/commons.php');
include_once($_SERVER['DOCUMENT_ROOT'].'/en/includes/generic_functions.php');
include '../includes/database.php';
include 'pagination.php';
do_header();?>
<script type="text/javascript">
$(function() {
    $("#from_date").datepicker({dateFormat: 'dd-mm-yy'});
    $("#to_date").datepicker({dateFormat: 'dd-mm-yy'});
});
function expXLS(){
    window.location.href="export_quranrecitationcourse.php?"+$('#frmAdvanceReport').serialize();
    return false
}
</script>
<?php
if(isset($_POST["from_date"]))
{
    $from_date=$_POST["from_date"];
    $from_date1 = explode("-",$from_date);
    $fromdate = $from_date1[2]."-".$from_date1[1]."-".$from_date1[0].' 00:00:00';

}
else
{
    $from_date=date("d-m-Y");
    $from_date = date('d-m-Y', strtotime($from_date .' -6 day'));
    $from_date1 = explode("-",$from_date);
    $fromdate = $from_date1[2]."-".$from_date1[1]."-".$from_date1[0];
}
if(isset($_POST["to_date"]))
{
    $to_date=$_POST["to_date"];
    $to_date1 = explode("-",$to_date);
    $todate = $to_date1[2]."-".$to_date1[1]."-".$to_date1[0].' 23:59:59';
}
else
{
    $todate=date("Y-m-d 23:59:59");
    $to_date=date("d-m-Y");
}
$db = new MyDatabase();
$conn = $db->getConnection();
$start = !empty($_GET['per_page']) ? $_GET['per_page'] : 0;
$number_of_rows = 25;
$where = "";

    if (!empty($fromdate)) {
        $where .= !empty($fromdate) ? " created_date_time >= '".$fromdate."' " : '';
    }
    if (!empty($todate)) {
        $where .= !empty($where) ? ' AND ' : '';
        $where .= !empty($todate) ? " created_date_time <= '".$todate."' " : '';
    }

$total_rows_query = "SELECT  count(*) as rowCount FROM quran_recitation_form ".(!empty($where) ? ' WHERE '. $where : '');
$query_result = mysql_query($total_rows_query, $conn);
$totalRows = 0;
while ($countResult = mysql_fetch_object($query_result)) {
    $totalRows = $countResult->rowCount;
}

$config['page_query_string'] = TRUE;
$config['enable_query_strings'] = TRUE;
$config['base_url'] = 'http://'.$_SERVER['HTTP_HOST'].'/admin/quranrecitationcourse.php?';

$config['cur_page'] = $start;
$config['total_rows'] = $totalRows;
$config['per_page'] = $number_of_rows;

$config['full_tag_open'] = '<div class="pagination">';
$config['full_tag_close'] = '</div>';

$config['first_link'] = 'FIRST';;
$config['first_tag_open'] = '<div class="main-page">';
$config['first_tag_close'] = '</div>';

$config['prev_link'] = 'PREV';
$config['prev_tag_open'] = '<div class="main-page" >';
$config['prev_tag_close'] = '</div>';

$config['num_tag_open'] = '<div>';
$config['num_tag_close'] = '</div>';

$config['next_link'] = '<div>NEXT</div>';
$config['next_tag_open'] = '<div class="main-page">';
$config['next_tag_close'] = '</div>';

$config['cur_tag_open'] = '<div><a class="current">';
$config['cur_tag_close'] = '</a></div>';

$config['last_link'] = '<div class="main-page">LAST</div>';;
$config['last_tag_open'] = '<div>';
$config['last_tag_close'] = '</div>';


$pagination = new CI_Pagination();
$pagination->initialize($config);

$sql = "SELECT  * FROM quran_recitation_form ".(!empty($where) ? ' WHERE '. $where : '')." order by created_date_time DESC LIMIT $start, $number_of_rows ";
$query_result = mysql_query($sql, $conn);
$count = mysql_num_rows($query_result);


?>
    <h1 style="margin-left:25px;">Applications To Quran Recitation Course</h1>
    <div id="toursPrintDiv" style="clear:both;">

        <style>
            .menu_list_table thead tr{background:transparent}
            .menu_list_table thead td{padding:0!important;border:0;text-align:center}
            .menu_list_table input,.menu_list_table select{width:auto!important;height:auto!important}
            .menu_list_table thead select,.menu_list_table thead input[type="text"]{width:99%!important}
            .menu_list_table thead input[type="submit"]{width:100px!important;text-align:center}
            .menu_list_table th{padding-left:10px;padding-right:10px}
            .pagination div {  width: 13px; float: left;  }
            .pagination{  margin-left: auto;  margin-right: auto;  width: 200px;  }
            .pagination div.main-page{width: 40px;}
        </style>

        <div style="width:150px; margin:0 auto; float:right">
            <div align="center" style="width:50px; float:left;"><a href="javascript:expXLS()"><img src="images/excel_icon.jpg" border="0" style="width:30px" align="middle"></a></div>
        </div>


            <table align="center" class="menu_list_table" style="margin-top:15px;" width="97%">
                <form action="<?php echo 'http://'.$_SERVER['HTTP_HOST'].'/admin/quranrecitationcourse.php'?>" method="post" id="frmAdvanceReport">
                    <input type="hidden" name="filter" value="1">
                <thead>
                    <tr>
                        <td colspan="14">
                            <?php if (!empty($_SESSION['success'])){ echo '<div style="text-align: center;color: green;">'.$_SESSION['success'].'</div>'; unset($_SESSION['success']); }?>
                            <?php if (!empty($_SESSION['error'])){ echo '<div style="text-align: center;color: red;">'.$_SESSION['error'].'</div>'; unset($_SESSION['error']); }?>
                            <?php if (!empty($_SESSION['vip_id'])){ echo '<script> alert("Entry has been saved. Reference Number is - '.$_SESSION['vip_id'].'");</script>'; unset($_SESSION['vip_id']); }?>
                            <table align="center">
                                <tbody>
                                <tr>
                                    <td>From <br><input type="text" id="from_date" name="from_date" style="width:80px;" value="<?php echo $from_date;?>"> </td>
                                    <td>To<br><input type="text" id="to_date" name="to_date" style="width:80px;" value="<?php echo $to_date;?>"> </td>

                                    <td>&nbsp;<br> <input type="submit" value="Submit" name="submit"> </td>
                                </tr></tbody></table>

                        </td>
                    </tr>
                </form>
                </thead><?php
                if ($count > 0) {?>

                <tbody>
                    <tr class="nodrag">
                        <th>Reference number</th>
                        <th>Name</th>
                        <th>Age</th>
                        <th>Gender</th>
                        <th>Nationality</th>
                        <th>City</th>
                        <th>Contact No.</th>
                        <th>Email</th>
                        <th>Education level</th>
                        <th>Memorization Level</th>
                        <th>Participated Previously Courses?</th>
                        <th>Time of Registration</th>
                    </tr>

                <tr></tr>
                <?php
                $counter = 1;
                while ($data_row = mysql_fetch_object($query_result)) {?>
                    <tr>
                        <td style="background:<?php echo (($counter%2) == 0 ? '#999' : '#ddd');?>;"><?php echo 'QR-'.str_pad($data_row->id, 4, '0', STR_PAD_LEFT);?>&nbsp;</td/>
                        <td style="background:<?php echo (($counter%2) == 0 ? '#999' : '#ddd');?>;"><?php echo $data_row->name;?>&nbsp;</td/>
                        <td style="background:<?php echo (($counter%2) == 0 ? '#999' : '#ddd');?>;"><?php echo $data_row->age;?>&nbsp;</td>
                        <td style="background:<?php echo (($counter%2) == 0 ? '#999' : '#ddd');?>;"><?php echo $data_row->gender;?>&nbsp;</td/>
                        <td style="background:<?php echo (($counter%2) == 0 ? '#999' : '#ddd');?>;"><?php echo $data_row->nationality;?>&nbsp;</td/>
                        <td style="background:<?php echo (($counter%2) == 0 ? '#999' : '#ddd');?>;"><?php echo $data_row->city_name;?>&nbsp;</td/>
                        <td style="background:<?php echo (($counter%2) == 0 ? '#999' : '#ddd');?>;"><?php echo $data_row->contact_no;?>&nbsp;</td/>
                        <td style="background:<?php echo (($counter%2) == 0 ? '#999' : '#ddd');?>;"><?php echo $data_row->email;?>&nbsp;</td/>
                        <td style="background:<?php echo (($counter%2) == 0 ? '#999' : '#ddd');?>;"><?php echo $data_row->education_level;?>&nbsp;</td/>
                        <td style="background:<?php echo (($counter%2) == 0 ? '#999' : '#ddd');?>;"><?php echo $data_row->memorization_level;?>&nbsp;</td/>
                        <td style="background:<?php echo (($counter%2) == 0 ? '#999' : '#ddd');?>;"><?php echo $data_row->already_participated;?>&nbsp;</td/>


                        <td style="background:<?php echo (($counter%2) == 0 ? '#999' : '#ddd');?>;"><?php echo date('d-m-Y', strtotime($data_row->created_date_time));?>&nbsp;</td/>
                    </tr><?php
                    $counter++;
                }?>
                    <tr>
                        <td colspan="14"><?php
                            echo $pagination->create_links();
                            ?>
                        </td>
                    </tr>
                </tbody>
                <?php }?>
            </table>
            <div align="center" style="margin-top:10px;"><span style="font-size:16px; font-weight:bold;">Results Count = <?php echo $totalRows;?></span></div>
    </div>
<?php
do_footer();
?>