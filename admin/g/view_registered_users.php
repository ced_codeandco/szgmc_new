<?php
include_once ('includes/commons.php');
do_header();
?>
		<script type="text/javascript" charset="utf-8">			
				var oTable;
				$(document).ready(function() {
				oTable=$('#my_table').dataTable( {
					"bProcessing": true,					
					"bJQueryUI": true,
					"sPaginationType": "full_numbers",
					"iDisplayLength": 10,
					"bServerSide": true,					 
					"sAjaxSource": 'src/common/json/server_side_json_registered_users.php?type=<?php echo $_GET['type']; ?>',
					"aaSorting": [[0,'desc']],
					"aoColumns": [						
						{ bSortable: true, sWidth: '40px' },
						{ bSortable: true, sWidth: '80px' },
						{ bSortable: true, sWidth: '20px' },						
						{ bSortable: true, sWidth: '80px' },
						{ bSortable: true, sWidth: '80px' },
						{ bSortable: true, sWidth: '80px' },
						{ bSortable: true, sWidth: '80px' },
						{ bSortable: true, sWidth: '80px' },
						{ bSortable: true, sWidth: '40px' },
						{ bSortable: true, sWidth: '80px' },
						{ bSortable: true, sWidth: '80px' },
						{ bSortable: true, sWidth: '80px' },
						{ bSortable: true, sWidth: '80px' },
						{ bSortable: true, sWidth: '80px' },
						{ bSortable: false, sWidth: '40px' },
						{ bSortable: false, sWidth: '40px' },
						<?php if((isset($_GET['type'])) && ($_GET['type']=='D' || $_GET['type']=='T' ||$_GET['type']=='A')){  ?>
							{ bSortable: false, sWidth: '40px' },
						<?php } ?>
						{ bSortable: true, sWidth: '80px' }
					]
					
				} );
			} );			
			
		</script>
		<script language="javascript" type="text/javascript">

function getXMLHTTP() { //fuction to return the xml http object
		var xmlhttp=false;	
		try{
			xmlhttp=new XMLHttpRequest();
		}
		catch(e)	{		
			try{			
				xmlhttp= new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch(e){
				try{
				xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
				}
				catch(e1){
					xmlhttp=false;
				}
			}
		}
		 	
		return xmlhttp;
    }
	
function setstatus(gid,id,name) {	
		
		var strURL="markApproveStatus.php?gid="+gid+"&id="+id;
		var req = getXMLHTTP();
		msg = 'Are you sure?';
		if(gid != '')
		{
			if (confirm(msg)){


		if (req) {
			
			req.onreadystatechange = function() {
				if (req.readyState == 4) {
					// only if "OK"
					if (req.status == 200) {						
						//document.getElementById('guide').innerHTML=req.responseText;	
						location.reload();				
					} else {
						alert("There was a problem while using XMLHTTP:\n" + req.statusText);
					}
				}				
			}			
			req.open("GET", strURL, true);
			req.send(null);
			
		} }
		else
		{
		return false;		
		}
		}
	
}
	</script>
<?php if(isset($_GET['type']) && $_GET['type']=='A'){  ?>
			<h2 style="padding-left:20px;">Approved Registered Users</h2>
		<?php }else if(isset($_GET['type']) && $_GET['type']=='T'){  ?>
			<h2 style="padding-left:20px;">Temporary Approved Registered Users</h2>
		<?php }else if(isset($_GET['type']) && $_GET['type']=='D'){  ?>
			<h2 style="padding-left:20px;">Disapproved Registered Users</h2>
		<?php }else{ ?>
			<h2 style="padding-left:20px;">View Registered Users</h2>
            
		<?php } ?>
<div align="center" style="width:50px; float:right;"><a href="export_registered_users.php?status=all"><img src="images/excel_icon.jpg" border="0" style="width:30px"></a></div>
<table width="100%"  cellpadding="3" cellspacing="3" align="center">
<tr><td align="center" valign="top" >
<div id="container">

<table cellpadding="0" cellspacing="0" border="0" class="display" id="my_table">
	<thead>
		<tr>
			<th>ID</th>
			<th>Reg date</th>
			<th>Username</th>
            <th>Company Name</th>
            <th>Emirate</th>
            <th>Trade License No.</th>
            <th>Trade License</th>
	    <th>Date of Expiry</th>
		<th>Status</th>
            <th>GM Name</th>
            <th>GM No.</th>
            <th>GM Email</th>
            <th>Operation Manager</th>
            <th>Operation Tel. No.</th>
            <th>Operations Mob No.</th>
            <th>Opeartion Email ID</th>
            <?php if(isset($_GET['type']) && $_GET['type']=='D'){  ?>
			 <th>Disapproved By</th>
		<?php }else if(isset($_GET['type']) && $_GET['type']=='A'){  ?>
			 <th>Approved By</th>
		<?php }else if(isset($_GET['type']) && $_GET['type']=='T'){  ?>
			 <th>Temporary Approved By</th>
		<?php }  ?>
            <th>Actions</th>
		</tr>
	</thead>
	<tbody>
	</tbody>
</table>	
</div>
</td></tr>
</table>
<?php
do_footer();
?>


