<?php
/**
 * Created by PhpStorm.
 * User: Anas
 * Date: 5/11/2015
 * Time: 9:54 AM
 */
include_once($_SERVER['DOCUMENT_ROOT'].'/en/includes/generic_functions.php');
include '../includes/database.php';
include_once ('includes/commons.php');

/**
 * Add vips visits
 */
if (!empty($_POST['btnsubmit']) && $_POST['btnsubmit'] == 'Save') {
    if (!empty($_POST['date']) && !empty($_POST['time']) && !empty($_POST['DelegateNameAndTitle']) && !empty($_POST['groupSize'])) {
        $db = new MyDatabase();
        $conn = $db->getConnection();

        $formData = $_POST;
        $dateParts = explode('-', $formData['date']);
        $formatedDate = $dateParts[2].'-'.$dateParts[1].'-'.$dateParts[0];
        $dateTime = $formatedDate.' '.$formData['time'];
        $sql = "INSERT INTO `vip_visits` (`dateTime`, `DelegateNameAndTitle`, `groupSize`, `email`, `contactName`, `phoneNumber`, `accompanyingName`, `accompanyingNumber`, `createdDate`)
                  VALUES ('$dateTime', '".$formData['DelegateNameAndTitle']."', '".$formData['groupSize']."', '".$formData['email']."', '".$formData['contactName']."', '".$formData['phoneNumber']."', '".$formData['accompanyingName']."', '".$formData['accompanyingNumber']."', '".date('Y-m-d H:i:s')."')";
        $query = mysql_query($sql, $conn);
        $id = mysql_insert_id($conn);
        //var_dump($id);
        //echo "11111111";
        if (!empty($id)) {
            $_SESSION['vip_id'] = 'VVIP'.str_pad($id, 4, '0', STR_PAD_LEFT);
            $_SESSION['success'] = 'VIP List added successfully!';
        } else {
            $_SESSION['error'] = 'Something went wrong! Please try again later';
        }/*
        echo "2222";
        var_dump($_SERVER);*/
        header("location: http://".$_SERVER['HTTP_HOST'].'/admin/vip_visits_list.php');
        //echo "3333";
        exit();
    } else {
        $_SESSION['error'] = 'Failed to add VIP Lists!<br /> Required fields are missing';
        header("location:".$_SERVER['PHP_SELF']);
        exit();
    }
}
//Print header file from commons.php
do_header();
?>

<table width="100%" height="100%" cellspacing="3" cellpadding="3" border="0" align="center" style="background-color:#E4F0FA;">
    <tbody>
        <tr><th valign="top" align="center">Add VIP Visits</th></tr>
        <tr>
            <td valign="top" align="center">
                <form enctype="multipart/form-data" id="addVipForm" name="addVipForm" method="post" action="<?php echo $_SERVER['PHP_SELF']?>">
                    <table cellspacing="2" cellpadding="2" border="0" align="center" style="border:0px #cccccc solid; padding:10px; background-color:#E4F0FA;">
                        <tbody>
                            <tr>
                                <td>
                                <?php if (!empty($_SESSION['success'])){ echo '<div style="text-align: center;color: green;">'.$_SESSION['success'].'</div>'; unset($_SESSION['success']); }?>
                                <?php if (!empty($_SESSION['error'])){ echo '<div style="text-align: center;color: red;">'.$_SESSION['error'].'</div>'; unset($_SESSION['error']); }?>
                                <table cellspacing="2" cellpadding="2" border="0" align="center" style="border:1px #cccccc solid; padding:10px; background-color:#E4F0FA;">
                                    <tbody>

                                        <tr>
                                            <td>Date of visit&nbsp;<font color="red">*</font></td>
                                            <td>
                                                <input type="text" value="" size="70" id="date_of_visit" name="date" style="width:100px;" class="required">
                                                Time:
                                                <select name="time" class="required">
                                                    <?php
                                                    $start = "00:00";
                                                    $end = "24:00";

                                                    $tStart = strtotime($start);
                                                    $tEnd = strtotime($end);
                                                    $tNow = $tStart;
                                                    $timeArray = array();
                                                    while($tNow < $tEnd){
                                                        $timeArray[] = $tNow;
                                                        $tNow = strtotime('+30 minutes',$tNow);
                                                    }
                                                    foreach ($timeArray as $time) {
                                                        echo '<option value="'.date('H:i', $time).'">' . date('g:i A', $time) . '</option>';
                                                    }
                                                    ?>

                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Name & title of Delegates &nbsp;<font color="red">*</font></td>
                                            <td><input type="text" value="" size="70" id="DelegateNameAndTitle" name="DelegateNameAndTitle" class="required"></td>
                                        </tr>
                                        <tr>
                                            <td>Group size&nbsp;<font color="red">*</font></td>
                                            <td><input type="number" value="" size="70" id="groupSize" name="groupSize" class="required"></td>
                                        </tr>
                                        <tr>
                                            <td>Email &nbsp;</td>
                                            <td><input type="email" value="" size="70" id="email" name="email"></td>
                                        </tr>
                                        <tr>
                                            <td>Contact name </td>
                                            <td><input type="text" value="" size="70" id="contactName" name="contactName"></td>
                                        </tr>
                                        <tr>
                                            <td>Phone number</td>
                                            <td><input type="text" value="" size="70" id="phoneNumber" name="phoneNumber"></td>
                                        </tr>
                                        <tr>
                                            <td>Contact name of person accompanying the group</td>
                                            <td><input type="text" value="" size="70" id="accompanyingName" name="accompanyingName"></td>
                                        </tr>
                                        <tr>
                                            <td>Contact mobile number of person accompanying the group</td>
                                            <td><input type="text" value="" size="70" id="accompanyingNumber" name="accompanyingNumber"></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="2">
                                                <input type="submit" onclick="return validate_form();" value="Save" name="btnsubmit">
                                                <input type="button" accesskey="b" onclick="javascript:history.back();" value="Back" name="btnback">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </form>
            </td>
        </tr>
    </tbody>
</table>

<script type="text/javascript">
    $(function() {
        $("#date_of_visit").datepicker({dateFormat: 'dd-mm-yy'});
        //$("#to_date").datepicker({dateFormat: 'dd-mm-yy'});
        $('#addVipForm').submit(function() {
            var error = 0;
            $('.required').each(function () {
                if ($(this).val() == '') {
                    if ($(this).parent().find('p.errorMessage').length > 0) {
                        $(this).parent().find('p.errorMessage').html('This field is required.');
                    } else {
                        $(this).parent().append("<p class='errorMessage'>This field is required.</p>");
                    }
                    error = 1;
                }
            })
            if (error == 1) { return false; }
        })
    });
</script>
<style>.errorMessage{margin: 0;
        padding: 0;
        font-size: 10px;
        height: 14px;
        color: #F00;}</style>
<?php
do_footer();
?>